package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Documento;
import es.uji.apps.bec.model.HistoricoBeca;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface BecaDAO extends BaseDAO
{
    boolean existeBeca(Long solicitanteId, Long convocatoriaId, Long estudioId);

    boolean isDuplicada(Long solicitanteId, Long convocatoriaId, Long estudioId);

    Beca getBeca(Long solicitanteId, Long convocatoriaId, Long estudioId);

    Beca getBecaById(Long becaId);

    Beca getBecaByCursoConvocatoriaTandaIdentificacion(Integer cursoAcademicoId,
            String convocatoria, Integer tanda, String identificacion);

    Beca getBecaByArchivoTemporal(Long archivoTemporal);

    List<Beca> getBecasByTandaId(Long tandaId);


    boolean existenBecasConEstudio(Long solicitanteId, Long convocatoriaId);

    List<Beca> getHistoricoObservacionesByBecaAndPersona(Long becaId, Long personaId);

    List<HistoricoBeca> getHistoricoEstadosByBeca(Long becaId);

    Long getNumeroBecasPorTandaAgrupadasPorEstadoConvocatoriaYProceso(Long tandaId);

    Long getNumeroBecasPorTandaEnEstadoNoDenegado(Long tandaId);

    Long getNumeroBecasPorTandaEnEstadoNoEnviado(Long tandaId);

    Long getResumenNumeroBecasNoNotificadasPorTanda(Long tandaId);

    Long getNumeroBecasPorTanda(Long tandaId);

    void updateOtrosDatos(Beca beca);

    void updateDatosGenerales(Beca beca);

    void updateConvocatoria(Beca beca);

    void updateEstado(Beca beca);

    void updateAcademicos(Beca beca);

    void updateEstudios(Beca beca);

    void updateObservacionesUji(Beca beca);

    void updateTanda(Beca beca);

    void updateBecaConcedida(Beca beca);

    void updateReclamada(Beca beca);

    List<Beca> getBecasByOrganismoIdAndCursoAcademicoId(Long organismoId, Long cursoAcademicoId);

    List<Beca> getBecasByOrganismoIdAndCursoAcademicoIdAndProcesoIdAndEstadoIdAndConvocatoriaId(
            Long organismoId, Long cursoAcademicoId, Long procesoId, Long estadoId,
            Long convocatoriaId);

    Beca getBecasByConvocatoriaIdAndNifAndCursoAcademico(Long convocatoria, String nif,
            Long cursoAcademico) throws BecaException;

    Beca updateBeca(Beca beca);

    List<Documento> getDocumentosPendientesNotificacion(Long id);

}
