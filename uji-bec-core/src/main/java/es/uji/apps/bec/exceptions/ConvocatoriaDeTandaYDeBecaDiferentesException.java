package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ConvocatoriaDeTandaYDeBecaDiferentesException extends CoreDataBaseException
{
    public ConvocatoriaDeTandaYDeBecaDiferentesException()
    {
        super("La convocatòria de la Beca i la de la Tanda son diferents.");
    }

    public ConvocatoriaDeTandaYDeBecaDiferentesException(String message)
    {
        super(message);
    }
}