package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class RecursoDeTandaNoEncontradoException extends CoreBaseException
{
    public RecursoDeTandaNoEncontradoException()
    {
        super("No s'ha trobat el recurs de una de les beques de la tanda");
    }

    public RecursoDeTandaNoEncontradoException(String message)
    {
        super(message);
    }
}
