package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaConDenegacionesException extends CoreDataBaseException
{
    public BecaConDenegacionesException()
    {
        super("La beca te denegacions. No es poden calcular les ajudes.");
    }

    public BecaConDenegacionesException(String message)
    {
        super(message);
    }
}
