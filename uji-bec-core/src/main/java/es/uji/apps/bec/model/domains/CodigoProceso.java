package es.uji.apps.bec.model.domains;

public enum CodigoProceso
{
    ORDINARIO(1L),

    ALEGACION(2L),

    AUMENTO_CUANTIA(3L),

    RECURSO(4L);

    private Long id;

    CodigoProceso(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public boolean igual(Object otherObject)
    {
        if (otherObject instanceof Long)
        {
            return ((Long) otherObject)== getId();
        }

        return super.equals(otherObject);
    }
}
