package es.uji.apps.bec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.BecaEstudioDAO;

@SuppressWarnings("serial")
@Component
@Entity
@Table(name="BC2_V_BECAS_ESTUDIOS")
public class BecaEstudio implements Serializable
{
    @Id
    private Long id;
    
    @Column(name = "estudio_id")
    private Long estudioId;
    
    @Column(name = "numero_cursos")
    private Long numeroCursos;
    
    @Column(name = "creditos_totales")
    private Float creditosTotales;
    
    private String presencial;
    
    @Column(name = "sin_docencia")
    private Boolean sinDocencia;
    
    @Column(name = "creditos_beca_completa")
    private Float creditosBecaCompleta;
    
    @Column(name = "creditos_beca_parcial")
    private Float creditosBecaParcial;
    
    @Column(name = "tipo_matricula_id")
    private Long tipoMatriculaId;
    
    @Column(name = "beca_id")
    private Long becaId;
    
    @Column(name = "curso_academico_id")
    private Long cursoAcademicoId;
    
    @Column(name = "curso_academico_activo")
    private Boolean cursoAcademicoActivo;
    
    @Column(name = "TITULACION_TECNICA")
    private Boolean titulacionTecnica;    

    private static BecaEstudioDAO becaEstudioDAO;
    
    @Autowired
    public void setBecaEstudioDAO(BecaEstudioDAO becaEstudioDAO)
    {
        BecaEstudio.becaEstudioDAO = becaEstudioDAO;
    }    
    
    public BecaEstudio()
    {
        this.setCursoAcademicoActivo(false);
        this.setSinDocencia(false);
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getTipoMatriculaId()
    {
        return tipoMatriculaId;
    }

    public void setTipoMatriculaId(Long tipoMatriculaId)
    {
        this.tipoMatriculaId = tipoMatriculaId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getNumeroCursos()
    {
        return numeroCursos;
    }

    public void setNumeroCursos(Long numeroCursos)
    {
        this.numeroCursos = numeroCursos;
    }

    public Float getCreditosTotales()
    {
        return creditosTotales;
    }

    public void setCreditosTotales(Float creditosTotales)
    {
        this.creditosTotales = creditosTotales;
    }

    public String getPresencial()
    {
        return presencial;
    }

    public void setPresencial(String presencial)
    {
        this.presencial = presencial;
    }

    public Boolean isSinDocencia()
    {
        return sinDocencia;
    }

    public void setSinDocencia(Boolean sinDocencia)
    {
        this.sinDocencia = sinDocencia;
    }

    public Float getCreditosBecaCompleta()
    {
        return creditosBecaCompleta;
    }

    public void setCreditosBecaCompleta(Float creditosBecaCompleta)
    {
        this.creditosBecaCompleta = creditosBecaCompleta;
    }

    public Float getCreditosBecaParcial()
    {
        return creditosBecaParcial;
    }

    public void setCreditosBecaParcial(Float creditosBecaParcial)
    {
        this.creditosBecaParcial = creditosBecaParcial;
    }

    public Long getBecaId()
    {
        return becaId;
    }

    public void setBecaId(Long becaId)
    {
        this.becaId = becaId;
    }

    public Long getCursoAcademicoId()
    {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Boolean isCursoAcademicoActivo()
    {
        return cursoAcademicoActivo;
    }

    public void setCursoAcademicoActivo(Boolean cursoAcademicoActivo)
    {
        this.cursoAcademicoActivo = cursoAcademicoActivo;
    }
    
    public Boolean isTitulacionTecnica()
    {
        return titulacionTecnica;
    }

    public void setTitulacionTecnica(Boolean titulacionTecnica)
    {
        this.titulacionTecnica = titulacionTecnica;
    }

    public static BecaEstudio getBecaEstudioByBecaId(Long becaId)
    {
        return becaEstudioDAO.getBecaEstudioByBecaId(becaId);
    }    
}
