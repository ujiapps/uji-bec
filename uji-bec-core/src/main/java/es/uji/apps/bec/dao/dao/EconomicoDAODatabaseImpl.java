package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.Economico;
import es.uji.apps.bec.model.QEconomico;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EconomicoDAODatabaseImpl extends BaseDAODatabaseImpl implements EconomicoDAO
{
    private QEconomico qEconomico = QEconomico.economico;

    @Override
    public List<Economico> getEconomicoById(Long economicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEconomico).where(qEconomico.id.eq(economicoId));
        return query.list(qEconomico);
    }

    @Override
    public List<Economico> getEconomicoBySolicitanteId(Long solicitanteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEconomico).where(qEconomico.solicitante.id.eq(solicitanteId));
        return query.list(qEconomico);
    }

    @Override
    @Transactional
    public void deleteEconomico(Economico economico)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qEconomico);
        delete.where(qEconomico.id.eq(economico.getId())).execute();
    }

    @Override
    @Transactional
    public void updateEconomico(Economico economico)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qEconomico);
        updateClause.where(qEconomico.id.eq(economico.getId()))
                .set(qEconomico.coeficientePrelacion, economico.getCoeficientePrelacion())
                .set(qEconomico.indBecario, economico.getIndBecario())
                .set(qEconomico.umbralBecario, economico.getUmbralBecario())
                .set(qEconomico.indCompensa, economico.getIndCompensa())
                .set(qEconomico.umbralCompensa, economico.getUmbralCompensa())
                .set(qEconomico.indTasas, economico.getIndTasas())
                .set(qEconomico.umbralTasas, economico.getUmbralTasas())
                .set(qEconomico.indMovGeneral, economico.getIndMovGeneral())
                .set(qEconomico.umbralMovGeneral, economico.getUmbralMovGeneral())
                .set(qEconomico.indMovEspecial, economico.getIndMovEspecial())
                .set(qEconomico.umbralMovEspecial, economico.getUmbralMovEspecial())
                .set(qEconomico.indResidMov, economico.getIndResidMov())
                .set(qEconomico.umbralResidMov, economico.getUmbralResidMov())
                .set(qEconomico.indPatrimonio, economico.getIndPatrimonio())
                .set(qEconomico.umbralPatrimonio, economico.getUmbralPatrimonio())
                .set(qEconomico.renta, economico.getRenta())
                .set(qEconomico.capitalMobiliario, economico.getCapitalMobiliario())
                .set(qEconomico.volumenNegocio, economico.getVolumenNegocio())
                .set(qEconomico.codRepesca, economico.getCodRepesca())
                .set(qEconomico.deducciones, economico.getDeducciones())
                .execute();
    }
}
