package es.uji.apps.bec.dao.dao;

import java.util.List;
import java.util.Map;

import es.uji.apps.bec.model.LoteMinisterio;
import es.uji.commons.db.BaseDAO;

public interface LoteMinisterioDAO extends BaseDAO
{
    List<LoteMinisterio> getLoteByLoteIdAndCursoAca(Long loteId, Long cursoAca);

    List<LoteMinisterio> getLoteByMultienvioId(Long multiEnvioId);

    List<LoteMinisterio> getLotes(Map<String, String> filtros);
}
