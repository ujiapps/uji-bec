package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class SolicitudTypeDatosDuplicadosException extends CoreDataBaseException
{
    public SolicitudTypeDatosDuplicadosException()
    {
        super("Dues solicituts descargades del Ministeri tenen el mateix Codi d'Arxiu Temporal i NIF");
    }

    public SolicitudTypeDatosDuplicadosException(String message)
    {
        super(message);
    }
}