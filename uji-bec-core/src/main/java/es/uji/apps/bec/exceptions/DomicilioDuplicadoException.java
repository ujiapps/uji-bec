package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class DomicilioDuplicadoException extends GeneralBECException
{
    public DomicilioDuplicadoException()
    {
        super("No es poden inserir dos domicilis del mateix tipus");
    }

    public DomicilioDuplicadoException(String message)
    {
        super(message);
    }
}