package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.TipoMinusvalia;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TipoMinusvaliaDAO extends BaseDAO
{
    List<TipoMinusvalia> getTiposMinusvalias();

    TipoMinusvalia getTipoMinusvaliaById(Long tipoMinusvaliaId);

    TipoMinusvalia getTipoMinusvaliaByOrden(String codigoMinusvalia);
    
    void deleteTipoMinusvalia(Long tipoIdentificacionId);
}