//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 01:14:30 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.denegaciones;


import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDate;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for FamiCatastralesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FamiCatastralesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FacaNumCatastro" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodIdentAEAT" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodResp" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodNat" minOccurs="0"/>
 *         &lt;element ref="{}FacaEjeFiscal" minOccurs="0"/>
 *         &lt;element ref="{}FacaAnoRevCat" minOccurs="0"/>
 *         &lt;element ref="{}FacaRefCat" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodVivHab" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodDerecho" minOccurs="0"/>
 *         &lt;element ref="{}FacaValorCatastral" minOccurs="0"/>
 *         &lt;element ref="{}FacaPorcTitular" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodUso" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodCalCat" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodProvIne" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodMuniIne" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodProcInfo" minOccurs="0"/>
 *         &lt;element ref="{}FacaReferencia" minOccurs="0"/>
 *         &lt;element ref="{}FacaImpvcfurn" minOccurs="0"/>
 *         &lt;element ref="{}FacaImpvcfr" minOccurs="0"/>
 *         &lt;element ref="{}FacaFechaInicioTit" minOccurs="0"/>
 *         &lt;element ref="{}FacaFechaFinTit" minOccurs="0"/>
 *         &lt;element ref="{}FacaReserva" minOccurs="0"/>
 *         &lt;element ref="{}FacaInfoControl" minOccurs="0"/>
 *         &lt;element ref="{}FacaCodAgenTrib" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FamiCatastralesType", propOrder = {
    "facaNumCatastro",
    "facaCodIdentAEAT",
    "facaCodResp",
    "facaCodNat",
    "facaEjeFiscal",
    "facaAnoRevCat",
    "facaRefCat",
    "facaCodVivHab",
    "facaCodDerecho",
    "facaValorCatastral",
    "facaPorcTitular",
    "facaCodUso",
    "facaCodCalCat",
    "facaCodProvIne",
    "facaCodMuniIne",
    "facaCodProcInfo",
    "facaReferencia",
    "facaImpvcfurn",
    "facaImpvcfr",
    "facaFechaInicioTit",
    "facaFechaFinTit",
    "facaReserva",
    "facaInfoControl",
    "facaCodAgenTrib"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.denegaciones.FamiCatastralesType")
@Table(name = "FAMICATASTRALESTYPE", schema = "UJI_BECAS_WS_DENEGACIONES")
@Inheritance(strategy = InheritanceType.JOINED)
public class FamiCatastralesType
    implements Equals, HashCode
{

    @XmlElement(name = "FacaNumCatastro")
    protected Long facaNumCatastro;
    @XmlElement(name = "FacaCodIdentAEAT")
    protected String facaCodIdentAEAT;
    @XmlElement(name = "FacaCodResp")
    protected String facaCodResp;
    @XmlElement(name = "FacaCodNat")
    protected String facaCodNat;
    @XmlElement(name = "FacaEjeFiscal")
    protected String facaEjeFiscal;
    @XmlElement(name = "FacaAnoRevCat")
    protected String facaAnoRevCat;
    @XmlElement(name = "FacaRefCat")
    protected String facaRefCat;
    @XmlElement(name = "FacaCodVivHab")
    protected String facaCodVivHab;
    @XmlElement(name = "FacaCodDerecho")
    protected String facaCodDerecho;
    @XmlElement(name = "FacaValorCatastral")
    protected Float facaValorCatastral;
    @XmlElement(name = "FacaPorcTitular")
    protected Float facaPorcTitular;
    @XmlElement(name = "FacaCodUso")
    protected String facaCodUso;
    @XmlElement(name = "FacaCodCalCat")
    protected String facaCodCalCat;
    @XmlElement(name = "FacaCodProvIne")
    protected String facaCodProvIne;
    @XmlElement(name = "FacaCodMuniIne")
    protected String facaCodMuniIne;
    @XmlElement(name = "FacaCodProcInfo")
    protected String facaCodProcInfo;
    @XmlElement(name = "FacaReferencia")
    protected String facaReferencia;
    @XmlElement(name = "FacaImpvcfurn")
    protected Float facaImpvcfurn;
    @XmlElement(name = "FacaImpvcfr")
    protected Float facaImpvcfr;
    @XmlElement(name = "FacaFechaInicioTit")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar facaFechaInicioTit;
    @XmlElement(name = "FacaFechaFinTit")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar facaFechaFinTit;
    @XmlElement(name = "FacaReserva")
    protected String facaReserva;
    @XmlElement(name = "FacaInfoControl")
    protected String facaInfoControl;
    @XmlElement(name = "FacaCodAgenTrib")
    protected String facaCodAgenTrib;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the facaNumCatastro property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "FACANUMCATASTRO", scale = 0)
    public Long getFacaNumCatastro() {
        return facaNumCatastro;
    }

    /**
     * Sets the value of the facaNumCatastro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFacaNumCatastro(Long value) {
        this.facaNumCatastro = value;
    }

    /**
     * Gets the value of the facaCodIdentAEAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODIDENTAEAT", length = 1)
    public String getFacaCodIdentAEAT() {
        return facaCodIdentAEAT;
    }

    /**
     * Sets the value of the facaCodIdentAEAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodIdentAEAT(String value) {
        this.facaCodIdentAEAT = value;
    }

    /**
     * Gets the value of the facaCodResp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODRESP", length = 1)
    public String getFacaCodResp() {
        return facaCodResp;
    }

    /**
     * Sets the value of the facaCodResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodResp(String value) {
        this.facaCodResp = value;
    }

    /**
     * Gets the value of the facaCodNat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODNAT")
    public String getFacaCodNat() {
        return facaCodNat;
    }

    /**
     * Sets the value of the facaCodNat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodNat(String value) {
        this.facaCodNat = value;
    }

    /**
     * Gets the value of the facaEjeFiscal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACAEJEFISCAL", length = 4)
    public String getFacaEjeFiscal() {
        return facaEjeFiscal;
    }

    /**
     * Sets the value of the facaEjeFiscal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaEjeFiscal(String value) {
        this.facaEjeFiscal = value;
    }

    /**
     * Gets the value of the facaAnoRevCat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACAANOREVCAT", length = 4)
    public String getFacaAnoRevCat() {
        return facaAnoRevCat;
    }

    /**
     * Sets the value of the facaAnoRevCat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaAnoRevCat(String value) {
        this.facaAnoRevCat = value;
    }

    /**
     * Gets the value of the facaRefCat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACAREFCAT", length = 20)
    public String getFacaRefCat() {
        return facaRefCat;
    }

    /**
     * Sets the value of the facaRefCat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaRefCat(String value) {
        this.facaRefCat = value;
    }

    /**
     * Gets the value of the facaCodVivHab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODVIVHAB", length = 1)
    public String getFacaCodVivHab() {
        return facaCodVivHab;
    }

    /**
     * Sets the value of the facaCodVivHab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodVivHab(String value) {
        this.facaCodVivHab = value;
    }

    /**
     * Gets the value of the facaCodDerecho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODDERECHO")
    public String getFacaCodDerecho() {
        return facaCodDerecho;
    }

    /**
     * Sets the value of the facaCodDerecho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodDerecho(String value) {
        this.facaCodDerecho = value;
    }

    /**
     * Gets the value of the facaValorCatastral property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "FACAVALORCATASTRAL", precision = 13, scale = 2)
    public Float getFacaValorCatastral() {
        return facaValorCatastral;
    }

    /**
     * Sets the value of the facaValorCatastral property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFacaValorCatastral(Float value) {
        this.facaValorCatastral = value;
    }

    /**
     * Gets the value of the facaPorcTitular property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "FACAPORCTITULAR", precision = 5, scale = 2)
    public Float getFacaPorcTitular() {
        return facaPorcTitular;
    }

    /**
     * Sets the value of the facaPorcTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFacaPorcTitular(Float value) {
        this.facaPorcTitular = value;
    }

    /**
     * Gets the value of the facaCodUso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODUSO", length = 1)
    public String getFacaCodUso() {
        return facaCodUso;
    }

    /**
     * Sets the value of the facaCodUso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodUso(String value) {
        this.facaCodUso = value;
    }

    /**
     * Gets the value of the facaCodCalCat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODCALCAT")
    public String getFacaCodCalCat() {
        return facaCodCalCat;
    }

    /**
     * Sets the value of the facaCodCalCat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodCalCat(String value) {
        this.facaCodCalCat = value;
    }

    /**
     * Gets the value of the facaCodProvIne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODPROVINE", length = 2)
    public String getFacaCodProvIne() {
        return facaCodProvIne;
    }

    /**
     * Sets the value of the facaCodProvIne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodProvIne(String value) {
        this.facaCodProvIne = value;
    }

    /**
     * Gets the value of the facaCodMuniIne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODMUNIINE", length = 3)
    public String getFacaCodMuniIne() {
        return facaCodMuniIne;
    }

    /**
     * Sets the value of the facaCodMuniIne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodMuniIne(String value) {
        this.facaCodMuniIne = value;
    }

    /**
     * Gets the value of the facaCodProcInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODPROCINFO")
    public String getFacaCodProcInfo() {
        return facaCodProcInfo;
    }

    /**
     * Sets the value of the facaCodProcInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodProcInfo(String value) {
        this.facaCodProcInfo = value;
    }

    /**
     * Gets the value of the facaReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACAREFERENCIA", length = 11)
    public String getFacaReferencia() {
        return facaReferencia;
    }

    /**
     * Sets the value of the facaReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaReferencia(String value) {
        this.facaReferencia = value;
    }

    /**
     * Gets the value of the facaImpvcfurn property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "FACAIMPVCFURN", precision = 13, scale = 2)
    public Float getFacaImpvcfurn() {
        return facaImpvcfurn;
    }

    /**
     * Sets the value of the facaImpvcfurn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFacaImpvcfurn(Float value) {
        this.facaImpvcfurn = value;
    }

    /**
     * Gets the value of the facaImpvcfr property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "FACAIMPVCFR", precision = 13, scale = 2)
    public Float getFacaImpvcfr() {
        return facaImpvcfr;
    }

    /**
     * Sets the value of the facaImpvcfr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFacaImpvcfr(Float value) {
        this.facaImpvcfr = value;
    }

    /**
     * Gets the value of the facaFechaInicioTit property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getFacaFechaInicioTit() {
        return facaFechaInicioTit;
    }

    /**
     * Sets the value of the facaFechaInicioTit property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFacaFechaInicioTit(XMLGregorianCalendar value) {
        this.facaFechaInicioTit = value;
    }

    /**
     * Gets the value of the facaFechaFinTit property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getFacaFechaFinTit() {
        return facaFechaFinTit;
    }

    /**
     * Sets the value of the facaFechaFinTit property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFacaFechaFinTit(XMLGregorianCalendar value) {
        this.facaFechaFinTit = value;
    }

    /**
     * Gets the value of the facaReserva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACARESERVA", length = 8)
    public String getFacaReserva() {
        return facaReserva;
    }

    /**
     * Sets the value of the facaReserva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaReserva(String value) {
        this.facaReserva = value;
    }

    /**
     * Gets the value of the facaInfoControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACAINFOCONTROL", length = 30)
    public String getFacaInfoControl() {
        return facaInfoControl;
    }

    /**
     * Sets the value of the facaInfoControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaInfoControl(String value) {
        this.facaInfoControl = value;
    }

    /**
     * Gets the value of the facaCodAgenTrib property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "FACACODAGENTRIB", length = 2)
    public String getFacaCodAgenTrib() {
        return facaCodAgenTrib;
    }

    /**
     * Sets the value of the facaCodAgenTrib property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacaCodAgenTrib(String value) {
        this.facaCodAgenTrib = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    @Basic
    @Column(name = "FACAFECHAINICIOTITITEM")
    @Temporal(TemporalType.DATE)
    public Date getFacaFechaInicioTitItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getFacaFechaInicioTit());
    }

    public void setFacaFechaInicioTitItem(Date target) {
        setFacaFechaInicioTit(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    @Basic
    @Column(name = "FACAFECHAFINTITITEM")
    @Temporal(TemporalType.DATE)
    public Date getFacaFechaFinTitItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getFacaFechaFinTit());
    }

    public void setFacaFechaFinTitItem(Date target) {
        setFacaFechaFinTit(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof FamiCatastralesType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final FamiCatastralesType that = ((FamiCatastralesType) object);
        {
            Long lhsFacaNumCatastro;
            lhsFacaNumCatastro = this.getFacaNumCatastro();
            Long rhsFacaNumCatastro;
            rhsFacaNumCatastro = that.getFacaNumCatastro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaNumCatastro", lhsFacaNumCatastro), LocatorUtils.property(thatLocator, "facaNumCatastro", rhsFacaNumCatastro), lhsFacaNumCatastro, rhsFacaNumCatastro)) {
                return false;
            }
        }
        {
            String lhsFacaCodIdentAEAT;
            lhsFacaCodIdentAEAT = this.getFacaCodIdentAEAT();
            String rhsFacaCodIdentAEAT;
            rhsFacaCodIdentAEAT = that.getFacaCodIdentAEAT();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodIdentAEAT", lhsFacaCodIdentAEAT), LocatorUtils.property(thatLocator, "facaCodIdentAEAT", rhsFacaCodIdentAEAT), lhsFacaCodIdentAEAT, rhsFacaCodIdentAEAT)) {
                return false;
            }
        }
        {
            String lhsFacaCodResp;
            lhsFacaCodResp = this.getFacaCodResp();
            String rhsFacaCodResp;
            rhsFacaCodResp = that.getFacaCodResp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodResp", lhsFacaCodResp), LocatorUtils.property(thatLocator, "facaCodResp", rhsFacaCodResp), lhsFacaCodResp, rhsFacaCodResp)) {
                return false;
            }
        }
        {
            String lhsFacaCodNat;
            lhsFacaCodNat = this.getFacaCodNat();
            String rhsFacaCodNat;
            rhsFacaCodNat = that.getFacaCodNat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodNat", lhsFacaCodNat), LocatorUtils.property(thatLocator, "facaCodNat", rhsFacaCodNat), lhsFacaCodNat, rhsFacaCodNat)) {
                return false;
            }
        }
        {
            String lhsFacaEjeFiscal;
            lhsFacaEjeFiscal = this.getFacaEjeFiscal();
            String rhsFacaEjeFiscal;
            rhsFacaEjeFiscal = that.getFacaEjeFiscal();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaEjeFiscal", lhsFacaEjeFiscal), LocatorUtils.property(thatLocator, "facaEjeFiscal", rhsFacaEjeFiscal), lhsFacaEjeFiscal, rhsFacaEjeFiscal)) {
                return false;
            }
        }
        {
            String lhsFacaAnoRevCat;
            lhsFacaAnoRevCat = this.getFacaAnoRevCat();
            String rhsFacaAnoRevCat;
            rhsFacaAnoRevCat = that.getFacaAnoRevCat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaAnoRevCat", lhsFacaAnoRevCat), LocatorUtils.property(thatLocator, "facaAnoRevCat", rhsFacaAnoRevCat), lhsFacaAnoRevCat, rhsFacaAnoRevCat)) {
                return false;
            }
        }
        {
            String lhsFacaRefCat;
            lhsFacaRefCat = this.getFacaRefCat();
            String rhsFacaRefCat;
            rhsFacaRefCat = that.getFacaRefCat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaRefCat", lhsFacaRefCat), LocatorUtils.property(thatLocator, "facaRefCat", rhsFacaRefCat), lhsFacaRefCat, rhsFacaRefCat)) {
                return false;
            }
        }
        {
            String lhsFacaCodVivHab;
            lhsFacaCodVivHab = this.getFacaCodVivHab();
            String rhsFacaCodVivHab;
            rhsFacaCodVivHab = that.getFacaCodVivHab();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodVivHab", lhsFacaCodVivHab), LocatorUtils.property(thatLocator, "facaCodVivHab", rhsFacaCodVivHab), lhsFacaCodVivHab, rhsFacaCodVivHab)) {
                return false;
            }
        }
        {
            String lhsFacaCodDerecho;
            lhsFacaCodDerecho = this.getFacaCodDerecho();
            String rhsFacaCodDerecho;
            rhsFacaCodDerecho = that.getFacaCodDerecho();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodDerecho", lhsFacaCodDerecho), LocatorUtils.property(thatLocator, "facaCodDerecho", rhsFacaCodDerecho), lhsFacaCodDerecho, rhsFacaCodDerecho)) {
                return false;
            }
        }
        {
            Float lhsFacaValorCatastral;
            lhsFacaValorCatastral = this.getFacaValorCatastral();
            Float rhsFacaValorCatastral;
            rhsFacaValorCatastral = that.getFacaValorCatastral();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaValorCatastral", lhsFacaValorCatastral), LocatorUtils.property(thatLocator, "facaValorCatastral", rhsFacaValorCatastral), lhsFacaValorCatastral, rhsFacaValorCatastral)) {
                return false;
            }
        }
        {
            Float lhsFacaPorcTitular;
            lhsFacaPorcTitular = this.getFacaPorcTitular();
            Float rhsFacaPorcTitular;
            rhsFacaPorcTitular = that.getFacaPorcTitular();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaPorcTitular", lhsFacaPorcTitular), LocatorUtils.property(thatLocator, "facaPorcTitular", rhsFacaPorcTitular), lhsFacaPorcTitular, rhsFacaPorcTitular)) {
                return false;
            }
        }
        {
            String lhsFacaCodUso;
            lhsFacaCodUso = this.getFacaCodUso();
            String rhsFacaCodUso;
            rhsFacaCodUso = that.getFacaCodUso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodUso", lhsFacaCodUso), LocatorUtils.property(thatLocator, "facaCodUso", rhsFacaCodUso), lhsFacaCodUso, rhsFacaCodUso)) {
                return false;
            }
        }
        {
            String lhsFacaCodCalCat;
            lhsFacaCodCalCat = this.getFacaCodCalCat();
            String rhsFacaCodCalCat;
            rhsFacaCodCalCat = that.getFacaCodCalCat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodCalCat", lhsFacaCodCalCat), LocatorUtils.property(thatLocator, "facaCodCalCat", rhsFacaCodCalCat), lhsFacaCodCalCat, rhsFacaCodCalCat)) {
                return false;
            }
        }
        {
            String lhsFacaCodProvIne;
            lhsFacaCodProvIne = this.getFacaCodProvIne();
            String rhsFacaCodProvIne;
            rhsFacaCodProvIne = that.getFacaCodProvIne();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodProvIne", lhsFacaCodProvIne), LocatorUtils.property(thatLocator, "facaCodProvIne", rhsFacaCodProvIne), lhsFacaCodProvIne, rhsFacaCodProvIne)) {
                return false;
            }
        }
        {
            String lhsFacaCodMuniIne;
            lhsFacaCodMuniIne = this.getFacaCodMuniIne();
            String rhsFacaCodMuniIne;
            rhsFacaCodMuniIne = that.getFacaCodMuniIne();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodMuniIne", lhsFacaCodMuniIne), LocatorUtils.property(thatLocator, "facaCodMuniIne", rhsFacaCodMuniIne), lhsFacaCodMuniIne, rhsFacaCodMuniIne)) {
                return false;
            }
        }
        {
            String lhsFacaCodProcInfo;
            lhsFacaCodProcInfo = this.getFacaCodProcInfo();
            String rhsFacaCodProcInfo;
            rhsFacaCodProcInfo = that.getFacaCodProcInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodProcInfo", lhsFacaCodProcInfo), LocatorUtils.property(thatLocator, "facaCodProcInfo", rhsFacaCodProcInfo), lhsFacaCodProcInfo, rhsFacaCodProcInfo)) {
                return false;
            }
        }
        {
            String lhsFacaReferencia;
            lhsFacaReferencia = this.getFacaReferencia();
            String rhsFacaReferencia;
            rhsFacaReferencia = that.getFacaReferencia();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaReferencia", lhsFacaReferencia), LocatorUtils.property(thatLocator, "facaReferencia", rhsFacaReferencia), lhsFacaReferencia, rhsFacaReferencia)) {
                return false;
            }
        }
        {
            Float lhsFacaImpvcfurn;
            lhsFacaImpvcfurn = this.getFacaImpvcfurn();
            Float rhsFacaImpvcfurn;
            rhsFacaImpvcfurn = that.getFacaImpvcfurn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaImpvcfurn", lhsFacaImpvcfurn), LocatorUtils.property(thatLocator, "facaImpvcfurn", rhsFacaImpvcfurn), lhsFacaImpvcfurn, rhsFacaImpvcfurn)) {
                return false;
            }
        }
        {
            Float lhsFacaImpvcfr;
            lhsFacaImpvcfr = this.getFacaImpvcfr();
            Float rhsFacaImpvcfr;
            rhsFacaImpvcfr = that.getFacaImpvcfr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaImpvcfr", lhsFacaImpvcfr), LocatorUtils.property(thatLocator, "facaImpvcfr", rhsFacaImpvcfr), lhsFacaImpvcfr, rhsFacaImpvcfr)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsFacaFechaInicioTit;
            lhsFacaFechaInicioTit = this.getFacaFechaInicioTit();
            XMLGregorianCalendar rhsFacaFechaInicioTit;
            rhsFacaFechaInicioTit = that.getFacaFechaInicioTit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaFechaInicioTit", lhsFacaFechaInicioTit), LocatorUtils.property(thatLocator, "facaFechaInicioTit", rhsFacaFechaInicioTit), lhsFacaFechaInicioTit, rhsFacaFechaInicioTit)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsFacaFechaFinTit;
            lhsFacaFechaFinTit = this.getFacaFechaFinTit();
            XMLGregorianCalendar rhsFacaFechaFinTit;
            rhsFacaFechaFinTit = that.getFacaFechaFinTit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaFechaFinTit", lhsFacaFechaFinTit), LocatorUtils.property(thatLocator, "facaFechaFinTit", rhsFacaFechaFinTit), lhsFacaFechaFinTit, rhsFacaFechaFinTit)) {
                return false;
            }
        }
        {
            String lhsFacaReserva;
            lhsFacaReserva = this.getFacaReserva();
            String rhsFacaReserva;
            rhsFacaReserva = that.getFacaReserva();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaReserva", lhsFacaReserva), LocatorUtils.property(thatLocator, "facaReserva", rhsFacaReserva), lhsFacaReserva, rhsFacaReserva)) {
                return false;
            }
        }
        {
            String lhsFacaInfoControl;
            lhsFacaInfoControl = this.getFacaInfoControl();
            String rhsFacaInfoControl;
            rhsFacaInfoControl = that.getFacaInfoControl();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaInfoControl", lhsFacaInfoControl), LocatorUtils.property(thatLocator, "facaInfoControl", rhsFacaInfoControl), lhsFacaInfoControl, rhsFacaInfoControl)) {
                return false;
            }
        }
        {
            String lhsFacaCodAgenTrib;
            lhsFacaCodAgenTrib = this.getFacaCodAgenTrib();
            String rhsFacaCodAgenTrib;
            rhsFacaCodAgenTrib = that.getFacaCodAgenTrib();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "facaCodAgenTrib", lhsFacaCodAgenTrib), LocatorUtils.property(thatLocator, "facaCodAgenTrib", rhsFacaCodAgenTrib), lhsFacaCodAgenTrib, rhsFacaCodAgenTrib)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theFacaNumCatastro;
            theFacaNumCatastro = this.getFacaNumCatastro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaNumCatastro", theFacaNumCatastro), currentHashCode, theFacaNumCatastro);
        }
        {
            String theFacaCodIdentAEAT;
            theFacaCodIdentAEAT = this.getFacaCodIdentAEAT();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodIdentAEAT", theFacaCodIdentAEAT), currentHashCode, theFacaCodIdentAEAT);
        }
        {
            String theFacaCodResp;
            theFacaCodResp = this.getFacaCodResp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodResp", theFacaCodResp), currentHashCode, theFacaCodResp);
        }
        {
            String theFacaCodNat;
            theFacaCodNat = this.getFacaCodNat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodNat", theFacaCodNat), currentHashCode, theFacaCodNat);
        }
        {
            String theFacaEjeFiscal;
            theFacaEjeFiscal = this.getFacaEjeFiscal();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaEjeFiscal", theFacaEjeFiscal), currentHashCode, theFacaEjeFiscal);
        }
        {
            String theFacaAnoRevCat;
            theFacaAnoRevCat = this.getFacaAnoRevCat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaAnoRevCat", theFacaAnoRevCat), currentHashCode, theFacaAnoRevCat);
        }
        {
            String theFacaRefCat;
            theFacaRefCat = this.getFacaRefCat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaRefCat", theFacaRefCat), currentHashCode, theFacaRefCat);
        }
        {
            String theFacaCodVivHab;
            theFacaCodVivHab = this.getFacaCodVivHab();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodVivHab", theFacaCodVivHab), currentHashCode, theFacaCodVivHab);
        }
        {
            String theFacaCodDerecho;
            theFacaCodDerecho = this.getFacaCodDerecho();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodDerecho", theFacaCodDerecho), currentHashCode, theFacaCodDerecho);
        }
        {
            Float theFacaValorCatastral;
            theFacaValorCatastral = this.getFacaValorCatastral();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaValorCatastral", theFacaValorCatastral), currentHashCode, theFacaValorCatastral);
        }
        {
            Float theFacaPorcTitular;
            theFacaPorcTitular = this.getFacaPorcTitular();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaPorcTitular", theFacaPorcTitular), currentHashCode, theFacaPorcTitular);
        }
        {
            String theFacaCodUso;
            theFacaCodUso = this.getFacaCodUso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodUso", theFacaCodUso), currentHashCode, theFacaCodUso);
        }
        {
            String theFacaCodCalCat;
            theFacaCodCalCat = this.getFacaCodCalCat();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodCalCat", theFacaCodCalCat), currentHashCode, theFacaCodCalCat);
        }
        {
            String theFacaCodProvIne;
            theFacaCodProvIne = this.getFacaCodProvIne();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodProvIne", theFacaCodProvIne), currentHashCode, theFacaCodProvIne);
        }
        {
            String theFacaCodMuniIne;
            theFacaCodMuniIne = this.getFacaCodMuniIne();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodMuniIne", theFacaCodMuniIne), currentHashCode, theFacaCodMuniIne);
        }
        {
            String theFacaCodProcInfo;
            theFacaCodProcInfo = this.getFacaCodProcInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodProcInfo", theFacaCodProcInfo), currentHashCode, theFacaCodProcInfo);
        }
        {
            String theFacaReferencia;
            theFacaReferencia = this.getFacaReferencia();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaReferencia", theFacaReferencia), currentHashCode, theFacaReferencia);
        }
        {
            Float theFacaImpvcfurn;
            theFacaImpvcfurn = this.getFacaImpvcfurn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaImpvcfurn", theFacaImpvcfurn), currentHashCode, theFacaImpvcfurn);
        }
        {
            Float theFacaImpvcfr;
            theFacaImpvcfr = this.getFacaImpvcfr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaImpvcfr", theFacaImpvcfr), currentHashCode, theFacaImpvcfr);
        }
        {
            XMLGregorianCalendar theFacaFechaInicioTit;
            theFacaFechaInicioTit = this.getFacaFechaInicioTit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaFechaInicioTit", theFacaFechaInicioTit), currentHashCode, theFacaFechaInicioTit);
        }
        {
            XMLGregorianCalendar theFacaFechaFinTit;
            theFacaFechaFinTit = this.getFacaFechaFinTit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaFechaFinTit", theFacaFechaFinTit), currentHashCode, theFacaFechaFinTit);
        }
        {
            String theFacaReserva;
            theFacaReserva = this.getFacaReserva();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaReserva", theFacaReserva), currentHashCode, theFacaReserva);
        }
        {
            String theFacaInfoControl;
            theFacaInfoControl = this.getFacaInfoControl();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaInfoControl", theFacaInfoControl), currentHashCode, theFacaInfoControl);
        }
        {
            String theFacaCodAgenTrib;
            theFacaCodAgenTrib = this.getFacaCodAgenTrib();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "facaCodAgenTrib", theFacaCodAgenTrib), currentHashCode, theFacaCodAgenTrib);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
