package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Denegacion;
import es.uji.apps.bec.model.QDenegacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DenegacionDAODatabaseImpl extends BaseDAODatabaseImpl implements DenegacionDAO
{
    private QDenegacion denegacion = QDenegacion.denegacion;

    @Override
    public List<Denegacion> getDenegaciones()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion).orderBy(denegacion.causa.asc(), denegacion.subCausa.asc());
        return query.list(denegacion);
    }

    @Override
    public List<Denegacion> getDenegacionesActivas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion).where(denegacion.activa.isTrue())
                .orderBy(denegacion.causa.asc(), denegacion.subCausa.asc());
        return query.list(denegacion);
    }

    @Override
    public Denegacion getDenegacionById(Long denegacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion).where(denegacion.id.eq(denegacionId));
        return query.uniqueResult(denegacion);
    }

    @Override
    public Denegacion getDenegacionByOrganismoEstadoCausaySubcausa(Long organismoId, String estado, String causa, String subcausa)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion).where(denegacion.organismo.id.eq(organismoId)
                .and(denegacion.estado.eq(estado))
                .and(denegacion.causa.eq(causa))
                .and(denegacion.subCausa.eq(subcausa)));
        return query.uniqueResult(denegacion);
    }

    public Denegacion getDenegacionByOrganismoYCausa(Long organismoId, String causa)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion).where(denegacion.organismo.id.eq(organismoId)
                .and(denegacion.activa.isTrue())
                .and(denegacion.causa.eq(causa)));

        return query.uniqueResult(denegacion);
    }

    @Override
    public List<Denegacion> getDenegacionesActivasByOrganismo(Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion)
                .where(denegacion.organismo.id.eq(organismoId).and(denegacion.activa.isTrue()))
                .orderBy(denegacion.causa.asc()).orderBy(denegacion.subCausa.asc());
        return query.list(denegacion);
    }

    @Override
    public List<Denegacion> getDenegacionesTodasByOrganismo(Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(denegacion)
                .where(denegacion.organismo.id.eq(organismoId))
                .orderBy(denegacion.causa.asc()).orderBy(denegacion.subCausa.asc());
        return query.list(denegacion);
    }

    @Override
    @Transactional
    public void deleteDenegacion(Long denegacionId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, denegacion);
        delete.where(denegacion.id.eq(denegacionId)).execute();
    }
}