package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Tanda;
import es.uji.commons.db.BaseDAO;

import java.util.List;
import java.util.Map;

public interface TandaDAO extends BaseDAO
{
    List<Tanda> getTandas(Map<String, String> filtros);

    List<Tanda> getTandasByTandaIdAndOrganismoIdAndCursoAcademicoId(Long tandaId, Long organismoId,
            Long cursoAcademicoId);

    List<Estado> getEstadosBecas(Long tandaId);

    List<Proceso> getProcesosBecas(Long tandaId);

    void updateTanda(Tanda tanda);

    Tanda getTandaByTandaIdAndCursoAca(Long tandaId, Long cursoAcaId);

    Long getMaximoTandaIdByCursoAca(Long cursoAcaId);
}
