package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.DomicilioDAO;
import es.uji.apps.bec.exceptions.DistanciaDomicilioFamiliarNoEncontradaException;
import es.uji.apps.bec.exceptions.DomicilioNoEncontradoException;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.model.domains.CodigoProvincia;
import es.uji.apps.bec.model.domains.MapeadorTipoDomicilio;
import es.uji.apps.bec.model.domains.MapeadorTipoResidencia;
import es.uji.apps.bec.model.domains.MapeadorTipoVia;
import es.uji.apps.bec.webservices.ministerio.solicitudes.ArrendatariosType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.DomicilioType;

/**
 * The persistent class for the BC2_DOMICILIOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_DOMICILIOS")
public class Domicilio implements Serializable
{
    private static DomicilioDAO domicilioDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "CODIGO_MUNICIPIO")
    private String codigoMunicipio;
    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;
    private Boolean correspondencia;
    private String escalera;
    private Boolean gratuito;
    @Column(name = "LOCALIDAD_NOMBRE")
    private String localidadNombre;
    @Column(name = "NOMBRE_VIA")
    private String nombreVia;
    private String numero;
    @Column(name = "OTRA_CLASE_DOMICILIO")
    private String otraClaseDomicilio;
    private String piso;
    private String puerta;
    @Column(name = "RESIDENCIA_ESP")
    private Boolean residenciaEsp;
    @Column(name = "IMPORTE_ALQUILER")
    private Float importeAlquiler;
    @Column(name = "ARRENDADOR_IDENTIFICACION")
    private String arrendadorIdentificacion;
    @Column(name = "ARRENDADOR_NOMBRE")
    private String arrendadorNombre;
    @Column(name = "NUMERO_ARRENDATARIOS")
    private Long numeroArrendatarios;
    @Column(name = "IMPORTE_FIANZA")
    private Float importeFianza;
    @Column(name = "FECHA_INICIO_CONTRATO")
    private Date fechaInicioContrato;
    @Column(name = "FECHA_FIN_CONTRATO")
    private Date fechaFinContrato;
    @Column(name = "REFERENCIA_CATASTRAL")
    private String referenciaCatastral;
    @Column(name = "FECHA_ADQUISICION_VIVIENDA")
    private Date fechaAdquisicionVivienda;
    private String departamento;

    // bi-directional many-to-one association to Localidad
    @ManyToOne
    private Localidad localidad;
    // bi-directional many-to-one association to Provincia
    @ManyToOne
    private Provincia provincia;
    // bi-directional many-to-one association to Pais
    @ManyToOne
    private Pais pais;
    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;
    // bi-directional many-to-one association to TipoDomicilio
    @ManyToOne
    @JoinColumn(name = "TIPO_DOMICILIO_ID")
    private TipoDomicilio tipoDomicilio;
    // bi-directional many-to-one association to TipoPropiedad
    @ManyToOne
    @JoinColumn(name = "PROPIEDAD_DE_ID")
    private TipoDomicilio propiedadDeId;
    // bi-directional many-to-one association to TipoResidencia
    @ManyToOne
    @JoinColumn(name = "TIPO_RESIDENCIA_ID")
    private TipoResidencia tipoResidencia;
    // bi-directional many-to-one association to TipoVia
    @ManyToOne
    @JoinColumn(name = "TIPO_VIA_ID")
    private TipoVia tipoVia;
    // bi-directional many-to-one association to TipoIdentificacion
    @ManyToOne
    @JoinColumn(name = "ARRENDADOR_TIPO_IDENT_ID")
    private TipoIdentificacion arrendadorTipoIdentificacion;
    // bi-directional many-to-one association to Arrendatario
    @OneToMany(mappedBy = "domicilio", cascade = CascadeType.ALL)
    private Set<Arrendatario> arrendatarios;

    public Domicilio()
    {
        this.setResidenciaEsp(false);
        this.setGratuito(false);
        this.setCorrespondencia(false);
    }

    public void setCorrespondencia(Boolean correspondencia)
    {
        this.correspondencia = correspondencia;
    }

    public void setGratuito(Boolean gratuito)
    {
        this.gratuito = gratuito;
    }

    public void setResidenciaEsp(Boolean residenciaEsp)
    {
        this.residenciaEsp = residenciaEsp;
    }

    public static List<Domicilio> getDomiciliosByBecaId(Long becaId)
    {
        return domicilioDAO.getDomiciliosByBecaId(becaId);
    }

    public static Domicilio getDomicilioGVA(Long becaId) throws DomicilioNoEncontradoException
    {
        Domicilio domicilioGVA = getDomicilioFamiliar(becaId);
        if (domicilioGVA == null)
        {
            domicilioGVA = getDomicilioResidencia(becaId);
        }
        if (domicilioGVA == null) {
            throw new DomicilioNoEncontradoException("la beca " + becaId + " no tiene domicilios");
        }
        return domicilioGVA;
    }

    public static Domicilio getDomicilioResidencia(Long becaId)
    {
        return domicilioDAO.getDomicilioResidencia(becaId);
    }

    public static Domicilio getDomicilioFamiliar(Long becaId)
    {
        return domicilioDAO.getDomicilioFamiliar(becaId);
    }

    @Autowired
    public void setDomicilioDAO(DomicilioDAO domicilioDAO)
    {
        Domicilio.domicilioDAO = domicilioDAO;
    }

    public String getCodigoMunicipio()
    {
        return this.codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio)
    {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getCodigoPostal()
    {
        return this.codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }

    public Boolean isCorrespondencia()
    {
        return this.correspondencia;
    }

    public String getEscalera()
    {
        return this.escalera;
    }

    public void setEscalera(String escalera)
    {
        this.escalera = escalera;
    }

    public Boolean isGratuito()
    {
        return this.gratuito;
    }

    public String getLocalidadNombre()
    {
        return this.localidadNombre;
    }

    public void setLocalidadNombre(String localidadNombre)
    {
        this.localidadNombre = localidadNombre;
    }

    public String getNombreVia()
    {
        return this.nombreVia;
    }

    public void setNombreVia(String nombreVia)
    {
        this.nombreVia = nombreVia;
    }

    public String getNumero()
    {
        return this.numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public String getOtraClaseDomicilio()
    {
        return this.otraClaseDomicilio;
    }

    public void setOtraClaseDomicilio(String otraClaseDomicilio)
    {
        this.otraClaseDomicilio = otraClaseDomicilio;
    }

    public String getPiso()
    {
        return this.piso;
    }

    public void setPiso(String piso)
    {
        this.piso = piso;
    }

    public String getPuerta()
    {
        return this.puerta;
    }

    public void setPuerta(String puerta)
    {
        this.puerta = puerta;
    }

    public Boolean isResidenciaEsp()
    {
        return this.residenciaEsp;
    }

    public Float getImporteAlquiler()
    {
        return importeAlquiler;
    }

    public void setImporteAlquiler(Float importeAlquiler)
    {
        this.importeAlquiler = importeAlquiler;
    }

    public Beca getBeca()
    {
        return this.beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    public TipoDomicilio getTipoDomicilio()
    {
        return this.tipoDomicilio;
    }

    public void setTipoDomicilio(TipoDomicilio tipoDomicilio)
    {
        this.tipoDomicilio = tipoDomicilio;
    }

    public TipoDomicilio getPropiedadDeId()
    {
        return this.propiedadDeId;
    }

    public void setPropiedadDeId(TipoDomicilio tipoDomicilio)
    {
        this.propiedadDeId = propiedadDeId;
    }

    public TipoResidencia getTipoResidencia()
    {
        return tipoResidencia;
    }

    public void setTipoResidencia(TipoResidencia tipoResidencia)
    {
        this.tipoResidencia = tipoResidencia;
    }

    public TipoVia getTipoVia()
    {
        return this.tipoVia;
    }

    public void setTipoVia(TipoVia tipoVia)
    {
        this.tipoVia = tipoVia;
    }

    public String getReferenciaCatastral()
    {
        return referenciaCatastral;
    }

    public void setReferenciaCatastral(String referenciaCatastral)
    {
        this.referenciaCatastral = referenciaCatastral;
    }

    public Date getFechaAdquisicionVivienda()
    {
        return fechaAdquisicionVivienda;
    }

    public void setFechaAdquisicionVivienda(Date fechaAdquisicionVivienda)
    {
        this.fechaAdquisicionVivienda = fechaAdquisicionVivienda;
    }

    public String getDepartamento()
    {
        return this.departamento;
    }

    public void setDepartamento(String departamento)
    {
        this.departamento = departamento;
    }

    @Transactional
    public Domicilio update()
    {
        return domicilioDAO.update(this);
    }

    @Transactional
    public Domicilio insert()
    {
        return domicilioDAO.insert(this);
    }

    @Transactional
    public void delete()
    {
        domicilioDAO.borraDomicilio(getId());
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void importarDatosMinisterio(DomicilioType domicilioType)
    {
        setCodigoPostal(domicilioType.getDomiCodPostal());
        setNombreVia(domicilioType.getDomiDomicilio());
        setEscalera(domicilioType.getDomiEscalera());
        setNumero(domicilioType.getDomiNumero());
        setPiso(domicilioType.getDomiPiso());
        setGratuito(domicilioType.getDomiIndGratuito());
        setResidenciaEsp(domicilioType.getDomiIndEspana());
        setCorrespondencia(domicilioType.getDomiCorrespondencia());
        setImporteAlquiler(domicilioType.getDomiImporteMensual());
        setArrendadorNombre(domicilioType.getDomiArrendador());
        setArrendadorIdentificacion(domicilioType.getDomiNif());
        setDepartamento(domicilioType.getDomiDepartamento());
        setNumeroArrendatarios(domicilioType.getDomiNumArrendatarios());
        setImporteFianza(domicilioType.getDomiImporteFianza());
        if (domicilioType.getDomiFechaIniContrato() != null)
        {
            setFechaInicioContrato(domicilioType.getDomiFechaIniContrato().toGregorianCalendar().getTime());
        }
        if (domicilioType.getDomiFechaFinContrato() != null)
        {
            setFechaFinContrato(domicilioType.getDomiFechaFinContrato().toGregorianCalendar().getTime());

        }
        setReferenciaCatastral(domicilioType.getDomiRefCatastral());

        if (domicilioType.getDomiFechaAdqVivienda() != null)
        {
            setFechaAdquisicionVivienda(domicilioType.getDomiFechaAdqVivienda().toGregorianCalendar().getTime());
        }

        if (domicilioType.getDomiCprov() != null)
        {
            setProvincia(Provincia.getProvinciaById(Long.parseLong(domicilioType.getDomiCprov())));
        }

        if (domicilioType.getDomiPais() != null)
        {
            setPais(Pais.getPaisById(domicilioType.getDomiPais()));
        }

        setPuerta(domicilioType.getDomiLetra());
        setOtraClaseDomicilio(domicilioType.getDomiOtroDomicilio());

        importarTipoDomicilio(domicilioType);
        importarCodigoResidencia(domicilioType);
        importarTipoVia(domicilioType);
        importarCodigoLocalidad(domicilioType);
        importarTipoIdentificacion(domicilioType);
    }

    private void setCorrespondencia(String domiCorrespondencia)
    {
        if (domiCorrespondencia != null)
        {
            setCorrespondencia(domiCorrespondencia.equals("S"));
        }
    }

    private void setGratuito(String domiIndGratuito)
    {
        if (domiIndGratuito != null)
        {
            setGratuito(domiIndGratuito.equals("S"));
        }
    }

    private void setResidenciaEsp(String domiIndEspana)
    {
        if (domiIndEspana != null)
        {
            setResidenciaEsp(domiIndEspana.equals("S"));
        }
    }

    private void importarTipoIdentificacion(DomicilioType domicilioType)
    {
        Long tipoIdentificacionMinisterio = domicilioType.getDomiCodDocuIdent();
        if (tipoIdentificacionMinisterio != null)
        {
            TipoIdentificacion tipoIdentificacionUji = TipoIdentificacion
                    .getTipoIdentificacionById(tipoIdentificacionMinisterio);
            setArrendadorTipoIdentificacion(tipoIdentificacionUji);
        }
    }

    private void importarTipoDomicilio(DomicilioType domicilioType)
    {
        String tipoDomicilioMinisterio = domicilioType.getDomiCodDomicilio();
        if (tipoDomicilioMinisterio != null)
        {
            MapeadorTipoDomicilio mapeadorTipoDomicilio = new MapeadorTipoDomicilio();
            Long tipoDomicilioIdUji = mapeadorTipoDomicilio.getValorUJI(tipoDomicilioMinisterio,
                    CodigoOrganismo.MINISTERIO);
            if (tipoDomicilioIdUji != null)
            {
                TipoDomicilio tipoDomicilioUji = new TipoDomicilio();
                tipoDomicilioUji.setId(tipoDomicilioIdUji);
                setTipoDomicilio(tipoDomicilioUji);
            }
        }
    }

    private void importarCodigoResidencia(DomicilioType domicilioType)
    {
        Long tipoResidenciaMinisterio = domicilioType.getDomiCodResidencia();
        if (tipoResidenciaMinisterio != null)
        {
            MapeadorTipoResidencia mapeadorTipoResidencia = new MapeadorTipoResidencia();
            Long tipoResidenciaIdUji = mapeadorTipoResidencia.getValorUJI(tipoResidenciaMinisterio,
                    CodigoOrganismo.MINISTERIO);
            if (tipoResidenciaIdUji != null)
            {
                TipoResidencia tipoResidenciaUji = new TipoResidencia();
                tipoResidenciaUji.setId(tipoResidenciaIdUji);
                setTipoResidencia(tipoResidenciaUji);
            }
        }
    }

    private void importarTipoVia(DomicilioType domicilioType)
    {
        String tipoViaMinisterio = domicilioType.getDomiCodVia();
        if (tipoViaMinisterio != null)
        {
            MapeadorTipoVia mapeadorTipoVia = new MapeadorTipoVia();
            Long tipoViaIdUji = mapeadorTipoVia.getValorUJI(Long.parseLong(tipoViaMinisterio),
                    CodigoOrganismo.MINISTERIO);
            if (tipoViaIdUji != null)
            {
                TipoVia tipoViaUji = new TipoVia();
                tipoViaUji.setId(tipoViaIdUji);
                setTipoVia(tipoViaUji);
            }
        }
    }

    private void importarCodigoLocalidad(DomicilioType domicilioType)
    {
        String codigoLocalidadMinisterio = domicilioType.getDomiCloc();
        if (codigoLocalidadMinisterio != null)
        {
            Localidad localidadUji = Localidad.getLocalidadByCodigo(codigoLocalidadMinisterio);
            setLocalidad(localidadUji);
        }
    }

    public void importarRelacionArrendatarios(DomicilioType domicilioType)
    {
        if (domicilioType != null)
        {
            List<ArrendatariosType> listaArrendatarios = domicilioType.getArrendatarios();
            borraArrendatariosExistentes(getArrendatarios());
            Set<Arrendatario> listaArrendatariosConvertidos = convierteListaArrendatarios(listaArrendatarios);
            this.setArrendatarios(listaArrendatariosConvertidos);
        }
    }

    @Transactional
    private void borraArrendatariosExistentes(Set<Arrendatario> listaArrendatarios)
    {
        this.setArrendatarios(null);
        for (Arrendatario arrendatario : listaArrendatarios)
        {
            Arrendatario.delete(arrendatario.getId());
        }
    }

    private Set<Arrendatario> convierteListaArrendatarios(List<ArrendatariosType> listaArrendatarios)
    {
        Set<Arrendatario> listaArrendatariosConvertidos = new HashSet<Arrendatario>();
        for (ArrendatariosType arrendatariosType : listaArrendatarios)
        {
            Arrendatario arrendatario = new Arrendatario();
            arrendatario.setDomicilio(this);
            arrendatario.importarDatosMinisterio(arrendatariosType);

            listaArrendatariosConvertidos.add(arrendatario);
            arrendatario.insert();
        }

        return listaArrendatariosConvertidos;
    }

    public Set<Arrendatario> getArrendatarios()
    {
        if (this.arrendatarios == null || this.arrendatarios.isEmpty())
        {
            return new HashSet<Arrendatario>();
        }
        return this.arrendatarios;
    }

    public void setArrendatarios(Set<Arrendatario> arrendatarios)
    {
        this.arrendatarios = arrendatarios;
    }

    public String getArrendadorIdentificacion()
    {
        return arrendadorIdentificacion;
    }

    public void setArrendadorIdentificacion(String arrendadorIdentificacion)
    {
        this.arrendadorIdentificacion = arrendadorIdentificacion;
    }

    public String getArrendadorNombre()
    {
        return arrendadorNombre;
    }

    public void setArrendadorNombre(String arrendadorNombre)
    {
        this.arrendadorNombre = arrendadorNombre;
    }

    public TipoIdentificacion getArrendadorTipoIdentificacion()
    {
        return arrendadorTipoIdentificacion;
    }

    public void setArrendadorTipoIdentificacion(TipoIdentificacion arrendadorTipoIdentificacion)
    {
        this.arrendadorTipoIdentificacion = arrendadorTipoIdentificacion;
    }

    public Long getNumeroArrendatarios()
    {
        return numeroArrendatarios;
    }

    public void setNumeroArrendatarios(Long numeroArrendatarios)
    {
        this.numeroArrendatarios = numeroArrendatarios;
    }

    public Float getImporteFianza()
    {
        return importeFianza;
    }

    public void setImporteFianza(Float importeFianza)
    {
        this.importeFianza = importeFianza;
    }

    public Date getFechaInicioContrato()
    {
        return fechaInicioContrato;
    }

    public void setFechaInicioContrato(Date fechaInicioContrato)
    {
        this.fechaInicioContrato = fechaInicioContrato;
    }

    public Date getFechaFinContrato()
    {
        return fechaFinContrato;
    }

    public void setFechaFinContrato(Date fechaFinContrato)
    {
        this.fechaFinContrato = fechaFinContrato;
    }

    public void updateDomicilio()
    {
        domicilioDAO.updateDomicilio(this);
    }

    @Transactional
    public void borraArrendatarios()
    {
        List<Arrendatario> listaArrendatarios = Arrendatario.getArrendatariosByDomicilio(getId());
        for (Arrendatario arrendatario : listaArrendatarios)
        {
            Arrendatario.delete(arrendatario.getId());
        }
        this.setArrendatarios(null);
    }

    public boolean isDistanciaDomicilioMenor(Float distanciaMaxima)
            throws DistanciaDomicilioFamiliarNoEncontradaException
    {
        Provincia provincia = this.getProvincia();

        Localidad localidad = this.getLocalidad();
        Float distancia = localidad.getDistancia();

        if (provincia.getId().equals(CodigoProvincia.CASTELLON.getId()) && distancia == null)
        {
            throw new DistanciaDomicilioFamiliarNoEncontradaException();
        }
        if (distancia == null)
        {
            return (false);
        }
        return (distancia < distanciaMaxima);

    }

    public Localidad getLocalidad()
    {
        return this.localidad;
    }

    public void setLocalidad(Localidad localidad)
    {
        this.localidad = localidad;
    }

    public Provincia getProvincia()
    {
        return this.provincia;
    }

    public void setProvincia(Provincia provincia)
    {
        this.provincia = provincia;
    }

    public Pais getPais()
    {
        return this.pais;
    }

    public void setPais(Pais pais)
    {
        this.pais = pais;
    }

    public boolean isPeninsular()
    {
        if (this.isEnCanarias())
        {
            return false;
        }

        if (this.isEnBaleares())
        {
            return false;
        }

        if (this.isEnCeutaMelilla())
        {
            return false;
        }

        return true;
    }

    public boolean isEnCeutaMelilla()
    {
        if (provincia.getId().equals(CodigoProvincia.CEUTA.getId()) || provincia.getId().equals(CodigoProvincia.MELILLA.getId()))
        {
            return true;
        }
        return false;
    }

    public boolean isEnCanarias()
    {
        if (provincia.getId().equals(CodigoProvincia.LASPALMAS.getId()) || provincia.getId().equals(CodigoProvincia.SANTACRUZ.getId()))
        {
            return true;
        }
        return false;
    }

    public boolean isEnBaleares()
    {
        if (provincia.getId().equals(CodigoProvincia.BALEARES.getId()))
        {
            return true;
        }
        return false;
    }

    public boolean isInsular()
    {
        if (this.isEnCanarias())
        {
            return true;
        }

        if (this.isEnBaleares())
        {
            return true;
        }
        return false;
    }
}
