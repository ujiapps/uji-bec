package es.uji.apps.bec.model.domains;

public enum EstadoLoteMinisterio
{
    CARREGAT, PROCESSAT, ERRONI;
}
