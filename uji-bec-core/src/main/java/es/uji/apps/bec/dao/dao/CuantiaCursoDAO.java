package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.CuantiaCurso;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface CuantiaCursoDAO extends BaseDAO
{
    List<CuantiaCurso> getCuantiasCurso();

    List<CuantiaCurso> getCuantiasCursoByCursoAcademicoyConvocatoria(Long cursoAcademicoId, Long convocatoriaId);

    CuantiaCurso getCuantiaCursoByCursoAcademicoAndCuantia(Long cursoAcademicoId, Long cuantiaId);

    CuantiaCurso getCuantiaCursoById(Long cuantiaCursoId);

    void deleteCuantiaCurso(Long cuantiaCursoId);

}
