package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.Convocatoria;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.util.List;

public interface ConvocatoriaDAO extends BaseDAO
{
    public List<Convocatoria> getConvocatoriasActivas();

    public Convocatoria getConvocatoriaById(Long convocatoriaId);

    public Convocatoria getConvocatoriaByAcronimo(String acronimo);

    public List<Convocatoria> getConvocatoriasActivasPorOrganismoId(Long organismoId);

    void deleteConvocatoria(Long convocatoriaId);
}
