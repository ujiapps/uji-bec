package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.model.PersonaEstudio;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface PersonaEstudioDAO extends BaseDAO
{
    PersonaEstudio getAcademicosbyPersonaEstudioYCursoAca(Long personaId, Long estudioId,
            Long cursoAcamedicoId);

    Boolean existePersonaEstudio(Long personaId, Long estudioId, Long cursoAcademicoId);

    List<PersonaEstudio> getAcademicosByPersonaYCursoAca(Long personaId, Long cursoAcademicoId);
    
    Long getCursoAcademicoAnteriorUJI(Long personaId, Long cursoAcademicoId);

    PersonaEstudio getPersonaEstudioByBecaId(Long becaId) throws BecaSinEstudioException;
}
