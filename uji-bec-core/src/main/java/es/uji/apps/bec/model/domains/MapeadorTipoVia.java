package es.uji.apps.bec.model.domains;

import es.uji.apps.bec.util.Mapeador;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class MapeadorTipoVia extends Mapeador<Long, Object>
{
    public MapeadorTipoVia()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        for (int i = 1; i < 10; i++)
        {
            Long valor = new Long(i);
            valores.put(CodigoOrganismo.MINISTERIO, valor);
            valores.put(CodigoOrganismo.CONSELLERIA, MessageFormat.format("0{0}", valor));

            add(valor, valores);
        }

        valores.put(CodigoOrganismo.MINISTERIO, 99L);
        valores.put(CodigoOrganismo.CONSELLERIA, "09");

        add(new Long(99L), valores);
    }
}
