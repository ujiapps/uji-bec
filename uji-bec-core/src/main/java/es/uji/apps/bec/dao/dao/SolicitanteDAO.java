package es.uji.apps.bec.dao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Solicitante;
import es.uji.commons.db.BaseDAO;

public interface SolicitanteDAO extends BaseDAO
{
    List<Beca> getSolicitanteConFiltros(Map<String, String> filterParams);

    Long getNumeroTotalBecasPorCursoAcademico(Long cursoAcademicoId);

    List<Solicitante> getDatosGenerales(Long becaId);

    Solicitante getSolicitanteById(Long solicitanteId);

    Solicitante getSolicitanteByCursoAcademicoAndIdentificacion(String cursoAcademicoId,
                                                                String identificacion);

    Solicitante getSolicitanteByCursoAcademicoAndPersonaId(String cursoAcademicoId, Long personaId);

    List<Solicitante> getSolicitantes(HashMap<String, String> filterParams);

    Solicitante getSolicitanteAndBeca(Long solicitanteId, Long becaId);

    void updateOtrosDatos(Solicitante solicitante);

    void updateDatosGenerales(Solicitante solicitante);

    List<Beca> getBecasByOrganismoAndSolicitante(Long solicitanteId, Long organismoId);

    Solicitante getSolicitanteConBecasById(Long solicitanteId);

    Solicitante updateSolicitante(Solicitante solicitante);
}