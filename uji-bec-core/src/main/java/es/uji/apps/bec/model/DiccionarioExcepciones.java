package es.uji.apps.bec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.DiccionarioExcepcionesDAO;

/**
 * The persistent class for the BC2_DICCIONARIO_EXCEPCIONES database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_DICCIONARIO_EXCEPCIONES")
public class DiccionarioExcepciones implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CLASE_UJI")
    private String claseUji;

    @Column(name = "VALOR_ORIGEN")
    private String valorOrigen;

    @Column(name = "VALOR_UJI")
    private Long valorUji;

    // bi-directional many-to-one association to CursoAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;

    // bi-directional many-to-one association to Organismo
    @ManyToOne
    private Organismo organismo;

    private static DiccionarioExcepcionesDAO diccionarioExcepcionesDAO;

    @Autowired
    public void setDiccionarioExcepcionesDAO(DiccionarioExcepcionesDAO diccionarioExcepcionesDAO)
    {
        DiccionarioExcepciones.diccionarioExcepcionesDAO = diccionarioExcepcionesDAO;
    }

    public DiccionarioExcepciones()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getClaseUji()
    {
        return this.claseUji;
    }

    public void setClaseUji(String claseUji)
    {
        this.claseUji = claseUji;
    }

    public String getValorOrigen()
    {
        return this.valorOrigen;
    }

    public void setValorOrigen(String valorOrigen)
    {
        this.valorOrigen = valorOrigen;
    }

    public Long getValorUji()
    {
        return this.valorUji;
    }

    public void setValorUji(Long valorUji)
    {
        this.valorUji = valorUji;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public void setOrganismo(Organismo Organismo)
    {
        this.organismo = Organismo;
    }

    public static Long getValor(Long cursoAcademicoId, String claseUji, Long organismoId,
            String valorOrigen)
    {
        return diccionarioExcepcionesDAO.getValor(cursoAcademicoId, claseUji, organismoId, valorOrigen);
    }

    public static String getValorOrigen(Long cursoAcademicoId, String claseUji, Long organismoId,
            String valorUji)
    {
        return diccionarioExcepcionesDAO.getValorOrigen(cursoAcademicoId, claseUji, organismoId, valorUji);
    }

}