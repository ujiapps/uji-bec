package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoPropiedadDAO;

/**
 * The persistent class for the BC2_TIPOS_DOMICILIOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TIPOS_PROPIEDAD")
public class TipoPropiedad implements Serializable
{
    @Id
    private Long id;
    private String nombre;
    private Long orden;

    // bi-directional many-to-one association to Propiedad
    @OneToMany(mappedBy = "propiedadDeId")
    private Set<Domicilio> domicilios;

    private static TipoPropiedadDAO tipoPropiedadDAO;

    @Autowired
    public void setTipoPropiedadDAO(TipoPropiedadDAO tipoPropiedadDAO)
    {
        TipoPropiedad.tipoPropiedadDAO = tipoPropiedadDAO;
    }

    public TipoPropiedad()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }
}