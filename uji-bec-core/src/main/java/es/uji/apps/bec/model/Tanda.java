package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.dao.dao.TandaDAO;
import es.uji.apps.bec.dao.dao.TodasConselleriaDAO;
import es.uji.apps.bec.exceptions.BecaMec2CrdException;
import es.uji.apps.bec.exceptions.NotificacionesException;
import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import es.uji.apps.bec.model.domains.CodigoEstado;

import org.apache.xpath.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the BC2_TANDAS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TANDAS")
public class Tanda implements Serializable
{
    public static final Long BECA_ESTADO_TRABAJADA = 2L;

    private static TandaDAO tandaDAO;
    private static TodasConselleriaDAO todasConselleriaDAO;
    private static BecaDAO becaDAO;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Boolean activa;
    private String descripcion;
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "TANDA_ID")
    private Long tandaId;
    private String usuario;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_NOTIFICACION")
    private Date fechaNotificacion;
    // bi-directional many-to-one association to Beca
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REUNION_JURADO")
    private Date fechaReunionJurado;

    @OneToMany(mappedBy = "tanda")
    private Set<Beca> becas;
    // bi-directional many-to-one association to Convocatoria
    @ManyToOne
    private Convocatoria convocatoria;
    // bi-directional many-to-one association to CursoAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;
    // bi-directional many-to-one association to RegistroEnvio
    @OneToMany(mappedBy = "tanda")
    private Set<RegistroEnvio> registroEnvios;
    // bi-directional many-to-one association to RegistroErroresEnvio
    @OneToMany(mappedBy = "tanda")
    private Set<RegistroErroresEnvio> registroErroresEnvios;

    public Tanda()
    {
        this.setActiva(false);
    }

    public static Tanda getTandaById(Long tandaId)
    {
        if (tandaId == null)
        {
            return null;
        }

        List<Tanda> listaTandas = tandaDAO.get(Tanda.class, tandaId);
        if (listaTandas.size() > 0)
        {
            return listaTandas.get(0);
        }
        return null;
    }

    public static List<Tanda> getTandasByTandaIdAndOrganismoIdAndCursoAcademicoId(Long tandaId,
            Long organismoId, Long cursoAcademicoId)
    {
        return tandaDAO.getTandasByTandaIdAndOrganismoIdAndCursoAcademicoId(tandaId, organismoId,
                cursoAcademicoId);
    }

    public static List<Estado> getEstadosBecas(Long tandaId)
    {
        return tandaDAO.getEstadosBecas(tandaId);
    }

    public static List<Proceso> getProcesosBecas(Long tandaId)
    {
        return tandaDAO.getProcesosBecas(tandaId);
    }

    public static Long getMaximoTandaIdByCursoAca(CursoAcademico cursoAca)
    {
        return tandaDAO.getMaximoTandaIdByCursoAca(cursoAca.getId());
    }

    @Autowired
    public void setTandaDAO(TandaDAO tandaDAO)
    {
        Tanda.tandaDAO = tandaDAO;
    }

    @Autowired
    public void setTodasConselleriaDAO(TodasConselleriaDAO todasConselleriaDAO) {
        Tanda.todasConselleriaDAO = todasConselleriaDAO;
    }

    @Autowired
    public void setBecaDAO(BecaDAO becaDAO) {
        Tanda.becaDAO = becaDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean isActiva()
    {
        return this.activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaNotificacion()
    {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion)
    {
        this.fechaNotificacion = fechaNotificacion;
    }

    public Date getFechaReunionJurado()
    {
        return this.fechaReunionJurado;
    }

    public void setFechaReunionJurado(Date fechaReunionJurado)
    {
        this.fechaReunionJurado = fechaReunionJurado;
    }

    public Long getTandaId()
    {
        return this.tandaId;
    }

    public void setTandaId(Long tandaId)
    {
        this.tandaId = tandaId;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public Convocatoria getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Set<RegistroEnvio> getRegistroEnvios()
    {
        return this.registroEnvios;
    }

    public void setRegistroEnvios(Set<RegistroEnvio> registroEnvios)
    {
        this.registroEnvios = registroEnvios;
    }

    public Set<RegistroErroresEnvio> getRegistroErroresEnvios()
    {
        return this.registroErroresEnvios;
    }

    public void setRegistroErroresEnvios(Set<RegistroErroresEnvio> registroErroresEnvios)
    {
        this.registroErroresEnvios = registroErroresEnvios;
    }

    @Transactional
    public Tanda insert()
    {
        return tandaDAO.insert(this);
    }

    public void enviaNotificacion(String usuario) throws NotificacionesException
    {
        if (!Beca.isBecasPorTanda(id))
        {
            throw new NotificacionesException("No hi ha beques en aquesta tanda.");
        }

        if (Beca.isBecasPorTandaDeDistintoTipo(id))
        {
            throw new NotificacionesException(
                    "Per a poder enviar una notificació, totes les beques de la mateixa tanda "
                            + "han de tenir el mateix estat, procés i convocatòria.");
        }

        if (Beca.isBecasPorTandaEnEstadoNoDenegado(id))
        {
            throw new NotificacionesException(
                    "Per a poder enviar una notificació totes, les beques de la mateixa tanda "
                            + "han de tenir el estat denegat o denegat acadèmics.");
        }

        this.activa = false;
        this.fechaNotificacion = new Date();
        this.usuario = usuario;

        this.updateTanda();

    }

    public void updateTanda()
    {
        tandaDAO.updateTanda(this);
    }

    public Boolean isDenegadaAcademicos()
    {
        List<Estado> listaEstados = Tanda.getEstadosBecas(id);
        if (listaEstados.get(0).getId() == CodigoEstado.DENEGADA_ACADEMICOS.getId()
                || listaEstados.get(0).getId() == CodigoEstado.BAJA.getId())
        {
            return true;
        }
        return false;
    }

    public Boolean isTrabajada() throws WsErrorAlPrepararMultienvioException
    {
        List<Estado> listaEstados = Tanda.getEstadosBecas(id);

        if (listaEstados.size() > 1)
        {
            return false;
        }

        if (listaEstados.get(0).getId() == CodigoEstado.TRABAJADA.getId())
        {
            return true;
        }
        return false;
    }

    public Boolean isAcademicosCalculados() throws WsErrorAlPrepararMultienvioException
    {
        List<Estado> listaEstados = Tanda.getEstadosBecas(id);

        if (listaEstados.size() > 1)
        {
            return false;
        }

        if (listaEstados.get(0).getId() == CodigoEstado.ACADEMICOS_CALCULADOS.getId())
        {
            return true;
        }
        return false;
    }

    public Boolean isCorrectaParaEnviar() throws WsErrorAlPrepararMultienvioException
    {
        List<Estado> listaEstados = Tanda.getEstadosBecas(id);

        if (listaEstados.isEmpty())
        {
            throw new WsErrorAlPrepararMultienvioException("La tanda no te cap beca");
        }

        if (listaEstados.size() > 1)
        {
            throw new WsErrorAlPrepararMultienvioException("La tanda te beques en diferents estats");
        }

        List<Proceso> listaProcesos = Tanda.getProcesosBecas(id);

        if (listaProcesos.size() > 1)
        {
            throw new WsErrorAlPrepararMultienvioException(
                    "La tanda te beques en diferents processos");
        }

        Tanda tanda = Tanda.getTandaById(id);

        if (tanda.getFecha() != null)
        {
            throw new WsErrorAlPrepararMultienvioException("La tanda ja ha sigut enviada");
        }

        return true;
    }

    public Boolean isTandaDuplicadaParaOrganismo()
    {
        List<Tanda> listaTandas = Tanda.getTandasByTandaIdAndOrganismoIdAndCursoAcademicoId(this
                .getTandaId(), this.getConvocatoria().getOrganismo().getId(), this.getCursoAcademico()
                .getId());

        if (listaTandas.isEmpty())
        {
            return false;
        }

        return true;
    }

    public void compruebaIsMec2Crd() throws BecaMec2CrdException {

        List<Beca> becas = becaDAO.getBecasByTandaId(this.getId());
        for (Beca beca : becas) {
            if (!todasConselleriaDAO.isMec2Crd(beca.getId(), this.getCursoAcademico().getId())) {
                throw new BecaMec2CrdException("La beca " + beca.getId() + " no cumpleix les condicions Mec2Crd");
            }
        }
    }

    public String compruebaTipoEnvioConselleria()
    {
        Boolean esUnaBecaDelMecConCreditosSegunda = false;
        Boolean esUnaBecaNoMec = false;

        List<Beca> becas = becaDAO.getBecasByTandaId(this.getId());
        for (Beca beca : becas)
        {
            if (todasConselleriaDAO.isMec2Crd(beca.getId(), this.getCursoAcademico().getId()))
            {
                esUnaBecaDelMecConCreditosSegunda = true;
            }
            else
            {
                esUnaBecaNoMec = true;
            }

            if (esUnaBecaDelMecConCreditosSegunda && esUnaBecaNoMec)
            {
                return "ERROR";
            }
        }

        if (esUnaBecaDelMecConCreditosSegunda)
        {
            return "MEC";
        }
        else
        {
            return "NO_MEC";
        }
    }

    public void reiniciaFechaYEstadosBeca(String usuario) throws NotificacionesException
    {
        if (!Beca.isBecasPorTanda(this.id))
        {
            throw new NotificacionesException("No hi ha beques en aquesta tanda.");
        }

        if (Beca.isBecasPorTandaEnEstadoNoEnviado(this.id))
        {
            throw new NotificacionesException("Totes les beques han d'estar en estat enviat.");
        }

        cambiaBecasAEstadoTrabajada();

        this.fecha = null;
        this.usuario = usuario;

        this.updateTanda();
    }

    private void cambiaBecasAEstadoTrabajada()
    {
        List<Beca> becas = becaDAO.getBecasByTandaId(this.getId());
        for (Beca beca : becas) {
            beca.cambiaEstado(BECA_ESTADO_TRABAJADA);
        }
    }
}
