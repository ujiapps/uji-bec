package es.uji.apps.bec.model;

import org.springframework.stereotype.Component;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the BC2_V_BECAS_RECUENTO database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name="BC2_V_BECAS_RECUENTO")
public class BecaRecuento implements Serializable {

    @Id
	@Column(name="BECA_CONCEDIDA")
	private String becaConcedida;

	private String convocatoria;

    @Id
	@Column(name="CONVOCATORIA_ID")
	private Long convocatoriaId;

	private Long cuantos;

    @Id
    @Column(name="CURSO_ACADEMICO_ID")
	private Long cursoAcademicoId;

	private String estado;

    @Id
    @Column(name="ESTADO_ID")
	private Long estadoId;

	private String organismo;

    @Id
    @Column(name="ORGANISMO_ID")
	private Long organismoId;

	private String proceso;

    @Id
    @Column(name="PROCESO_ID")
	private Long procesoId;

	public BecaRecuento() {
	}

	public String getBecaConcedida() {
		return this.becaConcedida;
	}

	public void setBecaConcedida(String becaConcedida) {
		this.becaConcedida = becaConcedida;
	}

	public String getConvocatoria() {
		return this.convocatoria;
	}

	public void setConvocatoria(String convocatoria) {
		this.convocatoria = convocatoria;
	}

	public Long getConvocatoriaId() {
		return this.convocatoriaId;
	}

	public void setConvocatoriaId(Long convocatoriaId) {
		this.convocatoriaId = convocatoriaId;
	}

	public Long getCuantos() {
		return this.cuantos;
	}

	public void setCuantos(Long cuantos) {
		this.cuantos = cuantos;
	}

	public Long getCursoAcademicoId() {
		return this.cursoAcademicoId;
	}

	public void setCursoAcademicoId(Long cursoAcademicoId) {
		this.cursoAcademicoId = cursoAcademicoId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Long getEstadoId() {
		return this.estadoId;
	}

	public void setEstadoId(Long estadoId) {
		this.estadoId = estadoId;
	}

	public String getOrganismo() {
		return this.organismo;
	}

	public void setOrganismo(String organismo) {
		this.organismo = organismo;
	}

	public Long getOrganismoId() {
		return this.organismoId;
	}

	public void setOrganismoId(Long organismoId) {
		this.organismoId = organismoId;
	}

	public String getProceso() {
		return this.proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public Long getProcesoId() {
		return this.procesoId;
	}

	public void setProcesoId(Long procesoId) {
		this.procesoId = procesoId;
	}

}
