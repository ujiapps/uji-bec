package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.HistoricoBecaDAO;

/**
 * The persistent class for the BC2_BECAS_HISTORICO database table.
 */

@Component
@Entity
@Table(name = "BC2_BECAS_HISTORICO")
public class HistoricoBeca implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ESTADO_ID")
    private Long estadoId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "PROCESO_ID")
    private Long procesoId;

    @Column(name = "TANDA_ID")
    private Long tandaId;

    private String usuario;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_NOTIFICACION")
    private Date fechaNotificacion;

    private Boolean concedida;

    private Boolean reclamada;

    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;

    private static HistoricoBecaDAO historicoBecaDAO;


    @Autowired
    public void setHistoricoBeca(HistoricoBecaDAO historicoBecaDAO)
    {
        HistoricoBeca.historicoBecaDAO = historicoBecaDAO;
    }

    public HistoricoBeca()
    {
        this.setConcedida(false);
        this.setReclamada(false);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(Long estadoId)
    {
        this.estadoId = estadoId;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getProcesoId()
    {
        return this.procesoId;
    }

    public void setProcesoId(Long procesoId)
    {
        this.procesoId = procesoId;
    }

    public Long getTandaId()
    {
        return this.tandaId;
    }

    public void setTandaId(Long tandaId)
    {
        this.tandaId = tandaId;
    }

    public String getUsuario()
    {
        return this.usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public Date getFechaNotificacion()
    {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion)
    {
        this.fechaNotificacion = fechaNotificacion;
    }

    public Boolean isConcedida()
    {
        return this.concedida;
    }

    public void setConcedida(Boolean concedida)
    {
        this.concedida = concedida;
    }

    public Boolean isReclamada()
    {
        return this.reclamada;
    }

    public void setReclamada(Boolean reclamada)
    {
        this.reclamada= reclamada;
    }

    public Beca getBeca()
    {
        return this.beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    @Transactional
    public void insert()
    {
        historicoBecaDAO.insert(this);
    }

    @Transactional
    public static void insertHistoricoSiCambios(Beca beca, Beca becaDataBase, String usuario)
    {
        Long tandaAnteriorId = 9999L;
        Long tandaActualId = 9999L;

        if (becaDataBase != null && becaDataBase.getTanda() != null)
        {
            tandaAnteriorId = becaDataBase.getTanda().getId();
        }

        if (beca.getTanda() != null)
        {
            tandaActualId = beca.getTanda().getId();
        }

        if ((becaDataBase == null)
                || (beca.getConvocatoria().getId() != becaDataBase.getConvocatoria().getId()
                || beca.getProceso().getId() != becaDataBase.getProceso().getId()
                || beca.getEstado().getId() != becaDataBase.getEstado().getId()
                || tandaActualId.longValue() != tandaAnteriorId.longValue()))
        {
            HistoricoBeca historicoBeca = new HistoricoBeca();
            historicoBeca.setBeca(beca);
            historicoBeca.setConvocatoriaId(beca.getConvocatoria().getId());
            historicoBeca.setEstadoId(beca.getEstado().getId());
            historicoBeca.setConcedida(beca.isBecaConcedida());
            historicoBeca.setReclamada(beca.isReclamada());
            historicoBeca.setProcesoId(beca.getProceso().getId());
            if (beca.getTanda() != null)
            {
                historicoBeca.setTandaId(beca.getTanda().getId());
            }
            historicoBeca.setFecha(new Date());
            historicoBeca.setUsuario(usuario);
            historicoBeca.insert();
        }
    }
}