package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class WsFaltaCodigoDeArchivoTemporalException extends CoreDataBaseException
{
    public WsFaltaCodigoDeArchivoTemporalException()
    {
        super("No s\'ha indicat el codi d\'arxiu temporal del MEC o es incorrecte.");
    }

    public WsFaltaCodigoDeArchivoTemporalException(String message)
    {
        super(message);
    }
}
