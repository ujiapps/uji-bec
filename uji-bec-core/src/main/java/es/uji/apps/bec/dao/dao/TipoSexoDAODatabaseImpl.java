package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoSexo;
import es.uji.apps.bec.model.TipoSexo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoSexoDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoSexoDAO
{
    private QTipoSexo tipoSexo = QTipoSexo.tipoSexo;

    @Override
    public List<TipoSexo> getTiposSexos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoSexo).orderBy(tipoSexo.orden.asc());
        return query.list(tipoSexo);
    }

    @Override
    public List<TipoSexo> getTipoSexoById(Long tipoSexoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoSexo).where(tipoSexo.id.eq(tipoSexoId));
        return query.list(tipoSexo);
    }
}
