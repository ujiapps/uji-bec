package es.uji.apps.bec.model.domains;

public enum CodigoCuantia
{
    TASAS_GENERAL(15L),
    
    TASAS_GENERAL_RESIDENCIA(16L),
    
    TASAS_CONSELLERIA(32L),

    RESIDENCIA(34L),

    INSULAR(42L),

    CEUTA_MELILLA(41L),

    GVA_FINALIZACION(48L),

    GVA_SALARIO(47L),

    GVA_MANUELA(59L);

    private Long id;

    CodigoCuantia(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
}
