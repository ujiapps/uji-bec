package es.uji.apps.bec.model.domains;

public enum CodigoOrganismo
{
    MINISTERIO(1L), CONSELLERIA(2L), GVA_ALTRES(3L);
    private Long id;

    CodigoOrganismo(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public boolean igual(Object otherObject)
    {
        if (otherObject instanceof Long)
        {
            return ((Long) otherObject) == getId();
        }

        return super.equals(otherObject);
    }
}
