package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Denegacion;
import es.uji.commons.db.BaseDAO;

public interface DenegacionDAO extends BaseDAO
{
    List<Denegacion> getDenegaciones();

    List<Denegacion> getDenegacionesActivas();

    Denegacion getDenegacionById(Long denegacionId);

    Denegacion getDenegacionByOrganismoEstadoCausaySubcausa(Long organismoId, String estado, String causa, String subcausa);

    Denegacion getDenegacionByOrganismoYCausa(Long organismoId, String causa);

    List<Denegacion> getDenegacionesActivasByOrganismo(Long organismoId);

    List<Denegacion> getDenegacionesTodasByOrganismo(Long organismoId);

    void deleteDenegacion(Long denegacionId);


}
