package es.uji.apps.bec.dao.dao;


import es.uji.apps.bec.model.RecursoDetalle;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface RecursoDetalleDAO extends BaseDAO {

    List<RecursoDetalle> getRecursoDetallesByRecursoIdAndTipo(Long recursoId, String tipo);
}
