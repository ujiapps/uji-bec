package es.uji.apps.bec.dao.dao;

import java.util.List;
import java.util.Map;

import es.uji.apps.bec.model.RegistroErroresEnvio;
import es.uji.commons.db.BaseDAO;

public interface RegistroErroresEnvioDAO extends BaseDAO
{
    List<RegistroErroresEnvio> getErroresEnvio(Map<String, String> filtros);
}
