package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoMiembro;
import es.uji.apps.bec.model.TipoMiembro;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoMiembroDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoMiembroDAO
{
    private QTipoMiembro tipoMiembro = QTipoMiembro.tipoMiembro;

    @Override
    public List<TipoMiembro> getTiposMiembros()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMiembro).orderBy(tipoMiembro.orden.asc());
        return query.list(tipoMiembro);
    }

    @Override
    public TipoMiembro getTipoMiembroById(Long tipoMiembroId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMiembro).where(tipoMiembro.id.eq(tipoMiembroId));
        return query.uniqueResult(tipoMiembro);
    }

    @Override
    @Transactional
    public void deleteTipoMiembro(Long tipoMiembroId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoMiembro);
        delete.where(tipoMiembro.id.eq(tipoMiembroId)).execute();
    }
}
