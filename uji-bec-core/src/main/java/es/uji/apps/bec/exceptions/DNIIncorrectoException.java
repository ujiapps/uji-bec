package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class DNIIncorrectoException extends CoreBaseException
{

    public DNIIncorrectoException()
    {
        super("El número de indentificació és incorrecte");
    }

    public DNIIncorrectoException(String message)
    {
        super(message);
    }
}
