package es.uji.apps.bec.dao.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.EstadoCivil;
import es.uji.apps.bec.model.QEstadoCivil;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstadoCivilDAODatabaseImpl extends BaseDAODatabaseImpl implements EstadoCivilDAO
{
    private QEstadoCivil estadoCivil = QEstadoCivil.estadoCivil;

    @Override
    public List<EstadoCivil> getEstadosCiviles()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estadoCivil).orderBy(estadoCivil.orden.asc());
        return query.list(estadoCivil);
    }

    @Override
    public EstadoCivil getEstadoCivilById(Long estadoCivilId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estadoCivil).where(estadoCivil.id.eq(estadoCivilId));
        return query.uniqueResult(estadoCivil);
    }

    @Override
    @Transactional
    public void deleteEstadoCivil(Long estadoCivilId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, estadoCivil);
        delete.where(estadoCivil.id.eq(estadoCivilId)).execute();
    }
}
