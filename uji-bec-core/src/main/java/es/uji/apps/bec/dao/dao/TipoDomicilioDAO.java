package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.commons.db.BaseDAO;

public interface TipoDomicilioDAO extends BaseDAO
{
  List<TipoDomicilio> getTiposDomicilios();
  List<TipoDomicilio> getTipoDomicilioById(Long tipoDomicilioId);
}
