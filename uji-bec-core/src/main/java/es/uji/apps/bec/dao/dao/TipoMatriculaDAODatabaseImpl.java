package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoMatricula;
import es.uji.apps.bec.model.TipoMatricula;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoMatriculaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoMatriculaDAO
{
    private QTipoMatricula tipoMatricula = QTipoMatricula.tipoMatricula;

    @Override
    public List<TipoMatricula> getTiposMatriculas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMatricula).orderBy(tipoMatricula.orden.asc());
        return query.list(tipoMatricula);
    }

    @Override
    public List<TipoMatricula> getTipoMatriculaById(Long tipoMatriculaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMatricula).where(tipoMatricula.id.eq(tipoMatriculaId));
        return query.list(tipoMatricula);
    }
}
