package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Declarante;
import es.uji.commons.db.BaseDAO;

public interface DeclaranteDAO extends BaseDAO
{
    void deleteDeclarante(Declarante declarante);
    List<Declarante> getDeclaranteById(Long declaranteId);
    List<Declarante> getDeclaranteBySolicitanteId(Long solicitanteId);
    void updateDeclarante(Declarante declarante);

}
