package es.uji.apps.bec.dao.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.PersonaEstudio;
import es.uji.apps.bec.model.QEstudio;
import es.uji.apps.bec.model.QPersonaEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EstudioDAODatabaseImpl extends BaseDAODatabaseImpl implements EstudioDAO
{
    @Override
    public List<Estudio> getEstudiosFiltrados(Map<String, Long> filtros)
    {
        BooleanBuilder builderFiltros = new BooleanBuilder();
        JPAQuery query = new JPAQuery(entityManager);
        QEstudio qEstudio = QEstudio.estudio;
        QPersonaEstudio qPersonaEstudio = QPersonaEstudio.personaEstudio;

        Long personaId = null;
        Long cursoAcademicoId = null;

        if (filtros.get("personaId") != null && filtros.get("cursoAcademicoId") != null)
        {

            List<PersonaEstudio> academicosByPersonaYCursoAca = PersonaEstudio
                    .getAcademicosByPersonaYCursoAca(filtros.get("personaId"),
                            filtros.get("cursoAcademicoId"));

            if (academicosByPersonaYCursoAca != null && !academicosByPersonaYCursoAca.isEmpty())
            {
                personaId = filtros.get("personaId");
                addPersonaFilter(personaId, builderFiltros);

                cursoAcademicoId = filtros.get("cursoAcademicoId");
                addCursoAcademicoFilter(cursoAcademicoId, builderFiltros);
            }
        }

        if (builderFiltros.hasValue())
        {
            query.from(qEstudio).join(qEstudio.personasEstudios, qPersonaEstudio);
            query.where(builderFiltros);
        }
        else
        {
            query.from(qEstudio);
        }

        query.orderBy(qEstudio.nombre.asc());

        return query.list(qEstudio);
    }

    private void addPersonaFilter(Long personaId, BooleanBuilder builderFiltros)
    {
        builderFiltros.and(QPersonaEstudio.personaEstudio.persona.id.eq(personaId));
    }

    private void addCursoAcademicoFilter(Long cursoAcademicoId, BooleanBuilder builderFiltros)
    {
        builderFiltros.and(QPersonaEstudio.personaEstudio.cursoAcademico.id.eq(cursoAcademicoId));
    }
}