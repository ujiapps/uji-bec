package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Economico;
import es.uji.commons.db.BaseDAO;

public interface EconomicoDAO extends BaseDAO
{
    void deleteEconomico(Economico economico);

    List<Economico> getEconomicoById(Long economicoId);

    List<Economico> getEconomicoBySolicitanteId(Long economicoId);

    void updateEconomico(Economico economico);
}
