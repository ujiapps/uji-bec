package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.LocalidadDAO;

/**
 * The persistent class for the BC2_EXT_LOCALIDADES database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_LOCALIDADES")
public class Localidad implements Serializable
{
    private static LocalidadDAO localidadDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String codigo;
    @Column(name = "CODIGO_AYUNTAMIENTO")
    private String codigoAyuntamiento;
    @Column(name = "NOMBRE_AYUNTAMIENTO")
    private String nombreAyuntamiento;
    private Boolean baja;
    private Float distancia;
    // bi-directional many-to-one association to Domicilio
    @OneToMany(mappedBy = "localidad")
    private Set<Domicilio> domicilios;
    // bi-directional many-to-one association to Provincia
    @ManyToOne
    private Provincia provincia;

    public Localidad()
    {
        this.setBaja(false);
    }

    public static Localidad getLocalidadById(Long localidadId)
    {
        return localidadDAO.get(Localidad.class, localidadId).get(0);
    }

    public static Localidad getLocalidadByCodigo(String codigoLocalidadMinisterio)
    {
        return localidadDAO.getLocalidadByCodigo(codigoLocalidadMinisterio);
    }

    @Autowired
    public void setLocalidadDAO(LocalidadDAO localidadDAO)
    {
        Localidad.localidadDAO = localidadDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getCodigoAyuntamiento()
    {
        return this.codigoAyuntamiento;
    }

    public void setCodigoAyuntamiento(String codigoAyuntamiento)
    {
        this.codigoAyuntamiento = codigoAyuntamiento;
    }

    public String getNombreAyuntamiento()
    {
        return this.nombreAyuntamiento;
    }

    public void setNombreAyuntamiento(String nombreAyuntamiento)
    {
        this.nombreAyuntamiento = nombreAyuntamiento;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean getBaja()
    {
        return this.baja;
    }

    public void setBaja(Boolean baja)
    {
        this.baja = baja;
    }

    public Float getDistancia()
    {
        return distancia;
    }

    public void setDistancia(Float distancia)
    {
        this.distancia = distancia;
    }

    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }

    public Provincia getProvincia()
    {
        return this.provincia;
    }

    public void setProvincia(Provincia provincia)
    {
        this.provincia = provincia;
    }
}