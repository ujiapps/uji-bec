package es.uji.apps.bec.dao.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.QBeca;
import es.uji.apps.bec.model.QConvocatoria;
import es.uji.apps.bec.model.QEstado;
import es.uji.apps.bec.model.QProceso;
import es.uji.apps.bec.model.QTanda;
import es.uji.apps.bec.model.Tanda;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Repository
public class TandaDAODatabaseImpl extends BaseDAODatabaseImpl implements TandaDAO
{
    @Override
    public List<Tanda> getTandas(Map<String, String> filtros)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;

        Long cursoAcademicoId = ParamUtils.parseLong(filtros.get("cursoAcademicoId"));
        Long convocatoriaId = ParamUtils.parseLong(filtros.get("convocatoriaId"));
        Long tandaId = ParamUtils.parseLong(filtros.get("tandaId"));

        String descripcion = filtros.get("descripcion");
        String stringActiva = filtros.get("activa");
        Boolean activa = false;
        if (stringActiva != null && stringActiva.equals("true"))
        {
            activa = true;
        }

        BooleanBuilder condiciones = new BooleanBuilder();
        addFiltroCursoAcademico(cursoAcademicoId, condiciones);
        addFiltroConvocatoria(convocatoriaId, condiciones);
        addFiltroTandaId(tandaId, condiciones);
        addFiltroDescripcion(descripcion, condiciones);
        addFiltroActivas(activa, condiciones);

        query.from(tanda);

        if (condiciones.hasValue())
        {
            query.where(condiciones);
        }
        query.orderBy(tanda.cursoAcademico.id.desc(), tanda.tandaId.desc());

        return query.list(tanda);
    }

    private void addFiltroActivas(Boolean activa, BooleanBuilder filtro)
    {
        if (activa != null && activa)
        {
            filtro.and(QTanda.tanda.activa.isTrue());
        }
    }

    private void addFiltroDescripcion(String descripcion, BooleanBuilder filtro)
    {
        if (descripcion != null && !descripcion.isEmpty())
        {
            filtro.and(QTanda.tanda.descripcion.lower().like('%' + descripcion.toLowerCase() + '%'));
        }
    }

    private void addFiltroCursoAcademico(Long cursoAcademicoId, BooleanBuilder filtro)
    {
        if (cursoAcademicoId != null)
        {
            filtro.and(QTanda.tanda.cursoAcademico.id.eq(cursoAcademicoId));
        }
    }

    private void addFiltroConvocatoria(Long convocatoriaId, BooleanBuilder filtro)
    {
        if (convocatoriaId != null)
        {
            filtro.and(QTanda.tanda.convocatoria.id.eq(convocatoriaId));
        }
    }

    private void addFiltroTandaId(Long tandaId, BooleanBuilder filtro)
    {
        if (tandaId != null)
        {
            filtro.and(QTanda.tanda.tandaId.eq(tandaId));
        }
    }

    @Override
    public List<Tanda> getTandasByTandaIdAndOrganismoIdAndCursoAcademicoId(Long tandaId,
            Long organismoId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;
        QConvocatoria convocatoria = QConvocatoria.convocatoria;

        query.from(tanda)
                .join(tanda.convocatoria, convocatoria)
                .where(tanda.tandaId.eq(tandaId).and(convocatoria.organismo.id.eq(organismoId))
                        .and(tanda.cursoAcademico.id.eq(cursoAcademicoId)));

        return query.list(tanda);
    }

    @Override
    public List<Estado> getEstadosBecas(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;
        QBeca becas = QBeca.beca;
        QEstado estados = QEstado.estado;

        query.from(tanda).join(tanda.becas, becas).join(becas.estado, estados)
                .where(tanda.id.eq(tandaId));

        return query.distinct().list(estados);
    }

    @Override
    public List<Proceso> getProcesosBecas(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;
        QBeca becas = QBeca.beca;
        QProceso procesos = QProceso.proceso;

        query.from(tanda).join(tanda.becas, becas).join(becas.proceso, procesos)
                .where(tanda.id.eq(tandaId));

        return query.distinct().list(procesos);
    }

    @Override
    @Transactional
    public void updateTanda(Tanda tanda)
    {
        QTanda qTanda = QTanda.tanda;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qTanda);

        updateClause.where(qTanda.id.eq(tanda.getId()))
                .set(qTanda.descripcion, tanda.getDescripcion())
                .set(qTanda.fecha, tanda.getFecha()).set(qTanda.activa, tanda.isActiva())
                .set(qTanda.fechaNotificacion, tanda.getFechaNotificacion())
                .set(qTanda.fechaReunionJurado, tanda.getFechaReunionJurado())
                .set(qTanda.usuario, tanda.getUsuario()).execute();
    }

    @Override
    public Tanda getTandaByTandaIdAndCursoAca(Long tandaId, Long cursoAcaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;

        query.from(tanda).where(
                tanda.tandaId.eq(tandaId).and(tanda.cursoAcademico.id.eq(cursoAcaId)));

        return query.list(tanda).get(0);
    }

    @Override
    public Long getMaximoTandaIdByCursoAca(Long cursoAcaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;

        query.from(tanda).where(tanda.cursoAcademico.id.eq(cursoAcaId));

        return query.uniqueResult(tanda.tandaId.max());
    }
}
