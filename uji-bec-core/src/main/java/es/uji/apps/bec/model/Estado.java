package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.EstadoDAO;

/**
 * The persistent class for the BC2_ESTADOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_ESTADOS")
public class Estado implements Serializable
{
    private static EstadoDAO estadoDAO;

    @Id
    private Long id;

    private String nombre;

    private Long orden;

    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "estado")
    private Set<Beca> becas;

    public Estado()
    {
    }

    public static Estado getEstadoById(Long estadoId)
    {
        return estadoDAO.get(Estado.class, estadoId).get(0);
    }

    @Autowired
    public void setEstadoDAO(EstadoDAO estadoDAO)
    {
        Estado.estadoDAO = estadoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }
}