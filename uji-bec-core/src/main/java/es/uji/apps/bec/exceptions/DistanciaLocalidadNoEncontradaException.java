package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class DistanciaLocalidadNoEncontradaException extends GeneralBECException
{
    public DistanciaLocalidadNoEncontradaException()
    {
        super("Error: No es pot inserir l\'ajuda de desplaçament perquè no es troba la distància de la localitat");
    }

    public DistanciaLocalidadNoEncontradaException(String message)
    {
        super(message);
    }
}
