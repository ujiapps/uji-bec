package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.BecaEstadoResolucion;
import es.uji.commons.db.BaseDAO;

public interface BecaEstadoResolucionDAO extends BaseDAO
{

    public List<BecaEstadoResolucion> getResolucionesByMultienvioId(Long multiEnvioId);

}
