package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class EstudioNoEncontradoException extends GeneralBECException
{
    public EstudioNoEncontradoException()
    {
        super("El codi Uji seleccionat no existeix");
    }

    public EstudioNoEncontradoException(String message)
    {
        super(message);
    }
}
