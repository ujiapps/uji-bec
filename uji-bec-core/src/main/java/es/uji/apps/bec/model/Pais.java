package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.PaisDAO;

/**
 * The persistent class for the BC2_EXT_PAISES database table.
 */
@Entity
@Table(name = "BC2_EXT_PAISES")
@Component
public class Pais implements Serializable
{
    private static final long serialVersionUID = 1L;
    public static PaisDAO paisDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    @Column(name = "CODIGO_ISO")
    private String codigoIso;
    private String nacionalidad;
    private String nombre;
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "pais")
    private Set<Miembro> miembros;

    public Pais()
    {
    }

    public static Pais getPaisById(String paisId)
    {

        return paisDAO.get(Pais.class, "id='" + paisId + "'").get(0);
    }

    @Autowired
    public void setPaisDAO(PaisDAO paisDAO)
    {
        Pais.paisDAO = paisDAO;
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCodigoIso()
    {
        return this.codigoIso;
    }

    public void setCodigoIso(String codigoIso)
    {
        this.codigoIso = codigoIso;
    }

    public String getNacionalidad()
    {
        return this.nacionalidad;
    }

    public void setNacionalidad(String nacionalidad)
    {
        this.nacionalidad = nacionalidad;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }
}