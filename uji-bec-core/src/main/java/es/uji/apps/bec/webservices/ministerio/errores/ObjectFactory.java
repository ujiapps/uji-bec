//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.12.21 at 12:24:35 PM CET 
//

package es.uji.apps.bec.webservices.ministerio.errores;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface
 * generated in the es.uji.apps.bec.webservices.ministerio.errores package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation
 * for XML content. The Java representation of XML content can consist of schema derived interfaces
 * and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{

    private final static QName _CaenNumSoliCarg_QNAME = new QName("", "CaenNumSoliCarg");
    private final static QName _CaenIdCurso_QNAME = new QName("", "CaenIdCurso");
    private final static QName _ErroFecha_QNAME = new QName("", "ErroFecha");
    private final static QName _CaenCodSituacEnvio_QNAME = new QName("", "CaenCodSituacEnvio");
    private final static QName _ErroLote_QNAME = new QName("", "ErroLote");
    private final static QName _CaenCodUTTramite_QNAME = new QName("", "CaenCodUTTramite");
    private final static QName _CaenTipoUTTramite_QNAME = new QName("", "CaenTipoUTTramite");
    private final static QName _CaenCodUTGen_QNAME = new QName("", "CaenCodUTGen");
    private final static QName _ErroLoteSSCC_QNAME = new QName("", "ErroLoteSSCC");
    private final static QName _CaenLote_QNAME = new QName("", "CaenLote");
    private final static QName _ErroTipoUTTramite_QNAME = new QName("", "ErroTipoUTTramite");
    private final static QName _ErroIdSolicitud_QNAME = new QName("", "ErroIdSolicitud");
    private final static QName _ErroClave_QNAME = new QName("", "ErroClave");
    private final static QName _ErroCodDocuIdent_QNAME = new QName("", "ErroCodDocuIdent");
    private final static QName _CaenTotSoli_QNAME = new QName("", "CaenTotSoli");
    private final static QName _CaenFechaRecep_QNAME = new QName("", "CaenFechaRecep");
    private final static QName _CaenNumSoliErr_QNAME = new QName("", "CaenNumSoliErr");
    private final static QName _CaenLoteGen_QNAME = new QName("", "CaenLoteGen");
    private final static QName _ErroNif_QNAME = new QName("", "ErroNif");
    private final static QName _ErroSecuenc_QNAME = new QName("", "ErroSecuenc");
    private final static QName _ErroTipoUTGen_QNAME = new QName("", "ErroTipoUTGen");
    private final static QName _ErroError_QNAME = new QName("", "ErroError");
    private final static QName _ErroApellido1_QNAME = new QName("", "ErroApellido1");
    private final static QName _CaenSecuenc_QNAME = new QName("", "CaenSecuenc");
    private final static QName _ErroApellido2_QNAME = new QName("", "ErroApellido2");
    private final static QName _ErroCodUTGen_QNAME = new QName("", "ErroCodUTGen");
    private final static QName _CaenFechaBD_QNAME = new QName("", "CaenFechaBD");
    private final static QName _CaenCodConv_QNAME = new QName("", "CaenCodConv");
    private final static QName _ErroNombre_QNAME = new QName("", "ErroNombre");
    private final static QName _CaenTipoEnvio_QNAME = new QName("", "CaenTipoEnvio");
    private final static QName _ErroCodTiperrg_QNAME = new QName("", "ErroCodTiperrg");
    private final static QName _CaenFechaGen_QNAME = new QName("", "CaenFechaGen");
    private final static QName _CaenFechaEnv_QNAME = new QName("", "CaenFechaEnv");
    private final static QName _ErroSecErr_QNAME = new QName("", "ErroSecErr");
    private final static QName _Multienvio_QNAME = new QName("", "Multienvio");
    private final static QName _ErroCodConv_QNAME = new QName("", "ErroCodConv");
    private final static QName _CaenTipoUTGen_QNAME = new QName("", "CaenTipoUTGen");
    private final static QName _ErroIdCurso_QNAME = new QName("", "ErroIdCurso");
    private final static QName _ErroDeslError_QNAME = new QName("", "ErroDeslError");
    private final static QName _ErroCodUTTramite_QNAME = new QName("", "ErroCodUTTramite");
    private final static QName _ErroCodTiso_QNAME = new QName("", "ErroCodTiso");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes
     * for package: es.uji.apps.bec.webservices.ministerio.errores
     * 
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link MultienvioType }
     * 
     */
    public MultienvioType createMultienvioType()
    {
        return new MultienvioType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType()
    {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link EnvioType }
     * 
     */
    public EnvioType createEnvioType()
    {
        return new EnvioType();
    }

    /**
     * Create an instance of {@link CabeceraEnvioType }
     * 
     */
    public CabeceraEnvioType createCabeceraEnvioType()
    {
        return new CabeceraEnvioType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenNumSoliCarg")
    public JAXBElement<Long> createCaenNumSoliCarg(Long value)
    {
        return new JAXBElement<Long>(_CaenNumSoliCarg_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenIdCurso")
    public JAXBElement<String> createCaenIdCurso(String value)
    {
        return new JAXBElement<String>(_CaenIdCurso_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroFecha")
    public JAXBElement<XMLGregorianCalendar> createErroFecha(XMLGregorianCalendar value)
    {
        return new JAXBElement<XMLGregorianCalendar>(_ErroFecha_QNAME, XMLGregorianCalendar.class,
                null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenCodSituacEnvio")
    public JAXBElement<Long> createCaenCodSituacEnvio(Long value)
    {
        return new JAXBElement<Long>(_CaenCodSituacEnvio_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroLote")
    public JAXBElement<String> createErroLote(String value)
    {
        return new JAXBElement<String>(_ErroLote_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenCodUTTramite")
    public JAXBElement<String> createCaenCodUTTramite(String value)
    {
        return new JAXBElement<String>(_CaenCodUTTramite_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenTipoUTTramite")
    public JAXBElement<String> createCaenTipoUTTramite(String value)
    {
        return new JAXBElement<String>(_CaenTipoUTTramite_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenCodUTGen")
    public JAXBElement<String> createCaenCodUTGen(String value)
    {
        return new JAXBElement<String>(_CaenCodUTGen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroLoteSSCC")
    public JAXBElement<String> createErroLoteSSCC(String value)
    {
        return new JAXBElement<String>(_ErroLoteSSCC_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenLote")
    public JAXBElement<String> createCaenLote(String value)
    {
        return new JAXBElement<String>(_CaenLote_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroTipoUTTramite")
    public JAXBElement<String> createErroTipoUTTramite(String value)
    {
        return new JAXBElement<String>(_ErroTipoUTTramite_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroIdSolicitud")
    public JAXBElement<Long> createErroIdSolicitud(Long value)
    {
        return new JAXBElement<Long>(_ErroIdSolicitud_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroClave")
    public JAXBElement<String> createErroClave(String value)
    {
        return new JAXBElement<String>(_ErroClave_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodDocuIdent")
    public JAXBElement<Long> createErroCodDocuIdent(Long value)
    {
        return new JAXBElement<Long>(_ErroCodDocuIdent_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenTotSoli")
    public JAXBElement<Long> createCaenTotSoli(Long value)
    {
        return new JAXBElement<Long>(_CaenTotSoli_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenFechaRecep")
    public JAXBElement<XMLGregorianCalendar> createCaenFechaRecep(XMLGregorianCalendar value)
    {
        return new JAXBElement<XMLGregorianCalendar>(_CaenFechaRecep_QNAME,
                XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenNumSoliErr")
    public JAXBElement<Long> createCaenNumSoliErr(Long value)
    {
        return new JAXBElement<Long>(_CaenNumSoliErr_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenLoteGen")
    public JAXBElement<String> createCaenLoteGen(String value)
    {
        return new JAXBElement<String>(_CaenLoteGen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroNif")
    public JAXBElement<String> createErroNif(String value)
    {
        return new JAXBElement<String>(_ErroNif_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroSecuenc")
    public JAXBElement<Long> createErroSecuenc(Long value)
    {
        return new JAXBElement<Long>(_ErroSecuenc_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroTipoUTGen")
    public JAXBElement<String> createErroTipoUTGen(String value)
    {
        return new JAXBElement<String>(_ErroTipoUTGen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroError")
    public JAXBElement<String> createErroError(String value)
    {
        return new JAXBElement<String>(_ErroError_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroApellido1")
    public JAXBElement<String> createErroApellido1(String value)
    {
        return new JAXBElement<String>(_ErroApellido1_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenSecuenc")
    public JAXBElement<Long> createCaenSecuenc(Long value)
    {
        return new JAXBElement<Long>(_CaenSecuenc_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroApellido2")
    public JAXBElement<String> createErroApellido2(String value)
    {
        return new JAXBElement<String>(_ErroApellido2_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodUTGen")
    public JAXBElement<String> createErroCodUTGen(String value)
    {
        return new JAXBElement<String>(_ErroCodUTGen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenFechaBD")
    public JAXBElement<XMLGregorianCalendar> createCaenFechaBD(XMLGregorianCalendar value)
    {
        return new JAXBElement<XMLGregorianCalendar>(_CaenFechaBD_QNAME,
                XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenCodConv")
    public JAXBElement<String> createCaenCodConv(String value)
    {
        return new JAXBElement<String>(_CaenCodConv_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroNombre")
    public JAXBElement<String> createErroNombre(String value)
    {
        return new JAXBElement<String>(_ErroNombre_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenTipoEnvio")
    public JAXBElement<String> createCaenTipoEnvio(String value)
    {
        return new JAXBElement<String>(_CaenTipoEnvio_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodTiperrg")
    public JAXBElement<String> createErroCodTiperrg(String value)
    {
        return new JAXBElement<String>(_ErroCodTiperrg_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenFechaGen")
    public JAXBElement<XMLGregorianCalendar> createCaenFechaGen(XMLGregorianCalendar value)
    {
        return new JAXBElement<XMLGregorianCalendar>(_CaenFechaGen_QNAME,
                XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenFechaEnv")
    public JAXBElement<XMLGregorianCalendar> createCaenFechaEnv(XMLGregorianCalendar value)
    {
        return new JAXBElement<XMLGregorianCalendar>(_CaenFechaEnv_QNAME,
                XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroSecErr")
    public JAXBElement<String> createErroSecErr(String value)
    {
        return new JAXBElement<String>(_ErroSecErr_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultienvioType }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Multienvio")
    public JAXBElement<MultienvioType> createMultienvio(MultienvioType value)
    {
        return new JAXBElement<MultienvioType>(_Multienvio_QNAME, MultienvioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodConv")
    public JAXBElement<String> createErroCodConv(String value)
    {
        return new JAXBElement<String>(_ErroCodConv_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CaenTipoUTGen")
    public JAXBElement<String> createCaenTipoUTGen(String value)
    {
        return new JAXBElement<String>(_CaenTipoUTGen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroIdCurso")
    public JAXBElement<String> createErroIdCurso(String value)
    {
        return new JAXBElement<String>(_ErroIdCurso_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroDeslError")
    public JAXBElement<String> createErroDeslError(String value)
    {
        return new JAXBElement<String>(_ErroDeslError_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodUTTramite")
    public JAXBElement<String> createErroCodUTTramite(String value)
    {
        return new JAXBElement<String>(_ErroCodUTTramite_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErroCodTiso")
    public JAXBElement<Long> createErroCodTiso(Long value)
    {
        return new JAXBElement<Long>(_ErroCodTiso_QNAME, Long.class, null, value);
    }

}
