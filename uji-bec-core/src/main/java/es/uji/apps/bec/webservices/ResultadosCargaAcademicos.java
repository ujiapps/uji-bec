package es.uji.apps.bec.webservices;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos;
import es.uji.commons.rest.CoreBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

@Repository
public class ResultadosCargaAcademicos extends CoreBaseService {
    private static final Integer SECUENCIA = 1;
    private static Logger log = Logger.getLogger(DatosSolicitudes.class);

    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    public ResultadosCargaAcademicos() {
    }

    public DatosAcademicos obtenerResultados(Long tandaId) throws IOException, GeneralBECException, JAXBException, SOAPException {
        Tanda tanda = Tanda.getTandaById(tandaId);
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tanda.getId());
        Long loteAcademicosMec = registroEnvio.getEnvioId();
        DatosAcademicos resultadosLote = service.getResultadoCargaDatosAcademicosUniv(loteAcademicosMec);

        if (envioNoDisponible(resultadosLote)) {
            throw new GeneralBECException("Resultados del proceso del lote todavía no disponibles");
        }

        return resultadosLote;
    }

    private boolean envioNoDisponible(DatosAcademicos erroresSolicitudes) {
        return erroresSolicitudes.getCabecera().getCodEstado() == null;
    }

}