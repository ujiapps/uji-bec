package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorEstadoCivil extends Mapeador<Long, Object>
{
    public MapeadorEstadoCivil()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "1");
        add(new Long(1L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "2");
        add(new Long(2L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "3");
        add(new Long(3L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "4");
        add(new Long(4L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "5");
        add(new Long(5L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "6");
        add(new Long(6L), valores);
    }
}
