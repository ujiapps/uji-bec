package es.uji.apps.bec.webservices.ministerio.colaboracion.lista;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SolicitudesRegistro")
public class SolicitudesRegistro
{
    private List<SolicitudRegistro> solicitudesRegistro;

    public SolicitudesRegistro()
    {
    }

    @XmlElement(name = "SolicitudRegistro")
    public List<SolicitudRegistro> getSolicitudesRegistro()
    {
        return solicitudesRegistro;
    }

    public void setSolicitudesRegistro(List<SolicitudRegistro> solicitudesRegistro)
    {
        this.solicitudesRegistro = solicitudesRegistro;
    }
}
