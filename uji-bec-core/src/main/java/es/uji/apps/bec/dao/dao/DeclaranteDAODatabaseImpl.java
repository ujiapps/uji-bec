package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.Declarante;
import es.uji.apps.bec.model.QDeclarante;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DeclaranteDAODatabaseImpl extends BaseDAODatabaseImpl implements DeclaranteDAO
{
    private  QDeclarante qDeclarante = QDeclarante.declarante1;

    @Override
    public List<Declarante> getDeclaranteById(Long declaranteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDeclarante).where(qDeclarante.id.eq(declaranteId));
        return query.list(qDeclarante);
    }

    @Override
    public List<Declarante> getDeclaranteBySolicitanteId(Long solicitanteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDeclarante).where(qDeclarante.solicitante.id.eq(solicitanteId));
        return query.list(qDeclarante);
    }

    @Override
    @Transactional
    public void deleteDeclarante(Declarante declarante)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qDeclarante);
        delete.where(qDeclarante.id.eq(declarante.getId())).execute();
    }

    @Override
    @Transactional
    public void updateDeclarante(Declarante declarante)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qDeclarante);

        updateClause.where(qDeclarante.id.eq(declarante.getId()))
        .set(qDeclarante.declarante, declarante.getDeclarante())
        .set(qDeclarante.indicadorRentasExtranjero, declarante.getIndicadorRentasExtranjero())
        .set(qDeclarante.rentasExtranjero, declarante.getRentasExtranjero())
        .set(qDeclarante.localidadDeclarante, declarante.getLocalidadDeclarante())
        .set(qDeclarante.fechaDeclarante, declarante.getFechaDeclarante())
        .set(qDeclarante.indicadorUltCurso, declarante.getIndicadorUltCurso())
        .set(qDeclarante.indicadorCursoPendiente, declarante.getIndicadorCursoPendiente())
        .set(qDeclarante.indicadorCursoAnterior, declarante.getIndicadorCursoAnterior())
        .set(qDeclarante.indicadorIndependiente, declarante.getIndicadorIndependiente())
        .set(qDeclarante.indicadorInem, declarante.getIndicadorInem())
        .set(qDeclarante.empleador, declarante.getEmpleador())
        .set(qDeclarante.tipoIdentificacion, declarante.getTipoIdentificacion())
        .set(qDeclarante.identificacion, declarante.getIdentificacion())
        .set(qDeclarante.ingresosAnuales, declarante.getIngresosAnuales())
        .set(qDeclarante.codigoParentesco,  declarante.getCodigoParentesco())
        .set(qDeclarante.decCodParentescoConsen,  declarante.getDecCodParentescoConsen())
        .set(qDeclarante.decTipoIdentificacionConsen,  declarante.getDecTipoIdentificacionConsen())
        .set(qDeclarante.decIdentificacionConsen,  declarante.getDecIdentificacionConsen())
        .execute();
    }
}
