package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.QProceso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProcesoDAODatabaseImpl extends BaseDAODatabaseImpl implements ProcesoDAO
{
    @Override
    public List<Proceso> getProcesos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QProceso proceso = QProceso.proceso;

        query.from(proceso).orderBy(proceso.orden.asc());

        return query.list(proceso);
    }
}
