package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.ArrendatarioDAO;
import es.uji.apps.bec.webservices.ministerio.solicitudes.ArrendatariosType;

/**
 * The persistent class for the BC2_DOM_ARRENDATARIOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_DOM_ARRENDATARIOS")
public class Arrendatario implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String identificacion;

    private String nombre;

    // bi-directional many-to-one association to Bc2Domicilio
    @ManyToOne
    private Domicilio domicilio;

    // bi-directional many-to-one association to Bc2TiposIdentificacion
    @ManyToOne
    @JoinColumn(name = "TIPO_IDENTIFICACION_ID")
    private TipoIdentificacion tipoIdentificacion;

    private static ArrendatarioDAO arrendatarioDAO;

    @Autowired
    public void setArrendatarioDAO(ArrendatarioDAO arrendatarioDAO)
    {
        Arrendatario.arrendatarioDAO = arrendatarioDAO;
    }

    public Arrendatario()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Domicilio getDomicilio()
    {
        return this.domicilio;
    }

    public void setDomicilio(Domicilio domicilio)
    {
        this.domicilio = domicilio;
    }

    public TipoIdentificacion getTipoIdentificacion()
    {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion)
    {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public static List<Arrendatario> getArrendatariosByDomicilio(Long domicilioId)
    {
        return arrendatarioDAO.getArrendatariosByDomicilio(domicilioId);
    }

    public static Arrendatario getArrendatarioById(Long arrendatarioId)
    {
        return arrendatarioDAO.getArrendatarioById(arrendatarioId);
    }

    public void updateArrendatario()
    {
        arrendatarioDAO.updateArrendatario(this);
    }

    @Transactional
    public void insert()
    {
        arrendatarioDAO.insert(this);
    }

    public static void delete(Long arrendatarioId)
    {
        arrendatarioDAO.delete(Arrendatario.class, arrendatarioId);
    }

    public void importarDatosMinisterio(ArrendatariosType arrendatariosType)
    {
        setNombre(arrendatariosType.getArreArrendatario());
        setIdentificacion(arrendatariosType.getArreNif());

        if (arrendatariosType.getArreCodDocuIdent() != null)
        {
            TipoIdentificacion tipoIdentificacionUji = TipoIdentificacion.getTipoIdentificacionById(
                    arrendatariosType.getArreCodDocuIdent());
            setTipoIdentificacion(tipoIdentificacionUji);
        }
    }
}