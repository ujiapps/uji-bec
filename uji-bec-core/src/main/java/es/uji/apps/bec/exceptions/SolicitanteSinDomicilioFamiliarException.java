package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class SolicitanteSinDomicilioFamiliarException extends GeneralBECException
{
    public SolicitanteSinDomicilioFamiliarException()
    {
        super("La beca no tiene domicilio Familiar.");
    }

    public SolicitanteSinDomicilioFamiliarException(String message)
    {
        super(message);
    }
}
