package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.QCursoAcademico;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoAcademicoDAODatabaseImpl extends BaseDAODatabaseImpl implements CursoAcademicoDAO
{
    private QCursoAcademico cursoAcademico = QCursoAcademico.cursoAcademico;

    @Override
    public List<CursoAcademico> getCursos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cursoAcademico).orderBy(cursoAcademico.id.desc());
        return query.list(cursoAcademico);
    }

    @Override
    public List<CursoAcademico> getCursoActivo()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cursoAcademico).where(cursoAcademico.activo.eq(true));
        return query.list(cursoAcademico);
    }

    @Override
    public CursoAcademico getCursoAcademicoById(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cursoAcademico).where(cursoAcademico.id.eq(cursoAcademicoId));
        return query.uniqueResult(cursoAcademico);
    }

    @Override
    @Transactional
    public void desactivaCursosActivos()
    {
        QCursoAcademico qCursoAcademico = QCursoAcademico.cursoAcademico;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qCursoAcademico);
        updateClause.set(qCursoAcademico.activo, false).execute();
    }

    @Override
    public List<CursoAcademico> getCursosMayoresOIgualesQue(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cursoAcademico).where(cursoAcademico.id.goe(cursoAcademicoId)).orderBy(cursoAcademico.id.desc());
        return query.list(cursoAcademico);
    }

    @Override
    @Transactional
    public void deleteCursoAcademico(Long cursoAcademicoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, cursoAcademico);
        delete.where(cursoAcademico.id.eq(cursoAcademicoId)).execute();
    }
}
