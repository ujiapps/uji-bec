package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class MiembroDuplicadoException extends GeneralBECException
{
    public MiembroDuplicadoException()
    {
        super("No es poden inserir dos membres amb el mateix DNI");
    }

    public MiembroDuplicadoException(String message)
    {
        super(message);
    }
}