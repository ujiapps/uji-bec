//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2024.10.08 at 02:52:32 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.solicitudes;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IdiomasCFGSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdiomasCFGSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}IdgsCodIdioma"/>
 *         &lt;element ref="{}IdgsCertificado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdiomasCFGSType", propOrder = {
    "idgsCodIdioma",
    "idgsCertificado"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.solicitudes.IdiomasCFGSType")
@Table(name = "IDIOMASCFGSTYPE", schema = "UJI_BECAS_WS_SOLICITUDES")
@Inheritance(strategy = InheritanceType.JOINED)
public class IdiomasCFGSType
    implements Equals, HashCode
{

    @XmlElement(name = "IdgsCodIdioma", required = true)
    protected String idgsCodIdioma;
    @XmlElement(name = "IdgsCertificado")
    protected String idgsCertificado;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the idgsCodIdioma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "IDGSCODIDIOMA", length = 2)
    public String getIdgsCodIdioma() {
        return idgsCodIdioma;
    }

    /**
     * Sets the value of the idgsCodIdioma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdgsCodIdioma(String value) {
        this.idgsCodIdioma = value;
    }

    /**
     * Gets the value of the idgsCertificado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "IDGSCERTIFICADO", length = 1)
    public String getIdgsCertificado() {
        return idgsCertificado;
    }

    /**
     * Sets the value of the idgsCertificado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdgsCertificado(String value) {
        this.idgsCertificado = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IdiomasCFGSType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final IdiomasCFGSType that = ((IdiomasCFGSType) object);
        {
            String lhsIdgsCodIdioma;
            lhsIdgsCodIdioma = this.getIdgsCodIdioma();
            String rhsIdgsCodIdioma;
            rhsIdgsCodIdioma = that.getIdgsCodIdioma();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idgsCodIdioma", lhsIdgsCodIdioma), LocatorUtils.property(thatLocator, "idgsCodIdioma", rhsIdgsCodIdioma), lhsIdgsCodIdioma, rhsIdgsCodIdioma)) {
                return false;
            }
        }
        {
            String lhsIdgsCertificado;
            lhsIdgsCertificado = this.getIdgsCertificado();
            String rhsIdgsCertificado;
            rhsIdgsCertificado = that.getIdgsCertificado();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idgsCertificado", lhsIdgsCertificado), LocatorUtils.property(thatLocator, "idgsCertificado", rhsIdgsCertificado), lhsIdgsCertificado, rhsIdgsCertificado)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theIdgsCodIdioma;
            theIdgsCodIdioma = this.getIdgsCodIdioma();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idgsCodIdioma", theIdgsCodIdioma), currentHashCode, theIdgsCodIdioma);
        }
        {
            String theIdgsCertificado;
            theIdgsCertificado = this.getIdgsCertificado();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idgsCertificado", theIdgsCertificado), currentHashCode, theIdgsCertificado);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
