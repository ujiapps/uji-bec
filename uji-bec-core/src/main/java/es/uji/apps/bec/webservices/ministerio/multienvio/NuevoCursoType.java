//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 12:21:09 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.multienvio;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDate;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for NuevoCursoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NuevoCursoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}NcurTipcentCtipocentro" minOccurs="0"/>
 *         &lt;element ref="{}NcurCcentro" minOccurs="0"/>
 *         &lt;element ref="{}NcurCodTcentro" minOccurs="0"/>
 *         &lt;element ref="{}NcurEstudios" minOccurs="0"/>
 *         &lt;element ref="{}NcurEspecialidad" minOccurs="0"/>
 *         &lt;element ref="{}NcurCurEduc" minOccurs="0"/>
 *         &lt;element ref="{}NcurCodMatri" minOccurs="0"/>
 *         &lt;element ref="{}NcurIndCompleto" minOccurs="0"/>
 *         &lt;element ref="{}NcurCurEducCompl" minOccurs="0"/>
 *         &lt;element ref="{}NcurTotalCredMatr" minOccurs="0"/>
 *         &lt;element ref="{}NcurCertifica" minOccurs="0"/>
 *         &lt;element ref="{}NcurLocCertif" minOccurs="0"/>
 *         &lt;element ref="{}NcurFechaCertif" minOccurs="0"/>
 *         &lt;element ref="{}NcurIndMatParcial" minOccurs="0"/>
 *         &lt;element ref="{}NcurIdPeriodo" minOccurs="0"/>
 *         &lt;element ref="{}NcurIndEstudiosFinales" minOccurs="0"/>
 *         &lt;element ref="{}NcurCreLimUniv" minOccurs="0"/>
 *         &lt;element ref="{}NcurTotalCrePlanEst" minOccurs="0"/>
 *         &lt;element ref="{}NcurAnosPlanEst" minOccurs="0"/>
 *         &lt;element name="AcademicosNCurso" type="{}AcademicosNCursoType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NuevoCursoType", propOrder = {
    "ncurTipcentCtipocentro",
    "ncurCcentro",
    "ncurCodTcentro",
    "ncurEstudios",
    "ncurEspecialidad",
    "ncurCurEduc",
    "ncurCodMatri",
    "ncurIndCompleto",
    "ncurCurEducCompl",
    "ncurTotalCredMatr",
    "ncurCertifica",
    "ncurLocCertif",
    "ncurFechaCertif",
    "ncurIndMatParcial",
    "ncurIdPeriodo",
    "ncurIndEstudiosFinales",
    "ncurCreLimUniv",
    "ncurTotalCrePlanEst",
    "ncurAnosPlanEst",
    "academicosNCurso"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.multienvio.NuevoCursoType")
@Table(name = "NUEVOCURSOTYPE", schema = "UJI_BECAS_WS_MULTIENVIO")
@Inheritance(strategy = InheritanceType.JOINED)
public class NuevoCursoType
    implements Equals, HashCode
{

    @XmlElement(name = "NcurTipcentCtipocentro")
    protected String ncurTipcentCtipocentro;
    @XmlElement(name = "NcurCcentro")
    protected String ncurCcentro;
    @XmlElement(name = "NcurCodTcentro")
    protected String ncurCodTcentro;
    @XmlElement(name = "NcurEstudios")
    protected String ncurEstudios;
    @XmlElement(name = "NcurEspecialidad")
    protected String ncurEspecialidad;
    @XmlElement(name = "NcurCurEduc")
    protected Long ncurCurEduc;
    @XmlElement(name = "NcurCodMatri")
    protected String ncurCodMatri;
    @XmlElement(name = "NcurIndCompleto")
    protected String ncurIndCompleto;
    @XmlElement(name = "NcurCurEducCompl")
    protected Long ncurCurEducCompl;
    @XmlElement(name = "NcurTotalCredMatr")
    protected Float ncurTotalCredMatr;
    @XmlElement(name = "NcurCertifica")
    protected String ncurCertifica;
    @XmlElement(name = "NcurLocCertif")
    protected String ncurLocCertif;
    @XmlElement(name = "NcurFechaCertif")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ncurFechaCertif;
    @XmlElement(name = "NcurIndMatParcial")
    protected String ncurIndMatParcial;
    @XmlElement(name = "NcurIdPeriodo")
    protected Long ncurIdPeriodo;
    @XmlElement(name = "NcurIndEstudiosFinales")
    protected String ncurIndEstudiosFinales;
    @XmlElement(name = "NcurCreLimUniv")
    protected String ncurCreLimUniv;
    @XmlElement(name = "NcurTotalCrePlanEst")
    protected Long ncurTotalCrePlanEst;
    @XmlElement(name = "NcurAnosPlanEst")
    protected Long ncurAnosPlanEst;
    @XmlElement(name = "AcademicosNCurso")
    protected List<AcademicosNCursoType> academicosNCurso;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the ncurTipcentCtipocentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURTIPCENTCTIPOCENTRO", length = 1)
    public String getNcurTipcentCtipocentro() {
        return ncurTipcentCtipocentro;
    }

    /**
     * Sets the value of the ncurTipcentCtipocentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurTipcentCtipocentro(String value) {
        this.ncurTipcentCtipocentro = value;
    }

    /**
     * Gets the value of the ncurCcentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURCCENTRO", length = 8)
    public String getNcurCcentro() {
        return ncurCcentro;
    }

    /**
     * Sets the value of the ncurCcentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurCcentro(String value) {
        this.ncurCcentro = value;
    }

    /**
     * Gets the value of the ncurCodTcentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURCODTCENTRO", length = 1)
    public String getNcurCodTcentro() {
        return ncurCodTcentro;
    }

    /**
     * Sets the value of the ncurCodTcentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurCodTcentro(String value) {
        this.ncurCodTcentro = value;
    }

    /**
     * Gets the value of the ncurEstudios property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURESTUDIOS", length = 255)
    public String getNcurEstudios() {
        return ncurEstudios;
    }

    /**
     * Sets the value of the ncurEstudios property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurEstudios(String value) {
        this.ncurEstudios = value;
    }

    /**
     * Gets the value of the ncurEspecialidad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURESPECIALIDAD", length = 255)
    public String getNcurEspecialidad() {
        return ncurEspecialidad;
    }

    /**
     * Sets the value of the ncurEspecialidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurEspecialidad(String value) {
        this.ncurEspecialidad = value;
    }

    /**
     * Gets the value of the ncurCurEduc property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "NCURCUREDUC", scale = 0)
    public Long getNcurCurEduc() {
        return ncurCurEduc;
    }

    /**
     * Sets the value of the ncurCurEduc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNcurCurEduc(Long value) {
        this.ncurCurEduc = value;
    }

    /**
     * Gets the value of the ncurCodMatri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURCODMATRI", length = 1)
    public String getNcurCodMatri() {
        return ncurCodMatri;
    }

    /**
     * Sets the value of the ncurCodMatri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurCodMatri(String value) {
        this.ncurCodMatri = value;
    }

    /**
     * Gets the value of the ncurIndCompleto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURINDCOMPLETO", length = 1)
    public String getNcurIndCompleto() {
        return ncurIndCompleto;
    }

    /**
     * Sets the value of the ncurIndCompleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurIndCompleto(String value) {
        this.ncurIndCompleto = value;
    }

    /**
     * Gets the value of the ncurCurEducCompl property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "NCURCUREDUCCOMPL", scale = 0)
    public Long getNcurCurEducCompl() {
        return ncurCurEducCompl;
    }

    /**
     * Sets the value of the ncurCurEducCompl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNcurCurEducCompl(Long value) {
        this.ncurCurEducCompl = value;
    }

    /**
     * Gets the value of the ncurTotalCredMatr property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "NCURTOTALCREDMATR", precision = 6, scale = 2)
    public Float getNcurTotalCredMatr() {
        return ncurTotalCredMatr;
    }

    /**
     * Sets the value of the ncurTotalCredMatr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setNcurTotalCredMatr(Float value) {
        this.ncurTotalCredMatr = value;
    }

    /**
     * Gets the value of the ncurCertifica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURCERTIFICA", length = 60)
    public String getNcurCertifica() {
        return ncurCertifica;
    }

    /**
     * Sets the value of the ncurCertifica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurCertifica(String value) {
        this.ncurCertifica = value;
    }

    /**
     * Gets the value of the ncurLocCertif property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURLOCCERTIF", length = 50)
    public String getNcurLocCertif() {
        return ncurLocCertif;
    }

    /**
     * Sets the value of the ncurLocCertif property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurLocCertif(String value) {
        this.ncurLocCertif = value;
    }

    /**
     * Gets the value of the ncurFechaCertif property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getNcurFechaCertif() {
        return ncurFechaCertif;
    }

    /**
     * Sets the value of the ncurFechaCertif property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNcurFechaCertif(XMLGregorianCalendar value) {
        this.ncurFechaCertif = value;
    }

    /**
     * Gets the value of the ncurIndMatParcial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURINDMATPARCIAL", length = 1)
    public String getNcurIndMatParcial() {
        return ncurIndMatParcial;
    }

    /**
     * Sets the value of the ncurIndMatParcial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurIndMatParcial(String value) {
        this.ncurIndMatParcial = value;
    }

    /**
     * Gets the value of the ncurIdPeriodo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "NCURIDPERIODO", scale = 0)
    public Long getNcurIdPeriodo() {
        return ncurIdPeriodo;
    }

    /**
     * Sets the value of the ncurIdPeriodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNcurIdPeriodo(Long value) {
        this.ncurIdPeriodo = value;
    }

    /**
     * Gets the value of the ncurIndEstudiosFinales property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURINDESTUDIOSFINALES", length = 1)
    public String getNcurIndEstudiosFinales() {
        return ncurIndEstudiosFinales;
    }

    /**
     * Sets the value of the ncurIndEstudiosFinales property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurIndEstudiosFinales(String value) {
        this.ncurIndEstudiosFinales = value;
    }

    /**
     * Gets the value of the ncurCreLimUniv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NCURCRELIMUNIV", length = 1)
    public String getNcurCreLimUniv() {
        return ncurCreLimUniv;
    }

    /**
     * Sets the value of the ncurCreLimUniv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNcurCreLimUniv(String value) {
        this.ncurCreLimUniv = value;
    }

    /**
     * Gets the value of the ncurTotalCrePlanEst property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "NCURTOTALCREPLANEST", scale = 0)
    public Long getNcurTotalCrePlanEst() {
        return ncurTotalCrePlanEst;
    }

    /**
     * Sets the value of the ncurTotalCrePlanEst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNcurTotalCrePlanEst(Long value) {
        this.ncurTotalCrePlanEst = value;
    }

    /**
     * Gets the value of the ncurAnosPlanEst property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "NCURANOSPLANEST", scale = 0)
    public Long getNcurAnosPlanEst() {
        return ncurAnosPlanEst;
    }

    /**
     * Sets the value of the ncurAnosPlanEst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNcurAnosPlanEst(Long value) {
        this.ncurAnosPlanEst = value;
    }

    /**
     * Gets the value of the academicosNCurso property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the academicosNCurso property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcademicosNCurso().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcademicosNCursoType }
     * 
     * 
     */
    @OneToMany(targetEntity = AcademicosNCursoType.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "ACADEMICOSNCURSO_NUEVOCURSOT_0")
    public List<AcademicosNCursoType> getAcademicosNCurso() {
        if (academicosNCurso == null) {
            academicosNCurso = new ArrayList<AcademicosNCursoType>();
        }
        return this.academicosNCurso;
    }

    /**
     * 
     * 
     */
    public void setAcademicosNCurso(List<AcademicosNCursoType> academicosNCurso) {
        this.academicosNCurso = academicosNCurso;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    @Basic
    @Column(name = "NCURFECHACERTIFITEM")
    @Temporal(TemporalType.DATE)
    public Date getNcurFechaCertifItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getNcurFechaCertif());
    }

    public void setNcurFechaCertifItem(Date target) {
        setNcurFechaCertif(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof NuevoCursoType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final NuevoCursoType that = ((NuevoCursoType) object);
        {
            String lhsNcurTipcentCtipocentro;
            lhsNcurTipcentCtipocentro = this.getNcurTipcentCtipocentro();
            String rhsNcurTipcentCtipocentro;
            rhsNcurTipcentCtipocentro = that.getNcurTipcentCtipocentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurTipcentCtipocentro", lhsNcurTipcentCtipocentro), LocatorUtils.property(thatLocator, "ncurTipcentCtipocentro", rhsNcurTipcentCtipocentro), lhsNcurTipcentCtipocentro, rhsNcurTipcentCtipocentro)) {
                return false;
            }
        }
        {
            String lhsNcurCcentro;
            lhsNcurCcentro = this.getNcurCcentro();
            String rhsNcurCcentro;
            rhsNcurCcentro = that.getNcurCcentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCcentro", lhsNcurCcentro), LocatorUtils.property(thatLocator, "ncurCcentro", rhsNcurCcentro), lhsNcurCcentro, rhsNcurCcentro)) {
                return false;
            }
        }
        {
            String lhsNcurCodTcentro;
            lhsNcurCodTcentro = this.getNcurCodTcentro();
            String rhsNcurCodTcentro;
            rhsNcurCodTcentro = that.getNcurCodTcentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCodTcentro", lhsNcurCodTcentro), LocatorUtils.property(thatLocator, "ncurCodTcentro", rhsNcurCodTcentro), lhsNcurCodTcentro, rhsNcurCodTcentro)) {
                return false;
            }
        }
        {
            String lhsNcurEstudios;
            lhsNcurEstudios = this.getNcurEstudios();
            String rhsNcurEstudios;
            rhsNcurEstudios = that.getNcurEstudios();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurEstudios", lhsNcurEstudios), LocatorUtils.property(thatLocator, "ncurEstudios", rhsNcurEstudios), lhsNcurEstudios, rhsNcurEstudios)) {
                return false;
            }
        }
        {
            String lhsNcurEspecialidad;
            lhsNcurEspecialidad = this.getNcurEspecialidad();
            String rhsNcurEspecialidad;
            rhsNcurEspecialidad = that.getNcurEspecialidad();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurEspecialidad", lhsNcurEspecialidad), LocatorUtils.property(thatLocator, "ncurEspecialidad", rhsNcurEspecialidad), lhsNcurEspecialidad, rhsNcurEspecialidad)) {
                return false;
            }
        }
        {
            Long lhsNcurCurEduc;
            lhsNcurCurEduc = this.getNcurCurEduc();
            Long rhsNcurCurEduc;
            rhsNcurCurEduc = that.getNcurCurEduc();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCurEduc", lhsNcurCurEduc), LocatorUtils.property(thatLocator, "ncurCurEduc", rhsNcurCurEduc), lhsNcurCurEduc, rhsNcurCurEduc)) {
                return false;
            }
        }
        {
            String lhsNcurCodMatri;
            lhsNcurCodMatri = this.getNcurCodMatri();
            String rhsNcurCodMatri;
            rhsNcurCodMatri = that.getNcurCodMatri();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCodMatri", lhsNcurCodMatri), LocatorUtils.property(thatLocator, "ncurCodMatri", rhsNcurCodMatri), lhsNcurCodMatri, rhsNcurCodMatri)) {
                return false;
            }
        }
        {
            String lhsNcurIndCompleto;
            lhsNcurIndCompleto = this.getNcurIndCompleto();
            String rhsNcurIndCompleto;
            rhsNcurIndCompleto = that.getNcurIndCompleto();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurIndCompleto", lhsNcurIndCompleto), LocatorUtils.property(thatLocator, "ncurIndCompleto", rhsNcurIndCompleto), lhsNcurIndCompleto, rhsNcurIndCompleto)) {
                return false;
            }
        }
        {
            Long lhsNcurCurEducCompl;
            lhsNcurCurEducCompl = this.getNcurCurEducCompl();
            Long rhsNcurCurEducCompl;
            rhsNcurCurEducCompl = that.getNcurCurEducCompl();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCurEducCompl", lhsNcurCurEducCompl), LocatorUtils.property(thatLocator, "ncurCurEducCompl", rhsNcurCurEducCompl), lhsNcurCurEducCompl, rhsNcurCurEducCompl)) {
                return false;
            }
        }
        {
            Float lhsNcurTotalCredMatr;
            lhsNcurTotalCredMatr = this.getNcurTotalCredMatr();
            Float rhsNcurTotalCredMatr;
            rhsNcurTotalCredMatr = that.getNcurTotalCredMatr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurTotalCredMatr", lhsNcurTotalCredMatr), LocatorUtils.property(thatLocator, "ncurTotalCredMatr", rhsNcurTotalCredMatr), lhsNcurTotalCredMatr, rhsNcurTotalCredMatr)) {
                return false;
            }
        }
        {
            String lhsNcurCertifica;
            lhsNcurCertifica = this.getNcurCertifica();
            String rhsNcurCertifica;
            rhsNcurCertifica = that.getNcurCertifica();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCertifica", lhsNcurCertifica), LocatorUtils.property(thatLocator, "ncurCertifica", rhsNcurCertifica), lhsNcurCertifica, rhsNcurCertifica)) {
                return false;
            }
        }
        {
            String lhsNcurLocCertif;
            lhsNcurLocCertif = this.getNcurLocCertif();
            String rhsNcurLocCertif;
            rhsNcurLocCertif = that.getNcurLocCertif();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurLocCertif", lhsNcurLocCertif), LocatorUtils.property(thatLocator, "ncurLocCertif", rhsNcurLocCertif), lhsNcurLocCertif, rhsNcurLocCertif)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsNcurFechaCertif;
            lhsNcurFechaCertif = this.getNcurFechaCertif();
            XMLGregorianCalendar rhsNcurFechaCertif;
            rhsNcurFechaCertif = that.getNcurFechaCertif();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurFechaCertif", lhsNcurFechaCertif), LocatorUtils.property(thatLocator, "ncurFechaCertif", rhsNcurFechaCertif), lhsNcurFechaCertif, rhsNcurFechaCertif)) {
                return false;
            }
        }
        {
            String lhsNcurIndMatParcial;
            lhsNcurIndMatParcial = this.getNcurIndMatParcial();
            String rhsNcurIndMatParcial;
            rhsNcurIndMatParcial = that.getNcurIndMatParcial();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurIndMatParcial", lhsNcurIndMatParcial), LocatorUtils.property(thatLocator, "ncurIndMatParcial", rhsNcurIndMatParcial), lhsNcurIndMatParcial, rhsNcurIndMatParcial)) {
                return false;
            }
        }
        {
            Long lhsNcurIdPeriodo;
            lhsNcurIdPeriodo = this.getNcurIdPeriodo();
            Long rhsNcurIdPeriodo;
            rhsNcurIdPeriodo = that.getNcurIdPeriodo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurIdPeriodo", lhsNcurIdPeriodo), LocatorUtils.property(thatLocator, "ncurIdPeriodo", rhsNcurIdPeriodo), lhsNcurIdPeriodo, rhsNcurIdPeriodo)) {
                return false;
            }
        }
        {
            String lhsNcurIndEstudiosFinales;
            lhsNcurIndEstudiosFinales = this.getNcurIndEstudiosFinales();
            String rhsNcurIndEstudiosFinales;
            rhsNcurIndEstudiosFinales = that.getNcurIndEstudiosFinales();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurIndEstudiosFinales", lhsNcurIndEstudiosFinales), LocatorUtils.property(thatLocator, "ncurIndEstudiosFinales", rhsNcurIndEstudiosFinales), lhsNcurIndEstudiosFinales, rhsNcurIndEstudiosFinales)) {
                return false;
            }
        }
        {
            String lhsNcurCreLimUniv;
            lhsNcurCreLimUniv = this.getNcurCreLimUniv();
            String rhsNcurCreLimUniv;
            rhsNcurCreLimUniv = that.getNcurCreLimUniv();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurCreLimUniv", lhsNcurCreLimUniv), LocatorUtils.property(thatLocator, "ncurCreLimUniv", rhsNcurCreLimUniv), lhsNcurCreLimUniv, rhsNcurCreLimUniv)) {
                return false;
            }
        }
        {
            Long lhsNcurTotalCrePlanEst;
            lhsNcurTotalCrePlanEst = this.getNcurTotalCrePlanEst();
            Long rhsNcurTotalCrePlanEst;
            rhsNcurTotalCrePlanEst = that.getNcurTotalCrePlanEst();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurTotalCrePlanEst", lhsNcurTotalCrePlanEst), LocatorUtils.property(thatLocator, "ncurTotalCrePlanEst", rhsNcurTotalCrePlanEst), lhsNcurTotalCrePlanEst, rhsNcurTotalCrePlanEst)) {
                return false;
            }
        }
        {
            Long lhsNcurAnosPlanEst;
            lhsNcurAnosPlanEst = this.getNcurAnosPlanEst();
            Long rhsNcurAnosPlanEst;
            rhsNcurAnosPlanEst = that.getNcurAnosPlanEst();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ncurAnosPlanEst", lhsNcurAnosPlanEst), LocatorUtils.property(thatLocator, "ncurAnosPlanEst", rhsNcurAnosPlanEst), lhsNcurAnosPlanEst, rhsNcurAnosPlanEst)) {
                return false;
            }
        }
        {
            List<AcademicosNCursoType> lhsAcademicosNCurso;
            lhsAcademicosNCurso = (((this.academicosNCurso!= null)&&(!this.academicosNCurso.isEmpty()))?this.getAcademicosNCurso():null);
            List<AcademicosNCursoType> rhsAcademicosNCurso;
            rhsAcademicosNCurso = (((that.academicosNCurso!= null)&&(!that.academicosNCurso.isEmpty()))?that.getAcademicosNCurso():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "academicosNCurso", lhsAcademicosNCurso), LocatorUtils.property(thatLocator, "academicosNCurso", rhsAcademicosNCurso), lhsAcademicosNCurso, rhsAcademicosNCurso)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theNcurTipcentCtipocentro;
            theNcurTipcentCtipocentro = this.getNcurTipcentCtipocentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurTipcentCtipocentro", theNcurTipcentCtipocentro), currentHashCode, theNcurTipcentCtipocentro);
        }
        {
            String theNcurCcentro;
            theNcurCcentro = this.getNcurCcentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCcentro", theNcurCcentro), currentHashCode, theNcurCcentro);
        }
        {
            String theNcurCodTcentro;
            theNcurCodTcentro = this.getNcurCodTcentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCodTcentro", theNcurCodTcentro), currentHashCode, theNcurCodTcentro);
        }
        {
            String theNcurEstudios;
            theNcurEstudios = this.getNcurEstudios();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurEstudios", theNcurEstudios), currentHashCode, theNcurEstudios);
        }
        {
            String theNcurEspecialidad;
            theNcurEspecialidad = this.getNcurEspecialidad();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurEspecialidad", theNcurEspecialidad), currentHashCode, theNcurEspecialidad);
        }
        {
            Long theNcurCurEduc;
            theNcurCurEduc = this.getNcurCurEduc();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCurEduc", theNcurCurEduc), currentHashCode, theNcurCurEduc);
        }
        {
            String theNcurCodMatri;
            theNcurCodMatri = this.getNcurCodMatri();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCodMatri", theNcurCodMatri), currentHashCode, theNcurCodMatri);
        }
        {
            String theNcurIndCompleto;
            theNcurIndCompleto = this.getNcurIndCompleto();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurIndCompleto", theNcurIndCompleto), currentHashCode, theNcurIndCompleto);
        }
        {
            Long theNcurCurEducCompl;
            theNcurCurEducCompl = this.getNcurCurEducCompl();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCurEducCompl", theNcurCurEducCompl), currentHashCode, theNcurCurEducCompl);
        }
        {
            Float theNcurTotalCredMatr;
            theNcurTotalCredMatr = this.getNcurTotalCredMatr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurTotalCredMatr", theNcurTotalCredMatr), currentHashCode, theNcurTotalCredMatr);
        }
        {
            String theNcurCertifica;
            theNcurCertifica = this.getNcurCertifica();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCertifica", theNcurCertifica), currentHashCode, theNcurCertifica);
        }
        {
            String theNcurLocCertif;
            theNcurLocCertif = this.getNcurLocCertif();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurLocCertif", theNcurLocCertif), currentHashCode, theNcurLocCertif);
        }
        {
            XMLGregorianCalendar theNcurFechaCertif;
            theNcurFechaCertif = this.getNcurFechaCertif();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurFechaCertif", theNcurFechaCertif), currentHashCode, theNcurFechaCertif);
        }
        {
            String theNcurIndMatParcial;
            theNcurIndMatParcial = this.getNcurIndMatParcial();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurIndMatParcial", theNcurIndMatParcial), currentHashCode, theNcurIndMatParcial);
        }
        {
            Long theNcurIdPeriodo;
            theNcurIdPeriodo = this.getNcurIdPeriodo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurIdPeriodo", theNcurIdPeriodo), currentHashCode, theNcurIdPeriodo);
        }
        {
            String theNcurIndEstudiosFinales;
            theNcurIndEstudiosFinales = this.getNcurIndEstudiosFinales();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurIndEstudiosFinales", theNcurIndEstudiosFinales), currentHashCode, theNcurIndEstudiosFinales);
        }
        {
            String theNcurCreLimUniv;
            theNcurCreLimUniv = this.getNcurCreLimUniv();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurCreLimUniv", theNcurCreLimUniv), currentHashCode, theNcurCreLimUniv);
        }
        {
            Long theNcurTotalCrePlanEst;
            theNcurTotalCrePlanEst = this.getNcurTotalCrePlanEst();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurTotalCrePlanEst", theNcurTotalCrePlanEst), currentHashCode, theNcurTotalCrePlanEst);
        }
        {
            Long theNcurAnosPlanEst;
            theNcurAnosPlanEst = this.getNcurAnosPlanEst();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ncurAnosPlanEst", theNcurAnosPlanEst), currentHashCode, theNcurAnosPlanEst);
        }
        {
            List<AcademicosNCursoType> theAcademicosNCurso;
            theAcademicosNCurso = (((this.academicosNCurso!= null)&&(!this.academicosNCurso.isEmpty()))?this.getAcademicosNCurso():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "academicosNCurso", theAcademicosNCurso), currentHashCode, theAcademicosNCurso);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
