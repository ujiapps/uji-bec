package es.uji.apps.bec.dao.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.QBeca;
import es.uji.apps.bec.model.QCursoAcademico;
import es.uji.apps.bec.model.QEstado;
import es.uji.apps.bec.model.QProceso;
import es.uji.apps.bec.model.QSolicitante;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstadoDAODatabaseImpl extends BaseDAODatabaseImpl implements EstadoDAO
{
    private QEstado estado = QEstado.estado;
    private QBeca beca = QBeca.beca;
    private QProceso proceso = QProceso.proceso;
    private QSolicitante solicitante = QSolicitante.solicitante;
    private QCursoAcademico cursoAcademico = QCursoAcademico.cursoAcademico;

    @Override
    public List<Estado> getEstados()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estado).orderBy(estado.orden.asc());
        return query.list(estado);
    }

    @Override
    public Estado getEstadoById(Long estadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estado).where(estado.id.eq(estadoId));
        return query.uniqueResult(estado);
    }

    @Override
    public List<Resumen> getResumenEstadosPorConvocatoria(Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estado).join(estado.becas, beca).join(beca.solicitante, solicitante)
                .join(solicitante.cursoAcademico, cursoAcademico)
                .where(beca.convocatoria.id.eq(convocatoriaId).and(cursoAcademico.activo.isTrue()))
                .groupBy(estado.id, estado.nombre);
        List<Tuple> listaQuery = query.list(estado.nombre, estado.count());
        return convierteResultadoAResumen(listaQuery);
    }

    @Override
    public List<Resumen> getResumenEstadosPorTanda(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estado).join(estado.becas, beca).where(beca.tanda.id.eq(tandaId))
                .groupBy(estado.id, estado.nombre).orderBy(estado.nombre.asc());
        List<Tuple> listaQuery = query.list(estado.nombre, estado.count());
        return convierteResultadoAResumen(listaQuery);
    }

    @Override
    @Transactional
    public void deleteEstado(Long estadoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, estado);
        delete.where(estado.id.eq(estadoId)).execute();
    }

    @Override
    public List<Resumen> getResumenProcesosPorConvocatoria(Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(proceso).join(proceso.becas, beca).join(beca.solicitante, solicitante)
                .join(solicitante.cursoAcademico, cursoAcademico)
                .where(beca.convocatoria.id.eq(convocatoriaId).and(cursoAcademico.activo.isTrue()))
                .groupBy(proceso.id, proceso.nombre);
        List<Tuple> listaQuery = query.list(proceso.nombre, proceso.count());
        return convierteResultadoAResumen(listaQuery);
    }

    private List<Resumen> convierteResultadoAResumen(List<Tuple> listaQuery)
    {
        List<Resumen> listaResultado = new ArrayList<>();
        for (Tuple columna : listaQuery)
        {
            String nombre = columna.toArray()[0].toString();
            Long count = (Long) columna.toArray()[1];

            listaResultado.add(creaResumen(nombre, count));
        }
        return listaResultado;
    }

    private Resumen creaResumen(String etiqueta, Long valor)
    {
        Resumen resumen = new Resumen();
        resumen.setEtiqueta(etiqueta);
        resumen.setValor(valor);
        return resumen;
    }
}
