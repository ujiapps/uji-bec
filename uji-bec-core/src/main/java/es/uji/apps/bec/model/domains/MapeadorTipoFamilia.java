package es.uji.apps.bec.model.domains;

import es.uji.apps.bec.util.Mapeador;

import java.util.HashMap;
import java.util.Map;

public class MapeadorTipoFamilia extends Mapeador<Long, Object>
{
    public MapeadorTipoFamilia()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "0");
        valores.put(CodigoOrganismo.CONSELLERIA, "0");
        add(new Long(1L), valores);
        valores.put(CodigoOrganismo.MINISTERIO, "1");
        valores.put(CodigoOrganismo.CONSELLERIA, "1");
        add(new Long(2L), valores);
        valores.put(CodigoOrganismo.MINISTERIO, "2");
        valores.put(CodigoOrganismo.CONSELLERIA, "2");
        add(new Long(3L), valores);
    }
}