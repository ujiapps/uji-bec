package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class CursoAcademicoException extends GeneralBECException
{
    public CursoAcademicoException()
    {
        super("El paràmetres del curs acadèmic no són correctes.");
    }

    public CursoAcademicoException(String message)
    {
        super(message);
    }
}
