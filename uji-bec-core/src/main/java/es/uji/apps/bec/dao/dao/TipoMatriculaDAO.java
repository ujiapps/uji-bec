package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoMatricula;
import es.uji.commons.db.BaseDAO;

public interface TipoMatriculaDAO extends BaseDAO
{
    List<TipoMatricula> getTiposMatriculas();

    List<TipoMatricula> getTipoMatriculaById(Long tipoMatriculaId);
}
