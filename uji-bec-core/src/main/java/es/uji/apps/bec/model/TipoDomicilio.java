package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoDomicilioDAO;

/**
 * The persistent class for the BC2_TIPOS_DOMICILIOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TIPOS_DOMICILIOS")
public class TipoDomicilio implements Serializable
{
    @Id
    private Long id;
    private String nombre;
    private Long orden;

    // bi-directional many-to-one association to Domicilio
    @OneToMany(mappedBy = "tipoDomicilio")
    private Set<Domicilio> domicilios;

    private static TipoDomicilioDAO tipoDomicilioDAO;

    @Autowired
    public void setTipoDomicilioDAO(TipoDomicilioDAO tipoDomicilioDAO)
    {
        TipoDomicilio.tipoDomicilioDAO = tipoDomicilioDAO;
    }

    public TipoDomicilio()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }

    public static TipoDomicilio getTipoDomicilioById(Long tipoDomicilioId)
    {
        return tipoDomicilioDAO.getTipoDomicilioById(tipoDomicilioId).get(0);
    }
}