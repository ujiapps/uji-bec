//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 01:14:30 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.denegaciones;


import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDate;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for PagosType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PagosType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}PagoIdOrdenPago" minOccurs="0"/>
 *         &lt;element ref="{}PagoSecuenciaPago" minOccurs="0"/>
 *         &lt;element ref="{}PagoImpTotal" minOccurs="0"/>
 *         &lt;element ref="{}PagoFechaEnvTeso" minOccurs="0"/>
 *         &lt;element ref="{}PagoIndAnulado" minOccurs="0"/>
 *         &lt;element ref="{}PagoDesglose" minOccurs="0"/>
 *         &lt;element ref="{}PagoClave" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagosType", propOrder = {
    "pagoIdOrdenPago",
    "pagoSecuenciaPago",
    "pagoImpTotal",
    "pagoFechaEnvTeso",
    "pagoIndAnulado",
    "pagoDesglose",
    "pagoClave"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.denegaciones.PagosType")
@Table(name = "PAGOSTYPE", schema = "UJI_BECAS_WS_DENEGACIONES")
@Inheritance(strategy = InheritanceType.JOINED)
public class PagosType
    implements Equals, HashCode
{

    @XmlElement(name = "PagoIdOrdenPago")
    protected Long pagoIdOrdenPago;
    @XmlElement(name = "PagoSecuenciaPago")
    protected Long pagoSecuenciaPago;
    @XmlElement(name = "PagoImpTotal")
    protected Float pagoImpTotal;
    @XmlElement(name = "PagoFechaEnvTeso")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar pagoFechaEnvTeso;
    @XmlElement(name = "PagoIndAnulado")
    protected String pagoIndAnulado;
    @XmlElement(name = "PagoDesglose")
    protected Long pagoDesglose;
    @XmlElement(name = "PagoClave")
    protected Long pagoClave;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the pagoIdOrdenPago property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "PAGOIDORDENPAGO", precision = 8, scale = 0)
    public Long getPagoIdOrdenPago() {
        return pagoIdOrdenPago;
    }

    /**
     * Sets the value of the pagoIdOrdenPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagoIdOrdenPago(Long value) {
        this.pagoIdOrdenPago = value;
    }

    /**
     * Gets the value of the pagoSecuenciaPago property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "PAGOSECUENCIAPAGO", scale = 0)
    public Long getPagoSecuenciaPago() {
        return pagoSecuenciaPago;
    }

    /**
     * Sets the value of the pagoSecuenciaPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagoSecuenciaPago(Long value) {
        this.pagoSecuenciaPago = value;
    }

    /**
     * Gets the value of the pagoImpTotal property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "PAGOIMPTOTAL", precision = 8, scale = 2)
    public Float getPagoImpTotal() {
        return pagoImpTotal;
    }

    /**
     * Sets the value of the pagoImpTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPagoImpTotal(Float value) {
        this.pagoImpTotal = value;
    }

    /**
     * Gets the value of the pagoFechaEnvTeso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getPagoFechaEnvTeso() {
        return pagoFechaEnvTeso;
    }

    /**
     * Sets the value of the pagoFechaEnvTeso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPagoFechaEnvTeso(XMLGregorianCalendar value) {
        this.pagoFechaEnvTeso = value;
    }

    /**
     * Gets the value of the pagoIndAnulado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "PAGOINDANULADO", length = 1)
    public String getPagoIndAnulado() {
        return pagoIndAnulado;
    }

    /**
     * Sets the value of the pagoIndAnulado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPagoIndAnulado(String value) {
        this.pagoIndAnulado = value;
    }

    /**
     * Gets the value of the pagoDesglose property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "PAGODESGLOSE", precision = 5, scale = 0)
    public Long getPagoDesglose() {
        return pagoDesglose;
    }

    /**
     * Sets the value of the pagoDesglose property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagoDesglose(Long value) {
        this.pagoDesglose = value;
    }

    /**
     * Gets the value of the pagoClave property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "PAGOCLAVE", precision = 6, scale = 0)
    public Long getPagoClave() {
        return pagoClave;
    }

    /**
     * Sets the value of the pagoClave property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagoClave(Long value) {
        this.pagoClave = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    @Basic
    @Column(name = "PAGOFECHAENVTESOITEM")
    @Temporal(TemporalType.DATE)
    public Date getPagoFechaEnvTesoItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getPagoFechaEnvTeso());
    }

    public void setPagoFechaEnvTesoItem(Date target) {
        setPagoFechaEnvTeso(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PagosType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PagosType that = ((PagosType) object);
        {
            Long lhsPagoIdOrdenPago;
            lhsPagoIdOrdenPago = this.getPagoIdOrdenPago();
            Long rhsPagoIdOrdenPago;
            rhsPagoIdOrdenPago = that.getPagoIdOrdenPago();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoIdOrdenPago", lhsPagoIdOrdenPago), LocatorUtils.property(thatLocator, "pagoIdOrdenPago", rhsPagoIdOrdenPago), lhsPagoIdOrdenPago, rhsPagoIdOrdenPago)) {
                return false;
            }
        }
        {
            Long lhsPagoSecuenciaPago;
            lhsPagoSecuenciaPago = this.getPagoSecuenciaPago();
            Long rhsPagoSecuenciaPago;
            rhsPagoSecuenciaPago = that.getPagoSecuenciaPago();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoSecuenciaPago", lhsPagoSecuenciaPago), LocatorUtils.property(thatLocator, "pagoSecuenciaPago", rhsPagoSecuenciaPago), lhsPagoSecuenciaPago, rhsPagoSecuenciaPago)) {
                return false;
            }
        }
        {
            Float lhsPagoImpTotal;
            lhsPagoImpTotal = this.getPagoImpTotal();
            Float rhsPagoImpTotal;
            rhsPagoImpTotal = that.getPagoImpTotal();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoImpTotal", lhsPagoImpTotal), LocatorUtils.property(thatLocator, "pagoImpTotal", rhsPagoImpTotal), lhsPagoImpTotal, rhsPagoImpTotal)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsPagoFechaEnvTeso;
            lhsPagoFechaEnvTeso = this.getPagoFechaEnvTeso();
            XMLGregorianCalendar rhsPagoFechaEnvTeso;
            rhsPagoFechaEnvTeso = that.getPagoFechaEnvTeso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoFechaEnvTeso", lhsPagoFechaEnvTeso), LocatorUtils.property(thatLocator, "pagoFechaEnvTeso", rhsPagoFechaEnvTeso), lhsPagoFechaEnvTeso, rhsPagoFechaEnvTeso)) {
                return false;
            }
        }
        {
            String lhsPagoIndAnulado;
            lhsPagoIndAnulado = this.getPagoIndAnulado();
            String rhsPagoIndAnulado;
            rhsPagoIndAnulado = that.getPagoIndAnulado();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoIndAnulado", lhsPagoIndAnulado), LocatorUtils.property(thatLocator, "pagoIndAnulado", rhsPagoIndAnulado), lhsPagoIndAnulado, rhsPagoIndAnulado)) {
                return false;
            }
        }
        {
            Long lhsPagoDesglose;
            lhsPagoDesglose = this.getPagoDesglose();
            Long rhsPagoDesglose;
            rhsPagoDesglose = that.getPagoDesglose();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoDesglose", lhsPagoDesglose), LocatorUtils.property(thatLocator, "pagoDesglose", rhsPagoDesglose), lhsPagoDesglose, rhsPagoDesglose)) {
                return false;
            }
        }
        {
            Long lhsPagoClave;
            lhsPagoClave = this.getPagoClave();
            Long rhsPagoClave;
            rhsPagoClave = that.getPagoClave();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pagoClave", lhsPagoClave), LocatorUtils.property(thatLocator, "pagoClave", rhsPagoClave), lhsPagoClave, rhsPagoClave)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long thePagoIdOrdenPago;
            thePagoIdOrdenPago = this.getPagoIdOrdenPago();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoIdOrdenPago", thePagoIdOrdenPago), currentHashCode, thePagoIdOrdenPago);
        }
        {
            Long thePagoSecuenciaPago;
            thePagoSecuenciaPago = this.getPagoSecuenciaPago();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoSecuenciaPago", thePagoSecuenciaPago), currentHashCode, thePagoSecuenciaPago);
        }
        {
            Float thePagoImpTotal;
            thePagoImpTotal = this.getPagoImpTotal();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoImpTotal", thePagoImpTotal), currentHashCode, thePagoImpTotal);
        }
        {
            XMLGregorianCalendar thePagoFechaEnvTeso;
            thePagoFechaEnvTeso = this.getPagoFechaEnvTeso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoFechaEnvTeso", thePagoFechaEnvTeso), currentHashCode, thePagoFechaEnvTeso);
        }
        {
            String thePagoIndAnulado;
            thePagoIndAnulado = this.getPagoIndAnulado();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoIndAnulado", thePagoIndAnulado), currentHashCode, thePagoIndAnulado);
        }
        {
            Long thePagoDesglose;
            thePagoDesglose = this.getPagoDesglose();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoDesglose", thePagoDesglose), currentHashCode, thePagoDesglose);
        }
        {
            Long thePagoClave;
            thePagoClave = this.getPagoClave();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pagoClave", thePagoClave), currentHashCode, thePagoClave);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
