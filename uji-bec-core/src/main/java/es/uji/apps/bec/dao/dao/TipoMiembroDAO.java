package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoMiembro;
import es.uji.commons.db.BaseDAO;

public interface TipoMiembroDAO extends BaseDAO
{
    List<TipoMiembro> getTiposMiembros();

    TipoMiembro getTipoMiembroById(Long tipoMiembroId);

    void deleteTipoMiembro(Long tipoMiembroId);
}
