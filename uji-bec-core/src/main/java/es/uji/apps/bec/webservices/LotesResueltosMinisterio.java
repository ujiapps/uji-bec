package es.uji.apps.bec.webservices;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.dao.dao.BecaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.BecaEstadoResolucion;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Denegacion;
import es.uji.apps.bec.model.Economico;
import es.uji.apps.bec.model.HistoricoBeca;
import es.uji.apps.bec.model.LoteMinisterio;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.ResolucionAyuda;
import es.uji.apps.bec.model.ResolucionDenegacion;
import es.uji.apps.bec.model.ResolucionEconomico;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.model.domains.CodigoDenegacion;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.model.domains.CodigoProceso;
import es.uji.apps.bec.model.domains.EstadoLoteMinisterio;
import es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType;
import org.springframework.transaction.annotation.Transactional;

import static es.uji.commons.rest.xml.CommonExceptionMapper.log;

@Component
public class LotesResueltosMinisterio {
    private static final String BECA_ABONADA = "01";
    private static final String BECA_PAGADA = "15";
    private static final String BECA_PAGADA_PARCIAL = "41";
    private static final String BECA_DENEGADA = "10";
    private static final String BECA_EXCLUIDA = "14";

    private static final Long BECA_DENEGADA_UJI = 3L;
    private static final Long CONVOCATORIA_CONSELLERIA = 3L;
    public static final long ORDEN_UNO = 1L;

    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    public LotesResueltosMinisterio() {
    }

    public JAXBElement<MultienvioType> obtenerListaLotesResoluciones(Long cursoAca, String acronimoConvocatoria)
            throws ServiceException, JAXBException, WsDatosEnviadosParaBajarBecaErroneosException, IOException, SOAPException {
        return service.obtenerXmlPendientes(cursoAca, acronimoConvocatoria);
    }

    public String obtenerListaLotesResolucionesXML(Long cursoAca, String acronimoConvocatoria)
            throws IOException, SOAPException {
        return service.obtenerXmlPendientesString(cursoAca, acronimoConvocatoria);
    }

    public Long descargarLoteResoluciones(String lote, String convocatoria, String cursoAca)
            throws WsDatosEnviadosParaBajarBecaErroneosException, ServiceException, IOException, JAXBException, NumberFormatException, SOAPException {

        LoteMinisterio loteMinisterio = LoteMinisterio.getLoteByLoteIdAndCursoAca(Long.parseLong(lote), Long.parseLong(cursoAca));

        if (loteMinisterio.getLoteId() == null) {
            MultienvioType loteResoluciones = bajarMultiEnvioResoluciones(lote, convocatoria, cursoAca);

            if (loteResoluciones != null) {
                insertaLoteMinisterio(lote, convocatoria, cursoAca, loteMinisterio, loteResoluciones);
                return loteResoluciones.getHjid();
            }
        }

        return null;
    }

    @Transactional
    private MultienvioType bajarMultiEnvioResoluciones(String lote, String convocatoria, String cursoAca)
            throws IOException, WsDatosEnviadosParaBajarBecaErroneosException, SOAPException {

        MultienvioType loteResoluciones = service.getXml(lote, convocatoria, cursoAca);
        if (loteResoluciones != null) {
            becaDAO.insert(loteResoluciones);
        } else {
            log.info("El lote " + lote + " del curso " + cursoAca + " y convocatoria " + convocatoria + " no se encuentra resolución");
        }

        return loteResoluciones;
    }

    private void insertaLoteMinisterio(String lote, String convocatoria, String cursoAca,
                                       LoteMinisterio loteMinisterio, MultienvioType loteResoluciones)
            throws NumberFormatException {
        loteMinisterio.setConvocatoriaId(Convocatoria.getConvocatoriaByAcronimo(convocatoria).getId());
        loteMinisterio.setCursoAcademicoId(Long.parseLong(cursoAca));
        loteMinisterio.setEstado(EstadoLoteMinisterio.CARREGAT.name());
        loteMinisterio.setFecha(new Date());
        loteMinisterio.setLoteId(Long.parseLong(lote));
        loteMinisterio.setMultienvioId(loteResoluciones.getHjid());
        loteMinisterio.insert();
    }

    public void procesaLoteResoluciones(Long multiEnvioId, String usuario)
            throws BecaDenegacionDuplicadaException, BecaCuantiaDuplicadaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException, BecaCuantiaNoExisteException {
        List<BecaEstadoResolucion> listaResoluciones = BecaEstadoResolucion
                .getResolucionesByMultienvioId(multiEnvioId);

        Tanda tanda = null;

        for (BecaEstadoResolucion resolucion : listaResoluciones) {
            String estadoId = resolucion.getEstadoId();
            Long becaId = resolucion.getBecaId();
            Long archivoTemporal = resolucion.getArchivoTemporal();
            Beca beca = Beca.getBecaById(becaId);

            if (beca == null) {
                beca = Beca.getBecaByArchivoTemporal(archivoTemporal);
            }

            if (beca == null || estadoId == null) {
                continue;
            }

            if (beca.isEnProceso(CodigoProceso.ORDINARIO)) {
                if (BECA_DENEGADA.equals(estadoId) || BECA_EXCLUIDA.equals(estadoId)) {
                    beca.borraAyudas();
                    beca.borraDenegacionesOrdenUno();
                    insertaDenegacionesMec(beca, multiEnvioId);
                    if (BECA_DENEGADA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.DENEGADA.getId());
                    }
                    if (BECA_EXCLUIDA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.EXCLUIDA.getId());
                    }
                    beca.cambiaANoConcedida();
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }

                if (BECA_PAGADA.equals(estadoId) || BECA_PAGADA_PARCIAL.equals(estadoId) || BECA_ABONADA.equals(estadoId)) {
                    beca.borraAyudas();
                    beca.borraDenegacionesOrdenUno();
                    insertaAyudasMec(beca, multiEnvioId);
                    beca.cambiaEstado(CodigoEstado.CONCEDIDA.getId());
                    beca.cambiaAConcedida();

                    if (tanda == null) {
                        tanda = creaNuevaTandaParaDenegadas(beca, resolucion.getLote());
                    }
                    deniegaConselleria(beca, usuario, tanda);
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }
            }
            // becas denegadas que no estan conformes
            if (beca.isEnProceso(CodigoProceso.ALEGACION)) {
                if (BECA_DENEGADA.equals(estadoId) || BECA_EXCLUIDA.equals(estadoId)) {
                    beca.borraAyudas();
                    beca.borraDenegacionesOrdenDos();
                    insertaDenegacionesMec(beca, multiEnvioId);
                    if (BECA_DENEGADA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.DENEGADA.getId());
                    }
                    if (BECA_EXCLUIDA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.EXCLUIDA.getId());
                    }
                    beca.cambiaANoConcedida();
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }
                if (BECA_PAGADA.equals(estadoId) || BECA_PAGADA_PARCIAL.equals(estadoId) || BECA_ABONADA.equals(estadoId)) {
                    beca.borraAyudas();
                    insertaAyudasMec(beca, multiEnvioId);
                    beca.cambiaEstado(CodigoEstado.CONCEDIDA.getId());
                    beca.cambiaAConcedida();
                    deniegaConselleria(beca, usuario, tanda);
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }
            }
            // becas concedidas que piden mas pasta

            if (beca.isEnProceso(CodigoProceso.AUMENTO_CUANTIA)) {
                if (BECA_DENEGADA.equals(estadoId) || BECA_EXCLUIDA.equals(estadoId)) {
                    //     beca.borraAyudas();
                    beca.borraDenegacionesOrdenUno();
                    insertaDenegacionesMec(beca, multiEnvioId);
                    //       insertaAyudasMec(beca, multiEnvioId);
                    if (BECA_DENEGADA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.DENEGADA.getId());
                    }
                    if (BECA_EXCLUIDA.equals(estadoId)) {
                        beca.cambiaEstado(CodigoEstado.EXCLUIDA.getId());
                    }
                    beca.cambiaAConcedida();
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }

                if (BECA_PAGADA.equals(estadoId) || BECA_PAGADA_PARCIAL.equals(estadoId) || BECA_ABONADA.equals(estadoId)) {
                    beca.borraAyudas();
                    insertaAyudasMec(beca, multiEnvioId);
                    beca.cambiaEstado(CodigoEstado.CONCEDIDA.getId());
                    beca.cambiaAConcedida();
                    HistoricoBeca.insertHistoricoSiCambios(beca, null, usuario);
                }
            }

            importarEconomicos(resolucion, beca);
        }
        actualizaLoteMinisterioProcesado(multiEnvioId);
    }

    private void importarEconomicos(BecaEstadoResolucion resolucion, Beca beca) {
        if (resolucion.getEconomicosId() == null) {
            return;
        }

        ResolucionEconomico resolucionEconomico = ResolucionEconomico.getResolucionEconomicoById(resolucion.getEconomicosId());

        Solicitante solicitante = beca.getSolicitante();
        Economico economico = preparaEconomico(resolucionEconomico, solicitante);

        guardaEconomico(solicitante, economico);
    }

    private void guardaEconomico(Solicitante solicitante, Economico economico) {
        List<Economico> listaEconomicos = Economico.getEconomicoBySolicitanteId(solicitante.getId());
        if (listaEconomicos.size() > 0) {
            Economico economicoSolicitante = listaEconomicos.get(0);
            economico.setId(economicoSolicitante.getId());
            economico.updateEconomico();
        } else {
            economico.insert();
        }
    }

    private Economico preparaEconomico(ResolucionEconomico resolucionEconomico,
                                       Solicitante solicitante) {
        Economico economico = new Economico();
        economico.setSolicitante(solicitante);

        economico.setCapitalMobiliario(resolucionEconomico.getCapitalMobiliario());
        economico.setDeducciones(resolucionEconomico.getDeducciones());
        economico.setRenta(resolucionEconomico.getRenta());
        economico.setVolumenNegocio(resolucionEconomico.getVolumenNegocio());

        economico.setUmbralBecario(resolucionEconomico.getUmbralBecario());
        economico.setIndBecario(convierteSiNoEn01(resolucionEconomico.getIndBecario()));

        economico.setUmbralCompensa(resolucionEconomico.getUmbralCompensa());
        economico.setIndCompensa(convierteSiNoEn01(resolucionEconomico.getIndCompensa()));

        economico.setUmbralMovEspecial(resolucionEconomico.getUmbralMovEspecial());
        economico.setIndMovEspecial(convierteSiNoEn01(resolucionEconomico.getIndMovEspecial()));

        economico.setUmbralMovGeneral(resolucionEconomico.getUmbralMovGeneral());
        economico.setIndMovGeneral(convierteSiNoEn01(resolucionEconomico.getIndMovGeneral()));

        economico.setUmbralPatrimonio(resolucionEconomico.getUmbralPatrimonio());
        economico.setIndPatrimonio(convierteSiNoEn01(resolucionEconomico.getIndPatrimonio()));

        economico.setUmbralResidMov(resolucionEconomico.getUmbralResidMov());
        economico.setIndResidMov(convierteSiNoEn01(resolucionEconomico.getIndResidMov()));

        economico.setUmbralTasas(resolucionEconomico.getUmbralTasas());
        economico.setIndTasas(convierteSiNoEn01(resolucionEconomico.getIndTasas()));

        return economico;
    }

    private Integer convierteSiNoEn01(String valorSN) {
        Integer valor01 = null;
        if (valorSN == null) {
            return null;
        }
        if (valorSN.equals("S")) {
            valor01 = 1;
        }
        if (valorSN.equals("N")) {
            valor01 = 0;
        }
        return valor01;
    }

    private Tanda creaNuevaTandaParaDenegadas(Beca beca, String lote) {
        Tanda tanda = new Tanda();
        CursoAcademico cursoAca = beca.getSolicitante().getCursoAcademico();
        Long siguienteTandaId = Tanda.getMaximoTandaIdByCursoAca(cursoAca) + 1;

        tanda.setActiva(false);
        tanda.setConvocatoria(Convocatoria.getConvocatoriaById(CONVOCATORIA_CONSELLERIA));
        tanda.setCursoAcademico(cursoAca);
        tanda.setDescripcion("Denegades Conselleria per a beca Ministeri (lot - " + lote + ")");
        tanda.setTandaId(siguienteTandaId);

        return tanda.insert();
    }

    private void actualizaLoteMinisterioProcesado(Long multiEnvioId) {
        LoteMinisterio loteMinisterio = LoteMinisterio.getLoteByMultienvioId(multiEnvioId);

        if (loteMinisterio != null) {
            loteMinisterio.setEstado(EstadoLoteMinisterio.PROCESSAT.name());
            loteMinisterio.update();
        }
    }

    private void insertaAyudasMec(Beca beca, Long multiEnvioId)
            throws BecaCuantiaDuplicadaException, BecaCuantiaNoExisteException {
        List<ResolucionAyuda> listaAyudas = ResolucionAyuda.getAyudasByMultienvioAndBeca(multiEnvioId, beca.getId());

        for (ResolucionAyuda ayuda : listaAyudas) {

            if (ayuda.getCuantiaId() == null) {
                throw new BecaCuantiaNoExisteException("No existeix la quantia amb codi: " + ayuda.getTipoayuda() + ayuda.getCodayuda() + ". És necessari donar-la d'alta");
            }

            CuantiaBeca cuantiaBeca = new CuantiaBeca();
            cuantiaBeca.setBeca(beca);
            cuantiaBeca.setImporte(ayuda.getImporte());
            cuantiaBeca.setCuantia(Cuantia.getCuantiaById(ayuda.getCuantiaId()));
            cuantiaBeca.insert();
        }
    }

    private void insertaDenegacionesMec(Beca beca, Long multiEnvioId)
            throws BecaDenegacionDuplicadaException {
        List<ResolucionDenegacion> listaDenegaciones = ResolucionDenegacion
                .getDenegacionesByMultienvioAndBeca(multiEnvioId, beca.getId());

        for (ResolucionDenegacion denegacion : listaDenegaciones) {
            BecaDenegacion becaDenegacion = new BecaDenegacion();
            becaDenegacion.setBeca(beca);
            Long orden = beca.calculaOrdenDenegacion();
            becaDenegacion.setOrdenDenegacion(orden);

            if (denegacion.getDenId() == null) {
                Denegacion nuevaDenegacion = new Denegacion();
                nuevaDenegacion.setOrganismo(Organismo.getOrganismoById(CodigoOrganismo.MINISTERIO
                        .getId()));
                nuevaDenegacion.setNombre("NO EXISTEIX DESCRIPCIÓ. BAIXADA DEL MINISTERI.");
                nuevaDenegacion.setActiva(true);
                nuevaDenegacion.setEstado(denegacion.getEstadoId());
                nuevaDenegacion.setCausa(denegacion.getCausa());
                nuevaDenegacion.setSubCausa(denegacion.getSubcausa());

                Denegacion denegacionInsertada = nuevaDenegacion.insert();
                becaDenegacion.setDenegacion(denegacionInsertada);
            } else {
                becaDenegacion.setDenegacion(Denegacion.getDenegacionById(denegacion.getDenId()));
            }

            try {
                becaDenegacion.insert();
            } catch (Exception e) {
                System.out.println("Error insertando denegació " + denegacion.getCausa() + "-" + denegacion.getSubcausa() + " per a la beca " + beca.getId());
            }
        }
    }

    private void deniegaConselleria(Beca beca, String usuario, Tanda tanda)
            throws BecaDenegacionDuplicadaException, ConvocatoriaDeTandaYDeBecaDiferentesException {
        if (beca.getFechaNotificacion() != null) {
            return;
        }

        Long solicitanteId = beca.getSolicitante().getId();
        Long estudioId = beca.getEstudio().getId();
        Beca becaConselleria = Beca.getBeca(solicitanteId, CONVOCATORIA_CONSELLERIA, estudioId);

        if (becaConselleria == null) {
            return;
        }
        if (becaConselleria.isTieneCreditos2a()) {
            return;
        }

        becaConselleria.borraAyudas();
        becaConselleria.borraDenegacionesOrdenUno();
        becaConselleria.insertaDenegacion(CodigoDenegacion.TENER_BECA_MEC.getId(), ORDEN_UNO);
        becaConselleria.cambiaEstado(BECA_DENEGADA_UJI);
        becaConselleria.cambiaTanda(tanda);

        HistoricoBeca.insertHistoricoSiCambios(becaConselleria, null, usuario);
    }
}
