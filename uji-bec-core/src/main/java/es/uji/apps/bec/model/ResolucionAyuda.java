package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ResolucionAyudaDAO;

/**
 * The persistent class for the BC2_V_RESOL_AYUDAS database table.
 * 
 */
@Component
@Entity
@Table(name = "BC2_V_RESOL_AYUDAS")
public class ResolucionAyuda implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "ARCHIVO_TEMPORAL")
    private Long archivoTemporal;

    @Column(name = "BECA_ID")
    private Long becaId;

    private String codayuda;

    private String convocatoria;

    private String cuantia;

    @Column(name = "CUANTIA_ID")
    private Long cuantiaId;

    @Column(name = "CURSO_ACADEMICO")
    private String cursoAcademico;

    private String estado;

    @Column(name = "ESTADO_ID")
    private String estadoId;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ESTADO")
    private Date fechaEstado;

    private String lote;

    @Column(name = "MULTIENVIO_ID")
    private Long multienvioId;

    private String nif;

    @Column(name = "SOLICITUD_ID")
    private Long solicitudId;

    private String tanda;

    private String tipoayuda;
    
    private Float importe;

    private static ResolucionAyudaDAO resolucionAyudaDAO;

    @Autowired
    public void setResolucionAyudaDAO(ResolucionAyudaDAO resolucionAyudaDAO)
    {
        ResolucionAyuda.resolucionAyudaDAO = resolucionAyudaDAO;
    }

    public ResolucionAyuda()
    {
    }

    public Long getArchivoTemporal()
    {
        return this.archivoTemporal;
    }

    public void setArchivoTemporal(Long archivoTemporal)
    {
        this.archivoTemporal = archivoTemporal;
    }

    public Long getBecaId()
    {
        return this.becaId;
    }

    public void setBecaId(Long becaId)
    {
        this.becaId = becaId;
    }

    public String getCodayuda()
    {
        return this.codayuda;
    }

    public void setCodayuda(String codayuda)
    {
        this.codayuda = codayuda;
    }

    public String getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(String convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public String getCuantia()
    {
        return this.cuantia;
    }

    public void setCuantia(String cuantia)
    {
        this.cuantia = cuantia;
    }

    public Long getCuantiaId()
    {
        return this.cuantiaId;
    }

    public void setCuantiaId(Long cuantiaId)
    {
        this.cuantiaId = cuantiaId;
    }

    public String getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(String cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(String estadoId)
    {
        this.estadoId = estadoId;
    }

    public Date getFechaEstado()
    {
        return this.fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado)
    {
        this.fechaEstado = fechaEstado;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLote()
    {
        return this.lote;
    }

    public void setLote(String lote)
    {
        this.lote = lote;
    }

    public Long getMultienvioId()
    {
        return this.multienvioId;
    }

    public void setMultienvioId(Long multienvioId)
    {
        this.multienvioId = multienvioId;
    }

    public String getNif()
    {
        return this.nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public Long getSolicitudId()
    {
        return this.solicitudId;
    }

    public void setSolicitudId(Long solicitudId)
    {
        this.solicitudId = solicitudId;
    }

    public String getTanda()
    {
        return this.tanda;
    }

    public void setTanda(String tanda)
    {
        this.tanda = tanda;
    }

    public String getTipoayuda()
    {
        return this.tipoayuda;
    }

    public void setTipoayuda(String tipoayuda)
    {
        this.tipoayuda = tipoayuda;
    }

    public Float getImporte()
    {
        return importe;
    }

    public void setImporte(Float importe)
    {
        this.importe = importe;
    }

    public static List<ResolucionAyuda> getAyudasByMultienvioAndBeca(Long multiEnvioId, Long becaId)
    {
        return resolucionAyudaDAO.getAyudasByMultienvioAndBeca(multiEnvioId, becaId);
    }

}