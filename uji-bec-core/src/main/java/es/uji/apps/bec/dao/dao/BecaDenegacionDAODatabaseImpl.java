package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.QBecaDenegacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class BecaDenegacionDAODatabaseImpl extends BaseDAODatabaseImpl implements BecaDenegacionDAO
{
    @Override
    public List<BecaDenegacion> getDenegacionesByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBecaDenegacion becaDenegacion = QBecaDenegacion.becaDenegacion;

        query.from(becaDenegacion).join(becaDenegacion.denegacion)
                .where(becaDenegacion.beca.id.eq(becaId))
                .orderBy(becaDenegacion.ordenDenegacion.asc());

        return query.list(becaDenegacion);
    }

    @Override
    public List<BecaDenegacion> getDenegacionesByBecaIdAndOrden(Long becaId, Long orden)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBecaDenegacion becaDenegacion = QBecaDenegacion.becaDenegacion;

        query.from(becaDenegacion).join(becaDenegacion.denegacion)
                .where(becaDenegacion.beca.id.eq(becaId)
                        .and(becaDenegacion.ordenDenegacion.eq(orden)))
                .orderBy(becaDenegacion.ordenDenegacion.asc());

        return query.list(becaDenegacion);
    }
}
