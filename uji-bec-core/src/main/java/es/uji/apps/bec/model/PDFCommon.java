package es.uji.apps.bec.model;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PDFCommon {
    public static Map<String, Object> getInfoRecurso(Recurso recurso, Beca beca)
    {
        Map<String, Object> infoRecurso = new HashMap<String, Object>();

        Solicitante solicitante = beca.getSolicitante();

        List<CuantiaBeca> listaCuantiasBecas = CuantiaBeca.getCuantiasBecaByBecaId(beca.getId());


        String listaInfoCuantiasString = null;
        List<String> listaInfoCuantias = new ArrayList<String>();
        for (CuantiaBeca cuantiaBeca : listaCuantiasBecas)
        {
            listaInfoCuantiasString = listaInfoCuantiasString + cuantiaBeca.getCuantia().getNombre();
        }

        List<RecursoDetalle> listaRecursoDetalleReclamadas = RecursoDetalle.getRecursoDetallesByRecursoIdAndTipo(recurso.getId(), "RE");
        List<String> listaInfoReclamadas = new ArrayList<String>();
        for (RecursoDetalle recursoDetalle : listaRecursoDetalleReclamadas)
        {
            listaInfoReclamadas.add(recursoDetalle.getCuantia().getCodigo() + " " + recursoDetalle.getCuantia().getNombre());
        }

        List<RecursoDetalle> listaRecursoDetallePropuestas = RecursoDetalle.getRecursoDetallesByRecursoIdAndTipo(recurso.getId(), "PR");
        List<String> listaInfoPropuestas = new ArrayList<String>();
        for (RecursoDetalle recursoDetalle : listaRecursoDetallePropuestas)
        {
            listaInfoPropuestas.add(recursoDetalle.getCuantia().getCodigo() + " " + recursoDetalle.getCuantia().getNombre());
        }

        CursoAcademico academico = beca.getSolicitante().getCursoAcademico();
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", academico.getId(), (academico.getId() +1));

        infoRecurso.put("estudiante", solicitante.getPersona().getNombre());
        infoRecurso.put("dni", solicitante.getPersona().getIdentificacion());
        infoRecurso.put("cuantias", listaInfoCuantiasString);
        infoRecurso.put("recursoDetalleReclamadas", listaInfoReclamadas);
        infoRecurso.put("recursoDetallePropuestas", listaInfoPropuestas);

        infoRecurso.put("textoAyudasReclamadas", recurso.getTextoAyudasReclamadas());
        infoRecurso.put("cursoAntCompleto", getTextoSiNoDeEntero(recurso.getCursoAntCompleto()));
        infoRecurso.put("cursoAntCreditosMatr", recurso.getCursoAntCreditosMatr());
        infoRecurso.put("cursoAntCreditos", recurso.getCursoAntCreditos());
        infoRecurso.put("cursoAntNotaMedia", recurso.getCursoAntNotaMedia());
        infoRecurso.put("cursoAntNotaMediaSin", recurso.getCursoAntNotaMediaSin());
        infoRecurso.put("cursoActCambioEstudios", recurso.getCursoActCambioEstudios());
        infoRecurso.put("cursoAntCreditosPend", recurso.getCursoAntCreditosPend());
        infoRecurso.put("cursoAntCreditosPendMax", recurso.getCursoAntCreditosPendMax());
        infoRecurso.put("cursoActCursoPosterior", getTextoSiNoDeEntero(recurso.getCursoActCursoPosterior()));
        infoRecurso.put("cursoActCursoPosteriorTxt", recurso.getCursoActCursoPosteriorTxt());
        infoRecurso.put("cursoActCursoCompleto", getTextoSiNoDeEntero(recurso.getCursoActCursoCompleto()));
        infoRecurso.put("cursoActCreditosMatr", recurso.getCursoActCreditosMatr());
        infoRecurso.put("cursoActCreditos", recurso.getCursoActCreditos());
        infoRecurso.put("cursoActAniosBecario", recurso.getCursoActAniosBecario());
        infoRecurso.put("cursoActPerdidaCurso", getTextoSiNoDeEntero(recurso.getCursoActPerdidaCurso()));
        infoRecurso.put("cursoActPerdidaCursoTexto", recurso.getCursoActPerdidaCursoTxt());
        infoRecurso.put("numeroMiembrosComputables", recurso.getMiembrosComputables());
        infoRecurso.put("ingresosFamiliares", recurso.getIngresosFamiliares());
        infoRecurso.put("deducciones", recurso.getDeducciones());
        infoRecurso.put("rentaFamiliarDisponible", recurso.getRentaFamiliarDisponible());
        infoRecurso.put("rentaFamiliarPerCapita", recurso.getRentaFamiliarPerCapita());
        infoRecurso.put("porcentajeIncrementoUmbral", recurso.getPorcentajeIncrementoUmbral());
        infoRecurso.put("superaUmbrales", getTextoSiNoDeEntero(recurso.getSuperaUmbrales()));
        infoRecurso.put("superaUmbralesTexto", recurso.getSuperaUmbralesTexto());
        infoRecurso.put("superaUmbralesCuantia", recurso.getSuperaUmbralesCuantia());
        infoRecurso.put("distancia", recurso.getDistancia());
        infoRecurso.put("existeCentroMasCercano", getTextoSiNoDeEntero(recurso.getExisteCentroMasCercano()));
        infoRecurso.put("estimacion", getTextoSiNoDeEntero(recurso.getEstimacion()));
        infoRecurso.put("circunstanciasTransporte", recurso.getCircunstanciasTransporte());
        infoRecurso.put("otrasBecas", getTextoSiNoDeEntero(recurso.getOtrasBecas()));
        infoRecurso.put("otrasCircunstancias", recurso.getOtrasCircunstancias());
        infoRecurso.put("textoPropuesta", recurso.getTextoPropuesta());
        infoRecurso.put("textoJustificacion", recurso.getTextoJustificacion());
        infoRecurso.put("causasDesestimacion", recurso.getCausasDesestimacion());
        infoRecurso.put("cursoAntEstudio", recurso.getCursoAntEstudio());
        infoRecurso.put("cursoAcademico", cursoAcademico);
        infoRecurso.put("denegacionesActuales", recurso.getDenegacionesActuales());
        infoRecurso.put("ayudasActuales", recurso.getAyudasActuales());
        infoRecurso.put("cursoAntEstudioRama", recurso.getCursoAntEstudioRama());
        infoRecurso.put("cursoActCambioEstudios", getTextoSiNoDeEntero(recurso.getCursoActCambioEstudios()));
        infoRecurso.put("miembrosComputables", recurso.getMiembrosComputables());


        return infoRecurso;
    }

    private static String getTextoSiNoDeEntero(Integer valor)
    {
        String result = "";
        if (valor != null)
        {
            result = valor.equals(1) ? "Sí" : "No";
        }
        return result;
    }
}
