package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Comunidad;
import es.uji.apps.bec.model.QComunidad;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ComunidadDAODatabaseImpl extends BaseDAODatabaseImpl implements ComunidadDAO
{
    @Override
    public List<Comunidad> getComunidades()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QComunidad comunidad = QComunidad.comunidad;
        query.from(comunidad).orderBy(comunidad.codigo.asc());

        return query.list(comunidad);
    }

    @Override
    public List<Comunidad> getComunidadByCodigo(String codigo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QComunidad comunidad = QComunidad.comunidad;
        query.from(comunidad).where(comunidad.codigo.eq(codigo));

        return query.list(comunidad);
    }
}
