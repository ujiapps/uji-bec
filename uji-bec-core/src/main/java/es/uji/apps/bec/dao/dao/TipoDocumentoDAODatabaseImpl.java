package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.QTipoDocumento;
import es.uji.apps.bec.model.TipoDocumento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TipoDocumentoDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoDocumentoDAO
{
    private QTipoDocumento tipoDocumento = QTipoDocumento.tipoDocumento;

    @Override
    public List<TipoDocumento> getTiposDocumentos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoDocumento).orderBy(tipoDocumento.orden.asc());
        return query.list(tipoDocumento);
    }
    @Override
    public TipoDocumento getTipoDocumentoById(Long tipoDocumentoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoDocumento).where(tipoDocumento.id.eq(tipoDocumentoId));
        return query.uniqueResult(tipoDocumento);
    }

    @Override
    @Transactional
    public void deleteTipoDocumento(Long tipoDocumentoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoDocumento);
        delete.where(tipoDocumento.id.eq(tipoDocumentoId)).execute();
    }


}
