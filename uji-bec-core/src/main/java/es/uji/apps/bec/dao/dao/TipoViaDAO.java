package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoVia;
import es.uji.commons.db.BaseDAO;

public interface TipoViaDAO extends BaseDAO
{
   List<TipoVia> getTiposVias();
  
   TipoVia getTipoViaById(Long tipoViaId);
   
   void deleteTipoVia(Long tipoViaId);
}
 