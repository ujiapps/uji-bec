package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoIdentificacionDAO;

/**
 * The persistent class for the BC2_TIPOS_IDENTIFICACION database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_TIPOS_IDENTIFICACION")
public class TipoIdentificacion implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    private Long orden;

    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "tipoIdentificacion")
    private Set<Beca> becas;

    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "tipoIdentificacion")
    private Set<Miembro> miembros;

    // bi-directional many-to-one association to Arrendatario
    @OneToMany(mappedBy = "tipoIdentificacion")
    private List<Arrendatario> arrendatarios;

    public static TipoIdentificacionDAO tipoIdentificacionDAO;

    @Autowired
    public void setTipoIdentificacionDAO(TipoIdentificacionDAO tipoIdentificacionDAO)
    {
        TipoIdentificacion.tipoIdentificacionDAO = tipoIdentificacionDAO;
    }

    public TipoIdentificacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }

    public static TipoIdentificacion getTipoIdentificacionById(long tipoIdentificacionId)
    {
        return tipoIdentificacionDAO.getTipoIdentificacionById(tipoIdentificacionId);
    }

    public List<Arrendatario> getArrendatarios()
    {
        return arrendatarios;
    }

    public void setArrendatarios(List<Arrendatario> arrendatarios)
    {
        this.arrendatarios = arrendatarios;
    }
}