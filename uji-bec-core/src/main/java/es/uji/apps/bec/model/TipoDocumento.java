package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.TipoDocumentoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_TIPOS_DOCUMENTOS")
public class TipoDocumento implements Serializable
{
    private static TipoDocumentoDAO tipoDocumentoDAO;
    @Id
    private Long id;
    private String nombre;
    private Long orden;

    // bi-directional many-to-one association to Documento
    @OneToMany(mappedBy = "tipoDocumento")
    private Set<Documento> documentos;

    public TipoDocumento()
    {

    }

    @Autowired
    public void setTipoDocumentoDAO(TipoDocumentoDAO tipoDocumentoDAO)
    {
        TipoDocumento.tipoDocumentoDAO = tipoDocumentoDAO;
    }

    public Long getId()
     {
         return this.id;
     }
     
    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public static TipoDocumento getTipoDocumentoById(Long tipoDocumentoId)
    {
        List<TipoDocumento> listaTiposDocumentos = tipoDocumentoDAO.get(TipoDocumento.class, tipoDocumentoId);
        if (listaTiposDocumentos.size() > 0)
        {
            return listaTiposDocumentos.get(0);
        }
        return new TipoDocumento();
    }
}