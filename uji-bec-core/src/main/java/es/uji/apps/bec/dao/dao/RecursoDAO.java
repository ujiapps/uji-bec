package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Recurso;
import es.uji.commons.db.BaseDAO;

public interface RecursoDAO extends BaseDAO
{
    List<Recurso> getRecursosByTandaId(Long tandaId);

    Recurso getRecursoByBecaId(Long becaId);

    void update(Recurso recurso);
}
