package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class TipoTitulacionNoExisteException extends CoreBaseException
{
    public TipoTitulacionNoExisteException(String message)
    {
        super(message);
    }
}
