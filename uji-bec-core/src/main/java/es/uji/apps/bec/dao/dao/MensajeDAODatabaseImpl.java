package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.Mensaje;
import es.uji.apps.bec.model.QMensaje;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MensajeDAODatabaseImpl extends BaseDAODatabaseImpl implements MensajeDAO
{
    QMensaje qMensaje = QMensaje.mensaje;

    @Override
    public List<Mensaje> getMensajesByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qMensaje)
                .where(qMensaje.referencia.eq(becaId.toString()))
                .orderBy(qMensaje.fecha.desc());

        return query.list(qMensaje);
    }
}
