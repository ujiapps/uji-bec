package es.uji.apps.bec.dao.dao;

import es.uji.commons.db.BaseDAO;

public interface DiccionarioExcepcionesDAO extends BaseDAO
{
    Long getValor(Long cursoAcademicoId, String claseUji, Long organismoId, String valorOrigen);

    String getValorOrigen(Long cursoAcademicoId, String claseUji, Long organismoId, String valorUji);
}
