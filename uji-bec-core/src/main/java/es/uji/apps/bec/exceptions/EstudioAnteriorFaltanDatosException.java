package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class EstudioAnteriorFaltanDatosException extends GeneralBECException
{
    public EstudioAnteriorFaltanDatosException()
    {
        super("Falten dades de l'estudi anterior en la pestanya d'Estudis");
    }

    public EstudioAnteriorFaltanDatosException(String message)
    {
        super(message);
    }
}
