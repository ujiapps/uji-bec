package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Cuantia;
import es.uji.commons.db.BaseDAO;

public interface CuantiaDAO extends BaseDAO
{
    public List<Cuantia> getCuantias();

    public List<Cuantia> getCuantiasActivas();

    public Cuantia getCuantiaById(Long cuantiaId);

    void deleteCuantia(Long cuantiaId);
}
