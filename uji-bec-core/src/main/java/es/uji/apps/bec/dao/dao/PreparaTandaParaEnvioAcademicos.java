package es.uji.apps.bec.dao.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class PreparaTandaParaEnvioAcademicos
{
    private GetMultiEnvioAcademicosProcedure getMultiEnvioAcademicos;

    private static DataSource dataSource;

    private String sql;

    public PreparaTandaParaEnvioAcademicos()
    {
    }

    public PreparaTandaParaEnvioAcademicos(String sql)
    {
        this.sql = sql;
    }

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        PreparaTandaParaEnvioAcademicos.dataSource = dataSource;
    }

    public void init()
    {
        this.getMultiEnvioAcademicos = new GetMultiEnvioAcademicosProcedure(dataSource, this.sql);
    }

    public Long getMultiEnvioAcademicos(Long tandaId, Long registroId)
    {
        return getMultiEnvioAcademicos.execute(tandaId, registroId);
    }

    private class GetMultiEnvioAcademicosProcedure extends StoredProcedure
    {
        private static final String FUNCION = ".lanzageneracion";
        private static final String PTANDA = "ptanda_id";
        private static final String PREGISTRO = "pregistro_id";

        public GetMultiEnvioAcademicosProcedure(DataSource dataSource, String sql)
        {
            setDataSource(dataSource);
            setFunction(true);
            setSql(sql + FUNCION);
            declareParameter(new SqlOutParameter("multiEnvioAcademicos", Types.BIGINT));
            declareParameter(new SqlParameter(PTANDA, Types.BIGINT));
            declareParameter(new SqlParameter(PREGISTRO, Types.BIGINT));
            compile();
        }

        public Long execute(Long tandaId, Long registroId)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PREGISTRO, registroId);
            inParams.put(PTANDA, tandaId);

            Map<String, Object> results = execute(inParams);
            Long envio = (Long) results.get("multiEnvioAcademicos");
            return envio;
        }
    }
}
