package es.uji.apps.bec.model;

import com.google.common.collect.Iterables;
import com.mysema.query.types.expr.StringExpression;
import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.domains.*;
import es.uji.commons.rest.ParamUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FicheroConselleria
{
    public static final String BECA_ACEPTADA = "A";
    public static final String BECA_DENEGADA = "N";
    public static final String BECA_JUSTIFICAR = "J";
    public static final String CODIGO_UNIVERSIDAT_JAUME_ID = "UJI";

    private static final Long BECA_ESTADO_CONCEDIDA = 5L;
    private static final Long BECA_ESTADO_DENEGADA = 6L;

    private static final int NUMERO_CAMPOS_CSV_MANUELA_CONCEDIDAS = 11;
    private static final int NUMERO_CAMPOS_CSV_MANUELA_DENEGADAS = 6;

    private List<Denegacion> mapaListaDenegaciones;

    public FicheroConselleria()
    {

    }

    public Map<String, String> getFicheroCSVPropuestaGVAManuelaParaTandaId(Long tandaId, Boolean notaMediaObligatoria) throws WsErrorAlPrepararMultienvioException, DNIIncorrectoException, ParseException {
        StringBuffer datos = new StringBuffer();
        Tanda tanda = Tanda.getTandaById(tandaId);
        tanda.isCorrectaParaEnviar();


        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            String dni = beca.getIdentificacion();
            if (dni.length() != 9)
            {
                throw new DNIIncorrectoException();
            }

            String codigoArchivoTemporal = beca.getCodigoArchivoTemporal().toString();
            Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());
            Estudio estudio = beca.getEstudio();
            if (estudio == null)
            {
                estudio = new Estudio();
            }

            // 01 N20  Id solicitud
            datos.append(codigoArchivoTemporal.concat(";"));
            // 02 A9   NIF/NIE solicitante según la DG
            datos.append(dni.concat(";"));
            // 03 A9   NIF/NIE con el que el solicitante se ha matriculado en la Universidad. Si coincide con el del apartado anterior lo dejamos vacío
            datos.append(calculaDNIUniversidadOVacio(beca).concat(";"));

            if (beca.isSolicitudMatriculada())
            {
                // 04 A8   Centro Universitario Código RUCT (Registro de Universidades, Centros y Títulos) del Centro Universitario. En el caso de Máster dejarlo a nulo.
                datos.append((getCodigoRuctCentro(estudio)).concat(";"));
                // 05 A100 Nombre Centro Denominación oficial del Centro Universitario. Los 100 caracteres es la longitud máxima, no hace falta completarlo con espacios en blanco. En el aso de Máster dejarlo a nulo.
                datos.append((getNombreCentro(estudio)).concat(";"));
                // 06 N7   Titulación Código RUCT de la Titulación, o código universidad para dobles grados
                datos.append(((estudio.getNombre() != null) ? getTitulacionGVA(beca, estudio) : "").concat(";"));
                // 07 A500 Nombre Titulación Denominación oficial de la Titulación. Los 500 caracteres es la longitud máxima, no hace falta completarlo con espacios en blanco.
                datos.append(((estudio.getNombre() != null) ? estudio.getNombre() : "").concat(";"));
                // 08 A1   Matriculado
                datos.append((beca.isSolicitudMatriculada()? "S" : "N").concat(";"));
                PersonaEstudio datosAcademicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), estudio.getId(), solicitante.getCursoAcademico().getId());
                // 09 A1   Matriculado por primera vez en el primer curso de Grado o Máster
                datos.append((datosAcademicos.isPrimeraVez() ? "S" : "N").concat(";"));
                // 10 A5   Número créditos matriculados en el último curso realizado en el grado o Máster, de la misma o distinta titulación. En caso de ser la primera matrícula lo dejaremos vacío
                datos.append((datosAcademicos.isPrimeraVez() ? "" : Fichero.rellenaString(calculaCreditosMatriculadosCursoAnterior(beca), 5, "0", "left")).concat(";"));
                // 11 A1   ¿Cumple el requisito establecidomen la convocatoria de mínimo demcréditos matriculados en elmúltimo curso realizado en elmgrado o Máster, de la misma omdistinta titulación?
                datos.append((datosAcademicos.isPrimeraVez() ? "" : (cumpleConcicionesMatriculaCursoAnterior(beca) ? "S" : "N")).concat(";"));
                // 12 A5   Número créditos superados en el último curso realizado en el grado o Máster, de la misma o distinta titulación. En caso de ser la primera matrícula lo dejaremos vacío
                datos.append((datosAcademicos.isPrimeraVez() ? "" : Fichero.rellenaString(calculaCreditosSuperadosAntGVAAltres(beca), 5, "0", "left")).concat(";"));
                // 13 A5   Número de créditos matriculados en el curso de la convocatoria (solo los creditos para Beca)
                datos.append(Fichero.rellenaString(calculaCreditosParaBeca(beca), 5, "0", "left").concat(";"));
                // 14 A1   ¿Cumple el requisito establecido en la convocatoria de mínimo de créditos matriculado en el curso de la convocatoria?
                datos.append((cumpleConcicionesMatricula(beca) ? "S" : "N").concat(";"));
                // 15 A4   Nota media según corresponda
                datos.append(getCampoNotaMediaCoefCorrectorManuela(beca, notaMediaObligatoria).concat(";"));
                // 16 A1   ¿Ha realizado cambio de estudios respecto a los últimos cursados antes de la convocatoria actual?
                datos.append((beca.isCambioEstudios() ? "S" : "N").concat(";"));
                // 17 A3   En el caso de que el apartado anterior sea SI indicar el código RUCT de la universidad, donde cursaba los estudios el año anterior al de la convocatoria o último cursado
                datos.append(("").concat(";"));
                // 18 N7   Titulación Código RUCT de la Titulación. Rellenar solo si en el apartado 16 se ha puesto S(Sí). En caso contrario dejarlo a nulo. Si es una titulación extranjera, el código será 9999999.
                datos.append((beca.isCambioEstudios() ? beca.getCodigoEstudioAnt() : "").concat(";"));
                // 19 A500 Nombre Titulación Denominación oficial de la Titulación. Rellenar solo si en el apartado 16 se ha puesto S (SÍ).
                datos.append((beca.isCambioEstudios() ? beca.getEstudioAnt() : "").concat(";"));
                // 20 A1   ¿Dispone de titulación del mismo nivel o superior que la titulación para la que solicita Beca?
                datos.append((beca.isPoseeTitulo() ? "S" : "N").concat(";"));
                // 21 A1   ¿Ha finalizado título de grado antes del 31/12/2023?
                datos.append(getMasterHabilitante(beca, estudio, datosAcademicos));
            }
            else {
                datos.append(Fichero.rellenaString("", 4, ";", "left"));
                datos.append("N;"); // No matriculado.
                datos.append(Fichero.rellenaString("", 12, ";", "left"));
            }
            datos.append(saltoDeLinea());
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());

        fichero.put("nombre", MessageFormat.format("{0,number,#}-conselleria-manuela-{1,number,#}.csv", tanda.getCursoAcademico().getId(), tanda.getTandaId()));

        return fichero;
    }

    private String getCreditosMatriculadosCursoAnterior(Beca beca) {

        if (beca.getCreditosMatriculadosAnt() != null)
        {
            Float creditosMatriculadosAnt = beca.getCreditosMatriculadosAnt()  * 100;
            return Fichero.rellenaString(creditosMatriculadosAnt, 5, "0", "left");
        }
        else
        {
            return "";
        }
    }

    private String getNombreCentro(Estudio estudio) {
        if (estudio.getTipo().equals("M")) {
            return "";
        }
        return (estudio.getNombreCentro() != null) ? estudio.getNombreCentro() : "";
    }

    private String getCodigoRuctCentro(Estudio estudio) {
        if (estudio.getTipo().equals("M")) {
            return "";
        }
        return (estudio.getCodigoCentro() != null) ? estudio.getCodigoCentro() : "";
    }

    private String getMasterHabilitante(Beca beca, Estudio estudio, PersonaEstudio datosAcademicos) throws ParseException {
        if (estudio.isHabilitante()) {
            Date fechaTitAcceso = datosAcademicos.getFechaTitAcceso();

            String curso = beca.getSolicitante().getCursoAcademico().getId().toString();

            String dateStr = "31/12/" + curso;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaLimite = dateFormat.parse(dateStr);

            int comparisonResult = fechaTitAcceso.compareTo(fechaLimite);

            if (comparisonResult <= 0) {
                return "S";
            } else {
                return "N";
            }
        }
        return "";
    }

    private String calculaDNIUniversidadOVacio(Beca beca) {
        if (beca.getIdentificacion().equals(beca.getSolicitante().getPersona().getIdentificacion())) {
            return "";
        }
        return beca.getSolicitante().getPersona().getIdentificacion();

    }

    public Map<String, String> getFicheroCSVPropuestaGVAFinalizacionParaTandaId(Long tandaId, Boolean notaMediaObligatoria)
            throws WsErrorAlPrepararMultienvioException, DNIIncorrectoException, FaltaNotaMediaException
    {
        Tanda tanda = Tanda.getTandaById(tandaId);
        tanda.isCorrectaParaEnviar();

        StringBuffer datos = getCSVDatosPropuestaGVAFinalizacion(notaMediaObligatoria, tanda);

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}conselleriaFinalizacion{1,number,#}.csv", tanda.getCursoAcademico().getId(), tanda.getTandaId()));
        return fichero;
    }

    private StringBuffer getCSVDatosPropuestaGVAFinalizacion(Boolean notaMediaObligatoria, Tanda tanda)
            throws DNIIncorrectoException, FaltaNotaMediaException
    {
        StringBuffer datos = new StringBuffer();

        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            String codigoArchivoTemporal = beca.getCodigoArchivoTemporal().toString();
            datos.append(codigoArchivoTemporal.concat(";"));

            Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());
            String dni = beca.getIdentificacion();
            if (dni.length() != 9)
            {
                throw new DNIIncorrectoException();
            }
            datos.append(dni.concat(";"));

            if (beca.getConvocatoria().getId() == CodigoConvocatoria.GVA_FINALIZACION.getId())
            {
                datos.append(getCSVAcademicosSolicitanteGVAFinalizacion(beca, solicitante, notaMediaObligatoria));
            }
            datos.append(saltoDeLinea());
        }

        return datos;
    }

    public Map<String, String> getFicheroPropuestaParaTandaId(Long tandaId, Boolean notaMediaObligatoria)
            throws WsErrorAlPrepararMultienvioException,
            DomicilioIncorrectoException, DNIIncorrectoException, DomicilioNoEncontradoException,
            MiembroIncorrectoException, FaltaNotaMediaException
    {
        StringBuffer datos = new StringBuffer();
        Tanda tanda = Tanda.getTandaById(tandaId);

        tanda.isCorrectaParaEnviar();

        datos.append(getCabecera(tanda));
        datos.append(saltoDeLinea());

        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());
            Miembro miembro = Miembro.getMiembroSolicitante(beca.getId());
            datos.append(getRegistroSolicitanteConAcademicos(beca, miembro, solicitante, notaMediaObligatoria));
            datos.append(saltoDeLinea());

            List<Miembro> listaFamiliares = Miembro.getMiembrosByBeca(beca.getId());
            for (Miembro familiar : listaFamiliares)
            {
                datos.append(getRegistroMiembro(miembro, familiar));
                datos.append(saltoDeLinea());
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}conselleria{1,number,#}.txt", tanda.getCursoAcademico().getId(), tanda.getTandaId()));
        return fichero;
    }

    public Map<String, String> getFicheroPropuestaMec2CrdParaTandaId(Long tandaId, Boolean notaMediaObligatoria)
            throws WsErrorAlPrepararMultienvioException,
            DomicilioIncorrectoException, DNIIncorrectoException, DomicilioNoEncontradoException,
            MiembroIncorrectoException, FaltaNotaMediaException
    {
        StringBuffer datos = new StringBuffer();
        Tanda tanda = Tanda.getTandaById(tandaId);

        tanda.isCorrectaParaEnviar();

        datos.append(getCabecera(tanda));
        datos.append(saltoDeLinea());

        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());
            Miembro miembro = Miembro.getMiembroSolicitante(beca.getId());
            datos.append(getRegistroSolicitanteConAcademicosMec2Crd(beca, miembro, solicitante, notaMediaObligatoria));
            datos.append(saltoDeLinea());
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}conselleriaMEC{1,number,#}.txt", tanda.getCursoAcademico().getId(), tanda.getTandaId()));
        return fichero;
    }


    private String getRegistroMiembro(Miembro miembro, Miembro familiar)
            throws DNIIncorrectoException
    {
        StringBuffer registro = new StringBuffer();
        registro.append("3");

        String dni = miembro.getIdentificacion();
        if (dni.length() != 9)
        {
            throw new DNIIncorrectoException();
        }

        registro.append(Fichero.rellenaString(dni, 9, " ", "right"));
        registro.append(Fichero.rellenaString(familiar.getIdentificacion(), 9, " ", "right"));
        registro.append(Fichero.rellenaString(familiar.getApellido1(), 24, " ", "right"));
        registro.append(Fichero.rellenaString(familiar.getApellido2(), 24, " ", "right"));
        registro.append(Fichero.rellenaString(familiar.getNombre(), 24, " ", "right"));
        registro.append(Fichero.rellenaString(getCampoParentesco(miembro, familiar), 2, " ", "right"));
        registro.append(Fichero.rellenaString(familiar.getTipoSustentador().getId(), 1, " ", "right"));
        registro.append(Fichero.rellenaString(getFechaNacimientoMiembroFormateado(familiar), 8, " ", "right"));

        return registro.toString();
    }

    private String getCampoParentesco(Miembro miembro, Miembro familiar)
    {
        if (miembro.getIdentificacion().equals(familiar.getIdentificacion()))
        {
            return "";
        }
        else
        {
            MapeadorParentesco mapeadorParentesco = new MapeadorParentesco();

            return mapeadorParentesco.getValorOrganismo(familiar.getTipoMiembro().getId(), CodigoOrganismo.CONSELLERIA);
        }
    }

    private String saltoDeLinea()
    {
        return "\n";
    }


    private String getRegistroSolicitanteConAcademicos(Beca beca, Miembro miembro,
                                                       Solicitante solicitante, Boolean notaMediaObligatoria)
            throws DNIIncorrectoException,
            DomicilioNoEncontradoException, DomicilioIncorrectoException,
            MiembroIncorrectoException, FaltaNotaMediaException
    {
        StringBuilder registro = new StringBuilder();

        registro.append(getRegistroSolicitante(beca, miembro));
        registro.append(getDatosUnidadFamiliarSolicitanteFormateado(miembro, solicitante));
        registro.append(getAcademicosSolicitante(beca, solicitante, notaMediaObligatoria));

        return registro.toString();
    }

    private String getRegistroSolicitanteConAcademicosMec2Crd(Beca beca, Miembro miembro,
                                                              Solicitante solicitante, Boolean notaMediaObligatoria)
            throws DNIIncorrectoException,
            DomicilioNoEncontradoException, DomicilioIncorrectoException,
            MiembroIncorrectoException, FaltaNotaMediaException
    {
        StringBuilder registro = new StringBuilder();

        registro.append(getRegistroSolicitante(beca, miembro));
        registro.append(getDatosUnidadFamiliarSolicitanteFormateadoMec2crd(miembro, solicitante));
        registro.append(getAcademicosSolicitanteMec2Crd(beca, notaMediaObligatoria));

        return registro.toString();
    }

    private String getRegistroSolicitante(Beca beca, Miembro miembro) throws DNIIncorrectoException,
            DomicilioNoEncontradoException, DomicilioIncorrectoException,
            MiembroIncorrectoException, FaltaNotaMediaException
    {
        StringBuilder registro = new StringBuilder();

        registro.append("2");

        String dni = beca.getIdentificacion();

        if (dni.length() != 9)
        {
            throw new DNIIncorrectoException();
        }

        String nombre = miembro.getNombre();
        String apellido1 = miembro.getApellido1();
        String apellido2 = miembro.getApellido2();

        registro.append(Fichero.rellenaString(dni, 9, " ", "right"));
        registro.append(Fichero.rellenaString(apellido1, 24, " ", "right"));
        registro.append(Fichero.rellenaString(apellido2, 24, " ", "right"));
        StringBuilder right = registro.append(Fichero.rellenaString(nombre, 24, " ", "right"));
        registro.append(Fichero.rellenaString(getDomicilioSolicitanteFormateado(beca), 72, " ", "right"));
        registro.append(getSexoSolicitanteFormateado(miembro));
        registro.append(getFechaNacimientoMiembroFormateado(miembro));

        return registro.toString();
    }

    private String getFechaNacimientoMiembroFormateado(Miembro miembro)
    {
        SimpleDateFormat sm = new SimpleDateFormat("YYYYMMdd");
        return sm.format(miembro.getFechaNacimiento());
    }

    private String getSexoSolicitanteFormateado(Miembro miembro) throws MiembroIncorrectoException
    {
        TipoSexo sexo = miembro.getTipoSexo();
        String sexoString;

        if (sexo == null)
        {
            throw new MiembroIncorrectoException("El tipus de sexe del membro no està definit   " + miembro.getBeca().getSolicitante().getPersona().getIdentificacion());
        }
        else
        {
            MapeadorTipoSexo mapeadorTipoSexo = new MapeadorTipoSexo();
            sexoString = mapeadorTipoSexo.getValorOrganismo(sexo.getId(), CodigoOrganismo.CONSELLERIA).toString();
        }
        return sexoString;
    }

    private String getDomicilioSolicitanteFormateado(Beca beca)
            throws DomicilioNoEncontradoException, DomicilioIncorrectoException
    {
        Domicilio domicilio = Domicilio.getDomicilioGVA(beca.getId());

        StringBuilder domicilioString = new StringBuilder();

        Long tipoViaId = domicilio.getTipoVia().getId();
        MapeadorTipoVia mapeadorTipoVia = new MapeadorTipoVia();

        domicilioString.append(Fichero.rellenaString(mapeadorTipoVia.getValorOrganismo(tipoViaId, CodigoOrganismo.CONSELLERIA), 2, " ", "right"));

        domicilioString.append(Fichero.rellenaString(domicilio.getNombreVia(), 50, " ", "right"));

        String numeroVia = domicilio.getNumero();

        if (numeroVia == null || numeroVia.isEmpty())
        {
            throw new DomicilioIncorrectoException("El número de via no pot estar buit");
        }

        domicilioString.append(Fichero.rellenaString(numeroVia, 4, " ", "right"));
        domicilioString.append(Fichero.rellenaString(domicilio.getEscalera(), 2, " ", "right"));
        domicilioString.append(Fichero.rellenaString(domicilio.getPiso(), 2, " ", "right"));
        domicilioString.append(Fichero.rellenaString(domicilio.getPuerta(), 2, " ", "right"));
        domicilioString.append(Fichero.rellenaString(getCampoLocalidad(domicilio), 3, "0", "left"));
        domicilioString.append(Fichero.rellenaString(domicilio.getCodigoPostal(), 5, "0", "left"));
        domicilioString.append(Fichero.rellenaString(domicilio.getProvincia().getId(), 2, "0", "left"));

        return domicilioString.toString();
    }

    private String getCampoLocalidad(Domicilio domicilio)
    {
        Localidad localidad = domicilio.getLocalidad();
        String localidadString = localidad.getCodigoAyuntamiento().substring(2, 5);

        return localidadString;
    }


    private String getAcademicosSolicitanteGVASalario(Beca beca, Solicitante solicitante,
                                                      Boolean notaMediaObligatoria) throws FaltaNotaMediaException
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        if (estudio == null)
        {
            estudio = new Estudio();
        }

        // DNI de matrícula A9
        getDatosEstudioGVAAltres(beca, academico, estudio);

        PersonaEstudio datosAcademicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), estudio.getId(), solicitante.getCursoAcademico().getId());



        if (!beca.isSolicitudMatriculada())
        {
            academico.append(Fichero.rellenaString(beca.isSolicitudMatriculada()? "S" : "N", 1, " ", "left"));
            //academico.append(Fichero.rellenaString(beca.isSolicitudMatriculada()? "0" : "1", 1, " ", "left"));
            academico.append(Fichero.rellenaString(datosAcademicos.isPrimeraVez()? "S" : "N", 1, " ", "left"));


            academico.append(Fichero.rellenaString(0, 5, "0", "left"));// numero de créditos
            academico.append(Fichero.rellenaString(1, 1, "0", "right"));// cumple matricula
            academico.append(Fichero.rellenaString(1, 1, " ", "left"));// inicia estudios
            academico.append(Fichero.rellenaString(1, 1, " ", "left"));// estudios anteriores
            academico.append(Fichero.rellenaString(0, 4, " ", "right"));//nota media
            academico.append(Fichero.rellenaString(1, 1, "0", "left"));// beca mec
            academico.append(Fichero.rellenaString(0, 4, " ", "right"));//nota media superadas
            academico.append(Fichero.rellenaString(0, 5, "0", "left"));// creditos curso anterior
            academico.append(Fichero.rellenaString(0, 5, "0", "left"));// creditos superados
            academico.append(Fichero.rellenaString(0, 1, "0", "left"));// curso que se matricula
            academico.append(Fichero.rellenaString(0, 1, " ", "left"));// otros estudios
            return academico.toString();
        }
///



        academico.append(Fichero.rellenaString(estudio.getCodigoCentro(), 8, " ", "right"));
        academico.append(Fichero.rellenaString(estudio.getNombreCentro(), 100, " ", "right"));
        academico.append(Fichero.rellenaString(getTitulacionGVA(beca, estudio), 7, " ", "right"));
        academico.append(Fichero.rellenaString(estudio.getNombre(), 100, " ", "right"));


        academico.append(Fichero.rellenaString(beca.isSolicitudMatriculada()? "S" : "N", 1, " ", "left"));
        academico.append(Fichero.rellenaString(datosAcademicos.isPrimeraVez()? "S" : "N", 1, " ", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculadosCursoAnterior(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(cumpleConcicionesMatriculaCursoAnterior(beca)? "S" : "N", 1, "0", "right"));
        academico.append(Fichero.rellenaString(calculaCreditosSuperadosAntGVAAltres(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculadosGVAAltres(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(cumpleConcicionesMatricula(beca)? "S" : "N", 1, "0", "right"));
        academico.append(Fichero.rellenaString(getCampoNotaMediaCoefCorrectorManuela(beca, notaMediaObligatoria), 4, " ", "right"));
        // 16 Cambio estudio ultimo año
        academico.append(Fichero.rellenaString(datosAcademicos.isCambioEstudios()? "S" : "N", 1, " ", "left"));

        academico.append(Fichero.rellenaString(0, 3, " ", "left"));// RUCT universidad procedencia
        academico.append(Fichero.rellenaString(0, 7, " ", "left"));// RUCT titulación procedencia
        academico.append(Fichero.rellenaString(0, 100, " ", "left"));// titulación procedencia

        academico.append(Fichero.rellenaString(beca.isPoseeTitulo()? "S" : "N", 1, " ", "left"));


        academico.append(Fichero.rellenaString(getCampoMatriculaIniciaEstudios(beca) == "S" ? "0":"1", 1, " ", "left"));
        academico.append(Fichero.rellenaString(getCampoMatriculaOtrosEstudios(beca, estudio) == "S" ? "0":"1", 1, " ", "left")); // matriculado en otros Estudios anteriormente
        academico.append(Fichero.rellenaString(getCampoNotaMediaCoefCorrector(beca, notaMediaObligatoria), 4, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoSolicitaMEC(solicitante), 1, "0", "left"));
        academico.append(Fichero.rellenaString(getCampoNotaMediaSuperadas(beca, notaMediaObligatoria), 4, " ", "right"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculadosAntGVAAltres(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosSuperadosAntGVAAltres(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(beca.getCurso(), 1, "0", "left"));
        academico.append(Fichero.rellenaString(beca.isPoseeTitulo()? "0" : "1", 1, " ", "left"));

        return academico.toString();
    }

    private String getAcademicosSolicitanteGVAFinalizacion(Beca beca, Solicitante solicitante, Boolean notaMediaObligatoria) throws FaltaNotaMediaException
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        if (estudio == null)
        {
            estudio = new Estudio();
        }

        getDatosEstudioGVAAltres(beca, academico, estudio);

        academico.append(Fichero.rellenaString(beca.isSolicitudMatriculada()? "0" : "1", 1, " ", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculadosGVAAltres(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosParaFinalizar(beca, solicitante), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculados3a(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaImportes3a(beca, solicitante), 6, "0", "left"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculados4ap(beca), 5, "0", "left"));
        academico.append(Fichero.rellenaString(calculaImportes4ap(beca, solicitante), 6, "0", "left"));
        academico.append(Fichero.rellenaString(beca.isPoseeTitulo() ? "0" : "1", 1, "0", "right"));
        academico.append(Fichero.rellenaString(getCampoNotaMedia(beca, notaMediaObligatoria), 4, " ", "right"));
        academico.append(Fichero.rellenaString(solicitante.tieneBecaFinalizacionConcedida()? "0" : "1", 1, " ", "left"));
        return academico.toString();
    }

    private String getCSVAcademicosSolicitanteGVAFinalizacion(Beca beca, Solicitante solicitante, Boolean notaMediaObligatoria) throws FaltaNotaMediaException
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        if (estudio == null)
        {
            estudio = new Estudio();
        }

        academico.append(estudio.getCodigoCentro().concat(";"));
        academico.append("\"" + estudio.getNombreCentro() + "\"" + ";");
        academico.append(getTitulacionGVA(beca, estudio).concat(";"));
        academico.append("\"" + estudio.getNombre() + "\"" + ";");

        academico.append((beca.isSolicitudMatriculada()? "0" : "1").concat(";"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculadosGVAAltres(beca), 5, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(calculaCreditosParaFinalizar(beca, solicitante), 5, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculados3a(beca), 5, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(calculaImportes3a(beca, solicitante), 6, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(calculaCreditosMatriculados4ap(beca), 5, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(calculaImportes4ap(beca, solicitante), 6, "0", "left").concat(";"));
        academico.append(Fichero.rellenaString(beca.isPoseeTitulo() ? "0" : "1", 1, "0", "right").concat(";"));
        academico.append(Fichero.rellenaString(getCampoNotaMedia(beca, notaMediaObligatoria), 4, " ", "right").concat(";"));
        academico.append(Fichero.rellenaString(solicitante.tieneBecaFinalizacionConcedida()? "0" : "1", 1, " ", "left").concat(";"));
        return academico.toString();
    }

    private void getDatosEstudioGVAAltres(Beca beca, StringBuilder academico, Estudio estudio)
    {
        academico.append(Fichero.rellenaString(estudio.getCodigoCentro(), 8, " ", "right"));
        academico.append(Fichero.rellenaString(estudio.getNombreCentro(), 100, " ", "right"));
        academico.append(Fichero.rellenaString(getTitulacionGVA(beca, estudio), 7, " ", "right"));
        academico.append(Fichero.rellenaString(estudio.getNombre(), 100, " ", "right"));
    }

    private String getAcademicosSolicitante(Beca beca, Solicitante solicitante,
                                            Boolean notaMediaObligatoria) throws FaltaNotaMediaException
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        academico.append(Fichero.rellenaString(estudio.getCodigoCentro(), 8, " ", "right"));
        academico.append(Fichero.rellenaString(getTitulacionGVA(beca, estudio), 7, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoSolicitaMEC(solicitante), 1, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoNotaMediaCoefCorrector(beca, notaMediaObligatoria), 4, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoIDSolicitud(beca), 50, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoAutorizacion(beca), 1, " ", "right"));

        try
        {
            Declarante declarante = Declarante.getDeclaranteBySolicitanteId(solicitante.getId()).get(0);
            academico.append(Fichero.rellenaString(getCampoIngresosExtranjeros(declarante), 1, " ", "right"));
            academico.append(Fichero.rellenaString(getCampoImportesExtranjeros(declarante), 15, "0", "left"));

        }
        catch (IndexOutOfBoundsException e)
        {
            academico.append(Fichero.rellenaString('N', 1, "0", "left"));
            academico.append(Fichero.rellenaString(0, 15, "0", "left"));
        }

        academico.append(Fichero.rellenaString(getCampoMatriculaGradoPrimeraVez(beca, estudio), 1, "0", "left"));
        academico.append(Fichero.rellenaString(" ", 20, " ", "left")); //Datos propios

        return academico.toString();

    }

    private String getAcademicosSolicitanteDenegado(Beca beca, Solicitante solicitante)
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        if (estudio == null)
        {
            academico.append(Fichero.rellenaString("00000000", 8, " ", "right"));
            academico.append(Fichero.rellenaString("0000000", 7, " ", "right"));
        }
        else
        {
            academico.append(Fichero.rellenaString(estudio.getCodigoCentro(), 8, " ", "right"));
            academico.append(Fichero.rellenaString(getTitulacionGVA(beca, estudio), 7, " ", "right"));
        }
        academico.append(Fichero.rellenaString(getCampoSolicitaMEC(solicitante), 1, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoIDSolicitud(beca), 50, " ", "right"));

        return academico.toString();
    }

    private String getAcademicosSolicitanteMec2Crd(Beca beca, Boolean notaMediaObligatoria) throws FaltaNotaMediaException
    {
        StringBuilder academico = new StringBuilder();
        Estudio estudio = beca.getEstudio();

        academico.append(Fichero.rellenaString(estudio.getCodigoCentro(), 8, " ", "right"));
        academico.append(Fichero.rellenaString(getTitulacionGVA(beca, estudio), 7, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoNotaMediaCoefCorrector(beca, notaMediaObligatoria), 4, " ", "right"));
        academico.append(Fichero.rellenaString(getCampoAutorizacion(beca), 1, " ", "right"));
        academico.append(Fichero.rellenaString(" ", 20, " ", "left")); //Datos propios
        academico.append(Fichero.rellenaString(getCampoIDSolicitud(beca), 50, " ", "right"));

        return academico.toString();
    }

    private String getCampoMatriculaGradoPrimeraVez(Beca beca, Estudio estudio)
    {
        String matriculado = "N";
        String tipoEstudio = estudio.getTipo();

        if (tipoEstudio.equals("G")
                && beca.getCurso() != null
                && beca.getCurso() == 1L
                && (beca.getCreditosMatriculadosAnt() == null ||
                Float.compare(beca.getCreditosMatriculadosAnt(), new Float("0.0")) == 0))
        {
            matriculado = "S";
        }
        return matriculado;
    }
    private String getCampoMatriculaIniciaEstudios(Beca beca)
    {
        if (beca.isCambioEstudios())
        {
            return "S";
        }
        else
        {
            return "N";
        }
    }
    private String getCampoMatriculaOtrosEstudios(Beca beca, Estudio estudio)
    {
        String matriculado = "N";
        String tipoEstudio = estudio.getTipo();

        if (tipoEstudio.equals("G")
                && beca.getCurso() != null
                && beca.getCurso() == 1L
                && getCampoMatriculaIniciaEstudios(beca) == "S"
                && (beca.getCreditosMatriculadosAnt() != null ||
                Float.compare(beca.getCreditosMatriculadosAnt(), new Float("0.0")) != 0)
        )
            matriculado = "S";
        return matriculado;
    }

    private String getCampoImportesExtranjeros(Declarante declarante)
    {
        if (declarante.getRentasExtranjero() == null)
        {
            return null;
        }
        else
        {
            Float rentasExtranjero = declarante.getRentasExtranjero();

            if (rentasExtranjero > 0)
            {
                return declarante.getRentasExtranjero().toString();
            }
            else
            {
                return "0";
            }
        }
    }

    private String getTitulacionGVA(Beca beca, Estudio estudio)
    {
        if (estudio == null || estudio.getId() == null)
        {
            return null;
        }

        Tanda tanda = beca.getTanda();
        Long cursoAcademicoId = tanda.getCursoAcademico().getId();

        String titulacionGVA = Diccionario.getValorOrigen(cursoAcademicoId, "Estudio",  CodigoOrganismo.CONSELLERIA.getId(), estudio.getId().toString());

        if (titulacionGVA == "")
        {
            titulacionGVA = DiccionarioExcepciones.getValorOrigen(cursoAcademicoId, "Estudio",  CodigoOrganismo.CONSELLERIA.getId(), estudio.getId().toString());
        }
        return titulacionGVA;
    }

    private String getCampoIngresosExtranjeros(Declarante declarante)
    {
        return (declarante.getIndicadorRentasExtranjero() != null && declarante
                .getIndicadorRentasExtranjero().equals(1)) ? "S" : "N";
    }

    private String getCampoAutorizacion(Beca beca)
    {
        return beca.isAutorizacionRenta() ? "S" : "N";
    }

    private String getCampoIDSolicitud(Beca beca)
    {
        return beca.getCodigoArchivoTemporal();
    }

    private String getCampoNotaMedia(Beca beca, Boolean notaMediaObligatoria)
    {
        Float notaMedia;

        if (!notaMediaObligatoria && beca.getCurso() == null)
        {
            return "0000";
        }

        notaMedia = beca.getNotaMediaAnt();

        if (notaMedia == null)
        {
            return "0000";
        }

        notaMedia = notaMedia * 100;

        Integer notaMediaEntera = notaMedia.intValue();
        if (notaMediaEntera.toString().length() == 3)
        {
            return MessageFormat.format("0{0}", notaMediaEntera);
        }
        else
        {
            return notaMediaEntera.toString();
        }
    }

    private String getCampoNotaMediaCoefCorrector(Beca beca, Boolean notaMediaObligatoria)
    {
        Float notaMedia;
        Solicitante solicitante = beca.getSolicitante();

        if (!notaMediaObligatoria && beca.getCurso() == null)
        {
            return "0000";
        }

        notaMedia = beca.getNotaMediaAnt();

        if (notaMedia == null)
        {
            return "0000";
        }

        if (beca.getEstudio() == null)
        {
            return "0000";
        }

        PersonaEstudio academicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), beca.getEstudio().getId(), solicitante.getCursoAcademico().getId());

        // Calculo del coeficiente corrector de GVA
        Long cursoAcademicoId = solicitante.getCursoAcademico().getId();
        Long estudioId = beca.getEstudio().getId();
        Minimo minimo = Minimo.getMinimosByEstudioId(estudioId, cursoAcademicoId);

        Float coeficiente = minimo.getCoefCorrectorCons();
        if (academicos.isPrimeraVez()) {
            coeficiente = 1.0F;
        }

        notaMedia = notaMedia * coeficiente * 100;

        Integer notaMediaEntera = notaMedia.intValue();

        if (notaMediaEntera.toString().length() == 3)
        {
            return MessageFormat.format("0{0}", notaMediaEntera);
        }
        else
        {
            return notaMediaEntera.toString();
        }
    }

    private String getCampoNotaMediaCoefCorrectorManuela(Beca beca, Boolean notaMediaObligatoria)
    {
        Float notaMedia;
        Solicitante solicitante = beca.getSolicitante();

        if (!notaMediaObligatoria && beca.getCurso() == null)
        {
            return "0000";
        }

        notaMedia = beca.getNotaMediaAnt();

        if (notaMedia == null)
        {
            return "0000";
        }

        if (beca.getEstudio() == null)
        {
            return "0000";
        }

        PersonaEstudio academicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), beca.getEstudio().getId(), solicitante.getCursoAcademico().getId());

        // Calculo del coeficiente corrector de GVA Manuela



        String tipoEstudio = beca.getEstudio().getTipo();
        Float coeficiente;
        Long cursoAcademicoId = solicitante.getCursoAcademico().getId();

        if (academicos.isPrimeraVez() && (tipoEstudio.equals("G"))) {
            // Grados 1º 1ª vez
            // Para notas acceso PAU
            coeficiente = 1.0F;
        } else if (tipoEstudio.equals("M")) {
            // Master
            // Cogemos el coeficiente de la titulación de procedencia de la nota.
            String codigoEstudioUjiAnt = beca.getCodigoEstudioUjiAnt();
            if (codigoEstudioUjiAnt != null) {
                Long estudioProcedenciaId = Long.parseLong(codigoEstudioUjiAnt);
                Minimo minimo = Minimo.getMinimosByEstudioId(estudioProcedenciaId, cursoAcademicoId);
                coeficiente = minimo.getCoefCorrectorConsManuela();
            } else {
                coeficiente = 1.0F;
            }
        } else {
            // Grados 2º y posteriores
            // Cogemos el coeficiente del estudio
            Long estudioId = beca.getEstudio().getId();
            Minimo minimo = Minimo.getMinimosByEstudioId(estudioId, cursoAcademicoId);
            coeficiente = minimo.getCoefCorrectorConsManuela();
        }

        notaMedia = notaMedia * coeficiente * 100;

        Integer notaMediaEntera = notaMedia.intValue();

        if (notaMediaEntera.toString().length() == 3)
        {
            return MessageFormat.format("0{0}", notaMediaEntera);
        }
        else
        {
            return notaMediaEntera.toString();
        }
    }

    private String getCampoNotaMediaSuperadas(Beca beca, Boolean notaMediaObligatoria)
    {
        Float notaMediaSuperadas;

        if (!notaMediaObligatoria && beca.getCurso() == null)
        {
            return "0000";
        }

        notaMediaSuperadas = beca.getNotaMediaSinSuspenso();

        if (notaMediaSuperadas == null)
        {
            return "0000";
        }

        notaMediaSuperadas = notaMediaSuperadas * 100;

        Integer notaMediaEntera = notaMediaSuperadas.intValue();
        if (notaMediaEntera.toString().length() == 3)
        {
            return MessageFormat.format("0{0}", notaMediaEntera);
        }
        else
        {
            return notaMediaEntera.toString();
        }
    }

    private String getCampoSolicitaMEC(Solicitante solicitante)
    {
        List<Beca> listaBecas = new ArrayList<Beca>(solicitante.getBecas());
        return (listaBecas.size() > 1) ? "0" : "1";
    }

    private String getDatosUnidadFamiliarSolicitanteFormateado(Miembro miembro,
                                                               Solicitante solicitante)
    {
        StringBuilder datosUF = new StringBuilder();

        datosUF.append(Fichero.rellenaString(solicitante.getNumeroMiembrosComputables(), 2, "0", "left"));

        MapeadorTipoFamilia mapeadorTipoFamilia = new MapeadorTipoFamilia();

        Long tipoFamilia;
        if (solicitante.getTipoFamilia() == null)
        {
            tipoFamilia = 1L; // Familia Ordinaria
        }
        else
        {
            tipoFamilia = solicitante.getTipoFamilia().getId();
        }
        datosUF.append(Fichero.rellenaString(mapeadorTipoFamilia.getValorOrganismo(tipoFamilia, CodigoOrganismo.CONSELLERIA), 2, "0", "left"));
        datosUF.append(Fichero.rellenaString(solicitante.getNumeroHermanos(), 2, "0", "left"));
        datosUF.append(Fichero.rellenaString(solicitante.getNumeroHermanosFuera(), 2, "0", "left"));
        datosUF.append(Fichero.rellenaString(solicitante.getNumeroMinusvalia33(), 2, "0", "left"));
        datosUF.append(Fichero.rellenaString(solicitante.getNumeroMinusvalia65(), 2, "0", "left"));
        //   datosUF.append(Fichero.rellenaString(getSolicitanteMinusvalia33(miembro), 1, " ", "right"));
        //   datosUF.append(Fichero.rellenaString(getSolicitanteMinusvalia65(miembro), 1, " ", "right"));
        datosUF.append(Fichero.rellenaString(getOrfandadSolicitante(solicitante), 1, " ", "right"));
        datosUF.append(Fichero.rellenaString(miembro.getPais().getCodigoIso(), 3, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoFijo(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoMovil(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(solicitante.getEmail(), 50, " ", "right"));

        return datosUF.toString();
    }

    private String getDatosUnidadFamiliarSolicitanteFormateadoMec2crd(Miembro miembro,
                                                                      Solicitante solicitante)
    {
        StringBuilder datosUF = new StringBuilder();

        datosUF.append(Fichero.rellenaString(miembro.getPais().getCodigoIso(), 3, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoFijo(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoMovil(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(solicitante.getEmail(), 50, " ", "right"));

        return datosUF.toString();
    }

    private String getRestoDatosSolicitanteDenegado(Miembro miembro, Solicitante solicitante)
    {
        StringBuilder datosUF = new StringBuilder();

        datosUF.append(Fichero.rellenaString(miembro.getPais().getCodigoIso(), 3, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoFijo(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(getTelefonoMovil(solicitante), 9, " ", "right"));
        datosUF.append(Fichero.rellenaString(solicitante.getEmail(), 50, " ", "right"));

        return datosUF.toString();
    }


    private String getOrfandadSolicitante(Solicitante solicitante)
    {
        return solicitante.getOrfandad() ? "1" : "0";
    }

    private String getTelefonoMovil(Solicitante solicitante)
    {
        if (solicitante.getTelefono2() == null || solicitante.getTelefono2().isEmpty())
        {
            return "000000000";
        }
        else
        {
            return solicitante.getTelefono2();
        }
    }

    private String getTelefonoFijo(Solicitante solicitante)
    {
        if (solicitante.getTelefono1() == null || solicitante.getTelefono1().isEmpty())
        {
            return "000000000";
        }
        else
        {
            return solicitante.getTelefono1();
        }
    }

    private String getSolicitanteMinusvalia65(Miembro miembro)
    {
        String resultado = "0";
        if (miembro.getTipoMinusvalia() != null)
        {
            return miembro.getTipoMinusvalia().getNombre().equals("65") ? "1" : "0";
        }
        return resultado;

    }

    private String getSolicitanteMinusvalia33(Miembro miembro)
    {
        String resultado = "0";

        if (miembro.getTipoMinusvalia() != null)
        {
            resultado = miembro.getTipoMinusvalia().getNombre().equals("33") ? "1" : "0";
        }

        return resultado;
    }

    private String getCabecera(Tanda tanda)
    {
        StringBuilder cabecera = new StringBuilder();
        String tipoRegistro = "1";
        String universidad = "40";
        Long year = tanda.getCursoAcademico().getId();
        Long nextYear = year + 1;
        String cursoAcademico = year.toString().substring(2, 4)
                + nextYear.toString().substring(2, 4);
        String convocatoria = "1";
        String lote = tanda.getTandaId().toString();
        if (tanda.getTandaId().longValue() > 999L)
        {
            lote = tanda.getTandaId().toString().substring(1, 4);
        }
        SimpleDateFormat sf = new SimpleDateFormat("YYYYMMdd");
        String fecha = sf.format(new Date());

        cabecera.append(tipoRegistro);
        cabecera.append(universidad);
        cabecera.append(cursoAcademico);
        cabecera.append(Fichero.rellenaString(convocatoria, 2, "0", "left"));
        cabecera.append(lote);
        cabecera.append(fecha);

        return cabecera.toString();
    }

    public Map<String, String> getFicheroTodas(Long cursoAcademicoId)
    {
        StringBuffer datos = new StringBuffer();

        List<TodasConselleria> listaTodasConselleria = TodasConselleria.getDatosByCursoAcademicoId(cursoAcademicoId);

        datos.append(getCabeceraFicheroTodas(cursoAcademicoId));

        for (TodasConselleria datosBeca : listaTodasConselleria)
        {
            datos.append(saltoDeLinea());
            datos.append("2"); // Tipo de registro
            datos.append(Fichero.rellenaString(datosBeca.getNif(), 9, "0", "right"));
            datos.append(Fichero.rellenaString(datosBeca.getApellido1(), 24, " ", "right"));
            datos.append(Fichero.rellenaString(datosBeca.getApellido2(), 24, " ", "right"));
            datos.append(Fichero.rellenaString(datosBeca.getNombre(), 24, " ", "right"));
            datos.append(Fichero.rellenaString(datosBeca.getLocalidadId().substring(2, 5), 3, " ", "right"));
            datos.append(Fichero.rellenaString(datosBeca.getCodigoPostal(), 5, " ", "right"));
            datos.append(Fichero.rellenaString(calculaCreditosMatriculados1a(datosBeca), 5, "0", "left"));
            datos.append(Fichero.rellenaString(calculaImporteCrd1a(datosBeca), 6, "0", "left"));
            datos.append(Fichero.rellenaString(calculaCreditosMatriculados2a(datosBeca), 5, "0", "left"));
            datos.append(Fichero.rellenaString(calculaImporteCrd2a(datosBeca), 6, "0", "left"));
            String concedida = datosBeca.isBecaMecConcedida() ? "1" : "0";
            datos.append(Fichero.rellenaString(concedida, 1, "0", "right"));

            if (datosBeca.getEstadoId() == 3 || datosBeca.getEstadoId() == 6)
            {
                Map<Long, Map> mapaDenegaciones = getMapaDenegaciones();

                rellenaDenegacionFicheroTodas(datosBeca.getDen1(), datosBeca, mapaDenegaciones, datos);
                rellenaDenegacionFicheroTodas(datosBeca.getDen2(), datosBeca, mapaDenegaciones, datos);
                rellenaDenegacionFicheroTodas(datosBeca.getDen3(), datosBeca, mapaDenegaciones, datos);
                rellenaDenegacionFicheroTodas(datosBeca.getDen4(), datosBeca, mapaDenegaciones, datos);
                rellenaDenegacionFicheroTodas(datosBeca.getDen5(), datosBeca, mapaDenegaciones, datos);
                rellenaDenegacionFicheroTodas(datosBeca.getDen6(), datosBeca, mapaDenegaciones, datos);
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("conselleriaTodas{0,number,#}.txt", cursoAcademicoId));
        return fichero;
    }

    private void rellenaDenegacionFicheroTodas(Long denegacionNumero, TodasConselleria datosBeca, Map<Long, Map> mapaDenegaciones, StringBuffer datos) {
        if (denegacionNumero != null)
        {
            datos.append(saltoDeLinea());
            datos.append("3"); // Tipo de registro
            datos.append(Fichero.rellenaString(datosBeca.getNif(), 9, "0", "right"));
            datos.append(addCodigoDenegacion(denegacionNumero, mapaDenegaciones));
        }
    }

    private String getCabeceraFicheroTodas(Long cursoAcademicoId)
    {
        StringBuilder cabecera = new StringBuilder();
        String tipoRegistro = "1";
        String universidad = "40";
        Long year = cursoAcademicoId;
        Long nextYear = year + 1;
        String cursoAcademico = year.toString().substring(2, 4) + nextYear.toString().substring(2, 4);
        String convocatoria = "1";

        SimpleDateFormat sf = new SimpleDateFormat("YYYYMMdd");
        String fecha = sf.format(new Date());

        cabecera.append(tipoRegistro);
        cabecera.append(universidad);
        cabecera.append(cursoAcademico);
        cabecera.append(Fichero.rellenaString(convocatoria, 2, "0", "left"));
        cabecera.append(fecha);

        return cabecera.toString();
    }

    private String addCodigoDenegacion(Long denegacionId, Map<Long, Map> mapaDenegaciones)
    {
        StringBuilder resultado = new StringBuilder();

        if (denegacionId != null)
        {
            Map<String, String> mapaDenegacion = mapaDenegaciones.get(denegacionId);
            String codigoConselleria = mapaDenegacion.get("codigoConselleria");
            resultado.append(Fichero.rellenaString(codigoConselleria, 3, "0", "left"));
        }

        return resultado.toString();
    }

    private String getFechaNacimiento(TodasConselleria datosBeca)
    {
        Date fecha = datosBeca.getFechaNacimiento();
        SimpleDateFormat sf = new SimpleDateFormat("YYMMdd");

        return sf.format(fecha);
    }

    private Long getCampoNotaMedia(TodasConselleria datosBeca)
    {
        if (datosBeca.getNotaMedia() != null)
        {
            return datosBeca.getNotaMedia().multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }



    private Long calculaCreditosMatriculados1a(TodasConselleria datosBeca)
    {
        return datosBeca.getCreditos1a().multiply(BigDecimal.valueOf(100)).longValue();
    }

    private Long calculaImportes3a(Beca beca, Solicitante solicitante)
    {
        PersonaEstudio personaEstudio = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), beca.getEstudio().getId(), solicitante.getCursoAcademico().getId());

        if (personaEstudio.getImportes3a() != null)
        {
            return BigDecimal.valueOf(personaEstudio.getImportes3a()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaImportes4ap(Beca beca, Solicitante solicitante)
    {
        PersonaEstudio personaEstudio = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), beca.getEstudio().getId(), solicitante.getCursoAcademico().getId());

        if (personaEstudio.getImportes4ap() != null)
        {
            return BigDecimal.valueOf(personaEstudio.getImportes4ap()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaImporteCrd1a(TodasConselleria datosBeca)
    {
        if ((datosBeca.getImporte1a() != null) && (!datosBeca.isBecaMecConcedida()))
        {
            return datosBeca.getImporte1a().multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculados2a(TodasConselleria datosBeca)
    {
        return datosBeca.getCreditos2a().multiply(BigDecimal.valueOf(100)).longValue();
    }

    private Long calculaImporteCrd2a(TodasConselleria datosBeca)
    {
        if (datosBeca.getImporte2a() != null)
        {
            return datosBeca.getImporte2a().multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculadosGVAAltres(Beca beca)
    {
        if (beca.getCreditosMatriculados() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosMatriculados()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosParaBeca(Beca beca)
    {
        if (beca.getCreditosParaBeca() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosParaBeca()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculadosCursoAnterior(Beca beca)
    {
        if (beca.getCreditosMatriculadosAnt() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosMatriculadosAnt()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculadosAntGVAAltres(Beca beca)
    {
        if (beca.getCreditosMatriculadosAnt() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosMatriculadosAnt()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosSuperadosAntGVAAltres(Beca beca)
    {
        if (beca.getCreditosSuperadosAnt() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosSuperadosAnt()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosParaFinalizar(Beca beca, Solicitante solicitante)
    {
        if (beca.getCreditosMatriculados() != null)
        {
            PersonaEstudio personaEstudio = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(solicitante.getPersona().getId(), beca.getEstudio().getId(), solicitante.getCursoAcademico().getId());

            return BigDecimal.valueOf(personaEstudio.getCreditosFaltantes()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculados3a(Beca beca)
    {
        if (beca.getCreditosTerceraMatricula() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosTerceraMatricula()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private Long calculaCreditosMatriculados4ap(Beca beca)
    {
        if (beca.getCreditosCuartaMatriculaPosteriores() != null)
        {
            return BigDecimal.valueOf(beca.getCreditosCuartaMatriculaPosteriores()).multiply(BigDecimal.valueOf(100)).longValue();
        }
        else
        {
            return 0L;
        }
    }

    private  boolean cumpleConcicionesMatricula(Beca beca)
    {
        Minimo minimos = beca.calculaCreditosMinimos();

        return ((beca.getCreditosParaBeca() >= minimos.getCreditosBecaCompleta()) || beca.isLimitarCreditosFinEstudios());
    }

    private  boolean cumpleConcicionesMatriculaCursoAnterior(Beca beca)
    {
        Minimo minimos = beca.calculaCreditosMinimos();

        return ((beca.getCreditosMatriculadosAnt() >= minimos.getCreditosBecaCompleta()) || beca.isLimitarCreditosFinEstudios());
    }

    private String getCodigoUniversidad()
    {
        return CODIGO_UNIVERSIDAT_JAUME_ID;
    }

    public Map<Long, Map> getMapaDenegaciones()
    {
        Map<Long, Map> mapaDenegaciones = new HashMap<Long, Map>();

        List<Denegacion> listaDenegaciones = Denegacion.getDenegacionesTodasByOrganismo(CodigoOrganismo.CONSELLERIA.getId());

        for (Denegacion denegacion : listaDenegaciones)
        {
            Map<String, String> mapaDenegacion = new HashMap();
            mapaDenegacion.put("codigo", denegacion.getCausa());
            mapaDenegacion.put("codigoConselleria", denegacion.getCodigoConselleria());
            mapaDenegacion.put("nombre", denegacion.getNombre());
            mapaDenegaciones.put(denegacion.getId(), mapaDenegacion);
        }

        return mapaDenegaciones;
    }

    public Map<String, String> getFicheroDenegacionesByTandaId(Long tandaId)
            throws WsErrorAlPrepararMultienvioException,
            DomicilioIncorrectoException, DNIIncorrectoException, DomicilioNoEncontradoException,
            MiembroIncorrectoException, FaltaNotaMediaException, BecaException
    {
        StringBuffer datos = new StringBuffer();
        Tanda tanda = Tanda.getTandaById(tandaId);

        tanda.isCorrectaParaEnviar();

        datos.append(getCabecera(tanda));
        datos.append(saltoDeLinea());

        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            beca.compruebaEstadoDenegadaUJI();
            Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());
            Miembro miembro = Miembro.getMiembroSolicitante(beca.getId());
            datos.append(getRegistroSolicitante(beca, miembro));
            datos.append(getRestoDatosSolicitanteDenegado(miembro, solicitante));
            datos.append(getAcademicosSolicitanteDenegado(beca, solicitante));
            datos.append(saltoDeLinea());

            List<BecaDenegacion> listaBecaDenegacion = BecaDenegacion.getDenegacionesByBecaId(beca.getId());
            for (BecaDenegacion becaDenegacion : listaBecaDenegacion)
            {
                datos.append(getRegistroDenegacion(miembro, becaDenegacion.getDenegacion()));
                datos.append(saltoDeLinea());
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}conselleriaDEN{1,number,#}.txt", tanda.getCursoAcademico().getId(), tanda.getTandaId()));
        return fichero;
    }

    public Map<String, String> getFicheroDenegacionesFinalizacionByTandaId(Long tandaId)
            throws WsErrorAlPrepararMultienvioException, DNIIncorrectoException, BecaException
    {
        StringBuffer datos = new StringBuffer();

        Tanda tanda = Tanda.getTandaById(tandaId);
        tanda.isCorrectaParaEnviar();

        for (Beca beca : Beca.getBecasByTandaId(tanda.getId()))
        {
            beca.compruebaEstadoDenegadaUJI();

            String codigoArchivoTemporal = beca.getCodigoArchivoTemporal().toString();
            datos.append(Fichero.rellenaString(codigoArchivoTemporal, 20, " ", "right"));

            Miembro miembro = Miembro.getMiembroSolicitante(beca.getId());

            String dni = beca.getIdentificacion();

            if (dni.length() != 9)
            {
                throw new DNIIncorrectoException();
            }

            String nombre = miembro.getNombre();
            String apellido1 = miembro.getApellido1();
            String apellido2 = miembro.getApellido2();

            datos.append(Fichero.rellenaString(dni, 9, " ", "right"));
            datos.append(Fichero.rellenaString(apellido1, 24, " ", "right"));
            datos.append(Fichero.rellenaString(apellido2, 24, " ", "right"));
            datos.append(Fichero.rellenaString(nombre, 24, " ", "right"));

            List<BecaDenegacion> listaBecaDenegacion = BecaDenegacion.getDenegacionesByBecaId(beca.getId());

            String listaCodigosDenegacion = listaBecaDenegacion.stream()
                    .map(BecaDenegacion::getDenegacion)
                    .map(Denegacion::getId)
                    .map(Object::toString)
                    .collect(Collectors.joining(","));

            datos.append(Fichero.rellenaString(listaCodigosDenegacion, 75, " ", "right"));
            datos.append(saltoDeLinea());
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}conselleriaDEN{1,number,#}.txt", tanda.getCursoAcademico().getId(), tanda.getTandaId()));
        return fichero;
    }


    private String getRegistroDenegacion(Miembro miembro, Denegacion denegacion) throws DNIIncorrectoException
    {
        StringBuffer registro = new StringBuffer();
        registro.append("3");

        String dni = miembro.getIdentificacion();
        if (dni.length() != 9)
        {
            throw new DNIIncorrectoException();
        }
        registro.append(Fichero.rellenaString(dni, 9, " ", "right"));
        registro.append(Fichero.rellenaString(denegacion.getCausa(), 3, "0", "right"));

        return registro.toString();
    }

    public String procesaRespuestaManuelaSolisDenegadas(byte[] ficheroConselleria, String usuarioConectado)
            throws UnsupportedEncodingException, BecaDenegacionDuplicadaException, BecaException {
        String archivo = new String(ficheroConselleria, "ISO-8859-1");
        String mensaje = "";
        String[] lineasArray = archivo.split(System.getProperty("line.separator"));

        List<String> lineas = Arrays.asList(lineasArray);
        compruebaNumeroColumnasFichero(lineas.get(0), NUMERO_CAMPOS_CSV_MANUELA_DENEGADAS);

        List<FicheroConselleriaManuelaSolisDenegadas> registrosFichero;
        try {
            registrosFichero= lineas.stream()
                .map(FicheroConselleriaManuelaSolisDenegadas::new)
                .collect(Collectors.toList());

            } catch (Exception e) {
            throw new BecaException("El format del fitxer no és correcte");
        }

        for (FicheroConselleriaManuelaSolisDenegadas registro : registrosFichero)
        {
            Beca beca = Beca.getBecaByArchivoTemporal(registro.getCodigoArchivoTemporal());
            if (beca != null)
            {
                Beca becaDataBase = SerializationUtils.clone(beca);
                beca.borraAyudas();
                beca.borraDenegacionesOrdenUno();
                for (String denegacion : registro.getDenegaciones())
                {
                    try {
                        insertaDenegacionesPorBecaYCodigoConselleria(beca, denegacion, CodigoOrganismo.GVA_ALTRES);
                    } catch (Exception e) {
                        throw new BecaException("La denegació " + denegacion + " de la beca " + beca.getId() + " no existeix.");
                    }
                }
                beca.cambiaEstado(BECA_ESTADO_DENEGADA);
                beca.cambiaANoConcedida();
                beca.updateEstado();

                HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
            }
        }

        mensaje = "Fitxer processat amb " + registrosFichero.size() + " beques.";

        return mensaje;
    }

    private static void compruebaNumeroColumnasFichero(String lineaPrimera, int numeroCamposFicheroCorrecto) throws BecaException {
        int numeroCamposFicheroEnviado = lineaPrimera.trim().split(";").length;
        if (numeroCamposFicheroEnviado != numeroCamposFicheroCorrecto)
        {
            throw new BecaException("El format del fitxer no és correcte");
        }
    }

    public String procesaRespuestaManuelaSolisConcedidas(byte[] ficheroConselleria, String usuarioConectado)
            throws UnsupportedEncodingException, BecaCuantiaDuplicadaException, BecaException {

        String archivo = new String(ficheroConselleria, "ISO-8859-1");
        String mensaje = "";
        String[] lineasArray = archivo.split(System.getProperty("line.separator"));

        List<String> lineas = Arrays.asList(lineasArray);
        compruebaNumeroColumnasFichero(lineas.get(0), NUMERO_CAMPOS_CSV_MANUELA_CONCEDIDAS);

        List<FicheroConselleriaManuelaSolisConcedidas> registrosFichero;
        try {
            registrosFichero = lineas.stream()
                    .map(FicheroConselleriaManuelaSolisConcedidas::new)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new BecaException("El format del fitxer no és correcte");
        }

        for (FicheroConselleriaManuelaSolisConcedidas registro : registrosFichero)
        {
            Beca beca = Beca.getBecaByArchivoTemporal(registro.getCodigoArchivoTemporal());
            if (beca != null)
            {
                Beca becaDataBase = SerializationUtils.clone(beca);
                System.out.println("Beca encontrada: " + beca.getId());
                beca.setIbanPais(registro.getIban());
                beca.setEntidad(registro.getEntidad());
                beca.setSucursal(registro.getOficina());
                beca.setIbanDC(registro.getDc());
                beca.setCuentaBancaria(registro.getCuenta());

                beca.borraAyudas();
                beca.borraDenegacionesOrdenUno();
                beca.insertaAyudaConImporte(CodigoCuantia.GVA_MANUELA, registro.getImporte());
                beca.cambiaEstado(BECA_ESTADO_CONCEDIDA);
                beca.cambiaAConcedida();
                beca.updateEstado();

                HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
            }
        }

        mensaje = "Fitxer processat amb " + registrosFichero.size() + " beques.";
        return mensaje;
    }

    public String procesaRespuesta(byte[] ficheroConselleria, String usuarioConectado)
            throws UnsupportedEncodingException, BecaException, BecaDuplicadaException,
            BecaDenegacionDuplicadaException
    {
        String archivo = new String(ficheroConselleria, "ISO-8859-1");
        String mensaje = "";
        String[] lineasArray = archivo.split(System.getProperty("line.separator"));

        List<String> lineas = Arrays.asList(lineasArray);

        Map<String, String> cabecera = extraeInformacionCabeceraRespuestaConselleria(lineas.get(0));

        if (cabecera.get("tipoRegistro").equals(1))
        {
            throw new BecaException("Tipo de archivo incorrecto");
        }

        Integer contador = 0;
        Integer contadorErrores = 0;

        Long cursoAcademico = getCursoAcademicoFromCabeceraConselleria(cabecera.get("curso"));

        System.out.println("Extrayendo mapa denegaciones tipo 3...");
        Map<String, List> motivosDenegacion = extraeMapaMotivosDenegacion(lineas);


        System.out.println("Cargando fichero tipo 2...");
        for (String linea : lineas.subList(1, lineas.size()))
        {
            String codigo = linea.substring(0, 1);

            if (codigo.equals("2"))
            {
                contador++;

                Map<String, String> infoSolicitante = extraeInformacionSolicitanteRespuestaConselleria(linea);
                String estado = infoSolicitante.get("estado");

                String nif = infoSolicitante.get("nif");
                Long convocatoria = CodigoConvocatoria.CONSELLERIA.getId();

                System.out.println(contador.toString() + " " + nif);
                Beca beca = null;
                Beca becaDataBase = null;
                try
                {
                    beca = Beca.getBecasByConvocatoriaIdAndNifAndCursoAcademico(convocatoria, nif, cursoAcademico);
                    becaDataBase = SerializationUtils.clone(beca);
                }
                catch (Exception e)
                {
                    System.out.println("Error al obtener la Beca " + nif);
                    mensaje = mensaje + "Error en obtindre la beca " + nif + "\n";
                    contadorErrores++;
                    continue;
                }

                if (estado.equals(BECA_ACEPTADA))
                {
                    procesaBecaAdjudicada(beca);
                    beca.cambiaAConcedida();
                }
                else
                {
                    if (estado.equals(BECA_DENEGADA))
                    {
                        List<Map> listaMotivosDenegacion = motivosDenegacion.get(infoSolicitante.get("nif"));
                        try
                        {
                            procesaBecaDenegada(beca, listaMotivosDenegacion);
                            beca.cambiaANoConcedida();
                        } catch (Exception e)
                        {
                            System.out.println("Error al procesar el motivo de denegación de la beca " + nif);
                            mensaje = mensaje + "Error en processar el motiu de la denegació " + nif + "-" + listaMotivosDenegacion.toString() + "\n";
                            contadorErrores++;
                            continue;
                        }
                    }
                    else
                    {
                        if (estado.equals(BECA_JUSTIFICAR))
                        {
                            procesaBecaJustificar(beca, infoSolicitante.get("situacion"));
                        }
                    }
                }

                HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
            }
        }

        mensaje = mensaje + "Processadas " + contador + " beques. " + contadorErrores + " errors.\n";
        return mensaje;
    }

    private Long getCursoAcademicoFromCabeceraConselleria(String curso)
    {
        String cursoAcademico = MessageFormat.format("20{0}", curso.substring(0, 2));
        return ParamUtils.parseLong(cursoAcademico);
    }

    private Map<String, List> extraeMapaMotivosDenegacion(List<String> lineasArchivo)
    {
        Map<String, List> mapaMotivosDenegacion = new HashMap<String, List>();

        for (String linea : lineasArchivo)
        {
            String codigo = linea.substring(0, 1);
            if (codigo.equals("3"))
            {
                String nif = linea.substring(1, 10);
                String motivo = linea.substring(10, 13);

                if (!mapaMotivosDenegacion.containsKey(nif))
                {
                    List<Map> listaRegistros = new ArrayList<Map>();
                    mapaMotivosDenegacion.put(nif, listaRegistros);
                }

                Map<String, String> registro = getRegistroMotivoDenegacion(motivo);
                mapaMotivosDenegacion.get(nif).add(registro);
            }
        }

        return mapaMotivosDenegacion;
    }

    private Map<String, String> getRegistroMotivoDenegacion(String motivo)
    {
        Map<String, String> registro = new HashMap<String, String>();
        registro.put("motivo", motivo);
        return registro;
    }

    @Transactional
    private void procesaBecaAdjudicada(Beca beca) throws BecaException, BecaDuplicadaException
    {
        borraDenegaciones(beca.getId());
        borraAyudas(beca.getId());
        Estado estado = Estado.getEstadoById(CodigoEstado.CONCEDIDA.getId());
        beca.setEstado(estado);
        beca.updateEstado();
    }

    private void procesaBecaDenegada(Beca beca, List<Map> motivosDenegacion) throws BecaException,
            BecaDuplicadaException, BecaDenegacionDuplicadaException
    {
        borraDenegaciones(beca.getId());
        for (Map<String, String> registro : motivosDenegacion)
        {
            insertaDenegacionesPorBecaYCodigoConselleria(beca, registro.get("motivo"), CodigoOrganismo.CONSELLERIA);
        }
        borraAyudas(beca.getId());

        Estado estado = Estado.getEstadoById(CodigoEstado.DENEGADA.getId());
        beca.setEstado(estado);
        beca.updateEstado();
    }

    private void borraDenegaciones(Long becaId)
    {
        List<BecaDenegacion> denegacionesBeca = BecaDenegacion.getDenegacionesByBecaId(becaId);

        for (BecaDenegacion denegacion : denegacionesBeca)
        {
            if (denegacion.getOrdenDenegacion() == 1L)
            {
                BecaDenegacion.delete(denegacion.getId());
            }
        }
    }

    private void borraAyudas(Long becaId)
    {
        List<CuantiaBeca> cuantiasBeca = CuantiaBeca.getCuantiasBecaByBecaId(becaId);

        for (CuantiaBeca cuantia : cuantiasBeca)
        {
            CuantiaBeca.delete(cuantia.getId());
        }
    }

    private void insertaDenegacionesPorBecaYCodigoConselleria(Beca beca, String motivo, CodigoOrganismo organismo)
            throws BecaDenegacionDuplicadaException, BecaException {

        Denegacion denegacion;
        try
        {
            denegacion = Denegacion.getDenegacionByOrganismoYCausa(organismo.getId(), motivo);
        }
        catch (Exception e)
        {
            throw new BecaException("El códi " + motivo + " de denegació no existeix o hi ha més d'un resultat");
        }

        BecaDenegacion becaDenegacion = new BecaDenegacion();
        becaDenegacion.setBeca(beca);
        becaDenegacion.setDenegacion(denegacion);
        becaDenegacion.setOrdenDenegacion(1L);
        becaDenegacion.insert();
    }

    private void procesaBecaJustificar(Beca beca, String situacion) throws BecaException,
            BecaDuplicadaException
    {
        Estado estado = Estado.getEstadoById(CodigoEstado.PENDIENTE.getId());
        beca.setObservacionesUji(situacion);
        beca.setEstado(estado);
        beca.updateEstado();
        beca.updateObservacionesUji();
    }

    private Map<String, String> extraeInformacionSolicitanteRespuestaConselleria(String linea)
    {
        Map<String, String> solicitante = new HashMap<String, String>();

        solicitante.put("tipoRegistro", linea.substring(0, 1).trim());
        solicitante.put("nif", linea.substring(1, 10).trim());
        solicitante.put("apellido1", linea.substring(10, 34).trim());
        solicitante.put("apellido2", linea.substring(34, 58).trim());
        solicitante.put("nombre", linea.substring(58, 82).trim());
        solicitante.put("tipoVia", linea.substring(82, 84).trim());
        solicitante.put("nombreVia", linea.substring(84, 134).trim());
        solicitante.put("numeroVia", linea.substring(134, 138).trim());
        solicitante.put("escalera", linea.substring(138, 140).trim());
        solicitante.put("piso", linea.substring(140, 142).trim());
        solicitante.put("puerta", linea.substring(142, 144).trim());
        solicitante.put("localidad", linea.substring(144, 147).trim());
        solicitante.put("codigoPostal", linea.substring(147, 152).trim());
        solicitante.put("provincia", linea.substring(152, 154).trim());
        solicitante.put("estado", linea.substring(154, 155).trim());
        solicitante.put("situacion", linea.substring(155).trim());

        return solicitante;
    }

    private Map<String, String> extraeInformacionCabeceraRespuestaConselleria(String linea)
    {
        Map<String, String> cabecera = new HashMap<String, String>();

        cabecera.put("tipoRegistro", linea.substring(0, 1).trim());
        cabecera.put("universidad", linea.substring(1, 3).trim());
        cabecera.put("curso", linea.substring(3, 7).trim());
        cabecera.put("convocatoria", linea.substring(7, 9).trim());
        cabecera.put("lote", linea.substring(9, 12).trim());
        cabecera.put("fechaEnvio", linea.substring(12, 20).trim());
        return cabecera;
    }

    public Map<String, Object> getEstructuraBecasDenegadasPorCursoAcademico(Long cursoAcademicoId)
    {
        Map<String, Object> datos = new HashMap<String, Object>();

        List<TodasConselleria> listaBecas = TodasConselleria.getDatosDenegadasByCursoAcademicoId(cursoAcademicoId);

        Map<String, Object> datosAcumulados = getDatosAcumulados(listaBecas);

        Map<Long, Map> mapaDenegaciones = getMapaDenegaciones();
        datos.put("becas", listaBecas);
        datos.put("numBecasDenegadas", datosAcumulados.get("numBecasDenegadas"));
        datos.put("mapaDenegaciones", mapaDenegaciones);

        return datos;
    }

    public Map<String, Object> getEstructuraBecasConcedidasPorCursoAcademico(Long cursoAcademicoId)
    {
        Map<String, Object> datos = new HashMap<String, Object>();

        List<TodasConselleria> listaBecas = TodasConselleria.getDatosConcedidasByCursoAcademicoId(cursoAcademicoId);

        Map<String, Object> datosAcumulados = getDatosAcumulados(listaBecas);

        datos.put("becas", listaBecas);
        datos.put("numBecasAceptadas", datosAcumulados.get("numBecasAceptadas"));
        datos.put("totalSinBecaMEC", datosAcumulados.get("totalSinBecaMEC"));
        datos.put("totalConBecaMEC", datosAcumulados.get("totalConBecaMEC"));
        datos.put("importeTotal", datosAcumulados.get("importeTotal"));
        datos.put("importePrimeraNoMEC", datosAcumulados.get("importePrimeraNoMEC"));
        datos.put("importeSegundaNoMEC", datosAcumulados.get("importeSegundaNoMEC"));
        datos.put("importeSegundaMEC", datosAcumulados.get("importeSegundaMEC"));

        return datos;
    }

    private Map<String, Object> getDatosAcumulados(List<TodasConselleria> listaBecas)
    {
        Map<String, Object> datosAcumulados = new HashMap<>();

        Long numBecasDenegadas = 0L;
        Long numBecasAceptadas = 0L;
        Long totalSinBecaMEC = 0L;
        Long totalConBecaMEC = 0L;
        BigDecimal importeTotal = new BigDecimal(0);
        BigDecimal importePrimeraNoMEC = new BigDecimal(0);
        BigDecimal importeSegundaNoMEC = new BigDecimal(0);
        BigDecimal importeSegundaMEC = new BigDecimal(0);

        for (TodasConselleria beca : listaBecas)
        {
            if (beca.getEstadoId().equals(CodigoEstado.CONCEDIDA.getId()))
            {
                numBecasAceptadas++;

                if (beca.isBecaMecConcedida())
                {
                    totalConBecaMEC++;
                }
                else
                {
                    totalSinBecaMEC++;
                }

                importeTotal = importeTotal.add(beca.getTasasTotal());

                if (beca.isBecaMecConcedida())
                {
                    if (beca.getImporte2a() != null)
                    {
                        importeSegundaMEC = importeSegundaMEC.add(beca.getImporte2a());
                    }
                }
                else
                {
                    if (beca.getImporte1a() != null)
                    {
                        importePrimeraNoMEC = importePrimeraNoMEC.add(beca.getImporte1a());
                    }

                    if (beca.getImporte2a() != null)
                    {
                        importeSegundaNoMEC = importeSegundaNoMEC.add(beca.getImporte2a());
                    }
                }
            }

            if (beca.getEstadoId().equals(CodigoEstado.DENEGADA.getId())
                    || beca.getEstadoId().equals(CodigoEstado.DENEGADA_ACADEMICOS.getId()))
            {
                numBecasDenegadas++;
            }
        }
        datosAcumulados.put("numBecasDenegadas", numBecasDenegadas);
        datosAcumulados.put("numBecasAceptadas", numBecasAceptadas);
        datosAcumulados.put("totalSinBecaMEC", totalSinBecaMEC);
        datosAcumulados.put("totalConBecaMEC", totalConBecaMEC);
        datosAcumulados.put("importeTotal", importeTotal);
        datosAcumulados.put("importePrimeraNoMEC", importePrimeraNoMEC);
        datosAcumulados.put("importeSegundaNoMEC", importeSegundaNoMEC);
        datosAcumulados.put("importeSegundaMEC", importeSegundaMEC);
        return datosAcumulados;
    }

    private Long countNumBecasAceptadas(List<TodasConselleria> listaBecas)
    {
        Long aceptadas = 0L;
        for (TodasConselleria beca : listaBecas)
        {
            if (beca.getEstadoId().equals(CodigoEstado.CONCEDIDA.getId()))
            {
                aceptadas++;
            }

        }
        return aceptadas;
    }

    private Long countNumBecasDenegadas(List<TodasConselleria> listaBecas)
    {
        Long denegadas = 0L;
        for (TodasConselleria beca : listaBecas)
        {
            if (beca.getEstadoId().equals(CodigoEstado.DENEGADA.getId())
                    || beca.getEstadoId().equals(CodigoEstado.DENEGADA_ACADEMICOS.getId()))
            {
                denegadas++;
            }
        }
        return denegadas;
    }
}