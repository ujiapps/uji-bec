package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class EstadoBecaNoPermiteBajarDelMinisteriorException extends CoreDataBaseException
{
    public EstadoBecaNoPermiteBajarDelMinisteriorException()
    {
        super("No es poden baixar les dades del ministeri en l\'estat actual de la beca.");
    }

    public EstadoBecaNoPermiteBajarDelMinisteriorException(String message)
    {
        super(message);
    }
}
