package es.uji.apps.bec.dao.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class BecaControlParaPropuesta
{
    private GetBecaControlProcedure getBecaControl;

    private static DataSource dataSource;

    private String sql;

    public BecaControlParaPropuesta()
    {
    }

    public BecaControlParaPropuesta(String sql)
    {
        this.sql = sql;
    }

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        BecaControlParaPropuesta.dataSource = dataSource;
    }

    public void init()
    {
        this.getBecaControl = new GetBecaControlProcedure(dataSource);
    }

    public String getBecaControl(Long becaId)
    {
        return getBecaControl.execute(becaId);
    }

    private class GetBecaControlProcedure extends StoredProcedure
    {
        private static final String FUNCION = "beca_control_para_propuesta";
        private static final String PBECA = "p_beca_id";

        public GetBecaControlProcedure(DataSource dataSource)
        {
            setDataSource(dataSource);
            setFunction(true);
            setSql(FUNCION);
            declareParameter(new SqlOutParameter("resultado", Types.VARCHAR));
            declareParameter(new SqlParameter(PBECA, Types.BIGINT));
            compile();
        }

        public String execute(Long becaId)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PBECA, becaId);

            Map<String, Object> results = execute(inParams);
            String resultado = (String) results.get("resultado");
            return resultado;
        }
    }
}
