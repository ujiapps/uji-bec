package es.uji.apps.bec.dao.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.QTuple;
import es.uji.apps.bec.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DocumentoDAODatabaseImpl extends BaseDAODatabaseImpl implements DocumentoDAO
{
    private QDocumento qDocumento = QDocumento.documento;

    @Override
    public List<Tuple> getDocumentosByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDocumento).where(qDocumento.beca.id.eq(becaId));

        return query.list(new QTuple(qDocumento.tipoDocumento.id, qDocumento.fechaActualiza,
                qDocumento.beca.id, qDocumento.validado, qDocumento.id, qDocumento.mimeType,
                qDocumento.nombre, qDocumento.fechaSolicita));
    }

    @Override
    @Transactional
    public void updateDocumento(Documento documento)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qDocumento);

        updateClause.where(qDocumento.id.eq(documento.getId()))
                .set(qDocumento.fechaActualiza, documento.getFechaActualiza())
                .set(qDocumento.fechaSolicita, documento.getFechaSolicita())
                .set(qDocumento.mimeType, documento.getMimeType())
                .set(qDocumento.nombre, documento.getNombre())
                .set(qDocumento.validado, documento.isValidado())
                .set(qDocumento.tipoDocumento, documento.getTipoDocumento()).execute();
        ;
    }

    @Override
    @Transactional
    public void borrarDocumento(Long documentoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qDocumento);
        delete.where(qDocumento.id.eq(documentoId)).execute();
    }
}
