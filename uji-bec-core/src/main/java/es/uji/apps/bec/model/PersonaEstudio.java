package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.PersonaEstudioDAO;
import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.exceptions.RamaNoExisteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * The persistent class for the BC2_EXT_PERSONAS_ESTUDIOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "BC2_EXT_PERSONAS_ESTUDIOS")
@Component
public class PersonaEstudio implements Serializable
{
    private static final Float CREDITOS_EXTRA_RENDIMIENTO = 12F;
    private static final int CURSO_ACADEMICO_CAMBIO_CONDICION_BECARIO = 2012;
    private static PersonaEstudioDAO personaEstudioDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long becario;

    @Column(name = "CREDITOS_FALTANTES")
    private Float creditosFaltantes;

    @Column(name = "CREDITOS_FUERA_BECA")
    private Float creditosFueraBeca;
    @Column(name = "CREDITOS_MATRICULADOS")
    private Float creditosMatriculados;
    @Column(name = "CREDITOS_PENDIENTES_CONVA")
    private Float creditosPendientesConva;
    @Column(name = "CREDITOS_PRESENTADOS")
    private Float creditosPresentados;
    @Column(name = "CREDITOS_SUPERADOS")
    private Float creditosSuperados;
    @Column(name = "LIMITAR_CREDITOS")
    private Boolean limitarCreditos;
    @Column(name = "LIMITAR_CREDITOS_FIN_ESTUDIOS")
    private Boolean limitarCreditosFinEstudios;
    @Column(name = "TIPO_MATRICULA")
    private String tipoMatricula;
    @Column(name = "MATRICULA_PARCIAL")
    private Boolean matriculaParcial;
    private Boolean minusvalia;
    @Column(name = "NUMERO_BECAS_MEC")
    private Long numeroBecasMec;
    @Column(name = "NUMERO_BECAS_CONS")
    private Long numeroBecasCons;
    @Column(name = "NUMERO_SEMESTRES")
    private Long numeroSemestres;
    @Column(name = "PROGRAMA_INTERCAMBIO")
    private Boolean programaIntercambio;
    @Column(name = "SIN_DOCENCIA")
    private Boolean sinDocencia;
    @Column(name = "CREDITOS_CONVALIDADOS")
    private Float creditosConvalidados;
    @Column(name = "CREDITOS_PARA_BECA")
    private Float creditosParaBeca;
    @Column(name = "CURSO")
    private Long curso;
    @Column(name = "NOTA_ACCESO")
    private Float notaAcceso;
    @Column(name = "NOTA_MEDIA")
    private Float notaMedia;
    @Column(name = "POSEE_TITULO")
    private Boolean poseeTitulo;
    @Column(name = "ALGUNA_BECA_PARCIAL")
    private Boolean algunaBecaParcial;
    @Column(name = "TIENE_BECA_MEC")
    private Boolean tieneBecaMec;
    @Column(name = "CREDITOS_COMPLEMENTOS")
    private Float creditosComplementos;
    @Column(name = "NOTA_MEDIA_ULTIMO_CURSO")
    private Float notaMediaUltimoCurso;
    @Column(name = "TASAS_TOTAL")
    private Float tasasTotal;
    @Column(name = "TASAS_MINISTERIO")
    private Float tasasMinisterio;
    @Column(name = "TASAS_CONSELLERIA")
    private Float tasasConselleria;
    @Column(name = "PRIMERA_VEZ")
    private Boolean primeraVez;

    @Column(name = "NOTA_ESPECIFICA")
    private Float notaEspecifica;

    @Column(name = "MEDIA_SIN_SUSPENSOS")
    private Float mediaSinSuspensos;

    @Column(name = "CREDITOS_1A")
    private Float creditosPrimeraMatricula;

    @Column(name = "CREDITOS_2A")
    private Float creditosSegundaMatricula;

    @Column(name = "CREDITOS_3A")
    private Float creditosTerceraMatricula;

    @Column(name = "CREDITOS_3AP")
    private Float creditosTerceraMatriculaPosteriores;

    @Column(name = "CREDITOS_4AP")
    private Float creditosCuartaMatriculaPosteriores;

    @Column(name = "IMPORTES_1A")
    private Float importes1a;

    @Column(name = "IMPORTES_2A")
    private Float importes2a;

    @Column(name = "IMPORTES_3A")
    private Float importes3a;

    @Column(name = "IMPORTES_3AP")
    private Float importes3ap;

    @Column(name = "IMPORTES_4AP")
    private Float importes4ap;

    @Column(name = "CREDITOS_COVID19")
    private Float creditosCovid19;

    @Column(name = "CAMBIO_ESTUDIOS")
    private Boolean cambioEstudios;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_TIT_ACCESO")
    private Date fechaTitAcceso;

    public String getProcedenciaNotaId()
    {
        return procedenciaNotaId;
    }

    public void setProcedenciaNotaId(String procedenciaNotaId)
    {
        this.procedenciaNotaId = procedenciaNotaId;
    }

    public Float getMediaSinSuspensos()
    {
        return mediaSinSuspensos;
    }

    public void setMediaSinSuspensos(Float mediaSinSuspensos)
    {
        this.mediaSinSuspensos = mediaSinSuspensos;
    }

    public Float getNotaEspecifica()
    {
        return notaEspecifica;
    }

    public void setNotaEspecifica(Float notaEspecifica)
    {
        this.notaEspecifica = notaEspecifica;
    }

    @Column(name = "PROCEDENCIA_NOTA_ID")
    private String procedenciaNotaId;

    @Column(name = "CREDITOS_CUBIERTOS")
    private Float creditosCubiertos;
    // bi-directional many-to-one association to Estudio
    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ACCESO_ID")
    private Estudio estudioAcceso;
    // bi-directional many-to-one association to CursoAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;
    // bi-directional many-to-one association to Estudio
    @ManyToOne
    private Estudio estudio;
    // bi-directional many-to-one association to Persona
    @ManyToOne
    private Persona persona;
    // bi-directional many-to-one association to TipoFamilia
    @ManyToOne
    @JoinColumn(name = "FAMILIA_NUMEROSA_ID")
    private TipoFamilia tipoFamilia;

    public PersonaEstudio()
    {
        this.setLimitarCreditos(false);
        this.setLimitarCreditosFinEstudios(false);
        this.setMatriculaParcial(false);
        this.setMinusvalia(false);
        this.setSinDocencia(false);
        this.setPrimeraVez(false);
        this.setProgramaIntercambio(false);
    }

    public static Float truncarDecimales(Float numero, int digitos)
    {
        int cifras = (int) Math.pow(10, digitos);
        Integer redondeado = (int) (numero * cifras);
        return (float) redondeado / cifras;
    }

    public static Boolean isRendimientoAcademico(PersonaEstudio academicosActual,
            PersonaEstudio academicosAnterior) throws RamaNoExisteException, BecaException
    {
        if (academicosActual.estudio == null)
        {
            return false;
        }

        if (academicosActual.estudio.getTipo().equals("M"))
        {
            return cumpleRendimientoMaster(academicosActual, academicosAnterior);
        }

        return cumpleRendimientoGrado(academicosActual, academicosAnterior);
    }

    private static Boolean cumpleRendimientoMaster(PersonaEstudio academicosActual,
            PersonaEstudio academicosAnterior) throws BecaException
    {
        Minimo minimoRendimientoMaster = Minimo.getMinimosByEstudioId(academicosActual.getEstudio()
                .getId(), academicosActual.getCursoAcademico().getId());

        if (minimoRendimientoMaster == null)
        {
            throw new BecaException(
                    "No es pot calcular el rendiment del màster perque no existeixen rendiments mínims per a aquesta titulació");
        }

        if (academicosActual.curso == 1L && academicosActual.notaAcceso != null)
        {
            Float notaAcceso = academicosActual.notaAcceso;
            Estudio estudioAcceso = academicosActual.getEstudioAcceso();

            if (estudioAcceso != null)
            {
                Minimo minimoTituAcceso = Minimo.getMinimosByEstudioId(estudioAcceso.getId(),
                        academicosActual.getCursoAcademico().getId());

                if (minimoTituAcceso != null && estudioAcceso.isTitulacionTecnica())
                {
                    notaAcceso = notaAcceso * minimoRendimientoMaster.getCoefCorrectorMec();

                }

                if (Float.compare(notaAcceso, minimoRendimientoMaster.getNotaRendimientoMec1()) >=
                        0)
                {
                    return true;
                }
            }
        }

        if (academicosActual.curso != 1
                && academicosAnterior != null
                && Float.compare(academicosAnterior.creditosParaBeca,
                academicosAnterior.creditosSuperados) >= 0
                && academicosAnterior.notaMedia >= minimoRendimientoMaster.getNotaRendimientoMec2())
        {
            return true;
        }

        return false;
    }

    private static Boolean cumpleRendimientoGrado(PersonaEstudio academicosActual,
            PersonaEstudio academicosAnterior) throws RamaNoExisteException
    {
        if (academicosActual.curso == 1L)
        {
            return false;
        }

        Estudio estudio = academicosActual.getEstudio();

        if (estudio != null)
        {
            Long estudioId = estudio.getId();
            Long cursoAcademicoId = academicosActual.getCursoAcademico().getId();
            Minimo minimosByEstudioId = Minimo.getMinimosByEstudioId(estudioId, cursoAcademicoId);
            Float crdBecaCompleta = minimosByEstudioId.getCreditosBecaCompleta();
            Float matriculadosAnt = academicosAnterior.getCreditosParaBeca();

            if (Float.compare(matriculadosAnt, crdBecaCompleta) < 0)
            {
                return false;
            }

            Float superadosAnt = academicosAnterior.getCreditosSuperados();
            Float incremento = ((matriculadosAnt / crdBecaCompleta) - 1F) * 100F;
            Minimo minimos = Minimo.getMinimosByEstudioId(academicosActual.getEstudio().getId(),
                    cursoAcademicoId);

            if (incremento == 0)
            {
                Float porcentajeSuperados = superadosAnt * 100F / matriculadosAnt;

                if (Float.compare(porcentajeSuperados, minimos.getPorcRendimientoMec()) >= 0)
                {
                    return true;
                }

                return false;
            }

            Float porcentaje = minimos.getPorcSuperadosMec();
            Float creditosRendimiento = ((porcentaje - incremento / 10F) * matriculadosAnt / 100F)
                    + CREDITOS_EXTRA_RENDIMIENTO;

            if (Float.compare(superadosAnt, creditosRendimiento) >= 0)
            {
                return true;
            }
        }

        return false;
    }

    public static PersonaEstudio getAcademicosCursoActualbyPersonaEstudioYCursoAca(Long personaId,
            Long estudioId, Long cursoAcaId)
    {
        return personaEstudioDAO.getAcademicosbyPersonaEstudioYCursoAca(personaId, estudioId,
                cursoAcaId);
    }

    public static Boolean existePersonaEstudio(Long personaId, Long estudioId)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().get(0).getId();

        return personaEstudioDAO.existePersonaEstudio(personaId, estudioId, cursoActivo);
    }

    public static List<PersonaEstudio> getAcademicosByPersonaYCursoAca(Long personaId,
            Long cursoAcademicoId)
    {
        return personaEstudioDAO.getAcademicosByPersonaYCursoAca(personaId, cursoAcademicoId);
    }

    @Autowired
    public void setPersonaEstudioDAO(PersonaEstudioDAO personaEstudioDAO)
    {
        PersonaEstudio.personaEstudioDAO = personaEstudioDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getBecario()
    {
        return becario;
    }

    public void setBecario(Long becario)
    {
        this.becario = becario;
    }

    public Float getCreditosFaltantes()
    {
        return this.creditosFaltantes;
    }

    public void setCreditosFaltantes(Float creditosFaltantes)
    {
        this.creditosFaltantes = creditosFaltantes;
    }

    public Float getCreditosFueraBeca()
    {
        return this.creditosFueraBeca;
    }

    public void setCreditosFueraBeca(Float creditosFueraBeca)
    {
        this.creditosFueraBeca = creditosFueraBeca;
    }

    public Float getCreditosMatriculados()
    {
        return this.creditosMatriculados;
    }

    public void setCreditosMatriculados(Float creditosMatriculados)
    {
        this.creditosMatriculados = creditosMatriculados;
    }

    public Float getCreditosPendientesConva()
    {
        return this.creditosPendientesConva;
    }

    public void setCreditosPendientesConva(Float creditosPendientesConva)
    {
        this.creditosPendientesConva = creditosPendientesConva;
    }

    public Float getCreditosPresentados()
    {
        return this.creditosPresentados;
    }

    public void setCreditosPresentados(Float creditosPresentados)
    {
        this.creditosPresentados = creditosPresentados;
    }

    public Float getCreditosSuperados()
    {
        return this.creditosSuperados;
    }

    public void setCreditosSuperados(Float creditosSuperados)
    {
        this.creditosSuperados = creditosSuperados;
    }

    public Boolean isLimitarCreditos()
    {
        return this.limitarCreditos;
    }

    public Boolean noLimitaCreditos()
    {
        return !this.limitarCreditos;
    }

    public void setLimitarCreditos(Boolean limitarCreditos)
    {
        this.limitarCreditos = limitarCreditos;
    }

    public Boolean isLimitarCreditosFinEstudios()
    {
        return this.limitarCreditosFinEstudios;
    }

    public void setLimitarCreditosFinEstudios(Boolean limitarCreditosFinEstudios)
    {
        this.limitarCreditosFinEstudios = limitarCreditosFinEstudios;
    }

    public String getTipoMatricula()
    {
        return tipoMatricula;
    }

    public void setTipoMatricula(String tipoMatricula)
    {
        this.tipoMatricula = tipoMatricula;
    }

    public Boolean isMatriculaParcial()
    {
        return this.matriculaParcial;
    }

    public void setMatriculaParcial(Boolean matriculaParcial)
    {
        this.matriculaParcial = matriculaParcial;
    }

    public Boolean isMinusvalia()
    {
        return this.minusvalia;
    }

    public void setMinusvalia(Boolean minusvalia)
    {
        this.minusvalia = minusvalia;
    }

    public Long getNumeroBecasMec()
    {
        return this.numeroBecasMec;
    }

    public void setNumeroBecasMec(Long numeroBecasMec)
    {
        this.numeroBecasMec = numeroBecasMec;
    }

    public Long getNumeroBecasCons()
    {
        return this.numeroBecasCons;
    }

    public void setNumeroBecasCons(Long numeroBecasCons)
    {
        this.numeroBecasCons = numeroBecasCons;
    }

    public Long getNumeroSemestres()
    {
        return this.numeroSemestres;
    }

    public void setNumeroSemestres(Long numeroSemestres)
    {
        this.numeroSemestres = numeroSemestres;
    }

    public Boolean isProgramaIntercambio()
    {
        return this.programaIntercambio;
    }

    public void setProgramaIntercambio(Boolean programaIntercambio)
    {
        this.programaIntercambio = programaIntercambio;
    }

    public Boolean isSinDocencia()
    {
        return this.sinDocencia;
    }

    public void setSinDocencia(Boolean sinDocencia)
    {
        this.sinDocencia = sinDocencia;
    }

    public Boolean isPrimeraVez()
    {
        return this.primeraVez;
    }

    public void setPrimeraVez(Boolean primeraVez)
    {
        this.primeraVez = primeraVez;
    }

    public Float getCreditosConvalidados()
    {
        return this.creditosConvalidados;
    }

    public void setCreditosConvalidados(Float creditosConvalidados)
    {
        this.creditosConvalidados = creditosConvalidados;
    }

    public Float getCreditosParaBeca()
    {
        return this.creditosParaBeca;
    }

    public void setCreditosParaBeca(Float creditosParaBeca)
    {
        this.creditosParaBeca = creditosParaBeca;
    }

    public Long getCurso()
    {
        return this.curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Float getNotaAcceso()
    {
        return notaAcceso;
    }

    public void setNotaAcceso(Float notaAcceso)
    {
        this.notaAcceso = notaAcceso;
    }

    public Float getNotaMedia()
    {
        return notaMedia;
    }

    public void setNotaMedia(Float notaMedia)
    {
        this.notaMedia = notaMedia;
    }

    public Boolean isPoseeTitulo()
    {
        return this.poseeTitulo;
    }

    public void setPoseeTitulo(Boolean poseeTitulo)
    {
        this.poseeTitulo = poseeTitulo;
    }

    public Boolean isAlgunaBecaParcial()
    {
        return this.algunaBecaParcial;
    }

    public void setAlgunaBecaParcial(Boolean algunaBecaParcial)
    {
        this.algunaBecaParcial = algunaBecaParcial;
    }

    public Boolean getTieneBecaMec()
    {
        return tieneBecaMec;
    }

    public void setTieneBecaMec(Boolean tieneBecaMec)
    {
        this.tieneBecaMec = tieneBecaMec;
    }

    public Boolean isTieneBecaMec()
    {
        return this.tieneBecaMec;
    }

    public Float getCreditosComplementos()
    {
        return creditosComplementos;
    }

    public void setCreditosComplementos(Float creditosComplementos)
    {
        this.creditosComplementos = creditosComplementos;
    }

    public Estudio getEstudioAcceso()
    {
        return estudioAcceso;
    }

    public void setEstudioAcceso(Estudio estudioAcceso)
    {
        this.estudioAcceso = estudioAcceso;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Estudio getEstudio()
    {
        return this.estudio;
    }

    public void setEstudio(Estudio estudio)
    {
        this.estudio = estudio;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public TipoFamilia getTipoFamilia()
    {
        return tipoFamilia;
    }

    public void setTipoFamilia(TipoFamilia tipoFamilia)
    {
        this.tipoFamilia = tipoFamilia;
    }

    public Boolean isCumpleAcadBecaAnt()
    {
        if (this.creditosMatriculados == null || Float.compare(this.creditosMatriculados, 0F) == 0)
        {
            return false;
        }

        if (this.creditosPresentados == null || Float.compare(this.creditosPresentados, 0F) == 0)
        {
            return false;
        }

        if (this.creditosParaBeca == null || Float.compare(this.creditosParaBeca, 0F) == 0)
        {
            return false;
        }

        if (this.cursoAcademico.getId() < CURSO_ACADEMICO_CAMBIO_CONDICION_BECARIO)
        {
            return (Float.compare(
                    truncarDecimales(this.creditosPresentados / this.creditosMatriculados, 2),
                    truncarDecimales(1F / 3F, 2)) >= 0);
        }

        if (this.cursoAcademico.getId() >= CURSO_ACADEMICO_CAMBIO_CONDICION_BECARIO)
        {
            Float cincuentaPorCientoCreditosParaBeca =
                    truncarDecimales(this.creditosParaBeca / 2, 2);
            Float creditosSuperados = truncarDecimales(this.creditosSuperados, 2);

            return (Float.compare(creditosSuperados, cincuentaPorCientoCreditosParaBeca) >= 0);
        }
        return false;
    }

    public Long getCursoAcademicoAnteriorUJI()
    {
        return personaEstudioDAO.getCursoAcademicoAnteriorUJI(this.persona.getId(),
                this.cursoAcademico.getId());
    }

    public Float getNotaMediaUltimoCurso()
    {
        return notaMediaUltimoCurso;
    }

    public void setNotaMediaUltimoCurso(Float notaMediaUltimoCurso)
    {
        this.notaMediaUltimoCurso = notaMediaUltimoCurso;
    }

    public Float getTasasTotal()
    {
        return tasasTotal;
    }

    public void setTasasTotal(Float tasasTotal)
    {
        this.tasasTotal = tasasTotal;
    }

    public Float getTasasMinisterio()
    {
        return tasasMinisterio;
    }

    public void setTasasMinisterio(Float tasasMinisterio)
    {
        this.tasasMinisterio = tasasMinisterio;
    }

    public Float getTasasConselleria()
    {
        return tasasConselleria;
    }

    public void setTasasConselleria(Float tasasConselleria)
    {
        this.tasasConselleria = tasasConselleria;
    }

    public Float getCreditosCubiertos()
    {
        return creditosCubiertos;
    }

    public void setCreditosCubiertos(Float creditosCubiertos)
    {
        this.creditosCubiertos = creditosCubiertos;
    }

    public Float getCreditosPrimeraMatricula()
    {
        return creditosPrimeraMatricula;
    }

    public void setCreditosPrimeraMatricula(Float creditosPrimeraMatricula)
    {
        this.creditosPrimeraMatricula = creditosPrimeraMatricula;
    }

    public Float getCreditosSegundaMatricula()
    {
        return creditosSegundaMatricula;
    }

    public void setCreditosSegundaMatricula(Float creditosSegundaMatricula)
    {
        this.creditosSegundaMatricula = creditosSegundaMatricula;
    }

    public Float getCreditosTerceraMatricula()
    {
        return creditosTerceraMatricula;
    }

    public void setCreditosTerceraMatricula(Float creditosTerceraMatricula)
    {
        this.creditosTerceraMatricula = creditosTerceraMatricula;
    }

    public Float getCreditosTerceraMatriculaPosteriores() {
        return creditosTerceraMatriculaPosteriores;
    }

    public void setCreditosTerceraMatriculaPosteriores(Float creditosTerceraMatriculaPosteriores) {
        this.creditosTerceraMatriculaPosteriores = creditosTerceraMatriculaPosteriores;
    }

    public Float getCreditosCuartaMatriculaPosteriores() {
        return creditosCuartaMatriculaPosteriores;
    }

    public void setCreditosCuartaMatriculaPosteriores(Float creditosCuartaMatriculaPosteriores) {
        this.creditosCuartaMatriculaPosteriores = creditosCuartaMatriculaPosteriores;
    }

    public Float getImportes1a() {
        return importes1a;
    }

    public void setImportes1a(Float importes1a) {
        this.importes1a = importes1a;
    }

    public Float getImportes2a() {
        return importes2a;
    }

    public void setImportes2a(Float importes2a) {
        this.importes2a = importes2a;
    }

    public Float getImportes3a() {
        return importes3a;
    }

    public void setImportes3a(Float importes3a) {
        this.importes3a = importes3a;
    }

    public Float getImportes3ap() {
        return importes3ap;
    }

    public void setImportes3ap(Float importes3ap) {
        this.importes3ap = importes3ap;
    }

    public Float getImportes4ap() {
        return importes4ap;
    }

    public void setImportes4ap(Float importes4ap) {
        this.importes4ap = importes4ap;
    }

    public Float getCreditosCovid19() {
        return creditosCovid19;
    }

    public void setCreditosCovid19(Float creditosCovid19) {
        this.creditosCovid19 = creditosCovid19;
    }

    public Boolean isCambioEstudios() {
        return cambioEstudios;
    }

    public void setCambioEstudios(Boolean cambioEstudios) {
        this.cambioEstudios = cambioEstudios;
    }

    public Date getFechaTitAcceso() {
        return fechaTitAcceso;
    }

    public void setFechaTitAcceso(Date fechaTitAcceso) {
        this.fechaTitAcceso = fechaTitAcceso;
    }
}
