package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class BecaDeDistintoOrganismoYaExisteException extends GeneralBECException
{
    public BecaDeDistintoOrganismoYaExisteException()
    {
        super("Ja existeix una beca para un altre organisme , concedida o pendent de processar.");
    }

    public BecaDeDistintoOrganismoYaExisteException(String message)
    {
        super(message);
    }
}
