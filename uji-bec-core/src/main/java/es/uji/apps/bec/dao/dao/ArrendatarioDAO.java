package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Arrendatario;
import es.uji.commons.db.BaseDAO;

public interface ArrendatarioDAO extends BaseDAO
{
    List<Arrendatario> getArrendatariosByDomicilio(Long domicilioId);

    Arrendatario getArrendatarioById(Long arrendatarioId);

    void updateArrendatario(Arrendatario arrendatario);
}
