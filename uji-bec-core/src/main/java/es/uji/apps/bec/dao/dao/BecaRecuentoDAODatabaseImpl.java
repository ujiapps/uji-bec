package es.uji.apps.bec.dao.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import es.uji.apps.bec.model.BecaRecuento;
import es.uji.apps.bec.model.QBecaRecuento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BecaRecuentoDAODatabaseImpl extends BaseDAODatabaseImpl implements BecaRecuentoDAO
{

    @Override
    public List<BecaRecuento> getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(
            Long cursoAcademicoId, Long organismoId, Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBecaRecuento qBecaRecuento = QBecaRecuento.becaRecuento;

        query.from(qBecaRecuento)
                .where(qBecaRecuento.cursoAcademicoId.eq(cursoAcademicoId).and(
                        qBecaRecuento.organismoId
                                .eq(organismoId)
                                .and(qBecaRecuento.convocatoriaId.eq(convocatoriaId).and(
                                        qBecaRecuento.cuantos.gt(0)))));
        return query.list(qBecaRecuento);
    }

    @Override
    public List<Tuple> getDistinctOrganismoConvocatoriaByCursoAcademico(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBecaRecuento qBecaRecuento = QBecaRecuento.becaRecuento;

        query.from(qBecaRecuento).where(
                qBecaRecuento.cursoAcademicoId.eq(cursoAcademicoId).and(
                        qBecaRecuento.cuantos.gt(0L)));

        return query.distinct().list(
                new QTuple(qBecaRecuento.organismo, qBecaRecuento.organismoId,
                        qBecaRecuento.convocatoria, qBecaRecuento.convocatoriaId));

    }
}
