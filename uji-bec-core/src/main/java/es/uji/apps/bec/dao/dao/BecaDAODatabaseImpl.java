package es.uji.apps.bec.dao.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BecaDAODatabaseImpl extends BaseDAODatabaseImpl implements BecaDAO
{
    @Override
    public Beca getBeca(Long solicitanteId, Long convocatoriaId, Long estudioId)
    {
        if (solicitanteId == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder filtros = new BooleanBuilder();

        addSolicitanteFilter(solicitanteId, filtros);
        addConvocatoriaFilter(convocatoriaId, filtros);
        addEstudioFilter(estudioId, filtros);

        if (filtros.hasValue())
        {
            query.from(QBeca.beca).where(filtros);
        }

        return query.singleResult(QBeca.beca);
    }

    @Override
    public Beca getBecaById(Long becaId)
    {
        if (becaId == null)
        {
            return null;
        }
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;

        query.from(qBeca)
                .leftJoin(qBeca.domicilios).fetch()
                .leftJoin(qBeca.miembros).fetch()
                .where(qBeca.id.eq(becaId));

        return query.uniqueResult(qBeca);
    }

    @Override
    public boolean existenBecasConEstudio(Long solicitanteId, Long convocatoriaId)
    {
        if (solicitanteId == null)
        {
            return false;
        }

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder filtros = new BooleanBuilder();

        addSolicitanteFilter(solicitanteId, filtros);
        addConvocatoriaFilter(convocatoriaId, filtros);
        addEstudioDistintoNull(filtros);

        if (filtros.hasValue())
        {
            query.from(QBeca.beca).where(filtros);
        }

        return query.count() > 0;
    }

    @Override
    public boolean existeBeca(Long solicitanteId, Long convocatoriaId, Long estudioId)
    {
        if (solicitanteId == null)
        {
            return false;
        }

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder filtros = new BooleanBuilder();

        addSolicitanteFilter(solicitanteId, filtros);
        addConvocatoriaFilter(convocatoriaId, filtros);
        addEstudioFilter(estudioId, filtros);

        if (filtros.hasValue())
        {
            query.from(QBeca.beca).where(filtros);
        }

        return query.count() > 0;
    }

    @Override
    public boolean isDuplicada(Long solicitanteId, Long convocatoriaId, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder filtros = new BooleanBuilder();

        addSolicitanteFilter(solicitanteId, filtros);
        addConvocatoriaFilter(convocatoriaId, filtros);
        addEstudioFilter(estudioId, filtros);

        if (filtros.hasValue())
        {
            query.from(QBeca.beca).where(filtros);
        }

        return query.count() > 0;
    }

    private void addConvocatoriaFilter(Long convocatoriaId, BooleanBuilder filtros)
    {
        filtros.and(QBeca.beca.convocatoria.id.eq(convocatoriaId));
    }

    private void addSolicitanteFilter(Long solicitanteId, BooleanBuilder filtros)
    {
        filtros.and(QBeca.beca.solicitante.id.eq(solicitanteId));
    }

    private void addEstudioFilter(Long estudioId, BooleanBuilder filtros)
    {
        if (estudioId != null)
        {
            filtros.and(QBeca.beca.estudio.id.eq(estudioId));
        }
        else
        {
            filtros.and(QBeca.beca.estudio.id.isNull());
        }
    }

    private void addEstudioDistintoNull(BooleanBuilder filtros)
    {
        filtros.and(QBeca.beca.estudio.id.isNotNull());
    }

    @Override
    public Beca getBecaByCursoConvocatoriaTandaIdentificacion(Integer cursoAcademicoId,
            String convocatoria, Integer tanda, String identificacion)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;
        QSolicitante qSolicitante = QSolicitante.solicitante;
        QTanda qTanda = QTanda.tanda;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;

        query.from(qBeca)
                .join(qBeca.solicitante, qSolicitante)
                .join(qBeca.convocatoria, qConvocatoria)
                .join(qBeca.tanda, qTanda)
                .where(qBeca.identificacion.toUpperCase().eq(identificacion.toUpperCase())
                        .and(qBeca.convocatoria.acronimo.eq(convocatoria))
                        .and(qTanda.tandaId.eq(new Long(tanda)))
                        .and(qSolicitante.cursoAcademico.id.eq(new Long(cursoAcademicoId))));

        return query.uniqueResult(qBeca);
    }

    @Override
    public Beca getBecaByArchivoTemporal(Long archivoTemporal)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;

        query.from(qBeca)
                .where(qBeca.codigoArchivoTemporal.eq(archivoTemporal.toString()));

        return query.uniqueResult(qBeca);
    }

    @Override
    public List<Beca> getBecasByTandaId(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(beca.tanda.id.eq(tandaId));
        if (query.list(beca).size() > 0)
        {
            return query.list(beca);
        }
        return null;
    }

    @Override
    public List<Beca> getHistoricoObservacionesByBecaAndPersona(Long becaId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;
        QSolicitante solicitante = QSolicitante.solicitante;

        query.from(beca)
                .join(beca.solicitante, solicitante)
                .fetch()
                .where(beca.id.ne(becaId).and(beca.observacionesUji.isNotEmpty())
                        .and(solicitante.persona.id.eq(personaId)))
                .orderBy(solicitante.cursoAcademico.id.desc(), beca.convocatoria.id.asc());

        return query.list(beca);
    }

    @Override
    public List<HistoricoBeca> getHistoricoEstadosByBeca(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QHistoricoBeca historico = QHistoricoBeca.historicoBeca;

        query.from(historico).where(historico.beca.id.eq(becaId)).orderBy(historico.fecha.desc());

        return query.list(historico);
    }

    @Override
    public Long getNumeroBecasPorTandaAgrupadasPorEstadoConvocatoriaYProceso(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(beca.tanda.id.eq(tandaId))
                .groupBy(beca.estado.id, beca.proceso.id, beca.convocatoria.id);
        Long numeroFilas = (long) query.list(beca.estado.id, beca.proceso.id, beca.convocatoria.id)
                .size();

        if (numeroFilas > 0L)
        {
            return numeroFilas;
        }

        return 0L;
    }

    @Override
    public Long getNumeroBecasPorTandaEnEstadoNoDenegado(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(
                beca.estado.id.ne(3L).and(beca.estado.id.ne(6L)).and(beca.estado.id.ne(7L))
                        .and(beca.tanda.id.eq(tandaId)));

        if (query.list(beca).size() > 0)
        {
            return query.list(beca.count()).get(0);
        }
        return 0L;
    }

    @Override
    public Long getNumeroBecasPorTandaEnEstadoNoEnviado(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(
                beca.estado.id.ne(4L)
                        .and(beca.tanda.id.eq(tandaId)));

        if (query.list(beca).size() > 0)
        {
            return query.list(beca.count()).get(0);
        }
        return 0L;
    }

    @Override
    public Long getResumenNumeroBecasNoNotificadasPorTanda(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(beca.fechaNotificacion.isNull().and(beca.tanda.id.eq(tandaId)));

        if (query.list(beca).size() > 0)
        {
            return query.list(beca.count()).get(0);
        }
        return 0L;
    }

    @Override
    public Long getNumeroBecasPorTanda(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;

        query.from(beca).where(beca.tanda.id.eq(tandaId));

        if (query.list(beca).size() > 0)
        {
            return query.list(beca.count()).get(0);
        }
        return 0L;
    }

    @Override
    @Transactional
    public void updateOtrosDatos(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId())).set(qBeca.entidad, beca.getEntidad())
                .set(qBeca.sucursal, beca.getSucursal())
                .set(qBeca.digitosControl, beca.getDigitosControl())
                .set(qBeca.cuentaBancaria, beca.getCuentaBancaria())
                .set(qBeca.observaciones, beca.getObservaciones())
                .set(qBeca.ibanPais, beca.getIbanPais())
                .set(qBeca.ibanDC, beca.getIbanDC())
                .execute();
    }

    @Override
    @Transactional
    public void updateDatosGenerales(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.convocatoria, beca.getConvocatoria())
                .set(qBeca.proceso, beca.getProceso()).set(qBeca.estado, beca.getEstado())
                .set(qBeca.tanda, beca.getTanda())
                .set(qBeca.codigoArchivoTemporal, beca.getCodigoArchivoTemporal())
                .set(qBeca.codigoExpediente, beca.getCodigoExpediente())
                .set(qBeca.identificacion, beca.getIdentificacion())
                .set(qBeca.tipoIdentificacion, beca.getTipoIdentificacion())
                .set(qBeca.numeroTransportes, beca.getNumeroTransportes())
                .set(qBeca.transporteUrbano, beca.isTransporteUrbano())
                .set(qBeca.autorizacionRenta, beca.isAutorizacionRenta())
                .set(qBeca.fechaNotificacion, beca.getFechaNotificacion())
                .set(qBeca.becaConcedida, beca.isBecaConcedida())
                .set(qBeca.reclamada, beca.isReclamada()).execute();
    }



    @Override
    @Transactional
    public void updateConvocatoria(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.convocatoria, beca.getConvocatoria()).execute();
    }

    @Override
    @Transactional
    public void updateEstado(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId())).set(qBeca.estado, beca.getEstado()).execute();
    }

    @Override
    @Transactional
    public void updateAcademicos(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause
                .where(qBeca.id.eq(beca.getId()))
                .set(qBeca.tieneTituloUniversitario, beca.isTieneTituloUniversitario())
                .set(qBeca.tituloNombre, beca.getTituloNombre())
                .set(qBeca.codigoEstudioAnt, beca.getCodigoEstudioAnt())
                .set(qBeca.codigoEstudioUjiAnt, beca.getCodigoEstudioUjiAnt())
                .set(qBeca.estudioAnt, beca.getEstudioAnt())
                .set(qBeca.cursoAcademicoAnt, beca.getCursoAcademicoAnt())
                .set(qBeca.causaOtros, beca.getCausaOtros())
                .set(qBeca.algunaBecaParcial, beca.isAlgunaBecaParcial())
                .set(qBeca.codigoCentroAnt, beca.getCodigoCentroAnt())
                .set(qBeca.nombreCentroAnt, beca.getNombreCentroAnt())
                .set(qBeca.codTituAcceso, beca.getCodTituAcceso())
                .set(qBeca.estudiosHomologados, beca.isEstudiosHomologados())
                .set(qBeca.tituloEspanol, beca.isTituloEspanol())
                .set(qBeca.univTitulacion, beca.getUnivTitulacion())
                .set(qBeca.notaMediaAnt, beca.getNotaMediaAnt())
                .set(qBeca.notaMediaUltimoCurso, beca.getNotaMediaUltimoCurso())
                .set(qBeca.creditosMatriculadosAnt, beca.getCreditosMatriculadosAnt())
                .set(qBeca.creditosSuperadosAnt, beca.getCreditosSuperadosAnt())
                .set(qBeca.creditosSuspensosAnt, beca.getCreditosSuspensosAnt())
                .set(qBeca.creditosConvalidadosAnt, beca.getCreditosConvalidadosAnt())
                .set(qBeca.becasConcedidasAnt, beca.getBecasConcedidasAnt())
                .set(qBeca.becaCursoAnt, beca.isBecaCursoAnt())
                .set(qBeca.matriculaParcialAnt, beca.isMatriculaParcialAnt())
                .set(qBeca.cumpleAcadBecaAnt, beca.isCumpleAcadBecaAnt())
                .set(qBeca.rendimientoAcademico, beca.isRendimientoAcademico())
                .set(qBeca.creditosMatriculados, beca.getCreditosMatriculados())
                .set(qBeca.creditosConvalidados, beca.getCreditosConvalidados())
                .set(qBeca.creditosCovid19, beca.getCreditosCovid19())
                .set(qBeca.creditosPendientesConvalidacion,beca.getCreditosPendientesConvalidacion())
                .set(qBeca.creditosFueraBeca, beca.getCreditosFueraBeca())
                .set(qBeca.creditosParaBeca, beca.getCreditosParaBeca())
                .set(qBeca.numeroSemestres, beca.getNumeroSemestres())
                .set(qBeca.curso, beca.getCurso()).set(qBeca.becaParcial, beca.isBecaParcial())
                .set(qBeca.matriculaParcial, beca.isMatriculaParcial())
                .set(qBeca.poseeTitulo, beca.isPoseeTitulo())
                .set(qBeca.limitarCreditos, beca.isLimitarCreditos())
                .set(qBeca.limitarCreditosFinEstudios, beca.isLimitarCreditosFinEstudios())
                .set(qBeca.planAnteriorNumCursos, beca.getPlanAnteriorNumCursos())
                .set(qBeca.creditosEstudioAnt, beca.getCreditosEstudioAnt())
                .set(qBeca.cumpleAcademicos, beca.isCumpleAcademicos())
                .set(qBeca.tipoTitulacion, beca.getTipoTitulacion())
                .set(qBeca.procedenciaNota, beca.getProcedenciaNota())
                .set(qBeca.notaPruebaEspecifica, beca.getNotaPruebaEspecifica())
                .set(qBeca.notaMediaSinSuspenso, beca.getNotaMediaSinSuspenso())
                .set(qBeca.creditosPrimeraMatricula, beca.getCreditosPrimeraMatricula())
                .set(qBeca.creditosSegundaMatricula, beca.getCreditosSegundaMatricula())
                .set(qBeca.creditosTerceraMatricula, beca.getCreditosTerceraMatricula())
                .set(qBeca.creditosTerceraMatriculaPosteriores, beca.getCreditosTerceraMatriculaPosteriores())
                .set(qBeca.creditosCuartaMatriculaPosteriores, beca.getCreditosCuartaMatriculaPosteriores())
                .set(qBeca.otraBecaConcedida, beca.isOtraBecaConcedida())
                .execute();
    }

    @Override
    @Transactional
    public void updateEstudios(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.tipoMatricula, beca.getTipoMatricula())
                .set(qBeca.estudio, beca.getEstudio())
                .set(qBeca.codigoEstudioAnt, beca.getCodigoEstudioAnt())
                .set(qBeca.codigoEstudioUjiAnt, beca.getCodigoEstudioUjiAnt())
                .set(qBeca.codigoCentroAnt, beca.getCodigoCentroAnt())
                .set(qBeca.nombreCentroAnt, beca.getNombreCentroAnt())
                .set(qBeca.estudioAnt, beca.getEstudioAnt())
                .set(qBeca.planAnteriorNumCursos, beca.getPlanAnteriorNumCursos())
                .set(qBeca.cambioEstudios, beca.isCambioEstudios())
                .set(qBeca.creditosEstudioAnt, beca.getCreditosEstudioAnt()).execute();
    }

    @Override
    @Transactional
    public void updateObservacionesUji(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.observacionesUji, beca.getObservacionesUji()).execute();
    }

    @Override
    @Transactional
    public void updateTanda(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId())).set(qBeca.tanda, beca.getTanda()).execute();
    }

    @Override
    @Transactional
    public void updateBecaConcedida(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.becaConcedida, beca.isBecaConcedida()).execute();
    }

    @Override
    @Transactional
    public void updateReclamada(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.reclamada, beca.isReclamada()).execute();
    }

    @Override
    public List<Beca> getBecasByOrganismoIdAndCursoAcademicoId(Long organismoId,
            Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QSolicitante qSolicitante = QSolicitante.solicitante;

        query.from(qBeca)
                .join(qBeca.convocatoria, qConvocatoria)
                .join(qBeca.solicitante, qSolicitante)
                .where(qConvocatoria.organismo.id.eq(organismoId).and(
                        qSolicitante.cursoAcademico.id.eq(cursoAcademicoId)));

        return query.list(qBeca);

    }

    @Override
    public List<Beca> getBecasByOrganismoIdAndCursoAcademicoIdAndProcesoIdAndEstadoIdAndConvocatoriaId(
            Long organismoId, Long cursoAcademicoId, Long procesoId, Long estadoId,
            Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QSolicitante qSolicitante = QSolicitante.solicitante;

        query.from(qBeca)
                .join(qBeca.convocatoria, qConvocatoria)
                .join(qBeca.solicitante, qSolicitante)
                .where(qConvocatoria.organismo.id.eq(organismoId)
                        .and(qSolicitante.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(qBeca.convocatoria.id.eq(convocatoriaId))
                        .and(qBeca.proceso.id.eq(procesoId).or(qBeca.estado.id.eq(estadoId))));

        return query.list(qBeca);
    }

    @Override
    public Beca getBecasByConvocatoriaIdAndNifAndCursoAcademico(Long convocatoria, String nif,
            Long cursoAcademico) throws BecaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca qBeca = QBeca.beca;
        QSolicitante qSolicitante = QSolicitante.solicitante;
        QPersona qPersona = QPersona.persona;

        query.from(qBeca)
                .join(qBeca.solicitante, qSolicitante)
                .join(qSolicitante.persona, qPersona)
                .where(qBeca.solicitante.id.eq(qSolicitante.id).and(
                        qBeca.convocatoria.id.eq(convocatoria).and(
                                qPersona.identificacion.eq(nif).and(
                                        qSolicitante.cursoAcademico.id.eq(cursoAcademico)))));

        List<Beca> listaBecas = query.list(qBeca);

        if (listaBecas.size() == 1)
        {
            return listaBecas.get(0);

        }
        else
        {
            throw new BecaException("No s'ha trobat la beca para el nif " + nif + " del curs "
                    + cursoAcademico);
        }
    }

    @Override
    @Transactional
    public Beca updateBeca(Beca beca)
    {
        QBeca qBeca = QBeca.beca;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBeca);

        updateClause.where(qBeca.id.eq(beca.getId()))
                .set(qBeca.autorizacionRenta, beca.isAutorizacionRenta())
                .set(qBeca.becaCursoAnt, beca.isBecaConcedida())
                .set(qBeca.becaCursoAnt2, beca.isBecaCursoAnt2())
                .set(qBeca.becaCursoAnt3, beca.isBecaCursoAnt3())
                .set(qBeca.becaParcial, beca.isBecaParcial())
                .set(qBeca.causaOtros, beca.getCausaOtros())
                .set(qBeca.codigoArchivoTemporal, beca.getCodigoArchivoTemporal())
                .set(qBeca.codigoBeca, beca.getCodigoBeca())
                .set(qBeca.codigoCentroAnt, beca.getCodigoCentroAnt())
                .set(qBeca.codigoEstudioAnt, beca.getCodigoEstudioAnt())
                .set(qBeca.codigoEstudioUjiAnt, beca.getCodigoEstudioUjiAnt())
                .set(qBeca.creditosConvalidadosAnt, beca.getCreditosConvalidadosAnt())
                .set(qBeca.creditosEstudioAnt, beca.getCreditosEstudioAnt())
                .set(qBeca.creditosMatriculadosAnt, beca.getCreditosMatriculadosAnt())
                .set(qBeca.creditosSuperadosAnt, beca.getCreditosSuperadosAnt())
                .set(qBeca.creditosSuspensosAnt, beca.getCreditosSuspensosAnt())
                .set(qBeca.cuentaBancaria, beca.getCuentaBancaria())
                .set(qBeca.curso, beca.getCurso())
                .set(qBeca.cursoAcademicoAnt, beca.getCursoAcademicoAnt())
                .set(qBeca.digitosControl, beca.getDigitosControl())
                .set(qBeca.distanciaAlCentro, beca.getDistanciaAlCentro())
                .set(qBeca.entidad, beca.getEntidad())
                .set(qBeca.estudioAnt, beca.getEstudioAnt())
                .set(qBeca.estudioAntOtros, beca.getEstudioAntOtros())
                .set(qBeca.estudiosSinDocencia, beca.isEstudiosSinDocencia())
                .set(qBeca.familiaEspecialidad, beca.getFamiliaEspecialidad())
                .set(qBeca.identificacion, beca.getIdentificacion())
                .set(qBeca.limitarCreditos, beca.isLimitarCreditos())
                .set(qBeca.sms, beca.isSms())
                .set(qBeca.limitarCreditosFinEstudios, beca.isLimitarCreditosFinEstudios())
                .set(qBeca.matriculaParcial, beca.isMatriculaParcial())
                .set(qBeca.matriculaParcialAnt, beca.isMatriculaParcialAnt())
                .set(qBeca.notaMediaAnt, beca.getNotaMediaAnt())
                .set(qBeca.notaMediaUltimoCurso, beca.getNotaMediaUltimoCurso())
                .set(qBeca.numeroSemestres, beca.getNumeroSemestres())
                .set(qBeca.numeroTransportes, beca.getNumeroTransportes())
                .set(qBeca.observaciones, beca.getObservaciones())
                .set(qBeca.observacionesUji, beca.getObservacionesUji())
                .set(qBeca.presencial, beca.isPresencial())
                .set(qBeca.programaIntercambio, beca.isProgramaIntercambio())
                .set(qBeca.sucursal, beca.getSucursal())
                .set(qBeca.tieneTituloUniversitario, beca.isTieneTituloUniversitario())
                .set(qBeca.tituloNombre, beca.getTituloNombre())
                .set(qBeca.transporteBarcoAvion, beca.isTransporteBarcoAvion())
                .set(qBeca.transporteUrbano, beca.isTransporteUrbano())
                .set(qBeca.creditosMatriculados, beca.getCreditosMatriculados())
                .set(qBeca.creditosConvalidados, beca.getCreditosConvalidados())
                .set(qBeca.creditosPendientesConvalidacion, beca.getCreditosPendientesConvalidacion())
                .set(qBeca.creditosFueraBeca, beca.getCreditosFueraBeca())
                .set(qBeca.cumpleAcademicos, beca.isCumpleAcademicos())
                .set(qBeca.becasConcedidasAnt, beca.getBecasConcedidasAnt())
                .set(qBeca.cumpleAcadBecaAnt, beca.isCumpleAcadBecaAnt())
                .set(qBeca.creditosParaBeca, beca.getCreditosParaBeca())
                .set(qBeca.rendimientoAcademico, beca.isRendimientoAcademico())
                .set(qBeca.poseeTitulo, beca.isPoseeTitulo())
                .set(qBeca.algunaBecaParcial, beca.isAlgunaBecaParcial())
                .set(qBeca.fechaNotificacion, beca.getFechaNotificacion())
                .set(qBeca.nombreCentroAnt, beca.getNombreCentroAnt())
                .set(qBeca.tituloEspanol, beca.isTituloEspanol())
                .set(qBeca.estudiosHomologados, beca.isEstudiosHomologados())
                .set(qBeca.univTitulacion, beca.getUnivTitulacion())
                .set(qBeca.indCoefCorrector, beca.isIndCoefCorrector())
                .set(qBeca.notaCorregida, beca.getNotaCorregida())
                .set(qBeca.codTituAcceso, beca.getCodTituAcceso())
                .set(qBeca.planAnteriorNumCursos, beca.getPlanAnteriorNumCursos())
                .set(qBeca.tipoTitulacion, beca.getTipoTitulacion())
                .set(qBeca.becaConcedida, beca.isBecaConcedida())
                .set(qBeca.reclamada, beca.isReclamada())
                .set(qBeca.ibanPais, beca.getIbanPais())
                .set(qBeca.ibanDC, beca.getIbanDC())
                .set(qBeca.creditosPrimeraMatricula, beca.getCreditosPrimeraMatricula())
                .set(qBeca.creditosSegundaMatricula, beca.getCreditosSegundaMatricula())
                .set(qBeca.creditosTerceraMatricula, beca.getCreditosTerceraMatricula())
                .set(qBeca.otraBecaConcedida, beca.isOtraBecaConcedida())
                .set(qBeca.codigoExpediente, beca.getCodigoExpediente())
                .execute();

        return beca;
    }

    @Override
    public List<Documento> getDocumentosPendientesNotificacion(Long becaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QDocumento documento = QDocumento.documento;

        query.from(documento)
                .where(documento.beca.id.eq(becaId)
                   .and(documento.validado.isFalse()))
                .orderBy(documento.tipoDocumento.orden.asc());

        return query.list(documento);

    }
}
