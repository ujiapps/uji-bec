package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.BecaReclamacion;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.UIEntity;

import java.util.List;

public interface BecaReclamacionDAO extends BaseDAO
{
    List<BecaReclamacion> getReclamacionesByBecaId(Long becaId);

    BecaReclamacion getReclamacionById(Long reclamacionId);

    void updateReclamacion(BecaReclamacion reclamacion);
}
