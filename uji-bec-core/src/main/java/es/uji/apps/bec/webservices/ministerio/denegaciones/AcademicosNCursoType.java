//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 01:14:30 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.denegaciones;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AcademicosNCursoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AcademicosNCursoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}AcncNumAsignatura" minOccurs="0"/>
 *         &lt;element ref="{}AcncAsignatura" minOccurs="0"/>
 *         &lt;element ref="{}AcncCurso" minOccurs="0"/>
 *         &lt;element ref="{}AcncNumCreditos" minOccurs="0"/>
 *         &lt;element ref="{}AcncCaracter" minOccurs="0"/>
 *         &lt;element ref="{}AcncTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcademicosNCursoType", propOrder = {
    "acncNumAsignatura",
    "acncAsignatura",
    "acncCurso",
    "acncNumCreditos",
    "acncCaracter",
    "acncTipo"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.denegaciones.AcademicosNCursoType")
@Table(name = "ACADEMICOSNCURSOTYPE", schema = "UJI_BECAS_WS_DENEGACIONES")
@Inheritance(strategy = InheritanceType.JOINED)
public class AcademicosNCursoType
    implements Equals, HashCode
{

    @XmlElement(name = "AcncNumAsignatura")
    protected Long acncNumAsignatura;
    @XmlElement(name = "AcncAsignatura")
    protected String acncAsignatura;
    @XmlElement(name = "AcncCurso")
    protected Long acncCurso;
    @XmlElement(name = "AcncNumCreditos")
    protected Float acncNumCreditos;
    @XmlElement(name = "AcncCaracter")
    protected String acncCaracter;
    @XmlElement(name = "AcncTipo")
    protected Long acncTipo;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the acncNumAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "ACNCNUMASIGNATURA", scale = 0)
    public Long getAcncNumAsignatura() {
        return acncNumAsignatura;
    }

    /**
     * Sets the value of the acncNumAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAcncNumAsignatura(Long value) {
        this.acncNumAsignatura = value;
    }

    /**
     * Gets the value of the acncAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ACNCASIGNATURA", length = 60)
    public String getAcncAsignatura() {
        return acncAsignatura;
    }

    /**
     * Sets the value of the acncAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcncAsignatura(String value) {
        this.acncAsignatura = value;
    }

    /**
     * Gets the value of the acncCurso property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "ACNCCURSO", scale = 0)
    public Long getAcncCurso() {
        return acncCurso;
    }

    /**
     * Sets the value of the acncCurso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAcncCurso(Long value) {
        this.acncCurso = value;
    }

    /**
     * Gets the value of the acncNumCreditos property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "ACNCNUMCREDITOS", precision = 4, scale = 2)
    public Float getAcncNumCreditos() {
        return acncNumCreditos;
    }

    /**
     * Sets the value of the acncNumCreditos property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setAcncNumCreditos(Float value) {
        this.acncNumCreditos = value;
    }

    /**
     * Gets the value of the acncCaracter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ACNCCARACTER", length = 30)
    public String getAcncCaracter() {
        return acncCaracter;
    }

    /**
     * Sets the value of the acncCaracter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcncCaracter(String value) {
        this.acncCaracter = value;
    }

    /**
     * Gets the value of the acncTipo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "ACNCTIPO", scale = 0)
    public Long getAcncTipo() {
        return acncTipo;
    }

    /**
     * Sets the value of the acncTipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAcncTipo(Long value) {
        this.acncTipo = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AcademicosNCursoType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AcademicosNCursoType that = ((AcademicosNCursoType) object);
        {
            Long lhsAcncNumAsignatura;
            lhsAcncNumAsignatura = this.getAcncNumAsignatura();
            Long rhsAcncNumAsignatura;
            rhsAcncNumAsignatura = that.getAcncNumAsignatura();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncNumAsignatura", lhsAcncNumAsignatura), LocatorUtils.property(thatLocator, "acncNumAsignatura", rhsAcncNumAsignatura), lhsAcncNumAsignatura, rhsAcncNumAsignatura)) {
                return false;
            }
        }
        {
            String lhsAcncAsignatura;
            lhsAcncAsignatura = this.getAcncAsignatura();
            String rhsAcncAsignatura;
            rhsAcncAsignatura = that.getAcncAsignatura();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncAsignatura", lhsAcncAsignatura), LocatorUtils.property(thatLocator, "acncAsignatura", rhsAcncAsignatura), lhsAcncAsignatura, rhsAcncAsignatura)) {
                return false;
            }
        }
        {
            Long lhsAcncCurso;
            lhsAcncCurso = this.getAcncCurso();
            Long rhsAcncCurso;
            rhsAcncCurso = that.getAcncCurso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncCurso", lhsAcncCurso), LocatorUtils.property(thatLocator, "acncCurso", rhsAcncCurso), lhsAcncCurso, rhsAcncCurso)) {
                return false;
            }
        }
        {
            Float lhsAcncNumCreditos;
            lhsAcncNumCreditos = this.getAcncNumCreditos();
            Float rhsAcncNumCreditos;
            rhsAcncNumCreditos = that.getAcncNumCreditos();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncNumCreditos", lhsAcncNumCreditos), LocatorUtils.property(thatLocator, "acncNumCreditos", rhsAcncNumCreditos), lhsAcncNumCreditos, rhsAcncNumCreditos)) {
                return false;
            }
        }
        {
            String lhsAcncCaracter;
            lhsAcncCaracter = this.getAcncCaracter();
            String rhsAcncCaracter;
            rhsAcncCaracter = that.getAcncCaracter();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncCaracter", lhsAcncCaracter), LocatorUtils.property(thatLocator, "acncCaracter", rhsAcncCaracter), lhsAcncCaracter, rhsAcncCaracter)) {
                return false;
            }
        }
        {
            Long lhsAcncTipo;
            lhsAcncTipo = this.getAcncTipo();
            Long rhsAcncTipo;
            rhsAcncTipo = that.getAcncTipo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acncTipo", lhsAcncTipo), LocatorUtils.property(thatLocator, "acncTipo", rhsAcncTipo), lhsAcncTipo, rhsAcncTipo)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theAcncNumAsignatura;
            theAcncNumAsignatura = this.getAcncNumAsignatura();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncNumAsignatura", theAcncNumAsignatura), currentHashCode, theAcncNumAsignatura);
        }
        {
            String theAcncAsignatura;
            theAcncAsignatura = this.getAcncAsignatura();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncAsignatura", theAcncAsignatura), currentHashCode, theAcncAsignatura);
        }
        {
            Long theAcncCurso;
            theAcncCurso = this.getAcncCurso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncCurso", theAcncCurso), currentHashCode, theAcncCurso);
        }
        {
            Float theAcncNumCreditos;
            theAcncNumCreditos = this.getAcncNumCreditos();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncNumCreditos", theAcncNumCreditos), currentHashCode, theAcncNumCreditos);
        }
        {
            String theAcncCaracter;
            theAcncCaracter = this.getAcncCaracter();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncCaracter", theAcncCaracter), currentHashCode, theAcncCaracter);
        }
        {
            Long theAcncTipo;
            theAcncTipo = this.getAcncTipo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acncTipo", theAcncTipo), currentHashCode, theAcncTipo);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
