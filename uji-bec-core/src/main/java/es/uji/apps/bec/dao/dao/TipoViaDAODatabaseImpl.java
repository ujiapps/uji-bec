package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoVia;
import es.uji.apps.bec.model.TipoVia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoViaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoViaDAO
{
    private QTipoVia tipoVia = QTipoVia.tipoVia;

    @Override
    public List<TipoVia> getTiposVias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoVia).orderBy(tipoVia.orden.asc());
        return query.list(tipoVia);
    }

    @Override
    public TipoVia getTipoViaById(Long tipoViaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoVia).where(tipoVia.id.eq(tipoViaId));
        return query.uniqueResult(tipoVia);
    }

    @Override
    @Transactional
    public void deleteTipoVia(Long tipoViaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoVia);
        delete.where(tipoVia.id.eq(tipoViaId)).execute();
    }
}      
