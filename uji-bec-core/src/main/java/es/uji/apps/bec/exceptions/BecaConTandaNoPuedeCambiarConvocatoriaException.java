package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaConTandaNoPuedeCambiarConvocatoriaException extends CoreDataBaseException
{
    public BecaConTandaNoPuedeCambiarConvocatoriaException()
    {
        super("La beca canviarà de convocatoria. Cal llevar la tanda abans de calcular la proposta.");
    }

    public BecaConTandaNoPuedeCambiarConvocatoriaException(String message)
    {
        super(message);
    }
}
