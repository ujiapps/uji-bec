package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.ConvocatoriaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_CONVOCATORIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_CONVOCATORIAS")
public class Convocatoria implements Serializable
{
    @Id
    private Long id;

    private String acronimo;

    private Boolean activa;

    private String nombre;

    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "convocatoria")
    private Set<Beca> becas;

    // bi-directional many-to-one association to Organismo
    @ManyToOne
    private Organismo organismo;

    // bi-directional many-to-one association to Tanda
    @OneToMany(mappedBy = "convocatoria")
    private Set<Tanda> tandas;

    // bi-directional many-to-one association to Cuantia
    @OneToMany(mappedBy = "convocatoria")
    private Set<Cuantia> cuantias;

    // bi-directional many-to-one association to RegistroEnvio
    @OneToMany(mappedBy = "convocatoria")
    private Set<RegistroEnvio> registroEnvios;

    private static ConvocatoriaDAO convocatoriaDAO;

    @Autowired
    public void setConvocatoriaDAO(ConvocatoriaDAO convocatoriaDAO)
    {
        Convocatoria.convocatoriaDAO = convocatoriaDAO;
    }

    public Convocatoria()
    {
        this.setActiva(false);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAcronimo()
    {
        return this.acronimo;
    }

    public void setAcronimo(String acronimo)
    {
        this.acronimo = acronimo;
    }

    public Boolean isActiva()
    {
        return this.activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public void setOrganismo(Organismo organismo)
    {
        this.organismo = organismo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Tanda> getTandas()
    {
        return this.tandas;
    }

    public void setTandas(Set<Tanda> tandas)
    {
        this.tandas = tandas;
    }

    public Set<Cuantia> getCuantias()
    {
        return this.cuantias;
    }

    public void setCuantias(Set<Cuantia> cuantias)
    {
        this.cuantias = cuantias;
    }

    public Set<RegistroEnvio> getRegistroEnvios()
    {
        return this.registroEnvios;
    }

    public void setRegistroEnvios(Set<RegistroEnvio> registroEnvios)
    {
        this.registroEnvios = registroEnvios;
    }

    public static List<Convocatoria> getConvocatoriasActivasPorOrganismoId(Long organismoId)
    {
        return convocatoriaDAO.getConvocatoriasActivasPorOrganismoId(organismoId);
    }

    public static Convocatoria getConvocatoriaById(Long convocatoriaId)
    {
        return convocatoriaDAO.get(Convocatoria.class, convocatoriaId).get(0);
    }

    public static Convocatoria getConvocatoriaByAcronimo(String acronimo)
    {
        return convocatoriaDAO.getConvocatoriaByAcronimo(acronimo);
    }
}