package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.CursoAcademicoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_CURSOS_ACADEMICOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_CURSOS_ACADEMICOS")
public class CursoAcademico implements Serializable
{
    private static CursoAcademicoDAO cursoAcademicoDAO;
    @Id
    private Long id;
    private Boolean activo;
    // bi-directional many-to-one association to CuantiaPorCurso
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<CuantiaCurso> cuantiasPorCursos;
    // bi-directional many-to-one association to PersonaEstudio
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<PersonaEstudio> personasEstudios;
    // bi-directional many-to-one association to Minimo
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<Minimo> minimos;
    // bi-directional many-to-one association to Solicitante
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<Solicitante> solicitantes;
    // bi-directional many-to-one association to Tanda
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<Tanda> tandas;
    // bi-directional many-to-one association to Diccionario
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<Diccionario> diccionarios;
    // bi-directional many-to-one association to RegistroEnvio
    @OneToMany(mappedBy = "cursoAcademico")
    private Set<RegistroEnvio> registroEnvios;

    public CursoAcademico()
    {
        this.setActivo(false);
    }

    public static List<CursoAcademico> getCursoActivo()
    {
        return cursoAcademicoDAO.getCursoActivo();
    }

    public static CursoAcademico getCursoAcademicoById(Long cursoAcaId)
    {
        return cursoAcademicoDAO.get(CursoAcademico.class, cursoAcaId).get(0);
    }

    @Autowired
    public void setCursoAcademicoDAO(CursoAcademicoDAO cursoAcademicoDAO)
    {
        CursoAcademico.cursoAcademicoDAO = cursoAcademicoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean isActivo()
    {
        return this.activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public Set<CuantiaCurso> getCuantiasPorCursos()
    {
        return this.cuantiasPorCursos;
    }

    public void setCuantiasPorCursos(Set<CuantiaCurso> cuantiasPorCursos)
    {
        this.cuantiasPorCursos = cuantiasPorCursos;
    }

    public Set<PersonaEstudio> getPersonasEstudios()
    {
        return this.personasEstudios;
    }

    public void setPersonasEstudios(Set<PersonaEstudio> personasEstudios)
    {
        this.personasEstudios = personasEstudios;
    }

    public Set<Minimo> getMinimos()
    {
        return this.minimos;
    }

    public void setMinimos(Set<Minimo> minimos)
    {
        this.minimos = minimos;
    }

    public Set<Solicitante> getSolicitantes()
    {
        return this.solicitantes;
    }

    public void setSolicitantes(Set<Solicitante> solicitantes)
    {
        this.solicitantes = solicitantes;
    }

    public Set<Tanda> getTandas()
    {
        return this.tandas;
    }

    public void setTandas(Set<Tanda> tandas)
    {
        this.tandas = tandas;
    }

    public Set<Diccionario> getDiccionarios()
    {
        return this.diccionarios;
    }

    public void setDiccionarios(Set<Diccionario> diccionarios)
    {
        this.diccionarios = diccionarios;
    }

    public Set<RegistroEnvio> getRegistroEnvios()
    {
        return this.registroEnvios;
    }

    public void setRegistroEnvios(Set<RegistroEnvio> registroEnvios)
    {
        this.registroEnvios = registroEnvios;
    }

    @Transactional
    public CursoAcademico insert()
    {
        return cursoAcademicoDAO.insert(this);
    }

    @Transactional
    public void update() {
        cursoAcademicoDAO.update(this);
    }
}