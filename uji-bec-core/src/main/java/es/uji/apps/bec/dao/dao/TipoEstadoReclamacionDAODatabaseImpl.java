package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.QTipoEstadoReclamacion;
import es.uji.apps.bec.model.TipoEstadoReclamacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TipoEstadoReclamacionDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoEstadoReclamacionDAO
{
    private QTipoEstadoReclamacion tipoEstadoReclamacion = QTipoEstadoReclamacion.tipoEstadoReclamacion;

    @Override
    public List<TipoEstadoReclamacion> getTiposEstadosReclamaciones() {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoEstadoReclamacion).orderBy(tipoEstadoReclamacion.orden.asc());
        return query.list(tipoEstadoReclamacion);
    }
}
