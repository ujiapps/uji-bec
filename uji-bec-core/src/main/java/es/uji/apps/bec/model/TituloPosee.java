package es.uji.apps.bec.model;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.TituloPoseeDAO;
import es.uji.apps.bec.webservices.ministerio.solicitudes.TitulacionesSSCCType;

@Component
@Entity
@Table(name = "BC2_TITULOS_POSEE")
@SuppressWarnings("serial")
public class TituloPosee implements Serializable
{
    private static TituloPoseeDAO tituloPoseeDAO;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CODIGO_NIVEL_TIPO")
    private String codigoNivelTipo;

    @Column(name = "NOMBRE_NIVEL_TIPO")
    private String nombreNivelTipo;

    @Column(name = "CODIGO_TITULO")
    private String codigoTitulo;

    @Column(name = "NOMBRE_TITULO")
    private String nombreTitulo;

    @Column(name = "NOMBRE_TITULO_LARGO")
    private String nombreTituloLargo;

    @Column(name = "SEC_TITULO")
    private String secTitulo;

    @ManyToOne
    private Solicitante solicitante;

    @Autowired
    public void setTitulosPoseeDAO(TituloPoseeDAO tituloPoseeDAO)
    {
        TituloPosee.tituloPoseeDAO = tituloPoseeDAO;
    }

    public TituloPosee()
    {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoNivelTipo() {
        return codigoNivelTipo;
    }

    public void setCodigoNivelTipo(String codigoNivelTipo) {
        this.codigoNivelTipo = codigoNivelTipo;
    }

    public String getNombreNivelTipo() {
        return nombreNivelTipo;
    }

    public void setNombreNivelTipo(String nombreNivelTipo) {
        this.nombreNivelTipo = nombreNivelTipo;
    }

    public String getCodigoTitulo() {
        return codigoTitulo;
    }

    public void setCodigoTitulo(String codigoTitulo) {
        this.codigoTitulo = codigoTitulo;
    }

    public String getNombreTitulo() {
        return nombreTitulo;
    }

    public void setNombreTitulo(String nombreTitulo) {
        this.nombreTitulo = nombreTitulo;
    }

    public String getNombreTituloLargo() {
        return nombreTituloLargo;
    }

    public void setNombreTituloLargo(String nombreTituloLargo) {
        this.nombreTituloLargo = nombreTituloLargo;
    }

    public String getSecTitulo() {
        return secTitulo;
    }

    public void setSecTitulo(String secTitulo) {
        this.secTitulo = secTitulo;
    }

    public Solicitante getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }

    public void importarDatosMinisterio(TitulacionesSSCCType titulacionesSSCCType)
    {
        this.setCodigoNivelTipo(titulacionesSSCCType.getTiscCodNivelTipo());
        this.setCodigoTitulo(titulacionesSSCCType.getTiscCodTitulo());
        this.setNombreNivelTipo(titulacionesSSCCType.getTiscDesNivelTipo());
        this.setNombreTitulo(titulacionesSSCCType.getTiscDesTitulo());
        this.setNombreTituloLargo(titulacionesSSCCType.getTiscDesTituloLarga());
        this.setSecTitulo(titulacionesSSCCType.getTiscSecTitulo());
    }

    @Transactional
    public TituloPosee insert()
    {
        return tituloPoseeDAO.insert(this);
    }

    @Transactional
    public void delete()
    {
        tituloPoseeDAO.delete(TituloPosee.class, getId());
    }
}
