package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class BecaException extends GeneralBECException
{
    public BecaException(String message)
    {
        super(message);
    }
}
