package es.uji.apps.bec.dao.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.QBeca;
import es.uji.apps.bec.model.QConvocatoria;
import es.uji.apps.bec.model.QPersona;
import es.uji.apps.bec.model.QSolicitante;
import es.uji.apps.bec.model.Solicitante;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SolicitanteDAODatabaseImpl extends BaseDAODatabaseImpl implements SolicitanteDAO
{
    @Override
    public List<Beca> getSolicitanteConFiltros(Map<String, String> filterParams)
    {
        Long cursoAcademicoId = convierteStringALong(filterParams.get("cursoAcademicoId").trim());
        Long convocatoriaId = convierteStringALong(filterParams.get("convocatoriaId").trim());
        Long procesoId = convierteStringALong(filterParams.get("procesoId").trim());
        Long estadoId = convierteStringALong(filterParams.get("estadoId").trim());
        Long personaId = convierteStringALong(filterParams.get("personaId").trim());
        Long tandaOrdenId = convierteStringALong(filterParams.get("tandaOrdenId").trim());
        Long becaId = convierteStringALong(filterParams.get("id").trim());
        Long estudioId = convierteStringALong(filterParams.get("estudioId").trim());
        Long numeroBecaUji = convierteStringALong(filterParams.get("numeroBecaUji").trim());
        String codigoArchivoTemporal = filterParams.get("codigoArchivoTemporal").trim();
        String sinTanda = filterParams.get("sinTanda").trim();
        String reclamada = filterParams.get("reclamada").trim();
        String identificacion = filterParams.get("identificacion").trim();
        String nombrePersona = filterParams.get("nombrePersona").trim();

        JPAQuery query = new JPAQuery(entityManager);

        QSolicitante solicitante = QSolicitante.solicitante;
        QBeca beca = QBeca.beca;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        boolean filtroNombreApellidosAceptado = false;
        boolean filtroIdentificacionAceptado = false;

        filtroNombreApellidosAceptado = filtraPorNombreApellidos(nombrePersona, condicionesWhere);
        filtroIdentificacionAceptado = filtraPorIdentificacion(identificacion, condicionesWhere);

        if (!filtroNombreApellidosAceptado || !filtroIdentificacionAceptado)
        {
            return new ArrayList<Beca>();
        }

        filtraSinTanda(sinTanda, condicionesWhere);
        filtraReclamada(reclamada, condicionesWhere);
        filtraPorTandaOrdenId(tandaOrdenId, condicionesWhere);
        filtraPorBecaId(becaId, condicionesWhere);
        filtraPorEstado(estadoId, condicionesWhere);
        filtraPorProceso(procesoId, condicionesWhere);
        filtraPorConvocatoria(convocatoriaId, condicionesWhere);
        filtraPorCodigoArchivoTemporal(codigoArchivoTemporal, condicionesWhere);
        filtraPorNumeroBecaUji(numeroBecaUji, condicionesWhere);
        filtraPorCursoAcademico(cursoAcademicoId, condicionesWhere);
        filtraPorPersonaId(personaId, condicionesWhere);
        filtraPorTitulacion(estudioId, condicionesWhere);

        query.from(beca).join(beca.solicitante, solicitante).fetch();

        if (condicionesWhere.hasValue())
        {
            query.where(condicionesWhere);
        }

        query.orderBy(solicitante.cursoAcademico.id.desc(), solicitante.numeroBecaUji.asc());

        return query.list(beca);
    }

    private Long convierteStringALong(String valor)
    {
        try
        {
            return Long.parseLong(valor);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private void filtraPorTitulacion(Long estudioId, BooleanBuilder filtro)
    {
        if (estudioId != null)
        {
            filtro.and(QBeca.beca.estudio.id.eq(estudioId));
        }
    }

    private boolean filtraPorNombreApellidos(String nombreApellidos, BooleanBuilder filtro)
    {
        boolean resultado = true;

        if (nombreApellidos != null && !nombreApellidos.isEmpty())
        {
            JPAQuery queryApellidos = new JPAQuery(entityManager);

            QPersona personaApellidos = QPersona.persona;

            queryApellidos.from(personaApellidos).where(
                    personaApellidos.nombre.toLowerCase().like(
                            "%" + nombreApellidos.toLowerCase() + "%"));

            List<Long> listaPersonasNombre = queryApellidos.list(personaApellidos.id);

            if (!listaPersonasNombre.isEmpty())
            {
                filtro.and(QSolicitante.solicitante.persona.id.in(listaPersonasNombre));
            }
            else
            {
                resultado = false;
            }
        }
        return resultado;

    }

    private boolean filtraPorIdentificacion(String identificacion, BooleanBuilder filtro)
    {
        boolean resultado = true;

        if (identificacion != null && !identificacion.isEmpty())
        {
            JPAQuery queryPersona = new JPAQuery(entityManager);

            QPersona persona = QPersona.persona;

            queryPersona.from(persona).where(
                    persona.identificacion.toLowerCase().like(
                            "%" + identificacion.toLowerCase() + "%"));

            List<Long> listaPersonas = queryPersona.list(persona.id);

            if (!listaPersonas.isEmpty())
            {
                filtro.and(QSolicitante.solicitante.persona.id.in(listaPersonas));
            }
            else
            {
                resultado = false;
            }
        }

        return resultado;
    }

    private void filtraPorBecaId(Long becaId, BooleanBuilder filtro)
    {
        if (becaId != null)
        {
            filtro.and(QBeca.beca.id.eq(becaId));
        }
    }

    private void filtraPorPersonaId(Long personaId, BooleanBuilder filtro)
    {
        if (personaId != null)
        {
            filtro.and(QSolicitante.solicitante.persona.id.eq(personaId));
        }
    }

    private void filtraPorTandaOrdenId(Long tandaId, BooleanBuilder filtro)
    {
        if (tandaId != null)
        {
            filtro.and(QBeca.beca.tanda.tandaId.eq(tandaId));
        }
    }

    private void filtraSinTanda(String sinTanda, BooleanBuilder filtro)
    {
        if (sinTanda != null && sinTanda.equals("true"))
        {
            filtro.and(QBeca.beca.tanda.isNull());
        }
    }

    private void filtraReclamada(String reclamada, BooleanBuilder filtro)
    {
        if (reclamada != null && reclamada.equals("true"))
        {
            filtro.and(QBeca.beca.reclamada.isTrue());
        }
    }

    private void filtraPorCodigoArchivoTemporal(String codigoArchivoTemporal, BooleanBuilder filtro)
    {
        if (codigoArchivoTemporal != null && !codigoArchivoTemporal.isEmpty())
        {
            filtro.and(QBeca.beca.codigoArchivoTemporal.eq(codigoArchivoTemporal));
        }
    }

    private void filtraPorNumeroBecaUji(Long numeroBecaUji, BooleanBuilder filtro)
    {
        if (numeroBecaUji != null)
        {
            filtro.and(QSolicitante.solicitante.numeroBecaUji.eq(numeroBecaUji));
        }
    }

    private void filtraPorEstado(Long estadoId, BooleanBuilder filtro)
    {
        if (estadoId != null)
        {
            filtro.and(QBeca.beca.estado.id.eq(estadoId));
        }
    }

    private void filtraPorProceso(Long procesoId, BooleanBuilder filtro)
    {
        if (procesoId != null)
        {
            filtro.and(QBeca.beca.proceso.id.eq(procesoId));
        }
    }

    private void filtraPorConvocatoria(Long convocatoriaId, BooleanBuilder filtro)
    {
        if (convocatoriaId != null)
        {
            filtro.and(QBeca.beca.convocatoria.id.eq(convocatoriaId));
        }
    }

    private void filtraPorCursoAcademico(Long cursoAcademicoId, BooleanBuilder filtro)
    {
        if (cursoAcademicoId != null)
        {
            filtro.and(QSolicitante.solicitante.cursoAcademico.id.eq(cursoAcademicoId));
        }
    }

    @Override
    public Long getNumeroTotalBecasPorCursoAcademico(Long cursoAcademicoId)
    {
        QSolicitante solicitante = QSolicitante.solicitante;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(solicitante).where(solicitante.cursoAcademico.id.eq(cursoAcademicoId));
        return (Long) query.count();
    }

    @Override
    public List<Solicitante> getDatosGenerales(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSolicitante solicitante = QSolicitante.solicitante;
        QBeca beca = QBeca.beca;

        query.from(solicitante)
                .join(solicitante.becas, beca).fetch()
                .where(beca.id.eq(becaId));

        return query.list(solicitante);
    }

    @Override
    public Solicitante getSolicitanteById(Long solicitanteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSolicitante solicitante = QSolicitante.solicitante;

        query.from(solicitante)
                //.leftJoin(solicitante.miembros).fetch()
                //.leftJoin(solicitante.domicilios).fetch()
                .leftJoin(solicitante.tipoFamilia).fetch()
                .leftJoin(solicitante.tituloPosee).fetch()
                .where(solicitante.id.eq(solicitanteId));

        return query.list(solicitante).get(0);
    }

    @Override
    public Solicitante getSolicitanteByCursoAcademicoAndIdentificacion(String cursoAcademicoId,
                                                                       String identificacion)
    {
        HashMap<String, String> filterParams = new HashMap<String, String>();
        filterParams.put("cursoAcademicoId", cursoAcademicoId);
        filterParams.put("identificacion", identificacion);

        List<Solicitante> solicitantes = getSolicitantes(filterParams);

        if (solicitantes.size() > 0)
        {
            return solicitantes.get(0);
        }

        return null;
    }

    @Override
    public Solicitante getSolicitanteByCursoAcademicoAndPersonaId(String cursoAcademicoId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSolicitante solicitante = QSolicitante.solicitante;

        query.from(solicitante)
                .leftJoin(solicitante.becas)
                .leftJoin(solicitante.declarante).fetch()
                .leftJoin(solicitante.tipoFamilia).fetch()
                .leftJoin(solicitante.tituloPosee).fetch()
                .where(solicitante.cursoAcademico.id.eq(Long.parseLong(cursoAcademicoId))
                        .and(solicitante.persona.id.eq(personaId)));

        List<Solicitante> resultado = query.list(solicitante);
        if (resultado.size() > 0)
        {
            return resultado.get(0);
        }
        return null;
    }

    @Override
    public List<Solicitante> getSolicitantes(HashMap<String, String> filterParams)
    {
        Long cursoAcademicoId = convierteStringALong(filterParams.get("cursoAcademicoId"));
        Long numeroBecaUji = convierteStringALong(filterParams.get("numeroBecaUji"));
        Long personaId = convierteStringALong(filterParams.get("personaId"));
        String identificacion = filterParams.get("identificacion");

        JPAQuery query = new JPAQuery(entityManager);

        QSolicitante qSolicitante = QSolicitante.solicitante;

        query.from(qSolicitante).leftJoin(qSolicitante.becas).fetch();

        BooleanBuilder condicionesWhere = new BooleanBuilder();

        filtraPorCursoAcademico(cursoAcademicoId, condicionesWhere);
        filtraPorPersonaId(personaId, condicionesWhere);
        filtraPorNumeroBecaUji(numeroBecaUji, condicionesWhere);
        filtraPorIdentificacion(identificacion, condicionesWhere);

        if (condicionesWhere.hasValue())
        {
            query.where(condicionesWhere);
        }

        query.orderBy(qSolicitante.cursoAcademico.id.desc(), qSolicitante.numeroBecaUji.asc());

        return query.list(qSolicitante);
    }

    @Override
    public Solicitante getSolicitanteAndBeca(Long solicitanteId, Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSolicitante solicitante = QSolicitante.solicitante;
        QBeca beca = QBeca.beca;

        query.from(solicitante).join(solicitante.becas, beca).fetch()
                .where(solicitante.id.eq(solicitanteId).and(beca.id.eq(becaId)));

        List<Solicitante> resultado = query.list(solicitante);
        if (resultado.size() > 0)
        {
            return resultado.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public void updateOtrosDatos(Solicitante solicitante)
    {
        QSolicitante qSolicitante = QSolicitante.solicitante;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qSolicitante);

        updateClause.where(qSolicitante.id.eq(solicitante.getId()))
                .set(qSolicitante.telefono1, solicitante.getTelefono1())
                .set(qSolicitante.telefono2, solicitante.getTelefono2())
                .set(qSolicitante.email, solicitante.getEmail()).execute();
    }

    @Override
    @Transactional
    public void updateDatosGenerales(Solicitante solicitante)
    {
        QSolicitante qSolicitante = QSolicitante.solicitante;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qSolicitante);

        updateClause
                .where(qSolicitante.id.eq(solicitante.getId()))
                .set(qSolicitante.tipoFamilia, solicitante.getTipoFamilia())
                .set(qSolicitante.numeroMiembrosComputables, solicitante.getNumeroMiembrosComputables())
                .set(qSolicitante.numeroHermanos, solicitante.getNumeroHermanos())
                .set(qSolicitante.numeroHermanosFuera, solicitante.getNumeroHermanosFuera())
                .set(qSolicitante.numeroMinusvalia33, solicitante.getNumeroMinusvalia33())
                .set(qSolicitante.numeroMinusvalia65, solicitante.getNumeroMinusvalia65())
                .set(qSolicitante.orfandad, solicitante.isOrfandad())
                .set(qSolicitante.profesionSustentador, solicitante.getProfesionSustentador())
                .set(qSolicitante.carnetFamiliaNumerosa, solicitante.getCarnetFamiliaNumerosa())
                .set(qSolicitante.comunidadFamiliaNumerosa, solicitante.getComunidadFamiliaNumerosa())
                .set(qSolicitante.fechaIniFamiliaNumerosa, solicitante.getFechaIniFamiliaNumerosa())
                .set(qSolicitante.fechaFinFamiliaNumerosa, solicitante.getFechaFinFamiliaNumerosa())
                .set(qSolicitante.fechaOrfandad, solicitante.getFechaOrfandad())
                .set(qSolicitante.fechaFallPrimProgenitor, solicitante.getFechaFallPrimProgenitor())
                .set(qSolicitante.indFamiMonoparental, solicitante.isIndFamiMonoparental())
                .set(qSolicitante.violenciaGenero, solicitante.isViolenciaGenero())
                .set(qSolicitante.residenciaPermanente, solicitante.isResidenciaPermanente())
                .set(qSolicitante.residenciaTrabajo, solicitante.isResidenciaTrabajo())
                .set(qSolicitante.residenciaEstanciaEstudios, solicitante.isResidenciaEstanciaEstudios())
                .set(qSolicitante.otraBeca, solicitante.getOtraBeca())
                .set(qSolicitante.indVictimaVg, solicitante.isIndVictimaVg())
                .set(qSolicitante.indHijoVictimaVg, solicitante.isIndHijoVictimaVg())
                .set(qSolicitante.fechaVictimaVg, solicitante.getFechaVictimaVg())
                .set(qSolicitante.fechaHijoVictimaVg, solicitante.getFechaHijoVictimaVg())
                .set(qSolicitante.fechaRevFamNumMec, solicitante.getFechaRevFamNumMec())
                .set(qSolicitante.fechaRevFamNumUt, solicitante.getFechaRevFamNumUt())
                .execute();
    }

    @Override
    public List<Beca> getBecasByOrganismoAndSolicitante(Long solicitanteId, Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;
        QSolicitante solicitante = QSolicitante.solicitante;
        QConvocatoria convocatoria = QConvocatoria.convocatoria;

        query.from(beca)
                .join(beca.solicitante, solicitante)
                .fetch()
                .join(beca.convocatoria, convocatoria)
                .fetch()
                .where(solicitante.id.eq(solicitanteId).and(
                        convocatoria.organismo.id.eq(organismoId)));

        return query.list(beca);
    }

    @Override
    public Solicitante getSolicitanteConBecasById(Long solicitanteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSolicitante solicitante = QSolicitante.solicitante;
        QBeca beca = QBeca.beca;

        query.from(solicitante).join(solicitante.becas, beca).fetch()
                .where(solicitante.id.eq(solicitanteId));

        List<Solicitante> resultado = query.list(solicitante);
        if (resultado.size() > 0)
        {
            return resultado.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public Solicitante updateSolicitante(Solicitante solicitante)
    {
        QSolicitante qSolicitante = QSolicitante.solicitante;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qSolicitante);

        updateClause.where(qSolicitante.id.eq(solicitante.getId()))
                .set(qSolicitante.email, solicitante.getEmail())
                .set(qSolicitante.numeroHermanos, solicitante.getNumeroHermanos())
                .set(qSolicitante.numeroMiembrosComputables, solicitante.getNumeroMiembrosComputables())
                .set(qSolicitante.numeroHermanosFuera, solicitante.getNumeroHermanosFuera())
                .set(qSolicitante.numeroMinusvalia33, solicitante.getNumeroMinusvalia33())
                .set(qSolicitante.numeroMinusvalia65, solicitante.getNumeroMinusvalia65())
                .set(qSolicitante.orfandad, solicitante.isOrfandad())
                .set(qSolicitante.violenciaGenero, solicitante.isViolenciaGenero())
                .set(qSolicitante.telefono1, solicitante.getTelefono1())
                .set(qSolicitante.telefono2, solicitante.getTelefono2())
                .set(qSolicitante.carnetFamiliaNumerosa, solicitante.getCarnetFamiliaNumerosa())
                .set(qSolicitante.fechaIniFamiliaNumerosa, solicitante.getFechaIniFamiliaNumerosa())
                .set(qSolicitante.fechaFinFamiliaNumerosa, solicitante.getFechaFinFamiliaNumerosa())
                .set(qSolicitante.fechaOrfandad, solicitante.getFechaOrfandad())

                .set(qSolicitante.comunidadFamiliaNumerosa, solicitante.getComunidadFamiliaNumerosa())
                .set(qSolicitante.cursoAcademico, solicitante.getCursoAcademico())
                .set(qSolicitante.persona, solicitante.getPersona())
                .set(qSolicitante.profesionSustentador, solicitante.getProfesionSustentador())
                .set(qSolicitante.tipoFamilia, solicitante.getTipoFamilia())

                .set(qSolicitante.residenciaPermanente, solicitante.isResidenciaPermanente())
                .set(qSolicitante.residenciaTrabajo, solicitante.isResidenciaTrabajo())
                .set(qSolicitante.residenciaEstanciaEstudios, solicitante.isResidenciaEstanciaEstudios())

                .set(qSolicitante.indVictimaVg, solicitante.isIndVictimaVg())
                .set(qSolicitante.indHijoVictimaVg, solicitante.isIndHijoVictimaVg())
                .set(qSolicitante.fechaVictimaVg, solicitante.getFechaVictimaVg())
                .set(qSolicitante.fechaHijoVictimaVg, solicitante.getFechaHijoVictimaVg())
                .set(qSolicitante.fechaRevFamNumMec, solicitante.getFechaRevFamNumMec())
                .set(qSolicitante.fechaRevFamNumUt, solicitante.getFechaRevFamNumUt())

                .execute();

        return solicitante;
    }
}
