package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaDenegacionNoEncontradaException extends CoreDataBaseException
{
    public BecaDenegacionNoEncontradaException()
    {
        super("La denegació no s'ha trobat");
    }

    public BecaDenegacionNoEncontradaException(String message)
    {
        super(message);
    }
}