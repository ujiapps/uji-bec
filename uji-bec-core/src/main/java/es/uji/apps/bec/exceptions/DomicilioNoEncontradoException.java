package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class DomicilioNoEncontradoException extends CoreBaseException
{

    public DomicilioNoEncontradoException()
    {
        super("No s'ha encontrat el domicili");
    }

    public DomicilioNoEncontradoException(String message)
    {
        super(message);
    }
}
