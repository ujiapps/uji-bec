package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.TipoEstadoReclamacion;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TipoEstadoReclamacionDAO extends BaseDAO
{
    List<TipoEstadoReclamacion> getTiposEstadosReclamaciones();
}
 