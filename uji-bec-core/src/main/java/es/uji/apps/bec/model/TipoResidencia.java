package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoResidenciaDAO;

/**
 * The persistent class for the BC2_TIPOS_RESIDENCIAS database table.
 */
@Entity
@Component
@Table(name = "BC2_TIPOS_RESIDENCIAS")
public class TipoResidencia implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static TipoResidenciaDAO tipoResidenciaDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private Long orden;
    // bi-directional many-to-one association to Domicilio
    @OneToMany(mappedBy = "tipoResidencia")
    private Set<Domicilio> domicilios;

    public TipoResidencia()
    {
    }

    @Autowired
    public void setTipoResidenciaDAO(TipoResidenciaDAO tipoResidenciaDAO)
    {
        TipoResidencia.tipoResidenciaDAO = tipoResidenciaDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Domicilio> getDomicilios()
    {
        return domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }
}