package es.uji.apps.bec.model.domains;

import es.uji.apps.bec.exceptions.RamaNoExisteException;

public enum Rama
{
    Humanitats("HU", 90L), Experimentals("EX", 80L), Socials("SO", 90L), Salut("SA", 80L), Tecniques(
            "TE", 65L);

    private String codigo;
    private Long porcentaje;

    private Rama(String codigo, Long porcentaje)
    {
        this.codigo = codigo;
        this.porcentaje = porcentaje;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public Long getPorcentaje()
    {
        return porcentaje;
    }

    public static Rama getRamaByCodigo(String codigo) throws RamaNoExisteException
    {
        for (Rama current : Rama.values())
        {
            if (current.getCodigo().equalsIgnoreCase(codigo))
            {
                return current;
            }
        }

        throw new RamaNoExisteException("El codi de rama indicat no existeix.");
    }
}
