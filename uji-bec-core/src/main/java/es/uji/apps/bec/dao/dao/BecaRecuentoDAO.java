package es.uji.apps.bec.dao.dao;

import com.mysema.query.Tuple;
import es.uji.apps.bec.model.BecaRecuento;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface BecaRecuentoDAO extends BaseDAO
{
    List<BecaRecuento> getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(
            Long cursoAcademicoId, Long organismoId, Long convocatoriaId);

    List<Tuple> getDistinctOrganismoConvocatoriaByCursoAcademico(Long cursoAcademicoId);
}
