package es.uji.apps.bec.model;

import javax.ws.rs.core.MediaType;

import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

public class CorreoElectronico
{
    public static void enviaCorreoLoteMecDescargado(Long lote) throws MessageNotSentException
    {
        StringBuffer cuerpo = new StringBuffer();
        cuerpo.append("S\'ha descarregat el lot " + lote.toString()
                + " de resolucions del Ministeri. Aquest lot queda pendent de ser processat");
        StringBuffer asunto = new StringBuffer();
        asunto.append("BEC - Lot Resolucions Ministeri " + lote.toString() + " descarregat ");

        enviar(cuerpo, asunto, "revillo@uji.es");
        enviar(cuerpo, asunto, "becas@uji.es" );
    }

    private static void enviar(StringBuffer cuerpo, StringBuffer asunto, String destinatario)
            throws MessageNotSentException
    {
        MailMessage mensaje = new MailMessage("BEC");
        mensaje.setTitle(asunto.toString());
        mensaje.setContentType(MediaType.TEXT_PLAIN);
        mensaje.setSender("no_reply@uji.es");
        mensaje.setContent(cuerpo.toString());

        mensaje.addToRecipient(destinatario);

        MessagingClient client = new MessagingClient();
//        Comentado para migracion
//        client.send(mensaje);
    }
}
