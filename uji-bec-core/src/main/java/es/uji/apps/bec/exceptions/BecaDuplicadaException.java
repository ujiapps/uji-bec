package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class BecaDuplicadaException extends GeneralBECException
{
    public BecaDuplicadaException()
    {
        super("Beca duplicada. Se está insertando/actualizando una beca que provoca duplicación de datos.");
    }
    
    public BecaDuplicadaException(String message)
    {
        super(message);
    }
}
