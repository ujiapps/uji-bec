package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.exceptions.BecaDenegacionDuplicadaException;
import es.uji.apps.bec.dao.dao.BecaDenegacionDAO;

/**
 * The persistent class for the BC2_BECAS_DENEGACIONES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_BECAS_DENEGACIONES")
public class BecaDenegacion implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ORDEN_DENEGACION")
    private Long ordenDenegacion;

    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;

    // bi-directional many-to-one association to Denegacion
    @ManyToOne
    private Denegacion denegacion;

    private static BecaDenegacionDAO becaDenegacionDAO;

    @Autowired
    public void setBecaDenegacionDAO(BecaDenegacionDAO becaDenegacionDAO)
    {
        BecaDenegacion.becaDenegacionDAO = becaDenegacionDAO;
    }

    public BecaDenegacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Beca getBeca()
    {
        return beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    public Long getOrdenDenegacion()
    {
        return this.ordenDenegacion;
    }

    public void setOrdenDenegacion(Long ordenDenegacion)
    {
        this.ordenDenegacion = ordenDenegacion;
    }

    public Denegacion getDenegacion()
    {
        return this.denegacion;
    }

    public void setDenegacion(Denegacion denegacion)
    {
        this.denegacion = denegacion;
    }

    public static void borraDenegacion(Long id)
    {
        becaDenegacionDAO.delete(BecaDenegacion.class, id);
    }

    @Transactional
    public void insert() throws BecaDenegacionDuplicadaException
    {
        try
        {
            becaDenegacionDAO.insert(this);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new BecaDenegacionDuplicadaException(
                    "No es poden inserir dues denegacions iguals amb el mateix nombre d'ordre per a la mateixa beca: "
                            + this.getBeca().getId().toString());
        }
    }

    public static List<BecaDenegacion> getDenegacionesByBecaId(Long becaId)
    {
        return becaDenegacionDAO.getDenegacionesByBecaId(becaId);
    }

    @Transactional
    public static void delete(Long becaDenegacionId)
    {
        becaDenegacionDAO.delete(BecaDenegacion.class, becaDenegacionId);
    }

    public static List<BecaDenegacion> getDenegacionesByBecaIdAndOrden(Long becaId, Long orden)
    {
        return becaDenegacionDAO.getDenegacionesByBecaIdAndOrden(becaId, orden);
    }
}