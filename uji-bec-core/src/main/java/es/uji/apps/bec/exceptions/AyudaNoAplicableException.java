package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class AyudaNoAplicableException extends GeneralBECException
{
    public AyudaNoAplicableException()
    {
        super("La beca ha d'estar en estat pendent o treballada per a poder calcular ajudes");
    }

    public AyudaNoAplicableException(String message)
    {
        super(message);
    }
}
