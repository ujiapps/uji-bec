package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoIdentificacion;
import es.uji.commons.db.BaseDAO;

public interface TipoIdentificacionDAO extends BaseDAO
{
    List<TipoIdentificacion> getTiposIdentificaciones();

    TipoIdentificacion getTipoIdentificacionById(Long tipoIdentificacionId);

    void deleteTipoIdentificacion(Long tipoIdentificacionId);
}