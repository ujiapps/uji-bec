package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Profesion;
import es.uji.commons.db.BaseDAO;

public interface ProfesionDAO extends BaseDAO
{
    List<Profesion> getProfesiones();

     List<Profesion>  getProfesionById(Long profesionId);
}