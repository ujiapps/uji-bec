package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class TandaAsignadaNoEstaActivaException extends CoreDataBaseException
{
    public TandaAsignadaNoEstaActivaException()
    {
        super("La tanda assignada no està activa.");
    }

    public TandaAsignadaNoEstaActivaException(String message)
    {
        super(message);
    }
}