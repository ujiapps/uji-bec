package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.TipoDocumento;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TipoDocumentoDAO extends BaseDAO
{
    List<TipoDocumento> getTiposDocumentos();

    TipoDocumento getTipoDocumentoById(Long tipoDocumentoId);

    void deleteTipoDocumento(Long id);
}
 