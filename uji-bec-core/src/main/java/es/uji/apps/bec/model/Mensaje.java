package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.MensajeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_EXT_MENSAJES", schema = "UJI_BECAS")
public class Mensaje implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "FECHA_INICIO")
    private Date fecha;
    private String asunto;
    private String cuerpo;
    private String remitente;
    private String referencia;
    @Column(name = "ENVIADO_A")
    private String enviadoA;

    private static MensajeDAO mensajeDAO;

    @Autowired
    public void setMensajeDAO(MensajeDAO mensajeDAO)
    {
        Mensaje.mensajeDAO = mensajeDAO;
    }

    public Mensaje()
    {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String texto) {
        this.asunto = asunto;
    }
    
    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String texto) {
        this.cuerpo = cuerpo;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    
    public String getEnviadoA() {
        return enviadoA;
    }

    public void setEnviadoA(String enviadoA) {
        this.enviadoA = enviadoA;
    }

    public static List<Mensaje> getMensajesByBecaId(Long becaId)
    {
        return mensajeDAO.getMensajesByBecaId(becaId);
    }
}