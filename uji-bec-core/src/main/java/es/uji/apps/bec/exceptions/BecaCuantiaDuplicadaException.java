package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaCuantiaDuplicadaException extends CoreDataBaseException
{
    public BecaCuantiaDuplicadaException()
    {
        super("No es poden inserir dues quanties iguals per a la mateixa beca");
    }

    public BecaCuantiaDuplicadaException(String message)
    {
        super(message);
    }
}