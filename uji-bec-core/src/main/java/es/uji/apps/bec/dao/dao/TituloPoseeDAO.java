package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TituloPosee;
import es.uji.commons.db.BaseDAO;

public interface TituloPoseeDAO extends BaseDAO
{

    List<TituloPosee> getTitulosPoseeBySolicitante(Long solicitanteId);
}
