package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Texto;
import es.uji.commons.db.BaseDAO;

public interface TextoDAO extends BaseDAO
{
    List<Texto> getTextosPorProcesoYOrganismo(Long procesoId, Long organismoId);

    Texto updateTexto(Texto texto);
}