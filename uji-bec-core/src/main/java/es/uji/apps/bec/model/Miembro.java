package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.MiembroDAO;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.model.domains.MapeadorEstadoCivil;
import es.uji.apps.bec.model.domains.MapeadorSituacionLaboral;
import es.uji.apps.bec.webservices.ministerio.solicitudes.ActivEconomicasType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.FamiliarType;

/**
 * The persistent class for the BC2_MIEMBROS database table.
 */
@Component
@Entity
@Table(name = "BC2_MIEMBROS")
@SuppressWarnings("serial")
public class Miembro implements Serializable
{
    public static final String NO_MINUSVALIA = "0";
    private static final Long ORGANISMO_MINISTERIO = 1L;
    private static final String CLASE_PROFESION = "Profesion";
    private static final long TIPO_MIEMBRO_SOLICITANTE = 1L;
    private static final long TIPO_SUSTENTADOR_PRINCIPAL = 1L;
    private static final long TIPO_SUSTENTADOR_CONYUGE = 2L;
    private static MiembroDAO miembroDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String apellido1;
    private String apellido2;
    private String nombre;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;
    private String identificacion;
    @Column(name = "LOCALIDAD_TRABAJO")
    private String localidadTrabajo;
    @Column(name = "NO_VALIDA_IDENTIFICACION")
    private Boolean noValidaIdentificacion;
    @Column(name = "PADRON_LOCALIDAD_ID")
    private Long padronLocalidadId;
    @Column(name = "PADRON_PROVINCIA_ID")
    private Long padronProvinciaId;
    @Column(name = "IDENTIFICACION_IDESP")
    private String identificacionIdesp;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CAD_NIF")
    private Date fechaCadNif;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RES_MINUS")
    private Date fechaResMinus;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_MINUS")
    private Date fechaFinMinus;
    @Column(name = "CODIGO_ESTUDIO_HERMANO")
    private String codigoEstudioHermano;
    @Column(name = "CODIGO_UNIV_HERMANO")
    private String codigoUnivHermano;
    @Column(name = "ESTUDIA_FUERA")
    private Boolean estudiaFuera;
    @Column(name = "FIRMA_SOLICITUD")
    private Boolean firmaSolicitud;
    @Column(name = "PORC_ACTIV_ECONOMICAS")
    private Float porcentajeActividadesEconomicas;

    @Column(name = "IMPORT_ACTIV_ECONOMICAS")
    private Float importeActividadesEconomicas;

    @Column(name = "IMPORT_RENTAS_EXTRAN")
    private Float importeRentasExtranjero;

    @Column(name = "RESIDENCIA")
    private Boolean residencia;

    @Column(name = "CUSTODIA")
    private Boolean custodia;
    @Column(name = "CUSTODIA_COMPARTIDA")
    private Boolean custodiaCompartida;

    @Column(name = "IND_DECL_RENTA")
    private Boolean indDeclRenta;

    @Column(name = "IND_RENTAS_ESPANNA")
    private Boolean indRentasEspanna;

    @Column(name = "IND_RENTAS_NAVARRA")
    private Boolean indRentasNavarra;

    @Column(name = "IND_RENTAS_PAIS_VASCO")
    private Boolean indRentasPaisVasco;

    @Column(name = "IND_CONVIVE_PAREJA")
    private Boolean indConvivePareja;

    @Column(name = "IND_INGRESOS_PROPIOS")
    private Boolean indIngresosPropios;

    @Column(name = "IND_REV_MINUS_MEC")
    private Boolean indRevMinusMec;

    @Column(name = "IND_REV_MINUS_UT")
    private Boolean indRevMinusUt;

    @Column(name = "IND_ARREN_UNID_FAMILIAR")
    private Boolean indArrenUnidFamiliar;

    @Column(name = "NIF_ARREN")
    private String nifArren;

    @Column(name = "PRECIO_ALQUILER")
    private Float precioAlquiler;

    @Column(name = "IND_HIJOS_MAYORES_25")
    private Boolean indHijosMayores25;

    @Column(name = "IND_PENSION_COMPENSATORIA")
    private Boolean indPensionCompensatoria;

    @ManyToOne
    @JoinColumn(name = "TIPO_MONEDA_ID")
    private TipoMoneda tipoMoneda;
    // bi-directional many-to-one association to EstadoCivil
    @ManyToOne
    @JoinColumn(name = "CCAA_MINUS")
    private Comunidad ccaaMinus;
    // bi-directional many-to-one association to EstadoCivil
    @ManyToOne
    @JoinColumn(name = "ESTADO_CIVIL_ID")
    private EstadoCivil estadoCivil;
    // bi-directional many-to-one association to Pais
    @ManyToOne
    @JoinColumn(name = "NACIONALIDAD_ID")
    private Pais pais;
    // bi-directional many-to-one association to Profesion
    @ManyToOne
    private Profesion profesion;
    // bi-directional many-to-one association to SituacionLaboral
    @ManyToOne
    @JoinColumn(name = "SITUACION_LABORAL_ID")
    private SituacionLaboral situacionLaboral;
    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;
    // bi-directional many-to-one association to TipoIdentificacion
    @ManyToOne
    @JoinColumn(name = "TIPO_IDENTIFICACION_ID")
    private TipoIdentificacion tipoIdentificacion;
    // bi-directional many-to-one association to TipoMiembro
    @ManyToOne
    @JoinColumn(name = "TIPO_MIEMBRO_ID")
    private TipoMiembro tipoMiembro;
    // bi-directional many-to-one association to TipoMinusvalia
    @ManyToOne
    @JoinColumn(name = "TIPO_MINUSVALIA_ID")
    private TipoMinusvalia tipoMinusvalia;
    // bi-directional many-to-one association to TipoSexo
    @ManyToOne
    @JoinColumn(name = "SEXO_ID")
    private TipoSexo tipoSexo;
    // bi-directional many-to-one association to TipSustentador
    @ManyToOne
    @JoinColumn(name = "SUSTENTADOR_ID")
    private TipoSustentador tipoSustentador;
    // bi-directional many-to-one association to ActividadEconomica
    @OneToMany(mappedBy = "miembro")
    private Set<ActividadEconomica> actividadesEconomicas;

    public Miembro()
    {
        this.setNoValidaIdentificacion(false);
        this.setEstudiaFuera(false);
        this.setFirmaSolicitud(false);
        this.setResidencia(false);
        this.setCustodia(false);
        this.setCustodiaCompartida(false);
        this.setIndDeclRenta(false);
        this.setIndRentasEspanna(false);
        this.setIndRentasNavarra(false);
        this.setIndRentasPaisVasco(false);
        this.setIndConvivePareja(false);
        this.setIndRevMinusMec(false);
        this.setIndRevMinusUt(false);
        this.setIndIngresosPropios(false);
        this.setIndArrenUnidFamiliar(false);
        this.setIndHijosMayores25(false);
        this.setIndPensionCompensatoria(false);
    }

    public void setNoValidaIdentificacion(Boolean noValidaIdentificacion)
    {
        this.noValidaIdentificacion = noValidaIdentificacion;
    }

    public void setResidencia(Boolean residencia)
    {
        this.residencia = residencia;
    }

    public void setCustodia(Boolean custodia)
    {
        this.custodia = custodia;
    }

    public void setIndDeclRenta(Boolean indDeclRenta)
    {
        this.indDeclRenta = indDeclRenta;
    }

    public void setIndRentasEspanna(Boolean indRentasEspanna)
    {
        this.indRentasEspanna = indRentasEspanna;
    }

    public void setIndRentasNavarra(Boolean indRentasNavarra)
    {
        this.indRentasNavarra = indRentasNavarra;
    }

    public void setIndRentasPaisVasco(Boolean indRentasPaisVasco)
    {
        this.indRentasPaisVasco = indRentasPaisVasco;
    }

    public void setCustodiaCompartida(Boolean custodiaCompartida)
    {
        this.custodiaCompartida = custodiaCompartida;
    }

    public void setEstudiaFuera(Boolean estudiaFuera)
    {
        this.estudiaFuera = estudiaFuera;
    }

    public void setFirmaSolicitud(Boolean firmaSolicitud)
    {
        this.firmaSolicitud = firmaSolicitud;
    }

    public static List<Miembro> getMiembrosByBeca(Long becaId)
    {
        return miembroDAO.getMiembrosByBeca(becaId);
    }

    public static Miembro getMiembroSolicitante(Long becaId)
    {
        return miembroDAO.getMiembroSolicitante(becaId);
    }

    @Autowired
    public void setMiembroDAO(MiembroDAO miembroDAO)
    {
        Miembro.miembroDAO = miembroDAO;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return this.apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public Date getFechaNacimiento()
    {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento)
    {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getLocalidadTrabajo()
    {
        return this.localidadTrabajo;
    }

    public void setLocalidadTrabajo(String localidadTrabajo)
    {
        this.localidadTrabajo = localidadTrabajo;
    }

    public Boolean isNoValidaIdentificacion()
    {
        return this.noValidaIdentificacion;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPadronLocalidadId()
    {
        return this.padronLocalidadId;
    }

    public void setPadronLocalidadId(Long padronLocalidadId)
    {
        this.padronLocalidadId = padronLocalidadId;
    }

    public Long getPadronProvinciaId()
    {
        return this.padronProvinciaId;
    }

    public void setPadronProvinciaId(Long padronProvinciaId)
    {
        this.padronProvinciaId = padronProvinciaId;
    }

    public EstadoCivil getEstadoCivil()
    {
        return this.estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil)
    {
        this.estadoCivil = estadoCivil;
    }

    public Profesion getProfesion()
    {
        return this.profesion;
    }

    public void setProfesion(Profesion profesion)
    {
        this.profesion = profesion;
    }

    public SituacionLaboral getSituacionLaboral()
    {
        return this.situacionLaboral;
    }

    public void setSituacionLaboral(SituacionLaboral situacionLaboral)
    {
        this.situacionLaboral = situacionLaboral;
    }

    public TipoIdentificacion getTipoIdentificacion()
    {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion)
    {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public TipoMinusvalia getTipoMinusvalia()
    {
        return this.tipoMinusvalia;
    }

    public void setTipoMinusvalia(TipoMinusvalia tipoMinusvalia)
    {
        this.tipoMinusvalia = tipoMinusvalia;
    }

    public TipoSexo getTipoSexo()
    {
        return this.tipoSexo;
    }

    public void setTipoSexo(TipoSexo tipoSexo)
    {
        this.tipoSexo = tipoSexo;
    }

    public Float getImporteRentasExtranjero()
    {
        return importeRentasExtranjero;
    }

    public void setImporteRentasExtranjero(Float importeRentasExtranjero)
    {
        this.importeRentasExtranjero = importeRentasExtranjero;
    }

    public Boolean isIndConvivePareja()
    {
        return indConvivePareja;
    }

    public Boolean isIndIngresosPropios()
    {
        return indIngresosPropios;
    }

    public Boolean isIndRevMinusMec()
    {
        return indRevMinusMec;
    }

    public Boolean isIndRevMinusUt()
    {
        return indRevMinusUt;
    }

    public void setIndConvivePareja(Boolean indConvivePareja)
    {
        this.indConvivePareja = indConvivePareja;
    }

    public void setIndIngresosPropios(Boolean indIngresosPropios)
    {
        this.indIngresosPropios = indIngresosPropios;
    }

    public void setIndRevMinusMec(Boolean indRevMinusMec)
    {
        this.indRevMinusMec = indRevMinusMec;
    }

    public void setIndRevMinusUt(Boolean indRevMinusUt)
    {
        this.indRevMinusUt = indRevMinusUt;
    }

    public Boolean isResidencia()
    {
        return residencia;
    }

    public Boolean isCustodia()
    {
        return custodia;
    }

    public Boolean isIndDeclRenta()
    {
        return indDeclRenta;
    }

    public Boolean isIndRentasEspanna()
    {
        return indRentasEspanna;
    }

    public Boolean isIndRentasNavarra()
    {
        return indRentasNavarra;
    }

    public Boolean isIndRentasPaisVasco()
    {
        return indRentasPaisVasco;
    }

    public Boolean isCustodiaCompartida()
    {
        return custodiaCompartida;
    }

    public Boolean isIndArrenUnidFamiliar()
    {
        return indArrenUnidFamiliar;
    }

    public void setIndArrenUnidFamiliar(Boolean indArrenUnidFamiliar)
    {
        this.indArrenUnidFamiliar = indArrenUnidFamiliar;
    }

    public String getNifArren()
    {
        return nifArren;
    }

    public void setNifArren(String nifArren)
    {
        this.nifArren = nifArren;
    }

    public Float getPrecioAlquiler()
    {
        return precioAlquiler;
    }

    public void setPrecioAlquiler(Float precioAlquiler)
    {
        this.precioAlquiler = precioAlquiler;
    }

    public Float getImporteActividadesEconomicas()
    {
        return importeActividadesEconomicas;
    }

    public void setImporteActividadesEconomicas(Float importeActividadesEconomicas)
    {
        this.importeActividadesEconomicas = importeActividadesEconomicas;
    }

    public Float getPorcentajeActividadesEconomicas()
    {
        return porcentajeActividadesEconomicas;
    }

    public void setPorcentajeActividadesEconomicas(Float porcentajeActividadesEconomicas)
    {
        this.porcentajeActividadesEconomicas = porcentajeActividadesEconomicas;
    }

    public Boolean getIndHijosMayores25() {
        return indHijosMayores25;
    }

    public Boolean isIndHijosMayores25() {
        return indHijosMayores25;
    }

    public void setIndHijosMayores25(Boolean indHijosMayores25) {
        this.indHijosMayores25 = indHijosMayores25;
    }

    public Boolean getIndPensionCompensatoria() {
        return indPensionCompensatoria;
    }

    public Boolean isIndPensionCompensatoria() {
        return indPensionCompensatoria;
    }

    public void setIndPensionCompensatoria(Boolean indPensionCompensatoria) {
        this.indPensionCompensatoria = indPensionCompensatoria;
    }

    public void update()
    {
        miembroDAO.updateMiembro(this);
    }

    @Transactional
    public Miembro insert()
    {
        return miembroDAO.insert(this);
    }

    public void importarDatosMinisterio(FamiliarType familiarType)
    {
        setNombre(familiarType.getFamiNombre());
        setApellido1(familiarType.getFamiApe1());
        setApellido2(familiarType.getFamiApe2());
        setFechaNacimiento(familiarType.getFamiFechaNac().toGregorianCalendar().getTime());
        setIdentificacion(familiarType.getFamiNiffam());
        setLocalidadTrabajo(familiarType.getFamiLocalidad());
        setNifArren(familiarType.getFamiNifArren());

        TipoSustentador sustentador = new TipoSustentador();
        if (familiarType.getFamiCodSus() == null)
        {
            sustentador.setId(3L);
        }
        else
        {
            sustentador.setId(Long.parseLong(familiarType.getFamiCodSus()));
        }
        setTipoSustentador(sustentador);

        setPorcentajeActividadesEconomicas(familiarType.getFamiPorcPartActivEcon());
        setImporteActividadesEconomicas(familiarType.getFamiImpPartActivEcon());
        setImporteRentasExtranjero(familiarType.getFamiRentasExtranj());
        setPrecioAlquiler(familiarType.getFamiPrecioAlquiler());

        if (familiarType.getFamiFechaCadNif() != null)
        {
            setFechaCadNif(familiarType.getFamiFechaCadNif().toGregorianCalendar().getTime());
        }

        setIdentificacionIdesp(familiarType.getFamiCodNsoporte());

        if (familiarType.getFamiFechaResMinus() != null)
        {
            setFechaResMinus(familiarType.getFamiFechaResMinus().toGregorianCalendar().getTime());
        }

        if (familiarType.getFamiFechaFinMinus() != null)
        {
            setFechaFinMinus(familiarType.getFamiFechaFinMinus().toGregorianCalendar().getTime());
        }

        if (familiarType.getFamiIndEstFuera() != null)
        {
            setEstudiaFuera(familiarType.getFamiIndEstFuera().equals("S"));
        }

        if (familiarType.getFamiIndResidencia() != null)
        {
            setResidencia(familiarType.getFamiIndResidencia().equals("S"));
        }

        if (familiarType.getFamiIndCustodia() != null)
        {
            setCustodia(familiarType.getFamiIndCustodia().equals("S"));
        }

        if (familiarType.getFamiIndDeclRenta() != null)
        {
            setIndDeclRenta(familiarType.getFamiIndDeclRenta().equals("S"));
        }

        if (familiarType.getFamiIndRentasEspanna() != null)
        {
            setIndRentasEspanna(familiarType.getFamiIndRentasEspanna().equals("S"));
        }

        if (familiarType.getFamiIndRentasNavarra() != null)
        {
            setIndRentasNavarra(familiarType.getFamiIndRentasNavarra().equals("S"));
        }

        if (familiarType.getFamiIndRentasPaisVasco() != null)
        {
            setIndRentasPaisVasco(familiarType.getFamiIndRentasPaisVasco().equals("S"));
        }

        if (familiarType.getFamiIndCustodiaCompart() != null)
        {
            setCustodiaCompartida(familiarType.getFamiIndCustodiaCompart().equals("S"));
        }

        if (familiarType.getFamiIndValidCiud() != null)
        {
            setFirmaSolicitud(familiarType.getFamiIndValidCiud().equals("S"));
        }

        if (familiarType.getFamiIndConvivePareja() != null)
        {
            setIndConvivePareja(familiarType.getFamiIndConvivePareja().equals("S"));
        }

        if (familiarType.getFamiIndIngresosPropios() != null)
        {
            setIndIngresosPropios(familiarType.getFamiIndIngresosPropios().equals("S"));
        }

        if (familiarType.getFamiIndArrenUnidFamiliar() != null)
        {
            setIndArrenUnidFamiliar(familiarType.getFamiIndArrenUnidFamiliar().equals("S"));
        }

        if (familiarType.getFamiIndPensionCompensatoria() != null)
        {
            setIndPensionCompensatoria(familiarType.getFamiIndPensionCompensatoria().equals("S"));
        }

        if (familiarType.getFamiIndConviveHijos25() != null)
        {
            setIndHijosMayores25(familiarType.getFamiIndConviveHijos25().equals("S"));
        }

        setCodigoUnivHermano(familiarType.getFamiCodUniv());
        setCodigoEstudioHermano(familiarType.getFamiCodEstUniv());

        establecePaisPorDefecto();

        importarTipoMoneda(familiarType);
        importarTipoIdentificacion(familiarType);
        importarProfesion(familiarType);
        importarEstadoCivil(familiarType);
        importarSituacionLaboral(familiarType);
        importarTipoMiembro(familiarType);
        importarTipoMinusvalia(familiarType);
        importarCcaaMinus(familiarType);
    }

    private void importarTipoMoneda(FamiliarType familiarType)
    {
        String famiMoneda = familiarType.getFamiMoneda();
        if (famiMoneda != null)
        {
            TipoMoneda tipoMoneda = TipoMoneda.getTipoMonedaByCodigo(famiMoneda);
            setTipoMoneda(tipoMoneda);
        }
    }

    private void importarCcaaMinus(FamiliarType familiarType)
    {
        if (familiarType.getFamiCcaaMinus() != null)
        {
            Comunidad comunidad = Comunidad.getComunidadByCodigo(familiarType.getFamiCcaaMinus());

            if (comunidad != null)
            {
                setCcaaMinus(comunidad);
            }
        }
        else
        {
            setCcaaMinus(null);
        }
    }

    private void importarTipoIdentificacion(FamiliarType familiarType)
    {
        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
        Long famiCodDocuIdent = familiarType.getFamiCodDocuIdent();
        if (famiCodDocuIdent != null)
        {
            tipoIdentificacion.setId(famiCodDocuIdent);
            setTipoIdentificacion(tipoIdentificacion);
        }
    }

    private void establecePaisPorDefecto()
    {
        Pais pais = new Pais();
        pais.setId("E");
        setPais(pais);
    }

    private void importarProfesion(FamiliarType familiarType)
    {
        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();
        String profesionMinisterio = familiarType.getFamiProfesion();

        if (profesionMinisterio != null)
        {
            Long profesionUjiId = Diccionario.getValor(cursoAcademico.getId(), CLASE_PROFESION,
                    ORGANISMO_MINISTERIO, profesionMinisterio);
            if (profesionUjiId != null)
            {
                Profesion profesionUji = new Profesion();
                profesionUji.setId(profesionUjiId);
                setProfesion(profesionUji);
            }
        }
    }

    private void importarEstadoCivil(FamiliarType familiarType)
    {
        String estadoCivilMinisterio = familiarType.getFamiCestcivil();
        if (estadoCivilMinisterio != null)
        {
            MapeadorEstadoCivil mapeadorEstadoCivil = new MapeadorEstadoCivil();
            Long estadoCivilIdUji = mapeadorEstadoCivil.getValorUJI(estadoCivilMinisterio,
                    CodigoOrganismo.MINISTERIO);
            if (estadoCivilIdUji != null)
            {
                EstadoCivil estadoCivilUji = new EstadoCivil();
                estadoCivilUji.setId(estadoCivilIdUji);
                setEstadoCivil(estadoCivilUji);
            }
        }
    }

    private void importarSituacionLaboral(FamiliarType familiarType)
    {
        String situacionLaboralMinisterio = familiarType.getFamiSitLaboral();
        if (situacionLaboralMinisterio != null)
        {
            MapeadorSituacionLaboral mapeadorSituacionLaboral = new MapeadorSituacionLaboral();
            Long situacionLaboralIdUji = mapeadorSituacionLaboral.getValorUJI(
                    situacionLaboralMinisterio, CodigoOrganismo.MINISTERIO);

            SituacionLaboral situacionLaboral = new SituacionLaboral();
            situacionLaboral.setId(situacionLaboralIdUji);
            setSituacionLaboral(situacionLaboral);
        }
    }

    private void importarTipoMiembro(FamiliarType familiarType)
    {
        TipoMiembro tipoMiembro = new TipoMiembro();
        tipoMiembro.setId(familiarType.getFamiCodParentesco());
        setTipoMiembro(tipoMiembro);
    }

    private void importarTipoMinusvalia(FamiliarType familiarType)
    {
        String codigoMinusvalia = familiarType.getFamiCodGrMinus();
        if (codigoMinusvalia == null)
        {
            codigoMinusvalia = NO_MINUSVALIA;
        }

        TipoMinusvalia tipoMinusvalia = TipoMinusvalia.getTipoMinusvaliaByOrden(codigoMinusvalia);
        setTipoMinusvalia(tipoMinusvalia);
    }

    public void importarActividadesEconomicas(FamiliarType familiarType)
    {
        List<ActivEconomicasType> listActivEconomicasType = familiarType.getActivEconomicas();
        Set<ActividadEconomica> listaActividadesEconomicas = new HashSet<ActividadEconomica>();
        this.setActividadesEconomicas(listaActividadesEconomicas);

        for (ActivEconomicasType activEconomicasType : listActivEconomicasType)
        {
            ActividadEconomica actividadEconomica = preparaActividadEconomica(activEconomicasType);
            actividadEconomica.insert();
            this.getActividadesEconomicas().add(actividadEconomica);
        }
    }

    private ActividadEconomica preparaActividadEconomica(ActivEconomicasType activEconomicasType)
    {
        ActividadEconomica actividadEconomica = new ActividadEconomica();
        actividadEconomica.setCifSociedad(activEconomicasType.getAcecCIFActivEcon());
        actividadEconomica.setPorcParticipacion(activEconomicasType.getAcecPorcPartActivEcon());
        actividadEconomica.setImpParticipacion(activEconomicasType.getAcecImpPartActivEcon());
        actividadEconomica.setMiembro(this);
        return actividadEconomica;
    }

    public Set<ActividadEconomica> getActividadesEconomicas()
    {
        return actividadesEconomicas;
    }

    public void setActividadesEconomicas(Set<ActividadEconomica> actividadesEconomicas)
    {
        this.actividadesEconomicas = actividadesEconomicas;
    }

    public String getIdentificacionIdesp()
    {
        return identificacionIdesp;
    }

    public void setIdentificacionIdesp(String identificacionIdesp)
    {
        this.identificacionIdesp = identificacionIdesp;
    }

    public Date getFechaCadNif()
    {
        return fechaCadNif;
    }

    public void setFechaCadNif(Date fechaCadNif)
    {
        this.fechaCadNif = fechaCadNif;
    }

    public Date getFechaResMinus()
    {
        return fechaResMinus;
    }

    public void setFechaResMinus(Date fechaResMinus)
    {
        this.fechaResMinus = fechaResMinus;
    }

    public Date getFechaFinMinus()
    {
        return fechaFinMinus;
    }

    public void setFechaFinMinus(Date fechaFinMinus)
    {
        this.fechaFinMinus = fechaFinMinus;
    }

    public String getCodigoEstudioHermano()
    {
        return codigoEstudioHermano;
    }

    public void setCodigoEstudioHermano(String codigoEstudioHermano)
    {
        this.codigoEstudioHermano = codigoEstudioHermano;
    }

    public String getCodigoUnivHermano()
    {
        return codigoUnivHermano;
    }

    public void setCodigoUnivHermano(String codigoUnivHermano)
    {
        this.codigoUnivHermano = codigoUnivHermano;
    }

    public Boolean isEstudiaFuera()
    {
        return estudiaFuera;
    }

    public Boolean isFirmaSolicitud()
    {
        return firmaSolicitud;
    }

    public Comunidad getCcaaMinus()
    {
        return ccaaMinus;
    }

    public void setCcaaMinus(Comunidad ccaaMinus)
    {
        this.ccaaMinus = ccaaMinus;
    }

    public TipoMoneda getTipoMoneda()
    {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda)
    {
        this.tipoMoneda = tipoMoneda;
    }

    public boolean esSustentador()
    {
        return getTipoSustentador().getId().equals(TIPO_SUSTENTADOR_PRINCIPAL);
    }

    public TipoSustentador getTipoSustentador()
    {
        return this.tipoSustentador;
    }

    public void setTipoSustentador(TipoSustentador tipoSustentador)
    {
        this.tipoSustentador = tipoSustentador;
    }

    public Boolean esExtranjero()
    {
        return !this.getPais().getId().equals("E");
    }

    public Pais getPais()
    {
        return this.pais;
    }

    public void setPais(Pais pais)
    {
        this.pais = pais;
    }

    public boolean esSustentadorOConyuge()
    {
        return getTipoSustentador().getId().equals(TIPO_SUSTENTADOR_PRINCIPAL)
                || getTipoSustentador().getId().equals(TIPO_SUSTENTADOR_CONYUGE);
    }

    public boolean esSolicitante()
    {
        return getTipoMiembro().getId().equals(TIPO_MIEMBRO_SOLICITANTE);
    }

    public TipoMiembro getTipoMiembro()
    {
        return this.tipoMiembro;
    }

    public void setTipoMiembro(TipoMiembro tipoMiembro)
    {
        this.tipoMiembro = tipoMiembro;
    }

    @Transactional
    public void delete()
    {
        miembroDAO.delete(Miembro.class, getId());
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void borraActividadesEconomicas()
    {
        List<ActividadEconomica> listaActividadesEconomicas = ActividadEconomica
                .getActividadesEconomicasByMiembroId(getId());
        for (ActividadEconomica actividadEconomica : listaActividadesEconomicas)
        {
            actividadEconomica.deleteActividadEconomica();
        }
        this.setActividadesEconomicas(null);
    }


    public Beca getBeca()
    {
        return beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }
}
