package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoSustentador;
import es.uji.commons.db.BaseDAO;

public interface TipoSustentadorDAO extends BaseDAO
{
    List<TipoSustentador> getTiposSustentadores();

    List<TipoSustentador> getTipoSustentadorById(Long tipoSustentadorId);
}