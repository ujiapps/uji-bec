package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Pais;
import es.uji.commons.db.BaseDAO;

public interface PaisDAO extends BaseDAO
{
    List<Pais> getPaises();
}
