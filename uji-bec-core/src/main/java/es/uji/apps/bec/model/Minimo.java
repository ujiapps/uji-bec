package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.MinimoDAO;

/**
 * The persistent class for the BC2_MINIMOS database table.
 *
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_MINIMOS")
public class Minimo implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CREDITOS_BECA_PARCIAL")
    private Float creditosBecaParcial;

    @Column(name = "CREDITOS_BECA_COMPLETA")
    private Float creditosBecaCompleta;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    @Column(name = "NOTA_ACCESO_MEC")
    private Float notaAccesoMec;

    @Column(name = "PORC_SUPERADOS_MEC")
    private Float porcSuperadosMec;

    @Column(name = "COEF_CORRECTOR_MEC")
    private Float coefCorrectorMec;

    @Column(name = "PORC_RENDIMIENTO_MEC")
    private Float porcRendimientoMec;

    @Column(name = "NOTA_RENDIMIENTO_MEC_1")
    private Float notaRendimientoMec1;

    @Column(name = "NOTA_RENDIMIENTO_MEC_2")
    private Float notaRendimientoMec2;

    @Column(name = "NOTA_ACCESO_CONS")
    private Float notaAccesoCons;

    @Column(name = "NOTA_SEGUNDOS_CONS")
    private Float notaSegundosCons;

    @Column(name = "PORC_SUPERADOS_CONS")
    private Float porcSuperadosCons;

    @Column(name = "COEF_CORRECTOR_CONS")
    private Float coefCorrectorCons;

    @Column(name = "COEF_CORRECTOR_CONS_MANUELA")
    private Float coefCorrectorConsManuela;

    // bi-directional many-to-one association to CursoAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;

    // bi-directional many-to-one association to Estudio
    @ManyToOne
    private Estudio estudio;

    private static MinimoDAO minimoDAO;

    @Autowired
    public void setMinimoDAO(MinimoDAO minimoDAO)
    {
        Minimo.minimoDAO = minimoDAO;
    }

    public Minimo()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Float getCreditosBecaCompleta()
    {
        return this.creditosBecaCompleta;
    }

    public void setCreditosBecaCompleta(Float creditosBecaCompleta)
    {
        this.creditosBecaCompleta = creditosBecaCompleta;
    }

    public Float getCreditosBecaParcial()
    {
        return this.creditosBecaParcial;
    }

    public void setCreditosBecaParcial(Float creditosBecaParcial)
    {
        this.creditosBecaParcial = creditosBecaParcial;
    }

    public String getTipoEstudio()
    {
        return this.tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Float getNotaAccesoMec()
    {
        return notaAccesoMec;
    }

    public void setNotaAccesoMec(Float notaAccesoMec)
    {
        this.notaAccesoMec = notaAccesoMec;
    }

    public Float getPorcSuperadosMec()
    {
        return porcSuperadosMec;
    }

    public void setPorcSuperadosMec(Float porcSuperadosMec)
    {
        this.porcSuperadosMec = porcSuperadosMec;
    }

    public Float getCoefCorrectorMec()
    {
        return coefCorrectorMec;
    }

    public void setCoefCorrectorMec(Float coefCorrectorMec)
    {
        this.coefCorrectorMec = coefCorrectorMec;
    }

    public Float getPorcRendimientoMec()
    {
        return porcRendimientoMec;
    }

    public void setPorcRendimientoMec(Float porcRendimientoMec)
    {
        this.porcRendimientoMec = porcRendimientoMec;
    }

    public Float getNotaRendimientoMec1()
    {
        return notaRendimientoMec1;
    }

    public void setNotaRendimientoMec1(Float notaRendimientoMec1)
    {
        this.notaRendimientoMec1 = notaRendimientoMec1;
    }

    public Float getNotaRendimientoMec2()
    {
        return notaRendimientoMec2;
    }

    public void setNotaRendimientoMec2(Float notaRendimientoMec2)
    {
        this.notaRendimientoMec2 = notaRendimientoMec2;
    }

    public Float getNotaAccesoCons()
    {
        return notaAccesoCons;
    }

    public void setNotaAccesoCons(Float notaAccesoCons)
    {
        this.notaAccesoCons = notaAccesoCons;
    }

    public Float getNotaSegundosCons()
    {
        return notaSegundosCons;
    }

    public void setNotaSegundosCons(Float notaSegundosCons)
    {
        this.notaSegundosCons = notaSegundosCons;
    }

    public Float getPorcSuperadosCons()
    {
        return porcSuperadosCons;
    }

    public void setPorcSuperadosCons(Float porcSuperadosCons)
    {
        this.porcSuperadosCons = porcSuperadosCons;
    }

    public Float getCoefCorrectorCons()
    {
        return coefCorrectorCons;
    }

    public void setCoefCorrectorCons(Float coefCorrectorCons)
    {
        this.coefCorrectorCons = coefCorrectorCons;
    }

    public Float getCoefCorrectorConsManuela() {
        return coefCorrectorConsManuela;
    }

    public void setCoefCorrectorConsManuela(Float coefCorrectorConsManuela) {
        this.coefCorrectorConsManuela = coefCorrectorConsManuela;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Estudio getEstudio()
    {
        return this.estudio;
    }

    public void setEstudio(Estudio estudio)
    {
        this.estudio = estudio;
    }

    public static Minimo getMinimosByEstudioId(Long estudioId, Long cursoAcademico)
    {
        List<Minimo> lista = minimoDAO.getMinimosByEstudioId(estudioId, cursoAcademico);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }

        return null;
    }
}
