package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTodasConselleria;
import es.uji.apps.bec.model.TodasConselleria;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TodasConselleriaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        TodasConselleriaDAO
{

    @Override
    public List<TodasConselleria> getDatosByCursoAcademicoId(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTodasConselleria qTodasConselleria = QTodasConselleria.todasConselleria;

        query.from(qTodasConselleria)
                .where(qTodasConselleria.cursoAcademicoId.eq(cursoAcademicoId)
                        .and(qTodasConselleria.estadoId.eq(CodigoEstado.DENEGADA.getId()).or(qTodasConselleria.estadoId.eq(CodigoEstado.CONCEDIDA.getId()).or(qTodasConselleria.estadoId.eq(CodigoEstado.DENEGADA_ACADEMICOS.getId())))));

        return query.list(qTodasConselleria);
    }

    @Override
    public List<TodasConselleria> getDatosConcedidasByCursoAcademicoId(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTodasConselleria qTodasConselleria = QTodasConselleria.todasConselleria;

        query.from(qTodasConselleria)
                .where(qTodasConselleria.cursoAcademicoId.eq(cursoAcademicoId)
                        .and(qTodasConselleria.estadoId.eq(CodigoEstado.CONCEDIDA.getId())))
                .orderBy(qTodasConselleria.apellido1.asc(), qTodasConselleria.apellido2.asc(), qTodasConselleria.nombre.asc());

        return query.list(qTodasConselleria);
    }

    @Override
    public List<TodasConselleria> getDatosDenegadasByCursoAcademicoId(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTodasConselleria qTodasConselleria = QTodasConselleria.todasConselleria;

        query.from(qTodasConselleria)
                .where(qTodasConselleria.cursoAcademicoId.eq(cursoAcademicoId)
                        .and(qTodasConselleria.estadoId.eq(CodigoEstado.DENEGADA.getId()).or(qTodasConselleria.estadoId.eq(CodigoEstado.DENEGADA_ACADEMICOS.getId()))))
                .orderBy(qTodasConselleria.apellido1.asc(), qTodasConselleria.apellido2.asc(), qTodasConselleria.nombre.asc());

        return query.list(qTodasConselleria);
    }

    @Override
    public boolean isMec2Crd(Long becaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTodasConselleria qTodasConselleria = QTodasConselleria.todasConselleria;

        List<TodasConselleria> result = query.from(qTodasConselleria).
                where(qTodasConselleria.becaMecConcedida.eq(true)
                        .and(qTodasConselleria.cursoAcademicoId.eq(cursoAcademicoId))
                        .and(qTodasConselleria.creditos2a.gt(0))
                        .and(qTodasConselleria.becaId.eq(becaId))).list(qTodasConselleria);

        return result.size() > 0;
    }
}