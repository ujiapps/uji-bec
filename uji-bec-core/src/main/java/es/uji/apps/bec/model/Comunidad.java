package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ComunidadDAO;

/**
 * The persistent class for the BC2_CCAA database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_CCAA")
public class Comunidad implements Serializable
{
    private static ComunidadDAO comunidadDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String codigo;
    private String nombre;
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "ccaaMinus")
    private Set<Miembro> miembros;

    public Comunidad()
    {
    }

    public static Comunidad getComunidadById(Long comunidadId)
    {
        List<Comunidad> comunidad = comunidadDAO.get(Comunidad.class, comunidadId);

        if (comunidad.size() > 0)
        {
            return comunidad.get(0);
        }
        return null;
    }

    public static Comunidad getComunidadByCodigo(String codigo)
    {
        List<Comunidad> comunidad = comunidadDAO.getComunidadByCodigo(codigo);

        if (comunidad.size() > 0)
        {
            return comunidad.get(0);
        }

        return null;
    }

    @Autowired
    public void setComunidadDAO(ComunidadDAO comunidadDAO)
    {
        Comunidad.comunidadDAO = comunidadDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }
}