package es.uji.apps.bec.model.denegaciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@IdClass(BecWsSoliDenegadasCodigoId.class)
@Table(name = "BEC_WS_SOLI_DENEGADAS_CODIGOS")
public class BecWsSoliDenegadasCodigo implements Serializable
{
    @Id
    private String escacodcausa;

    @Id
    private String escacodsubcausa;

    @Id
    private BigDecimal essocodestado;

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date essofecha;

    @Id
    private BigDecimal essosecuencestado;

    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private BecWsSoliDenegada becWsSoliDenegada;

    public BecWsSoliDenegadasCodigo()
    {
    }

    public String getEscacodcausa()
    {
        return this.escacodcausa;
    }

    public void setEscacodcausa(String escacodcausa)
    {
        this.escacodcausa = escacodcausa;
    }

    public String getEscacodsubcausa()
    {
        return this.escacodsubcausa;
    }

    public void setEscacodsubcausa(String escacodsubcausa)
    {
        this.escacodsubcausa = escacodsubcausa;
    }

    public BigDecimal getEssocodestado()
    {
        return this.essocodestado;
    }

    public void setEssocodestado(BigDecimal essocodestado)
    {
        this.essocodestado = essocodestado;
    }

    public Date getEssofecha()
    {
        return this.essofecha;
    }

    public void setEssofecha(Date essofecha)
    {
        this.essofecha = essofecha;
    }

    public BigDecimal getEssosecuencestado()
    {
        return this.essosecuencestado;
    }

    public void setEssosecuencestado(BigDecimal essosecuencestado)
    {
        this.essosecuencestado = essosecuencestado;
    }

    public BecWsSoliDenegada getBecWsSoliDenegada()
    {
        return this.becWsSoliDenegada;
    }

    public void setBecWsSoliDenegada(BecWsSoliDenegada becWsSoliDenegada)
    {
        this.becWsSoliDenegada = becWsSoliDenegada;
    }
}