package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class WsNoSeEncuentraPdfErroresMecException extends CoreDataBaseException
{
    public WsNoSeEncuentraPdfErroresMecException()
    {
        super("No es troba fitxer d\'errors d\'aquesta tanda.");
    }

    public WsNoSeEncuentraPdfErroresMecException(String message)
    {
        super(message);
    }
}