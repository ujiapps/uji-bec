package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Provincia;
import es.uji.commons.db.BaseDAO;

public interface ProvinciaDAO extends BaseDAO
{
    List<Provincia> getProvincias();
}
