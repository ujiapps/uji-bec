package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.PersonaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_EXT_PERSONAS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_EXT_PERSONAS")
public class Persona implements Serializable
{
    @Id
    private Long id;

    private String identificacion;

    private String nombre;

    @Column(name = "PERSONA_NOMBRE")
    private String personaNombre;

    @Column(name = "PERSONA_APELLIDO1")
    private String personaApellido1;

    @Column(name = "PERSONA_APELLIDO2")
    private String personaApellido2;

    @Column(name = "CORREO_UJI")
    private String correoUJI;

    // bi-directional many-to-one association to PersonaEstudio
    @OneToMany(mappedBy = "persona")
    private Set<PersonaEstudio> personasEstudios;

    // bi-directional many-to-one association to Solicitante
    @OneToMany(mappedBy = "persona")
    private Set<Solicitante> solicitantes;

    // bi-directional many-to-one association to RegistroEnvio
    @OneToMany(mappedBy = "persona")
    private Set<RegistroEnvio> registroEnvios;

    private static PersonaDAO personaDAO;

    @Autowired
    public void setPersonaDAO(PersonaDAO personaDAO)
    {
        Persona.personaDAO = personaDAO;
    }

    public Persona()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getPersonaNombre() {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre) {
        this.personaNombre = personaNombre;
    }

    public String getPersonaApellido1() {
        return personaApellido1;
    }

    public void setPersonaApellido1(String personaApellido1) {
        this.personaApellido1 = personaApellido1;
    }

    public String getPersonaApellido2() {
        return personaApellido2;
    }

    public void setPersonaApellido2(String personaApellido2) {
        this.personaApellido2 = personaApellido2;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCorreoUJI() {
        return correoUJI;
    }

    public void setCorreoUJI(String correoUJI) {
        this.correoUJI = correoUJI;
    }

    public Set<PersonaEstudio> getPersonasEstudios()
    {
        return this.personasEstudios;
    }

    public void setPersonasEstudios(Set<PersonaEstudio> personasEstudios)
    {
        this.personasEstudios = personasEstudios;
    }

    public Set<Solicitante> getSolicitantes()
    {
        return this.solicitantes;
    }

    public void setSolicitantes(Set<Solicitante> solicitantes)
    {
        this.solicitantes = solicitantes;
    }

    public Set<RegistroEnvio> getRegistroEnvios()
    {
        return this.registroEnvios;
    }

    public void setRegistroEnvios(Set<RegistroEnvio> registroEnvios)
    {
        this.registroEnvios = registroEnvios;
    }

    public static Persona getPersonaByDNI(String dni)
    {
        return personaDAO.getPersonaByDNI(dni);
    }

    public static Persona preparaPersonaConId(Long personaId)
    {
        Persona persona = new Persona();
        persona.setId(personaId);
        return persona;
    }

    public static Persona getPersonaByPersonaId(Long personaId)
    {
        return personaDAO.getPersonaByPersonaId(personaId);
    }
}