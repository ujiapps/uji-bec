package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.SituacionLaboralDAO;

/**
 * The persistent class for the BC2_SITUACIONES_LABORALES database table.
 */
@Entity
@Component
@Table(name = "BC2_SITUACIONES_LABORALES")
@SuppressWarnings("serial")
public class SituacionLaboral implements Serializable
{
    private static SituacionLaboralDAO situacionLaboralDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private Long orden;
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "situacionLaboral")
    private Set<Miembro> miembros;

    public SituacionLaboral()
    {
    }

    public static SituacionLaboral getSituacionLaboralById(Long situacionLaboralId)
    {
        return situacionLaboralDAO.getSituacionLaboralById(situacionLaboralId).get(0);
    }

    @Autowired
    public void setSituacionLaboralDAO(SituacionLaboralDAO situacionLaboralDAO)
    {
        SituacionLaboral.situacionLaboralDAO = situacionLaboralDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }
}