package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ProcesoDAO;

/**
 * The persistent class for the BC2_PROCESOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_PROCESOS")
public class Proceso implements Serializable
{
    private static ProcesoDAO procesoDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private Long orden;
    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "proceso")
    private Set<Beca> becas;

    public Proceso()
    {
    }

    public static Proceso getProcesoById(Long procesoId)
    {
        return procesoDAO.get(Proceso.class, procesoId).get(0);
    }

    @Autowired
    public void setProcesoDAO(ProcesoDAO procesoDAO)
    {
        Proceso.procesoDAO = procesoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }
}