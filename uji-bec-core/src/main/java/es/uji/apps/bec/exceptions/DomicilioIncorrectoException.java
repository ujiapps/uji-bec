package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class DomicilioIncorrectoException extends CoreBaseException
{

    public DomicilioIncorrectoException()
    {
        super("El domicili és incorrecte");
    }

    public DomicilioIncorrectoException(String message)
    {
        super(message);
    }
}
