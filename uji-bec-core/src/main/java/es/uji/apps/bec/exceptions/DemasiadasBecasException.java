package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class DemasiadasBecasException extends GeneralBECException
{

    public DemasiadasBecasException()
    {
        super("Nombre excessiu de beques. Defineix mes filtres");
    }

    public DemasiadasBecasException(String message)
    {
        super(message);
    }
}
