package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.DescargaSolicitudesLogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the BC2_DESCARGA_SOLICITUDES_LOG database table.
 */

@Component
@Entity
@Table(name = "BC2_DESCARGA_SOLICITUDES_LOG")
public class DescargaSolicitudesLog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "ARCHIVO_TEMPORAL")
    private Long archivoTemporal;

    private String nif;

    private String estado;

    private static DescargaSolicitudesLogDAO descargaSolicitudesLogDAO;

    @Autowired
    public void setDescargaSolicitudesLogDAO(DescargaSolicitudesLogDAO descargaSolicitudesLogDAO) {
        DescargaSolicitudesLog.descargaSolicitudesLogDAO = descargaSolicitudesLogDAO;
    }

    public DescargaSolicitudesLog() {

    }

    public DescargaSolicitudesLog(Long archivoTemporal, String nif, String estado) {
        this.archivoTemporal = archivoTemporal;
        this.nif = nif;
        this.estado = estado;
        this.fecha = new Date(System.currentTimeMillis());
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getArchivoTemporal() {
        return archivoTemporal;
    }

    public void setArchivoTemporal(Long archivoTemporal) {
        this.archivoTemporal = archivoTemporal;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void insert() {
        descargaSolicitudesLogDAO.insert(this);
    }
}