package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.QCuantia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CuantiaDAODatabaseImpl extends BaseDAODatabaseImpl implements CuantiaDAO
{
    private QCuantia cuantia = QCuantia.cuantia;

    @Override
    public List<Cuantia> getCuantias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cuantia).orderBy(cuantia.convocatoria.nombre.asc(), cuantia.codigo.asc());
        return query.list(cuantia);
    }

    @Override
    public List<Cuantia> getCuantiasActivas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cuantia).where(cuantia.activa.isTrue()).orderBy(cuantia.nombre.asc());
        return query.list(cuantia);
    }

    @Override
    public Cuantia getCuantiaById(Long cuantiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cuantia).where(cuantia.id.eq(cuantiaId));
        return query.uniqueResult(cuantia);
    }

    @Override
    @Transactional
    public void deleteCuantia(Long cuantiaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, cuantia);
        delete.where(cuantia.id.eq(cuantiaId)).execute();        
    }
}
