package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class TandaConBecasAsignadasException extends CoreDataBaseException
{
    public TandaConBecasAsignadasException()
    {
        super("La tanda no es pot esborrar. Està assignada a alguna beca.");
    }

    public TandaConBecasAsignadasException(String message)
    {
        super(message);
    }
}