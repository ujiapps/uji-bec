package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoFamiliaDAO;

/**
 * The persistent class for the BC2_TIPOS_FAMILIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TIPOS_FAMILIAS")
public class TipoFamilia implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    private Long orden;

    // bi-directional many-to-one association to Solicitante
    @OneToMany(mappedBy = "tipoFamilia", cascade = CascadeType.ALL)
    private Set<Solicitante> solicitantes;

    // bi-directional many-to-one association to PersonasEstudio
    @OneToMany(mappedBy = "tipoFamilia")
    private Set<PersonaEstudio> personasEstudios;

    private static TipoFamiliaDAO tipoFamiliaDAO;

    @Autowired
    public void setTipoFamiliaDAO(TipoFamiliaDAO tipoFamiliaDAO)
    {
        TipoFamilia.tipoFamiliaDAO = tipoFamiliaDAO;
    }

    public TipoFamilia()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Solicitante> getSolicitantes()
    {
        return this.solicitantes;
    }

    public void setSolicitantes(Set<Solicitante> solicitantes)
    {
        this.solicitantes = solicitantes;
    }

    public Set<PersonaEstudio> getPersonasEstudios()
    {
        return personasEstudios;
    }

    public void setPersonasEstudios(Set<PersonaEstudio> personasEstudios)
    {
        this.personasEstudios = personasEstudios;
    }
}