package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoPropiedad;
import es.uji.apps.bec.model.TipoPropiedad;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoPropiedadDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoPropiedadDAO
{
    private QTipoPropiedad tipoPropiedad = QTipoPropiedad.tipoPropiedad;

    @Override
    public List<TipoPropiedad> getTiposPropiedades()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoPropiedad).orderBy(tipoPropiedad.orden.asc());
        return query.list(tipoPropiedad);
    }

    @Override
    public TipoPropiedad getTipoPropiedadById(Long tipoPropiedadId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoPropiedad).where(tipoPropiedad.id.eq(tipoPropiedadId));
        return query.uniqueResult(tipoPropiedad);
    }

    @Override
    @Transactional
    public void deleteTipoPropiedad(Long tipoPropiedadId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoPropiedad);
        delete.where(tipoPropiedad.id.eq(tipoPropiedadId)).execute();
    }
}
