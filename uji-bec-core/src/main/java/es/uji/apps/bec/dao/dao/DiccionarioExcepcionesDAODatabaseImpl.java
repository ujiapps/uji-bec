package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.DiccionarioExcepciones;
import es.uji.apps.bec.model.QDiccionarioExcepciones;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DiccionarioExcepcionesDAODatabaseImpl extends BaseDAODatabaseImpl implements DiccionarioExcepcionesDAO
{
    @Override
    public Long getValor(Long cursoAcademicoId, String claseUji, Long organismoId, String valorOrigen)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDiccionarioExcepciones diccionarioExcepciones = QDiccionarioExcepciones.diccionarioExcepciones;

        if (cursoAcademicoId == null || organismoId == null || valorOrigen == null)
        {
          return null;
        }

        query.from(diccionarioExcepciones).where(
                diccionarioExcepciones.claseUji.eq(claseUji)
                        .and(diccionarioExcepciones.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(diccionarioExcepciones.organismo.id.eq(organismoId))
                        .and(diccionarioExcepciones.valorOrigen.eq(valorOrigen)));

        List<DiccionarioExcepciones> listaValores = query.list(diccionarioExcepciones);
        if (listaValores.size() > 0)
        {
            return listaValores.get(0).getValorUji();
        }
        return null;
    }

    @Override
    public String getValorOrigen(Long cursoAcademicoId, String claseUji, Long organismoId,
            String valorUji)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDiccionarioExcepciones diccionarioExcepciones = QDiccionarioExcepciones.diccionarioExcepciones;

        query.from(diccionarioExcepciones).where(
                diccionarioExcepciones.claseUji.eq(claseUji)
                        .and(diccionarioExcepciones.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(diccionarioExcepciones.organismo.id.eq(organismoId))
                        .and(diccionarioExcepciones.valorUji.eq(Long.parseLong(valorUji))));

        List<DiccionarioExcepciones> listaValores = query.list(diccionarioExcepciones);
        if (listaValores.size() > 0)
        {
            return listaValores.get(0).getValorOrigen();
        }
        return "";
    }
}
