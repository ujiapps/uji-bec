package es.uji.apps.bec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TextoDAO;

/**
 * The persistent class for the BC2_TEXTOS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TEXTOS")
public class Texto implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CABECERA_CA")
    private String cabeceraCa;

    @Column(name = "CABECERA_ES")
    private String cabeceraEs;

    private String nombre;

    @Column(name = "PIE_CA")
    private String pieCa;

    @Column(name = "PIE_ES")
    private String pieEs;

    // bi-directional many-to-one association to Bc2Organismo
    @ManyToOne
    @JoinColumn(name = "ORGANISMO_ID")
    private Organismo organismo;

    // bi-directional many-to-one association to Bc2Proceso
    @ManyToOne
    @JoinColumn(name = "PROCESO_ID")
    private Proceso proceso;

    private static TextoDAO textoDAO;

    @Autowired
    public void setTextoDAO(TextoDAO textoDAO)
    {
        Texto.textoDAO = textoDAO;
    }

    public Texto()
    {
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getCabeceraCa()
    {
        return this.cabeceraCa;
    }

    public void setCabeceraCa(String cabeceraCa)
    {
        this.cabeceraCa = cabeceraCa;
    }

    public String getCabeceraEs()
    {
        return this.cabeceraEs;
    }

    public void setCabeceraEs(String cabeceraEs)
    {
        this.cabeceraEs = cabeceraEs;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getPieCa()
    {
        return this.pieCa;
    }

    public void setPieCa(String pieCa)
    {
        this.pieCa = pieCa;
    }

    public String getPieEs()
    {
        return this.pieEs;
    }

    public void setPieEs(String pieEs)
    {
        this.pieEs = pieEs;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public void setOrganismo(Organismo organismo)
    {
        this.organismo = organismo;
    }

    public Proceso getProceso()
    {
        return this.proceso;
    }

    public void setProceso(Proceso proceso)
    {
        this.proceso = proceso;
    }
}