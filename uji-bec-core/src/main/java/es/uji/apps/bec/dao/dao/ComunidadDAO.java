package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Comunidad;
import es.uji.commons.db.BaseDAO;

public interface ComunidadDAO extends BaseDAO
{
    List<Comunidad> getComunidades();

    List<Comunidad> getComunidadByCodigo(String codigo);
}
