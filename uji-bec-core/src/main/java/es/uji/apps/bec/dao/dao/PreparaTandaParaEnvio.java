package es.uji.apps.bec.dao.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class PreparaTandaParaEnvio
{
    private GetMultiEnvioProcedure getMultiEnvio;

    private static DataSource dataSource;

    private String sql;

    public PreparaTandaParaEnvio()
    {
    }

    public PreparaTandaParaEnvio(String sql)
    {
        this.sql = sql;
    }

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        PreparaTandaParaEnvio.dataSource = dataSource;
    }

    public void init()
    {
        this.getMultiEnvio = new GetMultiEnvioProcedure(dataSource, this.sql);
    }

    public Long getMultiEnvio(Long tandaId, Long registroId)
    {
        return getMultiEnvio.execute(tandaId, registroId);
    }

    private class GetMultiEnvioProcedure extends StoredProcedure
    {
        private static final String FUNCION = ".lanzageneracion";
        private static final String PTANDA = "ptanda_id";
        private static final String PREGISTRO = "pregistro_id";

        public GetMultiEnvioProcedure(DataSource dataSource, String sql)
        {
            setDataSource(dataSource);
            setFunction(true);
            setSql(sql + FUNCION);
            declareParameter(new SqlOutParameter("multiEnvio", Types.BIGINT));
            declareParameter(new SqlParameter(PTANDA, Types.BIGINT));
            declareParameter(new SqlParameter(PREGISTRO, Types.BIGINT));
            compile();
        }

        public Long execute(Long tandaId, Long registroId)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PREGISTRO, registroId);
            inParams.put(PTANDA, tandaId);

            Map<String, Object> results = execute(inParams);
            Long envio = (Long) results.get("multiEnvio");
            return envio;
        }
    }
}
