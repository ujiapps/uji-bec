package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoFamilia;
import es.uji.apps.bec.model.TipoFamilia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoFamiliaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoFamiliaDAO
{
    private QTipoFamilia tipoFamilia = QTipoFamilia.tipoFamilia;

    @Override
    public List<TipoFamilia> getTiposFamilias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoFamilia).orderBy(tipoFamilia.orden.asc());
        return query.list(tipoFamilia);
    }

    @Override
    public TipoFamilia getTipoFamiliaById(Long tipoFamiliaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoFamilia).where(tipoFamilia.id.eq(tipoFamiliaId));
        return query.uniqueResult(tipoFamilia);
    }

    @Override
    @Transactional
    public void deleteTipoFamilia(Long tipoFamiliaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoFamilia);
        delete.where(tipoFamilia.id.eq(tipoFamiliaId)).execute();
    }
}
