package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoMoneda;
import es.uji.apps.bec.model.TipoMoneda;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoMonedaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoMonedaDAO
{
    private QTipoMoneda tipoMoneda = QTipoMoneda.tipoMoneda;

    @Override
    public List<TipoMoneda> getTiposMonedas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMoneda);
        return query.list(tipoMoneda);
    }

    @Override
    public TipoMoneda getTipoMonedaById(Long tipoMonedaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMoneda).where(tipoMoneda.id.eq(tipoMonedaId));
        return query.uniqueResult(tipoMoneda);
    }

    @Override
    public TipoMoneda getTipoMonedaByCodigo(String tipoMonedaCodigo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMoneda).where(tipoMoneda.codigo.eq(tipoMonedaCodigo));
        return query.uniqueResult(tipoMoneda);
    }

    @Override
    @Transactional
    public void deleteTipoMoneda(Long tipoMonedaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoMoneda);
        delete.where(tipoMoneda.id.eq(tipoMonedaId)).execute();
    }
}
