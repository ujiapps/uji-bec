package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class EstudioMinisterioMayores25Exception extends GeneralBECException
{
    public EstudioMinisterioMayores25Exception()
    {
        super("L\'estudi per al que ha sol·licitat beca al ministeri es:\n \'Accés majors de 25\' \nEstà matriculat de més d\'una titulació.");
    }

    public EstudioMinisterioMayores25Exception(String message)
    {
        super(message);
    }
}
