package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class RamaNoExisteException extends CoreBaseException
{
    public RamaNoExisteException(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }
}
