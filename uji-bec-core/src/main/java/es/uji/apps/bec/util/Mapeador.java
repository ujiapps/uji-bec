package es.uji.apps.bec.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import es.uji.apps.bec.model.domains.CodigoOrganismo;

public class Mapeador<T, K>
{
    private Map<T, Registro<T, K>> registros;

    public Mapeador()
    {
        registros = new HashMap<T, Registro<T, K>>();
    }

    public K getValorOrganismo(T valor, CodigoOrganismo entidad)
    {
        Registro<T, K> record = registros.get(valor);
        return record.getValor(entidad);
    }

    public T getValorUJI(K valor, CodigoOrganismo tipo)
    {
        for (Entry<T, Registro<T, K>> registro : registros.entrySet())
        {
            if (registro.getValue().getValor(tipo).equals(valor))
            {
                return registro.getKey();
            }
        }
        return null;
    }

    public void add(T valor, Map<CodigoOrganismo, K> relaciones)
    {
        Registro<T, K> registro = new Registro<T, K>(valor);

        for (Entry<CodigoOrganismo, K> relacion : relaciones.entrySet())
        {
            registro.addRelacionCon(relacion.getKey(), relacion.getValue());
        }
        registros.put(valor, registro);
    }
}