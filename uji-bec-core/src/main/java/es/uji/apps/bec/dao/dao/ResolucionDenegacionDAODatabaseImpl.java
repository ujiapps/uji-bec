package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QResolucionDenegacion;
import es.uji.apps.bec.model.ResolucionDenegacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResolucionDenegacionDAODatabaseImpl extends BaseDAODatabaseImpl implements
        ResolucionDenegacionDAO
{

    @Override
    public List<ResolucionDenegacion> getDenegacionesByMultienvioAndBeca(Long multiEnvioId,
            Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QResolucionDenegacion denegaciones = QResolucionDenegacion.resolucionDenegacion;
        
        query.from(denegaciones).where(
                denegaciones.multienvioId.eq(multiEnvioId).and(denegaciones.becaId.eq(becaId)));

        return query.list(denegaciones);
    }

}
