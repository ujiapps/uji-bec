package es.uji.apps.bec.model.domains;

public enum CodigoConvocatoria
{
    GENERAL(1L),

    MOVILIDAD(2L),

    CONSELLERIA(3L),

    GVA_SALARIO(11L),

    GVA_FINALIZACION(12L);

    private Long id;

    CodigoConvocatoria(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    
}
