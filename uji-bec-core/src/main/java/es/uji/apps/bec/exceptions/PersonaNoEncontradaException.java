package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class PersonaNoEncontradaException extends GeneralBECException
{
    public PersonaNoEncontradaException()
    {
        super("No es pot trobar el DNI");
    }

    public PersonaNoEncontradaException(String message)
    {
        super(message);
    }
}