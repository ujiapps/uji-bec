package es.uji.apps.bec.dao.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QRegistroEnvio;
import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class RegistroEnvioDAODatabaseImpl extends BaseDAODatabaseImpl implements RegistroEnvioDAO
{
    @Override
    public RegistroEnvio getRegistroEnvioByTanda(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QRegistroEnvio registroEnvio = QRegistroEnvio.registroEnvio;

        query.from(registroEnvio)
                .where(registroEnvio.tanda.id.eq(tandaId))
                .orderBy(registroEnvio.id.desc());

        if (query.list(registroEnvio).size() > 0)
        {
            return query.list(registroEnvio).get(0);
        }
        return new RegistroEnvio();
    }
}
