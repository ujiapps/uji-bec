package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ProvinciaDAO;

/**
 * The persistent class for the BC2_EXT_PROVINCIAS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_EXT_PROVINCIAS")
public class Provincia implements Serializable
{
    private static ProvinciaDAO provinciaDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String codigo;
    private String nombre;
    private Long orden;
    // bi-directional many-to-one association to Domicilio
    @OneToMany(mappedBy = "provincia")
    private Set<Domicilio> domicilios;
    // bi-directional many-to-one association to Localidad
    @OneToMany(mappedBy = "provincia")
    private Set<Localidad> localidades;

    public Provincia()
    {
    }

    public static List<Provincia> getProvincias()
    {
        return provinciaDAO.getProvincias();
    }

    public static Provincia getProvinciaById(Long provinciaId)
    {
        return provinciaDAO.get(Provincia.class, provinciaId).get(0);
    }

    @Autowired
    public void setProvinciaDAO(ProvinciaDAO provinciaDAO)
    {
        Provincia.provinciaDAO = provinciaDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }

    public Set<Localidad> getLocalidades()
    {
        return this.localidades;
    }

    public void setLocalidades(Set<Localidad> localidades)
    {
        this.localidades = localidades;
    }
}