package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class FaltaNotaMediaException extends CoreBaseException
{

    public FaltaNotaMediaException()
    {
        super("La nota mitjana és obligatoria");
    }

    public FaltaNotaMediaException(String message)
    {
        super(message);
    }
}
