package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoFamilia;
import es.uji.commons.db.BaseDAO;

public interface TipoFamiliaDAO extends BaseDAO
{
    List<TipoFamilia> getTiposFamilias();

    TipoFamilia getTipoFamiliaById(Long tipoFamiliaId);

    void deleteTipoFamilia(Long tipoFamiliaId);
}