package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.apps.bec.model.QBeca;
import es.uji.apps.bec.model.QCuantia;
import es.uji.apps.bec.model.QCuantiaBeca;
import es.uji.apps.bec.model.QCuantiaCurso;
import es.uji.apps.bec.model.QSolicitante;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CuantiaBecaDAODatabaseImpl extends BaseDAODatabaseImpl implements CuantiaBecaDAO
{
    @Override
    public List<CuantiaBeca> getCuantiasByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCuantiaBeca cuantiaBeca = QCuantiaBeca.cuantiaBeca;
        QCuantia cuantia = QCuantia.cuantia;
        QCuantiaCurso cuantiaCurso = QCuantiaCurso.cuantiaCurso;
        QBeca beca = QBeca.beca;
        QSolicitante solicitante = QSolicitante.solicitante;

        query.from(cuantiaBeca)
                .join(cuantiaBeca.beca, beca)
                .join(beca.solicitante, solicitante)
                .join(cuantiaBeca.cuantia, cuantia)
                .fetch()
                .join(cuantia.cuantiasPorCursos, cuantiaCurso)
                .fetch()
                .where(cuantiaBeca.beca.id.eq(becaId).and(
                        cuantiaCurso.cursoAcademico.id.eq(solicitante.cursoAcademico.id)));

        return query.list(cuantiaBeca);
    }

    @Override
    public CuantiaBeca getCuantiaBecaByBecaIdAndCuantiaId(Long becaId, Long cuantiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCuantiaBeca cuantiaBeca = QCuantiaBeca.cuantiaBeca;

        query.from(cuantiaBeca).where(
                cuantiaBeca.beca.id.eq(becaId).and(cuantiaBeca.cuantia.id.eq(cuantiaId)));

        List<CuantiaBeca> listaCuantias = query.list(cuantiaBeca);

        if (listaCuantias.size() > 0)
        {
            return listaCuantias.get(0);
        }
        return null;
    }
}
