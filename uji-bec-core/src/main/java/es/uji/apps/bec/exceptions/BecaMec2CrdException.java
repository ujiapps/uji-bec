package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaMec2CrdException extends CoreDataBaseException {
    public BecaMec2CrdException() {
        super("La beca no cumpleix les condicions Mec2Crd");
    }

    public BecaMec2CrdException(String message) {
        super(message);
    }
}
