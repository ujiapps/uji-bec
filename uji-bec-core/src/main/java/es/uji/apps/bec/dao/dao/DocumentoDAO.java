package es.uji.apps.bec.dao.dao;

import com.mysema.query.Tuple;
import es.uji.apps.bec.model.Documento;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface DocumentoDAO extends BaseDAO
{
    List<Tuple> getDocumentosByBecaId(Long becaId);

    void updateDocumento(Documento documento);

    void borrarDocumento(Long documentoId);
}
