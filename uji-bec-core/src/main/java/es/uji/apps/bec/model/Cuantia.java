package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.CuantiaDAO;

/**
 * The persistent class for the BC2_CUANTIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_CUANTIAS", schema = "UJI_BECAS")
public class Cuantia implements Serializable
{
    @Id
    private Long id;

    private Boolean activa;

    private String codigo;

    private String nombre;

    // bi-directional many-to-one association to CuantiaBeca
    @OneToMany(mappedBy = "cuantia")
    private Set<CuantiaBeca> cuantiasBecas;

    // bi-directional many-to-one association to CuantiaPorCurso
    @OneToMany(mappedBy = "cuantia")
    private Set<CuantiaCurso> cuantiasPorCursos;

    // bi-directional many-to-one association to Convocatoria
    @ManyToOne
    private Convocatoria convocatoria;

    private static CuantiaDAO cuantiaDAO;

    @Autowired
    public void setCuantiaDAO(CuantiaDAO cuantiaDAO)
    {
        Cuantia.cuantiaDAO = cuantiaDAO;
    }

    public Cuantia()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean isActiva()
    {
        return this.activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public String getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Convocatoria getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public Set<CuantiaBeca> getCuantiasBecas()
    {
        return this.cuantiasBecas;
    }

    public void setCuantiasBecas(Set<CuantiaBeca> cuantiasBecas)
    {
        this.cuantiasBecas = cuantiasBecas;
    }

    public Set<CuantiaCurso> getCuantiasPorCursos()
    {
        return this.cuantiasPorCursos;
    }

    public void setCuantiasPorCursos(Set<CuantiaCurso> cuantiasPorCursos)
    {
        this.cuantiasPorCursos = cuantiasPorCursos;
    }

    public static Cuantia getCuantiaById(Long cuantiaId)
    {
        List<Cuantia> cuantias = cuantiaDAO.get(Cuantia.class, cuantiaId);
        if (cuantias.size() > 0)
        {
            return cuantias.get(0);
        }
        return new Cuantia();
    }

    @Transactional
    public Cuantia insert()
    {
        return cuantiaDAO.insert(this);
    }
}