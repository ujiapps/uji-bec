package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaCuantiaNoExisteException extends CoreDataBaseException
{
    public BecaCuantiaNoExisteException()
    {
        super("No existeix la descripció de la quantia");
    }

    public BecaCuantiaNoExisteException(String message)
    {
        super(message);
    }
}