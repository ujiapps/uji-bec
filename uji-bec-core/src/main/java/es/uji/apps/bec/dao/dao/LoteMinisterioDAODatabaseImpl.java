package es.uji.apps.bec.dao.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.LoteMinisterio;
import es.uji.apps.bec.model.QLoteMinisterio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class LoteMinisterioDAODatabaseImpl extends BaseDAODatabaseImpl implements LoteMinisterioDAO
{

    @Override
    public List<LoteMinisterio> getLoteByLoteIdAndCursoAca(Long loteId, Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLoteMinisterio lote = QLoteMinisterio.loteMinisterio;

        query.from(lote).where(lote.cursoAcademicoId.eq(cursoAca).and(lote.loteId.eq(loteId)));

        return query.list(lote);
    }

    @Override
    public List<LoteMinisterio> getLoteByMultienvioId(Long multiEnvioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLoteMinisterio lote = QLoteMinisterio.loteMinisterio;

        query.from(lote).where(lote.multienvioId.eq(multiEnvioId));

        return query.list(lote);
    }

    @Override
    public List<LoteMinisterio> getLotes(Map<String, String> filtros)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLoteMinisterio loteMinisterio = QLoteMinisterio.loteMinisterio;

        Long cursoAcademicoId = ParamUtils.parseLong(filtros.get("cursoAcademicoId"));
        Long convocatoriaId = ParamUtils.parseLong(filtros.get("convocatoriaId"));

        BooleanBuilder condiciones = new BooleanBuilder();
        addFiltroCursoAcademico(cursoAcademicoId, condiciones);
        addFiltroConvocatoria(convocatoriaId, condiciones);

        query.from(loteMinisterio);

        if (condiciones.hasValue())
        {
            query.where(condiciones);
        }

        query.orderBy(loteMinisterio.cursoAcademicoId.desc(), loteMinisterio.loteId.desc());

        return query.list(loteMinisterio);
    }

    private void addFiltroCursoAcademico(Long cursoAcademicoId, BooleanBuilder filtro)
    {
        if (cursoAcademicoId != null)
        {
            filtro.and(QLoteMinisterio.loteMinisterio.cursoAcademicoId.eq(cursoAcademicoId));
        }
    }

    private void addFiltroConvocatoria(Long convocatoriaId, BooleanBuilder filtro)
    {
        if (convocatoriaId != null)
        {
            filtro.and(QLoteMinisterio.loteMinisterio.convocatoriaId.eq(convocatoriaId));
        }
    }
}
