package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.db.BaseDAO;

public interface CursoAcademicoDAO extends BaseDAO
{
    List<CursoAcademico> getCursos();

    List<CursoAcademico> getCursoActivo();

    CursoAcademico getCursoAcademicoById(Long cursoAcademicoId);

    void desactivaCursosActivos();

    List<CursoAcademico> getCursosMayoresOIgualesQue(Long cursoAcademicoId);

    void deleteCursoAcademico(Long cursoAcademicoId);
}
