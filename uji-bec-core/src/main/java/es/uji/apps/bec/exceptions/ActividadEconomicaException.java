package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class ActividadEconomicaException extends GeneralBECException
{
    public ActividadEconomicaException()
    {
        super("Error en la actividad económica");
    }

    public ActividadEconomicaException(String message)
    {
        super(message);
    }
}