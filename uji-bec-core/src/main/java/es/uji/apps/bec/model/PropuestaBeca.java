package es.uji.apps.bec.model;

import java.util.List;

import es.uji.apps.bec.dao.dao.BecaControlParaPropuesta;
import es.uji.apps.bec.exceptions.AcademicosNoCargadosException;
import es.uji.apps.bec.exceptions.BecaDenegacionDuplicadaException;
import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.model.domains.CodigoDenegacion;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;


public class PropuestaBeca
{
    private static final Long SUSTENTADOR_PRINCIPAL = 1L;
    private static final Long SUSTENTADOR_CONYUGE = 2L;
    private static final Long HERMANO = 4L;
    private static final Long HIJO = 9L;
    private static final Long VIVIENDA_EN_ALQUILER = 5L;
    private static final Long SIN_MINUSVALIA = 1L;
    private static final Long MINUSVALIA_33 = 2L;
    private static final Long MINUSVALIA_65 = 3L;
    private static final Long MINUSVALIA_80 = 4L;
    private static final Long SOLICITANTE = 1L;

    public static void calculaNumeroArrendatariosDomicilios(Beca beca)
    {
        List<Domicilio> listaDomicilios = Domicilio.getDomiciliosByBecaId(beca.getId());

        for (Domicilio domicilio : listaDomicilios)
        {
            TipoResidencia tipoResidencia = domicilio.getTipoResidencia();

            if (tipoResidencia != null && tipoResidencia.getId() != null
                    && PropuestaBeca.VIVIENDA_EN_ALQUILER.equals(tipoResidencia.getId()))
            {
                List<Arrendatario> arrendatarios = Arrendatario.getArrendatariosByDomicilio(domicilio.getId());

                if (arrendatarios != null)
                {
                    domicilio.setNumeroArrendatarios((long) arrendatarios.size() + 1);
                }
                else
                {
                    domicilio.setNumeroArrendatarios(1L);
                }

                domicilio.updateDomicilio();
            }
        }
    }

    public static void calculaNumeroHermanosYMinusvalias(Beca beca)
    {
        List<Miembro> listaMiembros = Miembro.getMiembrosByBeca(beca.getId());
        Long numeroHermanos = 0L;
        Long numeroHijos = 0L;
        Long numeroHermanosEstudianFuera = 0L;
        Long numeroHijosEstudianFuera = 0L;
        Long numeroMinusvalia33 = 0L;
        Long numeroMinusvalia65 = 0L;

        for (Miembro miembro : listaMiembros)
        {
            Long minusvaliaId = PropuestaBeca.SIN_MINUSVALIA;

            if (miembro.getTipoMinusvalia() != null)
            {
                minusvaliaId = miembro.getTipoMinusvalia().getId();
            }

            numeroMinusvalia33 = calculaMinusvalia33(numeroMinusvalia33, minusvaliaId);
            numeroMinusvalia65 = calculaMinusvalia65(numeroMinusvalia65, minusvaliaId);

            numeroHermanos = calculaNumeroHermanos(numeroHermanos, miembro);
            numeroHermanosEstudianFuera = calculaNumeroHermanosEstudianFuera(numeroHermanosEstudianFuera, miembro);
            numeroHijos = calculaNumeroHijos(numeroHijos, miembro);
            numeroHijosEstudianFuera = calculaNumeroHijosEstudianFuera(numeroHijosEstudianFuera, miembro);
        }

        Solicitante solicitante = beca.getSolicitante();
        solicitante.setNumeroMinusvalia33(numeroMinusvalia33);
        solicitante.setNumeroMinusvalia65(numeroMinusvalia65);

        guardaNumeroHermanos(solicitante, beca, numeroHermanos, numeroHijos);
        guardaNumeroHermanosEstudianFuera(solicitante, beca, numeroHermanosEstudianFuera, numeroHijosEstudianFuera);

        solicitante.updateDatosGenerales();
    }

    private static Long calculaNumeroHermanos(Long numeroHermanos, Miembro miembro)
    {
        Long tipoMiembroId = miembro.getTipoMiembro().getId();
        if (tipoMiembroId.equals(PropuestaBeca.HERMANO))
        {
            numeroHermanos = numeroHermanos + 1L;
        }
        return numeroHermanos;
    }

    private static Long calculaNumeroHermanosEstudianFuera(Long numeroHermanosEstudianFuera, Miembro miembro)
    {
        Long tipoMiembroId = miembro.getTipoMiembro().getId();
        Long tipoSustentadorId = miembro.getTipoSustentador().getId();

        if (!miembro.isEstudiaFuera())
        {
            return numeroHermanosEstudianFuera;
        }

        if (tipoSustentadorId.equals(PropuestaBeca.SUSTENTADOR_PRINCIPAL))
        {
            return numeroHermanosEstudianFuera;
        }

        if (tipoMiembroId.equals(PropuestaBeca.SUSTENTADOR_CONYUGE))
        {
            return numeroHermanosEstudianFuera;
        }

        if (tipoMiembroId.equals(PropuestaBeca.HERMANO) || tipoMiembroId.equals(PropuestaBeca.SOLICITANTE))
        {
            numeroHermanosEstudianFuera = numeroHermanosEstudianFuera + 1L;
        }
        return numeroHermanosEstudianFuera;
    }

    private static Long calculaNumeroHijos(Long numeroHijos, Miembro miembro)
    {
        Long tipoMiembroId = miembro.getTipoMiembro().getId();
        if (tipoMiembroId.equals(PropuestaBeca.HIJO))
        {
            numeroHijos = numeroHijos + 1L;
        }
        return numeroHijos;
    }

    private static Long calculaNumeroHijosEstudianFuera(Long numeroHijosEstudianFuera, Miembro miembro)
    {
        Long tipoMiembroId = miembro.getTipoMiembro().getId();
        if (tipoMiembroId.equals(PropuestaBeca.HIJO) && miembro.isEstudiaFuera())
        {
            numeroHijosEstudianFuera = numeroHijosEstudianFuera + 1L;
        }
        return numeroHijosEstudianFuera;
    }

    private static Long calculaMinusvalia65(Long numeroMinusvalia65, Long minusvaliaId)
    {
        if (minusvaliaId.equals(PropuestaBeca.MINUSVALIA_65) || minusvaliaId.equals(PropuestaBeca.MINUSVALIA_80))
        {
            numeroMinusvalia65 = numeroMinusvalia65 + 1;
        }
        return numeroMinusvalia65;
    }

    private static Long calculaMinusvalia33(Long numeroMinusvalia33, Long minusvaliaId)
    {
        if (minusvaliaId.equals(PropuestaBeca.MINUSVALIA_33))
        {
            numeroMinusvalia33 = numeroMinusvalia33 + 1;
        }
        return numeroMinusvalia33;
    }

    private static void guardaNumeroHermanos(Solicitante solicitante, Beca beca, Long numeroHermanos, Long numeroHijos)
    {
        Long SOLICITANTE = 1L;
        if (solicitante.esSustendatorOConyugeEnBeca(beca))
        {
            solicitante.setNumeroHermanos(numeroHijos);
        }
        else
        {
            solicitante.setNumeroHermanos(numeroHermanos + SOLICITANTE);
        }
    }

    private static void guardaNumeroHermanosEstudianFuera(Solicitante solicitante, Beca beca,
                                                          Long numeroHermanosEstudianFuera, Long numeroHijosEstudianFuera)
    {
        if (solicitante.esSustendatorOConyugeEnBeca(beca))
        {
            solicitante.setNumeroHermanosFuera(numeroHijosEstudianFuera);
        }
        else
        {
            solicitante.setNumeroHermanosFuera(numeroHermanosEstudianFuera);
        }
    }

    public static void calculaNumeroMiembrosComputables(Beca beca)
    {
        Solicitante solicitante = beca.getSolicitante();
        List<Miembro> listaMiembros = Miembro.getMiembrosByBeca(beca.getId());
        if (listaMiembros != null)
        {
            solicitante.setNumeroMiembrosComputables((long) listaMiembros.size());
            solicitante.updateDatosGenerales();
        }
    }

    public static Boolean isMatriculado(Beca beca)
    {
        Estudio estudioBeca = beca.getEstudio();

        if (estudioBeca != null)
        {
            PersonaEstudio estudioPersona = PersonaEstudio
                    .getAcademicosCursoActualbyPersonaEstudioYCursoAca(beca.getSolicitante()
                            .getPersona().getId(), estudioBeca.getId(), beca.getSolicitante()
                            .getCursoAcademico().getId());

            if (estudioPersona.getId() != null)
            {
                return true;
            }
        }
        return false;
    }

    public static void borraDenegaciones(Beca beca)
    {
        List<BecaDenegacion> denegacionesBeca = BecaDenegacion.getDenegacionesByBecaId(beca.getId());

        for (BecaDenegacion denegacionBeca : denegacionesBeca)
        {
            BecaDenegacion.delete(denegacionBeca.getId());
        }
    }

    public static BecaDenegacion insertaDenegacion(CodigoDenegacion codigoDenegacion, Beca beca, Long orden) throws
            BecaDenegacionDuplicadaException
    {
        Denegacion denegacion = Denegacion.getDenegacionById(codigoDenegacion.getId());

        BecaDenegacion becaDenegacion = creaBecaDenegacion(denegacion, beca, orden);
        becaDenegacion.insert();

        return becaDenegacion;
    }

    private static BecaDenegacion creaBecaDenegacion(Denegacion denegacion, Beca beca, Long orden)
    {
        BecaDenegacion becaDenegacion = new BecaDenegacion();
        becaDenegacion.setBeca(beca);
        becaDenegacion.setDenegacion(denegacion);
        becaDenegacion.setOrdenDenegacion(orden);

        return becaDenegacion;
    }

    public static Boolean isMatriculadoCreditosMinimos(Beca beca) throws AcademicosNoCargadosException
    {
        if (beca.getCreditosParaBeca() == null)
        {
            throw new AcademicosNoCargadosException();
        }
        Minimo minimos = beca.calculaCreditosMinimos();

        return (beca.getCreditosParaBeca() >= minimos.getCreditosBecaParcial() || beca.isLimitarCreditosFinEstudios());
    }

    public static Boolean isMatriculadoCreditosMinimosCursoAnterior(Beca beca) throws AcademicosNoCargadosException
    {
        if (beca.getCreditosMatriculadosAnt() == null)
        {
            throw new AcademicosNoCargadosException();
        }
        Minimo minimos = beca.calculaCreditosMinimos();
        return (beca.getCreditosMatriculadosAnt() >= minimos.getCreditosBecaParcial());
    }

    public static void controles(Beca beca) throws BecaException
    {
        Long becaId = beca.getId();

        BecaControlParaPropuesta becaControlParaPropuesta = new BecaControlParaPropuesta();
        becaControlParaPropuesta.init();

        String mensaje = becaControlParaPropuesta.getBecaControl(becaId);

        if (!mensaje.equals("OK"))
        {
            throw new BecaException(mensaje);
        }
    }
}
