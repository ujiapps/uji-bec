//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2024.10.08 at 02:52:32 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.solicitudes;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AlegacionesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AlegacionesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}AlciSqAlegacion"/>
 *         &lt;element ref="{}AlciAlegaciones"/>
 *         &lt;element ref="{}AlciCodEstado"/>
 *         &lt;element ref="{}AlciIndFirma"/>
 *         &lt;element name="DocumentoAlegacion" type="{}DocumentoAlegacionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlegacionesType", propOrder = {
    "alciSqAlegacion",
    "alciAlegaciones",
    "alciCodEstado",
    "alciIndFirma",
    "documentoAlegacion"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.solicitudes.AlegacionesType")
@Table(name = "ALEGACIONESTYPE", schema = "UJI_BECAS_WS_SOLICITUDES")
@Inheritance(strategy = InheritanceType.JOINED)
public class AlegacionesType
    implements Equals, HashCode
{

    @XmlElement(name = "AlciSqAlegacion", required = true)
    protected Float alciSqAlegacion;
    @XmlElement(name = "AlciAlegaciones", required = true)
    protected String alciAlegaciones;
    @XmlElement(name = "AlciCodEstado", required = true)
    protected String alciCodEstado;
    @XmlElement(name = "AlciIndFirma", required = true)
    protected String alciIndFirma;
    @XmlElement(name = "DocumentoAlegacion")
    protected List<DocumentoAlegacionType> documentoAlegacion;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the alciSqAlegacion property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @Basic
    @Column(name = "ALCISQALEGACION", precision = 15, scale = 0)
    public Float getAlciSqAlegacion() {
        return alciSqAlegacion;
    }

    /**
     * Sets the value of the alciSqAlegacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setAlciSqAlegacion(Float value) {
        this.alciSqAlegacion = value;
    }

    /**
     * Gets the value of the alciAlegaciones property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ALCIALEGACIONES", length = 4000)
    public String getAlciAlegaciones() {
        return alciAlegaciones;
    }

    /**
     * Sets the value of the alciAlegaciones property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlciAlegaciones(String value) {
        this.alciAlegaciones = value;
    }

    /**
     * Gets the value of the alciCodEstado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ALCICODESTADO", length = 2)
    public String getAlciCodEstado() {
        return alciCodEstado;
    }

    /**
     * Sets the value of the alciCodEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlciCodEstado(String value) {
        this.alciCodEstado = value;
    }

    /**
     * Gets the value of the alciIndFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ALCIINDFIRMA", length = 1)
    public String getAlciIndFirma() {
        return alciIndFirma;
    }

    /**
     * Sets the value of the alciIndFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlciIndFirma(String value) {
        this.alciIndFirma = value;
    }

    /**
     * Gets the value of the documentoAlegacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentoAlegacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentoAlegacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoAlegacionType }
     * 
     * 
     */
    @OneToMany(targetEntity = DocumentoAlegacionType.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "DOCUMENTOALEGACION_ALEGACION_0")
    public List<DocumentoAlegacionType> getDocumentoAlegacion() {
        if (documentoAlegacion == null) {
            documentoAlegacion = new ArrayList<DocumentoAlegacionType>();
        }
        return this.documentoAlegacion;
    }

    /**
     * 
     * 
     */
    public void setDocumentoAlegacion(List<DocumentoAlegacionType> documentoAlegacion) {
        this.documentoAlegacion = documentoAlegacion;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AlegacionesType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AlegacionesType that = ((AlegacionesType) object);
        {
            Float lhsAlciSqAlegacion;
            lhsAlciSqAlegacion = this.getAlciSqAlegacion();
            Float rhsAlciSqAlegacion;
            rhsAlciSqAlegacion = that.getAlciSqAlegacion();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alciSqAlegacion", lhsAlciSqAlegacion), LocatorUtils.property(thatLocator, "alciSqAlegacion", rhsAlciSqAlegacion), lhsAlciSqAlegacion, rhsAlciSqAlegacion)) {
                return false;
            }
        }
        {
            String lhsAlciAlegaciones;
            lhsAlciAlegaciones = this.getAlciAlegaciones();
            String rhsAlciAlegaciones;
            rhsAlciAlegaciones = that.getAlciAlegaciones();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alciAlegaciones", lhsAlciAlegaciones), LocatorUtils.property(thatLocator, "alciAlegaciones", rhsAlciAlegaciones), lhsAlciAlegaciones, rhsAlciAlegaciones)) {
                return false;
            }
        }
        {
            String lhsAlciCodEstado;
            lhsAlciCodEstado = this.getAlciCodEstado();
            String rhsAlciCodEstado;
            rhsAlciCodEstado = that.getAlciCodEstado();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alciCodEstado", lhsAlciCodEstado), LocatorUtils.property(thatLocator, "alciCodEstado", rhsAlciCodEstado), lhsAlciCodEstado, rhsAlciCodEstado)) {
                return false;
            }
        }
        {
            String lhsAlciIndFirma;
            lhsAlciIndFirma = this.getAlciIndFirma();
            String rhsAlciIndFirma;
            rhsAlciIndFirma = that.getAlciIndFirma();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alciIndFirma", lhsAlciIndFirma), LocatorUtils.property(thatLocator, "alciIndFirma", rhsAlciIndFirma), lhsAlciIndFirma, rhsAlciIndFirma)) {
                return false;
            }
        }
        {
            List<DocumentoAlegacionType> lhsDocumentoAlegacion;
            lhsDocumentoAlegacion = (((this.documentoAlegacion!= null)&&(!this.documentoAlegacion.isEmpty()))?this.getDocumentoAlegacion():null);
            List<DocumentoAlegacionType> rhsDocumentoAlegacion;
            rhsDocumentoAlegacion = (((that.documentoAlegacion!= null)&&(!that.documentoAlegacion.isEmpty()))?that.getDocumentoAlegacion():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "documentoAlegacion", lhsDocumentoAlegacion), LocatorUtils.property(thatLocator, "documentoAlegacion", rhsDocumentoAlegacion), lhsDocumentoAlegacion, rhsDocumentoAlegacion)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Float theAlciSqAlegacion;
            theAlciSqAlegacion = this.getAlciSqAlegacion();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alciSqAlegacion", theAlciSqAlegacion), currentHashCode, theAlciSqAlegacion);
        }
        {
            String theAlciAlegaciones;
            theAlciAlegaciones = this.getAlciAlegaciones();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alciAlegaciones", theAlciAlegaciones), currentHashCode, theAlciAlegaciones);
        }
        {
            String theAlciCodEstado;
            theAlciCodEstado = this.getAlciCodEstado();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alciCodEstado", theAlciCodEstado), currentHashCode, theAlciCodEstado);
        }
        {
            String theAlciIndFirma;
            theAlciIndFirma = this.getAlciIndFirma();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alciIndFirma", theAlciIndFirma), currentHashCode, theAlciIndFirma);
        }
        {
            List<DocumentoAlegacionType> theDocumentoAlegacion;
            theDocumentoAlegacion = (((this.documentoAlegacion!= null)&&(!this.documentoAlegacion.isEmpty()))?this.getDocumentoAlegacion():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "documentoAlegacion", theDocumentoAlegacion), currentHashCode, theDocumentoAlegacion);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
