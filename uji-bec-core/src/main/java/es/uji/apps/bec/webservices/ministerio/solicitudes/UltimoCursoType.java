//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2024.10.08 at 02:52:32 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.solicitudes;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for UltimoCursoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UltimoCursoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}UlcuIdUltCurso" minOccurs="0"/>
 *         &lt;element ref="{}UlcuEstudios" minOccurs="0"/>
 *         &lt;element ref="{}UlcuEspecialidad" minOccurs="0"/>
 *         &lt;element ref="{}UlcuTipcentCtipocentro" minOccurs="0"/>
 *         &lt;element ref="{}UlcuCcentro" minOccurs="0"/>
 *         &lt;element ref="{}UlcuCloc" minOccurs="0"/>
 *         &lt;element ref="{}UlcuCausa" minOccurs="0"/>
 *         &lt;element ref="{}UlcuNombreCentro" minOccurs="0"/>
 *         &lt;element ref="{}UlcuCodEstudio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UltimoCursoType", propOrder = {
    "ulcuIdUltCurso",
    "ulcuEstudios",
    "ulcuEspecialidad",
    "ulcuTipcentCtipocentro",
    "ulcuCcentro",
    "ulcuCloc",
    "ulcuCausa",
    "ulcuNombreCentro",
    "ulcuCodEstudio"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.solicitudes.UltimoCursoType")
@Table(name = "ULTIMOCURSOTYPE", schema = "UJI_BECAS_WS_SOLICITUDES")
@Inheritance(strategy = InheritanceType.JOINED)
public class UltimoCursoType
    implements Equals, HashCode
{

    @XmlElement(name = "UlcuIdUltCurso")
    protected String ulcuIdUltCurso;
    @XmlElement(name = "UlcuEstudios")
    protected String ulcuEstudios;
    @XmlElement(name = "UlcuEspecialidad")
    protected String ulcuEspecialidad;
    @XmlElement(name = "UlcuTipcentCtipocentro")
    protected String ulcuTipcentCtipocentro;
    @XmlElement(name = "UlcuCcentro")
    protected String ulcuCcentro;
    @XmlElement(name = "UlcuCloc")
    protected String ulcuCloc;
    @XmlElement(name = "UlcuCausa")
    protected String ulcuCausa;
    @XmlElement(name = "UlcuNombreCentro")
    protected String ulcuNombreCentro;
    @XmlElement(name = "UlcuCodEstudio")
    protected String ulcuCodEstudio;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the ulcuIdUltCurso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUIDULTCURSO", length = 4)
    public String getUlcuIdUltCurso() {
        return ulcuIdUltCurso;
    }

    /**
     * Sets the value of the ulcuIdUltCurso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuIdUltCurso(String value) {
        this.ulcuIdUltCurso = value;
    }

    /**
     * Gets the value of the ulcuEstudios property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUESTUDIOS", length = 255)
    public String getUlcuEstudios() {
        return ulcuEstudios;
    }

    /**
     * Sets the value of the ulcuEstudios property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuEstudios(String value) {
        this.ulcuEstudios = value;
    }

    /**
     * Gets the value of the ulcuEspecialidad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUESPECIALIDAD", length = 255)
    public String getUlcuEspecialidad() {
        return ulcuEspecialidad;
    }

    /**
     * Sets the value of the ulcuEspecialidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuEspecialidad(String value) {
        this.ulcuEspecialidad = value;
    }

    /**
     * Gets the value of the ulcuTipcentCtipocentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUTIPCENTCTIPOCENTRO", length = 1)
    public String getUlcuTipcentCtipocentro() {
        return ulcuTipcentCtipocentro;
    }

    /**
     * Sets the value of the ulcuTipcentCtipocentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuTipcentCtipocentro(String value) {
        this.ulcuTipcentCtipocentro = value;
    }

    /**
     * Gets the value of the ulcuCcentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUCCENTRO", length = 8)
    public String getUlcuCcentro() {
        return ulcuCcentro;
    }

    /**
     * Sets the value of the ulcuCcentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuCcentro(String value) {
        this.ulcuCcentro = value;
    }

    /**
     * Gets the value of the ulcuCloc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUCLOC", length = 9)
    public String getUlcuCloc() {
        return ulcuCloc;
    }

    /**
     * Sets the value of the ulcuCloc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuCloc(String value) {
        this.ulcuCloc = value;
    }

    /**
     * Gets the value of the ulcuCausa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUCAUSA", length = 60)
    public String getUlcuCausa() {
        return ulcuCausa;
    }

    /**
     * Sets the value of the ulcuCausa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuCausa(String value) {
        this.ulcuCausa = value;
    }

    /**
     * Gets the value of the ulcuNombreCentro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUNOMBRECENTRO", length = 60)
    public String getUlcuNombreCentro() {
        return ulcuNombreCentro;
    }

    /**
     * Sets the value of the ulcuNombreCentro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuNombreCentro(String value) {
        this.ulcuNombreCentro = value;
    }

    /**
     * Gets the value of the ulcuCodEstudio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ULCUCODESTUDIO", length = 8)
    public String getUlcuCodEstudio() {
        return ulcuCodEstudio;
    }

    /**
     * Sets the value of the ulcuCodEstudio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlcuCodEstudio(String value) {
        this.ulcuCodEstudio = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof UltimoCursoType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final UltimoCursoType that = ((UltimoCursoType) object);
        {
            String lhsUlcuIdUltCurso;
            lhsUlcuIdUltCurso = this.getUlcuIdUltCurso();
            String rhsUlcuIdUltCurso;
            rhsUlcuIdUltCurso = that.getUlcuIdUltCurso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuIdUltCurso", lhsUlcuIdUltCurso), LocatorUtils.property(thatLocator, "ulcuIdUltCurso", rhsUlcuIdUltCurso), lhsUlcuIdUltCurso, rhsUlcuIdUltCurso)) {
                return false;
            }
        }
        {
            String lhsUlcuEstudios;
            lhsUlcuEstudios = this.getUlcuEstudios();
            String rhsUlcuEstudios;
            rhsUlcuEstudios = that.getUlcuEstudios();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuEstudios", lhsUlcuEstudios), LocatorUtils.property(thatLocator, "ulcuEstudios", rhsUlcuEstudios), lhsUlcuEstudios, rhsUlcuEstudios)) {
                return false;
            }
        }
        {
            String lhsUlcuEspecialidad;
            lhsUlcuEspecialidad = this.getUlcuEspecialidad();
            String rhsUlcuEspecialidad;
            rhsUlcuEspecialidad = that.getUlcuEspecialidad();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuEspecialidad", lhsUlcuEspecialidad), LocatorUtils.property(thatLocator, "ulcuEspecialidad", rhsUlcuEspecialidad), lhsUlcuEspecialidad, rhsUlcuEspecialidad)) {
                return false;
            }
        }
        {
            String lhsUlcuTipcentCtipocentro;
            lhsUlcuTipcentCtipocentro = this.getUlcuTipcentCtipocentro();
            String rhsUlcuTipcentCtipocentro;
            rhsUlcuTipcentCtipocentro = that.getUlcuTipcentCtipocentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuTipcentCtipocentro", lhsUlcuTipcentCtipocentro), LocatorUtils.property(thatLocator, "ulcuTipcentCtipocentro", rhsUlcuTipcentCtipocentro), lhsUlcuTipcentCtipocentro, rhsUlcuTipcentCtipocentro)) {
                return false;
            }
        }
        {
            String lhsUlcuCcentro;
            lhsUlcuCcentro = this.getUlcuCcentro();
            String rhsUlcuCcentro;
            rhsUlcuCcentro = that.getUlcuCcentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuCcentro", lhsUlcuCcentro), LocatorUtils.property(thatLocator, "ulcuCcentro", rhsUlcuCcentro), lhsUlcuCcentro, rhsUlcuCcentro)) {
                return false;
            }
        }
        {
            String lhsUlcuCloc;
            lhsUlcuCloc = this.getUlcuCloc();
            String rhsUlcuCloc;
            rhsUlcuCloc = that.getUlcuCloc();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuCloc", lhsUlcuCloc), LocatorUtils.property(thatLocator, "ulcuCloc", rhsUlcuCloc), lhsUlcuCloc, rhsUlcuCloc)) {
                return false;
            }
        }
        {
            String lhsUlcuCausa;
            lhsUlcuCausa = this.getUlcuCausa();
            String rhsUlcuCausa;
            rhsUlcuCausa = that.getUlcuCausa();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuCausa", lhsUlcuCausa), LocatorUtils.property(thatLocator, "ulcuCausa", rhsUlcuCausa), lhsUlcuCausa, rhsUlcuCausa)) {
                return false;
            }
        }
        {
            String lhsUlcuNombreCentro;
            lhsUlcuNombreCentro = this.getUlcuNombreCentro();
            String rhsUlcuNombreCentro;
            rhsUlcuNombreCentro = that.getUlcuNombreCentro();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuNombreCentro", lhsUlcuNombreCentro), LocatorUtils.property(thatLocator, "ulcuNombreCentro", rhsUlcuNombreCentro), lhsUlcuNombreCentro, rhsUlcuNombreCentro)) {
                return false;
            }
        }
        {
            String lhsUlcuCodEstudio;
            lhsUlcuCodEstudio = this.getUlcuCodEstudio();
            String rhsUlcuCodEstudio;
            rhsUlcuCodEstudio = that.getUlcuCodEstudio();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ulcuCodEstudio", lhsUlcuCodEstudio), LocatorUtils.property(thatLocator, "ulcuCodEstudio", rhsUlcuCodEstudio), lhsUlcuCodEstudio, rhsUlcuCodEstudio)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theUlcuIdUltCurso;
            theUlcuIdUltCurso = this.getUlcuIdUltCurso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuIdUltCurso", theUlcuIdUltCurso), currentHashCode, theUlcuIdUltCurso);
        }
        {
            String theUlcuEstudios;
            theUlcuEstudios = this.getUlcuEstudios();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuEstudios", theUlcuEstudios), currentHashCode, theUlcuEstudios);
        }
        {
            String theUlcuEspecialidad;
            theUlcuEspecialidad = this.getUlcuEspecialidad();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuEspecialidad", theUlcuEspecialidad), currentHashCode, theUlcuEspecialidad);
        }
        {
            String theUlcuTipcentCtipocentro;
            theUlcuTipcentCtipocentro = this.getUlcuTipcentCtipocentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuTipcentCtipocentro", theUlcuTipcentCtipocentro), currentHashCode, theUlcuTipcentCtipocentro);
        }
        {
            String theUlcuCcentro;
            theUlcuCcentro = this.getUlcuCcentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuCcentro", theUlcuCcentro), currentHashCode, theUlcuCcentro);
        }
        {
            String theUlcuCloc;
            theUlcuCloc = this.getUlcuCloc();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuCloc", theUlcuCloc), currentHashCode, theUlcuCloc);
        }
        {
            String theUlcuCausa;
            theUlcuCausa = this.getUlcuCausa();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuCausa", theUlcuCausa), currentHashCode, theUlcuCausa);
        }
        {
            String theUlcuNombreCentro;
            theUlcuNombreCentro = this.getUlcuNombreCentro();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuNombreCentro", theUlcuNombreCentro), currentHashCode, theUlcuNombreCentro);
        }
        {
            String theUlcuCodEstudio;
            theUlcuCodEstudio = this.getUlcuCodEstudio();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ulcuCodEstudio", theUlcuCodEstudio), currentHashCode, theUlcuCodEstudio);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
