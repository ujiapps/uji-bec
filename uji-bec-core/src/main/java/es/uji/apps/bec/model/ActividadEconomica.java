package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.ActividadEconomicaDAO;

@Component
@Entity
@Table(name = "BC2_ACTIVIDADES_ECONOMICAS")
@SuppressWarnings("serial")
public class ActividadEconomica implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CIF_SOCIEDAD")
    private String cifSociedad;

    @Column(name = "IMP_PARTICIPACION")
    private Float impParticipacion;

    @Column(name = "NUM_SOCIEDAD")
    private Integer numSociedad;

    @Column(name = "PORC_PARTICIPACION")
    private Float porcParticipacion;

    @ManyToOne
    @JoinColumn(name = "MIEMBRO_ID")
    private Miembro miembro;

    private static ActividadEconomicaDAO actividadEconomicaDAO;

    @Autowired
    public void setActividadEconomicaDAO(ActividadEconomicaDAO actividadEconomicaDAO)
    {
        ActividadEconomica.actividadEconomicaDAO = actividadEconomicaDAO;
    }

    public ActividadEconomica()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCifSociedad()
    {
        return this.cifSociedad;
    }

    public void setCifSociedad(String cifSociedad)
    {
        this.cifSociedad = cifSociedad;
    }

    public Float getImpParticipacion()
    {
        return this.impParticipacion;
    }

    public void setImpParticipacion(Float impParticipacion)
    {
        this.impParticipacion = impParticipacion;
    }

    public Integer getNumSociedad()
    {
        return this.numSociedad;
    }

    public void setNumSociedad(Integer numSociedad)
    {
        this.numSociedad = numSociedad;
    }

    public Float getPorcParticipacion()
    {
        return this.porcParticipacion;
    }

    public void setPorcParticipacion(Float porcParticipacion)
    {
        this.porcParticipacion = porcParticipacion;
    }

    public Miembro getMiembro()
    {
        return this.miembro;
    }

    public void setMiembro(Miembro miembro)
    {
        this.miembro = miembro;
    }

    @Transactional
    public void delete()
    {
        actividadEconomicaDAO.delete(Miembro.class, getId());
    }

    public static List<ActividadEconomica> getActividadesEconomicasByMiembroId(Long miembroId)
    {
        return actividadEconomicaDAO.getActividadesEconomicasByMiembroId(miembroId);
    }

    public void deleteActividadEconomica()
    {
        actividadEconomicaDAO.deleteActividadEconomicaById(this.getId());
    }

    @Transactional
    public void insert() {
        actividadEconomicaDAO.insert(this);
    }
}
