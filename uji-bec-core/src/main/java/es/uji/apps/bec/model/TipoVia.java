package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoViaDAO;

/**
 * The persistent class for the BC2_TIPOS_VIAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_TIPOS_VIAS")
public class TipoVia implements Serializable
{
    private static TipoViaDAO tipoViaDAO;
    @Id
    private Long id;
    private String nombre;
    private Long orden;

    // bi-directional many-to-one association to Domicilio
    @OneToMany(mappedBy = "tipoVia")
    private Set<Domicilio> domicilios;

    public TipoVia()
    {
    }

    @Autowired
    public void setTipoViaDAO(TipoViaDAO tipoViaDAO)
    {
        TipoVia.tipoViaDAO = tipoViaDAO;
    }

     public Long getId()
     {
         return this.id;
     }
     
    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }
    
    public static TipoVia getTipoViabyId(Long tipoViaId)
    {
        return tipoViaDAO.getTipoViaById(tipoViaId);
    }
    
    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }
}