package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoResidencia;
import es.uji.apps.bec.model.TipoResidencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoResidenciaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoResidenciaDAO
{
    private QTipoResidencia tipoResidencia = QTipoResidencia.tipoResidencia;

    @Override
    public List<TipoResidencia> getTiposResidencias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoResidencia).orderBy(tipoResidencia.orden.asc());
        return query.list(tipoResidencia);
    }

    @Override
    public List<TipoResidencia> getTiposResidenciaById(Long tipoResidenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoResidencia).where(tipoResidencia.id.eq(tipoResidenciaId));
        return query.list(tipoResidencia);
    }
}
