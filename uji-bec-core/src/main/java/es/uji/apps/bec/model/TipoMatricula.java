package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoMatriculaDAO;

/**
 * The persistent class for the BC2_TIPOS_MATRICULA database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_TIPOS_MATRICULA")
public class TipoMatricula implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    private Long orden;

    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "tipoMatricula")
    private Set<Beca> becas;

    private static TipoMatriculaDAO tipoMatriculaDAO;

    @Autowired
    public void setTipoMatriculaDAO(TipoMatriculaDAO tipoMatriculaDAO)
    {
        TipoMatricula.tipoMatriculaDAO = tipoMatriculaDAO;
    }

    public TipoMatricula()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public static TipoMatricula getTipoMatriculaById(Long tipoMatriculaId)
    {
        List<TipoMatricula> listaTipoMatricula = tipoMatriculaDAO.get(TipoMatricula.class,
                tipoMatriculaId);
        if (listaTipoMatricula.size() > 0)
        {
            return listaTipoMatricula.get(0);
        }
        return null;
    }
}