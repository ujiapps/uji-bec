package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class TandaNoPreparadaParaProcesarException extends CoreDataBaseException
{
    public TandaNoPreparadaParaProcesarException()
    {
        super("Totes les beques han d'estar en estat treballada, denegada UJI o de baixa.");
    }

    public TandaNoPreparadaParaProcesarException(String message)
    {
        super(message);
    }
}