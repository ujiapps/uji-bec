package es.uji.apps.bec.model.domains;

import es.uji.apps.bec.util.Mapeador;

import java.util.HashMap;
import java.util.Map;

public class MapeadorParentesco extends Mapeador<Long, String>
{
    public MapeadorParentesco() {
        Map<CodigoOrganismo, String> valores = new HashMap<CodigoOrganismo, String>();

        valores.put(CodigoOrganismo.CONSELLERIA, "01");
        add(new Long(2L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "02");
        add(new Long(3L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "03");
        add(new Long(4L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "04");
        add(new Long(5L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "05");
        add(new Long(8L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "06");
        add(new Long(9L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "07");
        add(new Long(0L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "08");
        add(new Long(14L), valores);
        valores.put(CodigoOrganismo.CONSELLERIA, "09");
        add(new Long(10L), valores);
    }
}
