package es.uji.apps.bec.validation;

import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class ValidateSchema {
    public boolean validate(String xsdPath, String xmlPath) {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException | SAXException e) {
            System.out.println("Exception: " + e.getMessage());
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        ValidateSchema validateSchema = new ValidateSchema();
        validateSchema.validate("/opt/devel/workspaces/uji/uji-bec/uji-bec-base/src/main/resources/AE_AM_UNIV_Multienviook.xsd", "/tmp/ejemplo.xml");
    }
}
