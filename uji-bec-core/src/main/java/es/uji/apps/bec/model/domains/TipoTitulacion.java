package es.uji.apps.bec.model.domains;

public enum TipoTitulacion
{
    DIPLOMATURA("01", "Diplomatura"),
    GRADO("02", "Grau"),
    LICENCIATURA("03", "Llicenciatura"),
    ING_TECNICA("04", "Enginyeria tècnica/Arquitectura tècnica"),
    ING_SUPERIOR("05", "Enginyeria superior/Arquitectura superior"),
    MASTER("06", "Màster Oficial"),
    DOCTORADO( "07", "Doctorat"),
    GRADO_SUP_NO_UNI("08", "Grau Superior (no universitari)");

    private String codigo;
    private String descripcion;

    private TipoTitulacion(String codigo, String descripcion)
    {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public StringBuilder titulacionToXml()
    {
        StringBuilder builder = new StringBuilder();

        return builder
                .append("<tipoTitulacion>")
                .append("<id>").append(this.getCodigo()).append("</id>")
                .append("<nombre>").append(this.getDescripcion()).append("</nombre>")
                .append("</tipoTitulacion>");
    }

    @Override
    public String toString()
    {
        return descripcion;
    }
}
