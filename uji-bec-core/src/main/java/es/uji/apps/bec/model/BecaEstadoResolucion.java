package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.BecaEstadoResolucionDAO;

/**
 * The persistent class for the BC2_V_RESOL_BECAS_ESTADO database table.
 * 
 */
@Component
@Entity
@Table(name = "BC2_V_RESOL_BECAS_ESTADO")
public class BecaEstadoResolucion implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static BecaEstadoResolucionDAO becaEstadaEstadoResolucionDAO;
    @Id
    @Column(name = "SOLICITUD_ID")
    private Long solicitudId;
    @Column(name = "ACOGIMIENTO_ID")
    private Long acogimientoId;
    private String aeat;
    @Column(name = "ARCHIVO_TEMPORAL")
    private Long archivoTemporal;
    @Column(name = "BECA_ID")
    private Long becaId;
    private String convocatoria;
    @Column(name = "CURSO_ACADEMICO")
    private String cursoAcademicoId;
    @Column(name = "DECLARANTE_ID")
    private Long declaranteId;
    @Column(name = "ECONOMICOS_ID")
    private Long economicosId;
    private String estado;
    @Column(name = "ESTADO_ID")
    private String estadoId;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ESTADO")
    private Date fechaEstado;
    @Column(name = "GENERALES_ID")
    private Long generalesId;
    private String lote;
    @Column(name = "MULTIENVIO_ID")
    private Long multienvioId;
    private String nif;
    @Column(name = "PERSONALES_ID")
    private Long personalesId;
    @Column(name = "SITUACION_SOLICITUD_ID")
    private Long situacionSolicitudId;
    private String tanda;
    @Column(name = "TITULACION_ID")
    private Long titulacionId;

    public BecaEstadoResolucion()
    {

    }

    public static List<BecaEstadoResolucion> getResolucionesByMultienvioId(Long multiEnvioId)
    {
        return becaEstadaEstadoResolucionDAO.getResolucionesByMultienvioId(multiEnvioId);
    }

    @Autowired
    public void setBecaEstadoResolucionDAO(BecaEstadoResolucionDAO becaEstadoResolucionDAO)
    {
        BecaEstadoResolucion.becaEstadaEstadoResolucionDAO = becaEstadoResolucionDAO;
    }

    public Long getAcogimientoId()
    {
        return this.acogimientoId;
    }

    public void setAcogimientoId(Long acogimientoId)
    {
        this.acogimientoId = acogimientoId;
    }

    public String getAeat()
    {
        return this.aeat;
    }

    public void setAeat(String aeat)
    {
        this.aeat = aeat;
    }

    public Long getArchivoTemporal()
    {
        return this.archivoTemporal;
    }

    public void setArchivoTemporal(Long archivoTemporal)
    {
        this.archivoTemporal = archivoTemporal;
    }

    public Long getBecaId()
    {
        return this.becaId;
    }

    public void setBecaId(Long becaId)
    {
        this.becaId = becaId;
    }

    public String getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(String convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public String getCursoAcademicoId()
    {
        return this.cursoAcademicoId;
    }

    public void setCursoAcademicoId(String cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Long getDeclaranteId()
    {
        return this.declaranteId;
    }

    public void setDeclaranteId(Long declaranteId)
    {
        this.declaranteId = declaranteId;
    }

    public Long getEconomicosId()
    {
        return this.economicosId;
    }

    public void setEconomicosId(Long economicosId)
    {
        this.economicosId = economicosId;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(String estadoId)
    {
        this.estadoId = estadoId;
    }

    public Date getFechaEstado()
    {
        return this.fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado)
    {
        this.fechaEstado = fechaEstado;
    }

    public Long getGeneralesId()
    {
        return this.generalesId;
    }

    public void setGeneralesId(Long generalesId)
    {
        this.generalesId = generalesId;
    }

    public String getLote()
    {
        return this.lote;
    }

    public void setLote(String lote)
    {
        this.lote = lote;
    }

    public Long getMultienvioId()
    {
        return this.multienvioId;
    }

    public void setMultienvioId(Long multienvioId)
    {
        this.multienvioId = multienvioId;
    }

    public String getNif()
    {
        return this.nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public Long getPersonalesId()
    {
        return this.personalesId;
    }

    public void setPersonalesId(Long personalesId)
    {
        this.personalesId = personalesId;
    }

    public Long getSituacionSolicitudId()
    {
        return this.situacionSolicitudId;
    }

    public void setSituacionSolicitudId(Long situacionSolicitudId)
    {
        this.situacionSolicitudId = situacionSolicitudId;
    }

    public Long getSolicitudId()
    {
        return this.solicitudId;
    }

    public void setSolicitudId(Long solicitudId)
    {
        this.solicitudId = solicitudId;
    }

    public String getTanda()
    {
        return this.tanda;
    }

    public void setTanda(String tanda)
    {
        this.tanda = tanda;
    }

    public Long getTitulacionId()
    {
        return this.titulacionId;
    }

    public void setTitulacionId(Long titulacionId)
    {
        this.titulacionId = titulacionId;
    }
}
