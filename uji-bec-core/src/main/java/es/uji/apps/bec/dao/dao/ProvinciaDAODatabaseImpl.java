package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Provincia;
import es.uji.apps.bec.model.QProvincia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProvinciaDAODatabaseImpl extends BaseDAODatabaseImpl implements ProvinciaDAO
{

    @Override
    public List<Provincia> getProvincias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QProvincia provincia = QProvincia.provincia;
        query.from(provincia).orderBy(provincia.nombre.asc());
        
        return query.list(provincia);
    }

}
