package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class DistanciaDomicilioFamiliarNoEncontradaException extends CoreDataBaseException
{
    public DistanciaDomicilioFamiliarNoEncontradaException()
    {
        super("No es pot encontrar la distància a la localitat del domicili.");
    }

    public DistanciaDomicilioFamiliarNoEncontradaException(String message)
    {
        super(message);
    }
}