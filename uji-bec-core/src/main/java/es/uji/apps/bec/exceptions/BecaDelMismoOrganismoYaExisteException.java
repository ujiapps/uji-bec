package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class BecaDelMismoOrganismoYaExisteException extends GeneralBECException
{
    public BecaDelMismoOrganismoYaExisteException()
    {
        super("Ya existeix una beca per al mateix organisme i estudi però d\'una altra convocatoria.");
    }
    
    public BecaDelMismoOrganismoYaExisteException(String message)
    {
        super(message);
    }
}
