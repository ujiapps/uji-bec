package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Localidad;
import es.uji.commons.db.BaseDAO;

public interface LocalidadDAO extends BaseDAO
{
    List<Localidad> getLocalidades();

    List<Localidad> getLocalidadesByProvincia(Long provinciaId);

    Localidad getLocalidadByCodigo(String codigoLocalidadMinisterio);
}
