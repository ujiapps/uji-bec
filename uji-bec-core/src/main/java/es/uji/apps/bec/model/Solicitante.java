package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.SolicitanteDAO;
import es.uji.apps.bec.exceptions.BecaDeDistintoOrganismoYaExisteException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.DemasiadasBecasException;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.model.domains.MapeadorTipoFamilia;
import es.uji.apps.bec.webservices.ministerio.solicitudes.DatosPersonalesType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.DeclaranteType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.GeneralesType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.SituacionSolicitudType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.SolicitudType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.TitulacionesSSCCType;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the BC2_SOLICITANTES database table.
 */
@Component
@Entity
@Table(name = "BC2_SOLICITANTES")
@SuppressWarnings("serial")
public class Solicitante implements Serializable
{
    @Transient
    public static final int maximoSolicitantes = 300;
    public static final long BECA_FINALIZACION = 12L;

    private static SolicitanteDAO solicitanteDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String email;
    @Column(name = "NUMERO_BECA_UJI")
    private Long numeroBecaUji;
    @Column(name = "NUMERO_HERMANOS")
    private Long numeroHermanos;
    @Column(name = "NUMERO_HERMANOS_FUERA")
    private Long numeroHermanosFuera;
    @Column(name = "NUMERO_MIEMBROS_COMPUTABLES")
    private Long numeroMiembrosComputables;
    @Column(name = "NUMERO_MINUSVALIA_33")
    private Long numeroMinusvalia33;
    @Column(name = "NUMERO_MINUSVALIA_65")
    private Long numeroMinusvalia65;
    private Boolean orfandad;
    private String telefono1;
    private String telefono2;
    @Column(name = "CARNET_FAMILIA_NUMEROSA")
    private String carnetFamiliaNumerosa;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN_FAMILIA_NUMEROSA")
    private Date fechaFinFamiliaNumerosa;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INI_FAMILIA_NUMEROSA")
    private Date fechaIniFamiliaNumerosa;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ORFANDAD")
    private Date fechaOrfandad;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FALL_PRIM_PROGENITOR")
    private Date fechaFallPrimProgenitor;
    @Column(name = "IND_FAMI_MONOPARENTAL")
    private Boolean indFamiMonoparental;
    @Column(name = "VIOLENCIA_GENERO")
    private Boolean violenciaGenero;
    @Column(name = "OTRA_BECA")
    private String otraBeca;
    @Column(name = "RESIDENCIA_PERMANENTE")
    private Boolean residenciaPermanente;
    @Column(name = "RESIDENCIA_TRABAJO")
    private Boolean residenciaTrabajo;
    @Column(name = "IND_VICTIMA_VG")
    private Boolean indVictimaVg;
    @Column(name = "IND_HIJO_VICTIMA_VG")
    private Boolean indHijoVictimaVg;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_VICTIMA_VG")
    private Date fechaVictimaVg;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_HIJO_VICTIMA_VG")
    private Date fechaHijoVictimaVg;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_REV_FAM_NUM_MEC")
    private Date fechaRevFamNumMec;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_REV_FAM_NUM_UT")
    private Date fechaRevFamNumUt;
    @Column(name = "RESIDENCIA_ESTANCIA_ESTUDIOS")
    private Boolean residenciaEstanciaEstudios;


    @OneToOne(mappedBy = "solicitante", cascade = CascadeType.ALL, orphanRemoval = true)
    private Declarante declarante;
    @OneToOne(mappedBy = "solicitante")
    private Economico economico;
    @ManyToOne
    @JoinColumn(name = "CCAA_FAMILIA_NUMEROSA_ID")
    private Comunidad comunidadFamiliaNumerosa;
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;
    @ManyToOne
    private Persona persona;
    @ManyToOne
    @JoinColumn(name = "PROFESION_SUSTENTADOR")
    private Profesion profesionSustentador;
    @ManyToOne
    @JoinColumn(name = "TIPO_FAMILIA_ID")
    private TipoFamilia tipoFamilia;
    @OneToMany(mappedBy = "solicitante")
    private Set<Beca> becas;

    @OneToMany(mappedBy = "solicitante", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TituloPosee> tituloPosee;

    public Solicitante(Long cursoAcademicoId, Persona persona)
    {
        this();

        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setId(cursoAcademicoId);

        setCursoAcademico(cursoAcademico);
        setPersona(persona);
    }

    public Solicitante()
    {
        this.becas = new HashSet<Beca>();
        this.setOrfandad(false);
        this.setResidenciaPermanente(false);
        this.setResidenciaTrabajo(false);
        this.setIndVictimaVg(false);
        this.setIndHijoVictimaVg(false);
        this.setIndFamiMonoparental(false);
        this.setResidenciaEstanciaEstudios(false);
    }

    public void setResidenciaPermanente(Boolean residenciaPermanente)
    {
        this.residenciaPermanente = residenciaPermanente;
    }

    public void setResidenciaTrabajo(Boolean residenciaTrabajo)
    {
        this.residenciaTrabajo = residenciaTrabajo;
    }

    public static List<Beca> getSolicitantesConFiltros(Map<String, String> filterParams)
            throws DemasiadasBecasException
    {
        List<Beca> becas = solicitanteDAO.getSolicitanteConFiltros(filterParams);

        if (becas.size() > maximoSolicitantes)
        {
            throw new DemasiadasBecasException();
        }

        return becas;
    }

    public static Solicitante getSolicitanteAndBeca(Long solicitanteId, Long becaId)
    {
        return solicitanteDAO.getSolicitanteAndBeca(solicitanteId, becaId);
    }

    public static Solicitante getDatosGenerales(Long becaId)
    {
        List<Solicitante> listaDatosGenerales = solicitanteDAO.getDatosGenerales(becaId);
        if (listaDatosGenerales.size() > 0)
        {
            return listaDatosGenerales.get(0);
        }
        return null;
    }

    public static Solicitante getSolicitanteById(Long solicitanteId)
    {
        return solicitanteDAO.getSolicitanteById(solicitanteId);
    }

    public static Solicitante getSolicitanteByCursoAcademicoAndIdentificacion(
            String cursoAcademicoId, String identificacion)
    {
        return solicitanteDAO.getSolicitanteByCursoAcademicoAndIdentificacion(cursoAcademicoId,
                identificacion);
    }

    public static Solicitante getSolicitanteConBecasById(Long solicitanteId)
    {
        return solicitanteDAO.getSolicitanteConBecasById(solicitanteId);
    }

    public List<Beca> getBecasByOrganismoAndSolicitante(Long solicitanteId, Long organismoId)
    {
        return solicitanteDAO.getBecasByOrganismoAndSolicitante(solicitanteId, organismoId);
    }

    public String getCarnetFamiliaNumerosa()
    {
        return carnetFamiliaNumerosa;
    }

    public void setCarnetFamiliaNumerosa(String carnetFamiliaNumerosa)
    {
        this.carnetFamiliaNumerosa = carnetFamiliaNumerosa;
    }

    public Date getFechaFinFamiliaNumerosa()
    {
        return fechaFinFamiliaNumerosa;
    }

    public void setFechaFinFamiliaNumerosa(Date fechaFinFamiliaNumerosa)
    {
        this.fechaFinFamiliaNumerosa = fechaFinFamiliaNumerosa;
    }

    public Date getFechaIniFamiliaNumerosa()
    {
        return fechaIniFamiliaNumerosa;
    }

    public void setFechaIniFamiliaNumerosa(Date fechaIniFamiliaNumerosa)
    {
        this.fechaIniFamiliaNumerosa = fechaIniFamiliaNumerosa;
    }

    public Date getFechaOrfandad()
    {
        return fechaOrfandad;
    }

    public void setFechaOrfandad(Date fechaOrfandad)
    {
        this.fechaOrfandad = fechaOrfandad;
    }

    public Date getFechaFallPrimProgenitor() {
        return fechaFallPrimProgenitor;
    }

    public void setFechaFallPrimProgenitor(Date fechaFallPrimProgenitor) {
        this.fechaFallPrimProgenitor = fechaFallPrimProgenitor;
    }

    public Boolean getIndFamiMonoparental() {
        return indFamiMonoparental;
    }

    public void setIndFamiMonoparental(Boolean indFamiMonoparental) {
        this.indFamiMonoparental = indFamiMonoparental;
    }

    public Boolean isIndFamiMonoparental() {
        return indFamiMonoparental;
    }

    public Comunidad getComunidadFamiliaNumerosa()
    {
        return comunidadFamiliaNumerosa;
    }

    public void setComunidadFamiliaNumerosa(Comunidad comunidadFamiliaNumerosa)
    {
        this.comunidadFamiliaNumerosa = comunidadFamiliaNumerosa;
    }

    public Boolean getOrfandad()
    {
        return orfandad;
    }

    public void setOrfandad(Boolean orfandad)
    {
        this.orfandad = orfandad;
    }

    public Boolean getViolenciaGenero()
    {
        return violenciaGenero;
    }

    public void setViolenciaGenero(Boolean violenciaGenero)
    {
        this.violenciaGenero = violenciaGenero;
    }

    public Boolean isResidenciaPermanente()
    {
        return residenciaPermanente;
    }

    public Boolean isResidenciaTrabajo()
    {
        return residenciaTrabajo;
    }

    public Boolean isResidenciaEstanciaEstudios()
    {
        return residenciaEstanciaEstudios;
    }


    @Autowired
    public void setSolicitanteDAO(SolicitanteDAO solicitanteDAO)
    {
        Solicitante.solicitanteDAO = solicitanteDAO;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Long getNumeroBecaUji()
    {
        return this.numeroBecaUji;
    }

    public void setNumeroBecaUji(Long numeroBecaUji)
    {
        this.numeroBecaUji = numeroBecaUji;
    }

    public Long getNumeroHermanos()
    {
        return this.numeroHermanos;
    }

    public void setNumeroHermanos(Long numeroHermanos)
    {
        this.numeroHermanos = numeroHermanos;
    }

    public Long getNumeroHermanosFuera()
    {
        return this.numeroHermanosFuera;
    }

    public void setNumeroHermanosFuera(Long numeroHermanosFuera)
    {
        this.numeroHermanosFuera = numeroHermanosFuera;
    }

    public Long getNumeroMiembrosComputables()
    {
        return this.numeroMiembrosComputables;
    }

    public void setNumeroMiembrosComputables(Long numeroMiembrosComputables)
    {
        this.numeroMiembrosComputables = numeroMiembrosComputables;
    }

    public Long getNumeroMinusvalia33()
    {
        return this.numeroMinusvalia33;
    }

    public void setNumeroMinusvalia33(Long numeroMinusvalia33)
    {
        this.numeroMinusvalia33 = numeroMinusvalia33;
    }

    public Long getNumeroMinusvalia65()
    {
        return this.numeroMinusvalia65;
    }

    public void setNumeroMinusvalia65(Long numeroMinusvalia65)
    {
        this.numeroMinusvalia65 = numeroMinusvalia65;
    }

    public Boolean isOrfandad()
    {
        return this.orfandad;
    }

    public Boolean isViolenciaGenero()
    {
        return this.violenciaGenero;
    }

    public String getOtraBeca()
    {
        return otraBeca;
    }

    public void setOtraBeca(String otraBeca)
    {
        this.otraBeca = otraBeca;
    }

    public Profesion getProfesionSustentador()
    {
        return this.profesionSustentador;
    }

    public void setProfesionSustentador(Profesion profesionSustentador)
    {
        this.profesionSustentador = profesionSustentador;
    }

    public String getTelefono1()
    {
        return this.telefono1;
    }

    public void setTelefono1(String telefono1)
    {
        this.telefono1 = telefono1;
    }

    public String getTelefono2()
    {
        return this.telefono2;
    }

    public void setTelefono2(String telefono2)
    {
        this.telefono2 = telefono2;
    }

    public Economico getEconomico()
    {
        return this.economico;
    }

    public void setEconomico(Economico economico)
    {
        this.economico = economico;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public TipoFamilia getTipoFamilia()
    {
        return this.tipoFamilia;
    }

    public void setTipoFamilia(TipoFamilia tipoFamilia)
    {
        this.tipoFamilia = tipoFamilia;
    }


    public Boolean isIndVictimaVg() {
        return indVictimaVg;
    }

    public void setIndVictimaVg(Boolean indVictimaVg) {
        this.indVictimaVg = indVictimaVg;
    }

    public Boolean isIndHijoVictimaVg() {
        return indHijoVictimaVg;
    }

    public void setIndHijoVictimaVg(Boolean indHijoVictimaVg) {
        this.indHijoVictimaVg = indHijoVictimaVg;
    }

    public Date getFechaVictimaVg() {
        return fechaVictimaVg;
    }

    public void setFechaVictimaVg(Date fechaVictimaVg) {
        this.fechaVictimaVg = fechaVictimaVg;
    }

    public Date getFechaHijoVictimaVg() {
        return fechaHijoVictimaVg;
    }

    public void setFechaHijoVictimaVg(Date fechaHijoVictimaVg) {
        this.fechaHijoVictimaVg = fechaHijoVictimaVg;
    }

    public Date getFechaRevFamNumMec() {
        return fechaRevFamNumMec;
    }

    public void setFechaRevFamNumMec(Date fechaRevFamNumMec) {
        this.fechaRevFamNumMec = fechaRevFamNumMec;
    }

    public Date getFechaRevFamNumUt() {
        return fechaRevFamNumUt;
    }

    public void setFechaRevFamNumUt(Date fechaRevFamNumUt) {
        this.fechaRevFamNumUt = fechaRevFamNumUt;
    }

    public Boolean getResidenciaEstanciaEstudios() {
        return residenciaEstanciaEstudios;
    }

    public void setResidenciaEstanciaEstudios(Boolean residenciaEstanciaEstudios) {
        this.residenciaEstanciaEstudios = residenciaEstanciaEstudios;
    }

    public Beca addBeca(Beca beca) throws BecaSinEstudioException
    {
        Long estudioId = null;
        if (beca.getEstudio() != null)
        {
            estudioId = beca.getEstudio().getId();
        }

        if (Beca.existenBecasConEstudio(this.getId(), beca.getConvocatoria().getId())
                && estudioId == null)
        {
            throw new BecaSinEstudioException();
        }

        Beca becaExistente = null;

        if (Beca.existeBecaSinEstudio(this.getId(), beca.getConvocatoria().getId()))
        {
            becaExistente = Beca.getBeca(this.getId(), beca.getConvocatoria().getId(), null);

            becaExistente.setEstudio(beca.getEstudio());
            becaExistente.updateEstudios();
            beca = becaExistente;
        }
        else
        {
            becaExistente = Beca.getBeca(this.getId(), beca.getConvocatoria().getId(), estudioId);
            if (becaExistente == null)
            {
                beca.setSolicitante(this);
            }
            else
            {
                beca = becaExistente;
            }
        }

        this.getBecas().add(beca);

        return beca;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void removeBeca(Beca beca)
    {
        this.getBecas().remove(beca);
        beca.setSolicitante(null);
    }

    public Beca extraerBeca(Long convocatoriaId, Long estudioId)
    {
        boolean mismaConvocatoria = false;
        boolean mismoEstudio = false;

        for (Beca auxBeca : this.getBecas())
        {
            mismaConvocatoria = auxBeca.getConvocatoria().getId().equals(convocatoriaId);

            if (auxBeca.getEstudio() == null && estudioId == null)
            {
                mismoEstudio = true;
            }
            else
            {
                mismoEstudio = auxBeca.getEstudio().getId().equals(estudioId);
            }

            if (mismaConvocatoria && mismoEstudio)
            {
                return auxBeca;
            }
        }
        return null;
    }

    @Transactional
    public Solicitante insert()
    {
        return solicitanteDAO.insert(this);
    }

    public Solicitante update()
    {
        return solicitanteDAO.updateSolicitante(this);
    }

    public void updateOtrosDatos()
    {
        solicitanteDAO.updateOtrosDatos(this);
    }

    public void updateDatosGenerales()
    {
        solicitanteDAO.updateDatosGenerales(this);
    }

    public void importarDatosMinisterio(SolicitudType solicitudType)
            throws RegistroNoEncontradoException
    {
        importarDeDatosPersonalesType(solicitudType.getDatosPersonales());
        importarDeSituacionSolicitudType(solicitudType.getSituacionSolicitud());
        importarDeGeneralesType(solicitudType.getGenerales());
        importarRelacionDeclarante(solicitudType);
        importarRelacionTitulosPosee(solicitudType);
    }

    private void importarRelacionTitulosPosee(SolicitudType solicitudType)
    {
        List<TitulacionesSSCCType> titulacionesSSCC = solicitudType.getTitulacionesSSCC();
        borraTituloPoseeExistentes();

        Set<TituloPosee> listaTitulosPoseeConvertidos = convierteListaTitulosPosee(titulacionesSSCC);
        this.getTituloPosee().addAll(listaTitulosPoseeConvertidos);

    }

    private Set<TituloPosee> convierteListaTitulosPosee(List<TitulacionesSSCCType> titulacionesSSCC)
    {
        Set<TituloPosee> listaTitulacionesConvertidas = new HashSet<TituloPosee>();
        for (TitulacionesSSCCType titulacionesSSCCType : titulacionesSSCC)
        {
            TituloPosee tituloPosee = new TituloPosee();
            tituloPosee.setSolicitante(this);
            tituloPosee.importarDatosMinisterio(titulacionesSSCCType);

            listaTitulacionesConvertidas.add(tituloPosee);
            tituloPosee.insert();
        }
        return listaTitulacionesConvertidas;
    }

    private void borraTituloPoseeExistentes()
    {
        for (TituloPosee tituloPosee : getTituloPosee())
        {
            tituloPosee.delete();
        }
        this.getTituloPosee().clear();
    }

    public Set<TituloPosee> getTituloPosee()
    {
        if (this.tituloPosee == null || this.tituloPosee.isEmpty())
        {
            return new HashSet<TituloPosee>();
        }

        return this.tituloPosee;
    }

    public void setTituloPosee(Set<TituloPosee> tituloPosee)
    {
        this.tituloPosee = tituloPosee;
    }

    private void importarRelacionDeclarante(SolicitudType solicitudType)
            throws RegistroNoEncontradoException
    {
        DeclaranteType declaranteType = solicitudType.getDeclarante();
        if (declaranteType != null)
        {
            borraDeclaranteExistente();
            Declarante declarante = preparaDeclarante(declaranteType);
            this.setDeclarante(declarante);
            declarante.insert();
        }
    }

    private Declarante preparaDeclarante(DeclaranteType declaranteType)
    {
        Declarante declarante = new Declarante();
        declarante.setSolicitante(this);
        declarante.setDeclarante(declaranteType.getDeclDeclarante());
        declarante.setEmpleador(declaranteType.getDeclEmpleador());
        declarante.setLocalidadDeclarante(declaranteType.getDeclLocDecl());
        declarante.setIdentificacion(declaranteType.getDeclNif());
        declarante.setDecIdentificacionConsen(declaranteType.getDeclNifConsentimiento());

        if (declaranteType.getDeclCodDocuIdent() != null)
        {
            TipoIdentificacion tipoIdentificacion = preparaTipoIdentificacion(declaranteType);
            declarante.setTipoIdentificacion(tipoIdentificacion);
        }

        if (declaranteType.getDeclCodDocuIdentConsent() != null)
        {
            TipoIdentificacion declCodDocuIdentConsent = preparaTipoIdentificacionConsen(declaranteType);
            declarante.setDecTipoIdentificacionConsen(declCodDocuIdentConsent);
        }

        if (declaranteType.getDeclCodParentesco() != null)
        {
            TipoMiembro declCodParentesco = preparaTipoMiembro(declaranteType);
            declarante.setDecCodParentescoConsen(declCodParentesco);
        }

        if (declaranteType.getDeclFechaDecl() != null)
        {
            declarante.setFechaDeclarante(declaranteType.getDeclFechaDecl().toGregorianCalendar().getTime());
        }
        if (declaranteType.getDeclIngresosAnuales() != null)
        {
            declarante.setIngresosAnuales(declaranteType.getDeclIngresosAnuales());
        }
        if (declaranteType.getDeclRentasExtranj() != null)
        {
            declarante.setRentasExtranjero(declaranteType.getDeclRentasExtranj());
        }
        if (declaranteType.getDeclIndRentasExtranj() != null)
        {
            declarante.setIndicadorRentasExtranjero(declaranteType.getDeclIndRentasExtranj().equals("S"));
        }
        if (declaranteType.getDeclIndCurUlt() != null)
        {
            declarante.setIndicadorUltCurso(declaranteType.getDeclIndCurUlt().equals("S"));
        }
        if (declaranteType.getDeclIndCurPen() != null)
        {
            declarante.setIndicadorCursoPendiente(declaranteType.getDeclIndCurPen().equals("S"));
        }
        if (declaranteType.getDeclIndCurAnt() != null)
        {
            declarante.setIndicadorCursoAnterior(declaranteType.getDeclIndCurAnt().equals("S"));
        }
        if (declaranteType.getDeclIndIndep() != null)
        {
            declarante.setIndicadorIndependiente(declaranteType.getDeclIndIndep().equals("S"));
        }
        if (declaranteType.getDeclIndInem() != null)
        {
            declarante.setIndicadorInem(declaranteType.getDeclIndInem().equals("S"));
        }
        return declarante;
    }

    private TipoIdentificacion preparaTipoIdentificacion(DeclaranteType declaranteType)
    {
        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
        tipoIdentificacion.setId(declaranteType.getDeclCodDocuIdent());
        return tipoIdentificacion;
    }

    private TipoIdentificacion preparaTipoIdentificacionConsen(DeclaranteType declaranteType)
    {
        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
        tipoIdentificacion.setId(declaranteType.getDeclCodDocuIdentConsent());
        return tipoIdentificacion;
    }

    private TipoMiembro preparaTipoMiembro(DeclaranteType declaranteType)
    {
        TipoMiembro tipoMiembro = new TipoMiembro();
        tipoMiembro.setId(declaranteType.getDeclCodParentesco());
        return tipoMiembro;
    }

    private void borraDeclaranteExistente() throws RegistroNoEncontradoException
    {
        if (getDeclarante() != null)
        {
            Declarante.delete(declarante.getId());
            this.setDeclarante(null);
        }
    }

    public Declarante getDeclarante()
    {
        return this.declarante;
    }

    public void setDeclarante(Declarante declarante)
    {
        this.declarante = declarante;
    }

    private void importarDeGeneralesType(GeneralesType generales)
    {
        if (generales == null)
        {
            return;
        }

        if (generales != null && generales.getGeneCodProf() != null)
        {
            Profesion profesion = new Profesion();
            profesion.setId(ParamUtils.parseLong(generales.getGeneCodProf()));
            this.setProfesionSustentador(profesion);
        }
        this.setOtraBeca(generales.getGeneOtraBecaEspecifiqueSol());
    }

    private void importarDeSituacionSolicitudType(SituacionSolicitudType situacionSolicitud)
    {
        if (situacionSolicitud != null)
        {
            importaTipoFamilia(situacionSolicitud);
            importaComunidadFamiliaNumerosa(situacionSolicitud);

            this.setNumeroMinusvalia33(situacionSolicitud.getSisoMinus33());
            this.setNumeroMinusvalia65(situacionSolicitud.getSisoMinus66());
            this.setNumeroHermanosFuera(situacionSolicitud.getSisoHermuniff());

            this.setCarnetFamiliaNumerosa(situacionSolicitud.getSisoCarnetFamnum());

            if (situacionSolicitud.getSisoFechaFinFamnum() != null && situacionSolicitud.getSisoFechaFinFamnum().toGregorianCalendar() != null)
            {
                this.setFechaFinFamiliaNumerosa(situacionSolicitud.getSisoFechaFinFamnum().toGregorianCalendar().getTime());
            }

            if (situacionSolicitud.getSisoFechaIniFamnum() != null && situacionSolicitud.getSisoFechaIniFamnum().toGregorianCalendar() != null)
            {
                this.setFechaIniFamiliaNumerosa(situacionSolicitud.getSisoFechaIniFamnum().toGregorianCalendar().getTime());
            }

            if (situacionSolicitud.getSisoFechaFallUltprog() != null && situacionSolicitud.getSisoFechaFallUltprog().toGregorianCalendar() != null)
            {
                this.setFechaOrfandad(situacionSolicitud.getSisoFechaFallUltprog().toGregorianCalendar().getTime());
            }

            if (situacionSolicitud.getSisoFechaFallPrimprog() != null && situacionSolicitud.getSisoFechaFallPrimprog().toGregorianCalendar() != null)
            {
                this.setFechaFallPrimProgenitor(situacionSolicitud.getSisoFechaFallPrimprog().toGregorianCalendar().getTime());
            }

            if (situacionSolicitud.getSisoIndOrfandad() != null)
            {
                this.setOrfandad(situacionSolicitud.getSisoIndOrfandad().equals("S"));
            }
            else
            {
                this.setOrfandad(Boolean.FALSE);
            }

            if (situacionSolicitud.getSisoIndFamMonoparental() != null)
            {
                this.setIndFamiMonoparental(situacionSolicitud.getSisoIndFamMonoparental().equals("S"));
            }
            else
            {
                this.setIndFamiMonoparental(Boolean.FALSE);
            }

            if (situacionSolicitud.getSisoIndVictimVg() != null)
            {
                this.setIndVictimaVg(situacionSolicitud.getSisoIndVictimVg().equals("S"));
            }
            else
            {
                this.setIndVictimaVg(Boolean.FALSE);
            }

            if (situacionSolicitud.getSisoIndHijoVictimVg() != null)
            {
                this.setIndHijoVictimaVg(situacionSolicitud.getSisoIndHijoVictimVg().equals("S"));
            }
            else
            {
                this.setIndHijoVictimaVg(Boolean.FALSE);
            }

            if (situacionSolicitud.getSisoFechaSentenVictVg() != null && situacionSolicitud.getSisoFechaSentenVictVg().toGregorianCalendar() != null)
            {
                this.setFechaVictimaVg(situacionSolicitud.getSisoFechaSentenVictVg().toGregorianCalendar().getTime());
            }
            if (situacionSolicitud.getSisoFechaSentenHijoVictVg() != null && situacionSolicitud.getSisoFechaSentenHijoVictVg().toGregorianCalendar() != null)
            {
                this.setFechaHijoVictimaVg(situacionSolicitud.getSisoFechaSentenHijoVictVg().toGregorianCalendar().getTime());
            }
        }
    }

    private void importaComunidadFamiliaNumerosa(SituacionSolicitudType situacionSolicitud)
    {
        if (situacionSolicitud.getSisoCcaaFamnum() != null)
        {
            Comunidad comunidad = new Comunidad();
            comunidad.setId(ParamUtils.parseLong(situacionSolicitud.getSisoCcaaFamnum()));
            this.setComunidadFamiliaNumerosa(comunidad);
        }
    }

    private void importaTipoFamilia(SituacionSolicitudType situacionSolicitud)
    {
        String tipoFamiliaMinisterio = situacionSolicitud.getSisoCodTipfam();
        MapeadorTipoFamilia mapeadorTipoFamilia = new MapeadorTipoFamilia();
        Long tipoFamiliaUjiId = mapeadorTipoFamilia.getValorUJI(tipoFamiliaMinisterio, CodigoOrganismo.MINISTERIO);
        if (tipoFamiliaMinisterio != null)
        {
            TipoFamilia tipoFamiliaUji = new TipoFamilia();
            tipoFamiliaUji.setId(tipoFamiliaUjiId);
            this.setTipoFamilia(tipoFamiliaUji);
        }
    }

    private void importarDeDatosPersonalesType(DatosPersonalesType datosPersonales)
    {
        if (datosPersonales != null)
        {
            this.setTelefono1(datosPersonales.getDapeTelefono());
            this.setTelefono2(datosPersonales.getDapeTelefMovil());
            this.setEmail(datosPersonales.getDapeEmail());
            if (datosPersonales.getDapeIndResPermanente() != null)
            {
                setResidenciaPermanente(datosPersonales.getDapeIndResPermanente().equals("S"));
            }
            if (datosPersonales.getDapeIndResTrabajo() != null)
            {
                setResidenciaTrabajo(datosPersonales.getDapeIndResTrabajo().equals("S"));
            }
            if (datosPersonales.getDapeIndEstanciaEstudios() != null)
            {
                setResidenciaEstanciaEstudios(datosPersonales.getDapeIndEstanciaEstudios().equals("S"));
            }
        }
    }

    public Boolean esSustendatorOConyugeEnBeca(Beca beca)
    {
        Miembro miembroSolicitante = Miembro.getMiembroSolicitante(beca.getId());
        if (miembroSolicitante != null && miembroSolicitante.esSustentadorOConyuge())
        {
            return true;
        }
        return false;
    }

    public Boolean tieneBecasConcedidasDelMinisterio()
            throws BecaDeDistintoOrganismoYaExisteException {
        for (Beca beca : this.getBecas()) {
            if (beca.isConcedidaEnElMinisterio()) {
                return true;
            }
        }
        return false;
    }

    public Boolean tieneBecaMecSolicitada()
            throws BecaDeDistintoOrganismoYaExisteException
    {
        for (Beca beca : this.getBecas())
        {
            if (beca.isSolicitadaEnElMinisterio())
            {
                return true;
            }
        }
        return false;
    }

    public static Solicitante getSolicitanteByCursoAcademicoAndPersonaId(String cursoAcademicoId, Long personaId)
    {
        return solicitanteDAO.getSolicitanteByCursoAcademicoAndPersonaId(cursoAcademicoId, personaId);
    }

    public boolean tieneBecaFinalizacionConcedida() {
        for (Beca beca : this.getBecas())
        {
            if (beca.isBecaConcedida() && beca.getConvocatoria().getId() == BECA_FINALIZACION)
            {
                return true;
            }
        }
        return false;
    }
}
