package es.uji.apps.bec.model.denegaciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "BEC_WS_SOLI_DENEGADAS")
public class BecWsSoliDenegada implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PER_ID")
    private long perId;

    private BigDecimal caentipoenvio;

    private String dapeapellido1;

    private BigDecimal dapecoddocuident;

    private String dapeemail;

    private String dapenif;

    private String dapenombre;

    private String solicodconv;

    private BigDecimal solicodestadoact;

    private BigDecimal solicodnivel;

    private BigDecimal solicodtiso;

    private BigDecimal solicodutgen;

    private BigDecimal solicoduttramite;

    @Temporal(TemporalType.DATE)
    private Date solifechaestadoact;

    private BigDecimal soliidcurso;

    private BigDecimal soliidsolicitud;

    private BigDecimal solilote;

    private BigDecimal solisecuenc;

    private BigDecimal solitipoutgen;

    private BigDecimal solitipouttramite;

    @OneToMany(mappedBy = "becWsSoliDenegada")
    private Set<BecWsSoliDenegadasCodigo> becWsSoliDenegadasCodigos;

    @OneToMany(mappedBy = "becWsSoliDenegada")
    private Set<BecWsSoliDenegadasDomi> becWsSoliDenegadasDomis;

    public BecWsSoliDenegada()
    {
    }

    public long getPerId()
    {
        return this.perId;
    }

    public void setPerId(long perId)
    {
        this.perId = perId;
    }

    public BigDecimal getCaentipoenvio()
    {
        return this.caentipoenvio;
    }

    public void setCaentipoenvio(BigDecimal caentipoenvio)
    {
        this.caentipoenvio = caentipoenvio;
    }

    public String getDapeapellido1()
    {
        return this.dapeapellido1;
    }

    public void setDapeapellido1(String dapeapellido1)
    {
        this.dapeapellido1 = dapeapellido1;
    }

    public BigDecimal getDapecoddocuident()
    {
        return this.dapecoddocuident;
    }

    public void setDapecoddocuident(BigDecimal dapecoddocuident)
    {
        this.dapecoddocuident = dapecoddocuident;
    }

    public String getDapeemail()
    {
        return this.dapeemail;
    }

    public void setDapeemail(String dapeemail)
    {
        this.dapeemail = dapeemail;
    }

    public String getDapenif()
    {
        return this.dapenif;
    }

    public void setDapenif(String dapenif)
    {
        this.dapenif = dapenif;
    }

    public String getDapenombre()
    {
        return this.dapenombre;
    }

    public void setDapenombre(String dapenombre)
    {
        this.dapenombre = dapenombre;
    }

    public String getSolicodconv()
    {
        return this.solicodconv;
    }

    public void setSolicodconv(String solicodconv)
    {
        this.solicodconv = solicodconv;
    }

    public BigDecimal getSolicodestadoact()
    {
        return this.solicodestadoact;
    }

    public void setSolicodestadoact(BigDecimal solicodestadoact)
    {
        this.solicodestadoact = solicodestadoact;
    }

    public BigDecimal getSolicodnivel()
    {
        return this.solicodnivel;
    }

    public void setSolicodnivel(BigDecimal solicodnivel)
    {
        this.solicodnivel = solicodnivel;
    }

    public BigDecimal getSolicodtiso()
    {
        return this.solicodtiso;
    }

    public void setSolicodtiso(BigDecimal solicodtiso)
    {
        this.solicodtiso = solicodtiso;
    }

    public BigDecimal getSolicodutgen()
    {
        return this.solicodutgen;
    }

    public void setSolicodutgen(BigDecimal solicodutgen)
    {
        this.solicodutgen = solicodutgen;
    }

    public BigDecimal getSolicoduttramite()
    {
        return this.solicoduttramite;
    }

    public void setSolicoduttramite(BigDecimal solicoduttramite)
    {
        this.solicoduttramite = solicoduttramite;
    }

    public Date getSolifechaestadoact()
    {
        return this.solifechaestadoact;
    }

    public void setSolifechaestadoact(Date solifechaestadoact)
    {
        this.solifechaestadoact = solifechaestadoact;
    }

    public BigDecimal getSoliidcurso()
    {
        return this.soliidcurso;
    }

    public void setSoliidcurso(BigDecimal soliidcurso)
    {
        this.soliidcurso = soliidcurso;
    }

    public BigDecimal getSoliidsolicitud()
    {
        return this.soliidsolicitud;
    }

    public void setSoliidsolicitud(BigDecimal soliidsolicitud)
    {
        this.soliidsolicitud = soliidsolicitud;
    }

    public BigDecimal getSolilote()
    {
        return this.solilote;
    }

    public void setSolilote(BigDecimal solilote)
    {
        this.solilote = solilote;
    }

    public BigDecimal getSolisecuenc()
    {
        return this.solisecuenc;
    }

    public void setSolisecuenc(BigDecimal solisecuenc)
    {
        this.solisecuenc = solisecuenc;
    }

    public BigDecimal getSolitipoutgen()
    {
        return this.solitipoutgen;
    }

    public void setSolitipoutgen(BigDecimal solitipoutgen)
    {
        this.solitipoutgen = solitipoutgen;
    }

    public BigDecimal getSolitipouttramite()
    {
        return this.solitipouttramite;
    }

    public void setSolitipouttramite(BigDecimal solitipouttramite)
    {
        this.solitipouttramite = solitipouttramite;
    }

    public Set<BecWsSoliDenegadasCodigo> getBecWsSoliDenegadasCodigos()
    {
        return this.becWsSoliDenegadasCodigos;
    }

    public void setBecWsSoliDenegadasCodigos(Set<BecWsSoliDenegadasCodigo> becWsSoliDenegadasCodigos)
    {
        this.becWsSoliDenegadasCodigos = becWsSoliDenegadasCodigos;
    }

    public Set<BecWsSoliDenegadasDomi> getBecWsSoliDenegadasDomis()
    {
        return this.becWsSoliDenegadasDomis;
    }

    public void setBecWsSoliDenegadasDomis(Set<BecWsSoliDenegadasDomi> becWsSoliDenegadasDomis)
    {
        this.becWsSoliDenegadasDomis = becWsSoliDenegadasDomis;
    }
}