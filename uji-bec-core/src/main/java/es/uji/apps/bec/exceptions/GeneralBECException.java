package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class GeneralBECException extends CoreBaseException
{
    public GeneralBECException()
    {
        super("S'ha produït un error en l'operació");
    }

    public GeneralBECException(String message)
    {
        super(message);
    }
}
