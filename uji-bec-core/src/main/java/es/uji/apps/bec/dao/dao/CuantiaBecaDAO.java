package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.commons.db.BaseDAO;

public interface CuantiaBecaDAO extends BaseDAO
{
    List<CuantiaBeca> getCuantiasByBecaId(Long becaId);

    CuantiaBeca getCuantiaBecaByBecaIdAndCuantiaId(Long becaId, Long cuantiaId);
}
