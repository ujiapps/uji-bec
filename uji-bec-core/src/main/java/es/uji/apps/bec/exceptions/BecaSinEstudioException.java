package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class BecaSinEstudioException extends GeneralBECException
{
    public BecaSinEstudioException()
    {
        super("Beca no permitida. No se puede insertar una beca sin estudio cuando el alumno ya posee una beca con estudio.");
    }
    
    public BecaSinEstudioException(String message)
    {
        super(message);
    }
}
