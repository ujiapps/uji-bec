package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.OrganismoDAO;

/**
 * The persistent class for the BC2_ORGANISMOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_ORGANISMOS")
public class Organismo implements Serializable
{
    private static OrganismoDAO organismoDAO;

    @Id
    private Long id;

    private String nombre;

    private String webservice;
    // bi-directional many-to-one association to Convocatoria
    @OneToMany(mappedBy = "organismo")
    private Set<Convocatoria> convocatorias;
    // bi-directional many-to-one association to Diccionario
    @OneToMany(mappedBy = "organismo")
    private Set<Diccionario> diccionarios;
    // bi-directional many-to-one association to Denegacion
    @OneToMany(mappedBy = "organismo")
    private Set<Denegacion> denegaciones;
    //bi-directional many-to-one association to Listado
    @OneToMany(mappedBy = "organismo")
    private List<Listado> listados;

    public Organismo()
    {
    }

    public static Organismo getOrganismoById(Long idOrganismo)
    {
        return organismoDAO.get(Organismo.class, idOrganismo).get(0);
    }

    @Autowired
    public void setOrganismoDAO(OrganismoDAO organismoDAO)
    {
        Organismo.organismoDAO = organismoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getWebservice()
    {
        return this.webservice;
    }

    public void setWebservice(String webservice)
    {
        this.webservice = webservice;
    }

    public Set<Convocatoria> getConvocatorias()
    {
        return this.convocatorias;
    }

    public void setConvocatorias(Set<Convocatoria> convocatorias)
    {
        this.convocatorias = convocatorias;
    }

    public Set<Diccionario> getDiccionarios()
    {
        return this.diccionarios;
    }

    public void setDiccionarios(Set<Diccionario> diccionarios)
    {
        this.diccionarios = diccionarios;
    }

    public Set<Denegacion> getDenegaciones()
    {
        return this.denegaciones;
    }

    public void setDenegaciones(Set<Denegacion> denegaciones)
    {
        this.denegaciones = denegaciones;
    }

    public List<Listado> getListados()
    {
        return listados;
    }

    public void setListados(List<Listado> listados)
    {
        this.listados = listados;
    }
}