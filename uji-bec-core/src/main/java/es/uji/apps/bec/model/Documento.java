package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mysema.query.Tuple;
import es.uji.apps.bec.dao.dao.DocumentoDAO;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_BECAS_DOCUMENTOS", schema = "UJI_BECAS")
public class Documento implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    @Lob
    private byte[] fichero;
    @Column(name = "MIME_TYPE")
    private String mimeType;
    @Column(name = "FECHA_SOLICITA")
    private Date fechaSolicita;
    @Column(name = "FECHA_ACTUALIZA")
    private Date fechaActualiza;
    private Boolean validado;

    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;

    // bi-directional many-to-one association to TipoVia
    @ManyToOne
    @JoinColumn(name = "TIPO_DOCUMENTO_ID")
    private TipoDocumento tipoDocumento;

    private static DocumentoDAO documentoDAO;

    @Autowired
    public void setDocumentoDAO(DocumentoDAO documentoDAO)
    {
        Documento.documentoDAO = documentoDAO;
    }

    public Documento()
    {
        this.setValidado(false);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte[] getFichero()
    {
        return fichero;
    }

    public void setFichero(byte[] fichero)
    {
        this.fichero = fichero;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Date getFechaSolicita() {
        return fechaSolicita;
    }

    public void setFechaSolicita(Date fechaSolicita) {
        this.fechaSolicita = fechaSolicita;
    }

    public Date getFechaActualiza() {
        return fechaActualiza;
    }

    public void setFechaActualiza(Date fechaActualiza) {
        this.fechaActualiza = fechaActualiza;
    }

    public Boolean isValidado() {
        return validado;
    }

    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    public Beca getBeca() {
        return beca;
    }

    public void setBeca(Beca beca) {
        this.beca = beca;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public static List<Tuple> getDocumentosByBecaId(Long becaId)
    {
        return documentoDAO.getDocumentosByBecaId(becaId);
    }

    public static Documento getDocumentoById(Long id)
    {
        if (id == null)
        {
            return null;
        }

        List<Documento> listaDocumentos = documentoDAO.get(Documento.class, id);
        if (listaDocumentos.size() > 0)
        {
            return listaDocumentos.get(0);
        }

        return null;
    }

    public void updateDocumento()
    {
        documentoDAO.updateDocumento(this);
    }

    public void borrarDocumento()
    {
        documentoDAO.borrarDocumento(this.getId());
    }

    public Documento insertaDocumento()
    {
        documentoDAO.insert(this);
        return this;
    }
}