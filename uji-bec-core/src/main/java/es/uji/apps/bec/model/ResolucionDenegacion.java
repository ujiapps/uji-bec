package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ResolucionDenegacionDAO;

/**
 * The persistent class for the BC2_V_RESOL_DENEGACIONES database table.
 * 
 */
@Component
@Entity
@Table(name = "BC2_V_RESOL_DENEGACIONES")
public class ResolucionDenegacion implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "ARCHIVO_TEMPORAL")
    private Long archivoTemporal;

    @Column(name = "BECA_ID")
    private Long becaId;

    private String causa;

    private String convocatoria;

    @Column(name = "CURSO_ACADEMICO")
    private String cursoAcademico;

    @Column(name = "DEN_ESTADO")
    private String denEstado;

    @Column(name = "DEN_ID")
    private Long denId;

    @Column(name = "DEN_NOMBRE")
    private String denNombre;

    private String estado;

    @Column(name = "ESTADO_ID")
    private String estadoId;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ESTADO")
    private Date fechaEstado;

    private String lote;

    @Column(name = "MULTIENVIO_ID")
    private Long multienvioId;

    private String nif;

    @Column(name = "SECUENCIA_ESTADO")
    private Long secuenciaEstado;

    @Column(name = "SOLICITUD_ID")
    private Long solicitudId;

    private String subcausa;

    private String tanda;

    private static ResolucionDenegacionDAO resolucionDenegacionDAO;

    @Autowired
    public void setResolucionDenegacionDAO(ResolucionDenegacionDAO resolucionDenegacionDAO)
    {
        ResolucionDenegacion.resolucionDenegacionDAO = resolucionDenegacionDAO;
    }

    public ResolucionDenegacion()
    {
    }

    public Long getArchivoTemporal()
    {
        return this.archivoTemporal;
    }

    public void setArchivoTemporal(Long archivoTemporal)
    {
        this.archivoTemporal = archivoTemporal;
    }

    public Long getBecaId()
    {
        return this.becaId;
    }

    public void setBecaId(Long becaId)
    {
        this.becaId = becaId;
    }

    public String getCausa()
    {
        return this.causa;
    }

    public void setCausa(String causa)
    {
        this.causa = causa;
    }

    public String getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(String convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public String getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(String cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public String getDenEstado()
    {
        return this.denEstado;
    }

    public void setDenEstado(String denEstado)
    {
        this.denEstado = denEstado;
    }

    public Long getDenId()
    {
        return this.denId;
    }

    public void setDenId(Long denId)
    {
        this.denId = denId;
    }

    public String getDenNombre()
    {
        return this.denNombre;
    }

    public void setDenNombre(String denNombre)
    {
        this.denNombre = denNombre;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(String estadoId)
    {
        this.estadoId = estadoId;
    }

    public Date getFechaEstado()
    {
        return this.fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado)
    {
        this.fechaEstado = fechaEstado;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLote()
    {
        return this.lote;
    }

    public void setLote(String lote)
    {
        this.lote = lote;
    }

    public Long getMultienvioId()
    {
        return this.multienvioId;
    }

    public void setMultienvioId(Long multienvioId)
    {
        this.multienvioId = multienvioId;
    }

    public String getNif()
    {
        return this.nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public Long getSecuenciaEstado()
    {
        return this.secuenciaEstado;
    }

    public void setSecuenciaEstado(Long secuenciaEstado)
    {
        this.secuenciaEstado = secuenciaEstado;
    }

    public Long getSolicitudId()
    {
        return this.solicitudId;
    }

    public void setSolicitudId(Long solicitudId)
    {
        this.solicitudId = solicitudId;
    }

    public String getSubcausa()
    {
        return this.subcausa;
    }

    public void setSubcausa(String subcausa)
    {
        this.subcausa = subcausa;
    }

    public String getTanda()
    {
        return this.tanda;
    }

    public void setTanda(String tanda)
    {
        this.tanda = tanda;
    }

    public static List<ResolucionDenegacion> getDenegacionesByMultienvioAndBeca(Long multiEnvioId,
            Long becaId)
    {
        return resolucionDenegacionDAO.getDenegacionesByMultienvioAndBeca(multiEnvioId, becaId);
    }
}