package es.uji.apps.bec.dao.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QRegistroErroresEnvio;
import es.uji.apps.bec.model.QTanda;
import es.uji.apps.bec.model.RegistroErroresEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class RegistroErroresEnvioDAODatabaseImpl extends BaseDAODatabaseImpl implements
        RegistroErroresEnvioDAO
{
    @Override
    public List<RegistroErroresEnvio> getErroresEnvio(Map<String, String> filtros)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTanda tanda = QTanda.tanda;
        QRegistroErroresEnvio registroErroresEnvio = QRegistroErroresEnvio.registroErroresEnvio;
        
        BooleanBuilder condicionesFiltro = preparaCondicionesWhere(filtros);

        query.from(registroErroresEnvio).join(registroErroresEnvio.tanda, tanda);

        if (condicionesFiltro.hasValue())
        {
            query.where(condicionesFiltro);
        }
        
        query.orderBy(registroErroresEnvio.tanda.id.asc());

        if (query.list(registroErroresEnvio).size() > 0)
        {
            return query.list(registroErroresEnvio);
        }

        return null;
    }

    private BooleanBuilder preparaCondicionesWhere(Map<String, String> filtros)
    {
        Long tandaId = ParamUtils.parseLong(filtros.get("tandaId"));
        Long becaId = ParamUtils.parseLong(filtros.get("becaId"));
        
        BooleanBuilder condicionesFiltro = new BooleanBuilder();
        addFiltroTanda(condicionesFiltro, tandaId);
        addFiltroBeca(condicionesFiltro, becaId);
        return condicionesFiltro;
    }

    private void addFiltroTanda(BooleanBuilder condicionesFiltro, Long tandaId)
    {
        if (tandaId != null)
        {
            condicionesFiltro.and(QRegistroErroresEnvio.registroErroresEnvio.tanda.tandaId.eq(tandaId));
        }
    }

    private void addFiltroBeca(BooleanBuilder condicionesFiltro, Long becaId)
    {
        if (becaId != null)
        {
            condicionesFiltro.and(QRegistroErroresEnvio.registroErroresEnvio.beca.id.eq(becaId));
        }
    }
}
