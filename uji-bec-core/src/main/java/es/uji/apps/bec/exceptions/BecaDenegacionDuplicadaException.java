package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BecaDenegacionDuplicadaException extends CoreDataBaseException
{
    public BecaDenegacionDuplicadaException()
    {
        super("No es poden inserir dues denegacions iguals amb el mateix nombre d\'ordre per a la mateixa beca");
    }

    public BecaDenegacionDuplicadaException(String message)
    {
        super(message);
    }
}