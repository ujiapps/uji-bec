package es.uji.apps.bec.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FicheroConselleriaManuelaSolisDenegadas {
    private Long codigoArchivoTemporal;
    private String nif;
    private String apellido1;
    private String apellido2;
    private String nombre;
    private List<String> denegaciones;

    public FicheroConselleriaManuelaSolisDenegadas() {
    }

    public FicheroConselleriaManuelaSolisDenegadas(String codigoArchivoTemporal, String nif, String apellido1, String apellido2, String nombre, String denegaciones) {
        this.codigoArchivoTemporal = Long.parseLong(codigoArchivoTemporal);
        this.nif = nif;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.nombre = nombre;
        this.denegaciones = extraeDenegaciones(denegaciones);
    }

    public FicheroConselleriaManuelaSolisDenegadas(String linea) {
        String[] campos = linea.trim().split(";");

        this.codigoArchivoTemporal = Long.parseLong(campos[0]);
        this.nif = campos[1];
        this.apellido1 = campos[2];
        this.apellido2 = campos[3];
        this.nombre = campos[4];
        this.denegaciones = extraeDenegaciones(campos[5]);
    }

    private List<String> extraeDenegaciones(String denegaciones) {
        String[] listaDenegacionesString = denegaciones.split(",");
        return Arrays.asList(listaDenegacionesString);
    }

    public Long getCodigoArchivoTemporal() {
        return codigoArchivoTemporal;
    }

    public void setCodigoArchivoTemporal(Long codigoArchivoTemporal) {
        this.codigoArchivoTemporal = codigoArchivoTemporal;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getDenegaciones() {
        return denegaciones;
    }

    public void setDenegaciones(List<String> denegaciones) {
        this.denegaciones = denegaciones;
    }
}
