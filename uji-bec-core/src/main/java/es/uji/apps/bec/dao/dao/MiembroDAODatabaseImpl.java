package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.QMiembro;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.Miembro;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MiembroDAODatabaseImpl extends BaseDAODatabaseImpl implements MiembroDAO
{
    private QMiembro qMiembro = QMiembro.miembro;
    private static final long TIPO_MIEMBRO_SOLICITANTE = 1L;

    @Override
    public Miembro getMiembroById(Long miembroId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qMiembro).where(qMiembro.id.eq(miembroId));
        return query.uniqueResult(qMiembro);
    }

    @Override
    public List<Miembro> getMiembrosByBeca(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qMiembro).where(qMiembro.beca.id.eq(becaId));
        return query.list(qMiembro);
    }

    @Override
    public Miembro getMiembroSolicitante(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qMiembro).where(qMiembro.beca.id.eq(becaId).and(
                qMiembro.tipoMiembro.id.eq(TIPO_MIEMBRO_SOLICITANTE)));
        List<Miembro> listaMiembros = query.list(qMiembro);
        if (listaMiembros.size() > 0)
        {
            return listaMiembros.get(0);
        }
        return new Miembro();
    }

    @Override
    @Transactional
    public void updateMiembro(Miembro miembro)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qMiembro);
        updateClause
                .where(qMiembro.id.eq(miembro.getId()))
                .set(qMiembro.tipoMiembro, miembro.getTipoMiembro())
                .set(qMiembro.nombre, miembro.getNombre())
                .set(qMiembro.apellido1, miembro.getApellido1())
                .set(qMiembro.apellido2, miembro.getApellido2())
                .set(qMiembro.tipoSustentador, miembro.getTipoSustentador())
                .set(qMiembro.tipoIdentificacion, miembro.getTipoIdentificacion())
                .set(qMiembro.identificacion, miembro.getIdentificacion())
                .set(qMiembro.fechaCadNif, miembro.getFechaCadNif())
                .set(qMiembro.identificacionIdesp, miembro.getIdentificacionIdesp())
                .set(qMiembro.fechaNacimiento, miembro.getFechaNacimiento())
                .set(qMiembro.tipoSexo, miembro.getTipoSexo())
                .set(qMiembro.estadoCivil, miembro.getEstadoCivil())
                .set(qMiembro.noValidaIdentificacion, miembro.isNoValidaIdentificacion())
                .set(qMiembro.pais, miembro.getPais())
                .set(qMiembro.padronProvinciaId, miembro.getPadronProvinciaId())
                .set(qMiembro.padronLocalidadId, miembro.getPadronLocalidadId())
                .set(qMiembro.situacionLaboral, miembro.getSituacionLaboral())
                .set(qMiembro.localidadTrabajo, miembro.getLocalidadTrabajo())
                .set(qMiembro.profesion, miembro.getProfesion())
                .set(qMiembro.tipoMinusvalia, miembro.getTipoMinusvalia())
                .set(qMiembro.fechaResMinus, miembro.getFechaResMinus())
                .set(qMiembro.fechaFinMinus, miembro.getFechaFinMinus())
                .set(qMiembro.residencia, miembro.isResidencia())
                .set(qMiembro.porcentajeActividadesEconomicas, miembro.getPorcentajeActividadesEconomicas())
                .set(qMiembro.importeActividadesEconomicas, miembro.getImporteActividadesEconomicas())
                .set(qMiembro.importeRentasExtranjero, miembro.getImporteRentasExtranjero())
                .set(qMiembro.tipoMoneda, miembro.getTipoMoneda())
                .set(qMiembro.ccaaMinus, miembro.getCcaaMinus())
                .set(qMiembro.custodia, miembro.isCustodia())
                .set(qMiembro.indDeclRenta, miembro.isIndDeclRenta())
                .set(qMiembro.indRentasEspanna, miembro.isIndRentasEspanna())
                .set(qMiembro.indRentasNavarra, miembro.isIndRentasNavarra())
                .set(qMiembro.indRentasPaisVasco, miembro.isIndRentasPaisVasco())
                .set(qMiembro.custodiaCompartida, miembro.isCustodiaCompartida())
                .set(qMiembro.firmaSolicitud, miembro.isFirmaSolicitud())
                .set(qMiembro.estudiaFuera, miembro.isEstudiaFuera())
                .set(qMiembro.codigoUnivHermano, miembro.getCodigoUnivHermano())
                .set(qMiembro.codigoEstudioHermano, miembro.getCodigoEstudioHermano())
                .set(qMiembro.indConvivePareja, miembro.isIndConvivePareja())
                .set(qMiembro.indIngresosPropios, miembro.isIndIngresosPropios())
                .set(qMiembro.indRevMinusMec, miembro.isIndRevMinusMec())
                .set(qMiembro.indRevMinusUt, miembro.isIndRevMinusUt())
                .set(qMiembro.indArrenUnidFamiliar, miembro.isIndArrenUnidFamiliar())
                .set(qMiembro.indPensionCompensatoria, miembro.getIndPensionCompensatoria())
                .set(qMiembro.indHijosMayores25, miembro.getIndHijosMayores25())
                .set(qMiembro.nifArren, miembro.getNifArren())
                .set(qMiembro.precioAlquiler, miembro.getPrecioAlquiler())
                .execute();
    }

    @Override
    @Transactional
    public void deleteMiembro(Long miembroId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qMiembro);
        delete.where(qMiembro.id.eq(miembroId)).execute();
    }
}
