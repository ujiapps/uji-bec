package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.EstadoCivil;
import es.uji.commons.db.BaseDAO;

public interface EstadoCivilDAO extends BaseDAO
{
    List<EstadoCivil> getEstadosCiviles();

    EstadoCivil getEstadoCivilById(Long estadoCivilId);

    void deleteEstadoCivil(Long estadoCivilId);
}