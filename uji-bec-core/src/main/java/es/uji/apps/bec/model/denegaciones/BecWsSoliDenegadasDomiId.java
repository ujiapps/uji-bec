package es.uji.apps.bec.model.denegaciones;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
public class BecWsSoliDenegadasDomiId implements Serializable
{
    private BigDecimal domicloc;
    private String domicoddomicilio;
    private String domicodpostal;
    private String domicodvia;
    private String domicprov;
    private String domidomicilio;
    private String domiindespana;
    private String domilocalidad;
    private String dominumero;
    private String domipais;

    public BigDecimal getDomicloc()
    {
        return domicloc;
    }

    public void setDomicloc(BigDecimal domicloc)
    {
        this.domicloc = domicloc;
    }

    public String getDomicoddomicilio()
    {
        return domicoddomicilio;
    }

    public void setDomicoddomicilio(String domicoddomicilio)
    {
        this.domicoddomicilio = domicoddomicilio;
    }

    public String getDomicodpostal()
    {
        return domicodpostal;
    }

    public void setDomicodpostal(String domicodpostal)
    {
        this.domicodpostal = domicodpostal;
    }

    public String getDomicodvia()
    {
        return domicodvia;
    }

    public void setDomicodvia(String domicodvia)
    {
        this.domicodvia = domicodvia;
    }

    public String getDomicprov()
    {
        return domicprov;
    }

    public void setDomicprov(String domicprov)
    {
        this.domicprov = domicprov;
    }

    public String getDomidomicilio()
    {
        return domidomicilio;
    }

    public void setDomidomicilio(String domidomicilio)
    {
        this.domidomicilio = domidomicilio;
    }

    public String getDomiindespana()
    {
        return domiindespana;
    }

    public void setDomiindespana(String domiindespana)
    {
        this.domiindespana = domiindespana;
    }

    public String getDomilocalidad()
    {
        return domilocalidad;
    }

    public void setDomilocalidad(String domilocalidad)
    {
        this.domilocalidad = domilocalidad;
    }

    public String getDominumero()
    {
        return dominumero;
    }

    public void setDominumero(String dominumero)
    {
        this.dominumero = dominumero;
    }

    public String getDomipais()
    {
        return domipais;
    }

    public void setDomipais(String domipais)
    {
        this.domipais = domipais;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((domicloc == null) ? 0 : domicloc.hashCode());
        result = prime * result + ((domicoddomicilio == null) ? 0 : domicoddomicilio.hashCode());
        result = prime * result + ((domicodpostal == null) ? 0 : domicodpostal.hashCode());
        result = prime * result + ((domicodvia == null) ? 0 : domicodvia.hashCode());
        result = prime * result + ((domicprov == null) ? 0 : domicprov.hashCode());
        result = prime * result + ((domidomicilio == null) ? 0 : domidomicilio.hashCode());
        result = prime * result + ((domiindespana == null) ? 0 : domiindespana.hashCode());
        result = prime * result + ((domilocalidad == null) ? 0 : domilocalidad.hashCode());
        result = prime * result + ((dominumero == null) ? 0 : dominumero.hashCode());
        result = prime * result + ((domipais == null) ? 0 : domipais.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BecWsSoliDenegadasDomiId other = (BecWsSoliDenegadasDomiId) obj;
        if (domicloc == null)
        {
            if (other.domicloc != null)
                return false;
        }
        else if (!domicloc.equals(other.domicloc))
            return false;
        if (domicoddomicilio == null)
        {
            if (other.domicoddomicilio != null)
                return false;
        }
        else if (!domicoddomicilio.equals(other.domicoddomicilio))
            return false;
        if (domicodpostal == null)
        {
            if (other.domicodpostal != null)
                return false;
        }
        else if (!domicodpostal.equals(other.domicodpostal))
            return false;
        if (domicodvia == null)
        {
            if (other.domicodvia != null)
                return false;
        }
        else if (!domicodvia.equals(other.domicodvia))
            return false;
        if (domicprov == null)
        {
            if (other.domicprov != null)
                return false;
        }
        else if (!domicprov.equals(other.domicprov))
            return false;
        if (domidomicilio == null)
        {
            if (other.domidomicilio != null)
                return false;
        }
        else if (!domidomicilio.equals(other.domidomicilio))
            return false;
        if (domiindespana == null)
        {
            if (other.domiindespana != null)
                return false;
        }
        else if (!domiindespana.equals(other.domiindespana))
            return false;
        if (domilocalidad == null)
        {
            if (other.domilocalidad != null)
                return false;
        }
        else if (!domilocalidad.equals(other.domilocalidad))
            return false;
        if (dominumero == null)
        {
            if (other.dominumero != null)
                return false;
        }
        else if (!dominumero.equals(other.dominumero))
            return false;
        if (domipais == null)
        {
            if (other.domipais != null)
                return false;
        }
        else if (!domipais.equals(other.domipais))
            return false;
        return true;
    }
}
