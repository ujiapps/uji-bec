package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class NotificacionesException extends GeneralBECException
{
    public NotificacionesException()
    {
        super("Error a l'intentar enviar les notificacions");
    }

    public NotificacionesException(String message)
    {
        super(message);
    }
}
