package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ProfesionDAO;

/**
 * The persistent class for the BC2_PROFESIONES database table.
 */
@Entity
@Component
@Table(name = "BC2_PROFESIONES")
public class Profesion implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static ProfesionDAO profesionDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private Long orden;
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "profesion")
    private Set<Miembro> miembros;

    public Profesion()
    {
    }

    public static Profesion getProfesionById(Long profesionId)
    {
        return profesionDAO.getProfesionById(profesionId).get(0);
    }

    @Autowired
    public void setTipoProfesionDAO(ProfesionDAO profesionDAO)
    {
        Profesion.profesionDAO = profesionDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }
}