package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.Mensaje;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface MensajeDAO extends BaseDAO
{
    List<Mensaje> getMensajesByBecaId(Long becaId);
}
