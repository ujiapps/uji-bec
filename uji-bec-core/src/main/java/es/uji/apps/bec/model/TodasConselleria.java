package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.TodasConselleriaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the BC2_EXT_TODAS_CONSELLERIA database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_EXT_TODAS_CONSELLERIA")
public class TodasConselleria implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BECA_ID")
    private long becaId;

    private String apellido1;

    private String apellido2;

    private Long ciclo;

    @Column(name = "CODIGO_CENTRO")
    private String codigoCentro;

    @Column(name = "NOMBRE_CENTRO")
    private String nombreCentro;

    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CREDITOS_CUBIERTOS")
    private BigDecimal creditosCubiertos;

    @Column(name = "CREDITOS_MATRICULADOS")
    private BigDecimal creditosMatriculados;

    private Long curso;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoAcademicoId;

    @Column(name = "ESTADO_ID")
    private Long estadoId;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "NOMBRE_ESTUDIO")
    private String nombreEstudio;

    @Column(name = "ESTUDIO_ID_MEC")
    private String estudioIdMec;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    private String localidad;

    @Column(name = "LOCALIDAD_ID")
    private String localidadId;

    private String nif;

    private String nombre;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "PROCESO_ID")
    private Long procesoId;

    private Long sexo;

    @Column(name = "SOLICITANTE_ID")
    private Long solicitanteId;

    @Column(name = "TASAS_TOTAL")
    private BigDecimal tasasTotal;

    @Column(name = "BECA_MEC_CONCEDIDA")
    private Boolean becaMecConcedida;

    @Column(name = "CREDITOS_1A")
    private BigDecimal creditos1a;

    @Column(name = "IMPORTE_1A")
    private BigDecimal importe1a;

    @Column(name = "CREDITOS_2A")
    private BigDecimal creditos2a;

    @Column(name = "IMPORTE_2A")
    private BigDecimal importe2a;

    @Column(name = "CREDITOS_3A")
    private BigDecimal creditos3a;

    @Column(name = "IMPORTE_3A")
    private BigDecimal importe3a;

    @Column(name = "CREDITOS_3AP")
    private BigDecimal creditos3ap;

    @Column(name = "IMPORTE_3AP")
    private BigDecimal importe3ap;

    @Column(name = "CREDITOS_4AP")
    private BigDecimal creditos4ap;

    @Column(name = "IMPORTE_4AP")
    private BigDecimal importe4ap;

    @Column(name = "NOTA_MEDIA")
    private BigDecimal notaMedia;

    private Long tipobeca;

    private Long den1;
    private Long den2;
    private Long den3;
    private Long den4;
    private Long den5;
    private Long den6;


    private String ut;

    private static TodasConselleriaDAO todasConselleriaDAO;

    @Autowired
    public void setTodasConselleria(TodasConselleriaDAO todasConselleriaDAO)
    {
        TodasConselleria.todasConselleriaDAO = todasConselleriaDAO;
    }

    public TodasConselleria()
    {
    }

    public long getBecaId()
    {
        return this.becaId;
    }

    public void setBecaId(long becaId)
    {
        this.becaId = becaId;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return this.apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public Long getCiclo()
    {
        return this.ciclo;
    }

    public void setCiclo(Long ciclo)
    {
        this.ciclo = ciclo;
    }

    public String getCodigoCentro()
    {
        return this.codigoCentro;
    }

    public void setCodigoCentro(String codigoCentro)
    {
        this.codigoCentro = codigoCentro;
    }

    public String getNombreCentro()
    {
        return this.nombreCentro;
    }

    public void setNombreCentro(String nombreCentro)
    {
        this.nombreCentro = nombreCentro;
    }

    public String getCodigoPostal()
    {
        return this.codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }

    public Long getConvocatoriaId()
    {
        return this.convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public BigDecimal getCreditosCubiertos()
    {
        return this.creditosCubiertos;
    }

    public void setCreditosCubiertos(BigDecimal creditosCubiertos)
    {
        this.creditosCubiertos = creditosCubiertos;
    }

    public BigDecimal getCreditosMatriculados()
    {
        return this.creditosMatriculados;
    }

    public void setCreditosMatriculados(BigDecimal creditosMatriculados)
    {
        this.creditosMatriculados = creditosMatriculados;
    }

    public Long getCurso()
    {
        return this.curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getCursoAcademicoId()
    {
        return this.cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Long getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(Long estadoId)
    {
        this.estadoId = estadoId;
    }

    public Long getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombreEstudio()
    {
        return this.nombreEstudio;
    }

    public void setNombreEstudio(String nombreEstudio)
    {
        this.nombreEstudio = nombreEstudio;
    }


    public String getEstudioIdMec()
    {
        return this.estudioIdMec;
    }

    public void setEstudioIdMec(String estudioIdMec)
    {
        this.estudioIdMec = estudioIdMec;
    }

    public Date getFechaNacimiento()
    {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento)
    {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getLocalidad()
    {
        return this.localidad;
    }

    public void setLocalidad(String localidad)
    {
        this.localidad = localidad;
    }

    public String getLocalidadId()
    {
        return this.localidadId;
    }

    public void setLocalidadId(String localidadId)
    {
        this.localidadId = localidadId;
    }

    public String getNif()
    {
        return this.nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getProcesoId()
    {
        return this.procesoId;
    }

    public void setProcesoId(Long procesoId)
    {
        this.procesoId = procesoId;
    }

    public Long getSexo()
    {
        return this.sexo;
    }

    public void setSexo(Long sexo)
    {
        this.sexo = sexo;
    }

    public Long getSolicitanteId()
    {
        return this.solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId)
    {
        this.solicitanteId = solicitanteId;
    }

    public BigDecimal getTasasTotal()
    {
        return this.tasasTotal;
    }

    public void setTasasTotal(BigDecimal tasasTotal)
    {
        this.tasasTotal = tasasTotal;
    }

    public Long getTipobeca()
    {
        return this.tipobeca;
    }

    public void setTipobeca(Long tipobeca)
    {
        this.tipobeca = tipobeca;
    }

    public String getUt()
    {
        return this.ut;
    }

    public void setUt(String ut)
    {
        this.ut = ut;
    }

    public static List<TodasConselleria> getDatosByCursoAcademicoId(Long cursoAcademicoId)
    {
        return todasConselleriaDAO.getDatosByCursoAcademicoId(cursoAcademicoId);
    }

    public static List<TodasConselleria> getDatosDenegadasByCursoAcademicoId(Long cursoAcademicoId)
    {
        return todasConselleriaDAO.getDatosDenegadasByCursoAcademicoId(cursoAcademicoId);
    }

    public static List<TodasConselleria> getDatosConcedidasByCursoAcademicoId(Long cursoAcademicoId)
    {
        return todasConselleriaDAO.getDatosConcedidasByCursoAcademicoId(cursoAcademicoId);
    }

    public Long getDen1() {
        return den1;
    }

    public void setDen1(Long den1) {
        this.den1 = den1;
    }

    public Long getDen2() {
        return den2;
    }

    public void setDen2(Long den2) {
        this.den2 = den2;
    }

    public Long getDen3() {
        return den3;
    }

    public void setDen3(Long den3) {
        this.den3 = den3;
    }

    public Long getDen4() {
        return den4;
    }

    public void setDen4(Long den4) {
        this.den4 = den4;
    }

    public Long getDen5() {
        return den5;
    }

    public void setDen5(Long den5) {
        this.den5 = den5;
    }

    public Long getDen6() {
        return den6;
    }

    public void setDen6(Long den6) {
        this.den6 = den6;
    }

    public Boolean isBecaMecConcedida() {
        return becaMecConcedida;
    }

    public void setBecaMecConcedida(Boolean becaMecConcedida) {
        this.becaMecConcedida = becaMecConcedida;
    }

    public BigDecimal getCreditos1a() {
        return creditos1a;
    }

    public BigDecimal getImporte1a() {
        return this.importe1a;
    }

    public BigDecimal getCreditos2a() {
        return creditos2a;
    }

    public BigDecimal getImporte2a() {
        return this.importe2a;
    }

    public BigDecimal getCreditos3a() { return creditos3a; }

    public BigDecimal getImporte3a() { return this.importe3a; }

    public BigDecimal getCreditos3ap() { return creditos3ap; }

    public BigDecimal getImporte3ap() { return this.importe3ap; }

    public BigDecimal getCreditos4ap() { return creditos4ap; }

    public BigDecimal getImporte4ap() { return this.importe4ap; }

    public BigDecimal getNotaMedia() { return this.notaMedia;}

}

