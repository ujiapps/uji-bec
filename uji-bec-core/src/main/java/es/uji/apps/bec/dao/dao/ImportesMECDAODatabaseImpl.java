package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.ImportesMEC;
import es.uji.apps.bec.model.QImportesMEC;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ImportesMECDAODatabaseImpl extends BaseDAODatabaseImpl implements ImportesMECDAO
{
    @Override
    public List<ImportesMEC> getImportesMECByCursoAcademicoId(Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);

    }

    @Override
    public List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(Long convocatoriaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.convocatoriaId.eq(convocatoriaId).and(
                        qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId)).and(
                        qImportesMEC.tipoEstudio.eq("G")))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);
    }

    @Override
    public List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(Long convocatoriaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.convocatoriaId.eq(convocatoriaId).and(
                        qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId)).and(
                        qImportesMEC.tipoEstudio.eq("M")))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);
    }

    @Override
    public List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoId(Long convocatoriaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.convocatoriaId.eq(convocatoriaId).and(
                        qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId)))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);
    }

    @Override
    public List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdIncidencias(Long convocatoriaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.convocatoriaId.eq(convocatoriaId).and(
                        qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId)))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);
    }

    @Override
    public List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdFamiliasNumerosas(Long convocatoriaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QImportesMEC qImportesMEC = QImportesMEC.importesMEC;

        query.from(qImportesMEC)
                .where(qImportesMEC.convocatoriaId.in(1L, 2L).and(
                        qImportesMEC.cursoAcademicoId.eq(cursoAcademicoId)).and(
                        qImportesMEC.tipoFamiliaId.ne(1L)))
                .orderBy(qImportesMEC.apellido1.asc(), qImportesMEC.apellido2.asc());

        return query.list(qImportesMEC);
    }


}
