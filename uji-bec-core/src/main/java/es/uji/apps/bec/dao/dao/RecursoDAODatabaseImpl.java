package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.QBeca;
import es.uji.apps.bec.model.QRecurso;
import es.uji.apps.bec.model.Recurso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class RecursoDAODatabaseImpl extends BaseDAODatabaseImpl implements RecursoDAO
{
    private QRecurso qRecurso = QRecurso.recurso;

    @Override
    public List<Recurso> getRecursosByTandaId(Long tandaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBeca beca = QBeca.beca;
        query.from(qRecurso).join(qRecurso.beca, beca).where(beca.tanda.id.eq(tandaId));
        return query.list(qRecurso);
    }

    @Override
    public Recurso getRecursoByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qRecurso).where(qRecurso.beca.id.eq(becaId));
        return query.uniqueResult(qRecurso);
    }

    @Override
    @Transactional
    public void update(Recurso recurso)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qRecurso);

        updateClause.where(qRecurso.id.eq(recurso.getId()))
                .set(qRecurso.beca, recurso.getBeca())
                .set(qRecurso.circunstanciasTransporte, recurso.getCircunstanciasTransporte())
                .set(qRecurso.cursoActAniosBecario, recurso.getCursoActAniosBecario())
                .set(qRecurso.cursoActCreditos, recurso.getCursoActCreditos())
                .set(qRecurso.cursoActCreditosMatr, recurso.getCursoActCreditosMatr())
                .set(qRecurso.cursoActCursoCompleto, recurso.getCursoActCursoCompleto())
                .set(qRecurso.cursoActCursoPosterior, recurso.getCursoActCursoPosterior())
                .set(qRecurso.cursoActCursoPosteriorTxt, recurso.getCursoActCursoPosteriorTxt())
                .set(qRecurso.cursoActPerdidaCurso, recurso.getCursoActPerdidaCurso())
                .set(qRecurso.cursoActPerdidaCursoTxt, recurso.getCursoActPerdidaCursoTxt())
                .set(qRecurso.cursoAntEstudio, recurso.getCursoAntEstudio())
                .set(qRecurso.cursoAntCompleto, recurso.getCursoAntCompleto())
                .set(qRecurso.cursoAntCreditos, recurso.getCursoAntCreditos())
                .set(qRecurso.cursoAntCreditosMatr, recurso.getCursoAntCreditosMatr())
                .set(qRecurso.cursoAntCreditosPend, recurso.getCursoAntCreditosPend())
                .set(qRecurso.cursoAntCreditosPendMax, recurso.getCursoAntCreditosPendMax())
                .set(qRecurso.cursoAntNotaMedia, recurso.getCursoAntNotaMedia())
                .set(qRecurso.cursoAntNotaMediaSin, recurso.getCursoAntNotaMediaSin())
                .set(qRecurso.cursoAntEstudioRama, recurso.getCursoAntEstudioRama())
                .set(qRecurso.deducciones, recurso.getDeducciones())
                .set(qRecurso.distancia, recurso.getDistancia())
                .set(qRecurso.existeCentroMasCercano, recurso.getExisteCentroMasCercano())
                .set(qRecurso.ingresosFamiliares, recurso.getIngresosFamiliares())
                .set(qRecurso.otrasBecas, recurso.getOtrasBecas())
                .set(qRecurso.otrasCircunstancias, recurso.getOtrasCircunstancias())
                .set(qRecurso.porcentajeIncrementoUmbral, recurso.getPorcentajeIncrementoUmbral())
                .set(qRecurso.rentaFamiliarDisponible, recurso.getRentaFamiliarDisponible())
                .set(qRecurso.rentaFamiliarPerCapita, recurso.getRentaFamiliarPerCapita())
                .set(qRecurso.superaUmbrales, recurso.getSuperaUmbrales())
                .set(qRecurso.superaUmbralesCuantia, recurso.getSuperaUmbralesCuantia())
                .set(qRecurso.superaUmbralesTexto, recurso.getSuperaUmbralesTexto())
                .set(qRecurso.textoAyudasReclamadas, recurso.getTextoAyudasReclamadas())
                .set(qRecurso.textoJustificacion, recurso.getTextoJustificacion())
                .set(qRecurso.textoPropuesta, recurso.getTextoPropuesta())
                .set(qRecurso.miembrosComputables, recurso.getMiembrosComputables())
                .set(qRecurso.causasDesestimacion, recurso.getCausasDesestimacion())
                .set(qRecurso.estimacion, recurso.getEstimacion())
                .set(qRecurso.cursoActCambioEstudios, recurso.getCursoActCambioEstudios())
                .set(qRecurso.ayudasActuales, recurso.getAyudasActuales())
                .set(qRecurso.denegacionesActuales, recurso.getDenegacionesActuales())
                .execute();
    }
}
