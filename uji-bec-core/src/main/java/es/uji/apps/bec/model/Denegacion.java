package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.DenegacionDAO;

/**
 * The persistent class for the BC2_DENEGACIONES database table.
 */
@Entity
@Component
@Table(name = "BC2_DENEGACIONES")
public class Denegacion implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    private Boolean activa;

    private String estado;

    private String causa;

    private String subCausa;

    // bi-directional many-to-one association to BecaDenegacion
    @OneToMany(mappedBy = "denegacion")
    private Set<BecaDenegacion> becasDenegaciones;

    // bi-directional many-to-one association to Organismo
    @ManyToOne
    private Organismo organismo;

    private static DenegacionDAO denegacionDAO;

    @Autowired
    public void setDenegacionDAO(DenegacionDAO denegacionDAO)
    {
        Denegacion.denegacionDAO = denegacionDAO;
    }

    public Denegacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean isActiva()
    {
        return this.activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getCausa()
    {
        return causa;
    }

    public void setCausa(String causa)
    {
        this.causa = causa;
    }

    public String getSubCausa()
    {
        return subCausa;
    }

    public void setSubCausa(String subCausa)
    {
        this.subCausa = subCausa;
    }

    public Set<BecaDenegacion> getBecasDenegaciones()
    {
        return this.becasDenegaciones;
    }

    public void setBecasDenegaciones(Set<BecaDenegacion> becasDenegaciones)
    {
        this.becasDenegaciones = becasDenegaciones;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public void setOrganismo(Organismo organismo)
    {
        this.organismo = organismo;
    }

    public static List<Denegacion> getDenegacionesActivasByOrganismo(Long organismoId)
    {
        return denegacionDAO.getDenegacionesActivasByOrganismo(organismoId);
    }

    public static List<Denegacion> getDenegacionesTodasByOrganismo(Long organismoId)
    {
        return denegacionDAO.getDenegacionesTodasByOrganismo(organismoId);
    }

    public static Denegacion getDenegacionById(Long id)
    {
        return denegacionDAO.get(Denegacion.class, id).get(0);
    }

    public static Denegacion getDenegacionByOrganismoEstadoCausaySubcausa(Long organismoId, String estado, String causa, String subcausa)
    {
        return denegacionDAO.getDenegacionByOrganismoEstadoCausaySubcausa(organismoId, estado, causa, subcausa);
    }

    public static Denegacion getDenegacionByOrganismoYCausa(Long organismoId, String causa)
    {
        return denegacionDAO.getDenegacionByOrganismoYCausa(organismoId, causa);
    }

    @Transactional
    public Denegacion insert()
    {
        return denegacionDAO.insert(this);
    }

    public String getCodigoConselleria()
    {
        return String.format("%1$3s", getCausa().replace(".", "")).replace(" ", "0");
    }
}