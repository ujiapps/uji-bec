package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class EstadoBecaNoPermiteCalcularDenegacionesException extends CoreDataBaseException
{
    public EstadoBecaNoPermiteCalcularDenegacionesException()
    {
        super("No es poden calcular les denegacions en l\'estat actual de la beca.");
    }

    public EstadoBecaNoPermiteCalcularDenegacionesException(String message)
    {
        super(message);
    }
}
