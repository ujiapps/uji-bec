package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.ImportesMEC;
import es.uji.commons.db.BaseDAO;

public interface ImportesMECDAO extends BaseDAO
{
    List<ImportesMEC> getImportesMECByCursoAcademicoId(Long cursoAcademicoId);

    List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(Long convocatoriaId, Long cursoAcademicoId);

    List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(Long convocatoriaId, Long cursoAcademicoId);

    List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoId(Long convocatoriaId, Long cursoAcademicoId);

    List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdIncidencias(Long convocatoriaId, Long cursoAcademicoId);

    List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdFamiliasNumerosas(Long convocatoriaId, Long cursoAcademicoId);

}
