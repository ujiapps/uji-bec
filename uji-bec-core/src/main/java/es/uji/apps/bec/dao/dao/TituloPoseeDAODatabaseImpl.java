package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.QTituloPosee;
import es.uji.apps.bec.model.TituloPosee;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TituloPoseeDAODatabaseImpl extends BaseDAODatabaseImpl implements TituloPoseeDAO
{
    private QTituloPosee qTituloPosee = QTituloPosee.tituloPosee;

    @Override
    public List<TituloPosee> getTitulosPoseeBySolicitante(Long solicitanteId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTituloPosee).where(qTituloPosee.solicitante.id.eq(solicitanteId));
        return query.list(qTituloPosee);
    }
}
