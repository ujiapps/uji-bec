package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorTipoDomicilio extends Mapeador<Long, Object>
{
    public MapeadorTipoDomicilio()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "FAM");
        valores.put(CodigoOrganismo.CONSELLERIA, "FAM");
        add(10L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, "RES");
        valores.put(CodigoOrganismo.CONSELLERIA, "RES");
        add(20L, valores);
    }
}
