package es.uji.apps.bec.webservices;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import es.uji.apps.bec.dao.dao.BecaDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType;

@Component
public class DatosSolicitudes {
    private static Logger log = Logger.getLogger(DatosSolicitudes.class);

    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    public DatosSolicitudes() {
    }

    @Transactional
    public void cargar(Long tandaId)
            throws WsErrorAlPrepararMultienvioException, ServiceException, JAXBException, IOException, GeneralBECException, SOAPException {
        MultienvioType multienvio = getMultienvioType(tandaId);
        String result = service.cargarXmlSolicitudesBase64(multienvio);
        log.info(result);
    }

    @Transactional
    public String getXmlMultienvio(Long tandaId) throws WsErrorAlPrepararMultienvioException, JAXBException {
        MultienvioType multienvio = getMultienvioType(tandaId);
        return service.multiEnvioToString(multienvio);
    }

    private MultienvioType getMultienvioType(Long tandaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = getRegistroEnvio(tandaId);
        Long multiEnvioId = registroEnvio.getEnvioId();
        List<MultienvioType> multienvioTypes = becaDAO.get(MultienvioType.class, multiEnvioId);
        return multienvioTypes.get(0);
    }

    private RegistroEnvio getRegistroEnvio(Long tandaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tandaId);

        if (registroEnvio == null) {
            throw new WsErrorAlPrepararMultienvioException("L\'enviament no està preparat.");
        }

        if (registroEnvio.isIncorrecto()) {
            throw new WsErrorAlPrepararMultienvioException("La preparació de l\'enviament és incorrecta.");
        }

        if (registroEnvio.isPendiente()) {
            throw new WsErrorAlPrepararMultienvioException("Les dades de l\'enviament estan ja generant-se. Espereu...");
        }
        return registroEnvio;
    }
}