package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class TandaDuplicadaParaConvocatoriaAndCursoAcaException extends GeneralBECException
{
    public TandaDuplicadaParaConvocatoriaAndCursoAcaException()
    {
        super("Ja existeix aquesta tanda per al mateix curs i convocatoria.");
    }

    public TandaDuplicadaParaConvocatoriaAndCursoAcaException(String message)
    {
        super(message);
    }
}
