package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Domicilio;
import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.commons.db.BaseDAO;

public interface DomicilioDAO extends BaseDAO
{
    Domicilio getDomicilioById(Long domicilioId);

    List<Domicilio> getDomiciliosByBecaId(Long becaId);

    Domicilio getDomicilioByBecaAndTipo(Beca beca, TipoDomicilio tipoDomicilio);

    Domicilio getDomicilioResidencia(Long becaId);

    Domicilio getDomicilioFamiliar(Long becaId);

    void updateDomicilio(Domicilio domicilio);

    void borraDomicilio(Long domicilioId);
}
