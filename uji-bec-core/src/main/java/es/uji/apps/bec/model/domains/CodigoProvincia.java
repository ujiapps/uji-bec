package es.uji.apps.bec.model.domains;

public enum CodigoProvincia
{
    BALEARES(7L),

    LASPALMAS(35L),

    SANTACRUZ(38L),

    CEUTA(51L),

    MELILLA(52L),

    CASTELLON(12L),

    VALENCIA(46L),

    ALICANTE(3L);

    private Long id;

    CodigoProvincia(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public boolean igual(Object otherObject)
    {
        if (otherObject instanceof Long)
        {
            return ((Long) otherObject) == getId();
        }

        return super.equals(otherObject);
    }
}