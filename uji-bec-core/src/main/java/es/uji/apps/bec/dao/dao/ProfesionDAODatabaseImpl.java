package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Profesion;
import es.uji.apps.bec.model.QProfesion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProfesionDAODatabaseImpl extends BaseDAODatabaseImpl implements ProfesionDAO
{

    @Override
    public List<Profesion> getProfesiones()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QProfesion profesion = QProfesion.profesion;
        query.from(profesion).orderBy(profesion.nombre.asc());

        return query.list(profesion);
    }

    @Override
    public List<Profesion> getProfesionById(Long profesionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QProfesion profesion = QProfesion.profesion;
        query.from(profesion).where(profesion.id.eq(profesionId));
        return query.list(profesion);
    }

}
