package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.commons.db.BaseDAO;

public interface RegistroEnvioDAO extends BaseDAO
{
    RegistroEnvio getRegistroEnvioByTanda(Long tandaId);
}