package es.uji.apps.bec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ResolucionEconomicoDAO;

@Component
@Entity
@Table(name = "BC2_V_RESOL_ECONOMICOS")
public class ResolucionEconomico
{
    @Id
    private Long id;
    @Column(name = "ECONCAPITAL")
    private Float capitalMobiliario;
    @Column(name = "ECONDEDUCCIONES")
    private Float deducciones;
    @Column(name = "econNegocio")
    private Float volumenNegocio;
    @Column(name = "econRenta")
    private Float renta;
    @Column(name = "econImpModBecario")
    private Float umbralBecario;
    @Column(name = "econIndiceB")
    private String indBecario;
    @Column(name = "econImpModCompen")
    private Float umbralCompensa;
    @Column(name = "econIndiceC")
    private String indCompensa;
    @Column(name = "econImpModMovLGen")
    private Float umbralMovGeneral;
    @Column(name = "econIndiceM")
    private String indMovGeneral;
    @Column(name = "econImpModMovLEspc")
    private Float umbralMovEspecial;
    @Column(name = "econIndiceE")
    private String indMovEspecial;
    @Column(name = "econImpModResid")
    private Float umbralResidMov;
    @Column(name = "econIndiceR")
    private String indResidMov;
    @Column(name = "econImpModTasas")
    private Float umbralTasas;
    @Column(name = "econIndiceT")
    private String indTasas;
    @Column(name = "econImpModi")
    private Float umbralPatrimonio;
    @Column(name = "econIndiceI")
    private String indPatrimonio;


    private static ResolucionEconomicoDAO resolucionEconomicoDAO;

    @Autowired
    public void setResolucionEconomicoDAO(ResolucionEconomicoDAO resolucionEconomicoDAO)
    {
        ResolucionEconomico.resolucionEconomicoDAO = resolucionEconomicoDAO;
    }

    public static ResolucionEconomico getResolucionEconomicoById(Long economicosId)
    {
        return resolucionEconomicoDAO.getResolucionEconomicoById(economicosId);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Float getCapitalMobiliario()
    {
        return capitalMobiliario;
    }

    public void setCapitalMobiliario(Float econCapital)
    {
        this.capitalMobiliario = econCapital;
    }

    public Float getDeducciones()
    {
        return deducciones;
    }

    public void setDeducciones(Float econDeducciones)
    {
        this.deducciones = econDeducciones;
    }

    public Float getVolumenNegocio()
    {
        return volumenNegocio;
    }

    public void setVolumenNegocio(Float econNegocio)
    {
        this.volumenNegocio = econNegocio;
    }

    public Float getRenta()
    {
        return renta;
    }

    public void setRenta(Float econRenta)
    {
        this.renta = econRenta;
    }

    public Float getUmbralBecario()
    {
        return umbralBecario;
    }

    public void setUmbralBecario(Float econImpModBecario)
    {
        this.umbralBecario = econImpModBecario;
    }

    public Float getUmbralCompensa()
    {
        return umbralCompensa;
    }

    public void setUmbralCompensa(Float econImpModCompen)
    {
        this.umbralCompensa = econImpModCompen;
    }

    public Float getUmbralMovEspecial()
    {
        return umbralMovEspecial;
    }

    public void setUmbralMovEspecial(Float econImpModMovLEespc)
    {
        this.umbralMovEspecial = econImpModMovLEespc;
    }

    public Float getUmbralMovGeneral()
    {
        return umbralMovGeneral;
    }

    public void setUmbralMovGeneral(Float econImpModMovLGen)
    {
        this.umbralMovGeneral = econImpModMovLGen;
    }

    public Float getUmbralResidMov()
    {
        return umbralResidMov;
    }

    public void setUmbralResidMov(Float econImpModResid)
    {
        this.umbralResidMov = econImpModResid;
    }

    public Float getUmbralTasas()
    {
        return umbralTasas;
    }

    public void setUmbralTasas(Float econImpModTasas)
    {
        this.umbralTasas = econImpModTasas;
    }

    public Float getUmbralPatrimonio()
    {
        return umbralPatrimonio;
    }

    public void setUmbralPatrimonio(Float econImpModi)
    {
        this.umbralPatrimonio = econImpModi;
    }

    public String getIndBecario()
    {
        return indBecario;
    }

    public void setIndBecario(String econIndiceB)
    {
        this.indBecario = econIndiceB;
    }

    public String getIndCompensa()
    {
        return indCompensa;
    }

    public void setIndCompensa(String econIndiceC)
    {
        this.indCompensa = econIndiceC;
    }

    public String getIndMovEspecial()
    {
        return indMovEspecial;
    }

    public void setIndMovEspecial(String econIndiceE)
    {
        this.indMovEspecial = econIndiceE;
    }

    public String getIndPatrimonio()
    {
        return indPatrimonio;
    }

    public void setIndPatrimonio(String econIndiceI)
    {
        this.indPatrimonio = econIndiceI;
    }

    public String getIndMovGeneral()
    {
        return indMovGeneral;
    }

    public void setIndMovGeneral(String econIndiceM)
    {
        this.indMovGeneral = econIndiceM;
    }

    public String getIndResidMov()
    {
        return indResidMov;
    }

    public void setIndResidMov(String econIndiceR)
    {
        this.indResidMov = econIndiceR;
    }

    public String getIndTasas()
    {
        return indTasas;
    }

    public void setIndTasas(String econIndiceT)
    {
        this.indTasas = econIndiceT;
    }

}
