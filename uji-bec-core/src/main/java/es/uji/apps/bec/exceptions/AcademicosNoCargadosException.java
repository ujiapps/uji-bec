package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class AcademicosNoCargadosException extends CoreDataBaseException
{
    public AcademicosNoCargadosException()
    {
        super("No es poden calcular les ajudes per que falten dades acadèmics.");
    }

    public AcademicosNoCargadosException(String message)
    {
        super(message);
    }
}