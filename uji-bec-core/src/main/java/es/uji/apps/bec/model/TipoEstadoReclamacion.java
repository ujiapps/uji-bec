package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.TipoEstadoReclamacionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_ESTADOS_RECLAMACIONES")
public class TipoEstadoReclamacion implements Serializable
{
    private static TipoEstadoReclamacionDAO tipoEstadoReclamacionDAO;
    @Id
    private Long id;
    private String nombre;
    private Long orden;

    // bi-directional many-to-one association to Documento
    @OneToMany(mappedBy = "estado")
    private Set<BecaReclamacion> reclamaciones;

    public TipoEstadoReclamacion()
    {

    }

    @Autowired
    public void setTipoEstadoReclamacionDAO(TipoEstadoReclamacionDAO tipoEstadoReclamacionDAO)
    {
        TipoEstadoReclamacion.tipoEstadoReclamacionDAO = tipoEstadoReclamacionDAO;
    }

    public Long getId()
     {
         return this.id;
     }
     
    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public static TipoEstadoReclamacion getTipoEstadoReclamacionById(Long tipoEstadoReclamacionId)
    {
        List<TipoEstadoReclamacion> estadosReclamaciones = tipoEstadoReclamacionDAO.get(TipoEstadoReclamacion.class, tipoEstadoReclamacionId);
        if (estadosReclamaciones.size() > 0)
        {
            return estadosReclamaciones.get(0);
        }
        return new TipoEstadoReclamacion();
    }
}