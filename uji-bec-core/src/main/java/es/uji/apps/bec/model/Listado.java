package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ListadoDAO;

/**
 * The persistent class for the BC2_LISTADOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_LISTADOS")
public class Listado implements Serializable
{
    private static ListadoDAO listadoDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Boolean activo;
    private String nombre;
    private String url;
    //bi-directional many-to-one association to Organismo
    @ManyToOne
    private Organismo organismo;

    public Listado()
    {
    }

    @Autowired
    public void setListadoDAO(ListadoDAO listadoDAO)
    {
        Listado.listadoDAO = listadoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean getActivo()
    {
        return this.activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public void setOrganismo(Organismo organismo)
    {
        this.organismo = organismo;
    }

    public static List<Listado> getListadosByOrganismoId(Long organismoId)
    {
        return listadoDAO.getListadosByOrganismoId(organismoId);
    }
}