package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.bec.model.Arrendatario;
import es.uji.apps.bec.model.QArrendatario;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ArrendatarioDAODatabaseImpl extends BaseDAODatabaseImpl implements ArrendatarioDAO
{
    @Override
    public List<Arrendatario> getArrendatariosByDomicilio(Long domicilioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QArrendatario arrendatario = QArrendatario.arrendatario;

        query.from(arrendatario).where(arrendatario.domicilio.id.eq(domicilioId))
                .orderBy(arrendatario.nombre.asc());

        return  query.list(arrendatario);
    }

    @Override
    public Arrendatario getArrendatarioById(Long arrendatarioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QArrendatario arrendatario = QArrendatario.arrendatario;

        query.from(arrendatario).where(arrendatario.id.eq(arrendatarioId));

        List<Arrendatario> listaArrendatarios = query.list(arrendatario);
        if (listaArrendatarios.size() > 0)
        {
            return listaArrendatarios.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public void updateArrendatario(Arrendatario arrendatario)
    {
        QArrendatario qArrendatario = QArrendatario.arrendatario;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qArrendatario);

        updateClause.where(qArrendatario.id.eq(arrendatario.getId()))
                .set(qArrendatario.nombre, arrendatario.getNombre())
                .set(qArrendatario.identificacion, arrendatario.getIdentificacion())
                .set(qArrendatario.tipoIdentificacion, arrendatario.getTipoIdentificacion())
                .execute();
    }
}
