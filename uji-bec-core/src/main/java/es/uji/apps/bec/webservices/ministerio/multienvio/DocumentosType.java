//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 12:21:09 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.multienvio;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDate;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DocumentosType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentosType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}DocuNumDocumento"/>
 *         &lt;element ref="{}DocuNombre"/>
 *         &lt;element ref="{}DocuCodDocumento"/>
 *         &lt;element ref="{}DocuFile"/>
 *         &lt;element ref="{}DocuPath" minOccurs="0"/>
 *         &lt;element ref="{}DocuFecha" minOccurs="0"/>
 *         &lt;element ref="{}DocuReqId" minOccurs="0"/>
 *         &lt;element ref="{}DocuHash" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentosType", propOrder = {
    "docuNumDocumento",
    "docuNombre",
    "docuCodDocumento",
    "docuFile",
    "docuPath",
    "docuFecha",
    "docuReqId",
    "docuHash"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.multienvio.DocumentosType")
@Table(name = "DOCUMENTOSTYPE", schema = "UJI_BECAS_WS_MULTIENVIO")
@Inheritance(strategy = InheritanceType.JOINED)
public class DocumentosType
    implements Equals, HashCode
{

    @XmlElement(name = "DocuNumDocumento")
    protected Long docuNumDocumento;
    @XmlElement(name = "DocuNombre", required = true)
    protected String docuNombre;
    @XmlElement(name = "DocuCodDocumento", required = true)
    protected String docuCodDocumento;
    @XmlElement(name = "DocuFile", required = true)
    protected byte[] docuFile;
    @XmlElement(name = "DocuPath")
    protected String docuPath;
    @XmlElement(name = "DocuFecha")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar docuFecha;
    @XmlElement(name = "DocuReqId")
    protected Long docuReqId;
    @XmlElement(name = "DocuHash")
    protected String docuHash;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the docuNumDocumento property.
     * 
     */
    @Basic
    @Column(name = "DOCUNUMDOCUMENTO", scale = 0)
    public Long getDocuNumDocumento() {
        return docuNumDocumento;
    }

    /**
     * Sets the value of the docuNumDocumento property.
     * 
     */
    public void setDocuNumDocumento(Long value) {
        this.docuNumDocumento = value;
    }

    /**
     * Gets the value of the docuNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DOCUNOMBRE", length = 50)
    public String getDocuNombre() {
        return docuNombre;
    }

    /**
     * Sets the value of the docuNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocuNombre(String value) {
        this.docuNombre = value;
    }

    /**
     * Gets the value of the docuCodDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DOCUCODDOCUMENTO", length = 3)
    public String getDocuCodDocumento() {
        return docuCodDocumento;
    }

    /**
     * Sets the value of the docuCodDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocuCodDocumento(String value) {
        this.docuCodDocumento = value;
    }

    /**
     * Gets the value of the docuFile property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    @Basic
    @Column(name = "DOCUFILE")
    public byte[] getDocuFile() {
        return docuFile;
    }

    /**
     * Sets the value of the docuFile property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDocuFile(byte[] value) {
        this.docuFile = value;
    }

    /**
     * Gets the value of the docuPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DOCUPATH", length = 200)
    public String getDocuPath() {
        return docuPath;
    }

    /**
     * Sets the value of the docuPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocuPath(String value) {
        this.docuPath = value;
    }

    /**
     * Gets the value of the docuFecha property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getDocuFecha() {
        return docuFecha;
    }

    /**
     * Sets the value of the docuFecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocuFecha(XMLGregorianCalendar value) {
        this.docuFecha = value;
    }

    /**
     * Gets the value of the docuReqId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Basic
    @Column(name = "DOCUREQID", scale = 0)
    public Long getDocuReqId() {
        return docuReqId;
    }

    /**
     * Sets the value of the docuReqId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDocuReqId(Long value) {
        this.docuReqId = value;
    }

    /**
     * Gets the value of the docuHash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DOCUHASH", length = 64)
    public String getDocuHash() {
        return docuHash;
    }

    /**
     * Sets the value of the docuHash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocuHash(String value) {
        this.docuHash = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    @Basic
    @Column(name = "DOCUFECHAITEM")
    @Temporal(TemporalType.DATE)
    public Date getDocuFechaItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getDocuFecha());
    }

    public void setDocuFechaItem(Date target) {
        setDocuFecha(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DocumentosType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DocumentosType that = ((DocumentosType) object);
        {
            Long lhsDocuNumDocumento;
            lhsDocuNumDocumento = (true?this.getDocuNumDocumento(): 0L);
            Long rhsDocuNumDocumento;
            rhsDocuNumDocumento = (true?that.getDocuNumDocumento(): 0L);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuNumDocumento", lhsDocuNumDocumento), LocatorUtils.property(thatLocator, "docuNumDocumento", rhsDocuNumDocumento), lhsDocuNumDocumento, rhsDocuNumDocumento)) {
                return false;
            }
        }
        {
            String lhsDocuNombre;
            lhsDocuNombre = this.getDocuNombre();
            String rhsDocuNombre;
            rhsDocuNombre = that.getDocuNombre();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuNombre", lhsDocuNombre), LocatorUtils.property(thatLocator, "docuNombre", rhsDocuNombre), lhsDocuNombre, rhsDocuNombre)) {
                return false;
            }
        }
        {
            String lhsDocuCodDocumento;
            lhsDocuCodDocumento = this.getDocuCodDocumento();
            String rhsDocuCodDocumento;
            rhsDocuCodDocumento = that.getDocuCodDocumento();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuCodDocumento", lhsDocuCodDocumento), LocatorUtils.property(thatLocator, "docuCodDocumento", rhsDocuCodDocumento), lhsDocuCodDocumento, rhsDocuCodDocumento)) {
                return false;
            }
        }
        {
            byte[] lhsDocuFile;
            lhsDocuFile = this.getDocuFile();
            byte[] rhsDocuFile;
            rhsDocuFile = that.getDocuFile();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuFile", lhsDocuFile), LocatorUtils.property(thatLocator, "docuFile", rhsDocuFile), lhsDocuFile, rhsDocuFile)) {
                return false;
            }
        }
        {
            String lhsDocuPath;
            lhsDocuPath = this.getDocuPath();
            String rhsDocuPath;
            rhsDocuPath = that.getDocuPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuPath", lhsDocuPath), LocatorUtils.property(thatLocator, "docuPath", rhsDocuPath), lhsDocuPath, rhsDocuPath)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsDocuFecha;
            lhsDocuFecha = this.getDocuFecha();
            XMLGregorianCalendar rhsDocuFecha;
            rhsDocuFecha = that.getDocuFecha();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuFecha", lhsDocuFecha), LocatorUtils.property(thatLocator, "docuFecha", rhsDocuFecha), lhsDocuFecha, rhsDocuFecha)) {
                return false;
            }
        }
        {
            Long lhsDocuReqId;
            lhsDocuReqId = this.getDocuReqId();
            Long rhsDocuReqId;
            rhsDocuReqId = that.getDocuReqId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuReqId", lhsDocuReqId), LocatorUtils.property(thatLocator, "docuReqId", rhsDocuReqId), lhsDocuReqId, rhsDocuReqId)) {
                return false;
            }
        }
        {
            String lhsDocuHash;
            lhsDocuHash = this.getDocuHash();
            String rhsDocuHash;
            rhsDocuHash = that.getDocuHash();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "docuHash", lhsDocuHash), LocatorUtils.property(thatLocator, "docuHash", rhsDocuHash), lhsDocuHash, rhsDocuHash)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Long theDocuNumDocumento;
            theDocuNumDocumento = (true?this.getDocuNumDocumento(): 0L);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuNumDocumento", theDocuNumDocumento), currentHashCode, theDocuNumDocumento);
        }
        {
            String theDocuNombre;
            theDocuNombre = this.getDocuNombre();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuNombre", theDocuNombre), currentHashCode, theDocuNombre);
        }
        {
            String theDocuCodDocumento;
            theDocuCodDocumento = this.getDocuCodDocumento();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuCodDocumento", theDocuCodDocumento), currentHashCode, theDocuCodDocumento);
        }
        {
            byte[] theDocuFile;
            theDocuFile = this.getDocuFile();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuFile", theDocuFile), currentHashCode, theDocuFile);
        }
        {
            String theDocuPath;
            theDocuPath = this.getDocuPath();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuPath", theDocuPath), currentHashCode, theDocuPath);
        }
        {
            XMLGregorianCalendar theDocuFecha;
            theDocuFecha = this.getDocuFecha();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuFecha", theDocuFecha), currentHashCode, theDocuFecha);
        }
        {
            Long theDocuReqId;
            theDocuReqId = this.getDocuReqId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuReqId", theDocuReqId), currentHashCode, theDocuReqId);
        }
        {
            String theDocuHash;
            theDocuHash = this.getDocuHash();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "docuHash", theDocuHash), currentHashCode, theDocuHash);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
