package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.BecaReclamacionDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.ws.rs.core.MultivaluedMap;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the BC2_BECAS_RECLAMACIONES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_BECAS_RECLAMACIONES")
public class BecaReclamacion implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;
    // bi-directional many-to-one association to TipoEstadoReclamacion
    @ManyToOne
    private TipoEstadoReclamacion estado;
    @Column(name = "SOLICITUD_ID")
    private Long solicitudId;
    @Column(name = "REGISTRO_ID")
    private Long registroId;
    @Column(name = "REGISTRO_EJERCICIO_ID")
    private Long registroEjercicioId;
    @Column(name = "FECHA")
    private Date fecha;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "FECHA_NOTIFICACION")
    private Date fechaNotificacion;
    @Column(name = "FECHA_VALIDACION")
    private Date fechaValidacion;



    private static BecaReclamacionDAO becaReclamacionDAO;

    @Autowired
    public void setBecaReclamacionDAO(BecaReclamacionDAO becaReclamacionDAO)
    {
        BecaReclamacion.becaReclamacionDAO = becaReclamacionDAO;
    }

    public BecaReclamacion()
    {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Beca getBeca() {
        return beca;
    }

    public void setBeca(Beca beca) {
        this.beca = beca;
    }

    public TipoEstadoReclamacion getEstado() {
        return estado;
    }

    public void setEstado(TipoEstadoReclamacion estado) {
        this.estado = estado;
    }

    public Long getSolicitudId() {
        return solicitudId;
    }

    public void setSolicitudId(Long solicitudId) {
        this.solicitudId = solicitudId;
    }

    public Long getRegistroId() {
        return registroId;
    }

    public void setRegistroId(Long registroId) {
        this.registroId = registroId;
    }

    public Long getRegistroEjercicioId() {
        return registroEjercicioId;
    }

    public void setRegistroEjercicioId(Long registroEjercicioId) {
        this.registroEjercicioId = registroEjercicioId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaNotificacion() {
        return fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    public Date getFechaValidacion() {return fechaValidacion; }

    public void setFechaValidacion(Date fechaValidacion) {
        this.fechaValidacion = fechaValidacion;
    }


    public static List<BecaReclamacion> getReclamacionesByBecaId(Long becaId)
    {
        return becaReclamacionDAO.getReclamacionesByBecaId(becaId);
    }

    public static BecaReclamacion getReclamacionById(Long reclamacionId)
    {
        return becaReclamacionDAO.getReclamacionById(reclamacionId);
    }

    @Transactional
    public void delete()
    {
        becaReclamacionDAO.delete(BecaReclamacion.class, this.getId());
    }

    @Transactional
    public UIEntity actualiza(MultivaluedMap<String, String> params) throws ParseException
    {
        preparaReclamacion(params);

        becaReclamacionDAO.updateReclamacion(this);

        return UIEntity.toUI(this);
    }

    @Transactional
    public UIEntity inserta(MultivaluedMap<String, String> params) throws ParseException
    {
        preparaReclamacion(params);

        becaReclamacionDAO.insert(this);

        return UIEntity.toUI(this);
    }

    private BecaReclamacion preparaReclamacion(MultivaluedMap<String, String> params) throws ParseException
    {
        Long idParam = ParamUtils.parseLong(params.getFirst("id"));
        if (idParam != null)
        {
            this.setId(idParam);
        }

        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Long becaId = ParamUtils.parseLong(params.getFirst("becaId"));
        Beca beca = new Beca();
        beca.setId(becaId);
        this.setBeca(beca);

        Long estadoId = ParamUtils.parseLong(params.getFirst("estadoId"));
        TipoEstadoReclamacion estado = new TipoEstadoReclamacion();
        estado.setId(estadoId);
        this.setEstado(estado);

        this.setSolicitudId(ParamUtils.parseLong(params.getFirst("solicitudId")));
        this.setRegistroId(ParamUtils.parseLong(params.getFirst("registroId")));
        this.setRegistroEjercicioId(ParamUtils.parseLong(params.getFirst("registroEjercicioId")));

        String fechaParam = params.getFirst("fecha");
        if (fechaParam != null && fechaParam.length() > 0)
        {
            Date fecha = formatoFecha.parse(fechaParam);
            this.setFecha(fecha);
        }
        else
        {
            this.setFecha(null);
        }

        String fechaNotificacionParam = params.getFirst("fechaNotificacion");
        if (fechaNotificacionParam != null && fechaNotificacionParam.length() > 0)
        {
            Date fechaNotificacion = formatoFecha.parse(fechaNotificacionParam);
            this.setFechaNotificacion(fechaNotificacion);
        }
        else
        {
            this.setFechaNotificacion(null);
        }

        String fechaValidacionParam = params.getFirst("fechaValidacion");
        if (fechaValidacionParam != null && fechaValidacionParam.length() > 0)
        {
            Date fechaValidacion = formatoFecha.parse(fechaValidacionParam);
            this.setFechaValidacion(fechaValidacion);
        }
        else
        {
            this.setFechaValidacion(null);
        }

        this.setObservaciones(params.getFirst("observaciones"));

        return this;
    }
}