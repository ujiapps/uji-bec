package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.QArrendatario;
import es.uji.apps.bec.model.QRecursoDetalle;
import es.uji.apps.bec.model.RecursoDetalle;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RecursoDetalleDAODatabaseImpl extends BaseDAODatabaseImpl implements RecursoDetalleDAO {

    @Override
    public List<RecursoDetalle> getRecursoDetallesByRecursoIdAndTipo(Long recursoId, String tipo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QRecursoDetalle recursoDetalle = QRecursoDetalle.recursoDetalle;

        query.from(recursoDetalle).where(recursoDetalle.recurso.id.eq(recursoId)
                .and(recursoDetalle.tipo.eq(tipo)))
                .orderBy(recursoDetalle.codigo.asc());

        return  query.list(recursoDetalle);
    }
}
