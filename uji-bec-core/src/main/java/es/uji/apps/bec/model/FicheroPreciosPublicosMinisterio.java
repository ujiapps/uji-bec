package es.uji.apps.bec.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.*;

import es.uji.apps.bec.exceptions.DNIIncorrectoException;

public class FicheroPreciosPublicosMinisterio {
    private static final Long ORGANISMO_ID = 1L;
    private static final int CODIGO_UNIVERSIDAD_DOS_DIGITOS = 40;

    public FicheroPreciosPublicosMinisterio(String convocatoriaAcronimo) {

    }

    public Map<String, String> generaFicheroParaCursoAcademico(Long cursoAcademicoId, String tipoEstudio, String convocatoriaAcronimo, String tipoFichero)
            throws DNIIncorrectoException {
        StringBuffer datos = new StringBuffer();

        List<Map<String, Object>> listaDatosConvocatorias = getEstructuraTxtPorCursoAcademicoYTipoEstudioOrdenadosPorConvocatoria(cursoAcademicoId, tipoEstudio, convocatoriaAcronimo, tipoFichero);

        for (Map<String, Object> datosConvocatoria : listaDatosConvocatorias) {
            List<Map<String, Object>> registros = (List<Map<String, Object>>) datosConvocatoria.get("registros");
            for (Map<String, Object> registro : registros) {
                datos.append(getLineaRegistroMiembro(registro, convocatoriaAcronimo));
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}ministerioAE.txt", cursoAcademicoId));
        return fichero;
    }


    public Map<String, String> generaFicheroIncidenciasParaCursoAcademico(Long cursoAcademicoId, String convocatoria, String tipoFichero)
            throws DNIIncorrectoException {
        StringBuffer datos = new StringBuffer();

        List<Map<String, Object>> listaDatosConvocatorias = getEstructuraTxtPorCursoAcademico(cursoAcademicoId, convocatoria, tipoFichero);

        for (Map<String, Object> datosConvocatoria : listaDatosConvocatorias) {
            List<Map<String, Object>> registros = (List<Map<String, Object>>) datosConvocatoria.get("registros");
            for (Map<String, Object> registro : registros) {
                datos.append(getLineaRegistroMiembro(registro, convocatoria));
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}ministerioIN.txt", cursoAcademicoId));
        return fichero;
    }

    public Map<String, String> generaFicheroFamiliasNumerosasParaCursoAcademico(Long cursoAcademicoId, String convocatoria, String tipoFichero)
            throws DNIIncorrectoException {
        StringBuffer datos = new StringBuffer();

        List<Map<String, Object>> listaDatosConvocatorias = getEstructuraTxtPorCursoAcademicoFamiliasNumerosas(cursoAcademicoId, convocatoria, tipoFichero);

        for (Map<String, Object> datosConvocatoria : listaDatosConvocatorias) {
            List<Map<String, Object>> registros = (List<Map<String, Object>>) datosConvocatoria.get("registros");
            for (Map<String, Object> registro : registros) {
                datos.append(getLineaRegistroMiembro(registro, convocatoria));
            }
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", datos.toString());
        fichero.put("nombre", MessageFormat.format("{0,number,#}ministerioFN.txt", cursoAcademicoId));
        return fichero;
    }

    private String getLineaRegistroMiembro(Map<String, Object> registro, String convocatoriaAcronimo) {
        StringBuffer lineaRegistro = new StringBuffer();

        lineaRegistro.append(Fichero.rellenaString(CODIGO_UNIVERSIDAD_DOS_DIGITOS, 2, " ", "right"));
        lineaRegistro.append(Fichero.rellenaString(convocatoriaAcronimo, 2, " ", "right"));

        lineaRegistro.append(Fichero.rellenaString(registro.get("dni"), 9, " ", "right"));
        lineaRegistro.append(Fichero.rellenaString(registro.get("apellido1"), 22, " ", "right"));
        lineaRegistro.append(Fichero.rellenaString(registro.get("apellido2"), 22, " ", "right"));
        lineaRegistro.append(Fichero.rellenaString(registro.get("nombre"), 22, " ", "right"));

        lineaRegistro.append(Fichero.rellenaString(registro.get("tasasMinisterioCentimos"), 7, "0", "left"));
        lineaRegistro.append(Fichero.rellenaString(registro.get("cursoAcademico"), 9, " ", "right"));
        lineaRegistro.append(Fichero.rellenaString(registro.get("causaAyudaMec"), 2, " ", "right"));

        lineaRegistro.append("\r\n");

        return lineaRegistro.toString();
    }

    public Comparator<Map<String, Object>> mapComparator = new Comparator<Map<String, Object>>() {
        public int compare(Map<String, Object> m1, Map<String, Object> m2) {
            String m1Apellido1 = (String) m1.get("apellido1");
            String m2Apellido1 = (String) m2.get("apellido1");

            Integer compare = m1Apellido1.compareTo(m2Apellido1);

            if (compare.equals(0)) {
                String m1Apellido2 = (String) m1.get("apellido2");
                if (m1Apellido2 == null) {
                    return -1;
                }
                String m2Apellido2 = (String) m2.get("apellido2");
                if (m2Apellido2 == null) {
                    return 1;
                }

                return m1Apellido2.compareTo(m2Apellido2);
            } else {
                return m1Apellido1.compareTo(m2Apellido1);
            }
        }
    };

    public List<Map<String, Object>> getEstructuraTxtPorCursoAcademicoYTipoEstudioOrdenadosPorConvocatoria(Long cursoAcademicoId, String tipoEstudio, String convocatoriaAcronimo, String tipoFichero)
            throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();
        List<Convocatoria> listaConvocatoriasMinisterio = Convocatoria.getConvocatoriasActivasPorOrganismoId(ORGANISMO_ID);

        for (Convocatoria convocatoria : listaConvocatoriasMinisterio) {
            Map<String, Object> datosConvocatoria = new HashMap<String, Object>();
            List<ImportesMEC> listaImportesMEC;
            if (tipoEstudio == "M") {
                listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(convocatoria.getId(), cursoAcademicoId);
            } else if (tipoEstudio == "G") {
                listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(convocatoria.getId(), cursoAcademicoId);
            } else {
                listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoId(convocatoria.getId(), cursoAcademicoId);
            }

            List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
            for (ImportesMEC importesMEC : listaImportesMEC) {
                Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
                registros.add(registro);
                if (importesMEC.getTipoEstudio().equals("M")) {
                    registrosMaster.add(registro);
                } else {
                    registrosResto.add(registro);
                }

            }

            //Collections.sort(registros, mapComparator);

            datosConvocatoria.put("registros", registros);
            String nombreConvocatoria = convocatoria.getNombre();
            if (nombreConvocatoria.equals("Mobilitat")) {
                nombreConvocatoria = "Movilidad";
            }
            datosConvocatoria.put("nombre", nombreConvocatoria);
            datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
            datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
            datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
            datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasMinisterio"));

            listaDatosConvocatoria.add(datosConvocatoria);
        }

        return listaDatosConvocatoria;
    }

    public List<Map<String, Object>> getEstructuraTxtPorCursoAcademico(Long cursoAcademicoId, String convocatoriaAcronimo, String tipoFichero)
            throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();

        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setAcronimo(convocatoriaAcronimo);
        convocatoria.setId(99L);
        convocatoria.setNombre("Incidencias");

        Map<String, Object> datosConvocatoria = new HashMap<String, Object>();
        List<ImportesMEC> listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdIncidencias(convocatoria.getId(), cursoAcademicoId);

        List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
        for (ImportesMEC importesMEC : listaImportesMEC) {
            Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
            registros.add(registro);
            if (importesMEC.getTipoEstudio().equals("M")) {
                registrosMaster.add(registro);
            } else {
                registrosResto.add(registro);
            }
        }

        //Collections.sort(registros, mapComparator);

        datosConvocatoria.put("registros", registros);

        datosConvocatoria.put("nombre", convocatoria.getNombre());
        datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
        datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasMinisterio"));

        datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
        datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasMinisterio"));

        datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
        datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasMinisterio"));

        listaDatosConvocatoria.add(datosConvocatoria);

        return listaDatosConvocatoria;
    }

    public List<Map<String, Object>> getEstructuraPorCursoAcademicoYTipoEstudioOrdenadosPorConvocatoria(Long cursoAcademicoId, String tipoEstudio, String tipoFichero)
            throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();
        List<Convocatoria> listaConvocatoriasMinisterio = Convocatoria.getConvocatoriasActivasPorOrganismoId(ORGANISMO_ID);

        for (Convocatoria convocatoria : listaConvocatoriasMinisterio) {
            Map<String, Object> datosConvocatoria = new HashMap<String, Object>();

            List<ImportesMEC> listaImportesMEC;
            if (tipoEstudio == "M") {
                listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(convocatoria.getId(), cursoAcademicoId);
            } else {
                listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(convocatoria.getId(), cursoAcademicoId);
            }

            List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
            for (ImportesMEC importesMEC : listaImportesMEC) {
                Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
                registros.add(registro);
                if (importesMEC.getTipoEstudio().equals("M")) {
                    registrosMaster.add(registro);
                } else {
                    registrosResto.add(registro);
                }

            }

            //Collections.sort(registros, mapComparator);

            datosConvocatoria.put("registros", registros);
            String nombreConvocatoria = convocatoria.getNombre();
            if (nombreConvocatoria.equals("Mobilitat")) {
                nombreConvocatoria = "Movilidad";
            }
            datosConvocatoria.put("nombre", nombreConvocatoria);
            datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
            datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
            datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
            datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasMinisterio"));

            listaDatosConvocatoria.add(datosConvocatoria);
        }

        return listaDatosConvocatoria;
    }

    public Map<String, Object> getEstructuraCompensacionGVA(Long cursoAcademicoId) throws DNIIncorrectoException {
        Map<String, Object> datos = new HashMap<String, Object>();

        List<ImportesMEC> listaImportesMEC = ImportesMEC.getImportesMECByCursoAcademicoId(cursoAcademicoId);
        List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();

        BigDecimal sumaTasasMinisterio = BigDecimal.valueOf(0);
        BigDecimal sumaTasasConselleria = BigDecimal.valueOf(0);
        BigDecimal sumaTasasMinisterioTotal = BigDecimal.valueOf(0);

        for (ImportesMEC importesMEC : listaImportesMEC) {
            Map<String, Object> registro = getEstructuraCompensacionGVARegistroMiembro(importesMEC);
            registros.add(registro);

            sumaTasasMinisterio = sumaTasasMinisterio.add(importesMEC.getTasasMinisterio());
            sumaTasasConselleria = sumaTasasConselleria.add(importesMEC.getTasasConselleria());
            sumaTasasMinisterioTotal = sumaTasasMinisterioTotal.add(importesMEC.getTasasMinisterioTotal());
        }

        datos.put("registros", registros);
        datos.put("sumaTasasMinisterio", sumaTasasMinisterio);
        datos.put("sumaTasasConselleria", sumaTasasConselleria);
        datos.put("sumaTasasMinisterioTotal", sumaTasasMinisterioTotal);

        return datos;
    }

    public List<Map<String, Object>> getEstructuraTxtPorCursoAcademicoFamiliasNumerosas(Long cursoAcademicoId, String convocatoriaAcronimo, String tipoFichero)
            throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();

        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setAcronimo(convocatoriaAcronimo);
        convocatoria.setId(98L);
        convocatoria.setNombre("Familias Numerosas");

        Map<String, Object> datosConvocatoria = new HashMap<String, Object>();
        List<ImportesMEC> listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoIdFamiliasNumerosas(convocatoria.getId(), cursoAcademicoId);

        List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
        for (ImportesMEC importesMEC : listaImportesMEC) {
            Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
            registros.add(registro);
            if (importesMEC.getTipoEstudio().equals("M")) {
                registrosMaster.add(registro);
            } else {
                registrosResto.add(registro);
            }
        }

        //Collections.sort(registros, mapComparator);

        datosConvocatoria.put("registros", registros);

        datosConvocatoria.put("nombre", convocatoria.getNombre());
        datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
        datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasMinisterio"));

        datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
        datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasMinisterio"));

        datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
        datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasMinisterio"));

        listaDatosConvocatoria.add(datosConvocatoria);

        return listaDatosConvocatoria;
    }

    private BigDecimal calculaNumeroAlumnos(List<Map<String, Object>> registros) {
        Long numeroAlumnos = registros.stream()
                .map(s -> s.get("dni").toString())
                .distinct()
                .count();
        return BigDecimal.valueOf(numeroAlumnos);
    }

    private BigDecimal calculaTotalImporte(List<Map<String, Object>> registros, String tasas) {
        return registros.stream()
                .filter(Objects::nonNull)
                .map(s -> (BigDecimal) s.get(tasas))
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private Map<String, Object> getEstructuraRegistroMiembro(ImportesMEC importesMEC, String tipo) throws DNIIncorrectoException {
        Map<String, Object> registro = new HashMap<String, Object>();
        String dni = importesMEC.getNif();
        if (dni == null || dni.length() != 9) {
            throw new DNIIncorrectoException(MessageFormat.format("El número de indentificació (DNI) del solicitant de la beca {0} és incorrecte", importesMEC.getBecaId()));
        }

        registro.put("dni", dni);
        registro.put("apellido1", importesMEC.getApellido1());
        registro.put("apellido2", importesMEC.getApellido2());
        registro.put("nombre", importesMEC.getNombre());
        if (importesMEC.getApellido2() == null) {
            registro.put("nombreCompleto", MessageFormat.format("{0}, {1}", importesMEC.getApellido1(), importesMEC.getNombre()));
        } else {
            registro.put("nombreCompleto", MessageFormat.format("{0} {1}, {2}", importesMEC.getApellido1(), importesMEC.getApellido2(), importesMEC.getNombre()));
        }

        Long cursoAcaId = importesMEC.getCursoAcademicoId();

        BigDecimal tasasMinisterio = importesMEC.getTasasMinisterio();
        if (tasasMinisterio == null) {
            tasasMinisterio = BigDecimal.valueOf(0);
        }
        registro.put("tasasMinisterio", tasasMinisterio);
        Long tasasMinisterioCentimos = convierteCentimos(tasasMinisterio);
        registro.put("tasasMinisterioCentimos", tasasMinisterioCentimos);

        BigDecimal tasasConselleria = importesMEC.getTasasConselleria();
        if (tasasConselleria == null) {
            tasasConselleria = BigDecimal.valueOf(0);
        }
        registro.put("tasasConselleria", tasasConselleria);
        Long tasasConselleriaCentimos = convierteCentimos(tasasConselleria);
        registro.put("tasasConselleriaCentimos", tasasConselleriaCentimos);

        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", cursoAcaId, (cursoAcaId + 1));

        registro.put("cursoAcademico", cursoAcademico);

        registro.put("causaAyudaMec", tipo.concat(importesMEC.getCausaAyudaMec()));


        return registro;
    }

    private Long convierteCentimos(BigDecimal tasasMinisterio) {
        Long centimos;

        tasasMinisterio = tasasMinisterio.multiply(BigDecimal.valueOf(100));

        return tasasMinisterio.longValue();
    }

    private Map<String, Object> getEstructuraCompensacionGVARegistroMiembro(ImportesMEC importesMEC) throws DNIIncorrectoException {
        Map<String, Object> registro = new HashMap<String, Object>();
        String dni = importesMEC.getNif();
        if (dni == null || dni.length() != 9) {
            throw new DNIIncorrectoException(MessageFormat.format("El número de indentificació (DNI) del solicitant de la beca {0} és incorrecte", importesMEC.getBecaId()));
        }

        registro.put("dni", dni);
        registro.put("apellido1", importesMEC.getApellido1());
        registro.put("apellido2", importesMEC.getApellido2());
        registro.put("nombre", importesMEC.getNombre());
        if (importesMEC.getApellido2() == null) {
            registro.put("nombreCompleto", MessageFormat.format("{0}, {1}", importesMEC.getApellido1(), importesMEC.getNombre()));
        } else {
            registro.put("nombreCompleto", MessageFormat.format("{0} {1}, {2}", importesMEC.getApellido1(), importesMEC.getApellido2(), importesMEC.getNombre()));
        }

        BigDecimal notaMedia = importesMEC.getNotaMedia();
        if (notaMedia == null) {
            notaMedia = BigDecimal.valueOf(0);
        }
        registro.put("notaMedia", notaMedia);

        BigDecimal creditosPrimera = importesMEC.getCreditos1a();
        if (creditosPrimera == null) {
            creditosPrimera = BigDecimal.valueOf(0);
        }
        registro.put("creditosPrimera", creditosPrimera);

        BigDecimal importesPrimera = importesMEC.getImportesT1a();
        if (importesPrimera == null) {
            importesPrimera = BigDecimal.valueOf(0);
        }
        registro.put("importesPrimera", importesPrimera);


        BigDecimal tasasMinisterio = importesMEC.getTasasMinisterio();
        if (tasasMinisterio == null) {
            tasasMinisterio = BigDecimal.valueOf(0);
        }
        registro.put("tasasMinisterio", tasasMinisterio);

        BigDecimal tasasConselleria = importesMEC.getTasasConselleria();
        if (tasasConselleria == null) {
            tasasConselleria = BigDecimal.valueOf(0);
        }
        registro.put("tasasConselleria", tasasConselleria);

        BigDecimal tasasMinisterioTotal = importesMEC.getTasasMinisterioTotal();
        if (tasasMinisterioTotal == null) {
            tasasMinisterioTotal = BigDecimal.valueOf(0);
        }
        registro.put("tasasMinisterioTotal", tasasMinisterioTotal);

        Long cursoAcaId = importesMEC.getCursoAcademicoId();
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", cursoAcaId, (cursoAcaId + 1));
        registro.put("cursoAcademico", cursoAcademico);

        return registro;
    }

    public Map<String, String> getLineaRegistroCompensatoria(Long cursoAcademicoId) {
        StringBuffer lineaRegistro = new StringBuffer();

        List<ImportesMEC> listaImportesMEC = ImportesMEC.getImportesMECByCursoAcademicoId(cursoAcademicoId);

        for (ImportesMEC importe : listaImportesMEC) {
            lineaRegistro.append(Fichero.rellenaString(CODIGO_UNIVERSIDAD_DOS_DIGITOS, 2, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getNif(), 9, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getApellido1(), 24, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getApellido2(), 24, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getNombre(), 24, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getCentroId(), 8, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getCentro(), 100, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getRuct(), 7, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(importe.getEstudio(), 100, " ", "right"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getCreditos1a()), 5, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getImportesT1a()), 6, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getTasasTotal()), 6, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getTasasMinisterio()), 6, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getTasasConselleria()), 6, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(convierteCentimos(importe.getNotaMedia()), 4, "0", "left"));
            lineaRegistro.append(Fichero.rellenaString(importe.getSexo(), 1, " ", "right"));
            lineaRegistro.append("\r\n");
        }

        Map<String, String> fichero = new HashMap<String, String>();
        fichero.put("datos", lineaRegistro.toString());
        fichero.put("nombre", MessageFormat.format("conselleriaCompensacion{0,number,#}.txt", cursoAcademicoId));
        return fichero;
    }

    public List<Map<String, Object>> getEstructuraPorCursoAcademicoOrdenadosPorConvocatoria(Long cursoAcademicoId, String tipoFichero)
            throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();
        List<Convocatoria> listaConvocatoriasMinisterio = Convocatoria.getConvocatoriasActivasPorOrganismoId(ORGANISMO_ID);

        for (Convocatoria convocatoria : listaConvocatoriasMinisterio) {
            Map<String, Object> datosConvocatoria = new HashMap<String, Object>();

            List<ImportesMEC> listaImportesMEC;
            listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoId(convocatoria.getId(), cursoAcademicoId);

            List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
            for (ImportesMEC importesMEC : listaImportesMEC) {
                Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
                registros.add(registro);
                if (importesMEC.getTipoEstudio().equals("M")) {
                    registrosMaster.add(registro);
                } else {
                    registrosResto.add(registro);
                }

            }

            //Collections.sort(registros, mapComparator);

            datosConvocatoria.put("registros", registros);
            String nombreConvocatoria = convocatoria.getNombre();
            if (nombreConvocatoria.equals("Mobilitat")) {
                nombreConvocatoria = "Movilidad";
            }
            datosConvocatoria.put("nombre", nombreConvocatoria);
            datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
            datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
            datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasMinisterio"));

            datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
            datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasMinisterio"));

            listaDatosConvocatoria.add(datosConvocatoria);
        }

        return listaDatosConvocatoria;
    }

    public List<Map<String, Object>> getEstructuraRegistroCompensatoria(Long cursoAcademicoId, String tipoFichero) throws DNIIncorrectoException {
        List<Map<String, Object>> listaDatosConvocatoria = new ArrayList<Map<String, Object>>();
        List<Convocatoria> listaConvocatoriasMinisterio = Convocatoria.getConvocatoriasActivasPorOrganismoId(ORGANISMO_ID);

        for (Convocatoria convocatoria : listaConvocatoriasMinisterio) {
            Map<String, Object> datosConvocatoria = new HashMap<String, Object>();

            List<ImportesMEC> listaImportesMEC;
            listaImportesMEC = ImportesMEC.getImportesMECByConvocatoriaIdCursoAcademicoId(convocatoria.getId(), cursoAcademicoId);

            List<Map<String, Object>> registros = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosMaster = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> registrosResto = new ArrayList<Map<String, Object>>();
            for (ImportesMEC importesMEC : listaImportesMEC) {
                Map<String, Object> registro = getEstructuraRegistroMiembro(importesMEC, tipoFichero);
                registros.add(registro);
                if (importesMEC.getTipoEstudio().equals("M")) {
                    registrosMaster.add(registro);
                } else {
                    registrosResto.add(registro);
                }
            }

            datosConvocatoria.put("registros", registros);
            String nombreConvocatoria = convocatoria.getNombre();
            if (nombreConvocatoria.equals("Mobilitat")) {
                nombreConvocatoria = "Movilidad";
            }
            datosConvocatoria.put("nombre", nombreConvocatoria);
            datosConvocatoria.put("totalAlumnos", calculaNumeroAlumnos(registros));
            datosConvocatoria.put("totalImporte", calculaTotalImporte(registros, "tasasConselleria"));

            datosConvocatoria.put("totalAlumnosMaster", calculaNumeroAlumnos(registrosMaster));
            datosConvocatoria.put("totalImporteMaster", calculaTotalImporte(registrosMaster, "tasasConselleria"));

            datosConvocatoria.put("totalAlumnosResto", calculaNumeroAlumnos(registrosResto));
            datosConvocatoria.put("totalImporteResto", calculaTotalImporte(registrosResto, "tasasConselleria"));

            listaDatosConvocatoria.add(datosConvocatoria);
        }

        return listaDatosConvocatoria;
    }
}
