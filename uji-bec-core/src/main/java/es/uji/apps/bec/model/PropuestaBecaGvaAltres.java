package es.uji.apps.bec.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.PersonaEstudioDAO;
import es.uji.apps.bec.exceptions.AcademicosNoCargadosException;
import es.uji.apps.bec.exceptions.AyudaNoAplicableException;
import es.uji.apps.bec.exceptions.BecaConTandaNoPuedeCambiarConvocatoriaException;
import es.uji.apps.bec.exceptions.BecaCuantiaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaDeDistintoOrganismoYaExisteException;
import es.uji.apps.bec.exceptions.BecaDenegacionDuplicadaException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.ConvocatoriaDeTandaYDeBecaDiferentesException;
import es.uji.apps.bec.exceptions.DistanciaDomicilioFamiliarNoEncontradaException;
import es.uji.apps.bec.exceptions.DistanciaLocalidadNoEncontradaException;
import es.uji.apps.bec.exceptions.EstadoBecaNoPermiteCalcularDenegacionesException;
import es.uji.apps.bec.exceptions.SolicitanteSinDomicilioFamiliarException;
import es.uji.apps.bec.model.domains.CodigoEstado;


@Component
public class PropuestaBecaGvaAltres extends PropuestaBeca
{
    private static Boolean denegada;
    public static final Long CONVOCATORIA_BECA_SALARIO = 11L;
    public static final Long CONVOCATORIA_BECA_FINALIZACION = 12L;
    public static final java.lang.Float CRD_PARA_FINALIZAR = 24F;
    private static PersonaEstudioDAO personaEstudioDAO;

    @Autowired
    public void setPersonaEstudioDAO(PersonaEstudioDAO personaEstudioDAO)
    {
        PropuestaBecaGvaAltres.personaEstudioDAO = personaEstudioDAO;
    }

    public static void calculaPropuesta(Beca beca) throws BecaDenegacionDuplicadaException,
            BecaDuplicadaException, EstadoBecaNoPermiteCalcularDenegacionesException,
            BecaCuantiaDuplicadaException, AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            BecaDeDistintoOrganismoYaExisteException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        if (!beca.isEnEstadoPendienteTrabajadaODenegadaAcademico())
        {
            throw new EstadoBecaNoPermiteCalcularDenegacionesException();
        }

        procesaAyudas(beca);
    }

    private static void procesaAyudas(Beca beca) throws BecaCuantiaDuplicadaException,
            BecaDuplicadaException, AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        beca.setEstado(Estado.getEstadoById(CodigoEstado.TRABAJADA.getId()));
        beca.updateEstado();
        beca.borraAyudas();
        beca.insertaAyudas();
    }
}
