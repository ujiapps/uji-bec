package es.uji.apps.bec.webservices.ministerio.colaboracion.lista;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "SolicitudRegistro")
public class SolicitudRegistro
{
    private Long ciudSqCiudadano;
    private Integer ciudIdCurso;
    private String ciudCodConv;
    private Integer ciudSecuenc;
    private Integer ciudTipoUTTramite;
    private Integer ciudCodUTTramite;
    private String ciudNif;
    private String ciudNombre;
    private String ciudApellido1;
    private String ciudApellido2;
    private Date ciudEntrada;

    public SolicitudRegistro()
    {

    }

    @XmlElement(name = "CiudSqCiudadano")
    public Long getCiudSqCiudadano()
    {
        return ciudSqCiudadano;
    }

    public void setCiudSqCiudadano(Long ciudSqCiudadano)
    {
        this.ciudSqCiudadano = ciudSqCiudadano;
    }

    @XmlElement(name = "CiudIdCurso")
    public Integer getCiudIdCurso()
    {
        return ciudIdCurso;
    }

    public void setCiudIdCurso(Integer ciudIdCurso)
    {
        this.ciudIdCurso = ciudIdCurso;
    }

    @XmlElement(name = "CiudCodConv")
    public String getCiudCodConv()
    {
        return ciudCodConv;
    }

    public void setCiudCodConv(String ciudCodConv)
    {
        this.ciudCodConv = ciudCodConv;
    }

    @XmlElement(name = "CiudSecuenc")
    public Integer getCiudSecuenc()
    {
        return ciudSecuenc;
    }

    public void setCiudSecuenc(Integer ciudSecuenc)
    {
        this.ciudSecuenc = ciudSecuenc;
    }

    @XmlElement(name = "CiudTipoUTTramite")
    public Integer getCiudTipoUTTramite()
    {
        return ciudTipoUTTramite;
    }

    public void setCiudTipoUTTramite(Integer ciudTipoUTTramite)
    {
        this.ciudTipoUTTramite = ciudTipoUTTramite;
    }

    @XmlElement(name = "CiudCodUTTramite")
    public Integer getCiudCodUTTramite()
    {
        return ciudCodUTTramite;
    }

    public void setCiudCodUTTramite(Integer ciudCodUTTramite)
    {
        this.ciudCodUTTramite = ciudCodUTTramite;
    }

    @XmlElement(name = "CiudNif")
    public String getCiudNif()
    {
        return ciudNif;
    }

    public void setCiudNif(String ciudNif)
    {
        this.ciudNif = ciudNif;
    }

    @XmlElement(name = "CiudNombre")
    public String getCiudNombre()
    {
        return ciudNombre;
    }

    public void setCiudNombre(String ciudNombre)
    {
        this.ciudNombre = ciudNombre;
    }

    @XmlElement(name = "CiudApellido1")
    public String getCiudApellido1()
    {
        return ciudApellido1;
    }

    public void setCiudApellido1(String ciudApellido1)
    {
        this.ciudApellido1 = ciudApellido1;
    }

    @XmlElement(name = "CiudApellido2")
    public String getCiudApellido2()
    {
        return ciudApellido2;
    }

    public void setCiudApellido2(String ciudApellido2)
    {
        this.ciudApellido2 = ciudApellido2;
    }

    @XmlElement(name = "CiudEntrada")
    public Date getCiudEntrada()
    {
        return ciudEntrada;
    }

    public void setCiudEntrada(Date ciudEntrada)
    {
        this.ciudEntrada = ciudEntrada;
    }
}