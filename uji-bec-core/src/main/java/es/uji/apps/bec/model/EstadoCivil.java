package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.EstadoCivilDAO;

/**
 * The persistent class for the BC2_ESTADOS_CIVILES database table.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "BC2_ESTADOS_CIVILES")
@Component
public class EstadoCivil implements Serializable
{
    private static EstadoCivilDAO estadoCivilDAO;

    @Id
    private Long id;

    private String nombre;

    private Long orden;
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "estadoCivil")
    private Set<Miembro> miembros;

    public EstadoCivil()
    {
    }

    public static EstadoCivil getEstadoCivilById(Long estadoCivilId)
    {
        return estadoCivilDAO.getEstadoCivilById(estadoCivilId);
    }

    @Autowired
    public void setEstadoCivilDAO(EstadoCivilDAO estadoCivilDAO)
    {
        EstadoCivil.estadoCivilDAO = estadoCivilDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }
}