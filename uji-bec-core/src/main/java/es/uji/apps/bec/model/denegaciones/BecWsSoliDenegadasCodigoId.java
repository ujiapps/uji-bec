package es.uji.apps.bec.model.denegaciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@SuppressWarnings("serial")
public class BecWsSoliDenegadasCodigoId implements Serializable
{
    private String escacodcausa;
    private String escacodsubcausa;
    private BigDecimal essocodestado;
    private Date essofecha;
    private BigDecimal essosecuencestado;

    public String getEscacodcausa()
    {
        return escacodcausa;
    }

    public void setEscacodcausa(String escacodcausa)
    {
        this.escacodcausa = escacodcausa;
    }

    public String getEscacodsubcausa()
    {
        return escacodsubcausa;
    }

    public void setEscacodsubcausa(String escacodsubcausa)
    {
        this.escacodsubcausa = escacodsubcausa;
    }

    public BigDecimal getEssocodestado()
    {
        return essocodestado;
    }

    public void setEssocodestado(BigDecimal essocodestado)
    {
        this.essocodestado = essocodestado;
    }

    public Date getEssofecha()
    {
        return essofecha;
    }

    public void setEssofecha(Date essofecha)
    {
        this.essofecha = essofecha;
    }

    public BigDecimal getEssosecuencestado()
    {
        return essosecuencestado;
    }

    public void setEssosecuencestado(BigDecimal essosecuencestado)
    {
        this.essosecuencestado = essosecuencestado;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((escacodcausa == null) ? 0 : escacodcausa.hashCode());
        result = prime * result + ((escacodsubcausa == null) ? 0 : escacodsubcausa.hashCode());
        result = prime * result + ((essocodestado == null) ? 0 : essocodestado.hashCode());
        result = prime * result + ((essofecha == null) ? 0 : essofecha.hashCode());
        result = prime * result + ((essosecuencestado == null) ? 0 : essosecuencestado.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BecWsSoliDenegadasCodigoId other = (BecWsSoliDenegadasCodigoId) obj;
        if (escacodcausa == null)
        {
            if (other.escacodcausa != null)
                return false;
        }
        else if (!escacodcausa.equals(other.escacodcausa))
            return false;
        if (escacodsubcausa == null)
        {
            if (other.escacodsubcausa != null)
                return false;
        }
        else if (!escacodsubcausa.equals(other.escacodsubcausa))
            return false;
        if (essocodestado == null)
        {
            if (other.essocodestado != null)
                return false;
        }
        else if (!essocodestado.equals(other.essocodestado))
            return false;
        if (essofecha == null)
        {
            if (other.essofecha != null)
                return false;
        }
        else if (!essofecha.equals(other.essofecha))
            return false;
        if (essosecuencestado == null)
        {
            if (other.essosecuencestado != null)
                return false;
        }
        else if (!essosecuencestado.equals(other.essosecuencestado))
            return false;
        return true;
    }
}