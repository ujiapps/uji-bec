package es.uji.apps.bec.ui;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement
public class Resumen
{
    private String etiqueta;
    private Long valor;

    public Resumen()
    {

    }

    public void setEtiqueta(String etiqueta)
    {
        this.etiqueta = etiqueta;
    }

    public String getEtiqueta()
    {
        return etiqueta;
    }

    public void setValor(Long valor)
    {
        this.valor = valor;
    }

    public Long getValor()
    {
        return valor;
    }
}
