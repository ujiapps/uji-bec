package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoMinusvaliaDAO;

/**
 * The persistent class for the BC2_TIPOS_MINUSVALIA database table.
 */
@Component
@Entity
@Table(name = "BC2_TIPOS_MINUSVALIA")
public class TipoMinusvalia implements Serializable
{  @Id
    private Long id;
    private String nombre;
    private Long orden;
    
    private static final long serialVersionUID = 1L;
    private static TipoMinusvaliaDAO tipoMinusvaliaDAO; 
  
    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "tipoMinusvalia")
    private Set<Miembro> miembros;


    @Autowired
    public void setTipoMinusvaliaDAO(TipoMinusvaliaDAO tipoMinusvaliaDAO)
    {
        TipoMinusvalia.tipoMinusvaliaDAO = tipoMinusvaliaDAO;
    }
    public TipoMinusvalia()
    {
    }
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }
    public static TipoMinusvalia getTipoMinusvaliaById(Long tipoMinusvaliaId)
    {
         return tipoMinusvaliaDAO.getTipoMinusvaliaById(tipoMinusvaliaId);

    }
    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }

    public static TipoMinusvalia getTipoMinusvaliaByOrden(String codigoMinusvalia)
    {
        return tipoMinusvaliaDAO.getTipoMinusvaliaByOrden(codigoMinusvalia);
    }
}
