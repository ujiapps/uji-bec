package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class EstudioSolicitadoNoMatriculadoException extends GeneralBECException
{
    public EstudioSolicitadoNoMatriculadoException()
    {
        super("El alumno no està matriculat per a l'estudi sol·licitat.");
    }
    
    public EstudioSolicitadoNoMatriculadoException(String message)
    {
        super(message);
    }
}
