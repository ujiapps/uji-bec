package es.uji.apps.bec.dao.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO
{
    @Override
    public Persona getPersonaByDNI(String dni)
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;

        jpaQuery.from(qPersona).where(qPersona.identificacion.lower().eq(dni.toLowerCase()));
        Persona persona = jpaQuery.uniqueResult(qPersona);
        return persona;
    }

    @Override
    public Persona getPersonaByPersonaId(Long personaId)
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;

        jpaQuery.from(qPersona)
                .where(qPersona.id.eq(personaId));

        Persona persona = jpaQuery.uniqueResult(qPersona);
        return persona;
    }
}
