package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.db.BaseDAO;

public interface EstadoDAO extends BaseDAO
{
    List<Estado> getEstados();

    Estado getEstadoById(Long estadoId);

    List<Resumen> getResumenEstadosPorConvocatoria(Long convocatoriaId);

    List<Resumen> getResumenProcesosPorConvocatoria(Long convocatoriaId);

    List<Resumen> getResumenEstadosPorTanda(Long tandaId);

    void deleteEstado(Long estadoId);
}
