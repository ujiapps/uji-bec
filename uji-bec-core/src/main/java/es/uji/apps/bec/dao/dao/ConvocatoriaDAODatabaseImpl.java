package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.QConvocatoria;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ConvocatoriaDAODatabaseImpl extends BaseDAODatabaseImpl implements ConvocatoriaDAO
{
    private QConvocatoria convocatoria = QConvocatoria.convocatoria;

    @Override
    public List<Convocatoria> getConvocatoriasActivas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(convocatoria).where(convocatoria.activa.isTrue()).orderBy(convocatoria.id.asc());
        return query.list(convocatoria);
    }

    @Override
    public Convocatoria getConvocatoriaByAcronimo(String acronimo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(convocatoria).where(convocatoria.acronimo.eq(acronimo));
        return query.uniqueResult(convocatoria);
    }

    @Override
    public Convocatoria getConvocatoriaById(Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(convocatoria).where(convocatoria.id.eq(convocatoriaId));
        return query.uniqueResult(convocatoria);
    }

    @Override
    public List<Convocatoria> getConvocatoriasActivasPorOrganismoId(Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(convocatoria).where(convocatoria.activa.isTrue().and(convocatoria.organismo.id.eq(organismoId)));
        return query.list(convocatoria);
    }

    @Override
    @Transactional
    public void deleteConvocatoria(Long convocatoriaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, convocatoria);
        delete.where(convocatoria.id.eq(convocatoriaId)).execute();
    }
}
