package es.uji.apps.bec.model;

public class FicheroConselleriaManuelaSolisConcedidas {
    private Long codigoArchivoTemporal;
    private String nif;
    private String apellido1;
    private String apellido2;
    private String nombre;
    private Float importe;
    private String iban;
    private String entidad;
    private String oficina;
    private String dc;
    private String cuenta;

    public FicheroConselleriaManuelaSolisConcedidas() {
    }

    public FicheroConselleriaManuelaSolisConcedidas(String codigoArchivoTemporal, String nif, String apellido1, String apellido2, String nombre, String importe, String iban, String entidad, String oficina, String dc, String cuenta) {
        this.codigoArchivoTemporal = Long.parseLong(codigoArchivoTemporal);
        this.nif = nif;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.nombre = nombre;
        String parteEntera = importe.substring(0, importe.length() - 2);
        String parteDecimal = importe.substring(importe.length() - 2);
        this.importe = Float.parseFloat(parteEntera + "." + parteDecimal);
        this.iban = iban;
        this.entidad = entidad;
        this.oficina = oficina;
        this.dc = dc;
        this.cuenta = cuenta;
    }

    public FicheroConselleriaManuelaSolisConcedidas(String linea) {
        String[] campos = linea.split(";");
        this.codigoArchivoTemporal = Long.parseLong(campos[0]);
        this.nif = campos[1];
        this.apellido1 = campos[2];
        this.apellido2 = campos[3];
        this.nombre = campos[4];
        String parteEntera = campos[5].substring(0, campos[5].length() - 2);
        String parteDecimal = campos[5].substring(campos[5].length() - 2);
        this.importe = Float.parseFloat(parteEntera + "." + parteDecimal);
        this.iban = campos[6];
        this.entidad = campos[7];
        this.oficina = campos[8];
        this.dc = campos[9];
        this.cuenta = campos[10];
    }

    public Long getCodigoArchivoTemporal() {
        return codigoArchivoTemporal;
    }

    public void setCodigoArchivoTemporal(Long codigoArchivoTemporal) {
        this.codigoArchivoTemporal = codigoArchivoTemporal;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
}
