package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Diccionario;
import es.uji.apps.bec.model.QDiccionario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DiccionarioDAODatabaseImpl extends BaseDAODatabaseImpl implements DiccionarioDAO
{
    @Override
    public Long getValor(Long cursoAcademicoId, String claseUji, Long organismoId,
            String valorOrigen)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDiccionario diccionario = QDiccionario.diccionario;

        if (cursoAcademicoId == null || organismoId == null || valorOrigen == null)
        {
          return null;
        }

        query.from(diccionario).where(
                diccionario.claseUji.eq(claseUji)
                        .and(diccionario.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(diccionario.organismo.id.eq(organismoId))
                        .and(diccionario.valorOrigen.eq(valorOrigen)));

        List<Diccionario> listaValores = query.list(diccionario);
        if (listaValores.size() > 0)
        {
            return listaValores.get(0).getValorUji();
        }
        return null;
    }

    @Override
    public String getValorOrigen(Long cursoAcademicoId, String claseUji, Long organismoId,
            String valorUji)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDiccionario diccionario = QDiccionario.diccionario;

        query.from(diccionario).where(
                diccionario.claseUji.eq(claseUji)
                        .and(diccionario.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(diccionario.organismo.id.eq(organismoId))
                        .and(diccionario.valorUji.eq(Long.parseLong(valorUji))));

        List<Diccionario> listaValores = query.list(diccionario);
        if (listaValores.size() > 0)
        {
            return listaValores.get(0).getValorOrigen();
        }
        return "";
    }
}
