package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoMinusvalia;
import es.uji.apps.bec.model.TipoMinusvalia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoMinusvaliaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoMinusvaliaDAO
{
    private QTipoMinusvalia  tipoMinusvalia = QTipoMinusvalia.tipoMinusvalia;

    @Override
    public List<TipoMinusvalia> getTiposMinusvalias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMinusvalia).orderBy(tipoMinusvalia.orden.asc());
        return query.list(tipoMinusvalia);
    }

    @Override
    public TipoMinusvalia getTipoMinusvaliaById(Long tipoMinusvaliaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoMinusvalia).where(tipoMinusvalia.id.eq(tipoMinusvaliaId));
        return query.uniqueResult(tipoMinusvalia);
    }

    @Override
    public TipoMinusvalia getTipoMinusvaliaByOrden(String codigoMinusvalia)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoMinusvalia tipoMinusvalia = QTipoMinusvalia.tipoMinusvalia;
        query.from(tipoMinusvalia).where(tipoMinusvalia.orden.eq(Long.parseLong(codigoMinusvalia)));
        return query.uniqueResult(tipoMinusvalia);
    }

    @Override
    @Transactional
    public void deleteTipoMinusvalia(Long tipoMinusvaliaId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoMinusvalia);
        delete.where(tipoMinusvalia.id.eq(tipoMinusvaliaId)).execute();
    }
}
