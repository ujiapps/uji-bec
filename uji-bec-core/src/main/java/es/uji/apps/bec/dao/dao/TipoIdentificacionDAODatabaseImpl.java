package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoIdentificacion;
import es.uji.apps.bec.model.TipoIdentificacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoIdentificacionDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoIdentificacionDAO
{
    private QTipoIdentificacion tipoIdentificacion = QTipoIdentificacion.tipoIdentificacion;

    @Override
    public List<TipoIdentificacion> getTiposIdentificaciones()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoIdentificacion).orderBy(tipoIdentificacion.orden.asc());
        return query.list(tipoIdentificacion);
    }

    @Override
    public TipoIdentificacion getTipoIdentificacionById(Long tipoIdentificacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoIdentificacion).where(tipoIdentificacion.id.eq(tipoIdentificacionId));
        return query.uniqueResult(tipoIdentificacion);
    }

    @Override
    @Transactional
    public void deleteTipoIdentificacion(Long tipoIdentificacionId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, tipoIdentificacion);
        delete.where(tipoIdentificacion.id.eq(tipoIdentificacionId)).execute();
    }
}
