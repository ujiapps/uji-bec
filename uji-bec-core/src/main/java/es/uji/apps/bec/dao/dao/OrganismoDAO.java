package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Organismo;
import es.uji.commons.db.BaseDAO;

public interface OrganismoDAO extends BaseDAO
{
    List<Organismo> getOrganismos();

    Organismo getOrganismoById(Long organismoId);

    void deleteOrganismo(Long organismoId);
}
