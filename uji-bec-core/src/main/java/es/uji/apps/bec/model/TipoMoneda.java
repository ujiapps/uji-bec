package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.TipoMonedaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_TIPOS_MONEDA database table.
 */
@Component
@Entity
@Table(name = "BC2_TIPOS_MONEDA")
@SuppressWarnings("serial")
public class TipoMoneda implements Serializable
{
    private static TipoMonedaDAO tipoMonedaDAO;
    @Id
    private Long id;
    private String nombre;
    private String codigo;
    @OneToMany(mappedBy = "tipoMoneda")
    private Set<Miembro> miembros;

    public TipoMoneda()
    {
    }

    public static TipoMoneda getTipoMonedaById(long tipoMonedaId)
    {
        return tipoMonedaDAO.getTipoMonedaById(tipoMonedaId);
    }

    @Autowired
    public void setTipoMonedaDAO(TipoMonedaDAO tipoMonedaDAO)
    {
        TipoMoneda.tipoMonedaDAO = tipoMonedaDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Set<Miembro> getMiembros()
    {
        return miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }

    public static TipoMoneda getTipoMonedaByCodigo(String tipoMonedaCodigo)
    {
        return tipoMonedaDAO.getTipoMonedaByCodigo(tipoMonedaCodigo);
    }
}
