package es.uji.apps.bec.webservices;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType;
import es.uji.apps.bec.webservices.ministerio.denegaciones.ObjectFactory;

@Component
public class EnvioDenegaciones {
    private static Logger log = Logger.getLogger(EnvioDenegaciones.class);

    @InjectParam
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    private Marshaller marshaller;

    public EnvioDenegaciones() throws JAXBException {
        JAXBContext contextDenegaciones = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.denegaciones");
        marshaller = contextDenegaciones.createMarshaller();
    }

    @Transactional
    public byte[] getDenegaciones(Long multienvioId) throws JAXBException {
        MultienvioType denegaciones = (MultienvioType) becaDAO.get(MultienvioType.class, multienvioId);

        if (denegaciones == null) {
            return new byte[]{};
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectFactory objectFactory = new ObjectFactory();
        marshaller.marshal(objectFactory.createMultienvio(denegaciones), baos);

        return baos.toByteArray();
    }

    @Transactional
    public void cargar(Long tandaId)
            throws WsErrorAlPrepararMultienvioException, GeneralBECException, ServiceException, JAXBException, IOException, SOAPException {
        MultienvioType multienvio = getMultienvioType(tandaId);
        String result = service.cargarXmlSolicitudesBase64(multienvio);
        log.info(result);
    }

    @Transactional
    public String getXmlMultienvio(Long tandaId) throws WsErrorAlPrepararMultienvioException, JAXBException {
        MultienvioType multienvio = getMultienvioType(tandaId);
        return service.multiEnvioToString(multienvio);
    }

    private MultienvioType getMultienvioType(Long tandaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = getRegistroEnvio(tandaId);
        Long multiEnvioId = registroEnvio.getEnvioId();
        List<MultienvioType> multiEnvios = becaDAO.get(MultienvioType.class, multiEnvioId);
        return multiEnvios.get(0);
    }

    private RegistroEnvio getRegistroEnvio(Long tandaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tandaId);

        if (registroEnvio == null) {
            throw new WsErrorAlPrepararMultienvioException("L\'enviament no està preparat.");
        }

        if (registroEnvio.isIncorrecto()) {
            throw new WsErrorAlPrepararMultienvioException("La preparació de l\'enviament és incorrecta.");
        }

        if (registroEnvio.isPendiente()) {
            throw new WsErrorAlPrepararMultienvioException("Les dades de l\'enviament estan ja generant-se. Espereu...");
        }
        return registroEnvio;
    }
}