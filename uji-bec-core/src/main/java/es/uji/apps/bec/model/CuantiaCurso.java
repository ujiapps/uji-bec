package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.CuantiaCursoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the BC2_CUANTIAS_POR_CURSO database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_CUANTIAS_POR_CURSO")
public class CuantiaCurso implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long importe;

    // bi-directional many-to-one association to Cuantia
    @ManyToOne
    private Cuantia cuantia;

    // bi-directional many-to-one association to CursoAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;

    private static CuantiaCursoDAO cuantiaCursoDAO;

    @Autowired
    public void setCuantiaCursoDAO(CuantiaCursoDAO cuantiaCursoDAO)
    {
        CuantiaCurso.cuantiaCursoDAO = cuantiaCursoDAO;
    }

    public CuantiaCurso()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getImporte()
    {
        return this.importe;
    }

    public void setImporte(Long importe)
    {
        this.importe = importe;
    }

    public Cuantia getCuantia()
    {
        return this.cuantia;
    }

    public void setCuantia(Cuantia cuantia)
    {
        this.cuantia = cuantia;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursosAcademico)
    {
        this.cursoAcademico = cursosAcademico;
    }

    public static List<CuantiaCurso> getCuantiasCursoByCursoAcademicoyConvocatoria(
            Long cursoAcademicoId, Long convocatoriaId)
    {
        return cuantiaCursoDAO.getCuantiasCursoByCursoAcademicoyConvocatoria(cursoAcademicoId,
                convocatoriaId);
    }

    public static CuantiaCurso getCuantiaCursoByCursoAcademicoAndCuantia(Long cursoAcademicoId,
            Long cuantiaId)
    {
        return cuantiaCursoDAO.getCuantiaCursoByCursoAcademicoAndCuantia(cursoAcademicoId,
                cuantiaId);
    }
}