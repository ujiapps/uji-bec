package es.uji.apps.bec.model;

import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.domains.CodigoDenegacion;
import es.uji.apps.bec.model.domains.CodigoEstado;

public class PropuestaBecaConselleria extends PropuestaBeca
{
    private static Boolean denegada;

    public static void calculaPropuesta(Beca beca) throws BecaDenegacionDuplicadaException,
            BecaDuplicadaException, EstadoBecaNoPermiteCalcularDenegacionesException,
            BecaCuantiaDuplicadaException, AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            BecaDeDistintoOrganismoYaExisteException,
            DistanciaDomicilioFamiliarNoEncontradaException, EstudioNoEncontradoException, EstudioAnteriorFaltanDatosException {
        if (!beca.isEnEstadoPendienteTrabajadaODenegadaAcademico())
        {
            throw new EstadoBecaNoPermiteCalcularDenegacionesException();
        }

        denegada = false;

        borraDenegaciones(beca);
        procesaDenegaciones(beca);
        procesaAyudas(beca);
    }

    private static void procesaDenegaciones(Beca beca)
            throws BecaDenegacionDuplicadaException,
            BecaDeDistintoOrganismoYaExisteException, AcademicosNoCargadosException, EstudioNoEncontradoException, EstudioAnteriorFaltanDatosException {
        Long orden = beca.calculaOrdenDenegacion();
        if (isMatriculado(beca))
        {
            deniegaSiTieneBecaConcedidaDelMinisterioSinCreditosSegunda(beca, orden);
            deniegaSiPoseeTitulo(beca, orden);
            deniegaSiNoMinimoCreditosMatriculados(beca, orden);
            deniegaSiMaximoDeBecasConcedidas(beca, orden);
            deniegaSiNoMinimoCreditosMatriculadosCursoAnterior(beca, orden);
            deniegaSiNoRendimientoCursoAnterior(beca, orden);
            deniegaSiPerdidaDeCursoLectivoPorCambioEstudios(beca, orden);
            calculaNumeroMiembrosComputables(beca);
            calculaNumeroHermanosYMinusvalias(beca);
        }
        else
        {
            insertaDenegacion(CodigoDenegacion.NO_ESTAR_MATRICULADO_CONS, beca, orden);
            denegada = true;
        }
    }


    private static void deniegaSiPerdidaDeCursoLectivoPorCambioEstudios(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException, EstudioNoEncontradoException, EstudioAnteriorFaltanDatosException {
        Long curso = beca.getCurso();

        if (curso == null) {
            throw new AcademicosNoCargadosException();
        }

        if (beca.isCambioDeEstudio()) {
            Long becasConcedidasAnt = beca.getBecasConcedidasAnt();
            if (becasConcedidasAnt != null && curso <= becasConcedidasAnt) {
                insertaDenegacion(CodigoDenegacion.PERDIDA_CURSO_LECTIVO_CONS, beca, orden);
                denegada = true;
            }
        }
    }


    private static void deniegaSiPerdidaDeCursoLectivo(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException
    {
        Long curso = beca.getCurso();
        Long becasConcedidasAnt = beca.getBecasConcedidasAnt();

        if (curso == null)
        {
            throw new AcademicosNoCargadosException();
        }

        if (becasConcedidasAnt != null && curso <= becasConcedidasAnt)
        {
            insertaDenegacion(CodigoDenegacion.PERDIDA_CURSO_LECTIVO_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiTieneBecaConcedidaDelMinisterioSinCreditosSegunda(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, BecaDeDistintoOrganismoYaExisteException
    {
        Long solicitanteId = beca.getSolicitante().getId();
        Solicitante solicitante = Solicitante.getSolicitanteConBecasById(solicitanteId);
        if (solicitante.tieneBecasConcedidasDelMinisterio() && beca.getCreditosSegundaMatricula() <= 0)
        {
            insertaDenegacion(CodigoDenegacion.TENER_BECA_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiDomicilioFueraDeLaComunidadValenciana(Beca beca, Long orden)
            throws SolicitanteSinDomicilioFamiliarException, BecaDenegacionDuplicadaException
    {
        if (!beca.isDomicilioComunidadValenciana())
        {
            insertaDenegacion(CodigoDenegacion.NO_DOMICILIO_COMUNIDAD_VALENCIANA_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiPoseeTitulo(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        if (beca.isPoseeTitulo())
        {
            insertaDenegacion(CodigoDenegacion.POSESION_DE_TITULO_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiNoMinimoCreditosMatriculados(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException
    {
        if (!isMatriculadoCreditosMinimos(beca))
        {
            insertaDenegacion(CodigoDenegacion.NO_MINIMO_CREDITOS_MATRICULA_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiMaximoDeBecasConcedidas(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        if (beca.isSuperadoNumeroMaximoDeBecasConcedidas())
        {
            insertaDenegacion(CodigoDenegacion.MAX_BECAS_CONCEDIDAS_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiNoMinimoCreditosMatriculadosCursoAnterior(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException
    {
        if (beca.isParaSegundoCursoOPosterior() && !isMatriculadoCreditosMinimosCursoAnterior(beca))
        {
            insertaDenegacion(CodigoDenegacion.NO_MINIMO_CREDITOS_MATRICULA_ANT_CONS, beca, orden);
            denegada = true;
        }
    }

    private static void procesaAyudas(Beca beca) throws BecaCuantiaDuplicadaException,
            BecaDuplicadaException, AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        if (denegada)
        {
            procesaAyudasParaBecaDenengada(beca);
        }
        else
        {
            procesaAyudasParaBecaNoDenegada(beca);
        }
    }

    private static void procesaAyudasParaBecaDenengada(Beca beca)
    {
        beca.setEstado(Estado.getEstadoById(CodigoEstado.DENEGADA_ACADEMICOS.getId()));
        beca.updateEstado();
        beca.borraAyudas();
    }

    private static void procesaAyudasParaBecaNoDenegada(Beca beca)
            throws BecaCuantiaDuplicadaException, BecaDuplicadaException,
            AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        beca.setEstado(Estado.getEstadoById(CodigoEstado.TRABAJADA.getId()));
        beca.updateEstado();
        beca.borraAyudas();
        beca.insertaAyudas();
    }

    private static void deniegaSiNoRendimientoCursoAnterior(Beca beca, Long orden) throws BecaDenegacionDuplicadaException
    {
        CodigoDenegacion denegacionNota = calculaDenegacionDeRendimientoCursoAnteriorNota(beca);
        if (denegacionNota != null)
        {
            insertaDenegacion(denegacionNota, beca, orden);
            denegada = true;
        }

        CodigoDenegacion denegacionPorcentaje = calculaDenegacionDeRendimientoCursoAnteriorPorcentaje(beca);
        if (denegacionPorcentaje != null)
        {
            insertaDenegacion(denegacionPorcentaje, beca, orden);
            denegada = true;
        }
    }

    private static CodigoDenegacion calculaDenegacionDeRendimientoCursoAnteriorNota(Beca beca)
    {
        Estudio estudio = beca.getEstudio();
        Long cursoAcademicoId = beca.getSolicitante().getCursoAcademico().getId();
        Minimo minimos = Minimo.getMinimosByEstudioId(estudio.getId(), cursoAcademicoId);

        if (beca.isParaPrimerCurso())
        {

            if (beca.getNotaMediaAnt()*minimos.getCoefCorrectorCons() < minimos.getNotaAccesoCons())
            {
                return CodigoDenegacion.NO_NOTA_MEDIA_ACCESO_CONS;
            }
            return null;
        }

        if (beca.isParaSegundoCursoOPosterior())
        {
            if (estudio.isTipoMaster())
            {
                if (beca.getNotaMediaAnt() < minimos.getNotaSegundosCons())
                {
                    return CodigoDenegacion.NO_NOTA_MEDIA_ACCESO_CONS;
                }
                return null;
            }
        }
        return null;
    }

    private static CodigoDenegacion calculaDenegacionDeRendimientoCursoAnteriorPorcentaje(Beca beca)
    {
       Minimo minimos = Minimo.getMinimosByEstudioId(beca.getEstudio().getId(), beca.getSolicitante().getCursoAcademico().getId());

        if (beca.isParaSegundoCursoOPosterior())
        {
            Float porcentajeSuperadosAlumno = beca.getCreditosSuperadosAnt() * 100 / beca.getCreditosMatriculadosAnt();
            Float porcentajeSuperadosConselleria =  minimos.getPorcSuperadosCons() - ((beca.getCreditosMatriculadosAnt() - minimos.getCreditosBecaCompleta()) * 100 / minimos.getCreditosBecaCompleta())/ 10;
            if (beca.isMatriculaParcialAnt())
            {
                porcentajeSuperadosConselleria =  minimos.getPorcSuperadosCons();

            }
            if (Float.compare(porcentajeSuperadosAlumno, porcentajeSuperadosConselleria) < 0)
            {
                return CodigoDenegacion.NO_MINIMO_CREDITOS_APROBADOS_CONS;
            }
        }
        return null;
    }
}
