package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.LoteMinisterioDAO;

/**
 * The persistent class for the BC2_LOTES_MINISTERIO database table.
 * 
 */
@Component
@Entity
@Table(name = "BC2_LOTES_MINISTERIO")
public class LoteMinisterio implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoAcademicoId;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "LOTE_ID")
    private Long loteId;

    @Column(name = "MULTIENVIO_ID")
    private Long multienvioId;

    private String estado;

    private static LoteMinisterioDAO loteMinisterioDAO;

    @Autowired
    public void setLoteMinisterioDAO(LoteMinisterioDAO loteMinisterioDAO)
    {
        LoteMinisterio.loteMinisterioDAO = loteMinisterioDAO;
    }

    public LoteMinisterio()
    {
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Long getConvocatoriaId()
    {
        return this.convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getCursoAcademicoId()
    {
        return this.cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getLoteId()
    {
        return this.loteId;
    }

    public void setLoteId(Long loteId)
    {
        this.loteId = loteId;
    }

    public Long getMultienvioId()
    {
        return multienvioId;
    }

    public void setMultienvioId(Long multienvioId)
    {
        this.multienvioId = multienvioId;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public static LoteMinisterio getLoteByLoteIdAndCursoAca(Long loteId, Long cursoAca)
    {
        List<LoteMinisterio> listaLotes = loteMinisterioDAO.getLoteByLoteIdAndCursoAca(loteId,
                cursoAca);

        if (listaLotes != null && !listaLotes.isEmpty())
        {
            return listaLotes.get(0);
        }

        return new LoteMinisterio();
    }

    @Transactional
    public void insert()
    {
        loteMinisterioDAO.insert(this);
    }

    public static LoteMinisterio getLoteByMultienvioId(Long multiEnvioId)
    {
        List<LoteMinisterio> listaLotes = loteMinisterioDAO.getLoteByMultienvioId(multiEnvioId);

        if (listaLotes != null && !listaLotes.isEmpty())
        {
            return listaLotes.get(0);
        }

        return new LoteMinisterio();
    }

    @Transactional
    public void update()
    {
        loteMinisterioDAO.update(this);
    }

    public static List<LoteMinisterio> getLotes(Map<String, String> filtros)
    {
        return loteMinisterioDAO.getLotes(filtros);
    }
}
