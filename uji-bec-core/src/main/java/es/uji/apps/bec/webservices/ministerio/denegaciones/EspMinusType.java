//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.07 at 01:14:30 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.denegaciones;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EspMinusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EspMinusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}EsmiCodMinus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EspMinusType", propOrder = {
    "esmiCodMinus"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.denegaciones.EspMinusType")
@Table(name = "ESPMINUSTYPE", schema = "UJI_BECAS_WS_DENEGACIONES")
@Inheritance(strategy = InheritanceType.JOINED)
public class EspMinusType
    implements Equals, HashCode
{

    @XmlElement(name = "EsmiCodMinus")
    protected String esmiCodMinus;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the esmiCodMinus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ESMICODMINUS", length = 1)
    public String getEsmiCodMinus() {
        return esmiCodMinus;
    }

    /**
     * Sets the value of the esmiCodMinus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsmiCodMinus(String value) {
        this.esmiCodMinus = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EspMinusType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EspMinusType that = ((EspMinusType) object);
        {
            String lhsEsmiCodMinus;
            lhsEsmiCodMinus = this.getEsmiCodMinus();
            String rhsEsmiCodMinus;
            rhsEsmiCodMinus = that.getEsmiCodMinus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "esmiCodMinus", lhsEsmiCodMinus), LocatorUtils.property(thatLocator, "esmiCodMinus", rhsEsmiCodMinus), lhsEsmiCodMinus, rhsEsmiCodMinus)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEsmiCodMinus;
            theEsmiCodMinus = this.getEsmiCodMinus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "esmiCodMinus", theEsmiCodMinus), currentHashCode, theEsmiCodMinus);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
