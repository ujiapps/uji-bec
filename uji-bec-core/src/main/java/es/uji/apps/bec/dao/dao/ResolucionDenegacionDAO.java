package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.ResolucionDenegacion;
import es.uji.commons.db.BaseDAO;

public interface ResolucionDenegacionDAO extends BaseDAO
{

    public List<ResolucionDenegacion> getDenegacionesByMultienvioAndBeca(Long multiEnvioId,
            Long becaId);

}
