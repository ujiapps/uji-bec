package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.Estudio;
import es.uji.commons.db.BaseDAO;

import java.util.List;
import java.util.Map;

public interface EstudioDAO extends BaseDAO
{
    List<Estudio> getEstudiosFiltrados(Map<String, Long> filtros);
}
