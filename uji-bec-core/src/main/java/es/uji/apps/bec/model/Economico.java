package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.EconomicoDAO;

/**
 * The persistent class for the BC2_ECONOMICOS database table.
 */
@Component
@Entity
@Table(name = "BC2_ECONOMICOS")
@SuppressWarnings("serial")
public class Economico implements Serializable
{
    private static EconomicoDAO economicoDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "IND_BECARIO")
    private Integer indBecario;
    @Column(name = "IND_COMPENSA")
    private Integer indCompensa;
    @Column(name = "IND_MOV_ESPECIAL")
    private Integer indMovEspecial;
    @Column(name = "IND_MOV_GENERAL")
    private Integer indMovGeneral;
    @Column(name = "IND_PATRIMONIO")
    private Integer indPatrimonio;
    @Column(name = "IND_RESID_MOV")
    private Integer indResidMov;
    @Column(name = "IND_TASAS")
    private Integer indTasas;
    @Column(name = "UMBRAL_BECARIO")
    private Float umbralBecario;
    @Column(name = "UMBRAL_COMPENSA")
    private Float umbralCompensa;
    @Column(name = "UMBRAL_MOV_ESPECIAL")
    private Float umbralMovEspecial;
    @Column(name = "UMBRAL_MOV_GENERAL")
    private Float umbralMovGeneral;
    @Column(name = "UMBRAL_PATRIMONIO")
    private Float umbralPatrimonio;
    @Column(name = "UMBRAL_RESID_MOV")
    private Float umbralResidMov;
    @Column(name = "UMBRAL_TASAS")
    private Float umbralTasas;
    private Float renta;
    @Column(name = "CAPITAL_MOBILIARIO")
    private Float capitalMobiliario;
    @Column(name = "VOLUMEN_NEGOCIO")
    private Float volumenNegocio;
    private Float deducciones;
    @Column(name = "COD_REPESCA")
    private String codRepesca;
    @Column(name = "COEFICIENTE_PRELACION")
    private Float coeficientePrelacion;
    // bi-directional one-to-one association to Solicitante
    @OneToOne
    @JoinColumn(name = "SOLICITANTE_ID")
    private Solicitante solicitante;

    public Economico()
    {
    }

    public static List<Economico> getEconomicoBySolicitanteId(Long solicitanteId)
    {
        return economicoDAO.getEconomicoBySolicitanteId(solicitanteId);
    }

    public static List<Economico> getEconomicoById(Long economicoId)
    {
        return economicoDAO.getEconomicoById(economicoId);
    }

    @Transactional
    public static Economico insertRegistro(Long solicitanteId, Float capitalMobiliario, Float umbralMovGeneral,
            Integer indMovEspecial, Integer indPatrimonio, Float umbralCompensa,
            Integer indMovGeneral, Integer indTasas, Float renta, Float umbralBecario, Integer indBecario,
            Float umbralResidMov, Float umbralPatrimonio, Float coeficientePrelacion,
            Integer indResidMov, String codRepesca, Float umbralTasas, Integer indCompensa, Float volumenNegocio,
            Float umbralMovEspecial, Float deducciones)
    {
        Economico economico = new Economico();
        Solicitante solicitante = Solicitante.getSolicitanteById(solicitanteId);
        economico.setSolicitante(solicitante);

        economico.setCapitalMobiliario(capitalMobiliario);
        economico.setUmbralMovGeneral(umbralMovGeneral);
        economico.setIndMovEspecial(indMovEspecial);
        economico.setIndPatrimonio(indPatrimonio);
        economico.setUmbralCompensa(umbralCompensa);
        economico.setIndMovGeneral(indMovGeneral);
        economico.setUmbralResidMov(umbralResidMov);
        economico.setUmbralPatrimonio(umbralPatrimonio);
        economico.setIndBecario(indBecario);
        economico.setIndResidMov(indResidMov);
        economico.setCodRepesca(codRepesca);
        economico.setUmbralTasas(umbralTasas);
        economico.setIndCompensa(indCompensa);
        economico.setVolumenNegocio(volumenNegocio);
        economico.setUmbralMovEspecial(umbralMovEspecial);
        economico.setDeducciones(deducciones);

        economico = economicoDAO.insert(economico);
        return economico;
    }

    @Autowired
    public void setEconomicoDAO(EconomicoDAO economicoDAO)
    {
        Economico.economicoDAO = economicoDAO;
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Float getCapitalMobiliario()
    {
        return this.capitalMobiliario;
    }

    public void setCapitalMobiliario(Float capitalMobiliario)
    {
        this.capitalMobiliario = capitalMobiliario;
    }

    public String getCodRepesca()
    {
        return this.codRepesca;
    }

    public void setCodRepesca(String codRepesca)
    {
        this.codRepesca = codRepesca;
    }

    public Float getCoeficientePrelacion()
    {
        return this.coeficientePrelacion;
    }

    public void setCoeficientePrelacion(Float coeficientePrelacion)
    {
        this.coeficientePrelacion = coeficientePrelacion;
    }

    public Float getDeducciones()
    {
        return this.deducciones;
    }

    public void setDeducciones(Float deducciones)
    {
        this.deducciones = deducciones;
    }

    public Integer getIndBecario()
    {
        return this.indBecario;
    }

    public void setIndBecario(Integer indBecario)
    {
        this.indBecario = indBecario;
    }

    public Integer getIndCompensa()
    {
        return this.indCompensa;
    }

    public void setIndCompensa(Integer indCompensa)
    {
        this.indCompensa = indCompensa;
    }

    public Integer getIndMovEspecial()
    {
        return this.indMovEspecial;
    }

    public void setIndMovEspecial(Integer indMovEspecial)
    {
        this.indMovEspecial = indMovEspecial;
    }

    public Integer getIndMovGeneral()
    {
        return this.indMovGeneral;
    }

    public void setIndMovGeneral(Integer indMovGeneral)
    {
        this.indMovGeneral = indMovGeneral;
    }

    public Integer getIndPatrimonio()
    {
        return this.indPatrimonio;
    }

    public void setIndPatrimonio(Integer indPatrimonio)
    {
        this.indPatrimonio = indPatrimonio;
    }

    public Integer getIndResidMov()
    {
        return this.indResidMov;
    }

    public void setIndResidMov(Integer indResidMov)
    {
        this.indResidMov = indResidMov;
    }

    public Integer getIndTasas()
    {
        return this.indTasas;
    }

    public void setIndTasas(Integer indTasas)
    {
        this.indTasas = indTasas;
    }

    public Float getRenta()
    {
        return this.renta;
    }

    public void setRenta(Float renta)
    {
        this.renta = renta;
    }

    public Float getUmbralBecario()
    {
        return this.umbralBecario;
    }

    public void setUmbralBecario(Float umbralBecario)
    {
        this.umbralBecario = umbralBecario;
    }

    public Float getUmbralCompensa()
    {
        return this.umbralCompensa;
    }

    public void setUmbralCompensa(Float umbralCompensa)
    {
        this.umbralCompensa = umbralCompensa;
    }

    public Float getUmbralMovEspecial()
    {
        return this.umbralMovEspecial;
    }

    public void setUmbralMovEspecial(Float umbralMovEspecial)
    {
        this.umbralMovEspecial = umbralMovEspecial;
    }

    public Float getUmbralMovGeneral()
    {
        return this.umbralMovGeneral;
    }

    public void setUmbralMovGeneral(Float umbralMovGeneral)
    {
        this.umbralMovGeneral = umbralMovGeneral;
    }

    public Float getUmbralPatrimonio()
    {
        return this.umbralPatrimonio;
    }

    public void setUmbralPatrimonio(Float umbralPatrimonio)
    {
        this.umbralPatrimonio = umbralPatrimonio;
    }

    public Float getUmbralResidMov()
    {
        return this.umbralResidMov;
    }

    public void setUmbralResidMov(Float umbralResidMov)
    {
        this.umbralResidMov = umbralResidMov;
    }

    public Float getUmbralTasas()
    {
        return this.umbralTasas;
    }

    public void setUmbralTasas(Float umbralTasas)
    {
        this.umbralTasas = umbralTasas;
    }

    public Float getVolumenNegocio()
    {
        return this.volumenNegocio;
    }

    public void setVolumenNegocio(Float volumenNegocio)
    {
        this.volumenNegocio = volumenNegocio;
    }

    public Solicitante getSolicitante()
    {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante)
    {
        this.solicitante = solicitante;
    }

    public void updateEconomico()
    {
        economicoDAO.updateEconomico(this);
    }

    public Economico updateRegistro(Float capitalMobiliario, Float umbralMovGeneral, Integer indMovEspecial,
            Integer indPatrimonio, Float umbralCompensa, Integer indMovGeneral, Integer indTasas,
            Float renta, Float umbralBecario, Integer indBecario, Float umbralResidMov, Float umbralPatrimonio,
            Float coeficientePrelacion, Integer indResidMov, String codRepesca, Float umbralTasas,
            Integer indCompensa, Float volumenNegocio, Float umbralMovEspecial, Float deducciones)
    {
        this.setCapitalMobiliario(capitalMobiliario);
        this.setUmbralMovGeneral(umbralMovGeneral);
        this.setIndMovEspecial(indMovEspecial);
        this.setIndPatrimonio(indPatrimonio);
        this.setUmbralCompensa(umbralCompensa);
        this.setIndMovGeneral(indMovGeneral);
        this.setUmbralResidMov(umbralResidMov);
        this.setUmbralPatrimonio(umbralPatrimonio);
        this.setIndBecario(indBecario);
        this.setIndTasas(indTasas);
        this.setUmbralBecario(umbralBecario);
        this.setIndResidMov(indResidMov);
        this.setCodRepesca(codRepesca);
        this.setUmbralTasas(umbralTasas);
        this.setRenta(renta);
        this.setIndCompensa(indCompensa);
        this.setVolumenNegocio(volumenNegocio);
        this.setUmbralMovEspecial(umbralMovEspecial);
        this.setDeducciones(deducciones);

        economicoDAO.updateEconomico(this);
        return this;
    }

    @Transactional
    public void insert()
    {
        economicoDAO.insert(this);
    }
}
