package es.uji.apps.bec.services;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResultadoDescarga
{
    private boolean correcto;
    private Integer cursoAca;
    private String convocatoria;
    private String nif;
    private Long archivoTemporal;

    public ResultadoDescarga()
    {

    }

    public ResultadoDescarga(boolean correcto, Integer cursoAca, String convocatoria, String nif, Long archivoTemporal)
    {
        this.correcto = correcto;
        this.cursoAca = cursoAca;
        this.convocatoria = convocatoria;
        this.archivoTemporal = archivoTemporal;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(String convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public String getNif()
    {
        return nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public Long getArchivoTemporal()
    {
        return archivoTemporal;
    }

    public void setArchivoTemporal(Long archivoTemporal)
    {
        this.archivoTemporal = archivoTemporal;
    }

    public boolean isCorrecto()
    {
        return correcto;
    }

    public void setCorrecto(boolean correcto)
    {
        this.correcto = correcto;
    }
}
