package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorTipoSexo extends Mapeador<Long, Object>
{
    public MapeadorTipoSexo()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "1");
        valores.put(CodigoOrganismo.CONSELLERIA, "H");
        add(new Long(1L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "2");
        valores.put(CodigoOrganismo.CONSELLERIA, "M");
        add(new Long(2L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "3");
        add(new Long(3L), valores);
    }
}
