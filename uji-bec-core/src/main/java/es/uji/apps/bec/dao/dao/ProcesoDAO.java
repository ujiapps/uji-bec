package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Proceso;
import es.uji.commons.db.BaseDAO;

public interface ProcesoDAO extends BaseDAO
{
    List<Proceso> getProcesos();
}
