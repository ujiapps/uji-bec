package es.uji.apps.bec.model;

public class Fichero
{
    public static Object denegacion;

    public static String rellenaString(Object objeto, Integer longitud, String caracter, String align)
    {

        if (objeto == null || objeto.equals(""))
        {
            return String.format("%" + longitud + "s", "").replace(" ", caracter);
        }

        String cadena = objeto.toString();
        Integer espacios = longitud - cadena.length();
        if (espacios > 0)
        {
            if (align.equals("right")) {
                return cadena + String.format("%" + espacios + "s", "").replace(" ", caracter);
            } else {
                return String.format("%" + espacios + "s", "").replace(" ", caracter) + cadena;
            }
        }
        else if (espacios == 0)
        {
            return cadena;
        }
        else
        {
            return cadena.substring(0, longitud);
        }
    }
}
