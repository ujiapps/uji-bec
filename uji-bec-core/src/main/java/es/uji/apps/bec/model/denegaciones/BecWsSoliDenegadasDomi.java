package es.uji.apps.bec.model.denegaciones;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@IdClass(BecWsSoliDenegadasDomiId.class)
@Table(name = "BEC_WS_SOLI_DENEGADAS_DOMI")
public class BecWsSoliDenegadasDomi implements Serializable
{
    @Id
    private BigDecimal domicloc;

    @Id
    private String domicoddomicilio;

    @Id
    private String domicodpostal;

    @Id
    private String domicodvia;

    @Id
    private String domicprov;

    @Id
    private String domidomicilio;

    @Id
    private String domiindespana;

    @Id
    private String domilocalidad;

    @Id
    private String dominumero;

    @Id
    private String domipais;

    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private BecWsSoliDenegada becWsSoliDenegada;

    public BecWsSoliDenegadasDomi()
    {
    }

    public BigDecimal getDomicloc()
    {
        return this.domicloc;
    }

    public void setDomicloc(BigDecimal domicloc)
    {
        this.domicloc = domicloc;
    }

    public String getDomicoddomicilio()
    {
        return this.domicoddomicilio;
    }

    public void setDomicoddomicilio(String domicoddomicilio)
    {
        this.domicoddomicilio = domicoddomicilio;
    }

    public String getDomicodpostal()
    {
        return this.domicodpostal;
    }

    public void setDomicodpostal(String domicodpostal)
    {
        this.domicodpostal = domicodpostal;
    }

    public String getDomicodvia()
    {
        return this.domicodvia;
    }

    public void setDomicodvia(String domicodvia)
    {
        this.domicodvia = domicodvia;
    }

    public String getDomicprov()
    {
        return this.domicprov;
    }

    public void setDomicprov(String domicprov)
    {
        this.domicprov = domicprov;
    }

    public String getDomidomicilio()
    {
        return this.domidomicilio;
    }

    public void setDomidomicilio(String domidomicilio)
    {
        this.domidomicilio = domidomicilio;
    }

    public String getDomiindespana()
    {
        return this.domiindespana;
    }

    public void setDomiindespana(String domiindespana)
    {
        this.domiindespana = domiindespana;
    }

    public String getDomilocalidad()
    {
        return this.domilocalidad;
    }

    public void setDomilocalidad(String domilocalidad)
    {
        this.domilocalidad = domilocalidad;
    }

    public String getDominumero()
    {
        return this.dominumero;
    }

    public void setDominumero(String dominumero)
    {
        this.dominumero = dominumero;
    }

    public String getDomipais()
    {
        return this.domipais;
    }

    public void setDomipais(String domipais)
    {
        this.domipais = domipais;
    }

    public BecWsSoliDenegada getBecWsSoliDenegada()
    {
        return this.becWsSoliDenegada;
    }

    public void setBecWsSoliDenegada(BecWsSoliDenegada becWsSoliDenegada)
    {
        this.becWsSoliDenegada = becWsSoliDenegada;
    }
}