package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Listado;
import es.uji.commons.db.BaseDAO;

public interface ListadoDAO extends BaseDAO
{
    List<Listado> getListadosByOrganismoId(Long organismoId);
}
