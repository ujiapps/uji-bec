package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.BecaEstudio;
import es.uji.commons.db.BaseDAO;

public interface BecaEstudioDAO extends BaseDAO
{
    BecaEstudio getBecaEstudioByBecaId(Long becaId);
}
