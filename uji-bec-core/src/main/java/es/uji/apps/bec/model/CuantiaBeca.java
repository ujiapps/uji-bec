package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.exceptions.BecaCuantiaDuplicadaException;
import es.uji.apps.bec.dao.dao.CuantiaBecaDAO;

/**
 * The persistent class for the BC2_BECAS_CUANTIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "BC2_BECAS_CUANTIAS")
public class CuantiaBeca implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Float importe;

    // bi-directional many-to-one association to Beca
    @ManyToOne
    private Beca beca;

    // bi-directional many-to-one association to Cuantia
    @ManyToOne
    private Cuantia cuantia;

    private static CuantiaBecaDAO cuantiaBecaDAO;

    @Autowired
    public void setCuantiaBecaDAO(CuantiaBecaDAO cuantiaBecaDAO)
    {
        CuantiaBeca.cuantiaBecaDAO = cuantiaBecaDAO;
    }

    public CuantiaBeca()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Float getImporte()
    {
        return this.importe;
    }

    public void setImporte(Float importe)
    {
        this.importe = importe;
    }

    public Beca getBeca()
    {
        return this.beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    public Cuantia getCuantia()
    {
        return this.cuantia;
    }

    public void setCuantia(Cuantia cuantia)
    {
        this.cuantia = cuantia;
    }

    @Transactional
    public static void delete(Long id)
    {
        cuantiaBecaDAO.delete(CuantiaBeca.class, id);
    }

    @Transactional
    public void insert() throws BecaCuantiaDuplicadaException
    {
        try
        {
            cuantiaBecaDAO.insert(this);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new BecaCuantiaDuplicadaException();
        }
    }

    @Transactional
    public void update()
    {
        cuantiaBecaDAO.update(this);
    }

    public void incrementaCuantiaTantoPorCiento(int tantoPorciento)
    {
        Float importeIncrementado = importe * (1F + tantoPorciento / 100F);
        setImporte(importeIncrementado);
    }
    
    public void reduccionCuantiaTantoPorCiento(int tantoPorCiento)
    {
        Float importeReduccion = importe  * tantoPorCiento/100;
        setImporte(importeReduccion);
    }

    public static CuantiaBeca getCuantiaBecaByCuantiaBecaId(Long cuantiaBecaId)
    {
        List<CuantiaBeca> listaCuantiasBecas = cuantiaBecaDAO.get(CuantiaBeca.class, cuantiaBecaId);
        if (listaCuantiasBecas.size() > 0)
        {
            return listaCuantiasBecas.get(0);
        }
        return null;
    }

    public static List<CuantiaBeca> getCuantiasBecaByBecaId(Long becaId)
    {
        return cuantiaBecaDAO.getCuantiasByBecaId(becaId);
    }
    
    public static CuantiaBeca getCuantiaBecaByBecaIdAndCuantiaId(Long becaId, Long cuantiaId)
    {
        return cuantiaBecaDAO.getCuantiaBecaByBecaIdAndCuantiaId(becaId, cuantiaId);
    }
}