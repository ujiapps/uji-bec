package es.uji.apps.bec.exceptions;

@SuppressWarnings("serial")
public class DescargaSolicitudesException extends GeneralBECException
{
    public DescargaSolicitudesException()
    {
        super("Error en la descarga de solicitudes");
    }
    public DescargaSolicitudesException(String message)
    {
        super(message);
    }
}