package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.ResolucionEconomico;
import es.uji.commons.db.BaseDAO;

public interface ResolucionEconomicoDAO extends BaseDAO
{
    public ResolucionEconomico getResolucionEconomicoById(Long economicosId);
}
