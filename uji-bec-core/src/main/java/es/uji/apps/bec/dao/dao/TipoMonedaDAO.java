package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.TipoMoneda;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TipoMonedaDAO extends BaseDAO
{
    List<TipoMoneda> getTiposMonedas();

    TipoMoneda getTipoMonedaById(Long tipoMonedaId);

    TipoMoneda getTipoMonedaByCodigo(String tipoMonedaCodigo);

    void deleteTipoMoneda(Long tipoMonedaId);
}
