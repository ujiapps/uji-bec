package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class MiembroIncorrectoException extends CoreBaseException
{

    public MiembroIncorrectoException()
    {
        super("El membre és incorrecte");
    }

    public MiembroIncorrectoException(String message)
    {
        super(message);
    }
}
