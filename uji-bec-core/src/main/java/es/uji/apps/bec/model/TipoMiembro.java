package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.TipoMiembroDAO;

/**
 * The persistent class for the BC2_TIPOS_MIEMBROS database table.
 * 
 */

@Entity
@Table(name = "BC2_TIPOS_MIEMBROS")
@Component
@SuppressWarnings("serial")
public class TipoMiembro implements Serializable
{
    @Id
    private Long id;

    private String nombre;

    private Long orden;

    private Boolean solicitante;

    // bi-directional many-to-one association to Miembro
    @OneToMany(mappedBy = "tipoMiembro")
    private Set<Miembro> miembros;

    private static TipoMiembroDAO tipoMiembroDAO;

    @Autowired
    public void setTipoMiembroDAO(TipoMiembroDAO tipoMiembroDAO)
    {
        TipoMiembro.tipoMiembroDAO = tipoMiembroDAO;
    }

    public TipoMiembro()
    {
        this.setSolicitante(false);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Boolean isSolicitante()
    {
        return this.solicitante;
    }

    public void setSolicitante(Boolean solicitante)
    {
        this.solicitante = solicitante;
    }

    public Set<Miembro> getMiembros()
    {
        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }

    public static TipoMiembro getTipoMiembroById(long tipoMiembroId)
    {
        return tipoMiembroDAO.getTipoMiembroById(tipoMiembroId);
    }
}