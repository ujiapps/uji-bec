package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.TodasConselleria;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TodasConselleriaDAO extends BaseDAO
{
    List<TodasConselleria> getDatosByCursoAcademicoId(Long cursoAcademicoId);
    List<TodasConselleria> getDatosConcedidasByCursoAcademicoId(Long cursoAcademicoId);
    List<TodasConselleria> getDatosDenegadasByCursoAcademicoId(Long cursoAcademicoId);
    boolean isMec2Crd(Long becaId, Long cursoAcademicoId);

}
