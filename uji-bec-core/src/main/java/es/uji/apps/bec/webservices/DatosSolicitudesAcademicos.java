package es.uji.apps.bec.webservices;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import es.uji.apps.bec.model.RegistroEnvio;
import es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.util.List;

@Component
public class DatosSolicitudesAcademicos
{
    private static Logger log = Logger.getLogger(DatosSolicitudesAcademicos.class);

    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    public DatosSolicitudesAcademicos()
    {
    }
    @Transactional
    public Long cargar(Long tandaId) throws WsErrorAlPrepararMultienvioException, ServiceException,
            JAXBException, IOException, GeneralBECException, SOAPException {
        DatosAcademicos datosAcademicos = getDatosAcademicos(tandaId);
        String result = service.cargarXmlDatosAcademicosUniv(datosAcademicos);
        String lote = result.split("<Lote>")[1].split("</Lote>")[0];

        log.info(result);

        return Long.parseLong(lote);
    }

    @Transactional
    public String getXmlMultienvio(Long tandaId) throws WsErrorAlPrepararMultienvioException, JAXBException
    {
        DatosAcademicos datosAcademicos = getDatosAcademicos(tandaId);
        return service.datosAcademicosToString(datosAcademicos);
    }

    private DatosAcademicos getDatosAcademicos(Long tandaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = getRegistroEnvio(tandaId);
        Long datosAcademicosId = registroEnvio.getEnvioId();
        List<DatosAcademicos> datosAcademicos = becaDAO.get(DatosAcademicos.class, datosAcademicosId);
        return datosAcademicos.get(0);
    }

    private RegistroEnvio getRegistroEnvio(Long tandaId) throws WsErrorAlPrepararMultienvioException
    {
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tandaId);

        if (registroEnvio == null)
        {
            throw new WsErrorAlPrepararMultienvioException("L\'enviament no està preparat.");
        }

        if (registroEnvio.isIncorrecto())
        {
            throw new WsErrorAlPrepararMultienvioException("La preparació de l\'enviament és incorrecta.");
        }

        if (registroEnvio.isPendiente())
        {
            throw new WsErrorAlPrepararMultienvioException("Les dades de l\'enviament estan ja generant-se. Espereu...");
        }
        return registroEnvio;
    }
}