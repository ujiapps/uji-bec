package es.uji.apps.bec.dao.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonaEstudioDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaEstudioDAO
{
    @Override
    public PersonaEstudio getAcademicosbyPersonaEstudioYCursoAca(Long personaId, Long estudioId,
            Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaEstudio academicos = QPersonaEstudio.personaEstudio;

        query.from(academicos).where(academicos.persona.id.eq(personaId).and(academicos.estudio.id
                .eq(estudioId).and(academicos.cursoAcademico.id.eq(cursoAcademicoId))));

        List<PersonaEstudio> list = query.list(academicos);

        if (list.isEmpty())
        {
            return new PersonaEstudio();
        }
        else
        {
            return list.get(0);
        }
    }

    @Override
    public Boolean existePersonaEstudio(Long personaId, Long estudioId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaEstudio personaEstudio = QPersonaEstudio.personaEstudio;

        query.from(personaEstudio)
                .where(personaEstudio.estudio.id.eq(estudioId)
                        .and(personaEstudio.persona.id.eq(personaId))
                        .and(personaEstudio.cursoAcademico.id.eq(cursoAcademicoId)));

        return query.count() > 0;
    }

    @Override
    public List<PersonaEstudio> getAcademicosByPersonaYCursoAca(Long personaId,
            Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaEstudio personaEstudio = QPersonaEstudio.personaEstudio;

        query.from(personaEstudio)
                .where(personaEstudio.persona.id.eq(personaId)
                        .and(personaEstudio.cursoAcademico.id.eq(cursoAcademicoId))
                        .and(personaEstudio.estudio.id.lt(50000L)));

        return query.list(personaEstudio);
    }

    @Override
    public Long getCursoAcademicoAnteriorUJI(Long personaId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaEstudio personaEstudio = QPersonaEstudio.personaEstudio;

        query.from(personaEstudio).where(personaEstudio.persona.id.eq(personaId)
                .and(personaEstudio.cursoAcademico.id.lt(cursoAcademicoId)));

        return query.list(personaEstudio.cursoAcademico.id.max()).get(0);
    }

    @Override
    public PersonaEstudio getPersonaEstudioByBecaId(Long becaId) throws BecaSinEstudioException {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery query2 = new JPAQuery(entityManager);

        QPersonaEstudio qPersonaEstudio = QPersonaEstudio.personaEstudio;
        QBeca qBeca = QBeca.beca;
        QSolicitante qSolicitante = QSolicitante.solicitante;

        Beca beca = query.from(qBeca).where(qBeca.id.eq(becaId)).uniqueResult(qBeca);
        Solicitante solicitante = query.from(qSolicitante).where(qSolicitante.becas.contains(beca)).uniqueResult(qSolicitante);

        if (beca.getEstudio() == null)
        {
            throw new BecaSinEstudioException("Alumne no matriculat");
        }

        Long estudioId = beca.getEstudio().getId();
        Long personaId = solicitante.getPersona().getId();
        Long cursoAcademicoId = solicitante.getCursoAcademico().getId();

        query2.from(qPersonaEstudio)
                .where(qPersonaEstudio.cursoAcademico.id.eq(cursoAcademicoId)
                        .and(qPersonaEstudio.persona.id.eq(personaId))
                        .and(qPersonaEstudio.estudio.id.eq(estudioId)));

        return query2.uniqueResult(qPersonaEstudio);
    }
}
