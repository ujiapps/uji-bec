//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.12.21 at 12:24:35 PM CET 
//

package es.uji.apps.bec.webservices.ministerio.errores;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDate;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Java class for CabeceraEnvioType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CabeceraEnvioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CaenIdCurso"/>
 *         &lt;element ref="{}CaenCodConv"/>
 *         &lt;element ref="{}CaenSecuenc"/>
 *         &lt;element ref="{}CaenTipoUTTramite"/>
 *         &lt;element ref="{}CaenCodUTTramite"/>
 *         &lt;element ref="{}CaenLote"/>
 *         &lt;element ref="{}CaenCodSituacEnvio" minOccurs="0"/>
 *         &lt;element ref="{}CaenFechaGen" minOccurs="0"/>
 *         &lt;element ref="{}CaenFechaEnv"/>
 *         &lt;element ref="{}CaenFechaRecep" minOccurs="0"/>
 *         &lt;element ref="{}CaenTotSoli"/>
 *         &lt;element ref="{}CaenFechaBD" minOccurs="0"/>
 *         &lt;element ref="{}CaenNumSoliErr" minOccurs="0"/>
 *         &lt;element ref="{}CaenNumSoliCarg" minOccurs="0"/>
 *         &lt;element ref="{}CaenTipoEnvio"/>
 *         &lt;element ref="{}CaenTipoUTGen"/>
 *         &lt;element ref="{}CaenCodUTGen"/>
 *         &lt;element ref="{}CaenLoteGen"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabeceraEnvioType", propOrder = { "caenIdCurso", "caenCodConv", "caenSecuenc",
        "caenTipoUTTramite", "caenCodUTTramite", "caenLote", "caenCodSituacEnvio", "caenFechaGen",
        "caenFechaEnv", "caenFechaRecep", "caenTotSoli", "caenFechaBD", "caenNumSoliErr",
        "caenNumSoliCarg", "caenTipoEnvio", "caenTipoUTGen", "caenCodUTGen", "caenLoteGen" })
@Entity(name = "es.uji.apps.bec.webservices.ministerio.errores.CabeceraEnvioType")
@Table(name = "CABECERAENVIOTYPE", schema = "UJI_BECAS_WS_ERRORES")
@Inheritance(strategy = InheritanceType.JOINED)
public class CabeceraEnvioType implements Equals, HashCode
{

    @XmlElement(name = "CaenIdCurso", required = true)
    protected String caenIdCurso;
    @XmlElement(name = "CaenCodConv", required = true)
    protected String caenCodConv;
    @XmlElement(name = "CaenSecuenc")
    protected long caenSecuenc;
    @XmlElement(name = "CaenTipoUTTramite", required = true)
    protected String caenTipoUTTramite;
    @XmlElement(name = "CaenCodUTTramite", required = true)
    protected String caenCodUTTramite;
    @XmlElement(name = "CaenLote", required = true)
    protected String caenLote;
    @XmlElement(name = "CaenCodSituacEnvio")
    protected Long caenCodSituacEnvio;
    @XmlElement(name = "CaenFechaGen")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar caenFechaGen;
    @XmlElement(name = "CaenFechaEnv", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar caenFechaEnv;
    @XmlElement(name = "CaenFechaRecep")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar caenFechaRecep;
    @XmlElement(name = "CaenTotSoli")
    protected long caenTotSoli;
    @XmlElement(name = "CaenFechaBD")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar caenFechaBD;
    @XmlElement(name = "CaenNumSoliErr")
    protected Long caenNumSoliErr;
    @XmlElement(name = "CaenNumSoliCarg")
    protected Long caenNumSoliCarg;
    @XmlElement(name = "CaenTipoEnvio", required = true)
    protected String caenTipoEnvio;
    @XmlElement(name = "CaenTipoUTGen", required = true)
    protected String caenTipoUTGen;
    @XmlElement(name = "CaenCodUTGen", required = true)
    protected String caenCodUTGen;
    @XmlElement(name = "CaenLoteGen", required = true)
    protected String caenLoteGen;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the caenIdCurso property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENIDCURSO", length = 4)
    public String getCaenIdCurso()
    {
        return caenIdCurso;
    }

    /**
     * Sets the value of the caenIdCurso property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenIdCurso(String value)
    {
        this.caenIdCurso = value;
    }

    /**
     * Gets the value of the caenCodConv property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENCODCONV", length = 2)
    public String getCaenCodConv()
    {
        return caenCodConv;
    }

    /**
     * Sets the value of the caenCodConv property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenCodConv(String value)
    {
        this.caenCodConv = value;
    }

    /**
     * Gets the value of the caenSecuenc property.
     * 
     */
    @Basic
    @Column(name = "CAENSECUENC", scale = 0)
    public long getCaenSecuenc()
    {
        return caenSecuenc;
    }

    /**
     * Sets the value of the caenSecuenc property.
     * 
     */
    public void setCaenSecuenc(long value)
    {
        this.caenSecuenc = value;
    }

    /**
     * Gets the value of the caenTipoUTTramite property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENTIPOUTTRAMITE")
    public String getCaenTipoUTTramite()
    {
        return caenTipoUTTramite;
    }

    /**
     * Sets the value of the caenTipoUTTramite property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenTipoUTTramite(String value)
    {
        this.caenTipoUTTramite = value;
    }

    /**
     * Gets the value of the caenCodUTTramite property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENCODUTTRAMITE", length = 3)
    public String getCaenCodUTTramite()
    {
        return caenCodUTTramite;
    }

    /**
     * Sets the value of the caenCodUTTramite property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenCodUTTramite(String value)
    {
        this.caenCodUTTramite = value;
    }

    /**
     * Gets the value of the caenLote property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENLOTE", length = 5)
    public String getCaenLote()
    {
        return caenLote;
    }

    /**
     * Sets the value of the caenLote property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenLote(String value)
    {
        this.caenLote = value;
    }

    /**
     * Gets the value of the caenCodSituacEnvio property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    @Basic
    @Column(name = "CAENCODSITUACENVIO", precision = 1, scale = 0)
    public Long getCaenCodSituacEnvio()
    {
        return caenCodSituacEnvio;
    }

    /**
     * Sets the value of the caenCodSituacEnvio property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setCaenCodSituacEnvio(Long value)
    {
        this.caenCodSituacEnvio = value;
    }

    /**
     * Gets the value of the caenFechaGen property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    @Transient
    public XMLGregorianCalendar getCaenFechaGen()
    {
        return caenFechaGen;
    }

    /**
     * Sets the value of the caenFechaGen property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCaenFechaGen(XMLGregorianCalendar value)
    {
        this.caenFechaGen = value;
    }

    /**
     * Gets the value of the caenFechaEnv property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    @Transient
    public XMLGregorianCalendar getCaenFechaEnv()
    {
        return caenFechaEnv;
    }

    /**
     * Sets the value of the caenFechaEnv property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCaenFechaEnv(XMLGregorianCalendar value)
    {
        this.caenFechaEnv = value;
    }

    /**
     * Gets the value of the caenFechaRecep property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    @Transient
    public XMLGregorianCalendar getCaenFechaRecep()
    {
        return caenFechaRecep;
    }

    /**
     * Sets the value of the caenFechaRecep property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCaenFechaRecep(XMLGregorianCalendar value)
    {
        this.caenFechaRecep = value;
    }

    /**
     * Gets the value of the caenTotSoli property.
     * 
     */
    @Basic
    @Column(name = "CAENTOTSOLI", scale = 0)
    public long getCaenTotSoli()
    {
        return caenTotSoli;
    }

    /**
     * Sets the value of the caenTotSoli property.
     * 
     */
    public void setCaenTotSoli(long value)
    {
        this.caenTotSoli = value;
    }

    /**
     * Gets the value of the caenFechaBD property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    @Transient
    public XMLGregorianCalendar getCaenFechaBD()
    {
        return caenFechaBD;
    }

    /**
     * Sets the value of the caenFechaBD property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCaenFechaBD(XMLGregorianCalendar value)
    {
        this.caenFechaBD = value;
    }

    /**
     * Gets the value of the caenNumSoliErr property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    @Basic
    @Column(name = "CAENNUMSOLIERR", scale = 0)
    public Long getCaenNumSoliErr()
    {
        return caenNumSoliErr;
    }

    /**
     * Sets the value of the caenNumSoliErr property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setCaenNumSoliErr(Long value)
    {
        this.caenNumSoliErr = value;
    }

    /**
     * Gets the value of the caenNumSoliCarg property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    @Basic
    @Column(name = "CAENNUMSOLICARG", scale = 0)
    public Long getCaenNumSoliCarg()
    {
        return caenNumSoliCarg;
    }

    /**
     * Sets the value of the caenNumSoliCarg property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setCaenNumSoliCarg(Long value)
    {
        this.caenNumSoliCarg = value;
    }

    /**
     * Gets the value of the caenTipoEnvio property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENTIPOENVIO")
    public String getCaenTipoEnvio()
    {
        return caenTipoEnvio;
    }

    /**
     * Sets the value of the caenTipoEnvio property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenTipoEnvio(String value)
    {
        this.caenTipoEnvio = value;
    }

    /**
     * Gets the value of the caenTipoUTGen property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENTIPOUTGEN")
    public String getCaenTipoUTGen()
    {
        return caenTipoUTGen;
    }

    /**
     * Sets the value of the caenTipoUTGen property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenTipoUTGen(String value)
    {
        this.caenTipoUTGen = value;
    }

    /**
     * Gets the value of the caenCodUTGen property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENCODUTGEN", length = 3)
    public String getCaenCodUTGen()
    {
        return caenCodUTGen;
    }

    /**
     * Sets the value of the caenCodUTGen property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenCodUTGen(String value)
    {
        this.caenCodUTGen = value;
    }

    /**
     * Gets the value of the caenLoteGen property.
     * 
     * @return possible object is {@link String }
     * 
     */
    @Basic
    @Column(name = "CAENLOTEGEN", length = 5)
    public String getCaenLoteGen()
    {
        return caenLoteGen;
    }

    /**
     * Sets the value of the caenLoteGen property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCaenLoteGen(String value)
    {
        this.caenLoteGen = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid()
    {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setHjid(Long value)
    {
        this.hjid = value;
    }

    @Basic
    @Column(name = "CAENFECHAGENITEM")
    @Temporal(TemporalType.DATE)
    public Date getCaenFechaGenItem()
    {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getCaenFechaGen());
    }

    public void setCaenFechaGenItem(Date target)
    {
        setCaenFechaGen(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    @Basic
    @Column(name = "CAENFECHAENVITEM")
    @Temporal(TemporalType.DATE)
    public Date getCaenFechaEnvItem()
    {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getCaenFechaEnv());
    }

    public void setCaenFechaEnvItem(Date target)
    {
        setCaenFechaEnv(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    @Basic
    @Column(name = "CAENFECHARECEPITEM")
    @Temporal(TemporalType.DATE)
    public Date getCaenFechaRecepItem()
    {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class,
                this.getCaenFechaRecep());
    }

    public void setCaenFechaRecepItem(Date target)
    {
        setCaenFechaRecep(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    @Basic
    @Column(name = "CAENFECHABDITEM")
    @Temporal(TemporalType.DATE)
    public Date getCaenFechaBDItem()
    {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDate.class, this.getCaenFechaBD());
    }

    public void setCaenFechaBDItem(Date target)
    {
        setCaenFechaBD(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDate.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
            EqualsStrategy strategy)
    {
        if (!(object instanceof CabeceraEnvioType))
        {
            return false;
        }
        if (this == object)
        {
            return true;
        }
        final CabeceraEnvioType that = ((CabeceraEnvioType) object);
        {
            String lhsCaenIdCurso;
            lhsCaenIdCurso = this.getCaenIdCurso();
            String rhsCaenIdCurso;
            rhsCaenIdCurso = that.getCaenIdCurso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenIdCurso", lhsCaenIdCurso),
                    LocatorUtils.property(thatLocator, "caenIdCurso", rhsCaenIdCurso),
                    lhsCaenIdCurso, rhsCaenIdCurso))
            {
                return false;
            }
        }
        {
            String lhsCaenCodConv;
            lhsCaenCodConv = this.getCaenCodConv();
            String rhsCaenCodConv;
            rhsCaenCodConv = that.getCaenCodConv();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenCodConv", lhsCaenCodConv),
                    LocatorUtils.property(thatLocator, "caenCodConv", rhsCaenCodConv),
                    lhsCaenCodConv, rhsCaenCodConv))
            {
                return false;
            }
        }
        {
            long lhsCaenSecuenc;
            lhsCaenSecuenc = (true ? this.getCaenSecuenc() : 0L);
            long rhsCaenSecuenc;
            rhsCaenSecuenc = (true ? that.getCaenSecuenc() : 0L);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenSecuenc", lhsCaenSecuenc),
                    LocatorUtils.property(thatLocator, "caenSecuenc", rhsCaenSecuenc),
                    lhsCaenSecuenc, rhsCaenSecuenc))
            {
                return false;
            }
        }
        {
            String lhsCaenTipoUTTramite;
            lhsCaenTipoUTTramite = this.getCaenTipoUTTramite();
            String rhsCaenTipoUTTramite;
            rhsCaenTipoUTTramite = that.getCaenTipoUTTramite();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenTipoUTTramite", lhsCaenTipoUTTramite),
                    LocatorUtils.property(thatLocator, "caenTipoUTTramite", rhsCaenTipoUTTramite),
                    lhsCaenTipoUTTramite, rhsCaenTipoUTTramite))
            {
                return false;
            }
        }
        {
            String lhsCaenCodUTTramite;
            lhsCaenCodUTTramite = this.getCaenCodUTTramite();
            String rhsCaenCodUTTramite;
            rhsCaenCodUTTramite = that.getCaenCodUTTramite();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenCodUTTramite", lhsCaenCodUTTramite),
                    LocatorUtils.property(thatLocator, "caenCodUTTramite", rhsCaenCodUTTramite),
                    lhsCaenCodUTTramite, rhsCaenCodUTTramite))
            {
                return false;
            }
        }
        {
            String lhsCaenLote;
            lhsCaenLote = this.getCaenLote();
            String rhsCaenLote;
            rhsCaenLote = that.getCaenLote();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenLote", lhsCaenLote),
                    LocatorUtils.property(thatLocator, "caenLote", rhsCaenLote), lhsCaenLote,
                    rhsCaenLote))
            {
                return false;
            }
        }
        {
            Long lhsCaenCodSituacEnvio;
            lhsCaenCodSituacEnvio = this.getCaenCodSituacEnvio();
            Long rhsCaenCodSituacEnvio;
            rhsCaenCodSituacEnvio = that.getCaenCodSituacEnvio();
            if (!strategy
                    .equals(LocatorUtils.property(thisLocator, "caenCodSituacEnvio",
                            lhsCaenCodSituacEnvio), LocatorUtils.property(thatLocator,
                            "caenCodSituacEnvio", rhsCaenCodSituacEnvio), lhsCaenCodSituacEnvio,
                            rhsCaenCodSituacEnvio))
            {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsCaenFechaGen;
            lhsCaenFechaGen = this.getCaenFechaGen();
            XMLGregorianCalendar rhsCaenFechaGen;
            rhsCaenFechaGen = that.getCaenFechaGen();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenFechaGen", lhsCaenFechaGen),
                    LocatorUtils.property(thatLocator, "caenFechaGen", rhsCaenFechaGen),
                    lhsCaenFechaGen, rhsCaenFechaGen))
            {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsCaenFechaEnv;
            lhsCaenFechaEnv = this.getCaenFechaEnv();
            XMLGregorianCalendar rhsCaenFechaEnv;
            rhsCaenFechaEnv = that.getCaenFechaEnv();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenFechaEnv", lhsCaenFechaEnv),
                    LocatorUtils.property(thatLocator, "caenFechaEnv", rhsCaenFechaEnv),
                    lhsCaenFechaEnv, rhsCaenFechaEnv))
            {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsCaenFechaRecep;
            lhsCaenFechaRecep = this.getCaenFechaRecep();
            XMLGregorianCalendar rhsCaenFechaRecep;
            rhsCaenFechaRecep = that.getCaenFechaRecep();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenFechaRecep", lhsCaenFechaRecep),
                    LocatorUtils.property(thatLocator, "caenFechaRecep", rhsCaenFechaRecep),
                    lhsCaenFechaRecep, rhsCaenFechaRecep))
            {
                return false;
            }
        }
        {
            long lhsCaenTotSoli;
            lhsCaenTotSoli = (true ? this.getCaenTotSoli() : 0L);
            long rhsCaenTotSoli;
            rhsCaenTotSoli = (true ? that.getCaenTotSoli() : 0L);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenTotSoli", lhsCaenTotSoli),
                    LocatorUtils.property(thatLocator, "caenTotSoli", rhsCaenTotSoli),
                    lhsCaenTotSoli, rhsCaenTotSoli))
            {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsCaenFechaBD;
            lhsCaenFechaBD = this.getCaenFechaBD();
            XMLGregorianCalendar rhsCaenFechaBD;
            rhsCaenFechaBD = that.getCaenFechaBD();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenFechaBD", lhsCaenFechaBD),
                    LocatorUtils.property(thatLocator, "caenFechaBD", rhsCaenFechaBD),
                    lhsCaenFechaBD, rhsCaenFechaBD))
            {
                return false;
            }
        }
        {
            Long lhsCaenNumSoliErr;
            lhsCaenNumSoliErr = this.getCaenNumSoliErr();
            Long rhsCaenNumSoliErr;
            rhsCaenNumSoliErr = that.getCaenNumSoliErr();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenNumSoliErr", lhsCaenNumSoliErr),
                    LocatorUtils.property(thatLocator, "caenNumSoliErr", rhsCaenNumSoliErr),
                    lhsCaenNumSoliErr, rhsCaenNumSoliErr))
            {
                return false;
            }
        }
        {
            Long lhsCaenNumSoliCarg;
            lhsCaenNumSoliCarg = this.getCaenNumSoliCarg();
            Long rhsCaenNumSoliCarg;
            rhsCaenNumSoliCarg = that.getCaenNumSoliCarg();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenNumSoliCarg", lhsCaenNumSoliCarg),
                    LocatorUtils.property(thatLocator, "caenNumSoliCarg", rhsCaenNumSoliCarg),
                    lhsCaenNumSoliCarg, rhsCaenNumSoliCarg))
            {
                return false;
            }
        }
        {
            String lhsCaenTipoEnvio;
            lhsCaenTipoEnvio = this.getCaenTipoEnvio();
            String rhsCaenTipoEnvio;
            rhsCaenTipoEnvio = that.getCaenTipoEnvio();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenTipoEnvio", lhsCaenTipoEnvio),
                    LocatorUtils.property(thatLocator, "caenTipoEnvio", rhsCaenTipoEnvio),
                    lhsCaenTipoEnvio, rhsCaenTipoEnvio))
            {
                return false;
            }
        }
        {
            String lhsCaenTipoUTGen;
            lhsCaenTipoUTGen = this.getCaenTipoUTGen();
            String rhsCaenTipoUTGen;
            rhsCaenTipoUTGen = that.getCaenTipoUTGen();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenTipoUTGen", lhsCaenTipoUTGen),
                    LocatorUtils.property(thatLocator, "caenTipoUTGen", rhsCaenTipoUTGen),
                    lhsCaenTipoUTGen, rhsCaenTipoUTGen))
            {
                return false;
            }
        }
        {
            String lhsCaenCodUTGen;
            lhsCaenCodUTGen = this.getCaenCodUTGen();
            String rhsCaenCodUTGen;
            rhsCaenCodUTGen = that.getCaenCodUTGen();
            if (!strategy.equals(
                    LocatorUtils.property(thisLocator, "caenCodUTGen", lhsCaenCodUTGen),
                    LocatorUtils.property(thatLocator, "caenCodUTGen", rhsCaenCodUTGen),
                    lhsCaenCodUTGen, rhsCaenCodUTGen))
            {
                return false;
            }
        }
        {
            String lhsCaenLoteGen;
            lhsCaenLoteGen = this.getCaenLoteGen();
            String rhsCaenLoteGen;
            rhsCaenLoteGen = that.getCaenLoteGen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caenLoteGen", lhsCaenLoteGen),
                    LocatorUtils.property(thatLocator, "caenLoteGen", rhsCaenLoteGen),
                    lhsCaenLoteGen, rhsCaenLoteGen))
            {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object)
    {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy)
    {
        int currentHashCode = 1;
        {
            String theCaenIdCurso;
            theCaenIdCurso = this.getCaenIdCurso();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenIdCurso", theCaenIdCurso), currentHashCode,
                    theCaenIdCurso);
        }
        {
            String theCaenCodConv;
            theCaenCodConv = this.getCaenCodConv();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenCodConv", theCaenCodConv), currentHashCode,
                    theCaenCodConv);
        }
        {
            long theCaenSecuenc;
            theCaenSecuenc = (true ? this.getCaenSecuenc() : 0L);
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenSecuenc", theCaenSecuenc), currentHashCode,
                    theCaenSecuenc);
        }
        {
            String theCaenTipoUTTramite;
            theCaenTipoUTTramite = this.getCaenTipoUTTramite();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenTipoUTTramite", theCaenTipoUTTramite),
                    currentHashCode, theCaenTipoUTTramite);
        }
        {
            String theCaenCodUTTramite;
            theCaenCodUTTramite = this.getCaenCodUTTramite();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenCodUTTramite", theCaenCodUTTramite),
                    currentHashCode, theCaenCodUTTramite);
        }
        {
            String theCaenLote;
            theCaenLote = this.getCaenLote();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenLote", theCaenLote), currentHashCode,
                    theCaenLote);
        }
        {
            Long theCaenCodSituacEnvio;
            theCaenCodSituacEnvio = this.getCaenCodSituacEnvio();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenCodSituacEnvio", theCaenCodSituacEnvio),
                    currentHashCode, theCaenCodSituacEnvio);
        }
        {
            XMLGregorianCalendar theCaenFechaGen;
            theCaenFechaGen = this.getCaenFechaGen();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenFechaGen", theCaenFechaGen),
                    currentHashCode, theCaenFechaGen);
        }
        {
            XMLGregorianCalendar theCaenFechaEnv;
            theCaenFechaEnv = this.getCaenFechaEnv();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenFechaEnv", theCaenFechaEnv),
                    currentHashCode, theCaenFechaEnv);
        }
        {
            XMLGregorianCalendar theCaenFechaRecep;
            theCaenFechaRecep = this.getCaenFechaRecep();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenFechaRecep", theCaenFechaRecep),
                    currentHashCode, theCaenFechaRecep);
        }
        {
            long theCaenTotSoli;
            theCaenTotSoli = (true ? this.getCaenTotSoli() : 0L);
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenTotSoli", theCaenTotSoli), currentHashCode,
                    theCaenTotSoli);
        }
        {
            XMLGregorianCalendar theCaenFechaBD;
            theCaenFechaBD = this.getCaenFechaBD();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenFechaBD", theCaenFechaBD), currentHashCode,
                    theCaenFechaBD);
        }
        {
            Long theCaenNumSoliErr;
            theCaenNumSoliErr = this.getCaenNumSoliErr();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenNumSoliErr", theCaenNumSoliErr),
                    currentHashCode, theCaenNumSoliErr);
        }
        {
            Long theCaenNumSoliCarg;
            theCaenNumSoliCarg = this.getCaenNumSoliCarg();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenNumSoliCarg", theCaenNumSoliCarg),
                    currentHashCode, theCaenNumSoliCarg);
        }
        {
            String theCaenTipoEnvio;
            theCaenTipoEnvio = this.getCaenTipoEnvio();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenTipoEnvio", theCaenTipoEnvio),
                    currentHashCode, theCaenTipoEnvio);
        }
        {
            String theCaenTipoUTGen;
            theCaenTipoUTGen = this.getCaenTipoUTGen();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenTipoUTGen", theCaenTipoUTGen),
                    currentHashCode, theCaenTipoUTGen);
        }
        {
            String theCaenCodUTGen;
            theCaenCodUTGen = this.getCaenCodUTGen();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenCodUTGen", theCaenCodUTGen),
                    currentHashCode, theCaenCodUTGen);
        }
        {
            String theCaenLoteGen;
            theCaenLoteGen = this.getCaenLoteGen();
            currentHashCode = strategy.hashCode(
                    LocatorUtils.property(locator, "caenLoteGen", theCaenLoteGen), currentHashCode,
                    theCaenLoteGen);
        }
        return currentHashCode;
    }

    public int hashCode()
    {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
