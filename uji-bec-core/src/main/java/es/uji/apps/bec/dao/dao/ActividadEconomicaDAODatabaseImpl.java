package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.bec.model.ActividadEconomica;
import es.uji.apps.bec.model.QActividadEconomica;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ActividadEconomicaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        ActividadEconomicaDAO
{
    @Override
    public List<ActividadEconomica> getActividadesEconomicasByMiembroId(Long miembroId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActividadEconomica actividadEconomica = QActividadEconomica.actividadEconomica;

        query.from(actividadEconomica).where(actividadEconomica.miembro.id.eq(miembroId))
                .orderBy(actividadEconomica.cifSociedad.asc());

        return query.list(actividadEconomica);
    }

    @Override
    @Transactional
    public void updateActividadEconomica(ActividadEconomica actividadEconomica)
    {
        QActividadEconomica qActividadEconomica = QActividadEconomica.actividadEconomica;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qActividadEconomica);

        updateClause
                .where(qActividadEconomica.id.eq(actividadEconomica.getId()))
                .set(qActividadEconomica.cifSociedad, actividadEconomica.getCifSociedad())
                .set(qActividadEconomica.impParticipacion, actividadEconomica.getImpParticipacion())
                .set(qActividadEconomica.numSociedad, actividadEconomica.getNumSociedad())
                .set(qActividadEconomica.porcParticipacion,
                        actividadEconomica.getPorcParticipacion()).execute();
    }

    @Override
    @Transactional
    public void deleteActividadEconomicaById(Long actividadEconomicaId)
    {

        QActividadEconomica qActividadEconomica = QActividadEconomica.actividadEconomica;

        JPADeleteClause delete = new JPADeleteClause(entityManager, qActividadEconomica);
        delete.where(qActividadEconomica.id.eq(actividadEconomicaId)).execute();

    }
}
