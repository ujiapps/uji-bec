package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.QOrganismo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganismoDAODatabaseImpl extends BaseDAODatabaseImpl implements OrganismoDAO
{
    private QOrganismo organismo = QOrganismo.organismo;

    @Override
    public List<Organismo> getOrganismos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(organismo).orderBy(organismo.nombre.asc());
        return query.list(organismo);
    }

    @Override
    public Organismo getOrganismoById(Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(organismo).where(organismo.id.eq(organismoId));
        return query.uniqueResult(organismo);
    }

    @Override
    @Transactional
    public void deleteOrganismo(Long organismoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, organismo);
        delete.where(organismo.id.eq(organismoId)).execute();
    }
}
