package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.commons.db.BaseDAO;

public interface BecaDenegacionDAO extends BaseDAO
{
    List<BecaDenegacion> getDenegacionesByBecaId(Long becaId);

    List<BecaDenegacion> getDenegacionesByBecaIdAndOrden(Long becaId, Long orden);
}
