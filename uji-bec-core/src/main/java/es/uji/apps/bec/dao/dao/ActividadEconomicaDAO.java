package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.ActividadEconomica;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface ActividadEconomicaDAO extends BaseDAO
{
    List<ActividadEconomica> getActividadesEconomicasByMiembroId(Long miembroId);

    void updateActividadEconomica(ActividadEconomica actividadEconomica);

    void deleteActividadEconomicaById(Long actividadEconomicaId);
}
