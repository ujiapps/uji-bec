package es.uji.apps.bec.dao.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QResolucionEconomico;
import es.uji.apps.bec.model.ResolucionEconomico;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResolucionEconomicoDAODatabaseImpl extends BaseDAODatabaseImpl implements ResolucionEconomicoDAO
{
    @Override
    public ResolucionEconomico getResolucionEconomicoById(Long economicosId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QResolucionEconomico resolucionEconomico = QResolucionEconomico.resolucionEconomico;

        query.from(resolucionEconomico).where(resolucionEconomico.id.eq(economicosId));

        if (query.list(resolucionEconomico).size() > 0)
        {
            return query.list(resolucionEconomico).get(0);
        }
        return null;
    }
}
