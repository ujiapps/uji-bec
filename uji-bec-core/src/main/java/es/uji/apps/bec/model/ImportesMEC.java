package es.uji.apps.bec.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.ImportesMECDAO;

/**
 * The persistent class for the BC2_EXT_IMPORTES_MEC database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_EXT_IMPORTES_MEC")
public class ImportesMEC implements Serializable
{
    private String apellido1;

    private String apellido2;

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "BECA_ID")
    private Long becaId;

    private String conv;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CREDITOS_CUBIERTOS")
    private BigDecimal creditosCubiertos;

    @Column(name = "CREDITOS_1A")
    private BigDecimal creditos1a;

    @Column(name = "CREDITOS_2A")
    private BigDecimal creditos2a;

    @Column(name = "CREDITOS_3A")
    private BigDecimal creditos3a;

    @Column(name = "NOTA_MEDIA")
    private BigDecimal notaMedia;

    @Column(name = "IMPORTES_1A")
    private BigDecimal importes1a;

    @Column(name = "IMPORTES_T1A")
    private BigDecimal importesT1a;

    @Column(name = "IMPORTES_2A")
    private BigDecimal importes2a;

    @Column(name = "IMPORTES_3A")
    private BigDecimal importes3a;

    @Column(name = "CURSO_ACA")
    private String cursoAca;

    @Column(name = "CAUSA_AYUDA_MEC")
    private String causaAyudaMec;

    @Column(name = "SEXO")
    private String sexo;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoAcademicoId;

    @Column(name = "ESTADO_ID")
    private Long estadoId;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    private String estudio;

    @Column(name = "RUCT")
    private Long ruct;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    @Column(name = "CENTRO_ID")
    private Long centroId;

    private String centro;

    private String nif;

    private String nombre;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "PROCESO_ID")
    private Long procesoId;

    @Column(name = "SOLICITANTE_ID")
    private Long solicitanteId;

    @Column(name = "TASAS_CONSELLERIA")
    private BigDecimal tasasConselleria;

    @Column(name = "TASAS_MINISTERIO")
    private BigDecimal tasasMinisterio;

    @Column(name = "TASAS_MINISTERIO_TOTAL")
    private BigDecimal tasasMinisterioTotal;

    @Column(name = "TASAS_TOTAL")
    private BigDecimal tasasTotal;

    @Column(name = "TIPO_FAMILIA_ID")
    private Long tipoFamiliaId;

    private String ut;

    private static ImportesMECDAO importesMECDAO;

    @Autowired
    public void setImportesMEC(ImportesMECDAO importesMECDAO)
    {
        ImportesMEC.importesMECDAO = importesMECDAO;
    }

    public ImportesMEC()
    {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return this.apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public Long getBecaId()
    {
        return this.becaId;
    }

    public void setBecaId(Long becaId)
    {
        this.becaId = becaId;
    }

    public String getConv()
    {
        return this.conv;
    }

    public void setConv(String conv)
    {
        this.conv = conv;
    }

    public Long getConvocatoriaId()
    {
        return this.convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public BigDecimal getCreditosCubiertos()
    {
        return this.creditosCubiertos;
    }

    public void setCreditosCubiertos(BigDecimal creditosCubiertos)
    {
        this.creditosCubiertos = creditosCubiertos;
    }

    public BigDecimal getCreditos1a()
    {
        return this.creditos1a;
    }

    public void setCreditos1a(BigDecimal creditos1a)
    {
        this.creditos1a = creditos1a;
    }

    public BigDecimal getCreditos2a()
    {
        return this.creditos2a;
    }

    public void setCreditos2a(BigDecimal creditos2a)
    {
        this.creditos2a = creditos2a;
    }

    public BigDecimal getCreditos3a()
    {
        return this.creditos3a;
    }

    public void setCreditos3a(BigDecimal creditos3a)
    {
        this.creditos3a = creditos3a;
    }

    public BigDecimal getNotaMedia()
    {
        return notaMedia;
    }

    public void setNotaMedia(BigDecimal notaMedia)
    {
        this.notaMedia = notaMedia;
    }

    public BigDecimal getImportesT1a()
    {
        return this.importesT1a;
    }

    public void setImportesT1a(BigDecimal importesT1a)
    {
        this.importesT1a = importesT1a;
    }

    public BigDecimal getImportes1a()
    {
        return this.importes1a;
    }

    public void setImportes1a(BigDecimal importes1a)
    {
        this.importes1a = importes1a;
    }

    public BigDecimal getImportes2a()
    {
        return this.importes2a;
    }

    public void setImportes2a(BigDecimal importes2a)
    {
        this.importes2a = importes2a;
    }

    public BigDecimal getImportes3a()
    {
        return this.importes3a;
    }

    public void setImportes3a(BigDecimal importes3a)
    {
        this.importes3a = importes3a;
    }

    public String getCursoAca()
    {
        return this.cursoAca;
    }

    public void setCursoAca(String cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getCausaAyudaMec()
    {
        return this.causaAyudaMec;
    }

    public void setCausaAyudaMec(String causaAyudaMec)
    {
        this.causaAyudaMec = causaAyudaMec;
    }


    public String getSexo()
    {
        return this.sexo;
    }

    public void setSexo(String sexo)
    {
        this.sexo = sexo;
    }


    public Long getCursoAcademicoId()
    {
        return this.cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Long getEstadoId()
    {
        return this.estadoId;
    }

    public void setEstadoId(Long estadoId)
    {
        this.estadoId = estadoId;
    }

    public Long getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getRuct() {return this.ruct;}

    public void setRuct(Long ruct)
    {
        this.ruct = ruct;
    }

    public String getEstudio()
    {
        return estudio;
    }

    public void setEstudio(String estudio)
    {
        this.estudio = estudio;
    }

    public String getTipoEstudio()
    {
        return this.tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Long getCentroId()
    {
        return centroId;
    }

    public void setCentroId(Long centroId)
    {
        this.centroId = centroId;
    }

    public String getCentro()
    {
        return centro;
    }

    public void setCentro(String centro)
    {
        this.centro = centro;
    }

    public String getNif()
    {
        return this.nif;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getProcesoId()
    {
        return this.procesoId;
    }

    public void setProcesoId(Long procesoId)
    {
        this.procesoId = procesoId;
    }

    public Long getSolicitanteId()
    {
        return this.solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId)
    {
        this.solicitanteId = solicitanteId;
    }

    public BigDecimal getTasasConselleria()
    {
        return this.tasasConselleria;
    }

    public void setTasasConselleria(BigDecimal tasasConselleria)
    {
        this.tasasConselleria = tasasConselleria;
    }

    public BigDecimal getTasasMinisterio()
    {
        return this.tasasMinisterio;
    }

    public void setTasasMinisterio(BigDecimal tasasMinisterio)
    {
        this.tasasMinisterio = tasasMinisterio;
    }

    public BigDecimal getTasasMinisterioTotal()
    {
        return this.tasasMinisterioTotal;
    }

    public void setTasasMinisterioTotal(BigDecimal tasasMinisterioTotal)
    {
        this.tasasMinisterioTotal = tasasMinisterioTotal;
    }

    public BigDecimal getTasasTotal()
    {
        return this.tasasTotal;
    }

    public void setTasasTotal(BigDecimal tasasTotal)
    {
        this.tasasTotal = tasasTotal;
    }

    public Long getTipoFamiliaId()
    {
        return this.tipoFamiliaId;
    }

    public void setTipoFamiliaId(Long tipoFamiliaId)
    {
        this.tipoFamiliaId = tipoFamiliaId;
    }

    public String getUt()
    {
        return this.ut;
    }

    public void setUt(String ut)
    {
        this.ut = ut;
    }


    public static List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdIncidencias(Long convocatoriaId, Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByConvocatoriaIdCursoAcademicoIdIncidencias(convocatoriaId, cursoAcademicoId);
    }

    public static List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(Long convocatoriaId, Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByConvocatoriaIdCursoAcademicoIdGrados(convocatoriaId, cursoAcademicoId);
    }

    public static List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(Long convocatoriaId, Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByConvocatoriaIdCursoAcademicoIdMaster(convocatoriaId, cursoAcademicoId);
    }

    public static List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoId(Long convocatoriaId, Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByConvocatoriaIdCursoAcademicoId(convocatoriaId, cursoAcademicoId);
    }

    public static List<ImportesMEC> getImportesMECByConvocatoriaIdCursoAcademicoIdFamiliasNumerosas(Long convocatoriaId, Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByConvocatoriaIdCursoAcademicoIdFamiliasNumerosas(convocatoriaId, cursoAcademicoId);
    }

    public static List<ImportesMEC> getImportesMECByCursoAcademicoId(Long cursoAcademicoId)
    {
        return importesMECDAO.getImportesMECByCursoAcademicoId(cursoAcademicoId);
    }

}
