package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.bec.model.CuantiaCurso;
import es.uji.apps.bec.model.QCuantia;
import es.uji.apps.bec.model.QCuantiaCurso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static es.uji.apps.bec.model.QCuantiaCurso.cuantiaCurso;

@Repository
public class CuantiaCursoDAODatabaseImpl extends BaseDAODatabaseImpl implements CuantiaCursoDAO
{
    @Override
    public List<CuantiaCurso> getCuantiasCurso()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cuantiaCurso).orderBy(cuantiaCurso.cursoAcademico.id.desc(), cuantiaCurso.cuantia.codigo.asc());
        return query.list(cuantiaCurso);
    }

    @Override
    public CuantiaCurso getCuantiaCursoById(Long cuantiaCursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cuantiaCurso).where(cuantiaCurso.id.eq(cuantiaCursoId));
        return query.uniqueResult(cuantiaCurso);
    }
    
    @Override
    public List<CuantiaCurso> getCuantiasCursoByCursoAcademicoyConvocatoria(Long cursoAcademicoId,
            Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCuantiaCurso cuantiaCurso = QCuantiaCurso.cuantiaCurso;
        QCuantia cuantia = QCuantia.cuantia;
        query.from(cuantiaCurso)
                .join(cuantiaCurso.cuantia, cuantia)
                .where(cuantiaCurso.cursoAcademico.id.eq(cursoAcademicoId)
                        .and(cuantia.convocatoria.id.eq(convocatoriaId)));
        return query.list(cuantiaCurso);
    }

    @Override
    public CuantiaCurso getCuantiaCursoByCursoAcademicoAndCuantia(Long cursoAcademicoId,
            Long cuantiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCuantiaCurso cuantiaCurso = QCuantiaCurso.cuantiaCurso;
        query.from(cuantiaCurso).where(
                cuantiaCurso.cursoAcademico.id.eq(cursoAcademicoId).and(
                        cuantiaCurso.cuantia.id.eq(cuantiaId)));
        return query.list(cuantiaCurso).get(0);
    }

    @Override
    @Transactional
    public void deleteCuantiaCurso(Long cuantiaCursoId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, cuantiaCurso);
        delete.where(cuantiaCurso.id.eq(cuantiaCursoId)).execute();
    }
}
