package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Minimo;
import es.uji.commons.db.BaseDAO;

public interface MinimoDAO extends BaseDAO
{
    public List<Minimo> getMinimosByEstudioId(Long estudioId, Long cursoAcademicoId);
}
