package es.uji.apps.bec.dao.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.BecaEstudio;
import es.uji.apps.bec.model.QBecaEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class BecaEstudioDAODatabaseImpl extends BaseDAODatabaseImpl implements BecaEstudioDAO
{

    @Override
    public BecaEstudio getBecaEstudioByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QBecaEstudio qBecaEstudio = QBecaEstudio.becaEstudio;

        query.from(qBecaEstudio).where(
                qBecaEstudio.becaId.eq(becaId));
// .and(qBecaEstudio.cursoAcademicoActivo.eq(true)) si es de años anteriores el curso puede que no sea el activo.
        
        return query.uniqueResult(qBecaEstudio);
    }

}
