package es.uji.apps.bec.exceptions;

import es.uji.apps.bec.exceptions.GeneralBECException;

@SuppressWarnings("serial")
public class BecaEnEstadoNoModificableException extends GeneralBECException
{
    public BecaEnEstadoNoModificableException()
    {
        super(
                "L\'estat de la beca no permiteix modificar dades. Per a modificar cal que estigue en estat Pendent, Treballada o Incorrecta");
    }

    public BecaEnEstadoNoModificableException(String message)
    {
        super(message);
    }
}
