package es.uji.apps.bec.model;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.webservices.ministerio.solicitudes.*;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.sso.AccessManager;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.domains.CodigoConvocatoria;
import es.uji.apps.bec.model.domains.CodigoCuantia;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.model.domains.CodigoProceso;
import es.uji.apps.bec.model.domains.CodigoProvincia;
import es.uji.apps.bec.model.domains.MapeadorTipoSexo;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the BC2_BECAS database table. *
 */
@Component
@Entity
@Table(name = "BC2_BECAS")
@SuppressWarnings("serial")
public class Beca implements Serializable
{
    public static final Long ORDEN_UNO_DENEGACION = 1L;
    public static final Long ORDEN_DOS_DENEGACION = 2L;
    public static final long TIPO_RESIDENCIA_FAMILIAR = 1L;
    public static final long TIPO_RESIDENCIA_OTROS_FAMILIARES = 4L;
    private static final Long ORGANISMO_MINISTERIO = 1L;
    private static final String CLASE_ESTUDIO = "Estudio";
    private static final List<Long> COMUNIDAD_VALENCIANA = Arrays.asList(
            CodigoProvincia.CASTELLON.getId(), CodigoProvincia.VALENCIA.getId(),
            CodigoProvincia.ALICANTE.getId());
    private static final String ACCESO_MAYORES_25 = "39020001";
    private static final Float DISTANCIA_MAXIMA = 50F;
    private static BecaDAO becaDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "AUTORIZACION_RENTA")
    private Boolean autorizacionRenta;
    @Column(name = "BECA_CURSO_ANT")
    private Boolean becaCursoAnt;
    @Column(name = "BECA_CURSO_ANT_2")
    private Boolean becaCursoAnt2;
    @Column(name = "BECA_CURSO_ANT_3")
    private Boolean becaCursoAnt3;
    @Column(name = "BECA_PARCIAL")
    private Boolean becaParcial;
    @Column(name = "CAUSA_OTROS")
    private String causaOtros;
    @Column(name = "CODIGO_ARCHIVO_TEMPORAL")
    private String codigoArchivoTemporal;
    @Column(name = "CODIGO_BECA")
    private String codigoBeca;
    @Column(name = "CODIGO_CENTRO_ANT")
    private String codigoCentroAnt;
    @Column(name = "CODIGO_ESTUDIO_ANT")
    private String codigoEstudioAnt;
    @Column(name = "CODIGO_ESTUDIO_UJI_ANT")
    private String codigoEstudioUjiAnt;
    @Column(name = "CREDITOS_CONVALIDADOS_ANT")
    private Float creditosConvalidadosAnt;
    @Column(name = "CREDITOS_ESTUDIO_ANT")
    private Float creditosEstudioAnt;
    @Column(name = "CREDITOS_MATRICULADOS_ANT")
    private Float creditosMatriculadosAnt;
    @Column(name = "CREDITOS_SUPERADOS_ANT")
    private Float creditosSuperadosAnt;
    @Column(name = "CREDITOS_SUSPENSOS_ANT")
    private Float creditosSuspensosAnt;
    @Column(name = "CUENTA_BANCARIA")
    private String cuentaBancaria;
    private Long curso;
    @Column(name = "CURSO_ACADEMICO_ANT")
    private Long cursoAcademicoAnt;
    @Column(name = "DIGITOS_CONTROL")
    private String digitosControl;
    @Column(name = "DISTANCIA_AL_CENTRO")
    private Long distanciaAlCentro;
    private String entidad;
    @Column(name = "ESTUDIO_ANT")
    private String estudioAnt;
    @Column(name = "ESTUDIO_ANT_OTROS")
    private String estudioAntOtros;
    @Column(name = "ESTUDIOS_SIN_DOCENCIA")
    private Boolean estudiosSinDocencia;
    @Column(name = "FAMILIA_ESPECIALIDAD")
    private String familiaEspecialidad;
    private String identificacion;
    @Column(name = "LIMITAR_CREDITOS")
    private Boolean limitarCreditos;
    @Column(name = "SMS")
    private Boolean sms;
    @Column(name = "LIMITAR_CREDITOS_FIN_ESTUDIOS")
    private Boolean limitarCreditosFinEstudios;
    @Column(name = "MATRICULA_PARCIAL")
    private Boolean matriculaParcial;
    @Column(name = "MATRICULA_PARCIAL_ANT")
    private Boolean matriculaParcialAnt;
    @Column(name = "NOTA_MEDIA_ANT")
    private Float notaMediaAnt;
    @Column(name = "NOTA_MEDIA_ULTIMO_CURSO")
    private Float notaMediaUltimoCurso;
    @Column(name = "NUMERO_SEMESTRES")
    private Long numeroSemestres;
    @Column(name = "NUMERO_TRANSPORTES")
    private Long numeroTransportes;
    private String observaciones;
    @Column(name = "OBSERVACIONES_UJI")
    private String observacionesUji;
    private Boolean presencial;
    @Column(name = "PROGRAMA_INTERCAMBIO")
    private Boolean programaIntercambio;
    private String sucursal;
    @Column(name = "TIENE_TITULO_UNIVERSITARIO")
    private Boolean tieneTituloUniversitario;
    @Column(name = "TITULO_NOMBRE")
    private String tituloNombre;
    @Column(name = "TRANSPORTE_BARCO_AVION")
    private Boolean transporteBarcoAvion;
    @Column(name = "TRANSPORTE_URBANO")
    private Boolean transporteUrbano;
    @Column(name = "CREDITOS_MATRICULADOS")
    private Float creditosMatriculados;
    @Column(name = "CREDITOS_CONVALIDADOS")
    private Float creditosConvalidados;
    @Column(name = "CREDITOS_PEND_CONVA")
    private Float creditosPendientesConvalidacion;
    @Column(name = "CREDITOS_FUERA_BECA")
    private Float creditosFueraBeca;
    @Column(name = "CUMPLE_ACADEMICOS")
    private Boolean cumpleAcademicos;
    @Column(name = "BECAS_CONCEDIDAS_ANT")
    private Long becasConcedidasAnt;
    @Column(name = "CUMPLE_ACAD_BECA_ANT")
    private Boolean cumpleAcadBecaAnt;
    @Column(name = "CREDITOS_PARA_BECA")
    private Float creditosParaBeca;
    @Column(name = "RENDIMIENTO_ACADEMICO")
    private Boolean rendimientoAcademico;
    @Column(name = "POSEE_TITULO")
    private Boolean poseeTitulo;
    @Column(name = "ALGUNA_BECA_PARCIAL")
    private Boolean algunaBecaParcial;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_NOTIFICACION")
    private Date fechaNotificacion;
    @Column(name = "NOMBRE_CENTRO_ANT")
    private String nombreCentroAnt;
    @Column(name = "TITULO_ESPANOL")
    private Boolean tituloEspanol;
    @Column(name = "ESTUDIOS_HOMOLOGADOS")
    private Boolean estudiosHomologados;
    @Column(name = "UNIV_TITULACION")
    private String univTitulacion;
    @Column(name = "IND_COEF_CORRECTOR")
    private Boolean indCoefCorrector;
    @Column(name = "NOTA_CORREGIDA")
    private Float notaCorregida;
    @Column(name = "COD_TITU_ACCESO")
    private String codTituAcceso;
    @Column(name = "PLAN_ANTERIOR_NUM_CURSOS")
    private Long planAnteriorNumCursos;
    @Column(name = "TIPO_TITULACION")
    private String tipoTitulacion;
    @Column(name = "BECA_CONCEDIDA")
    private Boolean becaConcedida;
    @Column(name = "IBANPAIS")
    private String ibanPais;
    @Column(name = "IBANDC")
    private String ibanDC;
    @Column(name = "NOTA_PRUEBA_ESPECIFICA")
    private Float notaPruebaEspecifica;
    @Column(name = "PROCEDENCIA_NOTA")
    private String procedenciaNota;
    @Column(name = "NOTA_MEDIA_SIN_SUSPENSO")
    private Float notaMediaSinSuspenso;
    @Column(name = "RECLAMADA")
    private Boolean reclamada;
    @Column(name = "CREDITOS_1A")
    private Float creditosPrimeraMatricula;
    @Column(name = "CREDITOS_2A")
    private Float creditosSegundaMatricula;
    @Column(name = "CREDITOS_3A")
    private Float creditosTerceraMatricula;
    @Column(name = "CREDITOS_3AP")
    private Float creditosTerceraMatriculaPosteriores;
    @Column(name = "CREDITOS_4Ap")
    private Float creditosCuartaMatriculaPosteriores;
    @Column(name = "OTRA_BECA_CONCEDIDA")
    private Boolean otraBecaConcedida;
    @Column(name = "CODIGO_EXPEDIENTE")
    private String codigoExpediente;
    @Column(name = "CREDITOS_COVID19")
    private Float creditosCovid19;
    @Column(name = "CAMBIO_ESTUDIOS")
    private Boolean cambioEstudios;

    // bi-directional many-to-one association to Convocatoria
    @ManyToOne
    private Convocatoria convocatoria;
    // bi-directional many-to-one association to Estado
    @ManyToOne
    private Estado estado;
    // bi-directional many-to-one association to Estudio
    @ManyToOne
    private Estudio estudio;
    // bi-directional many-to-one association to Proceso
    @ManyToOne
    private Proceso proceso;
    // bi-directional many-to-one association to Solicitante
    @ManyToOne
    private Solicitante solicitante;
    // bi-directional many-to-one association to Tanda
    @ManyToOne
    private Tanda tanda;
    // bi-directional many-to-one association to TipoIdentificacion
    @ManyToOne
    @JoinColumn(name = "TIPO_IDENTIFICACION_ID")
    private TipoIdentificacion tipoIdentificacion;
    // bi-directional many-to-one association to TipoMatricula
    @ManyToOne
    @JoinColumn(name = "TIPO_MATRICULA_ID")
    private TipoMatricula tipoMatricula;
    // bi-directional many-to-one association to CuantiaBeca
    @OneToMany(mappedBy = "beca")
    private Set<CuantiaBeca> cuantiasBecas;
    // bi-directional many-to-one association to Documentos
    @OneToMany(mappedBy = "beca")
    private Set<Documento> documentos;
    // bi-directional many-to-one association to HistoricoBeca
    @OneToMany(mappedBy = "beca")
    private Set<HistoricoBeca> historicosBecas;
    // bi-directional many-to-one association to BecaDenegacion
    @OneToMany(mappedBy = "beca")
    private Set<BecaDenegacion> becasDenegaciones;
    // bi-directional many-to-one association to RegistroErroresEnvio
    @OneToMany(mappedBy = "beca")
    private Set<RegistroErroresEnvio> registroErroresEnvios;
    @OneToMany(mappedBy = "beca", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Miembro> miembros;
    @OneToMany(mappedBy = "beca", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Domicilio> domicilios;
    // bi-directional one-to-one association to Recurso
    @OneToOne(mappedBy = "beca")
    private Recurso recurso;

    public Beca(Solicitante solicitante, String identificacion, String codigoArchivoTemporal, Long estudioId,
                Long convocatoriaId)
    {
        this();

        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(convocatoriaId);

        this.setSolicitante(solicitante);

        this.setConvocatoria(convocatoria);
        this.setIdentificacion(identificacion);

        this.setEstado(Estado.getEstadoById(CodigoEstado.PENDIENTE.getId()));
        this.setProceso(Proceso.getProcesoById(1L));

        if (codigoArchivoTemporal != null && !codigoArchivoTemporal.isEmpty())
        {
            this.setCodigoArchivoTemporal(codigoArchivoTemporal);
        }

        if (estudioId != null)
        {
            Estudio estudio = new Estudio();
            estudio.setId(estudioId);
            this.setEstudio(estudio);
        }
    }

    public Beca()
    {
        this.setTieneTituloUniversitario(false);
        this.setBecaCursoAnt(false);
        this.setBecaCursoAnt2(false);
        this.setBecaCursoAnt3(false);
        this.setMatriculaParcialAnt(false);
        this.setMatriculaParcial(false);
        this.setEstudiosSinDocencia(false);
        this.setPresencial(false);
        this.setLimitarCreditos(false);
        this.setLimitarCreditosFinEstudios(false);
        this.setBecaParcial(false);
        this.setTransporteUrbano(false);
        this.setTransporteBarcoAvion(false);
        this.setAutorizacionRenta(false);
        this.setProgramaIntercambio(false);
        this.setCumpleAcademicos(false);
        this.setCumpleAcadBecaAnt(false);
        this.setRendimientoAcademico(false);
        this.setPoseeTitulo(false);
        this.setAlgunaBecaParcial(false);
        this.setBecaCursoAnt(false);
        this.setSms(false);
        this.setTituloEspanol(false);
        this.setEstudiosHomologados(false);
        this.setIndCoefCorrector(false);
        this.setBecaConcedida(false);
        this.setReclamada(false);
        this.setOtraBecaConcedida(false);
        this.setCambioEstudios(false);
    }

    public void setTieneTituloUniversitario(Boolean tieneTituloUniversitario)
    {
        this.tieneTituloUniversitario = tieneTituloUniversitario;
    }

    public void setBecaCursoAnt(Boolean becaCursoAnt)
    {
        this.becaCursoAnt = becaCursoAnt;
    }

    public void setBecaCursoAnt2(Boolean becaCursoAnt2)
    {
        this.becaCursoAnt2 = becaCursoAnt2;
    }

    public void setBecaCursoAnt3(Boolean becaCursoAnt3)
    {
        this.becaCursoAnt3 = becaCursoAnt3;
    }

    public void setMatriculaParcialAnt(Boolean matriculaParcialAnt)
    {
        this.matriculaParcialAnt = matriculaParcialAnt;
    }

    public void setMatriculaParcial(Boolean matriculaParcial)
    {
        this.matriculaParcial = matriculaParcial;
    }

    public void setEstudiosSinDocencia(Boolean estudiosSinDocencia)
    {
        this.estudiosSinDocencia = estudiosSinDocencia;
    }

    public void setPresencial(Boolean presencial)
    {
        this.presencial = presencial;
    }

    public void setLimitarCreditos(Boolean limitarCreditos)
    {
        this.limitarCreditos = limitarCreditos;
    }

    public void setLimitarCreditosFinEstudios(Boolean limitarCreditosFinEstudios)
    {
        this.limitarCreditosFinEstudios = limitarCreditosFinEstudios;
    }

    public void setBecaParcial(Boolean becaParcial)
    {
        this.becaParcial = becaParcial;
    }

    public void setTransporteUrbano(Boolean transporteUrbano)
    {
        this.transporteUrbano = transporteUrbano;
    }

    public void setTransporteBarcoAvion(Boolean transporteBarcoAvion)
    {
        this.transporteBarcoAvion = transporteBarcoAvion;
    }

    public void setAutorizacionRenta(Boolean autorizacionRenta)
    {
        this.autorizacionRenta = autorizacionRenta;
    }

    public void setProgramaIntercambio(Boolean programaIntercambio)
    {
        this.programaIntercambio = programaIntercambio;
    }

    public void setCumpleAcademicos(Boolean cumpleAcademicos)
    {
        this.cumpleAcademicos = cumpleAcademicos;
    }

    public void setCumpleAcadBecaAnt(Boolean cumpleAcadBecaAnt)
    {
        this.cumpleAcadBecaAnt = cumpleAcadBecaAnt;
    }

    public void setRendimientoAcademico(Boolean rendimientoAcademico)
    {
        this.rendimientoAcademico = rendimientoAcademico;
    }

    public void setPoseeTitulo(Boolean poseeTitulo)
    {
        this.poseeTitulo = poseeTitulo;
    }

    public void setAlgunaBecaParcial(Boolean algunaBecaParcial)
    {
        this.algunaBecaParcial = algunaBecaParcial;
    }

    public void setSms(Boolean sms)
    {
        this.sms = sms;
    }

    public void setTituloEspanol(Boolean tituloEspanol)
    {
        this.tituloEspanol = tituloEspanol;
    }

    public void setEstudiosHomologados(Boolean estudiosHomologados)
    {
        this.estudiosHomologados = estudiosHomologados;
    }

    public void setIndCoefCorrector(Boolean indCoefCorrector)
    {
        this.indCoefCorrector = indCoefCorrector;
    }

    public void setBecaConcedida(Boolean becaConcedida)
    {
        this.becaConcedida = becaConcedida;
    }

    public void setReclamada(Boolean reclamada)
    {
        this.reclamada = reclamada;
    }

    public void setOtraBecaConcedida(Boolean otraBecaConcedida)
    {
        this.otraBecaConcedida = otraBecaConcedida;
    }

    public void setCambioEstudios(Boolean cambioEstudios)
    {
        this.cambioEstudios = cambioEstudios;
    }

    public static boolean existeBeca(Long solicitanteId, Long convocatoriaId, Long estudioId)
    {
        return becaDAO.existeBeca(solicitanteId, convocatoriaId, estudioId);
    }

    public static boolean existenBecasConEstudio(Long solicitanteId, Long convocatoriaId)
    {
        return becaDAO.existenBecasConEstudio(solicitanteId, convocatoriaId);
    }

    public static boolean existeBecaSinEstudio(Long solicitanteId, Long convocatoriaId)
    {
        return becaDAO.existeBeca(solicitanteId, convocatoriaId, null);
    }

    public static Beca getBeca(Long solicitanteId, Long convocatoriaId, Long estudioId)
    {
        return becaDAO.getBeca(solicitanteId, convocatoriaId, estudioId);
    }

    public static List<Beca> getHistoricoObservacionesByBeca(Long becaId)
    {
        Long personaId = Beca.getBecaById(becaId).getSolicitante().getPersona().getId();

        return becaDAO.getHistoricoObservacionesByBecaAndPersona(becaId, personaId);
    }

    public static Beca getBecaById(Long becaId)
    {
        return becaDAO.getBecaById(becaId);
    }

    public Solicitante getSolicitante()
    {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante)
    {
        this.solicitante = solicitante;
    }

    public static List<HistoricoBeca> getHistoricoEstadosByBeca(Long becaId)
    {
        return becaDAO.getHistoricoEstadosByBeca(becaId);
    }

    public static Boolean isBecasPorTandaDeDistintoTipo(Long tandaId)
    {
        Long numBecas = becaDAO.getNumeroBecasPorTandaAgrupadasPorEstadoConvocatoriaYProceso(tandaId);
        return (numBecas > 1L);
    }

    public static Boolean isBecasPorTandaEnEstadoNoDenegado(Long tandaId)
    {
        Long numBecas = becaDAO.getNumeroBecasPorTandaEnEstadoNoDenegado(tandaId);
        return (numBecas > 0L);
    }

    public static boolean isBecasPorTandaEnEstadoNoEnviado(Long tandaId)
    {
        Long numBecas = becaDAO.getNumeroBecasPorTandaEnEstadoNoEnviado(tandaId);
        return (numBecas > 0L);
    }

    public static Long numeroBecasNoNotificadasPorTanda(Long tandaId)
    {
        return becaDAO.getResumenNumeroBecasNoNotificadasPorTanda(tandaId);
    }

    public static Boolean isBecasPorTanda(Long tandaId)
    {
        Long numBecas = becaDAO.getNumeroBecasPorTanda(tandaId);
        return (numBecas > 0L);
    }

    public static List<Beca> getBecasByTandaId(Long tandaId)
    {
        return becaDAO.getBecasByTandaId(tandaId);
    }

    public static Beca getBecasByConvocatoriaIdAndNifAndCursoAcademico(Long convocatoria,
                                                                       String nif, Long cursoAcademico)
            throws BecaException
    {
        return becaDAO.getBecasByConvocatoriaIdAndNifAndCursoAcademico(convocatoria, nif, cursoAcademico);
    }

    public static Beca getBecaByArchivoTemporal(Long archivoTemporal)
    {
        return becaDAO.getBecaByArchivoTemporal(archivoTemporal);
    }

    @Autowired
    public void setBecaDAO(BecaDAO becaDAO)
    {
        Beca.becaDAO = becaDAO;
    }

    public Boolean isAutorizacionRenta()
    {
        return this.autorizacionRenta;
    }

    public Boolean isBecaCursoAnt()
    {
        return this.becaCursoAnt;
    }

    public Boolean isBecaCursoAnt2()
    {
        return this.becaCursoAnt2;
    }

    public Boolean isBecaCursoAnt3()
    {
        return this.becaCursoAnt3;
    }

    public Boolean isBecaParcial()
    {
        return this.becaParcial;
    }

    public String getCausaOtros()
    {
        return this.causaOtros;
    }

    public void setCausaOtros(String causaOtros)
    {
        this.causaOtros = causaOtros;
    }

    public String getCodigoArchivoTemporal()
    {
        return this.codigoArchivoTemporal;
    }

    public void setCodigoArchivoTemporal(String codigoArchivoTemporal)
    {
        this.codigoArchivoTemporal = codigoArchivoTemporal;
    }

    public String getCodigoBeca()
    {
        return this.codigoBeca;
    }

    public void setCodigoBeca(String codigoBeca)
    {
        this.codigoBeca = codigoBeca;
    }

    public String getCodigoCentroAnt()
    {
        return this.codigoCentroAnt;
    }

    public void setCodigoCentroAnt(String codigoCentroAnt)
    {
        this.codigoCentroAnt = codigoCentroAnt;
    }

    public String getCodigoEstudioAnt()
    {
        return this.codigoEstudioAnt;
    }

    public void setCodigoEstudioAnt(String codigoEstudioAnt)
    {
        this.codigoEstudioAnt = codigoEstudioAnt;
    }

    public String getCodigoEstudioUjiAnt()
    {
        return this.codigoEstudioUjiAnt;
    }

    public void setCodigoEstudioUjiAnt(String codigoEstudioUjiAnt)
    {
        this.codigoEstudioUjiAnt = codigoEstudioUjiAnt;
    }

    public Float getCreditosConvalidadosAnt()
    {
        return this.creditosConvalidadosAnt;
    }

    public void setCreditosConvalidadosAnt(Float creditosConvalidadosAnt)
    {
        this.creditosConvalidadosAnt = creditosConvalidadosAnt;
    }

    public Float getCreditosEstudioAnt()
    {
        return this.creditosEstudioAnt;
    }

    public void setCreditosEstudioAnt(Float creditosEstudioAnt)
    {
        this.creditosEstudioAnt = creditosEstudioAnt;
    }

    public Float getCreditosSuperadosAnt()
    {
        return this.creditosSuperadosAnt;
    }

    public void setCreditosSuperadosAnt(Float creditosSuperadosAnt)
    {
        this.creditosSuperadosAnt = creditosSuperadosAnt;
    }

    public Float getCreditosSuspensosAnt()
    {
        return this.creditosSuspensosAnt;
    }

    public void setCreditosSuspensosAnt(Float creditosSuspensosAnt)
    {
        this.creditosSuspensosAnt = creditosSuspensosAnt;
    }

    public String getCuentaBancaria()
    {
        return this.cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria)
    {
        this.cuentaBancaria = cuentaBancaria;
    }

    public Long getCursoAcademicoAnt()
    {
        return this.cursoAcademicoAnt;
    }

    public void setCursoAcademicoAnt(Long cursoAcademicoAnt)
    {
        this.cursoAcademicoAnt = cursoAcademicoAnt;
    }

    public String getDigitosControl()
    {
        return this.digitosControl;
    }

    public void setDigitosControl(String digitosControl)
    {
        this.digitosControl = digitosControl;
    }

    public Long getDistanciaAlCentro()
    {
        return this.distanciaAlCentro;
    }

    public void setDistanciaAlCentro(Long distanciaAlCentro)
    {
        this.distanciaAlCentro = distanciaAlCentro;
    }

    public String getEntidad()
    {
        return this.entidad;
    }

    public void setEntidad(String entidad)
    {
        this.entidad = entidad;
    }

    public String getEstudioAnt()
    {
        return this.estudioAnt;
    }

    public void setEstudioAnt(String estudioAnt)
    {
        this.estudioAnt = estudioAnt;
    }

    public String getEstudioAntOtros()
    {
        return this.estudioAntOtros;
    }

    public void setEstudioAntOtros(String estudioAntOtros)
    {
        this.estudioAntOtros = estudioAntOtros;
    }

    public Boolean isEstudiosSinDocencia()
    {
        return this.estudiosSinDocencia;
    }

    public String getFamiliaEspecialidad()
    {
        return this.familiaEspecialidad;
    }

    public void setFamiliaEspecialidad(String familiaEspecialidad)
    {
        this.familiaEspecialidad = familiaEspecialidad;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public Boolean isLimitarCreditos()
    {
        return this.limitarCreditos;
    }

    public Boolean isSms()
    {
        return sms;
    }

    public Boolean isLimitarCreditosFinEstudios()
    {
        return this.limitarCreditosFinEstudios;
    }

    public Boolean isMatriculaParcial()
    {
        return this.matriculaParcial;
    }

    public Boolean isMatriculaParcialAnt()
    {
        return this.matriculaParcialAnt;
    }

    public Float getNotaMediaAnt()
    {
        return this.notaMediaAnt;
    }

    public void setNotaMediaAnt(Float notaMediaAnt)
    {
        this.notaMediaAnt = notaMediaAnt;
    }

    public Float getNotaMediaUltimoCurso()
    {
        return notaMediaUltimoCurso;
    }

    public void setNotaMediaUltimoCurso(Float notaMediaUltimoCurso)
    {
        this.notaMediaUltimoCurso = notaMediaUltimoCurso;
    }

    public Long getNumeroSemestres()
    {
        return this.numeroSemestres;
    }

    public void setNumeroSemestres(Long numeroSemestres)
    {
        this.numeroSemestres = numeroSemestres;
    }

    public Long getNumeroTransportes()
    {
        return this.numeroTransportes;
    }

    public void setNumeroTransportes(Long numeroTransportes)
    {
        this.numeroTransportes = numeroTransportes;
    }

    public String getObservacionesUji()
    {
        return this.observacionesUji;
    }

    public void setObservacionesUji(String observacionesUji)
    {
        this.observacionesUji = observacionesUji;
    }

    public Boolean isPresencial()
    {
        return this.presencial;
    }

    public Boolean isProgramaIntercambio()
    {
        return this.programaIntercambio;
    }

    public String getSucursal()
    {
        return this.sucursal;
    }

    public void setSucursal(String sucursal)
    {
        this.sucursal = sucursal;
    }

    public Boolean isTieneTituloUniversitario()
    {
        return this.tieneTituloUniversitario;
    }

    public String getTituloNombre()
    {
        return this.tituloNombre;
    }

    public void setTituloNombre(String tituloNombre)
    {
        this.tituloNombre = tituloNombre;
    }

    public Boolean isTransporteBarcoAvion()
    {
        return this.transporteBarcoAvion;
    }

    public Boolean isTransporteUrbano()
    {
        return this.transporteUrbano;
    }

    public Float getCreditosMatriculados()
    {
        return creditosMatriculados;
    }

    public void setCreditosMatriculados(Float creditosMatriculados)
    {
        this.creditosMatriculados = creditosMatriculados;
    }

    public Float getCreditosConvalidados()
    {
        return creditosConvalidados;
    }

    public void setCreditosConvalidados(Float creditosConvalidados)
    {
        this.creditosConvalidados = creditosConvalidados;
    }

    public Float getCreditosPendientesConvalidacion()
    {
        return creditosPendientesConvalidacion;
    }

    public void setCreditosPendientesConvalidacion(Float creditosPendientesConvalidacion)
    {
        this.creditosPendientesConvalidacion = creditosPendientesConvalidacion;
    }

    public Float getCreditosFueraBeca()
    {
        return creditosFueraBeca;
    }

    public void setCreditosFueraBeca(Float creditosFueraBeca)
    {
        this.creditosFueraBeca = creditosFueraBeca;
    }

    public Boolean isCumpleAcademicos()
    {
        return cumpleAcademicos;
    }

    public Boolean isCumpleAcadBecaAnt()
    {
        return cumpleAcadBecaAnt;
    }

    public Float getCreditosParaBeca()
    {
        return creditosParaBeca;
    }

    public void setCreditosParaBeca(Float creditosParaBeca)
    {
        this.creditosParaBeca = creditosParaBeca;
    }

    public Boolean isRendimientoAcademico()
    {
        return rendimientoAcademico;
    }

    public Boolean isPoseeTitulo()
    {
        return poseeTitulo;
    }

    public Boolean isAlgunaBecaParcial()
    {
        return algunaBecaParcial;
    }

    public Date getFechaNotificacion()
    {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion)
    {
        this.fechaNotificacion = fechaNotificacion;
    }

    public String getNombreCentroAnt()
    {
        return this.nombreCentroAnt;
    }

    public void setNombreCentroAnt(String nombreCentroAnt)
    {
        this.nombreCentroAnt = nombreCentroAnt;
    }

    public Boolean isTituloEspanol()
    {
        return tituloEspanol;
    }

    public Boolean isEstudiosHomologados()
    {
        return estudiosHomologados;
    }

    public String getUnivTitulacion()
    {
        return univTitulacion;
    }

    public void setUnivTitulacion(String univTitulacion)
    {
        this.univTitulacion = univTitulacion;
    }

    public Boolean isIndCoefCorrector()
    {
        return indCoefCorrector;
    }

    public Float getNotaCorregida()
    {
        return notaCorregida;
    }

    public void setNotaCorregida(Float notaCorregida)
    {
        this.notaCorregida = notaCorregida;
    }

    public String getCodTituAcceso()
    {
        return codTituAcceso;
    }

    public void setCodTituAcceso(String codTituAcceso)
    {
        this.codTituAcceso = codTituAcceso;
    }

    public Long getPlanAnteriorNumCursos()
    {
        return planAnteriorNumCursos;
    }

    public void setPlanAnteriorNumCursos(Long planAnteriorNumCursos)
    {
        this.planAnteriorNumCursos = planAnteriorNumCursos;
    }

    public String getTipoTitulacion()
    {
        return tipoTitulacion;
    }

    public void setTipoTitulacion(String tipoTitulacion)
    {
        this.tipoTitulacion = tipoTitulacion;
    }

    public Boolean isBecaConcedida()
    {
        return this.becaConcedida;
    }

    public String getIbanDC()
    {
        return ibanDC;
    }

    public void setIbanDC(String ibanDC)
    {
        this.ibanDC = ibanDC;
    }

    public String getIbanPais()
    {
        return ibanPais;
    }

    public void setIbanPais(String ibanPais)
    {
        this.ibanPais = ibanPais;
    }

    public Float getNotaMediaSinSuspenso()
    {
        return notaMediaSinSuspenso;
    }

    public void setNotaMediaSinSuspenso(Float notaMediaSinSuspenso)
    {
        this.notaMediaSinSuspenso = notaMediaSinSuspenso;
    }

    public String getProcedenciaNota()
    {
        return procedenciaNota;
    }

    public void setProcedenciaNota(String procedenciaNota)
    {
        this.procedenciaNota = procedenciaNota;
    }

    public Float getNotaPruebaEspecifica()
    {
        return notaPruebaEspecifica;
    }

    public void setNotaPruebaEspecifica(Float notaPruebaEspecifica)
    {
        this.notaPruebaEspecifica = notaPruebaEspecifica;
    }

    public TipoIdentificacion getTipoIdentificacion()
    {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion)
    {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public TipoMatricula getTipoMatricula()
    {
        return this.tipoMatricula;
    }

    public void setTipoMatricula(TipoMatricula tipoMatricula)
    {
        this.tipoMatricula = tipoMatricula;
    }

    public Boolean isReclamada()
    {
        return this.reclamada;
    }

    public Set<CuantiaBeca> getCuantiasBecas()
    {
        return this.cuantiasBecas;
    }

    public void setCuantiasBecas(Set<CuantiaBeca> cuantiasBecas)
    {
        this.cuantiasBecas = cuantiasBecas;
    }

    public Set<Documento> getDocumentoss()
    {
        return this.documentos;
    }

    public void setDocumentos(Set<Documento> documentos)
    {
        this.documentos = documentos;
    }

    public Set<HistoricoBeca> getHistoricosBecas()
    {
        return this.historicosBecas;
    }

    public void setHistoricosBecas(Set<HistoricoBeca> historicosBecas)
    {
        this.historicosBecas = historicosBecas;
    }

    public Set<BecaDenegacion> getBecasDenegaciones()
    {
        return becasDenegaciones;
    }

    public void setBecasDenegaciones(Set<BecaDenegacion> becasDenegaciones)
    {
        this.becasDenegaciones = becasDenegaciones;
    }

    public Set<RegistroErroresEnvio> getRegistroErroresEnvios()
    {
        return registroErroresEnvios;
    }

    public void setRegistroErroresEnvios(Set<RegistroErroresEnvio> registroErroresEnvios)
    {
        this.registroErroresEnvios = registroErroresEnvios;
    }

    public Recurso getRecurso()
    {
        return recurso;
    }

    public void setRecurso(Recurso recurso)
    {
        this.recurso = recurso;
    }

    public String getCodigoExpediente()
    {
        return codigoExpediente;
    }

    public void setCodigoExpediente(String codigoExpediente)
    {
        this.codigoExpediente = codigoExpediente;
    }

    @PrePersist
    public void initDefaultValues()
    {
        if (this.proceso == null)
        {
            this.proceso = new Proceso();
            this.proceso.setId(1L);
        }

        if (this.estado == null)
        {
            this.estado = new Estado();
            this.estado.setId(1L);
        }

        if (this.tipoIdentificacion == null)
        {
            this.tipoIdentificacion = new TipoIdentificacion();
            this.tipoIdentificacion.setId(1L);
        }
    }

    public void importarDatosMinisterio(SolicitudType solicitudType)
            throws EstudioSolicitadoNoMatriculadoException, EstudioMinisterioMayores25Exception,
            RegistroNoEncontradoException, BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException {
        importarRelacionMiembros(solicitudType);
        importarRelacionEstados(solicitudType);
        importarRelacionDomicilios(solicitudType);
        importarDeDatosPersonalesType(solicitudType.getDatosPersonales());
        importarDeGeneralesType(solicitudType.getGenerales());
        importarDeTitulacionType(solicitudType.getTitulacion());
        importarDeSolicitudType(solicitudType);
    }

    private void importarDeDatosPersonalesType(DatosPersonalesType datosPersonales)
    {
        if (datosPersonales != null)
        {
            importarDatosIdentificacion(datosPersonales);
        }
    }

    private void importarDatosIdentificacion(DatosPersonalesType datosPersonales)
    {
        setIdentificacion(datosPersonales.getDapeNif());
        Long tipoIdentificacionMinisterio = datosPersonales.getDapeCodDocuIdent();
        if (tipoIdentificacionMinisterio != null)
        {
            TipoIdentificacion tipoIdentificacionUji = new TipoIdentificacion();
            tipoIdentificacionUji.setId(tipoIdentificacionMinisterio);
            setTipoIdentificacion(tipoIdentificacionUji);
        }
    }

    private void importarDeGeneralesType(GeneralesType generales)
    {
        if (generales != null)
        {
            importarParametrosBaseDeGeneralesType(generales);
            importarDeUltimoCursoType(generales.getUltimoCurso());
        }
    }

    private void importarParametrosBaseDeGeneralesType(GeneralesType generales)
    {
        this.setDistanciaAlCentro(generales.getGeneDistCentro());

        if (generales.getGeneIndTranspUrbano() != null)
        {
            this.setTransporteUrbano(generales.getGeneIndTranspUrbano().equals("S"));
        }
        this.setNumeroTransportes(generales.getGeneNumTranspUrbano());
        this.setFamiliaEspecialidad(generales.getGeneEspecialidad());

        Long tipoMatriculaId = ParamUtils.parseLong(generales.getGeneCodMatri());
        TipoMatricula tipoMatriculaUji = TipoMatricula.getTipoMatriculaById(tipoMatriculaId);
        this.setTipoMatricula(tipoMatriculaUji);

        if (generales.getGeneIndMatParcialSol() != null)
        {
            this.setMatriculaParcial(generales.getGeneIndMatParcialSol().equals("S"));
        }

        if (generales.getGeneIndEstPresenSol() != null)
        {
            this.setBecaParcial(generales.getGeneIndEstPresenSol().equals("S"));
        }

        if (generales.getGeneIndTituEspana() != null)
        {
            this.setTituloEspanol(generales.getGeneIndTituEspana().equals("S"));
        }

        if (generales.getGeneIndEstudiosHomo() != null)
        {
            this.setEstudiosHomologados(generales.getGeneIndEstudiosHomo().equals("S"));
        }

        this.setUnivTitulacion(generales.getGeneCodUnivTitu());

        this.setCodTituAcceso(generales.getGeneCodTitulacion());
    }

    private void importarDeUltimoCursoType(UltimoCursoType ultimoCurso)
    {
        if (ultimoCurso != null)
        {
            if (ultimoCurso.getUlcuIdUltCurso() != null)
            {
                this.setCursoAcademicoAnt(Long.parseLong(ultimoCurso.getUlcuIdUltCurso()));
            }
            this.setEstudioAnt(ultimoCurso.getUlcuEstudios());
            this.setCodigoEstudioAnt(ultimoCurso.getUlcuCodEstudio());
            this.setCodigoCentroAnt(ultimoCurso.getUlcuCcentro());
            this.setNombreCentroAnt(ultimoCurso.getUlcuNombreCentro());
            this.setCausaOtros(ultimoCurso.getUlcuCausa());
        }
    }

    private void importarDeTitulacionType(TitulacionType titulacion)
    {
        if (titulacion != null)
        {
            if (titulacion.getTituIndTitulo() != null)
            {
                this.setTieneTituloUniversitario(titulacion.getTituIndTitulo().equals("S"));
            }
            this.setTituloNombre(titulacion.getTituTitulo());
            this.setTipoTitulacion(titulacion.getTituCodTipoTit());
        }
    }

    private void importarDeSolicitudType(SolicitudType solicitudType)
            throws EstudioSolicitadoNoMatriculadoException, EstudioMinisterioMayores25Exception
    {
        importarNotificaciones(solicitudType);
        importarDatosBancarios(solicitudType);
        importarObservaciones(solicitudType);
        importarEstudio(solicitudType);
        importarOtrosDatos(solicitudType);
    }

    private void importarNotificaciones(SolicitudType solicitudType)
    {
        if (solicitudType.getSoliIndCertfElectr() != null)
        {
            this.setSms(solicitudType.getSoliIndCertfElectr().equals("S"));
        } else {
            this.setSms(false);
        }
    }

    private void importarDatosBancarios(SolicitudType solicitudType)
    {
        this.setEntidad(solicitudType.getSoliCbanco());
        this.setSucursal(solicitudType.getSoliCsucu());
        this.setDigitosControl(solicitudType.getSoliDC());
        this.setCuentaBancaria(solicitudType.getSoliNumCuenta());
        this.setIbanPais(solicitudType.getSoliIbanCodPais());
        this.setIbanDC(solicitudType.getSoliIbanDc());
    }

    private void importarObservaciones(SolicitudType solicitud)
    {
        this.setObservaciones(solicitud.getSoliObservaciones());
    }

    private void importarEstudio(SolicitudType solicitudType)
            throws EstudioSolicitadoNoMatriculadoException
    {
        this.setEstudioAntOtros(solicitudType.getSoliCodEstudioEspecific());
        Estudio estudioUji = buscaEstudioMatricula();
        this.setEstudio(estudioUji);
        Boolean esCambioEstudios = buscaCambioEstudios();
        this.setCambioEstudios(esCambioEstudios);
    }

    private void importarOtrosDatos(SolicitudType solicitudType)
    {
        this.setCodigoExpediente(solicitudType.getSoliExpediente());
    }

    private Boolean buscaCambioEstudios() throws EstudioSolicitadoNoMatriculadoException
    {
        Long personaId = this.getSolicitante().getPersona().getId();
        Long cursoAcademicoId = this.getSolicitante().getCursoAcademico().getId();
        List<PersonaEstudio> titulaciones = PersonaEstudio.getAcademicosByPersonaYCursoAca(personaId, cursoAcademicoId);

        if (titulaciones.size() > 1)
        {
            throw new EstudioSolicitadoNoMatriculadoException("S'ha trovat mès d'un estudi per a aquest curs acadèmic");
        }

        if (titulaciones == null || titulaciones.size() == 0)
        {
            throw new EstudioSolicitadoNoMatriculadoException();
        }

        return titulaciones.get(0).isCambioEstudios();
    }

    private Estudio buscaEstudioMatricula() throws EstudioSolicitadoNoMatriculadoException
    {
        Long personaId = this.getSolicitante().getPersona().getId();
        Long cursoAcademicoId = this.getSolicitante().getCursoAcademico().getId();
        List<PersonaEstudio> titulaciones = PersonaEstudio.getAcademicosByPersonaYCursoAca(personaId, cursoAcademicoId);

        if (titulaciones.size() > 1)
        {
            throw new EstudioSolicitadoNoMatriculadoException("S'ha trovat mès d'un estudi per a aquest curs acadèmic");
        }

        if (titulaciones == null || titulaciones.size() == 0)
        {
            throw new EstudioSolicitadoNoMatriculadoException();
        }

        Long estudioUjiId = titulaciones.get(0).getEstudio().getId();

        Estudio estudioUJI = Estudio.getEstudioById(estudioUjiId);

        return estudioUJI;
    }

    public String getObservaciones()
    {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public Estudio getEstudio()
    {
        return this.estudio;
    }

    public void setEstudio(Estudio estudio)
    {
        this.estudio = estudio;
    }

    public Beca update()
    {
        return becaDAO.updateBeca(this);
    }

    public boolean isDuplicada()
    {
        Long estudioId = this.getEstudio() != null ? this.getEstudio().getId() : null;

        return becaDAO.isDuplicada(this.getSolicitante().getId(), this.getConvocatoria().getId(), estudioId);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Convocatoria getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    private void importarRelacionEstados(SolicitudType solicitudType) throws BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException {
        List<EstadoSolicitudType> listaEstadosSolicitud = solicitudType.getEstadoSolicitud();

        if (listaEstadosSolicitud.size() > 0) {
            EstadoSolicitudType estadoSolicitudTypeFinal = listaEstadosSolicitud
                    .stream()
                    .max(Comparator.comparing(v -> v.getEssoSecuencEstado()))
                    .get();

            String estadoSolicitud = estadoSolicitudTypeFinal.getEssoCodEstado();

            if (estadoSolicitud.equals("10")) // Denegada en el ministerio
            {
                Beca beca = Beca.getBecaById(this.getId());
                Beca becaDataBase = SerializationUtils.clone(beca);

                beca.borraDenegacionesOrdenUno();

                Estado estadoDenegada = new Estado();
                estadoDenegada.setId(CodigoEstado.DENEGADA.getId());
                this.setEstado(estadoDenegada);

                List<EstadoCausaType> listaEstadoCausa = estadoSolicitudTypeFinal.getEstadoCausa();
                for (EstadoCausaType estadoCausaType : listaEstadoCausa) {
                    String causa = estadoCausaType.getEscaCodCausa();
                    String subcausa = estadoCausaType.getEscaCodSubcausa();

                    Denegacion denegacion = Denegacion.getDenegacionByOrganismoEstadoCausaySubcausa(1L, estadoSolicitud, causa, subcausa);

                    if (denegacion == null) {
                        throw new BecaDenegacionNoEncontradaException("Denegació 1-" + estadoSolicitud + ", causa " + causa + " i subcausa " + subcausa + " no trobada");
                    }

                    BecaDenegacion becaDenegacion = new BecaDenegacion();
                    becaDenegacion.setBeca(beca);
                    becaDenegacion.setDenegacion(denegacion);
                    becaDenegacion.setOrdenDenegacion(1L);
                    becaDenegacion.insert();
                }
                HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, "revillo");
            }
        }
    }


    private void importarRelacionMiembros(SolicitudType solicitudType)
    {
        List<FamiliarType> listaFamiliares = solicitudType.getFamiliar();
        borraMiembrosExistentes();

        Set<Miembro> listaMiembrosConvertidos = convierteListaFamiliaresAListaMiembros(listaFamiliares);
        completarDatosPersonalesSolicitante(listaMiembrosConvertidos, solicitudType.getDatosPersonales());
        this.setMiembros(listaMiembrosConvertidos);
    }

    private void borraMiembrosExistentes()
    {
        for (Miembro miembro : getMiembros())
        {
            miembro.borraActividadesEconomicas();
            miembro.delete();
        }

        this.setMiembros(null);
    }

    public Set<Miembro> getMiembros()
    {
        if (this.miembros == null || this.miembros.isEmpty())
        {
            return new HashSet<Miembro>();
        }

        return this.miembros;
    }

    public void setMiembros(Set<Miembro> miembros)
    {
        this.miembros = miembros;
    }

    private Set<Miembro> convierteListaFamiliaresAListaMiembros(List<FamiliarType> listaFamiliares)
    {
        Set<Miembro> listaMiembrosConvertidos = new HashSet<Miembro>();
        for (FamiliarType familiarType : listaFamiliares)
        {
            Miembro miembro = buscaMiembroExistente(familiarType, getMiembros());
            miembro.importarDatosMinisterio(familiarType);
            listaMiembrosConvertidos.add(miembro);
            miembro.insert();
            miembro.importarActividadesEconomicas(familiarType);
        }
        return listaMiembrosConvertidos;
    }

    private Miembro buscaMiembroExistente(FamiliarType familiar, Set<Miembro> listaMiembros)
    {
        for (Miembro miembro : listaMiembros)
        {
            if (familiar.getFamiNiffam().equals(miembro.getIdentificacion()))
            {
                return miembro;
            }
        }
        Miembro miembro = new Miembro();
        miembro.setBeca(this);
        return miembro;
    }

    private void completarDatosPersonalesSolicitante(Set<Miembro> listaMiembrosConvertidos, DatosPersonalesType datosPersonales)
    {
        String identificacionSolicitante = datosPersonales.getDapeNif();
        for (Miembro miembro : listaMiembrosConvertidos)
        {
            if (identificacionSolicitante != null
                    && identificacionSolicitante.equals(miembro.getIdentificacion()))
            {
                String tipoSexoMinisterio = datosPersonales.getDapeIdSexo();
                if (tipoSexoMinisterio != null)
                {
                    MapeadorTipoSexo mapeadorTipoSexo = new MapeadorTipoSexo();
                    Long tipoSexoIdUji = mapeadorTipoSexo.getValorUJI(tipoSexoMinisterio, CodigoOrganismo.MINISTERIO);
                    if (tipoSexoIdUji != null)
                    {
                        TipoSexo tipoSexoUji = new TipoSexo();
                        tipoSexoUji.setId(tipoSexoIdUji);
                        miembro.setTipoSexo(tipoSexoUji);
                    }
                }
            }
        }
    }

    private void importarRelacionDomicilios(SolicitudType solicitudType)
            throws RegistroNoEncontradoException
    {
        DatosPersonalesType datosPersonales = solicitudType.getDatosPersonales();
        if (datosPersonales != null)
        {
            List<DomicilioType> listaDomicilios = datosPersonales.getDomicilio();
            borraDomiciliosExistentes();
            Set<Domicilio> listaDomiciliosconvertidos = convierteListaDomicilios(listaDomicilios);
            this.setDomicilios(listaDomiciliosconvertidos);
        }
    }

    private void borraDomiciliosExistentes() throws RegistroNoEncontradoException
    {
        if (getDomicilios() == null || getDomicilios().size() == 0)
        {
            return;
        }

        for (Domicilio domicilio : getDomicilios())
        {
            domicilio.borraArrendatarios();
            domicilio.delete();
        }
        this.setDomicilios(null);
    }

    public Set<Domicilio> getDomicilios()
    {
        return this.domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios)
    {
        this.domicilios = domicilios;
    }

    private Set<Domicilio> convierteListaDomicilios(List<DomicilioType> listaDomicilios)
    {
        Set<Domicilio> listaDomiciliosConvertidos = new HashSet<Domicilio>();
        for (DomicilioType domicilioType : listaDomicilios)
        {
            if (domicilioType.getDomiDomicilio() != null)
            {
                Domicilio domicilio = new Domicilio();
                domicilio.setBeca(this);
                domicilio.importarDatosMinisterio(domicilioType);

                listaDomiciliosConvertidos.add(domicilio);
                domicilio.insert();
                domicilio.importarRelacionArrendatarios(domicilioType);
            }
        }
        return listaDomiciliosConvertidos;
    }

    public Domicilio getdomicilioResidencia() throws SolicitanteSinDomicilioFamiliarException
    {
        return Domicilio.getDomicilioResidencia(this.getId());
    }

    private Boolean isConvocatoriaGeneral()
    {
        if (this.getConvocatoria().getId() == CodigoConvocatoria.GENERAL.getId())
        {
            return true;
        }
        return false;
    }

    private Boolean isConvocatoriaConselleria()
    {
        return (this.getConvocatoria().getId() == CodigoConvocatoria.CONSELLERIA.getId());
    }

    private Boolean isConvocatoriaGva_Salario()
    {
        return (this.getConvocatoria().getId() == CodigoConvocatoria.GVA_SALARIO.getId());
    }

    private Boolean isConvocatoriaGva_Finalizacion()
    {
        return (this.getConvocatoria().getId() == CodigoConvocatoria.GVA_FINALIZACION.getId());
    }


    public void insertaAyudas() throws BecaCuantiaDuplicadaException, BecaDuplicadaException,
            AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        if (!isEstadoPendienteOTrabajada())
        {
            throw new AyudaNoAplicableException();
        }

        if (!existenAcademicos())
        {
            throw new AcademicosNoCargadosException();
        }

        if (isConvocatoriaGeneral())
        {
            insertaAyudasGeneral();
        }

        if (isConvocatoriaConselleria() || this.isDeOrganismo(CodigoOrganismo.GVA_ALTRES))
        {
            insertaTasas();
        }

    }

    private Boolean existenAcademicos()
    {
        return (this.isBecaParcial() != null && this.getNumeroSemestres() != null);
    }

    private Boolean isEstadoPendienteOTrabajada()
    {
        Long estadoId = estado.getId();
        return (CodigoEstado.PENDIENTE.igual(estadoId) || CodigoEstado.TRABAJADA.igual(estadoId));
    }

    public void borraAyudas()
    {
        List<CuantiaBeca> cuantiasBeca = CuantiaBeca.getCuantiasBecaByBecaId(this.getId());

        for (CuantiaBeca cuantiaBeca : cuantiasBeca)
        {
            CuantiaBeca.delete(cuantiaBeca.getId());
        }
    }

    public void borraDenegacionesOrdenUno()
    {
        List<BecaDenegacion> denegacionesBeca = BecaDenegacion.getDenegacionesByBecaId(this.getId());

        for (BecaDenegacion denegacion : denegacionesBeca)
        {
            if (denegacion.getOrdenDenegacion() == ORDEN_UNO_DENEGACION)
            {
                BecaDenegacion.delete(denegacion.getId());
            }
        }
    }

    public void borraDenegacionesOrdenDos()
    {
        List<BecaDenegacion> denegacionesBeca = BecaDenegacion.getDenegacionesByBecaId(this.getId());

        for (BecaDenegacion denegacion : denegacionesBeca)
        {
            if (denegacion.getOrdenDenegacion() == ORDEN_DOS_DENEGACION)
            {
                BecaDenegacion.delete(denegacion.getId());
            }
        }
    }

    private void insertaAyudasGeneral() throws BecaCuantiaDuplicadaException,
            DistanciaLocalidadNoEncontradaException, SolicitanteSinDomicilioFamiliarException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        insertaTasas();
        insertaResidencia();
        insertaResidenciaInsularCeutaMelilla();
    }

    private void insertaResidencia() throws BecaCuantiaDuplicadaException,
            SolicitanteSinDomicilioFamiliarException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        if (!isCumpleCondicionesResidencia())
        {
            return;
        }

        Domicilio domicilio = this.getDomicilioFamiliar();
        if (domicilio == null || domicilio.isDistanciaDomicilioMenor(DISTANCIA_MAXIMA))
        {
            return;
        }

        this.borraAyudas();
        insertaAyuda(CodigoCuantia.TASAS_GENERAL_RESIDENCIA);
        insertaAyuda(CodigoCuantia.RESIDENCIA);
    }

    private void insertaResidenciaInsularCeutaMelilla() throws BecaCuantiaDuplicadaException,
            SolicitanteSinDomicilioFamiliarException
    {
        if (!isCumpleCondicionesResidencia())
        {
            return;
        }

        Domicilio domicilio = this.getDomicilioFamiliar();
        if (domicilio == null || domicilio.isPeninsular())
        {
            return;
        }

        this.borraAyudas();
        insertaAyuda(CodigoCuantia.TASAS_GENERAL_RESIDENCIA);
        insertaAyuda(CodigoCuantia.RESIDENCIA);

        if (domicilio.isInsular())
        {
            insertaAyuda(CodigoCuantia.INSULAR);
        }

        if (domicilio.isEnCeutaMelilla())
        {
            insertaAyuda(CodigoCuantia.CEUTA_MELILLA);
        }
    }

    private boolean isCumpleCondicionesResidencia() throws SolicitanteSinDomicilioFamiliarException
    {
        Estudio estudio = this.getEstudio();
        if (estudio.isSinDocencia())
        {
            return false;
        }

        Declarante declarante = this.getSolicitante().getDeclarante();
        if (declarante == null || declarante.isIndicadorIndependiente())
        {
            return false;
        }

        if (this.isBecaParcial())
        {
            return false;
        }

        List<Miembro> listaMiembros = Miembro.getMiembrosByBeca(this.getId());
        for (Miembro miembro : listaMiembros)
        {
            if (miembro.esExtranjero() && (miembro.esSolicitante() || miembro.esSustentador()))
            {
                if (this.getSolicitante().isResidenciaPermanente() == false
                        && this.getSolicitante().isResidenciaTrabajo() == false)
                {
                    return false;
                }
            }
        }

        Domicilio domicilioResidencia = this.getdomicilioResidencia();
        if (domicilioResidencia == null || domicilioResidencia.isGratuito())
        {
            return false;
        }

        if (domicilioResidencia == null || domicilioResidencia.getTipoResidencia().getId() == TIPO_RESIDENCIA_FAMILIAR
                || domicilioResidencia.getTipoResidencia().getId() == TIPO_RESIDENCIA_OTROS_FAMILIARES)
        {
            return false;
        }
        return true;
    }

    private void insertaTasas() throws BecaCuantiaDuplicadaException
    {
        if (isConvocatoriaGeneral())
        {
            insertaAyuda(CodigoCuantia.TASAS_GENERAL);
        }
        if (isConvocatoriaConselleria())
        {
            insertaAyuda(CodigoCuantia.TASAS_CONSELLERIA);
        }
        if (isConvocatoriaGva_Salario())
        {
            insertaAyuda(CodigoCuantia.GVA_SALARIO);
        }
        if (isConvocatoriaGva_Finalizacion())
        {
            insertaAyuda(CodigoCuantia.GVA_FINALIZACION);
        }
    }

    private void insertaAyuda(CodigoCuantia codigoCuantia) throws BecaCuantiaDuplicadaException
    {
        Cuantia cuantia = Cuantia.getCuantiaById(codigoCuantia.getId());
        Long importe = importeCuantia(codigoCuantia.getId());

        CuantiaBeca cuantiaBeca = creaCuantiaBeca(cuantia, importe);
        cuantiaBeca.insert();
    }

    public void insertaAyudaConImporte(CodigoCuantia codigoCuantia, Float importe) throws BecaCuantiaDuplicadaException
    {
        Cuantia cuantia = Cuantia.getCuantiaById(codigoCuantia.getId());

        CuantiaBeca cuantiaBeca = creaCuantiaBeca(cuantia, importe.longValue());
        cuantiaBeca.insert();
    }

    private CuantiaBeca creaCuantiaBeca(Cuantia cuantia, Long importe)
            throws BecaCuantiaDuplicadaException
    {
        CuantiaBeca cuantiaBeca = new CuantiaBeca();
        cuantiaBeca.setBeca(this);
        cuantiaBeca.setCuantia(cuantia);
        cuantiaBeca.setImporte(new Float(importe));

        return cuantiaBeca;
    }

    private Long importeCuantia(Long cuantiaId)
    {
        Long cursoAcademicoId = this.getSolicitante().getCursoAcademico().getId();
        CuantiaCurso cuantiaCurso = CuantiaCurso.getCuantiaCursoByCursoAcademicoAndCuantia(
                cursoAcademicoId, cuantiaId);

        return cuantiaCurso.getImporte();
    }

    public void updateOtrosDatos()
    {
        becaDAO.updateOtrosDatos(this);
    }

    public void updateDatosGenerales() throws ConvocatoriaDeTandaYDeBecaDiferentesException
    {
        compruebaConvocatoriaBecaTanda();
        becaDAO.updateDatosGenerales(this);
    }

    private void compruebaConvocatoriaBecaTanda() throws ConvocatoriaDeTandaYDeBecaDiferentesException
    {
        Tanda tanda = this.getTanda();

        if (tanda != null && !tanda.getConvocatoria().getId().equals(this.getConvocatoria().getId()))
        {
            throw new ConvocatoriaDeTandaYDeBecaDiferentesException();
        }
    }

    public Tanda getTanda()
    {
        return this.tanda;
    }

    public void setTanda(Tanda tanda)
    {
        this.tanda = tanda;
    }

    public void updateAcademicos()
    {
        becaDAO.updateAcademicos(this);
    }

    public void updateEstudios()
    {
        becaDAO.updateEstudios(this);
    }

    public Boolean noEsModificable()
    {
        return (!esModificable());
    }

    public Boolean esModificable()
    {
        Long estadoId = this.getEstado().getId();
        return (CodigoEstado.PENDIENTE.igual(estadoId)
                || CodigoEstado.TRABAJADA.igual(estadoId)
                || CodigoEstado.ACADEMICOS_CALCULADOS.igual(estadoId)
                || CodigoEstado.ACADEMICOS_ENVIADOS.igual(estadoId)
                || CodigoEstado.INCORRECTA.igual(estadoId));
    }

    public Estado getEstado()
    {
        return this.estado;
    }

    public void setEstado(Estado estado)
    {
        this.estado = estado;
    }

    public Boolean isEnEstadoPendienteTrabajadaODenegadaAcademico()
    {
        Long estadoId = this.getEstado().getId();
        return (CodigoEstado.PENDIENTE.igual(estadoId)
                || CodigoEstado.TRABAJADA.igual(estadoId)
                || CodigoEstado.DENEGADA_ACADEMICOS.igual(estadoId));
    }

    public Boolean noEstaEnProceso(CodigoProceso proceso)
    {
        return !isEnProceso(proceso);
    }

    public Boolean isEnProceso(CodigoProceso proceso)
    {
        return proceso.igual(getProceso().getId());
    }

    public Proceso getProceso()
    {
        return this.proceso;
    }

    public void setProceso(Proceso proceso)
    {
        this.proceso = proceso;
    }

    public Boolean isParaPrimerCurso()
    {
        return !isParaSegundoCursoOPosterior();
    }

    public Boolean isParaSegundoCursoOPosterior()
    {
        return this.getCurso().compareTo(1L) > 0 || (this.getCurso().compareTo(1L) == 0 &&
                this.getCreditosMatriculadosAnt() != null &&
                this.getCreditosMatriculadosAnt() != 0);
    }

    public Long getCurso()
    {
        return this.curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Float getCreditosMatriculadosAnt()
    {
        return this.creditosMatriculadosAnt;
    }

    public void setCreditosMatriculadosAnt(Float creditosMatriculadosAnt)
    {
        this.creditosMatriculadosAnt = creditosMatriculadosAnt;
    }

    public Long calculaOrdenDenegacion()
    {
        if (this.isEnProceso(CodigoProceso.ORDINARIO))
        {
            return 1L;
        }

        return 2L;
    }

    public Minimo calculaCreditosMinimos()
    {
        Long estudioId = this.getEstudio().getId();
        Long cursoAcademicoId = this.getSolicitante().getCursoAcademico().getId();
        return Minimo.getMinimosByEstudioId(estudioId, cursoAcademicoId);
    }

    public Boolean isConcedidaEnElMinisterio() throws BecaDeDistintoOrganismoYaExisteException
    {
        if (this.isDeOrganismo(CodigoOrganismo.MINISTERIO))
        {
            if (this.isEnEstado(CodigoEstado.CONCEDIDA) && this.isEnProcesoOrdinarioAlegacionORecurso())
            {
                return true;
            }

            if (this.isEnProceso(CodigoProceso.AUMENTO_CUANTIA))
            {
                return true;
            }
        }
        return false;
    }

    public Boolean isDeOrganismo(CodigoOrganismo organismo)
    {
        return organismo.igual(this.getConvocatoria().getOrganismo().getId());
    }

    public Boolean isEnEstado(CodigoEstado estado)
    {
        return estado.igual(this.getEstado().getId());
    }

    public Boolean isEnProcesoOrdinarioAlegacionORecurso()
    {
        Long procesoId = this.getProceso().getId();
        return (CodigoProceso.ORDINARIO.igual(procesoId)
                || CodigoProceso.ALEGACION.igual(procesoId)
                || CodigoProceso.RECURSO.igual(procesoId));
    }

    public Boolean isSolicitadaEnElMinisterio() throws BecaDeDistintoOrganismoYaExisteException
    {
        if (this.isDeOrganismo(CodigoOrganismo.MINISTERIO))
        {
            return true;

        }
        return false;
    }

    public Boolean isEnEstadoPendienteTrabajadaOEnviada()
    {
        Long estadoId = this.getEstado().getId();
        return (CodigoEstado.PENDIENTE.igual(estadoId)
                || CodigoEstado.TRABAJADA.igual(estadoId)
                || CodigoEstado.ENVIADA.igual(estadoId));
    }

    public Boolean isSuperadoNumeroMaximoDeBecasConcedidas()
    {
        Long numeroBecasConcedidasConAnterioridad = this.getBecasConcedidasAnt();

        Long numeroMaximoBecasDelEstudio = this.getEstudio().getNumeroMaximoBecas().longValue();

        if (this.getBecasConcedidasAnt() == null)
        {
            return (false);
        }

        return (numeroBecasConcedidasConAnterioridad >= numeroMaximoBecasDelEstudio);
    }

    public Long getBecasConcedidasAnt()
    {
        return becasConcedidasAnt;
    }

    public void setBecasConcedidasAnt(Long becasConcedidasAnt)
    {
        this.becasConcedidasAnt = becasConcedidasAnt;
    }

    public BecaDenegacion insertaDenegacion(Long denegacionId, Long orden) throws BecaDenegacionDuplicadaException
    {
        BecaDenegacion becaDenegacion = new BecaDenegacion();
        becaDenegacion.setBeca(this);
        becaDenegacion.setDenegacion(Denegacion.getDenegacionById(denegacionId));
        becaDenegacion.setOrdenDenegacion(orden);
        becaDenegacion.insert();

        return becaDenegacion;
    }

    public void updateObservacionesUji()
    {
        becaDAO.updateObservacionesUji(this);
    }

    public void cambiaTanda(Tanda tanda)
    {
        this.setTanda(tanda);
        this.updateTanda();
    }

    public void updateTanda()
    {
        becaDAO.updateTanda(this);
    }

    public void cambiaEstado(Long estadoId)
    {
        this.setEstado(Estado.getEstadoById(estadoId));
        this.updateEstado();
    }

    public void updateEstado()
    {
        becaDAO.updateEstado(this);
    }

    public void cambiaAConcedida()
    {
        this.setBecaConcedida(true);
        this.updateBecaConcedida();
    }

    public void updateBecaConcedida()
    {
        becaDAO.updateBecaConcedida(this);
    }

    public void cambiaANoConcedida()
    {
        this.setBecaConcedida(false);
        this.updateBecaConcedida();
    }

    public void compruebaEstadoDenegadaUJI() throws BecaException
    {
        Long estadoId = estado.getId();
        Long denegadaAcademicosId = CodigoEstado.DENEGADA_ACADEMICOS.getId();
        if (!estadoId.equals(denegadaAcademicosId))
        {
            throw new BecaException(MessageFormat.format("La beca {0} ({1}) deu estar en estat denegat UJI",
                    getSolicitante().getNumeroBecaUji(), getId()));
        }
    }

    public void insert()
    {
        becaDAO.insert(this);
    }

    public boolean isDomicilioComunidadValenciana() throws SolicitanteSinDomicilioFamiliarException
    {
        Domicilio domicilioFamiliar = this.getDomicilioFamiliar();

        if (domicilioFamiliar == null)
        {
            throw new SolicitanteSinDomicilioFamiliarException();
        }

        Provincia provincia = domicilioFamiliar.getProvincia();

        if (provincia != null)
        {
            if (COMUNIDAD_VALENCIANA.contains(provincia.getId()))
            {
                return true;
            }
        }
        return false;
    }

    public Domicilio getDomicilioFamiliar()
    {
        return Domicilio.getDomicilioFamiliar(this.getId());
    }

    public List<Documento> getDocumentosPendientesNotificacion()
    {
        return becaDAO.getDocumentosPendientesNotificacion(this.getId());
    }

    public Float getCreditosPrimeraMatricula()
    {
        return creditosPrimeraMatricula;
    }

    public void setCreditosPrimeraMatricula(Float creditosPrimeraMatricula)
    {
        this.creditosPrimeraMatricula = creditosPrimeraMatricula;
    }

    public Float getCreditosSegundaMatricula()
    {
        return creditosSegundaMatricula;
    }

    public void setCreditosSegundaMatricula(Float creditosSegundaMatricula)
    {
        this.creditosSegundaMatricula = creditosSegundaMatricula;
    }

    public Float getCreditosTerceraMatricula()
    {
        return creditosTerceraMatricula;
    }

    public void setCreditosTerceraMatricula(Float creditosTerceraMatricula)
    {
        this.creditosTerceraMatricula = creditosTerceraMatricula;
    }

    public Float getCreditosTerceraMatriculaPosteriores()
    {
        return creditosTerceraMatriculaPosteriores;
    }

    public void setCreditosTerceraMatriculaPosteriores(Float creditosTerceraMatriculaPosteriores)
    {
        this.creditosTerceraMatriculaPosteriores = creditosTerceraMatriculaPosteriores;
    }

    public Float getCreditosCuartaMatriculaPosteriores()
    {
        return creditosCuartaMatriculaPosteriores;
    }

    public void setCreditosCuartaMatriculaPosteriores(Float creditosCuartaMatriculaPosteriores)
    {
        this.creditosCuartaMatriculaPosteriores = creditosCuartaMatriculaPosteriores;
    }

    public Float getCreditosCovid19() {
        return creditosCovid19;
    }

    public void setCreditosCovid19(Float creditosCovid19) {
        this.creditosCovid19 = creditosCovid19;
    }

    public Boolean isOtraBecaConcedida()
    {
        return otraBecaConcedida;
    }

    public Boolean isCambioEstudios()
    {
        return cambioEstudios;
    }

    public Boolean isCambioDeEstudio() throws EstudioNoEncontradoException, EstudioAnteriorFaltanDatosException {
        Long estudioActualId = this.estudio.getId();
        Long cursoAcademicoAnteriorId = getCursoAcademicoAnt();

        if (this.getCodigoEstudioAnt() == null)
        {
            throw new EstudioAnteriorFaltanDatosException();
        }

        Solicitante solicitante = this.getSolicitante();
        Long personaId = solicitante.getPersona().getId();
        Long cursoAcademicoActualId = solicitante.getCursoAcademico().getId();

        PersonaEstudio personaEstudio = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(personaId, estudioActualId, cursoAcademicoActualId);

        if (personaEstudio.isPrimeraVez())
        {
            return false;
        }

        Long estudioAnteriorId = Diccionario.getValor(cursoAcademicoAnteriorId, "Estudio", CodigoOrganismo.CONSELLERIA.getId(), this.getCodigoEstudioAnt());

        if (estudioAnteriorId == null)
        {
            estudioAnteriorId = DiccionarioExcepciones.getValor(cursoAcademicoAnteriorId, "Estudio", CodigoOrganismo.CONSELLERIA.getId(), this.getCodigoEstudioAnt());
        }

        if (estudioAnteriorId == null)
        {
            throw new EstudioNoEncontradoException();
        }

        return !estudioAnteriorId.equals(estudioActualId);
    }

    public boolean isTieneCreditos2a()
    {

        Long personaId = this.getSolicitante().getPersona().getId();
        Long cursoAcaId = this.getSolicitante().getCursoAcademico().getId();
        Long estudioId = this.getEstudio().getId();

        PersonaEstudio personaEstudio = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(personaId,
                estudioId, cursoAcaId);

        return (personaEstudio.getCreditosSegundaMatricula() != null && personaEstudio.getCreditosSegundaMatricula() > 0);

    }

    public boolean isSolicitudMatriculada()
    {
        Estudio estudioBeca = this.getEstudio();

        if (estudioBeca != null)
        {
            PersonaEstudio estudioPersona = PersonaEstudio
                    .getAcademicosCursoActualbyPersonaEstudioYCursoAca(this.getSolicitante()
                            .getPersona().getId(), estudioBeca.getId(), this.getSolicitante()
                            .getCursoAcademico().getId());

            if (estudioPersona.getId() != null)
            {
                return true;
            }
        }
        return false;
    }

}