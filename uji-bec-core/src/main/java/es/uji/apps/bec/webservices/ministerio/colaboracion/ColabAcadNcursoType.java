//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.11.30 at 11:46:12 AM CET 
//


package es.uji.apps.bec.webservices.ministerio.colaboracion;

import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ColabAcadNcursoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ColabAcadNcursoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CoanNumAsig"/>
 *         &lt;element ref="{}CoanCurso"/>
 *         &lt;element ref="{}CoanAsignatura"/>
 *         &lt;element ref="{}CoanNumCreditos"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ColabAcadNcursoType", propOrder = {
    "coanNumAsig",
    "coanCurso",
    "coanAsignatura",
    "coanNumCreditos"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.colaboracion.ColabAcadNcursoType")
@Table(name = "COLABACADNCURSOTYPE", schema = "UJI_BECAS_WS_COLABORACION")
@Inheritance(strategy = InheritanceType.JOINED)
public class ColabAcadNcursoType
    implements Equals, HashCode
{

    @XmlElement(name = "CoanNumAsig")
    protected long coanNumAsig;
    @XmlElement(name = "CoanCurso")
    protected long coanCurso;
    @XmlElement(name = "CoanAsignatura", required = true)
    protected String coanAsignatura;
    @XmlElement(name = "CoanNumCreditos", required = true)
    protected BigDecimal coanNumCreditos;
    @XmlTransient
    protected Long hjid;

    /**
     * Gets the value of the coanNumAsig property.
     * 
     */
    @Basic
    @Column(name = "COANNUMASIG", scale = 0)
    public long getCoanNumAsig() {
        return coanNumAsig;
    }

    /**
     * Sets the value of the coanNumAsig property.
     * 
     */
    public void setCoanNumAsig(long value) {
        this.coanNumAsig = value;
    }

    /**
     * Gets the value of the coanCurso property.
     * 
     */
    @Basic
    @Column(name = "COANCURSO", scale = 0)
    public long getCoanCurso() {
        return coanCurso;
    }

    /**
     * Sets the value of the coanCurso property.
     * 
     */
    public void setCoanCurso(long value) {
        this.coanCurso = value;
    }

    /**
     * Gets the value of the coanAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "COANASIGNATURA", length = 60)
    public String getCoanAsignatura() {
        return coanAsignatura;
    }

    /**
     * Sets the value of the coanAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoanAsignatura(String value) {
        this.coanAsignatura = value;
    }

    /**
     * Gets the value of the coanNumCreditos property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    @Basic
    @Column(name = "COANNUMCREDITOS", precision = 4, scale = 2)
    public BigDecimal getCoanNumCreditos() {
        return coanNumCreditos;
    }

    /**
     * Sets the value of the coanNumCreditos property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoanNumCreditos(BigDecimal value) {
        this.coanNumCreditos = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ColabAcadNcursoType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ColabAcadNcursoType that = ((ColabAcadNcursoType) object);
        {
            long lhsCoanNumAsig;
            lhsCoanNumAsig = (true?this.getCoanNumAsig(): 0L);
            long rhsCoanNumAsig;
            rhsCoanNumAsig = (true?that.getCoanNumAsig(): 0L);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "coanNumAsig", lhsCoanNumAsig), LocatorUtils.property(thatLocator, "coanNumAsig", rhsCoanNumAsig), lhsCoanNumAsig, rhsCoanNumAsig)) {
                return false;
            }
        }
        {
            long lhsCoanCurso;
            lhsCoanCurso = (true?this.getCoanCurso(): 0L);
            long rhsCoanCurso;
            rhsCoanCurso = (true?that.getCoanCurso(): 0L);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "coanCurso", lhsCoanCurso), LocatorUtils.property(thatLocator, "coanCurso", rhsCoanCurso), lhsCoanCurso, rhsCoanCurso)) {
                return false;
            }
        }
        {
            String lhsCoanAsignatura;
            lhsCoanAsignatura = this.getCoanAsignatura();
            String rhsCoanAsignatura;
            rhsCoanAsignatura = that.getCoanAsignatura();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "coanAsignatura", lhsCoanAsignatura), LocatorUtils.property(thatLocator, "coanAsignatura", rhsCoanAsignatura), lhsCoanAsignatura, rhsCoanAsignatura)) {
                return false;
            }
        }
        {
            BigDecimal lhsCoanNumCreditos;
            lhsCoanNumCreditos = this.getCoanNumCreditos();
            BigDecimal rhsCoanNumCreditos;
            rhsCoanNumCreditos = that.getCoanNumCreditos();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "coanNumCreditos", lhsCoanNumCreditos), LocatorUtils.property(thatLocator, "coanNumCreditos", rhsCoanNumCreditos), lhsCoanNumCreditos, rhsCoanNumCreditos)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            long theCoanNumAsig;
            theCoanNumAsig = (true?this.getCoanNumAsig(): 0L);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "coanNumAsig", theCoanNumAsig), currentHashCode, theCoanNumAsig);
        }
        {
            long theCoanCurso;
            theCoanCurso = (true?this.getCoanCurso(): 0L);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "coanCurso", theCoanCurso), currentHashCode, theCoanCurso);
        }
        {
            String theCoanAsignatura;
            theCoanAsignatura = this.getCoanAsignatura();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "coanAsignatura", theCoanAsignatura), currentHashCode, theCoanAsignatura);
        }
        {
            BigDecimal theCoanNumCreditos;
            theCoanNumCreditos = this.getCoanNumCreditos();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "coanNumCreditos", theCoanNumCreditos), currentHashCode, theCoanNumCreditos);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
