package es.uji.apps.bec.model.domains;

import es.uji.apps.bec.util.Mapeador;

import java.util.HashMap;
import java.util.Map;

public class MapeadorCodigoDenegacionRespuestaCons extends Mapeador<Long, Object>
{
    public MapeadorCodigoDenegacionRespuestaCons()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.CONSELLERIA, "002");
        add(new Long(191L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "004");
        add(new Long(87L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "005");
        add(new Long(88L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "006");
        add(new Long(89L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "009");
        add(new Long(185L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "010");
        add(new Long(66L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "012");
        add(new Long(67L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "017");
        add(new Long(77L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "018");
        add(new Long(78L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "019");
        add(new Long(79L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "020");
        add(new Long(400L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "027");
        add(new Long(81L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "028");
        add(new Long(82L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "030");
        add(new Long(280L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "032");
        add(new Long(280L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "033");
        add(new Long(303L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "141");
        add(new Long(282L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "142");
        add(new Long(283L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "144");
        add(new Long(285L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "145");
        add(new Long(286L), valores);

        valores.put(CodigoOrganismo.CONSELLERIA, "146");
        add(new Long(295L), valores);
    }
}