package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.SituacionLaboral;
import es.uji.commons.db.BaseDAO;

public interface SituacionLaboralDAO extends BaseDAO
{
    List<SituacionLaboral> getSituacionesLaborales();
    List<SituacionLaboral> getSituacionLaboralById(Long situacionLaboralId);

}