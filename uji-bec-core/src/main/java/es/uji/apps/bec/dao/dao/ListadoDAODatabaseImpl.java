package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Listado;
import es.uji.apps.bec.model.QListado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ListadoDAODatabaseImpl extends BaseDAODatabaseImpl implements ListadoDAO
{
    @Override
    public List<Listado> getListadosByOrganismoId(Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QListado qListado = QListado.listado;

        query.from(qListado).where(qListado.organismo.id.eq(organismoId).and(qListado.activo.isTrue())).orderBy(
                qListado.nombre.asc());

        return query.list(qListado);
    }
}
