package es.uji.apps.bec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_V_TANDAS_CONVOCATORIA database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "BC2_V_TANDAS_CONVOCATORIA")
public class TandaConvocatoria implements Serializable
{
    private String acronimo;

    @Column(name = "CONVOCATORIA_ACTIVA")
    private Boolean convocatoriaActiva;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoAcademicoId;

    @Id
    private String id;

    private String nombre;

    @Column(name = "NOMBRE_TANDA")
    private String nombreTanda;

    @Column(name = "ORGANISMO_ID")
    private Long organismoId;

    @Column(name = "TANDA_ACTIVA")
    private Boolean tandaActiva;

    @Column(name = "TANDA_ID")
    private Long tandaId;

    public TandaConvocatoria()
    {
        this.setConvocatoriaActiva(false);
        this.setTandaActiva(false);
    }

    public String getAcronimo()
    {
        return this.acronimo;
    }

    public void setAcronimo(String acronimo)
    {
        this.acronimo = acronimo;
    }

    public Boolean isConvocatoriaActiva()
    {
        return this.convocatoriaActiva;
    }

    public void setConvocatoriaActiva(Boolean convocatoriaActiva)
    {
        this.convocatoriaActiva = convocatoriaActiva;
    }

    public Long getConvocatoriaId()
    {
        return this.convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getCursoAcademicoId()
    {
        return this.cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNombreTanda()
    {
        return this.nombreTanda;
    }

    public void setNombreTanda(String nombreTanda)
    {
        this.nombreTanda = nombreTanda;
    }

    public Long getOrganismoId()
    {
        return this.organismoId;
    }

    public void setOrganismoId(Long organismoId)
    {
        this.organismoId = organismoId;
    }

    public Boolean isTandaActiva()
    {
        return this.tandaActiva;
    }

    public void setTandaActiva(Boolean tandaActiva)
    {
        this.tandaActiva = tandaActiva;
    }

    public Long getTandaId()
    {
        return this.tandaId;
    }

    public void setTandaId(Long tandaId)
    {
        this.tandaId = tandaId;
    }

}