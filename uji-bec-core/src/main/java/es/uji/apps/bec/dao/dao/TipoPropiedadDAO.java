package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoPropiedad;
import es.uji.commons.db.BaseDAO;

public interface TipoPropiedadDAO extends BaseDAO
{
    List<TipoPropiedad> getTiposPropiedades();

    TipoPropiedad getTipoPropiedadById(Long tipoPropiedadId);

    void deleteTipoPropiedad(Long tipoPropiedadId);
}
