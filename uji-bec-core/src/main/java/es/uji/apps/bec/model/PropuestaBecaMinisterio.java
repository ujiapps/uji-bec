package es.uji.apps.bec.model;

import es.uji.apps.bec.exceptions.AcademicosNoCargadosException;
import es.uji.apps.bec.exceptions.AyudaNoAplicableException;
import es.uji.apps.bec.exceptions.BecaConTandaNoPuedeCambiarConvocatoriaException;
import es.uji.apps.bec.exceptions.BecaCuantiaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaDenegacionDuplicadaException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.ConvocatoriaDeTandaYDeBecaDiferentesException;
import es.uji.apps.bec.exceptions.DistanciaDomicilioFamiliarNoEncontradaException;
import es.uji.apps.bec.exceptions.DistanciaLocalidadNoEncontradaException;
import es.uji.apps.bec.exceptions.EstadoBecaNoPermiteCalcularDenegacionesException;
import es.uji.apps.bec.exceptions.SolicitanteSinDomicilioFamiliarException;
import es.uji.apps.bec.model.domains.CodigoDenegacion;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.model.domains.CodigoProceso;

public class PropuestaBecaMinisterio extends PropuestaBeca
{
    private static Boolean denegada;

    public static void calculaPropuesta(Beca beca) throws BecaDenegacionDuplicadaException,
            BecaDuplicadaException, EstadoBecaNoPermiteCalcularDenegacionesException,
            BecaCuantiaDuplicadaException, AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        if (!beca.isEnEstadoPendienteTrabajadaODenegadaAcademico())
        {
            throw new EstadoBecaNoPermiteCalcularDenegacionesException();
        }

        denegada = false;

        procesaTotales(beca);
        if (beca.isEnProceso(CodigoProceso.ORDINARIO)
                || beca.isEnProceso(CodigoProceso.AUMENTO_CUANTIA))
        {
            beca.borraDenegacionesOrdenUno();
            procesaDenegaciones(beca);
        }
        else
        {
            beca.borraDenegacionesOrdenDos();
        }
        procesaAyudas(beca);
    }

    private static void procesaTotales(Beca beca)
    {
        calculaNumeroMiembrosComputables(beca);
        calculaNumeroHermanosYMinusvalias(beca);
        calculaNumeroArrendatariosDomicilios(beca);
    }

    private static void procesaDenegaciones(Beca beca) throws BecaDenegacionDuplicadaException,
            AcademicosNoCargadosException
    {
        Long orden = beca.calculaOrdenDenegacion();
        if (isMatriculado(beca))
        {
            deniegaSiPoseeTituloNoProcesoOrdinario(beca, orden);
            deniegaSiMaximoDeBecasConcedidasNoProcesoOrdinario(beca, orden);
            deniegaSiNoMinimoCreditosMatriculadosNoProcesoOrdinario(beca, orden);
            deniegaSiNoMinimoCreditosMatriculadosCursoAnteriorNoProcesoOrdinario(beca, orden);
            deniegaSiNoRendimientoCursoAnteriorNoProcesoOrdinario(beca, orden);
            deniegaSiPerdidaDeCursoLectivoNoProcesoOrdinario(beca, orden);

        }
        else
        {
            insertaDenegacion(CodigoDenegacion.NO_ESTAR_MATRICULADO_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiPerdidaDeCursoLectivoNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        if (beca.isEnProceso(CodigoProceso.ORDINARIO))
        {
            return;
        }

        Long curso = beca.getCurso();
        if (curso == null)
        {
            return;
        }

        Long becasConcedidasAnt = beca.getBecasConcedidasAnt();
        if (becasConcedidasAnt == null)
        {
            return;
        }

        if (curso > becasConcedidasAnt)
        {
            return;
        }

        insertaDenegacion(CodigoDenegacion.PERDIDA_CURSO_LECTIVO_MEC, beca, orden);
        denegada = true;
    }

    private static void deniegaSiNoRendimientoCursoAnteriorNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        CodigoDenegacion denegacion = calculaDenegacionDeRendimientoCursoAnterior(beca);
        if (denegacion != null && beca.noEstaEnProceso(CodigoProceso.ORDINARIO))
        {
            insertaDenegacion(denegacion, beca, orden);
            denegada = true;
        }
    }

    private static CodigoDenegacion calculaDenegacionDeRendimientoCursoAnterior(Beca beca)
    {
        Minimo minimos = Minimo.getMinimosByEstudioId(beca.getEstudio().getId(), beca.getSolicitante().getCursoAcademico().getId());

        if (!beca.isParaSegundoCursoOPosterior())
        {
            if (beca.getNotaMediaAnt()*minimos.getCoefCorrectorMec() < minimos.getNotaAccesoMec())
            {
                return CodigoDenegacion.NO_NOTA_MEDIA_ACCESO_MEC;
            }
            return null;
        }

        Float porcentajeSuperados = beca.getCreditosSuperadosAnt() * 100 / beca.getCreditosMatriculadosAnt();

        Float porcSuperadosMec = minimos.getPorcSuperadosMec();

        if (!Estudio.getEstudioById(beca.getEstudio().getId()).getTipo().equals("M"))
        {
            porcSuperadosMec = minimos.getPorcSuperadosMec() - ((beca.getCreditosMatriculadosAnt()
                    - minimos.getCreditosBecaCompleta()) * 100 / minimos.getCreditosBecaCompleta()) / 10;
        }

        if (beca.getCreditosMatriculadosAnt() >= minimos.getCreditosBecaParcial()
                && beca.getCreditosMatriculadosAnt() < minimos.getCreditosBecaCompleta())
        {
            porcSuperadosMec = 100F;
        }

        if (Float.compare(porcentajeSuperados, porcSuperadosMec) < 0)
        {
            return CodigoDenegacion.NO_MINIMO_CREDITOS_APROBADOS_MEC;
        }

        return null;
    }

    private static void deniegaSiNoMinimoCreditosMatriculadosNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException
    {
        if (!isMatriculadoCreditosMinimos(beca) && beca.noEstaEnProceso(CodigoProceso.ORDINARIO))
        {
            insertaDenegacion(CodigoDenegacion.NO_MINIMO_CREDITOS_MATRICULA_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiNoMinimoCreditosMatriculadosCursoAnteriorNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException, AcademicosNoCargadosException
    {
        if (beca.isParaSegundoCursoOPosterior() && beca.noEstaEnProceso(CodigoProceso.ORDINARIO)
                && !isMatriculadoCreditosMinimosCursoAnterior(beca))
        {
            insertaDenegacion(CodigoDenegacion.NO_MINIMO_CREDITOS_MATRICULA_ANT_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiMaximoDeBecasConcedidasNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        if (beca.isSuperadoNumeroMaximoDeBecasConcedidas()
                && beca.noEstaEnProceso(CodigoProceso.ORDINARIO))
        {
            insertaDenegacion(CodigoDenegacion.MAX_MECAS_CONCEDIDAS_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void deniegaSiPoseeTituloNoProcesoOrdinario(Beca beca, Long orden)
            throws BecaDenegacionDuplicadaException
    {
        if (beca.isPoseeTitulo() && beca.noEstaEnProceso(CodigoProceso.ORDINARIO))
        {
            insertaDenegacion(CodigoDenegacion.POSESION_DE_TITULO_MEC, beca, orden);
            denegada = true;
        }
    }

    private static void procesaAyudas(Beca beca) throws BecaCuantiaDuplicadaException,
            DistanciaLocalidadNoEncontradaException, BecaDuplicadaException,
            SolicitanteSinDomicilioFamiliarException, AcademicosNoCargadosException,
            BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException, AyudaNoAplicableException,
            BecaSinEstudioException, DistanciaDomicilioFamiliarNoEncontradaException
    {
        beca.borraAyudas();
        beca.setEstado(Estado.getEstadoById(CodigoEstado.TRABAJADA.getId()));
        beca.updateEstado();
        beca.insertaAyudas();
    }
}
