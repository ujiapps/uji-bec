package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.ResolucionAyuda;
import es.uji.commons.db.BaseDAO;

public interface ResolucionAyudaDAO extends BaseDAO
{
  public List<ResolucionAyuda> getAyudasByMultienvioAndBeca(Long multiEnvioId, Long becaId);
}
