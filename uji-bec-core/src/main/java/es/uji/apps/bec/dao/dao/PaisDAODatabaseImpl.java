package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Pais;
import es.uji.apps.bec.model.QPais;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PaisDAODatabaseImpl extends BaseDAODatabaseImpl implements PaisDAO
{
    @Override
    public List<Pais> getPaises()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPais pais = QPais.pais;
        query.from(pais).orderBy(pais.nombre.asc());

        return query.list(pais);
    }
}
