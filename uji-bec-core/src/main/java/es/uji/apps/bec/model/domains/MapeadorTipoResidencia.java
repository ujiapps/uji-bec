package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorTipoResidencia extends Mapeador<Long, Object>
{
    public MapeadorTipoResidencia()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, 1L);
        add(1L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 2L);
        add(2L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 3L);
        add(3L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 4L);
        add(4L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 5L);
        add(5L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 6L);
        add(6L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, 8L);
        add(8L, valores);
    }
}
