package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.DeclaranteDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the BC2_DECLARANTE database table.
 * 
 */
@Component
@Entity
@Table(name = "BC2_DECLARANTE")
@SuppressWarnings("serial")
public class Declarante implements Serializable
{
    private static DeclaranteDAO declaranteDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "CODIGO_PARENTESCO")
    private String codigoParentesco;
    private String declarante;
    private String empleador;
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DECLARANTE")
    private Date fechaDeclarante;
    private String identificacion;
    @Column(name = "INDICADOR_CURSO_ANTERIOR")
    private Integer indicadorCursoAnterior;
    @Column(name = "INDICADOR_CURSO_PENDIENTE")
    private Integer indicadorCursoPendiente;
    @Column(name = "INDICADOR_INDEPENDIENTE")
    private Integer indicadorIndependiente;
    @Column(name = "INDICADOR_INEM")
    private Integer indicadorInem;
    @Column(name = "INDICADOR_RENTAS_EXTRANJERO")
    private Integer indicadorRentasExtranjero;
    @Column(name = "INDICADOR_ULT_CURSO")
    private Integer indicadorUltCurso;
    @Column(name = "INGRESOS_ANUALES")
    private Float ingresosAnuales;
    @Column(name = "LOCALIDAD_DECLARANTE")
    private String localidadDeclarante;
    @Column(name = "RENTAS_EXTRANJERO")
    private Float rentasExtranjero;

    @Column(name = "DEC_IDENTIFICACION_CONSEN")
    private String decIdentificacionConsen;

    // bi-directional many-to-one association to TipoIdentificacion
    @ManyToOne
    @JoinColumn(name = "DEC_TIPO_IDENTIFICACION_CONSEN")
    private TipoIdentificacion decTipoIdentificacionConsen;

    // bi-directional many-to-one association to TipoMiembro
    @ManyToOne
    @JoinColumn(name = "DEC_COD_PARENTESCO_CONSEN")
    private TipoMiembro decCodParentescoConsen;

    // bi-directional many-to-one association to TipoIdentificacion
    @ManyToOne
    @JoinColumn(name = "TIPO_IDENTIFICACION_ID")
    private TipoIdentificacion tipoIdentificacion;
    // one-to-one association to Solicitante
    @OneToOne
    @JoinColumn(name = "SOLICITANTE_ID")
    private Solicitante solicitante;

    public Declarante()
    {
    }

    public static void delete(Long declaranteId) throws RegistroNoEncontradoException
    {
        List<Declarante> listaDeclarantes = declaranteDAO.get(Declarante.class, declaranteId);

        if (listaDeclarantes != null && !listaDeclarantes.isEmpty())
        {
            declaranteDAO.deleteDeclarante(listaDeclarantes.get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Autowired
    public void setDeclaranteDAO(DeclaranteDAO declaranteDAO)
    {
        Declarante.declaranteDAO = declaranteDAO;
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getCodigoParentesco()
    {
        return this.codigoParentesco;
    }

    public void setCodigoParentesco(String codigoParentesco)
    {
        this.codigoParentesco = codigoParentesco;
    }

    public String getDeclarante()
    {
        return this.declarante;
    }

    public void setDeclarante(String declarante)
    {
        this.declarante = declarante;
    }

    public String getEmpleador()
    {
        return this.empleador;
    }

    public void setEmpleador(String empleador)
    {
        this.empleador = empleador;
    }

    public Date getFechaDeclarante()
    {
        return this.fechaDeclarante;
    }

    public void setFechaDeclarante(Date fechaDeclarante)
    {
        this.fechaDeclarante = fechaDeclarante;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public Integer getIndicadorCursoAnterior()
    {
        return this.indicadorCursoAnterior;
    }

    public void setIndicadorCursoAnterior(Integer indicadorCursoAnterior)
    {
        this.indicadorCursoAnterior = indicadorCursoAnterior;
    }

    public void setIndicadorCursoAnterior(Boolean indicadorCursoAnterior)
    {
        if (indicadorCursoAnterior == null)
        {
            this.indicadorCursoAnterior = null;
        }
        this.indicadorCursoAnterior = (indicadorCursoAnterior ? 1 : 0);
    }

    public Boolean isIndicadorCursoAnterior()
    {
        if (this.indicadorCursoAnterior == null)
        {
            return null;
        }
        return (this.indicadorCursoAnterior.equals(1) ? true : false);
    }

    public Integer getIndicadorCursoPendiente()
    {
        return this.indicadorCursoPendiente;
    }

    public void setIndicadorCursoPendiente(Integer indicadorCursoPendiente)
    {
        this.indicadorCursoPendiente = indicadorCursoPendiente;
    }

    public void setIndicadorCursoPendiente(Boolean indicadorCursoPendiente)
    {
        if (indicadorCursoPendiente == null)
        {
            this.indicadorCursoPendiente = null;
        }
        this.indicadorCursoPendiente = (indicadorCursoPendiente ? 1 : 0);
    }

    public Boolean isIndicadorCursoPendiente()
    {
        if (this.indicadorCursoPendiente == null)
        {
            return null;
        }
        return (this.indicadorCursoPendiente.equals(1) ? true : false);
    }

    public Integer getIndicadorIndependiente()
    {
        return this.indicadorIndependiente;
    }

    public void setIndicadorIndependiente(Integer indicadorIndependiente)
    {
        this.indicadorIndependiente = indicadorIndependiente;
    }

    public void setIndicadorIndependiente(Boolean indicadorIndependiente)
    {
        if (indicadorIndependiente == null)
        {
            this.indicadorIndependiente = null;
        }
        this.indicadorIndependiente = (indicadorIndependiente ? 1 : 0);
    }

    public Boolean isIndicadorIndependiente()
    {
        if (this.indicadorIndependiente == null)
        {
            return null;
        }
        return (this.indicadorIndependiente.equals(1) ? true : false);
    }

    public Integer getIndicadorInem()
    {
        return this.indicadorInem;
    }

    public void setIndicadorInem(Integer indicadorInem)
    {
        this.indicadorInem = indicadorInem;
    }

    public void setIndicadorInem(Boolean indicadorInem)
    {
        if (indicadorInem == null)
        {
            this.indicadorInem = null;
        }
        this.indicadorInem = (indicadorInem ? 1 : 0);
    }

    public Boolean isIndicadorInem()
    {
        if (this.indicadorInem == null)
        {
            return null;
        }
        return (this.indicadorInem.equals(1) ? true : false);
    }

    public Integer getIndicadorRentasExtranjero()
    {
        return this.indicadorRentasExtranjero;
    }

    public void setIndicadorRentasExtranjero(Integer indicadorRentasExtranjero)
    {
        this.indicadorRentasExtranjero = indicadorRentasExtranjero;
    }

    public void setIndicadorRentasExtranjero(Boolean indicadorRentasExtranjero)
    {
        if (indicadorRentasExtranjero == null)
        {
            this.indicadorRentasExtranjero = null;
        }
        this.indicadorRentasExtranjero = (indicadorRentasExtranjero ? 1 : 0);
    }

    public Boolean isIndicadorRentasExtranjero()
    {
        if (this.indicadorRentasExtranjero == null)
        {
            return null;
        }
        return (this.indicadorRentasExtranjero.equals(1) ? true : false);
    }

    public Integer getIndicadorUltCurso()
    {
        return this.indicadorUltCurso;
    }

    public void setIndicadorUltCurso(Integer indicadorUltCurso)
    {
        this.indicadorUltCurso = indicadorUltCurso;
    }

    public void setIndicadorUltCurso(Boolean indicadorUltCurso)
    {
        if (indicadorUltCurso == null)
        {
            this.indicadorUltCurso = null;
        }
        this.indicadorUltCurso = (indicadorUltCurso ? 1 : 0);
    }

    public Boolean isIndicadorUltCurso()
    {
        if (this.indicadorUltCurso == null)
        {
            return null;
        }
        return (this.indicadorUltCurso.equals(1) ? true : false);
    }

    public Float getIngresosAnuales()
    {
        return this.ingresosAnuales;
    }

    public void setIngresosAnuales(Float ingresosAnuales)
    {
        this.ingresosAnuales = ingresosAnuales;
    }

    public String getLocalidadDeclarante()
    {
        return this.localidadDeclarante;
    }

    public void setLocalidadDeclarante(String localidadDeclarante)
    {
        this.localidadDeclarante = localidadDeclarante;
    }

    public Float getRentasExtranjero()
    {
        return this.rentasExtranjero;
    }

    public void setRentasExtranjero(Float rentasExtranjero)
    {
        this.rentasExtranjero = rentasExtranjero;
    }

    public TipoIdentificacion getTipoIdentificacion()
    {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion)
    {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getDecIdentificacionConsen() {
        return decIdentificacionConsen;
    }

    public void setDecIdentificacionConsen(String decIdentificacionConsen) {
        this.decIdentificacionConsen = decIdentificacionConsen;
    }

    public TipoIdentificacion getDecTipoIdentificacionConsen()
    {
        return this.decTipoIdentificacionConsen;
    }

    public void setDecTipoIdentificacionConsen(TipoIdentificacion decTipoIdentificacionConsen)
    {
        this.decTipoIdentificacionConsen = decTipoIdentificacionConsen;
    }

    public TipoMiembro getDecCodParentescoConsen()
    {
        return this.decCodParentescoConsen;
    }

    public void setDecCodParentescoConsen(TipoMiembro decCodParentescoConsen)
    {
        this.decCodParentescoConsen = decCodParentescoConsen;
    }

    public Solicitante getSolicitante()
    {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante)
    {
        this.solicitante = solicitante;
    }

    @Transactional
    public void delete()
    {
        declaranteDAO.delete(this);
    }

    public static List<Declarante> getDeclaranteById(Long declaranteId)
    {
        return declaranteDAO.getDeclaranteById(declaranteId);
    }

    @Transactional
    public static Declarante insertRegistro(Long solicitanteId, String declarante,
            Integer indicadorRentasExtranjero, Float rentasExtranjero, String localidadDeclarante,
            Date fechaDeclarante, Integer indicadorUltCurso, Integer indicadorCursoPendiente,
            Integer indicadorCursoAnterior, Integer indicadorIndependiente, Integer indicadorInem,
            String empleador, Long tipoIdentificacionId, String identificacion,
            Float ingresosAnuales, String codigoParentesco,
            String decIdentificacionConsen, Long tipoIdentificacionConsenId, Long decCodParentescoConsenId )
    {
        Declarante registroDeclarante = new Declarante();
        Solicitante solicitante = Solicitante.getSolicitanteById(solicitanteId);

        if (tipoIdentificacionId != null)
        {
            TipoIdentificacion tipoIdentificacion = TipoIdentificacion.getTipoIdentificacionById(tipoIdentificacionId);
            registroDeclarante.setTipoIdentificacion(tipoIdentificacion);
        }

        if (tipoIdentificacionConsenId != null)
        {
            TipoIdentificacion tipoIdentificacionConsen = TipoIdentificacion.getTipoIdentificacionById(tipoIdentificacionConsenId);
            registroDeclarante.setTipoIdentificacion(tipoIdentificacionConsen);
        }

        if (decCodParentescoConsenId != null)
        {
            TipoMiembro decCodParentescoConsen = TipoMiembro.getTipoMiembroById(decCodParentescoConsenId);
            registroDeclarante.setDecCodParentescoConsen(decCodParentescoConsen);
        }

        registroDeclarante.setSolicitante(solicitante);
        registroDeclarante.setDeclarante(declarante);
        registroDeclarante.setIndicadorRentasExtranjero(indicadorRentasExtranjero);
        registroDeclarante.setRentasExtranjero(rentasExtranjero);
        registroDeclarante.setLocalidadDeclarante(localidadDeclarante);
        registroDeclarante.setFechaDeclarante(fechaDeclarante);
        registroDeclarante.setIndicadorUltCurso(indicadorUltCurso);
        registroDeclarante.setIndicadorCursoPendiente(indicadorCursoPendiente);
        registroDeclarante.setIndicadorCursoAnterior(indicadorCursoAnterior);
        registroDeclarante.setIndicadorIndependiente(indicadorIndependiente);
        registroDeclarante.setIndicadorIndependiente(indicadorIndependiente);
        registroDeclarante.setEmpleador(empleador);
        registroDeclarante.setIdentificacion(identificacion);
        registroDeclarante.setIngresosAnuales(ingresosAnuales);
        registroDeclarante.setCodigoParentesco(codigoParentesco);
        registroDeclarante.setDecIdentificacionConsen(decIdentificacionConsen);

        registroDeclarante = declaranteDAO.insert(registroDeclarante);
        return registroDeclarante;

    }

    @Transactional
    public Declarante updateRegistro(String declarante, Integer indicadorRentasExtranjero,
            Float rentasExtranjero, String localidadDeclarante, Date fechaDeclarante,
            Integer indicadorUltCurso, Integer indicadorCursoPendiente,
            Integer indicadorCursoAnterior, Integer indicadorIndependiente, Integer indicadorInem,
            String empleador, Long tipoIdentificacionId, String identificacion,
            Float ingresosAnuales, String codigoParentesco,
            String decIdentificacionConsen, Long decTipoIdentificacionConsenId, Long decCodParentescoConsenId)
    {

        TipoIdentificacion tipoIdentificacion = null;
        if (tipoIdentificacionId != null && tipoIdentificacionId > 0)
        {
            tipoIdentificacion = TipoIdentificacion.getTipoIdentificacionById(tipoIdentificacionId);
        }

        TipoIdentificacion decTipoIdentificacionConsen = null;
        if (decTipoIdentificacionConsenId != null && decTipoIdentificacionConsenId > 0)
        {
            decTipoIdentificacionConsen = TipoIdentificacion.getTipoIdentificacionById(decTipoIdentificacionConsenId);
        }

        TipoMiembro decCodParentescoConsen = null;
        if (decCodParentescoConsenId != null && decCodParentescoConsenId > 0)
        {
            decCodParentescoConsen = TipoMiembro.getTipoMiembroById(decCodParentescoConsenId);
        }

        this.setDeclarante(declarante);
        this.setIndicadorRentasExtranjero(indicadorRentasExtranjero);
        this.setRentasExtranjero(rentasExtranjero);
        this.setLocalidadDeclarante(localidadDeclarante);
        this.setFechaDeclarante(fechaDeclarante);
        this.setIndicadorUltCurso(indicadorUltCurso);
        this.setIndicadorCursoPendiente(indicadorCursoPendiente);
        this.setIndicadorCursoAnterior(indicadorCursoAnterior);
        this.setIndicadorIndependiente(indicadorIndependiente);
        this.setIndicadorInem(indicadorInem);
        this.setEmpleador(empleador);
        this.setTipoIdentificacion(tipoIdentificacion);
        this.setDecTipoIdentificacionConsen(decTipoIdentificacionConsen);
        this.setDecCodParentescoConsen(decCodParentescoConsen);
        this.setDecIdentificacionConsen(decIdentificacionConsen);
        this.setIdentificacion(identificacion);
        this.setIngresosAnuales(ingresosAnuales);
        this.setCodigoParentesco(codigoParentesco);

        declaranteDAO.updateDeclarante(this);
        return this;
    }

    @Transactional
    public void insert()
    {
        declaranteDAO.insert(this);
    }

    public static List<Declarante> getDeclaranteBySolicitanteId(Long solicitanteId)
    {
        return declaranteDAO.getDeclaranteBySolicitanteId(solicitanteId);
    }
}
