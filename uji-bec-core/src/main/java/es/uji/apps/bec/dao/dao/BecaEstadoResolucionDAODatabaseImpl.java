package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.BecaEstadoResolucion;
import es.uji.apps.bec.model.QBecaEstadoResolucion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class BecaEstadoResolucionDAODatabaseImpl extends BaseDAODatabaseImpl implements
        BecaEstadoResolucionDAO
{

    @Override
    public List<BecaEstadoResolucion> getResolucionesByMultienvioId(Long multiEnvioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QBecaEstadoResolucion qBecaEstadoResolucion = QBecaEstadoResolucion.becaEstadoResolucion;

        query.from(qBecaEstadoResolucion)
                .where(qBecaEstadoResolucion.multienvioId.eq(multiEnvioId));

        return query.list(qBecaEstadoResolucion);
    }

}
