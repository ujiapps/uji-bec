package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Localidad;
import es.uji.apps.bec.model.QLocalidad;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LocalidadDAODatabaseImpl extends BaseDAODatabaseImpl implements LocalidadDAO
{
    @Override
    public List<Localidad> getLocalidades()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLocalidad localidad = QLocalidad.localidad;
        query.from(localidad).orderBy(localidad.nombre.asc());

        return query.list(localidad);
    }

    @Override
    public List<Localidad> getLocalidadesByProvincia(Long provinciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLocalidad localidad = QLocalidad.localidad;
        query.from(localidad).where(localidad.provincia.id.eq(provinciaId))
                .orderBy(localidad.nombre.asc());

        return query.list(localidad);
    }

    @Override
    public Localidad getLocalidadByCodigo(String codigoLocalidadMinisterio)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLocalidad localidad = QLocalidad.localidad;
        query.from(localidad).where(localidad.codigo.eq(codigoLocalidadMinisterio))
                .orderBy(localidad.nombre.asc());
        if (query.list(localidad).size() > 0)
        {
            return query.list(localidad).get(0);
        }
        return null;
    }
}
