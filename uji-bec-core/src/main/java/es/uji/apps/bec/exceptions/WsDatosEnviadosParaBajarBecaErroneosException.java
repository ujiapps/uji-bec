package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class WsDatosEnviadosParaBajarBecaErroneosException extends CoreDataBaseException
{
    public WsDatosEnviadosParaBajarBecaErroneosException()
    {
        super("Alguna de les dades enviades al MEC per baixar la beca es errònia.");
    }

    public WsDatosEnviadosParaBajarBecaErroneosException(String message)
    {
        super(message);
    }
}
