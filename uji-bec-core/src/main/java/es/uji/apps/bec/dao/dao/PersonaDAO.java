package es.uji.apps.bec.dao.dao;

import es.uji.apps.bec.model.Persona;
import es.uji.commons.db.BaseDAO;

public interface PersonaDAO extends BaseDAO
{
    Persona getPersonaByDNI(String dni);

    Persona getPersonaByPersonaId(Long personaId);
}
