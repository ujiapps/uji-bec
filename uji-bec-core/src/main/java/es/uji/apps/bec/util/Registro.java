package es.uji.apps.bec.util;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.model.domains.CodigoOrganismo;

public class Registro<T, K>
{
    private T valor;
    private Map<CodigoOrganismo, Registro<T, K>> relaciones;

    public Registro(T valor)
    {
        this.valor = valor;
        this.relaciones = new HashMap<CodigoOrganismo, Registro<T, K>>();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void addRelacionCon(CodigoOrganismo tipo, K valor)
    {
        relaciones.put(tipo, new Registro(valor));
    }

    public T getValor()
    {
        return this.valor;
    }

    @SuppressWarnings("unchecked")
    public K getValor(CodigoOrganismo tipo)
    {
        if (relaciones.get(tipo) != null)
        {
            return (K) relaciones.get(tipo).getValor();
        }

        return null;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public boolean equals(Object obj)
    {
        if (obj instanceof Registro)
        {
            return ((Registro) obj).getValor().equals(valor);
        }

        return false;
    }
}