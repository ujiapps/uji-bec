package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.RecursoDetalleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "BC2_BECAS_RECURSOS_DETALLE")
@Component
public class RecursoDetalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Recurso recurso;
    @ManyToOne
    @JoinColumn(name = "CUANTIA_ID")
    private Cuantia cuantia;
    @Column(name = "IMPORTE")
    private Float importe;
    @Column(name = "TEXTO")
    private String texto;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "CODIGO")
    private String codigo;

    public RecursoDetalle()
    {

    }

    private static RecursoDetalleDAO recursoDetalleDAO;

    @Autowired
    public void setRecursoDetalleDAO(RecursoDetalleDAO recursoDetalleDAO)
    {
        RecursoDetalle.recursoDetalleDAO = recursoDetalleDAO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Recurso getRecurso() {
        return recurso;
    }

    public void setRecurso(Recurso recurso) {
        this.recurso = recurso;
    }

    public Cuantia getCuantia() {
        return cuantia;
    }

    public void setCuantia(Cuantia cuantia) {
        this.cuantia = cuantia;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void insert()
    {
        recursoDetalleDAO.insert(this);
    }

    public static void delete(Long cuantiaId)
    {
        recursoDetalleDAO.delete(RecursoDetalle.class, cuantiaId);
    }

    public static List<RecursoDetalle> getRecursoDetallesByRecursoIdAndTipo(Long recursoId, String tipo)
    {
        return recursoDetalleDAO.getRecursoDetallesByRecursoIdAndTipo(recursoId, tipo);
    }
}
