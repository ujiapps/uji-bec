package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.RegistroEnvioDAO;

/**
 * The persistent class for the BC2_REGISTRO_ENVIOS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_REGISTRO_ENVIOS")
public class RegistroEnvio implements Serializable
{
    private static final long EJECUCION_CORRECTA = 1L;
    private static final long EJECUCION_EN_PROCESO = 0L;
    private static final long EJECUCION_INCORRECTA = -1L;    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long estado;

    @Column(name = "ENVIO_ID")
    private Long envioId;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    // bi-directional many-to-one association to Bc2Convocatoria
    @ManyToOne
    @JoinColumn(name = "CONVOCATORIA_ID")
    private Convocatoria convocatoria;

    // bi-directional many-to-one association to Bc2CursosAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private CursoAcademico cursoAcademico;

    // bi-directional many-to-one association to Bc2ExtPersona
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    // bi-directional many-to-one association to Bc2Tanda
    @ManyToOne
    @JoinColumn(name = "TANDA_ID")
    private Tanda tanda;

    private static RegistroEnvioDAO registroEnvioDAO;

    @Autowired
    public void setRegistroEnvioDAO(RegistroEnvioDAO registroEnvioDAO)
    {
        RegistroEnvio.registroEnvioDAO = registroEnvioDAO;
    }

    public RegistroEnvio()
    {
        this.setEstado(0L);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getEstado()
    {
        return this.estado;
    }

    public void setEstado(Long estado)
    {
        this.estado = estado;
    }

    public Long getEnvioId()
    {
        return this.envioId;
    }

    public void setEnvioId(Long envioId)
    {
        this.envioId = envioId;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Convocatoria getConvocatoria()
    {
        return this.convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public void setCursoAcademico(CursoAcademico cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Tanda getTanda()
    {
        return this.tanda;
    }

    public void setTanda(Tanda tanda)
    {
        this.tanda = tanda;
    }

    @Transactional
    public RegistroEnvio insert()
    {
        return registroEnvioDAO.insert(this);
    }

    public static RegistroEnvio getRegistroEnvioByTanda(Long tandaId)
    {
        return registroEnvioDAO.getRegistroEnvioByTanda(tandaId);
    }

    @Transactional
    public void update()
    {
        registroEnvioDAO.update(this);
    }

    public static RegistroEnvio getRegistroEnvioById(Long registroId)
    {
        return registroEnvioDAO.get(RegistroEnvio.class, registroId).get(0);
    }
    
    public RegistroEnvio preparar(Tanda tanda, Long personaId) throws WsErrorAlPrepararMultienvioException
    {
        Persona persona = Persona.preparaPersonaConId(personaId);
        
        if (this.getId() == null)
        {
            this.setCursoAcademico(tanda.getCursoAcademico());
            this.setTanda(tanda);
            this.setConvocatoria(tanda.getConvocatoria());
            this.setPersona(persona);
            this.insert();
        }
        else
        {
            if (this.isCorrecto())
            {
                throw new WsErrorAlPrepararMultienvioException(
                        "Les dades de l\'enviament ja estan generades. Ja podeu enviar-la");
            }

            if (this.isPendiente())
            {
                throw new WsErrorAlPrepararMultienvioException(
                        "Les dades de l\'enviament estan ja generant-se. Espereu...");
            }

            this.setEstado(0L);
            this.update();
        }
        return null;
    }

    public RegistroEnvio prepararAcademicos(Tanda tanda, Long personaId) throws WsErrorAlPrepararMultienvioException
    {
        Persona persona = Persona.preparaPersonaConId(personaId);

        this.setCursoAcademico(tanda.getCursoAcademico());
        this.setTanda(tanda);
        this.setConvocatoria(tanda.getConvocatoria());
        this.setPersona(persona);
        this.insert();

        return this;
    }

    public Boolean isPendiente()
    {
        return this.getEstado() == EJECUCION_EN_PROCESO;
    }

    public Boolean isCorrecto()
    {
        return this.getEstado() == EJECUCION_CORRECTA;
    }
    
    public Boolean isIncorrecto()
    {
        return this.getEstado() == EJECUCION_INCORRECTA;
    }
}