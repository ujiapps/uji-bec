package es.uji.apps.bec.webservices;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.*;
import es.uji.apps.bec.model.domains.CodigoConvocatoria;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.services.ResultadoDescarga;
import es.uji.apps.bec.webservices.ministerio.colaboracion.lista.SolicitudRegistro;
import es.uji.apps.bec.webservices.ministerio.colaboracion.lista.SolicitudesRegistro;
import es.uji.apps.bec.webservices.ministerio.solicitudes.SolicitudType;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class DescargaSolicitudes {
    private static Logger log = Logger.getLogger(DescargaSolicitudes.class);

    private static final String CLASE_ESTUDIO = "Estudio";
    private final Unmarshaller unmarshallerListaSolicitudes;
    private final Unmarshaller unmarshallerSolicitud;

    private WebserviceMinisterio webserviceMinisterio;
    private BecaDAO becaDAO;

    private static final String NUMERO_SECUENCIA = "1";
    private static final String UNIDAD_DE_TRAMITE_TIPO = "1";
    private static final String UNIDAD_DE_TRAMITE_CODIGO = "400";


    @Autowired
    public DescargaSolicitudes(WebserviceMinisterio webserviceMinisterio, BecaDAO becaDAO)
            throws JAXBException {
        this.webserviceMinisterio = webserviceMinisterio;
        this.becaDAO = becaDAO;

        JAXBContext context = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.colaboracion.lista");
        unmarshallerListaSolicitudes = context.createUnmarshaller();

        JAXBContext contextColaboracion = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.solicitudes");
        unmarshallerSolicitud = contextColaboracion.createUnmarshaller();
    }

    public List<ResultadoDescarga> descarga(Integer cursoAca, String convocatoria, Boolean descargadas) throws Exception {
        Integer contador = 0;

        SolicitudesRegistro solicitudesRegistro = obtenerListaSolicitudes(cursoAca, convocatoria, descargadas);

        List<ResultadoDescarga> resultados = new ArrayList<ResultadoDescarga>();

        List<SolicitudRegistro> listaSolicitudesRegistro = solicitudesRegistro.getSolicitudesRegistro();

        String mensajeError;

        System.out.println("Total becas: " + listaSolicitudesRegistro.size());
        log.info("Total de solicitudes descargadas: " + contador.toString());
        DescargaSolicitudesLog descargaSolicitudesLog = new DescargaSolicitudesLog(Long.parseLong("-1"), "", "Inicio descarga "+listaSolicitudesRegistro.size());
        descargaSolicitudesLog.insert();

        /*
        // Si queremos descargar solo algunas becas determinadas y sabemnos DNI
        listaSolicitudesRegistro = listaSolicitudesRegistro.stream()
                .filter(v -> Arrays.asList(
                        "-1", "-1"

                        )
                        .contains(v.getCiudNif()))
                .collect(Collectors.toList());

        // Si queremos descargar solo algunas becas determinadas y sabemnos codigo archivo temporal
        listaSolicitudesRegistro = listaSolicitudesRegistro.stream()
                .filter(v -> Arrays.asList(
                                32044734L,31521675L
                        )
                        .contains(v.getCiudSqCiudadano()))
                .collect(Collectors.toList());
        */

        for (SolicitudRegistro solicitudRegistro : listaSolicitudesRegistro) {
            contador++;
            String nombre = solicitudRegistro.getCiudApellido1() + " " + solicitudRegistro.getCiudApellido2() + ", " + solicitudRegistro.getCiudNombre();

            Beca beca = Beca.getBecaByArchivoTemporal(solicitudRegistro.getCiudSqCiudadano());
            if (beca != null) {
                /*
                mensajeError = "Beca descargada previamente";
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
                descargaSolicitudesLog = new DescargaSolicitudesLog(solicitudRegistro.getCiudSqCiudadano(), solicitudRegistro.getCiudNif(), mensajeError);
                descargaSolicitudesLog.insert();
                */
                continue;
            }

            try {
                ResultadoDescarga resultado = guardaSolicitud(solicitudRegistro, cursoAca, convocatoria);
                if (resultado.isCorrecto()) {
                    resultados.add(resultado);
                    mensajeError = "OK";
                } else {
                    mensajeError = "No se ha podido descargar la solicitud ";
                }
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
            } catch (EstudioSolicitadoNoMatriculadoException e) {
                mensajeError = "ERROR Beca importada pero sin estudios";
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
            } catch (PersonaNoEncontradaException e) {
                mensajeError = "ERROR DNI no encontrado en personas";
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );

            } catch (BecaDuplicadaException e) {
                mensajeError = "ERROR Beca duplicada";
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
            } catch (BecaDenegacionNoEncontradaException e) {
                mensajeError = "ERROR " + e.getMessage();
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
            } catch (Exception e) {
                mensajeError = "ERROR No controlado. Ha fallado la solicitud. " + e.getMessage();
                System.out.println(
                        String.format("%4d", contador)
                                + String.format("%15s", solicitudRegistro.getCiudNif())
                                + String.format("%15d", solicitudRegistro.getCiudSqCiudadano())
                                + String.format("%-70s", "   " + mensajeError)
                                + String.format("%-70s", "   " + nombre)
                );
            }

            descargaSolicitudesLog = new DescargaSolicitudesLog(solicitudRegistro.getCiudSqCiudadano(), solicitudRegistro.getCiudNif(), mensajeError);
            descargaSolicitudesLog.insert();
        }

        log.info("Total de solicitudes descargadas: " + contador.toString());
        descargaSolicitudesLog = new DescargaSolicitudesLog(Long.parseLong("-2"), "", "Total de solicitudes descargadas: " + contador.toString());
        descargaSolicitudesLog.insert();

        return resultados;
    }

    @Transactional
    private ResultadoDescarga guardaSolicitud(SolicitudRegistro solicitudRegistro, Integer cursoAca, String convocatoria)
            throws RegistroNoEncontradoException, BecaDuplicadaException, EstudioMinisterioMayores25Exception,
            EstudioSolicitadoNoMatriculadoException, BecaSinEstudioException, PersonaNoEncontradaException, BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException {
        boolean descargaCorrecta = false;
        int numeroIntentos = 0;
        SolicitudType solicitudType = null;

        while (!descargaCorrecta && numeroIntentos < 3) {
            try {
                solicitudType = obtenerSolicitudCiudadano(cursoAca, convocatoria, solicitudRegistro);
                becaDAO.insert(solicitudType);
                descargaCorrecta = true;
            } catch (Exception e) {
                realizaPausaEntreIntentos();
                numeroIntentos++;
            }
        }
        if (descargaCorrecta) {
            importaDatosDeSolicitud(solicitudType);
        } else {
            log.info("No ha podido descargarse la solicitud: " + solicitudRegistro.getCiudNif());
        }
        return new ResultadoDescarga(descargaCorrecta, cursoAca, convocatoria, solicitudRegistro.getCiudNif(), solicitudRegistro.getCiudSqCiudadano());
    }

    private void importaDatosDeSolicitud(SolicitudType solicitudType)
            throws PersonaNoEncontradaException, BecaDuplicadaException, BecaSinEstudioException, RegistroNoEncontradoException,
            EstudioSolicitadoNoMatriculadoException, EstudioMinisterioMayores25Exception, BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException {
        String identificacion = solicitudType.getDatosPersonales().getDapeNif();

        Long cursoAcademicoId = Long.parseLong(solicitudType.getSoliIdCurso());
        Long estudioId = Diccionario.getValor(cursoAcademicoId, CLASE_ESTUDIO, CodigoOrganismo.MINISTERIO.getId(), solicitudType.getSoliCodEstudio());
        Long convocatoriaId = CodigoConvocatoria.GENERAL.getId();
        Long codigoArchivoTemporal = solicitudType.getSoliSqCiudadano();

        Persona persona = Persona.getPersonaByDNI(identificacion);
        if (persona == null) {
            throw new PersonaNoEncontradaException();
        }

        Solicitante solicitante = Solicitante.getSolicitanteByCursoAcademicoAndPersonaId(cursoAcademicoId.toString(), persona.getId());

        if (solicitante == null) {
            solicitante = new Solicitante(cursoAcademicoId, persona);
        }

        Beca beca = new Beca(solicitante, identificacion, codigoArchivoTemporal.toString(), estudioId, convocatoriaId);

        solicitante.addBeca(beca);
        solicitante.insert();

        if (beca.isDuplicada()) {
            throw new BecaDuplicadaException();
        }
        beca.insert();

        solicitante.importarDatosMinisterio(solicitudType);
        solicitante.update();

        beca.importarDatosMinisterio(solicitudType);
        beca.update();
    }

    private void realizaPausaEntreIntentos() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Error en la espera de reintento de descarga de solicitud", e);
        }
    }

    private SolicitudesRegistro obtenerListaSolicitudes(Integer cursoAca, String convocatoria, Boolean descargadas)
            throws Exception
    {

        String xmlSolicitudesCiudadano = webserviceMinisterio.getXmlSolicitudesCiudadanoRT(cursoAca, convocatoria, descargadas);

        Pattern pattern = Pattern.compile("<Error>(.*?)</Error>");
        Matcher matcher = pattern.matcher(xmlSolicitudesCiudadano);

        if (matcher.find()) {
            String errorMessage = matcher.group(1);
            throw new DescargaSolicitudesException(errorMessage);
        }

        return (SolicitudesRegistro) unmarshallerListaSolicitudes.unmarshal(new ByteArrayInputStream(xmlSolicitudesCiudadano.getBytes()));
    }

    private SolicitudType obtenerSolicitudCiudadano(Integer cursoAca, String convocatoria, SolicitudRegistro solicitudRegistro)
            throws RemoteException, MalformedURLException,
            ServiceException, UnsupportedEncodingException,
            WsDatosEnviadosParaBajarBecaErroneosException, JAXBException, SOAPException {
        String nif = solicitudRegistro.getCiudNif();
        Long archivoTemporal = solicitudRegistro.getCiudSqCiudadano();

        // Borrado de la posible fila existente en solicitudes

        byte[] xmlSolicitudCiudadano = webserviceMinisterio.getXmlSolicitudCiudadano(cursoAca, convocatoria, 1, nif, archivoTemporal);

        if (xmlSolicitudCiudadano == null || xmlSolicitudCiudadano.length == 0) {
            return new SolicitudType();
        }

        log.debug(new String(xmlSolicitudCiudadano));

        JAXBElement<SolicitudType> element = (JAXBElement<SolicitudType>) unmarshallerSolicitud.unmarshal(new ByteArrayInputStream(xmlSolicitudCiudadano));
        SolicitudType solicitudType = element.getValue();
        solicitudType.setSoliSqCiudadano(solicitudRegistro.getCiudSqCiudadano());
        return solicitudType;
    }

    @Transactional
    public void insertaNuevaSolicitud(SolicitudType solicitudType)
            throws SolicitudTypeDatosDuplicadosException {
        // List<SolicitudType> solicitudTypeExistente = becaDAO.getByArchivoTemporalAndNIF(
        // solicitudType.getSoliIdArchivo(), solicitudType.getSoliNifCuenta());

        List<SolicitudType> solicitudTypeExistente = new ArrayList<SolicitudType>();
        if (solicitudTypeExistente.size() > 1) {
            throw new SolicitudTypeDatosDuplicadosException();
        }
        if (solicitudTypeExistente.size() == 1) {
            becaDAO.delete(SolicitudType.class, solicitudTypeExistente.get(0).getHjid());
        }
        becaDAO.insert(solicitudType);
    }
}
