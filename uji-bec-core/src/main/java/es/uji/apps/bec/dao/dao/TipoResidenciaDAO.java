package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoResidencia;
import es.uji.commons.db.BaseDAO;

public interface TipoResidenciaDAO extends BaseDAO
{

    List<TipoResidencia> getTiposResidencias();

    List<TipoResidencia> getTiposResidenciaById(Long tipoResidenciaId);

}
