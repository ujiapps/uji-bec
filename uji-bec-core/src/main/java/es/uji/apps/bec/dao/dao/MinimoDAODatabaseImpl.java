package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.Minimo;
import es.uji.apps.bec.model.QMinimo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MinimoDAODatabaseImpl extends BaseDAODatabaseImpl implements MinimoDAO
{
    private QMinimo minimo = QMinimo.minimo;

    @Override
    public List<Minimo> getMinimosByEstudioId(Long estudioId, Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(minimo).where(minimo.estudio.id.eq(estudioId).and(minimo.cursoAcademico.id.eq(cursoAcademicoId)));
        return query.list(minimo);
    }
}
