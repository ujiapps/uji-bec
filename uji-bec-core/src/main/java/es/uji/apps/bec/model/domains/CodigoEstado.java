package es.uji.apps.bec.model.domains;

public enum CodigoEstado
{
    PENDIENTE(1L),

    TRABAJADA(2L),

    DENEGADA_ACADEMICOS(3L),

    ENVIADA(4L),

    CONCEDIDA(5L),

    DENEGADA(6L),

    BAJA(7L),

    TRASLADADA(8L),

    INCORRECTA(9L),

    EXCLUIDA(10L),

    REVOCADA_VOLUNTARIA(11L),

    REVOCADA_CREDITOS(12L),

    DOCUMENTACION_PENDIENTE(13L),

    DOCUMENTACION_ENVIADA(14L),

    DOCUMENTACION_CORRECTA(15L),

    ACADEMICOS_CALCULADOS(16L),

    ACADEMICOS_ENVIADOS(17L);

    private Long id;

    CodigoEstado(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public boolean igual(Object otherObject)
    {
        if (otherObject instanceof Long)
        {
            return ((Long) otherObject) == getId();
        }

        return super.equals(otherObject);
    }
}
