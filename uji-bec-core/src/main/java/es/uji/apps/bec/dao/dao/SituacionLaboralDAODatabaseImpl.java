package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QSituacionLaboral;
import es.uji.apps.bec.model.SituacionLaboral;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SituacionLaboralDAODatabaseImpl extends BaseDAODatabaseImpl implements
        SituacionLaboralDAO
{

    @Override
    public List<SituacionLaboral> getSituacionesLaborales()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSituacionLaboral situacionLaboral = QSituacionLaboral.situacionLaboral;
        query.from(situacionLaboral).orderBy(situacionLaboral.nombre.asc());

        return query.list(situacionLaboral);
    }

    @Override
    public List<SituacionLaboral> getSituacionLaboralById(Long situacionLaboralId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QSituacionLaboral situacionLaboral = QSituacionLaboral.situacionLaboral;
        query.from(situacionLaboral).where(situacionLaboral.id.eq(situacionLaboralId));

        return query.list(situacionLaboral);
    }

}
