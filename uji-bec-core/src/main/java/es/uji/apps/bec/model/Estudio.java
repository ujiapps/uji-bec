package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.EstudioDAO;

import es.uji.apps.bec.dao.dao.MinimoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC2_EXT_ESTUDIOS database table.
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_EXT_ESTUDIOS")
public class Estudio implements Serializable
{
    private static EstudioDAO estudioDAO;
    private static MinimoDAO minimoDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "CREDITOS_POR_CURSO")
    private Float creditosPorCurso;
    @Column(name = "CREDITOS_TOTALES")
    private Float creditosTotales;
    private Boolean extinguida;
    private String nombre;
    @Column(name = "NUMERO_CURSOS")
    private Long numeroCursos;
    private Boolean oficial;
    private String presencial;
    @Column(name = "SIN_DOCENCIA")
    private Boolean sinDocencia;
    @Column(name = "TITULACION_TECNICA")
    private Boolean titulacionTecnica;
    private String tipo;
    @Column(name = "RAMA")
    private String rama;
    @Column(name = "DOBLE_TITULACION")
    private Boolean dobleTitulacion;
    @Column(name = "HABILITANTE")
    private Boolean habilitante;
    @Column(name = "CICLOS")
    private Long ciclos;
    @Column(name = "CODIGO_CENTRO")
    private String codigoCentro;
    @Column(name = "NOMBRE_CENTRO")
    private String nombreCentro;
    @Column(name = "RUCT")
    private String ruct;
    // bi-directional many-to-one association to Beca
    @OneToMany(mappedBy = "estudio")
    private Set<Beca> becas;
    // bi-directional many-to-one association to PersonaEstudio
    @OneToMany(mappedBy = "estudio")
    private Set<PersonaEstudio> personasEstudios;
    // bi-directional many-to-one association to Minimo
    @OneToMany(mappedBy = "estudio")
    private Set<Minimo> minimos;

    public Estudio()
    {
        this.setExtinguida(false);
        this.setSinDocencia(false);
        this.setTitulacionTecnica(false);
    }

    public static Estudio getEstudioById(Long estudioId)
    {
        if (estudioId != null)
        {
            List<Estudio> listaEstudios = estudioDAO.get(Estudio.class, estudioId);
            if (listaEstudios.size() > 0)
            {
                return listaEstudios.get(0);
            }
        }
        return null;
    }

    @Autowired
    public void setEstudioDAO(EstudioDAO estudioDAO)
    {
        Estudio.estudioDAO = estudioDAO;
    }

    @Autowired
    public void setMinimoDAO(MinimoDAO minimoDAO) {
        Estudio.minimoDAO = minimoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Float getCreditosPorCurso()
    {
        return this.creditosPorCurso;
    }

    public void setCreditosPorCurso(Float creditosPorCurso)
    {
        this.creditosPorCurso = creditosPorCurso;
    }

    public Float getCreditosTotales()
    {
        return this.creditosTotales;
    }

    public void setCreditosTotales(Float creditosTotales)
    {
        this.creditosTotales = creditosTotales;
    }

    public Boolean isExtinguida()
    {
        return this.extinguida;
    }

    public void setExtinguida(Boolean extinguida)
    {
        this.extinguida = extinguida;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean tieneDobleTitulacion()
    {
        return dobleTitulacion;
    }

    public Boolean getDobleTitulacion()
    {
        return dobleTitulacion;
    }

    public void setDobleTitulacion(Boolean dobleTitulacion)
    {
        this.dobleTitulacion = dobleTitulacion;
    }

    public Boolean getHabilitante() {
        return habilitante;
    }

    public void setHabilitante(Boolean habilitante) {
        this.habilitante = habilitante;
    }

    public Boolean isHabilitante()
    {
        return this.habilitante;
    }

    public Long getNumeroCursos()
    {
        return this.numeroCursos;
    }

    public void setNumeroCursos(Long numeroCursos)
    {
        this.numeroCursos = numeroCursos;
    }

    public Boolean isOficial()
    {
        return this.oficial;
    }

    public void setOficial(Boolean oficial)
    {
        this.oficial = oficial;
    }

    public String getPresencial()
    {
        return this.presencial;
    }

    public void setPresencial(String presencial)
    {
        this.presencial = presencial;
    }

    public Boolean isSinDocencia()
    {
        return this.sinDocencia;
    }

    public void setSinDocencia(Boolean sinDocencia)
    {
        this.sinDocencia = sinDocencia;
    }

    public Boolean isTitulacionTecnica()
    {
        return this.titulacionTecnica;
    }

    public void setTitulacionTecnica(Boolean titulacionTecnica)
    {
        this.titulacionTecnica = titulacionTecnica;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getRama()
    {
        return rama;
    }

    public void setRama(String rama)
    {
        this.rama = rama;
    }

    public Long getCiclos()
    {
        return ciclos;
    }

    public void setCiclos(Long ciclos)
    {
        this.ciclos = ciclos;
    }

    public String getCodigoCentro()
    {
        return codigoCentro;
    }

    public void setCodigoCentro(String codigoCentro)
    {
        this.codigoCentro = codigoCentro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getRuct() {
        return ruct;
    }

    public void setRuct(String ruct) {
        this.ruct = ruct;
    }

    public Set<Beca> getBecas()
    {
        return this.becas;
    }

    public void setBecas(Set<Beca> becas)
    {
        this.becas = becas;
    }

    public Set<PersonaEstudio> getPersonasEstudios()
    {
        return this.personasEstudios;
    }

    public void setPersonasEstudios(Set<PersonaEstudio> personasEstudios)
    {
        this.personasEstudios = personasEstudios;
    }

    public Set<Minimo> getMinimos()
    {
        return this.minimos;
    }

    public void setMinimos(Set<Minimo> minimos)
    {
        this.minimos = minimos;
    }

    public Integer getNumeroMaximoBecas()
    {
        Integer maximoBecas = Math.round(this.getNumeroCursos());

        if (this.isTipoGrado() || isTipoPrimerSegundoCiclo())
        {
            maximoBecas = maximoBecas + 1;

            if (this.getCiclos() == 2L || this.isTitulacionTecnica())
            {
                maximoBecas = maximoBecas + 1;
            }
        }

        if (this.getPresencial().equals("S"))
        {
            maximoBecas = maximoBecas + 1;
        }

        return maximoBecas;
    }

    private boolean isTipoPrimerSegundoCiclo()
    {
        return this.getTipo().equals("12C");
    }

    public Boolean isTipoMaster()
    {
        return this.getTipo().equals("M");
    }

    public Boolean isTipoGrado()
    {
        return this.getTipo().equals("G");
    }

    public Minimo getMinimosByEstudioid(Long estudioId, Long cursoAcademicoId) {
        return minimoDAO.getMinimosByEstudioId(estudioId, cursoAcademicoId).get(0);
    }
}