package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.QTexto;
import es.uji.apps.bec.model.Texto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TextoDAODatabaseImpl extends BaseDAODatabaseImpl implements TextoDAO
{
    @Override
    public List<Texto> getTextosPorProcesoYOrganismo(Long procesoId, Long organismoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTexto texto = QTexto.texto;

        query.from(texto)
                .where(texto.proceso.id.eq(procesoId)
                        .and(texto.organismo.id.eq(organismoId)));

        return query.list(texto);
    }

    @Override
    @Transactional
    public Texto updateTexto(Texto texto)
    {
        QTexto qTexto = QTexto.texto;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qTexto);

        updateClause
                .where(qTexto.id.eq(texto.getId()))
                .set(qTexto.organismo, texto.getOrganismo())
                .set(qTexto.proceso, texto.getProceso())
                .set(qTexto.nombre, texto.getNombre())
                .set(qTexto.cabeceraCa, texto.getCabeceraCa())
                .set(qTexto.pieCa, texto.getPieCa())
                .set(qTexto.cabeceraEs, texto.getCabeceraEs())
                .set(qTexto.pieEs, texto.getPieEs())
                .execute();

        return texto;
    }
}

