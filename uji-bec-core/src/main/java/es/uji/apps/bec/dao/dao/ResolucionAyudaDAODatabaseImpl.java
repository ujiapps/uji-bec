package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QResolucionAyuda;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResolucionAyudaDAODatabaseImpl extends BaseDAODatabaseImpl implements ResolucionAyudaDAO
{

    @Override
    public List<es.uji.apps.bec.model.ResolucionAyuda> getAyudasByMultienvioAndBeca(
            Long multiEnvioId, Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QResolucionAyuda ayudas = QResolucionAyuda.resolucionAyuda;

        query.from(ayudas)
                .where(ayudas.multienvioId.eq(multiEnvioId).and(ayudas.becaId.eq(becaId)));

        return query.list(ayudas);
    }

}
