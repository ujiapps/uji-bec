//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2024.10.08 at 02:52:32 PM CEST 
//


package es.uji.apps.bec.webservices.ministerio.solicitudes;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EspecialesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EspecialesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}EspeCodFase"/>
 *         &lt;element ref="{}EspeEstudiosUltimoCurso" minOccurs="0"/>
 *         &lt;element ref="{}EspeIndBecAnt" minOccurs="0"/>
 *         &lt;element ref="{}EspeCodProf" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EspecialesType", propOrder = {
    "espeCodFase",
    "espeEstudiosUltimoCurso",
    "espeIndBecAnt",
    "espeCodProf"
})
@Entity(name = "es.uji.apps.bec.webservices.ministerio.solicitudes.EspecialesType")
@Table(name = "ESPECIALESTYPE", schema = "UJI_BECAS_WS_SOLICITUDES")
@Inheritance(strategy = InheritanceType.JOINED)
public class EspecialesType
    implements Equals, HashCode
{

    @XmlElement(name = "EspeCodFase", required = true)
    protected String espeCodFase;
    @XmlElement(name = "EspeEstudiosUltimoCurso")
    protected String espeEstudiosUltimoCurso;
    @XmlElement(name = "EspeIndBecAnt")
    protected String espeIndBecAnt;
    @XmlElement(name = "EspeCodProf")
    protected String espeCodProf;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the espeCodFase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ESPECODFASE", length = 1)
    public String getEspeCodFase() {
        return espeCodFase;
    }

    /**
     * Sets the value of the espeCodFase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspeCodFase(String value) {
        this.espeCodFase = value;
    }

    /**
     * Gets the value of the espeEstudiosUltimoCurso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ESPEESTUDIOSULTIMOCURSO", length = 255)
    public String getEspeEstudiosUltimoCurso() {
        return espeEstudiosUltimoCurso;
    }

    /**
     * Sets the value of the espeEstudiosUltimoCurso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspeEstudiosUltimoCurso(String value) {
        this.espeEstudiosUltimoCurso = value;
    }

    /**
     * Gets the value of the espeIndBecAnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ESPEINDBECANT", length = 1)
    public String getEspeIndBecAnt() {
        return espeIndBecAnt;
    }

    /**
     * Sets the value of the espeIndBecAnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspeIndBecAnt(String value) {
        this.espeIndBecAnt = value;
    }

    /**
     * Gets the value of the espeCodProf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ESPECODPROF", length = 2)
    public String getEspeCodProf() {
        return espeCodProf;
    }

    /**
     * Sets the value of the espeCodProf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspeCodProf(String value) {
        this.espeCodProf = value;
    }

    /**
     * Gets the value of the hjid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EspecialesType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EspecialesType that = ((EspecialesType) object);
        {
            String lhsEspeCodFase;
            lhsEspeCodFase = this.getEspeCodFase();
            String rhsEspeCodFase;
            rhsEspeCodFase = that.getEspeCodFase();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "espeCodFase", lhsEspeCodFase), LocatorUtils.property(thatLocator, "espeCodFase", rhsEspeCodFase), lhsEspeCodFase, rhsEspeCodFase)) {
                return false;
            }
        }
        {
            String lhsEspeEstudiosUltimoCurso;
            lhsEspeEstudiosUltimoCurso = this.getEspeEstudiosUltimoCurso();
            String rhsEspeEstudiosUltimoCurso;
            rhsEspeEstudiosUltimoCurso = that.getEspeEstudiosUltimoCurso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "espeEstudiosUltimoCurso", lhsEspeEstudiosUltimoCurso), LocatorUtils.property(thatLocator, "espeEstudiosUltimoCurso", rhsEspeEstudiosUltimoCurso), lhsEspeEstudiosUltimoCurso, rhsEspeEstudiosUltimoCurso)) {
                return false;
            }
        }
        {
            String lhsEspeIndBecAnt;
            lhsEspeIndBecAnt = this.getEspeIndBecAnt();
            String rhsEspeIndBecAnt;
            rhsEspeIndBecAnt = that.getEspeIndBecAnt();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "espeIndBecAnt", lhsEspeIndBecAnt), LocatorUtils.property(thatLocator, "espeIndBecAnt", rhsEspeIndBecAnt), lhsEspeIndBecAnt, rhsEspeIndBecAnt)) {
                return false;
            }
        }
        {
            String lhsEspeCodProf;
            lhsEspeCodProf = this.getEspeCodProf();
            String rhsEspeCodProf;
            rhsEspeCodProf = that.getEspeCodProf();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "espeCodProf", lhsEspeCodProf), LocatorUtils.property(thatLocator, "espeCodProf", rhsEspeCodProf), lhsEspeCodProf, rhsEspeCodProf)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEspeCodFase;
            theEspeCodFase = this.getEspeCodFase();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "espeCodFase", theEspeCodFase), currentHashCode, theEspeCodFase);
        }
        {
            String theEspeEstudiosUltimoCurso;
            theEspeEstudiosUltimoCurso = this.getEspeEstudiosUltimoCurso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "espeEstudiosUltimoCurso", theEspeEstudiosUltimoCurso), currentHashCode, theEspeEstudiosUltimoCurso);
        }
        {
            String theEspeIndBecAnt;
            theEspeIndBecAnt = this.getEspeIndBecAnt();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "espeIndBecAnt", theEspeIndBecAnt), currentHashCode, theEspeIndBecAnt);
        }
        {
            String theEspeCodProf;
            theEspeCodProf = this.getEspeCodProf();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "espeCodProf", theEspeCodProf), currentHashCode, theEspeCodProf);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
