package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.bec.dao.dao.RegistroErroresEnvioDAO;

/**
 * The persistent class for the BC2_REGISTRO_ERRORES_ENVIO database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "BC2_REGISTRO_ERRORES_ENVIO")
public class RegistroErroresEnvio implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String error;

    // bi-directional many-to-one association to Bc2Beca
    @ManyToOne
    @JoinColumn(name = "BECA_ID")
    private Beca beca;

    // bi-directional many-to-one association to Bc2Tanda
    @ManyToOne
    @JoinColumn(name = "TANDA_ID")
    private Tanda tanda;

    private static RegistroErroresEnvioDAO registroErroresenvioDAO;

    @Autowired
    public void setRegistroErroresEnvio(RegistroErroresEnvioDAO registroErroresEnvioDAO)
    {
        RegistroErroresEnvio.registroErroresenvioDAO = registroErroresEnvioDAO;
    }

    public RegistroErroresEnvio()
    {

    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getError()
    {
        return this.error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public Beca getBeca()
    {
        return this.beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    public Tanda getTanda()
    {
        return this.tanda;
    }

    public void setTanda(Tanda tanda)
    {
        this.tanda = tanda;
    }

    public static List<RegistroErroresEnvio> getErroresEnvioFiltrado(Map<String, String> filtros)
    {
        return registroErroresenvioDAO.getErroresEnvio(filtros);
    }
}