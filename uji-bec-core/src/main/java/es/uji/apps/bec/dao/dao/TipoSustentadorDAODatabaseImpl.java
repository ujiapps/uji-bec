package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoSustentador;
import es.uji.apps.bec.model.TipoSustentador;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoSustentadorDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoSustentadorDAO
{
    private QTipoSustentador tipoSustentador = QTipoSustentador.tipoSustentador;

    @Override
    public List<TipoSustentador> getTiposSustentadores()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoSustentador).orderBy(tipoSustentador.orden.asc());
        return query.list(tipoSustentador);
    }

    @Override
    public List<TipoSustentador> getTipoSustentadorById(Long tipoSustentadorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoSustentador).where(tipoSustentador.id.eq(tipoSustentadorId));
        return query.list(tipoSustentador);
    }
}
