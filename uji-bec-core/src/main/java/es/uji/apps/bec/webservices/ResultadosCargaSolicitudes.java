package es.uji.apps.bec.webservices;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.HistoricoBeca;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.webservices.ministerio.errores.EnvioType;
import es.uji.apps.bec.webservices.ministerio.errores.ErrorType;
import es.uji.apps.bec.webservices.ministerio.errores.MultienvioType;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.Map.Entry;

@Repository
public class ResultadosCargaSolicitudes extends CoreBaseService {
    private static final Integer SECUENCIA = 1;
    private static Logger log = Logger.getLogger(DatosSolicitudes.class);

    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private WebserviceMinisterio service;

    public ResultadosCargaSolicitudes() {
    }

    public String marcaBecasIncorrectas(MultienvioType resultados) {
        Map<String, Object[]> becas = obtenerListaDeBecasDistintasDesdeErrores(resultados);

        if (becas.size() == 0) {
            return "ENHORABUENA!! No hay becas incorrectas";
        }

        cambiaEstadoBecasConErrores(becas);
        registraErroresEnBaseDatos(resultados);

        return "Se han marcado como INCORRECTAS " + becas.size() + " becas";
    }

    public MultienvioType obtenerResultados(Long tandaId)
            throws IOException, GeneralBECException, JAXBException, SOAPException {

        Tanda tanda = Tanda.getTandaById(tandaId);
        int cursoAcademico = tanda.getCursoAcademico().getId().intValue();
        String acronimo = tanda.getConvocatoria().getAcronimo();
        String lote = String.format("%05d", tanda.getTandaId());

        MultienvioType resultadosLote = service.obtenerResultadoCargarXmlSolicitudes(cursoAcademico, acronimo, SECUENCIA, lote);

        if (envioNoDisponible(resultadosLote)) {
            throw new GeneralBECException("Resultados del proceso del lote todavía no disponibles");
        }

        return resultadosLote;
    }

    public byte[] obtenerListadoErroresPDF(String tandaId)
            throws MalformedURLException, GeneralBECException, RemoteException, ServiceException, SOAPException {

        Tanda tanda = Tanda.getTandaById(Long.parseLong(tandaId));

        Integer curso = tanda.getCursoAcademico().getId().intValue();
        String acronimo = tanda.getConvocatoria().getAcronimo();
        String lote = String.format("%05d", tanda.getTandaId());

        return service.obtenerListadoErroresCargarXml(curso, acronimo, SECUENCIA, lote);
    }

    private boolean envioNoDisponible(MultienvioType erroresSolicitudes) {
        return erroresSolicitudes.getEnvio().getCabeceraEnvio().getCaenCodSituacEnvio() == null;
    }

    @Transactional
    private void registraErroresEnBaseDatos(MultienvioType erroresSolicitudes) {
        becaDAO.insert(erroresSolicitudes);
    }

    private Map<String, Object[]> obtenerListaDeBecasDistintasDesdeErrores(
            MultienvioType erroresSolicitudes) {
        Map<String, Object[]> becas = new HashMap<String, Object[]>();

        EnvioType envio = erroresSolicitudes.getEnvio();

        for (ErrorType error : envio.getError()) {
            Integer curso = Integer.parseInt(error.getErroIdCurso());
            String convocatoria = error.getErroCodConv();
            String nif = error.getErroNif();
            String tanda = error.getErroLote();

            Object[] valores = new Object[]{curso, convocatoria, nif, tanda};
            becas.put(StringUtils.join(valores, "#"), valores);
        }

        return becas;
    }

    private void cambiaEstadoBecasConErrores(Map<String, Object[]> becas) {
        for (Entry<String, Object[]> becaOrdenada : becas.entrySet()) {
            Integer curso = (Integer) becaOrdenada.getValue()[0];
            String convocatoria = (String) becaOrdenada.getValue()[1];
            String nif = (String) becaOrdenada.getValue()[2];
            String tanda = (String) becaOrdenada.getValue()[3];

            Beca beca = becaDAO.getBecaByCursoConvocatoriaTandaIdentificacion(curso, convocatoria, new Integer(tanda), nif);

            if (beca != null) {
                Beca becaDataBase = SerializationUtils.clone(beca);

                beca.setEstado(Estado.getEstadoById(CodigoEstado.INCORRECTA.getId()));
                becaDAO.updateEstado(beca);
                String usuarioConectado = AccessManager.getConnectedUser(request).getName();
                HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
            }
        }
    }

    public void marcarBecasCorrectasComoEnviadas(Long tandaId) {
        List<Beca> listaBecas = Beca.getBecasByTandaId(tandaId);
        if (listaBecas.size() > 0) {
            for (Iterator<Beca> iterator = listaBecas.iterator(); iterator.hasNext(); ) {
                Beca beca = (Beca) iterator.next();
                Beca becaDataBase = SerializationUtils.clone(beca);

                if (laBecaNoEstaEnEstadoEnviado(beca)) {
                    beca.setEstado(Estado.getEstadoById(CodigoEstado.ENVIADA.getId()));
                    beca.updateEstado();
                    String usuarioConectado = AccessManager.getConnectedUser(request).getName();
                    HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
                }
            }
        }
    }

    private boolean laBecaNoEstaEnEstadoEnviado(Beca beca) {
        return beca.getEstado().getId() != CodigoEstado.ENVIADA.getId();
    }

    public void setFechaEnvioEnTanda(Long tandaId) {
        Tanda tanda = Tanda.getTandaById(tandaId);
        if (tanda.getFecha() == null) {
            Date ahora = new Date();
            tanda.setFecha(ahora);
            tanda.updateTanda();
        }
    }
}