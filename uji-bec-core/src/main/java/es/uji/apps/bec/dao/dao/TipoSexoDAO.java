package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.TipoSexo;
import es.uji.commons.db.BaseDAO;

public interface TipoSexoDAO extends BaseDAO
{
    List<TipoSexo> getTiposSexos();

    List<TipoSexo> getTipoSexoById(Long tipoSexoId);
}