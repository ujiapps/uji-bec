package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorSituacionLaboral extends Mapeador<Long, Object>
{
    public MapeadorSituacionLaboral()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "E");
        add(new Long(1L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "A");
        add(new Long(2L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "D");
        add(new Long(3L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "I");
        add(new Long(4L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "J");
        add(new Long(5L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "M");
        add(new Long(6L), valores);

        valores.put(CodigoOrganismo.MINISTERIO, "O");
        add(new Long(7L), valores);
    }
}
