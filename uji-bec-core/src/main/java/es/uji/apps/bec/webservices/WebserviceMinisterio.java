package es.uji.apps.bec.webservices;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.WsDatosEnviadosParaBajarBecaErroneosException;
import es.uji.apps.bec.exceptions.WsErrorAlPrepararMultienvioException;
import es.uji.apps.bec.exceptions.WsFaltaCodigoDeArchivoTemporalException;
import es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos;
import es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType;
import es.uji.apps.bec.webservices.ministerio.solicitudes.SolicitudType;
import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


@Component
public class WebserviceMinisterio {

    /*
     * https://servicios.educacion.gob.es/becaswebservices/ws/solicitudes?wsdl
     * https://servicios.educacion.gob.es:443/becaswebservices/ws/solicitudes?xsd=1
     */

    private static Logger log = Logger.getLogger(WebserviceMinisterio.class);

    private static final Integer NUMERO_SECUENCIA = 1;
    private static final String UNIDAD_DE_TRAMITE_TIPO = "1";
    private static final String UNIDAD_DE_TRAMITE_CODIGO = "400";

    private static final String NAMESPACE_URI = "http://services.gestion.becas.educ/";
    private static final String PREFIX = "ser";
    private static final String URL_WEBSERVICE = "https://servicios.educacion.gob.es/becaswebservices/ws/solicitudes";

    private String urlWebService;
    private String usuarioWebService;
    private String passwordWebService;
    private String activoWebService;

    private Marshaller marshallerMultienvio;
    private Unmarshaller unmarshallerMultienvio;

    private Marshaller marshallerErrores;
    private Unmarshaller unmarshallerErrores;

    private Marshaller marshallerSolicitudes;
    private Unmarshaller unmarshallerSolicitudes;

    private Marshaller marshallerDenegaciones;
    private Unmarshaller unmarshallerDenegaciones;

    private Marshaller marshallerAcademicos;
    private Unmarshaller unmarshallerAcademicos;

    @Autowired
    public WebserviceMinisterio(@Value("${uji.ws.url}") String urlWebService,
                                @Value("${uji.ws.usuario}") String usuarioWebService,
                                @Value("${uji.ws.password}") String passwordWebService,
                                @Value("${uji.ws.activo}") String activoWebService) throws JAXBException {

        this.urlWebService = urlWebService;
        this.usuarioWebService = usuarioWebService;
        this.passwordWebService = passwordWebService;
        this.activoWebService = activoWebService;

        JAXBContext context = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.multienvio");
        marshallerMultienvio = context.createMarshaller();
        unmarshallerMultienvio = context.createUnmarshaller();

        JAXBContext contextErrores = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.errores");
        marshallerErrores = contextErrores.createMarshaller();
        unmarshallerErrores = contextErrores.createUnmarshaller();

        JAXBContext contextSolicitudes = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.solicitudes");
        marshallerSolicitudes = contextSolicitudes.createMarshaller();
        unmarshallerSolicitudes = contextSolicitudes.createUnmarshaller();

        JAXBContext contextDenegaciones = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.denegaciones");
        marshallerDenegaciones = contextDenegaciones.createMarshaller();
        unmarshallerDenegaciones = contextDenegaciones.createUnmarshaller();

        JAXBContext contextAcademicos = JAXBContext.newInstance("es.uji.apps.bec.webservices.ministerio.academicos");
        marshallerAcademicos = contextAcademicos.createMarshaller();
        unmarshallerAcademicos = contextAcademicos.createUnmarshaller();
    }

    public WebserviceMinisterio() {

    }

    public String obtenerXmlPendientesString(Long curso, String convocatoria) throws SOAPException, MalformedURLException {
        Integer rangoPendientes = 0;

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("rango", rangoPendientes.toString());

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "obtenerXmlPendientes");
        return soapResponse.getSOAPBody().getTextContent();
    }

    @SuppressWarnings("unchecked")
    public JAXBElement<MultienvioType> obtenerXmlPendientes(Long curso, String convocatoria)
            throws MalformedURLException, JAXBException, SOAPException {

        String result = obtenerXmlPendientesString(curso, convocatoria);

        JAXBElement<es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType> element =
                (JAXBElement<es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType>) unmarshallerMultienvio
                        .unmarshal(new ByteArrayInputStream(result.getBytes()));

        return element;
    }

    // Obtener lote resoluciones
    public MultienvioType getXml(String lote, String convocatoria, String curso)
            throws IOException, WsDatosEnviadosParaBajarBecaErroneosException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso);
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", NUMERO_SECUENCIA.toString());
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("lote", lote);
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getXml");
        String result = soapResponse.getSOAPBody().getTextContent();

        String error = null;
        if (result.indexOf("<Error>") != -1) {
            error = result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>"));
        }

        if (error == null || error.length() == 0) {

            int ini = result.indexOf("<zipBase64>") + "<zipBase64>".length();
            int fin = result.indexOf("</zipBase64>");

            byte[] data = Base64.decode(result.substring(ini, fin));

            ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(data));
            zis.getNextEntry();

            byte[] xmlData = extraeZipMultienvio(zis);

            try {
                @SuppressWarnings("unchecked")
                JAXBElement<es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType> element =
                        (JAXBElement<es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType>) unmarshallerMultienvio
                                .unmarshal(new ByteArrayInputStream(xmlData));
                return element.getValue();
            } catch (JAXBException e) {
                throw new WsDatosEnviadosParaBajarBecaErroneosException();
            }
        }

        return null;
    }

    // Cargar multienvio
    public String cargarXmlSolicitudesBase64(es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType multiEnvio)
            throws JAXBException, IOException, WsErrorAlPrepararMultienvioException, GeneralBECException, SOAPException {

        if (webServiceDeshabilitado()) {
            throw new WsErrorAlPrepararMultienvioException("En desarrollo no se puede realizar envíos");
        }

        es.uji.apps.bec.webservices.ministerio.multienvio.EnvioType envio = multiEnvio.getEnvio().get(0);
        es.uji.apps.bec.webservices.ministerio.multienvio.CabeceraEnvioType cabeceraEnvio = envio.getCabeceraEnvio();

        //creaFicheroMultienvioEnDisco(multiEnvio, lote);

        byte[] envioZip = creaZipMultienvio(multiEnvio);
        String xmlData = Base64.encode(envioZip);

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", cabeceraEnvio.getCaenIdCurso());
        parametros.put("convocatoria", cabeceraEnvio.getCaenCodConv());
        parametros.put("secuencia", cabeceraEnvio.getCaenSecuenc().toString());
        parametros.put("tipoUtTramite", cabeceraEnvio.getCaenTipoUTTramite());
        parametros.put("codUtTramite", cabeceraEnvio.getCaenCodUTTramite());
        parametros.put("lote", cabeceraEnvio.getCaenLote());
        parametros.put("fechaEnvio", cabeceraEnvio.getCaenFechaEnv().toGregorianCalendar().getTime().toString());
        parametros.put("totalSolicitudes", cabeceraEnvio.getCaenTotSoli().toString());
        parametros.put("tipoEnvio", cabeceraEnvio.getCaenTipoEnvio());
        parametros.put("tipoUtGeneracion", cabeceraEnvio.getCaenTipoUTGen());
        parametros.put("codUtGeneracion", cabeceraEnvio.getCaenCodUTGen());
        parametros.put("loteGeneracion", cabeceraEnvio.getCaenLoteGen());
        parametros.put("xmlBase64", xmlData);

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "cargarXmlSolicitudesBase64");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Error>") != -1) {
            throw new GeneralBECException("Error a l'intentar carregar la tanda en el ministeri\n"
                    + result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>")));
        }

        if (result.indexOf("error.bdxml.validar.schema") != -1) {
            throw new GeneralBECException("Error de validació de l'esquema");
        }

        return result;
    }

    // Cargar denegaciones
    public String cargarXmlSolicitudesBase64(es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType multiEnvio)
            throws JAXBException, IOException, WsErrorAlPrepararMultienvioException, GeneralBECException, SOAPException {

        if (webServiceDeshabilitado()) {
            throw new WsErrorAlPrepararMultienvioException("En desarrollo no se puede realizar envíos");
        }

        es.uji.apps.bec.webservices.ministerio.denegaciones.EnvioType envio = multiEnvio.getEnvio().get(0);
        es.uji.apps.bec.webservices.ministerio.denegaciones.CabeceraEnvioType cabeceraEnvio = envio.getCabeceraEnvio();

        //creaFicheroMultienvioEnDisco(multiEnvio, lote);

        byte[] envioZip = creaZipMultienvio(multiEnvio);
        String xmlData = Base64.encode(envioZip);

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", cabeceraEnvio.getCaenIdCurso());
        parametros.put("convocatoria", cabeceraEnvio.getCaenCodConv());
        parametros.put("secuencia", cabeceraEnvio.getCaenSecuenc().toString());
        parametros.put("tipoUtTramite", cabeceraEnvio.getCaenTipoUTTramite());
        parametros.put("codUtTramite", cabeceraEnvio.getCaenCodUTTramite());
        parametros.put("lote", cabeceraEnvio.getCaenLote());
        parametros.put("fechaEnvio", cabeceraEnvio.getCaenFechaEnv().toGregorianCalendar().getTime().toString());
        parametros.put("totalSolicitudes", cabeceraEnvio.getCaenTotSoli().toString());
        parametros.put("tipoEnvio", cabeceraEnvio.getCaenTipoEnvio());
        parametros.put("tipoUtGeneracion", cabeceraEnvio.getCaenTipoUTGen());
        parametros.put("codUtGeneracion", cabeceraEnvio.getCaenCodUTGen());
        parametros.put("loteGeneracion", cabeceraEnvio.getCaenLoteGen());
        parametros.put("xmlBase64", xmlData);

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "cargarXmlSolicitudesBase64");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Error>") != -1) {
            throw new GeneralBECException("Error a l'intentar carregar la tanda en el ministeri\n"
                    + result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>")));
        }

        if (result.indexOf("error.bdxml.validar.schema") != -1) {
            throw new GeneralBECException("Error de validació de l'esquema");
        }

        return result;
    }

    public String getXmlSolicitudesCiudadanoRT(Integer curso, String convocatoria, Boolean descargadas)
            throws MalformedURLException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", NUMERO_SECUENCIA.toString());
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("descargadas", descargadas.toString());
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getXmlSolicitudesCiudadanoRT");
        return soapResponse.getSOAPBody().getTextContent();
    }

    public String cargarXmlDatosAcademicosUniv(
            es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos datosAcademicos)
            throws JAXBException, IOException, WsErrorAlPrepararMultienvioException,
            GeneralBECException, SOAPException {

        if (webServiceDeshabilitado()) {
            throw new WsErrorAlPrepararMultienvioException("En desarrollo no se puede realizar envíos");
        }

        es.uji.apps.bec.webservices.ministerio.academicos.Cabecera cabeceraEnvio = datosAcademicos.getCabecera();

        Integer curso = cabeceraEnvio.getCursoAcademico();
        String tipoUt = cabeceraEnvio.getTipoUt();
        String codUt = cabeceraEnvio.getCodUt();

        //creaFicheroMultienvioEnDisco(datosAcademicos, lote);

        byte[] envioZip = creaZipMultienvio(datosAcademicos);
        String xmlData = Base64.encode(envioZip);

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("tipoUt", tipoUt);
        parametros.put("codUt", codUt);
        parametros.put("xmlBase64", xmlData);
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "cargarXmlDatosAcademicosUniv");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Error>") != -1) {
            throw new GeneralBECException("Error a l'intentar carregar les dades acadèmics en el ministeri\n"
                    + result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>")));
        }

        if (result.indexOf("error.bdxml.validar.schema") != -1) {
            throw new GeneralBECException("Error de validació de l'esquema");
        }

        return result;
    }

    public byte[] getXmlSolicitudCiudadano(Integer curso, String convocatoria, Integer secuencia, String nif, Long archivoTemporal)
            throws MalformedURLException, WsDatosEnviadosParaBajarBecaErroneosException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", secuencia.toString());
        parametros.put("nif", nif);
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getXmlSolicitudCiudadano");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Solicitud>") == -1) {
            throw new WsDatosEnviadosParaBajarBecaErroneosException();
        }

        return result.substring(result.indexOf("<Solicitud>")).getBytes();
    }

    @SuppressWarnings("unchecked")
    public SolicitudType getXmlSolicitudCiudadanoSolicitudType(Integer curso, String convocatoria, Integer secuencia, String nif, Long archivoTemporal)
            throws IOException, WsDatosEnviadosParaBajarBecaErroneosException, SOAPException {

        byte[] solicitudData = getXmlSolicitudCiudadano(curso, convocatoria, secuencia, nif, archivoTemporal);

        try {
            JAXBElement<SolicitudType> solicitud = (JAXBElement<SolicitudType>) unmarshallerSolicitudes.unmarshal(new ByteArrayInputStream(solicitudData));
            return solicitud.getValue();
        } catch (JAXBException e) {
            throw new WsDatosEnviadosParaBajarBecaErroneosException();
        }
    }

    public es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos getResultadoCargaDatosAcademicosUniv(Long lote)
            throws JAXBException, IOException, GeneralBECException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("lote", lote.toString());
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getResultadoCargaDatosAcademicosUniv");
        String result = soapResponse.getSOAPBody().getTextContent();

        String error = null;
        if (result.indexOf("<Error>") != -1) {
            error = result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>"));
        }

        if (error != null && error.length() > 0) {
            throw new GeneralBECException("Error al processar la tanda\n" + error);
        }

        return ((JAXBElement<es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos>) unmarshallerErrores
                .unmarshal(new ByteArrayInputStream(result.getBytes()))).getValue();
    }

    @SuppressWarnings("unchecked")
    public es.uji.apps.bec.webservices.ministerio.errores.MultienvioType obtenerResultadoCargarXmlSolicitudes(
            Integer curso, String convocatoria, Integer secuencia, String lote)
            throws JAXBException, IOException, GeneralBECException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", secuencia.toString());
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("lote", lote);
        parametros.put("tipoUtGeneracion", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtGeneracion", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("loteGeneracion", lote);

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "obtenerResultadoCargarXmlSolicitudes");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Error>") != -1) {
            throw new GeneralBECException("Error al processar la tanda\n"
                    + result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>")));
        }

        if (result.indexOf("error.ws.cargarxml.respuesta.etiqueta.no.encontrada") != -1) {
            throw new GeneralBECException("Error al processar la tanda. Etiqueta no encontrada\n");
        }

        return ((JAXBElement<es.uji.apps.bec.webservices.ministerio.errores.MultienvioType>) unmarshallerErrores
                .unmarshal(new ByteArrayInputStream(result.getBytes()))).getValue();
    }

    public String getXmlSolicitud(Integer curso, String convocatoria, String nif)
            throws MalformedURLException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("nif", nif);

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getXmlSolicitud");
        return soapResponse.getSOAPBody().getTextContent();
    }

    public es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType getXmlSolicitudAsMultienvio(Integer curso, String convocatoria, String nif)
            throws MalformedURLException, JAXBException, SOAPException {

        String result = getXmlSolicitud(curso, convocatoria, nif);

        return (es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType) unmarshallerMultienvio
                .unmarshal(new ByteArrayInputStream(result.getBytes()));
    }

    public es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType getXml(Integer curso, String convocatoria, Integer secuencia, String lote)
            throws MalformedURLException, SOAPException, JAXBException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", secuencia.toString());
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("lote", lote);
        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "getXml");
        String result = soapResponse.getSOAPBody().getTextContent();

        return (es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType) unmarshallerMultienvio
                .unmarshal(new ByteArrayInputStream(result.getBytes()));
    }

    public byte[] obtenerListadoErroresCargarXml(Integer curso, String convocatoria, Integer secuencia, String lote)
            throws MalformedURLException, ServiceException, GeneralBECException, RemoteException, SOAPException {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("curso", curso.toString());
        parametros.put("convocatoria", convocatoria);
        parametros.put("secuencia", NUMERO_SECUENCIA.toString());
        parametros.put("tipoUtTramite", UNIDAD_DE_TRAMITE_TIPO);
        parametros.put("codUtTramite", UNIDAD_DE_TRAMITE_CODIGO);
        parametros.put("lote", lote);

        SOAPMessage soapResponse = buildWebServiceSoapCall(parametros, "obtenerListadoErroresCargarXml");
        String result = soapResponse.getSOAPBody().getTextContent();

        if (result.indexOf("<Error>") != -1) {
            throw new GeneralBECException(result.substring(result.indexOf("<Error>") + 7, result.indexOf("</Error>")));
        }

        if (result.indexOf("<pdf>") != -1) {
            int indexIniFile = result.indexOf("<pdf>") + "<pdf>".length();
            int indexFinFile = result.indexOf("</pdf>");

            String fileContent = result.substring(indexIniFile, indexFinFile);

            return Base64.decode(fileContent);
        }

        throw new GeneralBECException("Error en el resultado de la operacion");
    }

    private SOAPMessage buildWebServiceSoapCall(Map<String, String> parametros, String metodo)
            throws SOAPException, MalformedURLException {

        SOAPMessage soapMessage = createSOAPRequest(parametros, metodo);
        return sendSOAPRequest(soapMessage, URL_WEBSERVICE);
    }

    private SOAPMessage createSOAPRequest(Map<String, String> parametros, String metodo)
            throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(PREFIX, NAMESPACE_URI);

        SOAPHeader header = envelope.getHeader();
        SOAPElement security = header.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
        usernameToken.addChildElement("Username", "wsse").addTextNode(usuarioWebService);
        usernameToken.addChildElement("Password", "wsse")
                .addAttribute(envelope.createName("Type"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText")
                .addTextNode(passwordWebService);

        SOAPBody body = envelope.getBody();
        SOAPElement bodyElement = body.addChildElement(metodo, PREFIX);

        for (Map.Entry<String, String> entry : parametros.entrySet()) {
            bodyElement.addChildElement(entry.getKey()).addTextNode(entry.getValue());
        }

        soapMessage.saveChanges();
        return soapMessage;
    }

    private SOAPMessage sendSOAPRequest(SOAPMessage soapMessage, String url)
            throws SOAPException, MalformedURLException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        URL endpoint = new URL(url);
        SOAPMessage soapResponse = soapConnection.call(soapMessage, endpoint);

        soapConnection.close();
        return soapResponse;
    }


    private boolean webServiceDeshabilitado() {
        return !("S".equals(this.activoWebService));
    }

    private byte[] extraeZipMultienvio(InputStream in) throws IOException {
        ByteArrayOutputStream baos = null;
        byte[] buffer = new byte[2048];

        int length = 0;

        baos = new java.io.ByteArrayOutputStream();

        while ((length = in.read(buffer)) >= 0) {
            baos.write(buffer, 0, length);
        }

        return baos.toByteArray();
    }

    private void creaFicheroMultienvioEnDisco(
            es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType multiEnvio,
            String lote)
            throws JAXBException, IOException {
        byte[] solicitudes = multiEnvioToByteArray(multiEnvio);
        FileOutputStream fos = new FileOutputStream("/tmp/" + lote + "-multienvio.xml");
        fos.write(solicitudes);
        fos.flush();
        fos.close();
    }

    private void creaFicheroMultienvioEnDisco(
            es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType multiEnvio,
            String lote)
            throws JAXBException, IOException {
        byte[] solicitudes = multiEnvioToByteArray(multiEnvio);
        FileOutputStream fos = new FileOutputStream("/tmp/" + lote + "-multienvio.xml");
        fos.write(solicitudes);
        fos.flush();
        fos.close();
    }

    private void creaFicheroMultienvioEnDisco(
            es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos datosAcademicos,
            Integer lote)
            throws JAXBException, IOException {
        byte[] solicitudes = datosAcademicosToByteArray(datosAcademicos);
        FileOutputStream fos = new FileOutputStream("/tmp/" + lote + "-academicos.xml");
        fos.write(solicitudes);
        fos.flush();
        fos.close();
    }

    private byte[] creaZipMultienvio(es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType multiEnvio)
            throws JAXBException, IOException {
        byte[] solicitudes = multiEnvioToByteArray(multiEnvio);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(baos);
        out.putNextEntry(new ZipEntry("envio.xml"));
        out.write(solicitudes);
        out.closeEntry();
        out.close();

        return baos.toByteArray();
    }

    private byte[] creaZipMultienvio(es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType multiEnvio)
            throws JAXBException, IOException {
        byte[] solicitudes = multiEnvioToByteArray(multiEnvio);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(baos);
        out.putNextEntry(new ZipEntry("envio.xml"));
        out.write(solicitudes);
        out.closeEntry();
        out.close();

        return baos.toByteArray();
    }

    private byte[] creaZipMultienvio(es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos datosAcademicos)
            throws JAXBException, IOException {
        byte[] solicitudes = datosAcademicosToByteArray(datosAcademicos);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(baos);
        out.putNextEntry(new ZipEntry("envio.xml"));
        out.write(solicitudes);
        out.closeEntry();
        out.close();

        return baos.toByteArray();
    }

    private byte[] multiEnvioToByteArray(es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType multiEnvio)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        es.uji.apps.bec.webservices.ministerio.multienvio.ObjectFactory objectFactory =
                new es.uji.apps.bec.webservices.ministerio.multienvio.ObjectFactory();
        marshallerMultienvio.marshal(objectFactory.createMultienvio(multiEnvio), baos);

        return baos.toByteArray();
    }

    public String multiEnvioToString(es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType multiEnvio)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        es.uji.apps.bec.webservices.ministerio.multienvio.ObjectFactory objectFactory =
                new es.uji.apps.bec.webservices.ministerio.multienvio.ObjectFactory();
        marshallerMultienvio.marshal(objectFactory.createMultienvio(multiEnvio), baos);

        return baos.toString();
    }

    private byte[] multiEnvioToByteArray(es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType multiEnvio)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        es.uji.apps.bec.webservices.ministerio.denegaciones.ObjectFactory objectFactory =
                new es.uji.apps.bec.webservices.ministerio.denegaciones.ObjectFactory();
        marshallerDenegaciones.marshal(objectFactory.createMultienvio(multiEnvio), baos);

        return baos.toByteArray();
    }

    public String multiEnvioToString(es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType multiEnvio)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        es.uji.apps.bec.webservices.ministerio.denegaciones.ObjectFactory objectFactory =
                new es.uji.apps.bec.webservices.ministerio.denegaciones.ObjectFactory();
        marshallerMultienvio.marshal(objectFactory.createMultienvio(multiEnvio), baos);

        return baos.toString();
    }

    private byte[] datosAcademicosToByteArray(es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos datosAcademicos)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBElement<DatosAcademicos> datosAcademicosJAXBElement = createDatosAcademicos(datosAcademicos);
        marshallerAcademicos.marshal(datosAcademicosJAXBElement, baos);

        return baos.toByteArray();
    }

    public String datosAcademicosToString(es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos datosAcademicos)
            throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBElement<DatosAcademicos> datosAcademicosJAXBElement = createDatosAcademicos(datosAcademicos);
        marshallerAcademicos.marshal(datosAcademicosJAXBElement, baos);
        return baos.toString();
    }

    public JAXBElement<DatosAcademicos> createDatosAcademicos(DatosAcademicos value) {
        return new JAXBElement<>(new QName("", "DatosAcademicos"), DatosAcademicos.class, null, value);
    }
}