package es.uji.apps.bec.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class WsErrorAlPrepararMultienvioException extends CoreDataBaseException
{
    public WsErrorAlPrepararMultienvioException()
    {
        super("Error no controlat al preparar el enviament al MEC");
    }

    public WsErrorAlPrepararMultienvioException(String message)
    {
        super(message);
    }
}
