package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.bec.model.QTipoDomicilio;
import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoDomicilioDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoDomicilioDAO
{

    @Override
    public List<TipoDomicilio> getTiposDomicilios()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoDomicilio tipoDomicilio = QTipoDomicilio.tipoDomicilio;
        query.from(tipoDomicilio).orderBy(tipoDomicilio.orden.asc());

        return query.list(tipoDomicilio);
    }

    @Override
    public List<TipoDomicilio> getTipoDomicilioById(Long tipoDomicilioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoDomicilio tipoDomicilio = QTipoDomicilio.tipoDomicilio;
        query.from(tipoDomicilio).where(tipoDomicilio.id.eq(tipoDomicilioId));

        return query.list(tipoDomicilio);
    }

}
