package es.uji.apps.bec.dao.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Domicilio;
import es.uji.apps.bec.model.QDomicilio;
import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DomicilioDAODatabaseImpl extends BaseDAODatabaseImpl implements DomicilioDAO
{
    private static final Long TIPO_DOMICILIO_RESIDENCIA = 20L;
    private static final Long TIPO_DOMICILIO_FAMILIAR = 10L;
    private QDomicilio qDomicilio = QDomicilio.domicilio;

    @Override
    public Domicilio getDomicilioById(Long domicilioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDomicilio).where(qDomicilio.id.eq(domicilioId));
        return query.uniqueResult(qDomicilio);
    }

    @Override
    public List<Domicilio> getDomiciliosByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDomicilio).where(qDomicilio.beca.id.eq(becaId));
        return query.list(qDomicilio);
    }

    @Override
    public Domicilio getDomicilioByBecaAndTipo(Beca beca, TipoDomicilio tipoDomicilio)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDomicilio).where(qDomicilio.beca.id.eq(beca.getId())
                .and(qDomicilio.tipoDomicilio.id.eq(tipoDomicilio.getId())));
        return query.uniqueResult(qDomicilio);
    }

    @Override
    public Domicilio getDomicilioResidencia(Long becaId)
    {
        Beca beca = Beca.getBecaById(becaId);
        TipoDomicilio tipoDomicilioResidencia = TipoDomicilio.getTipoDomicilioById(TIPO_DOMICILIO_RESIDENCIA);
        Domicilio domicilioResidencia = getDomicilioByBecaAndTipo(beca, tipoDomicilioResidencia);
        return domicilioResidencia;
    }

    @Override
    public Domicilio getDomicilioFamiliar(Long becaId)
    {
        Beca beca = Beca.getBecaById(becaId);
        TipoDomicilio tipoDomicilioFamiliar = TipoDomicilio.getTipoDomicilioById(TIPO_DOMICILIO_FAMILIAR);
        Domicilio domicilioFamiliar = getDomicilioByBecaAndTipo(beca, tipoDomicilioFamiliar);
        return domicilioFamiliar;
    }

    @Override
    @Transactional
    public void updateDomicilio(Domicilio domicilio)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qDomicilio);
        updateClause.where(qDomicilio.id.eq(domicilio.getId()))
                .set(qDomicilio.tipoDomicilio, domicilio.getTipoDomicilio())
                .set(qDomicilio.tipoVia, domicilio.getTipoVia())
                .set(qDomicilio.provincia, domicilio.getProvincia())
                .set(qDomicilio.pais, domicilio.getPais())
                .set(qDomicilio.tipoResidencia, domicilio.getTipoResidencia())
                .set(qDomicilio.arrendadorTipoIdentificacion, domicilio.getArrendadorTipoIdentificacion())
                .set(qDomicilio.localidad, domicilio.getLocalidad())
                .set(qDomicilio.localidadNombre, domicilio.getLocalidadNombre())
                .set(qDomicilio.codigoMunicipio, domicilio.getCodigoMunicipio())
                .set(qDomicilio.otraClaseDomicilio, domicilio.getOtraClaseDomicilio())
                .set(qDomicilio.nombreVia, domicilio.getNombreVia())
                .set(qDomicilio.numero, domicilio.getNumero())
                .set(qDomicilio.escalera, domicilio.getEscalera())
                .set(qDomicilio.piso, domicilio.getPiso())
                .set(qDomicilio.puerta, domicilio.getPuerta())
                .set(qDomicilio.codigoPostal, domicilio.getCodigoPostal())
                .set(qDomicilio.arrendadorIdentificacion, domicilio.getArrendadorIdentificacion())
                .set(qDomicilio.arrendadorNombre, domicilio.getArrendadorNombre())
                .set(qDomicilio.importeAlquiler, domicilio.getImporteAlquiler())
                .set(qDomicilio.residenciaEsp, domicilio.isResidenciaEsp())
                .set(qDomicilio.gratuito, domicilio.isGratuito())
                .set(qDomicilio.correspondencia, domicilio.isCorrespondencia())
                .set(qDomicilio.beca, domicilio.getBeca())
                .set(qDomicilio.fechaInicioContrato, domicilio.getFechaInicioContrato())
                .set(qDomicilio.fechaFinContrato, domicilio.getFechaFinContrato())
                .set(qDomicilio.importeFianza, domicilio.getImporteFianza())
                .set(qDomicilio.fechaAdquisicionVivienda, domicilio.getFechaAdquisicionVivienda())
                .set(qDomicilio.referenciaCatastral, domicilio.getReferenciaCatastral())
                .set(qDomicilio.departamento, domicilio.getDepartamento())
                .set(qDomicilio.numeroArrendatarios, domicilio.getNumeroArrendatarios()).execute();
    }

    @Override
    @Transactional
    public void borraDomicilio(Long domicilioId)
    {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qDomicilio);
        delete.where(qDomicilio.id.eq(domicilioId)).execute();
    }
}
