package es.uji.apps.bec.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.dao.dao.RecursoDAO;

/**
 * The persistent class for the BC2_BECAS_RECURSOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "BC2_BECAS_RECURSOS")
@Component
public class Recurso implements Serializable
{
    private static RecursoDAO recursoDAO;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "AYUDAS_ACTUALES")
    private String ayudasActuales;
    @Column(name = "CIRCUNSTANCIAS_TRANSPORTE")
    private String circunstanciasTransporte;
    @Column(name = "CURSO_ACT_ANIOS_BECARIO")
    private Integer cursoActAniosBecario;
    @Column(name = "CURSO_ACT_CAMBIO_ESTUDIOS")
    private Integer cursoActCambioEstudios;
    @Column(name = "CURSO_ACT_CREDITOS")
    private Float cursoActCreditos;
    @Column(name = "CURSO_ACT_CREDITOS_MATR")
    private Float cursoActCreditosMatr;
    @Column(name = "CURSO_ACT_CURSO_COMPLETO")
    private Integer cursoActCursoCompleto;
    @Column(name = "CURSO_ACT_CURSO_POSTERIOR")
    private Integer cursoActCursoPosterior;
    @Column(name = "CURSO_ACT_CURSO_POSTERIOR_TXT")
    private String cursoActCursoPosteriorTxt;
    @Column(name = "CURSO_ACT_PERDIDA_CURSO")
    private Integer cursoActPerdidaCurso;
    @Column(name = "CURSO_ACT_PERDIDA_CURSO_TXT")
    private String cursoActPerdidaCursoTxt;
    @Column(name = "CURSO_ANT_ESTUDIO")
    private String cursoAntEstudio;
    @Column(name = "CURSO_ANT_COMPLETO")
    private Integer cursoAntCompleto;
    @Column(name = "CURSO_ANT_CREDITOS")
    private Float cursoAntCreditos;
    @Column(name = "CURSO_ANT_CREDITOS_MATR")
    private Float cursoAntCreditosMatr;
    @Column(name = "CURSO_ANT_CREDITOS_PEND")
    private Float cursoAntCreditosPend;
    @Column(name = "CURSO_ANT_CREDITOS_PEND_MAX")
    private Float cursoAntCreditosPendMax;
    @Column(name = "CURSO_ANT_ESTUDIO_RAMA")
    private String cursoAntEstudioRama;
    @Column(name = "CURSO_ANT_NOTA_MEDIA")
    private Float cursoAntNotaMedia;
    @Column(name = "CURSO_ANT_NOTA_MEDIA_SIN")
    private Float cursoAntNotaMediaSin;
    private Float deducciones;
    @Column(name = "DENEGACIONES_ACTUALES")
    private String denegacionesActuales;
    private String distancia;
    private Integer estimacion;
    @Column(name = "EXISTE_CENTRO_MAS_CERCANO")
    private Integer existeCentroMasCercano;
    @Column(name = "INGRESOS_FAMILIARES")
    private Float ingresosFamiliares;
    @Column(name = "OTRAS_BECAS")
    private Integer otrasBecas;
    @Column(name = "OTRAS_CIRCUNSTANCIAS")
    private String otrasCircunstancias;
    @Column(name = "PORCENTAJE_INCREMENTO_UMBRAL")
    private Float porcentajeIncrementoUmbral;
    @Column(name = "RENTA_FAMILIAR_DISPONIBLE")
    private Float rentaFamiliarDisponible;
    @Column(name = "RENTA_FAMILIAR_PER_CAPITA")
    private Float rentaFamiliarPerCapita;
    @Column(name = "SUPERA_UMBRALES")
    private Integer superaUmbrales;
    @Column(name = "SUPERA_UMBRALES_CUANTIA")
    private Float superaUmbralesCuantia;
    @Column(name = "SUPERA_UMBRALES_TEXTO")
    private String superaUmbralesTexto;
    @Column(name = "TEXTO_AYUDAS_RECLAMADAS")
    private String textoAyudasReclamadas;
    @Column(name = "TEXTO_JUSTIFICACION")
    private String textoJustificacion;
    @Column(name = "TEXTO_PROPUESTA")
    private String textoPropuesta;
    @Column(name = "MIEMBROS_COMPUTABLES")
    private Integer miembrosComputables;
    @Column(name = "CAUSAS_DESESTIMACION")
    private String causasDesestimacion;

    @OneToMany(mappedBy = "recurso", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RecursoDetalle> recursoDetalles;

    // bi-directional one-to-one association to Beca
    @OneToOne
    @JoinColumn(name = "BECA_ID")
    private Beca beca;


    public Recurso()
    {
    }

    public static List<Recurso> getRecursosByTandaId(Long tandaId)
    {
        return recursoDAO.getRecursosByTandaId(tandaId);
    }

    public static Recurso getRecursoByBecaId(Long becaId)
    {
        return recursoDAO.getRecursoByBecaId(becaId);
    }

    @Transactional
    public static void delete(Long recursoId)
    {
        recursoDAO.delete(Recurso.class, recursoId);
    }

    @Autowired
    public void setRecursoDAO(RecursoDAO recursoDAO)
    {
        Recurso.recursoDAO = recursoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCircunstanciasTransporte()
    {
        return this.circunstanciasTransporte;
    }

    public void setCircunstanciasTransporte(String circunstanciasTransporte)
    {
        this.circunstanciasTransporte = circunstanciasTransporte;
    }

    public Integer getCursoActAniosBecario()
    {
        return this.cursoActAniosBecario;
    }

    public void setCursoActAniosBecario(Integer cursoActAniosBecario)
    {
        this.cursoActAniosBecario = cursoActAniosBecario;
    }

    public Float getCursoActCreditos()
    {
        return this.cursoActCreditos;
    }

    public void setCursoActCreditos(Float cursoActCreditos)
    {
        this.cursoActCreditos = cursoActCreditos;
    }

    public Float getCursoActCreditosMatr()
    {
        return this.cursoActCreditosMatr;
    }

    public void setCursoActCreditosMatr(Float cursoActCreditosMatr)
    {
        this.cursoActCreditosMatr = cursoActCreditosMatr;
    }

    public Integer getCursoActCursoCompleto()
    {
        return this.cursoActCursoCompleto;
    }

    public void setCursoActCursoCompleto(Integer cursoActCursoCompleto)
    {
        this.cursoActCursoCompleto = cursoActCursoCompleto;
    }

    public Integer getCursoActCursoPosterior()
    {
        return this.cursoActCursoPosterior;
    }

    public void setCursoActCursoPosterior(Integer cursoActCursoPosterior)
    {
        this.cursoActCursoPosterior = cursoActCursoPosterior;
    }

    public String getCursoActCursoPosteriorTxt()
    {
        return this.cursoActCursoPosteriorTxt;
    }

    public void setCursoActCursoPosteriorTxt(String cursoActCursoPosteriorTxt)
    {
        this.cursoActCursoPosteriorTxt = cursoActCursoPosteriorTxt;
    }

    public Integer getCursoActPerdidaCurso()
    {
        return this.cursoActPerdidaCurso;
    }

    public void setCursoActPerdidaCurso(Integer cursoActPerdidaCurso)
    {
        this.cursoActPerdidaCurso = cursoActPerdidaCurso;
    }

    public String getCursoActPerdidaCursoTxt()
    {
        return this.cursoActPerdidaCursoTxt;
    }

    public void setCursoActPerdidaCursoTxt(String cursoActPerdidaCursoTxt)
    {
        this.cursoActPerdidaCursoTxt = cursoActPerdidaCursoTxt;
    }

    public Integer getCursoAntCompleto()
    {
        return this.cursoAntCompleto;
    }

    public void setCursoAntCompleto(Integer cursoAntCompleto)
    {
        this.cursoAntCompleto = cursoAntCompleto;
    }

    public Float getCursoAntCreditos()
    {
        return this.cursoAntCreditos;
    }

    public void setCursoAntCreditos(Float cursoAntCreditos)
    {
        this.cursoAntCreditos = cursoAntCreditos;
    }

    public Float getCursoAntCreditosMatr()
    {
        return this.cursoAntCreditosMatr;
    }

    public void setCursoAntCreditosMatr(Float cursoAntCreditosMatr)
    {
        this.cursoAntCreditosMatr = cursoAntCreditosMatr;
    }

    public Float getCursoAntCreditosPend()
    {
        return this.cursoAntCreditosPend;
    }

    public void setCursoAntCreditosPend(Float cursoAntCreditosPend)
    {
        this.cursoAntCreditosPend = cursoAntCreditosPend;
    }

    public Float getCursoAntCreditosPendMax()
    {
        return this.cursoAntCreditosPendMax;
    }

    public void setCursoAntCreditosPendMax(Float cursoAntCreditosPendMax)
    {
        this.cursoAntCreditosPendMax = cursoAntCreditosPendMax;
    }

    public Float getCursoAntNotaMedia()
    {
        return this.cursoAntNotaMedia;
    }

    public void setCursoAntNotaMedia(Float cursoAntNotaMedia)
    {
        this.cursoAntNotaMedia = cursoAntNotaMedia;
    }

    public Float getDeducciones()
    {
        return this.deducciones;
    }

    public void setDeducciones(Float deducciones)
    {
        this.deducciones = deducciones;
    }

    public String getDistancia()
    {
        return this.distancia;
    }

    public void setDistancia(String distancia)
    {
        this.distancia = distancia;
    }

    public Integer getExisteCentroMasCercano()
    {
        return this.existeCentroMasCercano;
    }

    public void setExisteCentroMasCercano(Integer existeCentroMasCercano)
    {
        this.existeCentroMasCercano = existeCentroMasCercano;
    }

    public Float getIngresosFamiliares()
    {
        return this.ingresosFamiliares;
    }

    public void setIngresosFamiliares(Float ingresosFamiliares)
    {
        this.ingresosFamiliares = ingresosFamiliares;
    }

    public Integer getOtrasBecas()
    {
        return this.otrasBecas;
    }

    public void setOtrasBecas(Integer otrasBecas)
    {
        this.otrasBecas = otrasBecas;
    }

    public String getOtrasCircunstancias()
    {
        return this.otrasCircunstancias;
    }

    public void setOtrasCircunstancias(String otrasCircunstancias)
    {
        this.otrasCircunstancias = otrasCircunstancias;
    }

    public Float getPorcentajeIncrementoUmbral()
    {
        return this.porcentajeIncrementoUmbral;
    }

    public void setPorcentajeIncrementoUmbral(Float porcentajeIncrementoUmbral)
    {
        this.porcentajeIncrementoUmbral = porcentajeIncrementoUmbral;
    }

    public Float getRentaFamiliarDisponible()
    {
        return this.rentaFamiliarDisponible;
    }

    public void setRentaFamiliarDisponible(Float rentaFamiliarDisponible)
    {
        this.rentaFamiliarDisponible = rentaFamiliarDisponible;
    }

    public Float getRentaFamiliarPerCapita()
    {
        return this.rentaFamiliarPerCapita;
    }

    public void setRentaFamiliarPerCapita(Float rentaFamiliarPerCapita)
    {
        this.rentaFamiliarPerCapita = rentaFamiliarPerCapita;
    }

    public Integer getSuperaUmbrales()
    {
        return this.superaUmbrales;
    }

    public void setSuperaUmbrales(Integer superaUmbrales)
    {
        this.superaUmbrales = superaUmbrales;
    }

    public Float getSuperaUmbralesCuantia()
    {
        return this.superaUmbralesCuantia;
    }

    public void setSuperaUmbralesCuantia(Float superaUmbralesCuantia)
    {
        this.superaUmbralesCuantia = superaUmbralesCuantia;
    }

    public String getSuperaUmbralesTexto()
    {
        return this.superaUmbralesTexto;
    }

    public void setSuperaUmbralesTexto(String superaUmbralesTexto)
    {
        this.superaUmbralesTexto = superaUmbralesTexto;
    }

    public String getTextoAyudasReclamadas()
    {
        return this.textoAyudasReclamadas;
    }

    public void setTextoAyudasReclamadas(String textoAyudasReclamadas)
    {
        this.textoAyudasReclamadas = textoAyudasReclamadas;
    }

    public String getTextoJustificacion()
    {
        return this.textoJustificacion;
    }

    public void setTextoJustificacion(String textoJustificacion)
    {
        this.textoJustificacion = textoJustificacion;
    }

    public String getTextoPropuesta()
    {
        return this.textoPropuesta;
    }

    public void setTextoPropuesta(String textoPropuesta)
    {
        this.textoPropuesta = textoPropuesta;
    }

    public Beca getBeca()
    {
        return this.beca;
    }

    public void setBeca(Beca beca)
    {
        this.beca = beca;
    }

    @Transactional
    public void insert()
    {
        recursoDAO.insert(this);
    }

    public void update()
    {
        recursoDAO.update(this);
    }

    public String getCursoAntEstudio()
    {
        return cursoAntEstudio;
    }

    public void setCursoAntEstudio(String cursoAntEstudio)
    {
        this.cursoAntEstudio = cursoAntEstudio;
    }

    public Integer getMiembrosComputables()
    {
        return miembrosComputables;
    }

    public void setMiembrosComputables(Integer miembrosComputables)
    {
        this.miembrosComputables = miembrosComputables;
    }

    public String getCausasDesestimacion()
    {
        return causasDesestimacion;
    }

    public void setCausasDesestimacion(String causasDesestimacion)
    {
        this.causasDesestimacion = causasDesestimacion;
    }

    public String getAyudasActuales() {
        return ayudasActuales;
    }

    public void setAyudasActuales(String ayudasActuales) {
        this.ayudasActuales = ayudasActuales;
    }

    public String getDenegacionesActuales() {
        return denegacionesActuales;
    }

    public void setDenegacionesActuales(String denegacionesActuales) {
        this.denegacionesActuales = denegacionesActuales;
    }

    public Float getCursoAntNotaMediaSin() {
        return cursoAntNotaMediaSin;
    }

    public void setCursoAntNotaMediaSin(Float cursoAntNotaMediaSin) {
        this.cursoAntNotaMediaSin = cursoAntNotaMediaSin;
    }

    public String getCursoAntEstudioRama() {
        return cursoAntEstudioRama;
    }

    public void setCursoAntEstudioRama(String cursoAntEstudioRama) {
        this.cursoAntEstudioRama = cursoAntEstudioRama;
    }

    public Integer getCursoActCambioEstudios() {
        return cursoActCambioEstudios;
    }

    public void setCursoActCambioEstudios(Integer cursoActCambioEstudios) {
        this.cursoActCambioEstudios = cursoActCambioEstudios;
    }

    public Integer getEstimacion() {
        return estimacion;
    }

    public void setEstimacion(Integer estimacion) {
        this.estimacion = estimacion;
    }

    public Set<RecursoDetalle> getRecursoDetalles() {
        return recursoDetalles;
    }

    public void setRecursoDetalles(Set<RecursoDetalle> recursoDetalles) {
        this.recursoDetalles = recursoDetalles;
    }
}