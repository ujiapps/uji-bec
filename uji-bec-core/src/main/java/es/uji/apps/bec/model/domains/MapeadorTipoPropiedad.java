package es.uji.apps.bec.model.domains;

import java.util.HashMap;
import java.util.Map;

import es.uji.apps.bec.util.Mapeador;

public class MapeadorTipoPropiedad extends Mapeador<Long, Object>
{
    public MapeadorTipoPropiedad()
    {
        Map<CodigoOrganismo, Object> valores = new HashMap<CodigoOrganismo, Object>();

        valores.put(CodigoOrganismo.MINISTERIO, "UF");
        add(1L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, "OF");
        add(2L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, "PA");
        add(3L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, "SV");
        add(4L, valores);

        valores.put(CodigoOrganismo.MINISTERIO, "WA");
        add(5L, valores);
    }
}
