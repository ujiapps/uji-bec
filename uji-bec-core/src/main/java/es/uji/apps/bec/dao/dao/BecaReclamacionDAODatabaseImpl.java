package es.uji.apps.bec.dao.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.bec.model.BecaReclamacion;
import es.uji.apps.bec.model.QBecaDenegacion;
import es.uji.apps.bec.model.QBecaReclamacion;
import es.uji.apps.bec.model.QTipoEstadoReclamacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BecaReclamacionDAODatabaseImpl extends BaseDAODatabaseImpl implements BecaReclamacionDAO
{
    QBecaReclamacion becaReclamacion = QBecaReclamacion.becaReclamacion;
    QTipoEstadoReclamacion estado = QTipoEstadoReclamacion.tipoEstadoReclamacion;

    @Override
    public List<BecaReclamacion> getReclamacionesByBecaId(Long becaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(becaReclamacion)
                .where(becaReclamacion.beca.id.eq(becaId))
                .orderBy(becaReclamacion.fecha.asc());

        return query.list(becaReclamacion);
    }

    @Override
    public BecaReclamacion getReclamacionById(Long reclamacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(becaReclamacion).where(becaReclamacion.id.eq(reclamacionId));
        return query.uniqueResult(becaReclamacion);
    }

    @Override
    public void updateReclamacion(BecaReclamacion reclamacion)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, becaReclamacion);
        updateClause
                .where(becaReclamacion.id.eq(reclamacion.getId()))
                .set(becaReclamacion.observaciones, reclamacion.getObservaciones())
                .set(becaReclamacion.fechaNotificacion, reclamacion.getFechaNotificacion())
                .set(becaReclamacion.registroId, reclamacion.getRegistroId())
                .set(becaReclamacion.registroEjercicioId, reclamacion.getRegistroEjercicioId())
                .set(becaReclamacion.estado, reclamacion.getEstado())
                .set(becaReclamacion.fechaValidacion, reclamacion.getFechaValidacion())
                .execute();
    }
}
