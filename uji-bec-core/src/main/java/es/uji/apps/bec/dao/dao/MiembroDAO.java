package es.uji.apps.bec.dao.dao;

import java.util.List;

import es.uji.apps.bec.model.Miembro;
import es.uji.commons.db.BaseDAO;

public interface MiembroDAO extends BaseDAO
{
    Miembro getMiembroById(Long miembroId);

    Miembro getMiembroSolicitante(Long becaId);

    List<Miembro> getMiembrosByBeca(Long becaId);

    void updateMiembro(Miembro miembro);

    void deleteMiembro(Long miembroId);
}
