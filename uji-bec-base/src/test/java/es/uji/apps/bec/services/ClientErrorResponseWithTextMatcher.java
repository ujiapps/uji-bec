package es.uji.apps.bec.services;

import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class ClientErrorResponseWithTextMatcher extends TypeSafeMatcher<ClientResponse>
{
    private String errorText;

    public ClientErrorResponseWithTextMatcher(String errorText)
    {
        this.errorText = errorText;
    }

    @Factory
    public static <T> Matcher<ClientResponse> errorClientWithTextResponse(String errorText)
    {
        return new ClientErrorResponseWithTextMatcher(errorText);
    }

    @Override
    public boolean matchesSafely(ClientResponse response)
    {
        return (response != null && response.getStatus() >= 400 && response.getEntity(String.class).contains(errorText));
    }

    public void describeTo(Description description)
    {
        description.appendText("an error 500 HTTP response with text \"" + errorText + "\"");
    }
}