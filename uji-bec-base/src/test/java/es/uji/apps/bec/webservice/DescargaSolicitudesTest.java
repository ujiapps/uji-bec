package es.uji.apps.bec.webservice;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.exceptions.WsDatosEnviadosParaBajarBecaErroneosException;
import es.uji.apps.bec.services.ResultadoDescarga;
import es.uji.apps.bec.webservices.DescargaSolicitudes;
import es.uji.apps.bec.webservices.WebserviceMinisterio;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class DescargaSolicitudesTest {
    private BecaDAO becaDAO;
    private WebserviceMinisterio webServiceMinisterio;

    @Before
    public void init() {
        becaDAO = mock(BecaDAO.class);
        webServiceMinisterio = mock(WebserviceMinisterio.class);
    }

    @Test
    public void siSuperadoNumeroReintentosDescargaIncorrecta() throws Exception {
        paraUnaSolicitudQueSiempreFalla(webServiceMinisterio);

        DescargaSolicitudes descargaSolicitudes = new DescargaSolicitudes(webServiceMinisterio,
                becaDAO);
        Boolean descargadas = true;
        List<ResultadoDescarga> resultados = descargaSolicitudes.descarga(2013, "AE", descargadas);
        ResultadoDescarga resultadoDescarga = resultados.get(0);

        assertFalse(resultadoDescarga.isCorrecto());
    }

    @Test
    public void siRecuperacionSolicitudFallaReintentamosTresVeces() throws Exception {
        paraUnaSolicitudQueSiempreFalla(webServiceMinisterio);

        DescargaSolicitudes descargaSolicitudes = new DescargaSolicitudes(webServiceMinisterio,
                becaDAO);
        Boolean descargadas = true;

        descargaSolicitudes.descarga(2013, "AE", descargadas);

        verificaTodasLasLlamadasFallidas(becaDAO, webServiceMinisterio);
    }

    @Test
    @Ignore
    public void laListaDeResultadosDeLaCargaDebeSerIgualAlNumeroDeSolicitudes() throws Exception {
        paraUnaListaDeSolicitudes(webServiceMinisterio);

        DescargaSolicitudes descargaSolicitudes = new DescargaSolicitudes(webServiceMinisterio,
                becaDAO);
        Boolean descargadas = true;

        List<ResultadoDescarga> resultados = descargaSolicitudes.descarga(2013, "AE", descargadas);

        assertThat(resultados.size(), is(1));
    }

    private void paraUnaSolicitudQueSiempreFalla(WebserviceMinisterio webServiceMinisterio) throws Exception {
        paraUnaListaDeSolicitudes(webServiceMinisterio);

        when(webServiceMinisterio.getXmlSolicitudCiudadano(anyInt(), anyString(), anyInt(), anyString(), anyLong()))
                .thenThrow(new ServiceException());
    }

    private void paraUnaListaDeSolicitudes(WebserviceMinisterio webServiceMinisterio) throws Exception {
        when(webServiceMinisterio.getXmlSolicitudesCiudadanoRT(anyInt(), anyString(), anyBoolean()))
                .thenReturn(
                        "<SolicitudesRegistro>\n" + "<SolicitudRegistro>\n"
                                + "<CiudSqCiudadano>8740495</CiudSqCiudadano>\n"
                                + "<CiudIdCurso>2013</CiudIdCurso>\n"
                                + "<CiudCodConv>AE</CiudCodConv>\n"
                                + "<CiudSecuenc>1</CiudSecuenc>\n"
                                + "<CiudTipoUTTramite>1</CiudTipoUTTramite>\n"
                                + "<CiudCodUTTramite>400</CiudCodUTTramite>\n"
                                + "<CiudNif>11111111X</CiudNif>\n"
                                + "<CiudNombre>XXXX</CiudNombre>\n"
                                + "<CiudApellido1>YYYY</CiudApellido1>\n"
                                + "<CiudApellido2>ZZZZ</CiudApellido2>\n"
                                + "<CiudEntrada>2013-09-19</CiudEntrada>\n"
                                + "    </SolicitudRegistro>\n" + "</SolicitudesRegistro>\n");
    }

    private void verificaTodasLasLlamadasFallidas(BecaDAO becaDAO, WebserviceMinisterio webServiceMinisterio)
            throws Exception {
        verify(webServiceMinisterio
                , times(3)).getXmlSolicitudCiudadano(2013, "AE", 1, "11111111X", 8740495L);
        verify(becaDAO, times(0)).insert(anyObject());
    }
}
