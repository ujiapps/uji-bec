package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Estudio;
import es.uji.commons.db.BaseDAO;

public class EstudioBuilder
{

    private BaseDAO baseDAO;
    private Estudio estudio;
    
    public EstudioBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        estudio = new Estudio();
        estudio.setSinDocencia(false);
        estudio.setExtinguida(false);
        estudio.setTitulacionTecnica(false);
    }
    
    public EstudioBuilder withNombre(String nombre)
    {
        estudio.setNombre(nombre);
        return this;
    }
    
    public EstudioBuilder withTipo(String tipo)
    {
        estudio.setTipo(tipo);
        return this;
    }
    
    public EstudioBuilder withOficial(Boolean oficial)
    {
        estudio.setOficial(oficial);
        return this;
    }
    
    public EstudioBuilder withSinDocencia(Boolean sinDocencia)
    {
        estudio.setSinDocencia(sinDocencia);
        return this;
    }
    
    public EstudioBuilder withExtinguida(Boolean extinguida)
    {
        estudio.setExtinguida(extinguida);
        return this;
    }
    
    public EstudioBuilder withPresencial(String presencial)
    {
        estudio.setPresencial(presencial);
        return this;
    }
    
    public EstudioBuilder withTitulacionExtra(Boolean titulacionTecnica)
    {
        estudio.setTitulacionTecnica(titulacionTecnica);
        return this;
    }
    
    public Estudio build()
    {
        return baseDAO.insert(estudio);
    }
}
