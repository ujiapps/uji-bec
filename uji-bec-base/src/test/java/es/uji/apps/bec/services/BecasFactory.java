package es.uji.apps.bec.services;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.apps.bec.model.Denegacion;

public class BecasFactory
{
    public static CuantiaBeca nuevaCuantiaBeca(Beca beca, Cuantia cuantia, Float importe)
    {
        CuantiaBeca cuantiaBeca = new CuantiaBeca();
        cuantiaBeca.setBeca(beca);
        cuantiaBeca.setCuantia(cuantia);
        cuantiaBeca.setImporte(importe);
        
        return cuantiaBeca;
    }

    public static BecaDenegacion nuevaBecaDenegacion(Beca beca, Denegacion denegacion, Long ordenDenegacion)
    {
        BecaDenegacion becaDenegacion = new BecaDenegacion();
        becaDenegacion.setBeca(beca);
        becaDenegacion.setDenegacion(denegacion);
        becaDenegacion.setOrdenDenegacion(ordenDenegacion);
        
        return becaDenegacion;
    }
}
