package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Organismo;
import es.uji.commons.db.BaseDAO;

public class OrganismoBuilder
{
    private BaseDAO baseDAO;
    private Organismo organismo;

    public OrganismoBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        organismo = new Organismo();
    }

    public OrganismoBuilder withId(Long id)
    {
        organismo.setId(id);
        return this;
    }

    public OrganismoBuilder withNombre(String nombre)
    {
        organismo.setNombre(nombre);
        return this;
    }

    public Organismo build()
    {
        return baseDAO.insert(organismo);
    }
}
