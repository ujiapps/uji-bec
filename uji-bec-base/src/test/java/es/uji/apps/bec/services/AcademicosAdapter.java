package es.uji.apps.bec.services;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.http.MediaType;

public class AcademicosAdapter
{
    private WebResource resource;

    public AcademicosAdapter(WebResource resource)
    {
        this.resource = resource;
    }

    public ClientResponse actualizaAcademicosCursoActual(Long becaId)
    {
        return resource.path("/academicos/actual/" + becaId).accept(MediaType.TEXT_XML_VALUE).
                put(ClientResponse.class);
    }
}
