package es.uji.apps.bec.builder;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Minimo;

public class MinimoBuilder
{
    private Minimo minimo;
    private BecaDAO becaDAO;

    public MinimoBuilder(BecaDAO becaDAO)
    {
        this.becaDAO = becaDAO;
        this.minimo = new Minimo();
    }

    public MinimoBuilder withCreditosBecaCompleta(Float creditosBecaCompleta)
    {
        minimo.setCreditosBecaCompleta(creditosBecaCompleta);
        return this;
    }

    public MinimoBuilder withCreditosBecaParcial(Float creditosBecaParcial)
    {
        minimo.setCreditosBecaParcial(creditosBecaParcial);
        return this;
    }

    public MinimoBuilder withEstudio(Estudio estudio)
    {
        minimo.setEstudio(estudio);
        return this;
    }

    public MinimoBuilder withCursoAcademico(CursoAcademico cursoAcademico)
    {
        minimo.setCursoAcademico(cursoAcademico);
        return this;
    }

    public Minimo build()
    {
        if (becaDAO != null)
        {
            minimo = this.becaDAO.insert(minimo);
        }

        return minimo;
    }
}
