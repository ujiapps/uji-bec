package es.uji.apps.bec.services;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.builder.BecaBuilder;
import es.uji.apps.bec.builder.EstudioBuilder;
import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Estudio;
import es.uji.commons.rest.UIEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class ObservacionesServiceTest extends JerseyBaseTest
{
    @Autowired
    private BecaDAO becaDAO;
    private Beca beca;

    public ObservacionesServiceTest()
    {
        super("ferrerq", "65394");
    }

    @Before
    @Transactional
    public void init()
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);
        becaConstructor.init();
        becaConstructor.build();
        beca = becaConstructor.getBeca();
    }

    @Test
    @Transactional
    public void testRecuperaObservacionesDeOtrasBecasMismoSolicitante() throws Exception
    {
        creaBecaHistorica();

        List<UIEntity> observaciones = becaObservacionAdapter.getObservacionesHistoricoBeca(beca
                .getId());
        assertThat(observaciones, hasSize(1));
    }

    private void creaBecaHistorica()
    {
        EstudioBuilder estudioBuilder = new EstudioBuilder(becaDAO);
        Estudio estudio = estudioBuilder.withNombre("Titulacion2").withOficial(true)
                .withTipo("Tipo1").build();

        BecaBuilder becaBuilder = new BecaBuilder(becaDAO);
        becaBuilder.withSolicitante(beca.getSolicitante()).withConvocatoria(beca.getConvocatoria())
                .withEstudio(estudio).withProceso(beca.getProceso()).withEstado(beca.getEstado())
                .withTipoIdentificacion(beca.getTipoIdentificacion())
                .withObservacionesUji("Soy la Observación de la beca 2").build();
    }
}