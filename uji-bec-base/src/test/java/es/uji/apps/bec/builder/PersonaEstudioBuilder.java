package es.uji.apps.bec.builder;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.PersonaEstudio;

public class PersonaEstudioBuilder
{
    private PersonaEstudio personaEstudio;
    private BecaDAO becaDAO;

    public PersonaEstudioBuilder(BecaDAO becaDAO)
    {
        this.becaDAO = becaDAO;
        personaEstudio = new PersonaEstudio();
    }

    public PersonaEstudioBuilder withId(Long id)
    {
        personaEstudio.setId(id);
        return this;
    }

    public PersonaEstudioBuilder withCreditosMatriculados(Float creditosMatriculados)
    {
        personaEstudio.setCreditosMatriculados(creditosMatriculados);
        return this;

    }

    public PersonaEstudioBuilder withCreditosConvalidados(Float creditosConvalidados)
    {
        personaEstudio.setCreditosConvalidados(creditosConvalidados);
        return this;
    }

    public PersonaEstudioBuilder withCreditosPendientesConva(Float creditosPendientesConva)
    {
        personaEstudio.setCreditosPendientesConva(creditosPendientesConva);
        return this;
    }

    public PersonaEstudioBuilder withCreditosFueraBeca(Float creditosFueraBeca)
    {
        personaEstudio.setCreditosFueraBeca(creditosFueraBeca);
        return this;
    }

    public PersonaEstudioBuilder withCreditosParaBeca(Float creditosParaBeca)
    {
        personaEstudio.setCreditosParaBeca(creditosParaBeca);
        return this;
    }

    public PersonaEstudioBuilder withNumeroSemestres(Long numeroSemestres)
    {
        personaEstudio.setNumeroSemestres(numeroSemestres);
        return this;
    }

    public PersonaEstudioBuilder withCurso(Long curso)
    {
        personaEstudio.setCurso(curso);
        return this;
    }

    public PersonaEstudioBuilder withCursoAcademico(CursoAcademico cursoAcademico)
    {
        personaEstudio.setCursoAcademico(cursoAcademico);
        return this;
    }

    public PersonaEstudioBuilder withLimitarCreditos(Boolean limitarCreditos)
    {
        personaEstudio.setLimitarCreditos(limitarCreditos);
        return this;
    }

    public PersonaEstudioBuilder withPoseeTitulo(Boolean poseeTitulo)
    {
        personaEstudio.setPoseeTitulo(poseeTitulo);
        return this;

    }

    public PersonaEstudioBuilder withCreditosFaltantes(Float creditosFaltantes)
    {
        personaEstudio.setCreditosFaltantes(creditosFaltantes);
        return this;
    }

    public PersonaEstudioBuilder withAlgunaBecaParcial(Boolean algunaBecaParcial)
    {
        personaEstudio.setAlgunaBecaParcial(algunaBecaParcial);
        return this;
    }

    public PersonaEstudioBuilder withPersona(Persona persona)
    {
        personaEstudio.setPersona(persona);
        return this;
    }

    public PersonaEstudioBuilder withEstudio(Estudio estudio)
    {
        personaEstudio.setEstudio(estudio);
        return this;
    }

    public PersonaEstudio build()
    {
        if (becaDAO != null)
        {
            personaEstudio = this.becaDAO.insert(personaEstudio);
        }

        return personaEstudio;
    }
}
