package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.Organismo;
import es.uji.commons.db.BaseDAO;

public class ConvocatoriaBuilder
{
    private BaseDAO baseDAO;
    private Convocatoria convocatoria;
    
    public ConvocatoriaBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        convocatoria = new Convocatoria();
        convocatoria.setActiva(true);
    }

    public ConvocatoriaBuilder withId(Long id)
    {
        convocatoria.setId(id);
        return this;
    }

    public ConvocatoriaBuilder withNombre(String nombre)
    {
        convocatoria.setNombre(nombre);
        return this;
    }
    
    public ConvocatoriaBuilder withOrganismo(Organismo organismo)
    {
        convocatoria.setOrganismo(organismo);
        return this;
    }
    
    public ConvocatoriaBuilder withActiva(Boolean activa)
    {
        convocatoria.setActiva(activa);
        return this;
    }
    
    public Convocatoria build()
    {
        return baseDAO.insert(convocatoria);
    }
}
