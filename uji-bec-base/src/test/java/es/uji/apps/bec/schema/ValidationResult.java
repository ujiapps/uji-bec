package es.uji.apps.bec.schema;

public class ValidationResult {
    private boolean success;
    private String error;

    public ValidationResult() {
        this.success = true;
    }

    public ValidationResult(String error) {
        this.success = false;
        this.error = error;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public String retrieveError() {
        return this.error;
    }
}
