package es.uji.apps.bec.model;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.dao.dao.BecaDenegacionDAO;
import es.uji.apps.bec.dao.dao.ConvocatoriaDAO;
import es.uji.apps.bec.dao.dao.CuantiaBecaDAO;
import es.uji.apps.bec.dao.dao.DenegacionDAO;
import es.uji.apps.bec.dao.dao.DomicilioDAO;
import es.uji.apps.bec.dao.dao.EstadoDAO;
import es.uji.apps.bec.dao.dao.PersonaEstudioDAO;
import es.uji.apps.bec.model.domains.CodigoDenegacion;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DenegacionAcademicosMECTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    private static final Long PROCESO_ORDINARIO = 1L;

    private BecaDAO becaDAO;
    private BecaDenegacionDAO becaDenegacionDAO;
    private DenegacionDAO denegacionDAO;
    private PersonaEstudioDAO personaEstudioDAO;
    private EstadoDAO estadoDAO;
    private DomicilioDAO domicilioDAO;
    private CuantiaBecaDAO cuantiaBecaDAO;
    private ConvocatoriaDAO convocatoriaDAO;

    @Before
    public void initDAOs()
    {
        becaDAO = mock(BecaDAO.class);
        new Beca().setBecaDAO(becaDAO);

        becaDenegacionDAO = mock(BecaDenegacionDAO.class);
        new BecaDenegacion().setBecaDenegacionDAO(becaDenegacionDAO);

        denegacionDAO = mock(DenegacionDAO.class);
        new Denegacion().setDenegacionDAO(denegacionDAO);

        personaEstudioDAO = mock(PersonaEstudioDAO.class);
        new PersonaEstudio().setPersonaEstudioDAO(personaEstudioDAO);

        estadoDAO = mock(EstadoDAO.class);
        new Estado().setEstadoDAO(estadoDAO);

        domicilioDAO = mock(DomicilioDAO.class);
        new Domicilio().setDomicilioDAO(domicilioDAO);

        cuantiaBecaDAO = mock(CuantiaBecaDAO.class);
        new CuantiaBeca().setCuantiaBecaDAO(cuantiaBecaDAO);

        convocatoriaDAO = mock(ConvocatoriaDAO.class);
        new Convocatoria().setConvocatoriaDAO(convocatoriaDAO);
    }

    @Test
    @Ignore
    public void alDenegarBecaPorNoMatriculaBecaCambiaADenegadaUJI() throws Exception
    {
        Beca beca = aPartirDeUnaBeca();

        conUnAlumnoNoMatriculado();
        conUnDomicilioFamiliarEnLaComunidadValenciana(beca);
        conUnaConvocatoriaGeneral();

        seObtendraUnEstadoDenegacionAcademicos();

        PropuestaBecaMinisterio.calculaPropuesta(beca);

        assertThat(beca.getEstado().getId(), is(equalTo(CodigoEstado.DENEGADA_ACADEMICOS.getId())));
    }

    private void conUnaConvocatoriaGeneral()
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(1L);
        when(convocatoriaDAO.get(Convocatoria.class, 1L)).thenReturn(Collections.singletonList(convocatoria));
    }

    private void conUnDomicilioFamiliarEnLaComunidadValenciana(Beca beca)
    {
        when(domicilioDAO.getDomicilioFamiliar(1L)).thenReturn(
                beca.getDomicilios().iterator().next());
    }

    private void seObtendraUnEstadoDenegacionAcademicos()
    {
        Estado estadoDenegadaUJI = new Estado();
        estadoDenegadaUJI.setId(CodigoEstado.DENEGADA_ACADEMICOS.getId());

        when(estadoDAO.get(Estado.class, CodigoEstado.DENEGADA_ACADEMICOS.getId())).thenReturn(
                Collections.singletonList(estadoDenegadaUJI));
    }

    private void conUnAlumnoNoMatriculado()
    {
        when(
                personaEstudioDAO.getAcademicosbyPersonaEstudioYCursoAca(anyLong(), anyLong(),
                        anyLong())).thenReturn(new PersonaEstudio());

        Denegacion denegacion = new Denegacion();
        denegacion.setId(1L);

        when(denegacionDAO.get(Denegacion.class, CodigoDenegacion.NO_ESTAR_MATRICULADO_MEC.getId()))
                .thenReturn(Collections.singletonList(denegacion));
    }

    private Beca aPartirDeUnaBeca()
    {
        Beca beca = new Beca();

        Estudio estudio = new Estudio();
        estudio.setId(1L);
        beca.setEstudio(estudio);

        Solicitante solicitante = new Solicitante();
        solicitante.setId(1L);

        Persona persona = new Persona();
        persona.setId(1L);
        solicitante.setPersona(persona);

        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setId(1L);
        solicitante.setCursoAcademico(cursoAcademico);

        Domicilio domicilio = new Domicilio();
        domicilio.setId(1L);

        Provincia provincia = new Provincia();
        provincia.setId(12L);
        domicilio.setProvincia(provincia);

        beca.setDomicilios(Collections.singleton(domicilio));

        beca.setSolicitante(solicitante);

        Estado estado = new Estado();
        estado.setId(CodigoEstado.TRABAJADA.getId());
        beca.setEstado(estado);

        Proceso proceso = new Proceso();
        proceso.setId(PROCESO_ORDINARIO);
        beca.setProceso(proceso);

        return beca;
    }
}
