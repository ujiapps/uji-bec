package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Denegacion;
import es.uji.apps.bec.model.Organismo;
import es.uji.commons.db.BaseDAO;

public class DenegacionBuilder
{
    private BaseDAO baseDAO;
    private Denegacion denegacion;

    public DenegacionBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        denegacion = new Denegacion();
        denegacion.setActiva(true);
    }

    public DenegacionBuilder withId(Long id)
    {
        denegacion.setId(id);
        return this;
    }

    public DenegacionBuilder withNombre(String nombre)
    {
        denegacion.setNombre(nombre);
        return this;
    }

    public DenegacionBuilder withOrganismo(Organismo organismo)
    {
        denegacion.setOrganismo(organismo);
        return this;
    }

    public DenegacionBuilder withActiva(Boolean activa)
    {
        denegacion.setActiva(activa);
        return this;
    }

    public DenegacionBuilder withEstado(String estado)
    {
        denegacion.setEstado(estado);
        return this;
    }

    public DenegacionBuilder withCausa(String causa)
    {
        denegacion.setCausa(causa);
        return this;
    }

    public DenegacionBuilder withSubCausa(String subCausa)
    {
        denegacion.setSubCausa(subCausa);
        return this;
    }

    public Denegacion build()
    {
        return baseDAO.insert(denegacion);
    }
}
