package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.Cuantia;
import es.uji.commons.db.BaseDAO;

public class CuantiaBuilder
{
    private BaseDAO baseDAO;
    private Cuantia cuantia;

    public CuantiaBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        cuantia = new Cuantia();
        cuantia.setActiva(true);
    }

    public Cuantia getCuantia()
    {
        return cuantia;
    }

    public CuantiaBuilder withId(Long id)
    {
        cuantia.setId(id);
        return this;
    }

    public CuantiaBuilder withNombre(String nombre)
    {
        cuantia.setNombre(nombre);
        return this;
    }

    public CuantiaBuilder withCodigo(String codigo)
    {
        cuantia.setCodigo(codigo);
        return this;
    }

    public CuantiaBuilder withActiva(Boolean activa)
    {
        cuantia.setActiva(activa);
        return this;
    }

    public CuantiaBuilder withOrganismo(Convocatoria convocatoria)
    {
        cuantia.setConvocatoria(convocatoria);
        return this;
    }

    public Cuantia build()
    {
        return baseDAO.insert(cuantia);
    }
}
