package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaCurso;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.db.BaseDAO;

public class CuantiaCursoBuilder
{
    private BaseDAO baseDAO;
    private CuantiaCurso cuantiaCurso;
    
    public CuantiaCursoBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        cuantiaCurso = new CuantiaCurso();
    }
    
    public CuantiaCursoBuilder withImporte(Long importe)
    {
        cuantiaCurso.setImporte(importe);
        return this;
    }

    public CuantiaCursoBuilder withCuantia(Cuantia cuantia)
    {
        cuantiaCurso.setCuantia(cuantia);
        return this;
    }
    
    public CuantiaCursoBuilder withCursoAcademico(CursoAcademico cursoAcademico)
    {
        cuantiaCurso.setCursoAcademico(cursoAcademico);
        return this;
    }
    
    public CuantiaCurso build()
    {
        return baseDAO.insert(cuantiaCurso);
    }
}
