package es.uji.apps.bec.webservice;

import es.uji.apps.bec.exceptions.WsDatosEnviadosParaBajarBecaErroneosException;
import es.uji.apps.bec.exceptions.WsFaltaCodigoDeArchivoTemporalException;
import es.uji.apps.bec.webservices.WebserviceMinisterio;
import es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType;
import es.uji.apps.bec.webservices.ministerio.multienvio.SolicitudType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

public class WSBecasMinisterioTest
{
    private WebserviceMinisterio client;

    @Before
    public void initWSBecas() throws Exception
    {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/etc/uji/bec/app.properties"));

        String urlWebService = properties.getProperty("uji.ws.url");
        String usuarioWebService = properties.getProperty("uji.ws.usuario");
        String passwordWebService = properties.getProperty("uji.ws.password");
        String activoWebService = properties.getProperty("uji.ws.activo");
        client = new WebserviceMinisterio(urlWebService, usuarioWebService, passwordWebService,
                activoWebService);
    }

    @Ignore
    @Test
    public void getXmlSolicitudCiudadanoAE() throws Exception {
        es.uji.apps.bec.webservices.ministerio.solicitudes.SolicitudType solicitud = client
                .getXmlSolicitudCiudadanoSolicitudType(2011, "AE", 1, "52947392N", 5665011L);

        Assert.assertEquals("12", solicitud.getSoliCodProvCentro());
    }

    @Ignore
    @Test
    public void getXmlSolicitud() throws Exception
    {
        String dniBecaOk = "73397651M";
        String dniBecaMal = "20482348C";
        String dni = "73397651M";

        MultienvioType lote = client.getXmlSolicitudAsMultienvio(2012, "AE", dni);

        if (lote != null)
        {
            for (Object item : lote.getEnvio().get(0).getOrdenPagoAndErrorOrSolicitud())
            {
                SolicitudType solicitud = (SolicitudType) item;
                System.out.println("DNI: " + solicitud.getDatosPersonales().getDapeNif()
                        + ", Estado: " + solicitud.getSoliCodEstadoAct());
            }
        }
        else
        {
            System.out.println("Lote nulo");
        }
    }

    @Ignore
    @Test
    public void getXmlLote() throws Exception
    {
        MultienvioType lote = client.getXml(2012, "AE", 1, "00013");

        if (lote != null)
        {
            for (Object item : lote.getEnvio().get(0).getOrdenPagoAndErrorOrSolicitud())
            {
                SolicitudType solicitud = (SolicitudType) item;
                System.out.println("DNI: " + solicitud.getDatosPersonales().getDapeNif()
                        + ", Estado: " + solicitud.getSoliCodEstadoAct());
            }
        }
        else
        {
            System.out.println("Lote nulo");
        }
    }
}
