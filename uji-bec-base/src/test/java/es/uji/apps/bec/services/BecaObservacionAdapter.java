package es.uji.apps.bec.services;

import static es.uji.commons.testing.hamcrest.ClientOkResponseMatcher.okClientResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.springframework.http.MediaType;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import es.uji.commons.rest.UIEntity;

public class BecaObservacionAdapter
{
    private static final String URL_RECURSO = "beca/";
    private static final String URL_SUBRECURSO_OBSERVACION = "/observaciones";

    private WebResource resource;

    public BecaObservacionAdapter(WebResource resource)
    {
        this.resource = resource;
    }

    public List<UIEntity> getObservacionesHistoricoBeca(Long becaId)
    {
        ClientResponse response = resource.path(URL_RECURSO + becaId + URL_SUBRECURSO_OBSERVACION)
                .accept(MediaType.TEXT_XML_VALUE).get(ClientResponse.class);

        assertThat(response, is(okClientResponse()));

        return response.getEntity(new GenericType<List<UIEntity>>()
        {
        });
    }
}
