package es.uji.apps.bec.services;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.builder.BecaDenegacionBuilder;
import es.uji.apps.bec.builder.DenegacionBuilder;
import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.Denegacion;
import es.uji.apps.bec.model.Organismo;
import es.uji.commons.rest.UIEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class DenegacionesServiceTest extends JerseyBaseTest
{
    @Autowired
    private BecaDAO becaDAO;
    private Beca beca;
    private Denegacion denegacion;

    public DenegacionesServiceTest()
    {
        super("ferrerq", "65394");
    }

    @Before
    @Transactional
    public void init()
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);
        becaConstructor.init();
        becaConstructor.build();
        beca = becaConstructor.getBeca();

        Organismo organismo = becaConstructor.getOrganismo();

        DenegacionBuilder denegacionBuilder = new DenegacionBuilder(becaDAO);
        denegacion = denegacionBuilder.withOrganismo(organismo).withId(1L).withNombre("Denegacion 1")
                .withActiva(true).withCausa("00").withSubCausa("01").withEstado("10").build();

        BecaDenegacionBuilder becaDenegacionBuilder = new BecaDenegacionBuilder(becaDAO);
        becaDenegacionBuilder.withBeca(beca).withDenegacion(denegacion).withOrdenDenegacion(10L)
                .build();
    }

    @Test
    @Transactional
    public void testRecuperaDenegaciones() throws Exception
    {
        List<UIEntity> denegaciones = becaDenegacionAdapter.getDenegacionesBeca(beca.getId());
        assertThat(denegaciones, hasSize(1));
    }

    @Test
    @Transactional
    public void testBorraUnaDenegacion() throws Exception
    {
        List<UIEntity> denegaciones = becaDenegacionAdapter.deleteDenegacionBeca(beca.getId(),
                denegacion.getId());
        assertThat(denegaciones, hasSize(0));
    }

    @Test
    @Transactional
    public void testAnadirUnaDenegacion() throws Exception
    {
        List<UIEntity> denegaciones = becaDenegacionAdapter.getDenegacionesBeca(beca.getId());
        assertThat(denegaciones, hasSize(1));

        BecaDenegacion becaDenegacion = BecasFactory.nuevaBecaDenegacion(beca, denegacion, 10L);
        UIEntity entity = UIEntity.toUI(becaDenegacion);
        becaDenegacionAdapter.insertBecaDenegacion(beca.getId(), entity);

        denegaciones = becaDenegacionAdapter.getDenegacionesBeca(beca.getId());
        assertThat(denegaciones, hasSize(2));
    }
}