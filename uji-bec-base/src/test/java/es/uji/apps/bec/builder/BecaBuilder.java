package es.uji.apps.bec.builder;

import java.util.HashSet;
import java.util.Set;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.TipoIdentificacion;

public class BecaBuilder
{

    private BecaDAO becaDAO;
    private Beca beca;
    private Set<CuantiaBeca> cuantiasBeca;

    public BecaBuilder(BecaDAO becaDAO)
    {
        this.becaDAO = becaDAO;
        this.cuantiasBeca = new HashSet<CuantiaBeca>();

        beca = new Beca();
        beca.setTieneTituloUniversitario(true);
        beca.setBecaCursoAnt(true);
        beca.setBecaCursoAnt2(true);
        beca.setBecaCursoAnt3(true);
        beca.setMatriculaParcial(true);
        beca.setMatriculaParcialAnt(true);
        beca.setCumpleAcadBecaAnt(true);
        beca.setEstudiosSinDocencia(true);
        beca.setPresencial(true);
        beca.setLimitarCreditos(true);
        beca.setLimitarCreditosFinEstudios(true);
        beca.setBecaParcial(true);
        beca.setTransporteUrbano(true);
        beca.setTransporteBarcoAvion(true);
        beca.setAutorizacionRenta(true);
        beca.setProgramaIntercambio(true);
        beca.setCuantiasBecas(this.cuantiasBeca);
    }

    public Beca getBeca()
    {
        return beca;
    }

    public BecaBuilder withBecaId(Long becaId)
    {
        beca.setId(becaId);
        return this;
    }

    public BecaBuilder withSolicitante(Solicitante solicitante)
    {
        beca.setSolicitante(solicitante);
        return this;
    }

    public BecaBuilder withConvocatoria(Convocatoria convocatoria)
    {
        beca.setConvocatoria(convocatoria);
        return this;
    }

    public BecaBuilder withEstudio(Estudio estudio)
    {
        beca.setEstudio(estudio);
        return this;
    }

    public BecaBuilder withProceso(Proceso proceso)
    {
        beca.setProceso(proceso);
        return this;
    }

    public BecaBuilder withEstado(Estado estado)
    {
        beca.setEstado(estado);
        return this;
    }

    public BecaBuilder withTipoIdentificacion(TipoIdentificacion tipoIdentificacion)
    {
        beca.setTipoIdentificacion(tipoIdentificacion);
        return this;
    }

    public BecaBuilder withObservacionesUji(String observacionesUji)
    {
        beca.setObservacionesUji(observacionesUji);
        return this;
    }

    public BecaBuilder withCuantiaBeca(CuantiaBeca cuantiaBeca)
    {
        cuantiasBeca.add(cuantiaBeca);
        return this;
    }

    public BecaBuilder withCreditosParaBeca(Float creditosParaBeca)
    {
        beca.setCreditosParaBeca(creditosParaBeca);
        return this;
    }

    public Beca build()
    {
        if (becaDAO != null)
        {
            beca = becaDAO.insert(beca);
        }

        return beca;
    }
}