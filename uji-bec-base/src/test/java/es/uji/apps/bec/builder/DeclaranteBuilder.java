package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Declarante;
import es.uji.commons.db.BaseDAO;

public class DeclaranteBuilder
{
    private BaseDAO baseDAO;
    private Declarante declarante;

    public DeclaranteBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        declarante = new Declarante();
    }

    public DeclaranteBuilder withDeclarante(String declaranteNombre)
    {
        declarante.setDeclarante(declaranteNombre);
        return this;
    }

    public DeclaranteBuilder withUnidadFamiliarIndependiente(Boolean unidadFamiliarIndependiente)
    {
        declarante.setIndicadorIndependiente(unidadFamiliarIndependiente);
        return this;
    }

    public Declarante build()
    {
        return baseDAO.insert(declarante);
    }
}
