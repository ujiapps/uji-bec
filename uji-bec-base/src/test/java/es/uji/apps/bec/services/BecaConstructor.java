package es.uji.apps.bec.services;

import java.util.UUID;

import es.uji.apps.bec.builder.BecaBuilder;
import es.uji.apps.bec.builder.ConvocatoriaBuilder;
import es.uji.apps.bec.builder.CursoAcademicoBuilder;
import es.uji.apps.bec.builder.EstadoBuilder;
import es.uji.apps.bec.builder.EstudioBuilder;
import es.uji.apps.bec.builder.OrganismoBuilder;
import es.uji.apps.bec.builder.PersonaBuilder;
import es.uji.apps.bec.builder.ProcesoBuilder;
import es.uji.apps.bec.builder.SolicitanteBuilder;
import es.uji.apps.bec.builder.TipoIdentificacionBuilder;
import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.TipoIdentificacion;

public class BecaConstructor
{
    private BecaDAO becaDAO;
    private Beca beca;
    private CursoAcademico cursoAcademico;
    private Persona persona;
    private Solicitante solicitante;
    private Organismo organismo;
    private Convocatoria convocatoria;
    private Estudio estudio;
    private Proceso proceso;
    private TipoIdentificacion tipoIdentificacion;
    private BecaBuilder becaBuilder;
    private Estado estadoPendiente;
    private Estado estadoDenegadaAcademicos;
    private Estado estadoEnviada;
    private Estado estadoTrabajada;

    public BecaConstructor(BecaDAO becaDAO)
    {
        this.becaDAO = becaDAO;
    }

    public BecaBuilder getBecaBuilder()
    {
        return becaBuilder;
    }

    public BecaBuilder init()
    {
        String dniPersona = UUID.randomUUID().toString();
        PersonaBuilder personaBuilder = new PersonaBuilder(becaDAO);
        persona = personaBuilder.withId(999998L).withNombre("Persona 1")
                .withIdentificacion(dniPersona).build();

        CursoAcademicoBuilder cursoAcademicoBuilder = new CursoAcademicoBuilder(becaDAO);
        cursoAcademico = cursoAcademicoBuilder.withId(2013L).withActivo(true).build();

        SolicitanteBuilder solicitanteBuilder = new SolicitanteBuilder(becaDAO);
        solicitante = solicitanteBuilder.withPersona(persona).withCursoAcademico(cursoAcademico)
                .build();

        OrganismoBuilder organismoBuilder = new OrganismoBuilder(becaDAO);
        organismo = organismoBuilder.withId(1L).withNombre("Ministerio").build();

        ConvocatoriaBuilder convocatoriaBuilder = new ConvocatoriaBuilder(becaDAO);
        convocatoria = convocatoriaBuilder.withId(1L).withNombre("Convocatoria X").withOrganismo(organismo).build();

        EstudioBuilder estudioBuilder = new EstudioBuilder(becaDAO);
        estudio = estudioBuilder.withNombre("Titulacion1").withOficial(true).withTipo("Tipo1")
                .build();

        ProcesoBuilder procesoBuilder = new ProcesoBuilder(becaDAO);
        proceso = procesoBuilder.withNombre("Ordinario").withOrden(1L).build();

        EstadoBuilder estadoBuilderPendiente = new EstadoBuilder(becaDAO);
        estadoPendiente = estadoBuilderPendiente.withId(1L).withNombre("PENDIENTE").withOrden(1L).build();

        EstadoBuilder estadoBuilderTrabajada = new EstadoBuilder(becaDAO);
        estadoTrabajada = estadoBuilderTrabajada.withId(2L).withNombre("TRABAJADA").withOrden(2L).build();

        EstadoBuilder estadoBuilderDenegadaAcademicos = new EstadoBuilder(becaDAO);
        estadoDenegadaAcademicos = estadoBuilderDenegadaAcademicos.withId(3L)
                 .withNombre("DENEGADA_ACADEMICOS").withOrden(3L).build();

        EstadoBuilder estadoBuilderEnviada = new EstadoBuilder(becaDAO);
        estadoEnviada = estadoBuilderEnviada.withId(4L).withNombre("ENVIADA").withOrden(4L).build();

        TipoIdentificacionBuilder tipoIdentificacionBuilder =
                new TipoIdentificacionBuilder(becaDAO);
        tipoIdentificacion = tipoIdentificacionBuilder.withId(1L)
                .withNombre("Tipo identificacion 1").withOrden(1L).build();

        becaBuilder =
                new BecaBuilder(becaDAO).withSolicitante(solicitante).withConvocatoria(convocatoria)
                        .withEstudio(estudio).withProceso(proceso).withEstado(estadoPendiente)
                        .withTipoIdentificacion(tipoIdentificacion)
                        .withObservacionesUji("Soy la observación UJI");

        return becaBuilder;
    }

    public void build()
    {
        this.becaBuilder.build();
    }

    public Beca getBeca()
    {
        return this.becaBuilder.getBeca();
    }

    public CursoAcademico getCursoAcademico()
    {
        return this.cursoAcademico;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public Solicitante getSolicitante()
    {
        return this.solicitante;
    }

    public Organismo getOrganismo()
    {
        return this.organismo;
    }

    public Proceso getProceso()
    {
        return this.proceso;
    }

    public Estado getEstadoPendiente()
    {
        return this.estadoPendiente;
    }

    public Estado getEstadoTrabajada()
    {
        return this.estadoTrabajada;
    }

    public Estado getEstadoDenegadaAcademicos()
    {
        return this.estadoDenegadaAcademicos;
    }

    public Estado getEstadoEnviada()
    {
        return this.estadoEnviada;
    }

    public TipoIdentificacion getTipoIdentificacion()
    {
        return this.tipoIdentificacion;
    }

    public Convocatoria getConvocatoria()
    {
        return this.convocatoria;
    }

    public Estudio getEstudio()
    {
        return this.estudio;
    }
}
