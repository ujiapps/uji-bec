package es.uji.apps.bec.services;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import es.uji.apps.bec.model.Beca;
import es.uji.commons.rest.UIEntity;
import org.springframework.http.MediaType;

import java.util.List;

public class BecaAdapter
{
    private WebResource resource;

    public BecaAdapter(WebResource resource)
    {
        this.resource = resource;
    }

    public Beca getBeca(Long becaId)
    {
        ClientResponse response = resource.path("/beca/" + becaId).accept(MediaType.TEXT_XML_VALUE).
                get(ClientResponse.class);
        List<UIEntity> becas = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        return becas.get(0).toModel(Beca.class);
    }
}
