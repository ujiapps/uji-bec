package es.uji.apps.bec.services;

import ch.qos.logback.ext.spring.web.LogbackConfigListener;

import java.util.HashMap;
import java.util.Map;

import com.sun.jersey.api.client.filter.LoggingFilter;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.util.Log4jConfigListener;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerFactory;
import com.sun.jersey.test.framework.spi.container.grizzly.web.GrizzlyWebTestContainerFactory;

import es.uji.apps.bec.UIEntityMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityListMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityMessageBodyWriter;
import es.uji.commons.sso.filters.UjiAppsFilter;
import es.uji.si.SSO.filter.LsmFilter;

public class JerseyBaseTest extends JerseyTest
{
    protected WebResource resource;

    protected BecaAdapter becaAdapter;
    protected BecaCuantiaAdapter becaCuantiaAdapter;
    protected BecaDenegacionAdapter becaDenegacionAdapter;
    protected BecaObservacionAdapter becaObservacionAdapter;
    protected AcademicosAdapter academicosAdapter;

    public JerseyBaseTest(String login, String personaId)
    {
        super(new WebAppDescriptor.Builder("es.uji.commons.rest.shared; es.uji.commons.rest.xml; es.uji.apps.bec.services")
                .contextParam("contextConfigLocation", "classpath:applicationContext-test.xml")
                .contextParam("webAppRootKey", "uji-bec-base.root")
                .contextListenerClass(LogbackConfigListener.class)
                .contextListenerClass(ContextLoaderListener.class)
                .requestListenerClass(RequestContextListener.class)
                .servletClass(SpringServlet.class)
                .addFilter(LsmFilter.class, "/*", getLsmAuthFilterConfig(login, personaId))
                .addFilter(UjiAppsFilter.class, "/*", getUjiAppsAuthFilterConfig(login, personaId))
                .clientConfig(createClientConfig()).build());

        this.client().addFilter(new LoggingFilter());
        this.resource = resource();

        becaAdapter = new BecaAdapter(resource);
        becaCuantiaAdapter = new BecaCuantiaAdapter(resource);
        becaDenegacionAdapter = new BecaDenegacionAdapter(resource);
        becaObservacionAdapter = new BecaObservacionAdapter(resource);
        academicosAdapter = new AcademicosAdapter(resource);
    }

    private static Map<String, String> getLsmAuthFilterConfig(String login, String personaId)
    {
        Map<String, String> params = new HashMap<String, String>();

        params.put("ssoAuthentication", "true");
        params.put("dontFilterIfLocalhost", "true");

        return params;
    }

    private static Map<String, String> getUjiAppsAuthFilterConfig(String login, String personaId)
    {
        Map<String, String> params = new HashMap<String, String>();

        params.put("defaultUserName", login);
        params.put("defaultUserId", personaId);
        params.put("exclude", ".*/bec/rest/beca/matricula.*");

        return params;
    }

    private static ClientConfig createClientConfig()
    {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(UIEntityMessageBodyReader.class);
        config.getClasses().add(UIEntityListMessageBodyReader.class);
        config.getClasses().add(UIEntityMessageBodyWriter.class);

        return config;
    }
    
    @Override
    protected TestContainerFactory getTestContainerFactory()
    {
        return new GrizzlyWebTestContainerFactory();
    }    
}
