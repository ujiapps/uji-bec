package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.db.BaseDAO;

public class CursoAcademicoBuilder
{
    private BaseDAO baseDAO;
    private CursoAcademico cursoAcademico;

    public CursoAcademicoBuilder(BaseDAO baeDAO)
    {
        this.baseDAO = baeDAO;
        cursoAcademico = new CursoAcademico();
        cursoAcademico.setActivo(false);
    }

    public CursoAcademicoBuilder withId(Long id)
    {
        cursoAcademico.setId(id);
        return this;
    }
    
    public CursoAcademicoBuilder withActivo(Boolean activo)
    {
        cursoAcademico.setActivo(activo);
        return this;
    }

    public CursoAcademico build()
    {
        return baseDAO.insert(cursoAcademico);
    }

}
