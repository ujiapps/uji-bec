package es.uji.apps.bec.services;

import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class ClientErrorResponseMatcher extends TypeSafeMatcher<ClientResponse>
{
    @Factory
    public static <T> Matcher<ClientResponse> errorClientResponse()
    {
        return new ClientErrorResponseMatcher();
    }

    @Override
    public boolean matchesSafely(ClientResponse response)
    {
        return (response != null && response.getStatus() >= 400);
    }

    public void describeTo(Description description)
    {
        description.appendText("an error 500 HTTP response");
    }
}