package es.uji.apps.bec.dao.dao;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.builder.BecaBuilder;
import es.uji.apps.bec.builder.ConvocatoriaBuilder;
import es.uji.apps.bec.builder.CursoAcademicoBuilder;
import es.uji.apps.bec.builder.EstadoBuilder;
import es.uji.apps.bec.builder.EstudioBuilder;
import es.uji.apps.bec.builder.OrganismoBuilder;
import es.uji.apps.bec.builder.PersonaBuilder;
import es.uji.apps.bec.builder.ProcesoBuilder;
import es.uji.apps.bec.builder.SolicitanteBuilder;
import es.uji.apps.bec.builder.TipoIdentificacionBuilder;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.TipoIdentificacion;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = { "/applicationContext.xml" })
@Transactional
public class BecaDAOTest
{

    private Beca beca1;
    private Beca beca2;
    private Persona persona;
    private CursoAcademico cursoAcademico;
    private Solicitante solicitante;
    private Organismo organismo;
    private Convocatoria convocatoria1;
    private Convocatoria convocatoria2;
    private Proceso proceso;
    private Estado estado;
    private Estudio estudio;
    private TipoIdentificacion tipoIdentificacion;

    @Autowired
    private BecaDAO becaDAO;

    @Before
    public void init()
    {
        PersonaBuilder personaBuilder = new PersonaBuilder(becaDAO);
        persona = personaBuilder.withNombre("Persona 1").withIdentificacion("123456789A").build();

        CursoAcademicoBuilder cursoAcademicoBuilder = new CursoAcademicoBuilder(becaDAO);
        cursoAcademico = cursoAcademicoBuilder.withActivo(true).build();

        OrganismoBuilder organismoBuilder = new OrganismoBuilder(becaDAO);
        organismo = organismoBuilder.withId(1L).withNombre("Ministerio").build();

        ConvocatoriaBuilder convocatoriaBuilder = new ConvocatoriaBuilder(becaDAO);
        convocatoria1 = convocatoriaBuilder.withId(1L).withNombre("Convocatoria X").withOrganismo(organismo).build();

        convocatoriaBuilder = new ConvocatoriaBuilder(becaDAO);
        convocatoria2 = convocatoriaBuilder.withId(1L).withNombre("Convocatoria Y").withOrganismo(organismo).build();

        EstudioBuilder estudioBuilder = new EstudioBuilder(becaDAO);
        estudio = estudioBuilder.withNombre("Titulacion1").withOficial(true).withTipo("Tipo1")
                .build();

        ProcesoBuilder procesoBuilder = new ProcesoBuilder(becaDAO);
        proceso = procesoBuilder.withNombre("Ordinario").withOrden(1L).build();

        EstadoBuilder estadoBuilder = new EstadoBuilder(becaDAO);
        estado = estadoBuilder.withId(1L).withNombre("Estado 1").withOrden(1L).build();

        TipoIdentificacionBuilder tipoIdentificacionBuilder = new TipoIdentificacionBuilder(becaDAO);
        tipoIdentificacion = tipoIdentificacionBuilder.withId(1L)
                .withNombre("Tipo identificacion 1").withOrden(1L).build();

        SolicitanteBuilder solicitanteBuilder = new SolicitanteBuilder(becaDAO);
        solicitante = solicitanteBuilder.withPersona(persona).withCursoAcademico(cursoAcademico)
                .build();

        BecaBuilder becaBuilder1 = new BecaBuilder(becaDAO);
        beca1 = becaBuilder1.withSolicitante(solicitante).withConvocatoria(convocatoria1)
                .withEstudio(estudio).withProceso(proceso).withEstado(estado)
                .withTipoIdentificacion(tipoIdentificacion).build();

        BecaBuilder becaBuilder2 = new BecaBuilder(becaDAO);
        beca2 = becaBuilder2.withSolicitante(solicitante).withConvocatoria(convocatoria2)
                .withEstudio(estudio).withProceso(proceso).withEstado(estado)
                .withTipoIdentificacion(tipoIdentificacion).build();

        becaDAO.flush();
    }

    @Ignore
    @Test
    public void unaBecaEstaDuplicadaSiCoincidenSolicitanteConvocatoriaYEstudio()
    {
        beca1.setConvocatoria(convocatoria2);

        Long solicitanteId = beca1.getSolicitante().getId();
        Long estudioId = beca1.getEstudio().getId();
        Long convocatoriaId = beca1.getConvocatoria().getId();

        boolean duplicada = becaDAO.isDuplicada(solicitanteId, convocatoriaId, estudioId);

        Assert.assertEquals(true, duplicada);
    }
}
