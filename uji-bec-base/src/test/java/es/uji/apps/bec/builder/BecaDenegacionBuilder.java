package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.Denegacion;
import es.uji.commons.db.BaseDAO;

public class BecaDenegacionBuilder
{
    private BaseDAO baseDAO;
    private BecaDenegacion becaDenegacion;

    public BecaDenegacionBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        becaDenegacion = new BecaDenegacion();
    }

    public BecaDenegacionBuilder withDenegacion(Denegacion denegacion)
    {
        becaDenegacion.setDenegacion(denegacion);
        return this;
    }

    public BecaDenegacionBuilder withBeca(Beca beca)
    {
        becaDenegacion.setBeca(beca);
        return this;
    }

    public BecaDenegacionBuilder withOrdenDenegacion(Long ordenDenegacion)
    {
        becaDenegacion.setOrdenDenegacion(ordenDenegacion);
        return this;
    }

    public BecaDenegacion build()
    {
        return baseDAO.insert(becaDenegacion);
    }
}
