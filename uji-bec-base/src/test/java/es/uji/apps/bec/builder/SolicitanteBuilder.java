package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Declarante;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Solicitante;
import es.uji.commons.db.BaseDAO;

public class SolicitanteBuilder
{
    private BaseDAO baseDAO;
    private Solicitante solicitante;

    public SolicitanteBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        solicitante = new Solicitante();
        solicitante.setOrfandad(false);
    }

    public SolicitanteBuilder withCursoAcademico(CursoAcademico cursoAcademico)
    {
        solicitante.setCursoAcademico(cursoAcademico);
        return this;
    }

    public SolicitanteBuilder withPersona(Persona persona)
    {
        solicitante.setPersona(persona);
        return this;
    }

    public SolicitanteBuilder withOrfandad(Boolean orfandad)
    {
        solicitante.setOrfandad(orfandad);
        return this;
    }


    public SolicitanteBuilder withDeclarante(Declarante declarante)
    {
        solicitante.setDeclarante(declarante);
        return this;
    }

    public Solicitante build()
    {
        return baseDAO.insert(solicitante);
    }
}
