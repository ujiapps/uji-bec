package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.commons.db.BaseDAO;

public class CuantiaBecaBuilder
{
    private BaseDAO baseDAO;
    private CuantiaBeca becaCuantia;

    public CuantiaBecaBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        becaCuantia = new CuantiaBeca();
    }

    public CuantiaBecaBuilder withImporte(Float importe)
    {
        becaCuantia.setImporte(importe);
        return this;
    }
    
    public CuantiaBecaBuilder withBeca(Beca beca)
    {
        becaCuantia.setBeca(beca);
        return this;
    }
    
    public CuantiaBecaBuilder withCuantia(Cuantia cuantia)
    {
        becaCuantia.setCuantia(cuantia);
        return this;
    }
    
    public CuantiaBeca build()
    {
        return baseDAO.insert(becaCuantia);
    }
}
