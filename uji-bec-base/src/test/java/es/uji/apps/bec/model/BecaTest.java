package es.uji.apps.bec.model;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.dao.dao.BecaDAO;

public class BecaTest
{
    private BecaDAO becaDAOMock;
    private Beca beca;

    @Before
    public void init()
    {
        becaDAOMock = mock(BecaDAO.class);
        beca = generaBeca(becaDAOMock);
    }

    @Test(expected = BecaDuplicadaException.class)
    public void ocurreErrorSiAlActualizarBecaSeProduceUnaBecaDuplicada()
            throws BecaDuplicadaException
    {
        when(becaDAOMock.isDuplicada(anyLong(), anyLong(), anyLong())).thenReturn(true);

        beca.update();
    }

    @Test
    public void noOcurreErrorSiAlActualizarBecaNoSeProduceUnaBecaDuplicada()
            throws BecaDuplicadaException
    {
        when(becaDAOMock.isDuplicada(anyLong(), anyLong(), anyLong())).thenReturn(false);

        beca.update();

        verify(becaDAOMock).updateBeca(beca);
    }

    private Beca generaBeca(BecaDAO becaDAOMock)
    {
        Beca beca = new Beca();
        beca.setSolicitante(new Solicitante());
        beca.setConvocatoria(new Convocatoria());
        beca.setBecaDAO(becaDAOMock);

        return beca;
    }
}