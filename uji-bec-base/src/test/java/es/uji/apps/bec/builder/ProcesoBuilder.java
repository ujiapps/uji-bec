package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Proceso;
import es.uji.commons.db.BaseDAO;

public class ProcesoBuilder
{
    private BaseDAO baseDAO;
    private Proceso proceso;
    
    public ProcesoBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        proceso = new Proceso();
    }
    
    public ProcesoBuilder withNombre(String nombre)
    {
        proceso.setNombre(nombre);
        return this;
    }
    
    public ProcesoBuilder withOrden(Long orden)
    {
        proceso.setOrden(orden);
        return this;
    }
    
    public Proceso build()
    {
        return baseDAO.insert(proceso);
    }

}
