package es.uji.apps.bec.schema;

import org.junit.Test;

import java.io.FileInputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SchemaValidationTest {
    @Test
    public void validateXmlSchema() throws Exception {
        XmlSchemaValidator validator = new XmlSchemaValidator();

        ValidationResult result = validator.validate(
                new FileInputStream("src/test/resources/test.xsd"),
                new FileInputStream("src/test/resources/test.xml")
        );

        assertThat(result.isSuccess(), is(true));
    }
}
