package es.uji.apps.bec.webservice;

import es.uji.apps.bec.webservices.WebserviceMinisterio;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;

public class WebServiceBecasColaboracionTest
{
    private WebserviceMinisterio client;

    @Before
    public void initWSBecas() throws Exception
    {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/etc/uji/bec/app.properties"));

        String urlWebService = properties.getProperty("uji.ws.url");
        String usuarioWebService = properties.getProperty("uji.ws.usuario");
        String passwordWebService = properties.getProperty("uji.ws.password");
        String activoWebService = properties.getProperty("uji.ws.activo");

        client = new WebserviceMinisterio(urlWebService, usuarioWebService, passwordWebService,
                activoWebService);
    }

    @Test
    public void becasColaboracion() throws Exception
    {
        String resultado = client.getXmlSolicitudesCiudadanoRT(2013, "CO", false);
        assertNotNull(resultado);
    }
}