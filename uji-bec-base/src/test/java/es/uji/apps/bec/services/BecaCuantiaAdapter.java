package es.uji.apps.bec.services;

import static es.uji.commons.testing.hamcrest.ClientOkResponseMatcher.okClientResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.springframework.http.MediaType;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import es.uji.commons.rest.UIEntity;

public class BecaCuantiaAdapter
{
    private static final String URL_RECURSO = "beca/";
    private static final String URL_SUBRECURSO_CUANTIA = "/cuantia";

    private WebResource resource;

    public BecaCuantiaAdapter(WebResource resource)
    {
        this.resource = resource;
    }

    public List<UIEntity> getCuantiasBeca(Long becaId)
    {
        ClientResponse response = resource.path(URL_RECURSO + becaId + URL_SUBRECURSO_CUANTIA)
                .accept(MediaType.TEXT_XML_VALUE).get(ClientResponse.class);

        assertThat(response, is(okClientResponse()));

        return response.getEntity(new GenericType<List<UIEntity>>()
        {
        });
    }

    public List<UIEntity> deleteCuantiaBeca(Long becaId, Long cuantiaId)
    {
        ClientResponse response = resource
                .path(URL_RECURSO + becaId + URL_SUBRECURSO_CUANTIA + "/" + cuantiaId)
                .accept(MediaType.APPLICATION_XML_VALUE).delete(ClientResponse.class);

        assertThat(response, is(okClientResponse()));

        return response.getEntity(new GenericType<List<UIEntity>>()
        {
        });
    }

    public List<UIEntity> insertCuantiaBeca(Long becaId, UIEntity entity)
    {
        ClientResponse response = resource.path(URL_RECURSO + becaId + URL_SUBRECURSO_CUANTIA)
                .accept(MediaType.TEXT_XML_VALUE).type(MediaType.TEXT_XML_VALUE)
                .post(ClientResponse.class, entity);

        assertThat(response, is(okClientResponse()));

        List<UIEntity> cuantias = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(cuantias, hasSize(1));

        return cuantias;
    }
}
