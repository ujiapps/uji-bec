package es.uji.apps.bec.webservice;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.webservices.ministerio.denegaciones.MultienvioType;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import static junit.framework.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = { "/applicationContext.xml" })
@Transactional
@Ignore
public class DenegacionesTest
{
    @Autowired
    private BecaDAO becaDAO;

    @Test
    public void marshall() throws Exception
    {
        JAXBContext contextDenegaciones = JAXBContext
                .newInstance("es.uji.apps.bec.webservices.ministerio.denegaciones");

        Marshaller marshallerDenegaciones = contextDenegaciones.createMarshaller();
        Unmarshaller unmarshallerDenegaciones = contextDenegaciones.createUnmarshaller();


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MultienvioType multienvioType = becaDAO.get(MultienvioType.class, 4745L).get(0);
        JAXBElement<MultienvioType> element = new JAXBElement<MultienvioType>(new QName("x"), MultienvioType.class, multienvioType);
        marshallerDenegaciones.marshal(element, baos);

        assertTrue(baos.toByteArray().length > 0);
    }
}
