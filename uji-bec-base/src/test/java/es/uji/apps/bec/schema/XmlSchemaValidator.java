package es.uji.apps.bec.schema;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.InputStream;

public class XmlSchemaValidator {
    public ValidationResult validate(InputStream xsd, InputStream xml) {
        try {

            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));

            return new ValidationResult();
        }
        catch (Exception e) {
            return new ValidationResult(e.getLocalizedMessage());
        }
    }
}
