package es.uji.apps.bec.services;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.builder.CuantiaBecaBuilder;
import es.uji.apps.bec.builder.CuantiaBuilder;
import es.uji.apps.bec.builder.CuantiaCursoBuilder;
import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.dao.dao.CuantiaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.rest.UIEntity;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class CuantiasServiceTest extends JerseyBaseTest
{
    @Autowired
    private BecaDAO becaDAO;

    @Autowired
    private CuantiaDAO cuantiaDAO;

    private Beca beca;
    private Cuantia cuantia;

    public CuantiasServiceTest()
    {
        super("ferrerq", "65394");
    }

    @Before
    @Transactional
    public void init()
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);
        becaConstructor.init();
        becaConstructor.build();
        beca = becaConstructor.getBeca();
        CursoAcademico cursoAcademico = becaConstructor.getCursoAcademico();

        CuantiaBuilder cuantiaBuilder = new CuantiaBuilder(becaDAO);
        cuantiaBuilder.withId(1L)
                      .withCodigo("XYZ")
                      .withNombre("Cuantia 1")
                      .withActiva(true);

        cuantiaBuilder.build();

        cuantia = cuantiaBuilder.getCuantia();

        CuantiaCursoBuilder cuantiaCursoBuilder = new CuantiaCursoBuilder(becaDAO);
        cuantiaCursoBuilder.withCuantia(cuantia).withImporte(1000L)
                .withCursoAcademico(cursoAcademico).build();

        CuantiaBecaBuilder cuantiaBecaBuilder = new CuantiaBecaBuilder(becaDAO);
        cuantiaBecaBuilder.withBeca(beca).withCuantia(cuantia).withImporte(100F).build();
    }

    @Test
    @Transactional
    @Ignore
    public void testRecuperaCuantias() throws Exception
    {
        List<UIEntity> cuantias = becaCuantiaAdapter.getCuantiasBeca(beca.getId());
        assertThat(cuantias, hasSize(1));
    }

    @Test
    @Transactional
    @Ignore
    public void testBorraUnaCuantia() throws Exception
    {
        List<UIEntity> cuantias = becaCuantiaAdapter.deleteCuantiaBeca(beca.getId(),
                cuantia.getId());
        assertThat(cuantias, hasSize(0));
    }

    @Test
    @Transactional
    @Ignore
    public void testAnadirUnaCuantia() throws Exception
    {
        List<UIEntity> cuantias = becaCuantiaAdapter.getCuantiasBeca(beca.getId());
        assertThat(cuantias, hasSize(1));

        CuantiaBeca cuantiaBeca = BecasFactory.nuevaCuantiaBeca(beca, cuantia, 555F);
        UIEntity entity = UIEntity.toUI(cuantiaBeca);
        becaCuantiaAdapter.insertCuantiaBeca(beca.getId(), entity);

        cuantias = becaCuantiaAdapter.getCuantiasBeca(beca.getId());
        assertThat(cuantias, hasSize(2));
    }
}