package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Persona;
import es.uji.commons.db.BaseDAO;

public class PersonaBuilder
{
    private BaseDAO baseDAO;
    private Persona persona;

    public PersonaBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        persona = new Persona();
    }

    public PersonaBuilder withId(Long id)
    {
        persona.setId(id);
        return this;
    }

    public PersonaBuilder withNombre(String nombre)
    {
        persona.setNombre(nombre);
        return this;
    }

    public PersonaBuilder withIdentificacion(String identificacion)
    {
        persona.setIdentificacion(identificacion);
        return this;
    }

    public Persona build()
    {
        return baseDAO.insert(persona);
    }
}
