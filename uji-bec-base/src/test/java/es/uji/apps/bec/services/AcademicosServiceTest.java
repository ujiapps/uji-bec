package es.uji.apps.bec.services;

import com.sun.jersey.api.client.ClientResponse;
import es.uji.apps.bec.builder.BecaBuilder;
import es.uji.apps.bec.builder.MinimoBuilder;
import es.uji.apps.bec.builder.PersonaBuilder;
import es.uji.apps.bec.builder.PersonaEstudioBuilder;
import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Persona;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static es.uji.apps.bec.services.ClientErrorResponseWithTextMatcher.errorClientWithTextResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class AcademicosServiceTest extends JerseyBaseTest
{
    @Autowired
    private BecaDAO becaDAO;

    public AcademicosServiceTest()
    {
        super("ferrerq", "65394");
    }

    // TODO: Naming malo acopla a la implementación
    // TODO: Test para setLimitarCreditosMatriculados
    // TODO: DRY
    // TODO: Inicialización del beca constructor

    @Test
    @Transactional
    public void noSeActualizaBecaSiMarcadaComoNoModificable() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);
        Beca beca = becaConstructor.init()
                .withEstado(becaConstructor.getEstadoEnviada())
                .build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(10F)
                .withCreditosBecaParcial(2F)
                .build();

        becaDAO.flush();

        ClientResponse response = academicosAdapter.actualizaAcademicosCursoActual(beca.getId());
        assertThat(response, is(errorClientWithTextResponse("no permiteix modificar dades")));
    }

    @Test
    @Transactional
    public void noSeActualizaBecaSiNoHayDatosPersonaEstudioValidos() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        Beca beca = becaConstructor.init()
                .withCreditosParaBeca(10F)
                .build();

        Persona personaError = new PersonaBuilder(becaDAO).withId(111L).build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(personaError)
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(10F)
                .withCreditosBecaParcial(2F)
                .build();

        becaDAO.flush();

        ClientResponse response = academicosAdapter.actualizaAcademicosCursoActual(beca.getId());
        assertThat(response, is(errorClientWithTextResponse("No hi ha dades del curs actual")));
    }

    @Test
    @Transactional
    @Ignore
    public void becaSeActualizaConDatosAcademicosCursoActual() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);
        Beca beca = becaConstructor.init().build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(1F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.getCreditosMatriculados(), is(2F));
        assertThat(becaModificada.getCreditosConvalidados(), is(3F));
        assertThat(becaModificada.getCreditosPendientesConvalidacion(), is(4F));
        assertThat(becaModificada.getCreditosFueraBeca(), is(5F));
        assertThat(becaModificada.getCreditosParaBeca(), is(6F));
        assertThat(becaModificada.getNumeroSemestres(), is(1L));
        assertThat(becaModificada.isLimitarCreditos(), is(false));
        assertThat(becaModificada.isPoseeTitulo(), is(true));
        assertThat(becaModificada.isAlgunaBecaParcial(), is(false));
    }

    @Test
    @Transactional
    @Ignore
    public void becaNoParcialSiCreditosParaBecaMayorQueCreditosBecaParcial() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        BecaBuilder becaBuilder = becaConstructor.init();
        becaBuilder.withCreditosParaBeca(10F);
        Beca beca = becaBuilder.build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(1F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.isBecaParcial(), is(false));
    }

    @Test
    @Transactional
    @Ignore
    public void becaNoParcialSiCreditosBecaCompletaMayorQueCreditosParaBecaAndLimitarCreditos() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        BecaBuilder becaBuilder = becaConstructor.init();
        becaBuilder.withCreditosParaBeca(10F);
        Beca beca = becaBuilder.build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(true)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(10F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.isBecaParcial(), is(false));
    }

    @Test
    @Transactional
    public void becaParcialSiCreditosBecaCompletaMayorQueCreditosParaBecaAndNoLimitarCreditosAndCreditosParaBecaMayorQueCreditosBecaParcial() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        BecaBuilder becaBuilder = becaConstructor.init();
        becaBuilder.withCreditosParaBeca(10F);
        Beca beca = becaBuilder.build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosMatriculados(2F)
                .withCreditosConvalidados(3F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(6F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(10F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.isBecaParcial(), is(true));
    }

    @Test
    @Transactional
    @Ignore
    public void matriculaParcialEnPreparaMatriculaParcial() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        BecaBuilder becaBuilder = becaConstructor.init();
        becaBuilder.withCreditosParaBeca(10F);
        Beca beca = becaBuilder.build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosConvalidados(3F)
                .withCreditosMatriculados(2F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(20F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(10F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.isMatriculaParcial(), is(true));
    }

    @Test
    @Transactional
    @Ignore
    public void matriculaNoParcialEnPreparaMatriculaParcial() throws Exception
    {
        BecaConstructor becaConstructor = new BecaConstructor(becaDAO);

        BecaBuilder becaBuilder = becaConstructor.init();
        becaBuilder.withCreditosParaBeca(10F);
        Beca beca = becaBuilder.build();

        new PersonaEstudioBuilder(becaDAO)
                .withPersona(becaConstructor.getPersona())
                .withEstudio(becaConstructor.getEstudio())
                .withCreditosConvalidados(3F)
                .withCreditosMatriculados(2F)
                .withCreditosPendientesConva(4F)
                .withCreditosFueraBeca(5F)
                .withCreditosParaBeca(20F)
                .withNumeroSemestres(1L)
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withLimitarCreditos(false)
                .withPoseeTitulo(true)
                .withCreditosFaltantes(7F)
                .withAlgunaBecaParcial(false)
                .build();

        new MinimoBuilder(becaDAO)
                .withEstudio(becaConstructor.getEstudio())
                .withCursoAcademico(becaConstructor.getCursoAcademico())
                .withCreditosBecaCompleta(30F)
                .withCreditosBecaParcial(2F)
                .build();

        academicosAdapter.actualizaAcademicosCursoActual(beca.getId());

        Beca becaModificada = becaAdapter.getBeca(beca.getId());

        assertThat(becaModificada.isMatriculaParcial(), is(false));
    }
}