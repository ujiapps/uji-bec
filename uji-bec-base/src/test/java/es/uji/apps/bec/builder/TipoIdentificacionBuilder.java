package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.TipoIdentificacion;
import es.uji.commons.db.BaseDAO;

public class TipoIdentificacionBuilder
{
    private BaseDAO baseDAO;
    private TipoIdentificacion tipoIdentificacion;

    public TipoIdentificacionBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        tipoIdentificacion = new TipoIdentificacion();
    }

    public TipoIdentificacionBuilder withId(Long id)
    {
        tipoIdentificacion.setId(id);
        return this;
    }

    public TipoIdentificacionBuilder withNombre(String nombre)
    {
        tipoIdentificacion.setNombre(nombre);
        return this;
    }

    public TipoIdentificacionBuilder withOrden(Long orden)
    {
        tipoIdentificacion.setOrden(1L);
        return this;
    }

    public TipoIdentificacion build()
    {
        return baseDAO.insert(tipoIdentificacion);
    }

}
