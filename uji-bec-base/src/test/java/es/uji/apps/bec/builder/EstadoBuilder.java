package es.uji.apps.bec.builder;

import es.uji.apps.bec.model.Estado;
import es.uji.commons.db.BaseDAO;

public class EstadoBuilder
{
    private BaseDAO baseDAO;
    private Estado estado;

    public EstadoBuilder(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
        estado = new Estado();
    }

    public EstadoBuilder withId(Long id)
    {
        estado.setId(id);
        return this;
    }

    public EstadoBuilder withNombre(String nombre)
    {
        estado.setNombre(nombre);
        return this;
    }

    public EstadoBuilder withOrden(Long orden)
    {
        estado.setOrden(orden);
        return this;
    }

    public Estado build()
    {
        if (baseDAO != null)
        {
            estado = baseDAO.insert(estado);
            baseDAO.flush();
        }

        return estado;
    }
}
