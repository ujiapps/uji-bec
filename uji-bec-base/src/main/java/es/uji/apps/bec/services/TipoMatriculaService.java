package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoMatriculaDAO;
import es.uji.apps.bec.model.TipoMatricula;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoMatriculaService
{
    private TipoMatriculaDAO tipoMatriculaDAO;

    @Autowired
    public TipoMatriculaService(TipoMatriculaDAO tipoMatriculaDAO)
    {
        this.tipoMatriculaDAO = tipoMatriculaDAO;
    }

    public List<UIEntity> getTiposMatriculas()
    {
        List<TipoMatricula> tiposMatriculas = tipoMatriculaDAO.getTiposMatriculas();
        return UIEntity.toUI(tiposMatriculas);
    }

    public UIEntity insertaTipoMatricula(UIEntity entity)
    {
        TipoMatricula tipoMatricula = preparaTipoMatricula(entity);
        if (tipoMatricula.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoMatriculaDAO.insert(tipoMatricula));
    }

    public ResponseMessage actualizaTipoMatricula(UIEntity entity)
    {
        TipoMatricula tipoMatricula = preparaTipoMatricula(entity);

        tipoMatriculaDAO.update(tipoMatricula);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoMatricula(Long tipoMatriculaId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        List<TipoMatricula> tiposMatricula = tipoMatriculaDAO.getTipoMatriculaById(tipoMatriculaId);

        if (tiposMatricula == null || tiposMatricula.isEmpty())
        {
            throw new RegistroNoEncontradoException();
        }

        TipoMatricula tipoMatricula = tiposMatricula.get(0);
        try
        {
            tipoMatriculaDAO.delete(TipoMatricula.class, tipoMatricula.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoMatricula preparaTipoMatricula(UIEntity entity)
    {
        TipoMatricula tipoMatricula = new TipoMatricula();
        tipoMatricula.setId(ParamUtils.parseLong(entity.get("id")));
        tipoMatricula.setNombre(entity.get("nombre"));
        tipoMatricula.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoMatricula;
    }
}
