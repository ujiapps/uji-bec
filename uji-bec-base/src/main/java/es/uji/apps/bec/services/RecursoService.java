package es.uji.apps.bec.services;

import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.model.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

@Path("recurso")
public class RecursoService extends CoreBaseService
{
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertaRecurso(MultivaluedMap<String, String> params)
    {
        Recurso recurso = preparaRecurso(params);

        recurso.insert();

        return UIEntity.toUI(recurso);
    }

    @PUT
    @Path("{recursoId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response guardaRecurso(MultivaluedMap<String, String> params)
            throws BecaDuplicadaException, BecaEnEstadoNoModificableException
    {
        Recurso recurso = preparaRecurso(params);

        recurso.setId(ParamUtils.parseLong(params.getFirst("id")));
        recurso.update();

        return Response.ok(new UIEntity()).build();
    }

    @DELETE
    @Path("{recursoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteRecurso(@PathParam("recursoId") String recursoId)
    {
        Recurso.delete(Long.parseLong(recursoId));
        return new ResponseMessage(true);
    }

    @PUT
    @Path("/beca/{becaId}/recupera")
    @Produces(MediaType.APPLICATION_JSON)
    public Response recuperaInformacionRecurso(@PathParam("becaId") String becaId)
    {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        Recurso recurso = Recurso.getRecursoByBecaId(Long.parseLong(becaId));

        if (recurso == null)
        {
            recurso = new Recurso();
            recuperaDatosRecurso(beca, recurso);
            recurso.setBeca(beca);
            recurso.insert();
        }
        else
        {
            recuperaDatosRecurso(beca, recurso);
            recurso.update();
        }

        return Response.ok(new UIEntity()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{recursoId}/cuantia")
    public List<UIEntity> insertCuantia(UIEntity entity, @PathParam("recursoId") Long recursoId, @QueryParam("tipo") String tipo)
    {
        Long cuantiaId = ParamUtils.parseLong(entity.get("cuantiaId"));

        if (cuantiaId == null || recursoId == null)
        {
            return Collections.singletonList(new UIEntity());
        }
        entity.put("tipo", tipo);
        RecursoDetalle recursoDetalle = preparaRecursoDetalle(recursoId, cuantiaId, entity);

        recursoDetalle.insert();

        entity = UIEntity.toUI(recursoDetalle);

        return Collections.singletonList(entity);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{recursoId}/cuantia")
    public List<UIEntity> getCuantiasByRecursoId(@PathParam("recursoId") Long recursoId, @QueryParam("tipo") String tipo)
    {
        if (recursoId == null || tipo == null)
        {
            return Collections.singletonList(new UIEntity());
        }
        List<RecursoDetalle> listaRecursoDetalles = RecursoDetalle.getRecursoDetallesByRecursoIdAndTipo(recursoId, tipo);
        return UIEntity.toUI(listaRecursoDetalles);
    }

    @DELETE
    @Path("{recursoId}/cuantia/{cuantiaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteCuantia(@PathParam("recursoId") Long recursoId, @PathParam("cuantiaId") Long cuantiaId)
    {
        RecursoDetalle.delete(cuantiaId);

        return new ResponseMessage(true);
    }

    private RecursoDetalle preparaRecursoDetalle(Long recursoId, Long cuantiaId, UIEntity entity)
    {
        RecursoDetalle recursoDetalle = new RecursoDetalle();

        Recurso recurso = new Recurso();
        recurso.setId(recursoId);

        Cuantia cuantia = Cuantia.getCuantiaById(cuantiaId);

        recursoDetalle.setRecurso(recurso);
        recursoDetalle.setCuantia(cuantia);
        Float importe = ParamUtils.parseFloat(entity.get("importe").replace(".", ","));
        recursoDetalle.setImporte(importe);
        recursoDetalle.setTexto(cuantia.getNombre());
        recursoDetalle.setTipo(entity.get("tipo"));
        recursoDetalle.setCodigo(cuantia.getCodigo());

        return recursoDetalle;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/tanda/{tandaId}")
    public List<UIEntity> getRecursosByTandaId(@PathParam("tandaId") String tandaId)
    {
        return UIEntity.toUI(Recurso.getRecursosByTandaId(Long.parseLong(tandaId)));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/beca/{becaId}")
    public UIEntity getRecursoByBecaId(@PathParam("becaId") String becaId)
    {
        return UIEntity.toUI(Recurso.getRecursoByBecaId(Long.parseLong(becaId)));
    }

    private Recurso preparaRecurso(MultivaluedMap<String, String> params)
    {
        Long becaId = ParamUtils.parseLong(params.getFirst("becaId"));
        Recurso recurso = Recurso.getRecursoByBecaId(becaId);

        if (recurso == null)
        {
            recurso = new Recurso();
            Beca beca = Beca.getBecaById(becaId);
            recurso.setBeca(beca);
            recuperaDatosRecurso(beca, recurso);
        }

        recurso.setCursoActAniosBecario(parseIntegerOrNull(params.getFirst("cursoActAniosBecario")));
        recurso.setCursoActCursoPosterior(parseIntegerOrNull(params.getFirst("cursoActCursoPosterior")));
        recurso.setCursoActCambioEstudios(parseIntegerOrNull(params.getFirst("cursoActCambioEstudios")));
        recurso.setEstimacion(parseIntegerOrNull(params.getFirst("estimacion")));
        recurso.setTextoJustificacion(params.getFirst("textoJustificacion"));

        return recurso;
    }

    private void recuperaDatosRecurso(Beca beca, Recurso recurso)
    {
        recurso.setCursoAntCompleto(extraeCursoAntCompleto(beca));
        recurso.setCursoAntCreditosMatr(beca.getCreditosMatriculadosAnt());
        recurso.setCursoAntCreditos(extraeCursoAntCreditos(beca));
        recurso.setCursoActAniosBecario(beca.getBecasConcedidasAnt().intValue());

        recurso.setCursoAntNotaMedia(beca.getNotaMediaAnt());
        recurso.setCursoAntNotaMediaSin(beca.getNotaMediaSinSuspenso());
        recurso.setCursoAntCreditosPend(extraeCursoAntCreditosPend(beca));
        recurso.setCursoAntEstudioRama(extraeCursoAntEstudioRama(beca));

        recurso.setCursoActCursoCompleto(extraeCursoActCursoCompleto(beca));
        recurso.setCursoActCreditosMatr(beca.getCreditosParaBeca());
        recurso.setCursoActCreditos(extraeCursoActCreditos(beca));

        recurso.setMiembrosComputables(extraeMiembrosComputables(beca));
        recurso.setDeducciones(extraeDeducciones(beca));
        recurso.setRentaFamiliarDisponible(extraeRentaFamiliarDisponible(beca));
        recurso.setSuperaUmbrales(extraeSuperaUmbrales(beca));
        recurso.setSuperaUmbralesCuantia(extraeSuperaUmbralesCuantia(beca));
        recurso.setDistancia(extraeDistancia(beca));
        recurso.setDenegacionesActuales(extraeDenegacionesActuales(beca));
        recurso.setAyudasActuales(extraeAyudasActuales(beca));
    }

    private Float extraeCursoAntCreditosPend(Beca beca)
    {
        if (beca.getCreditosMatriculadosAnt() == null)
        {
            return null;
        }

        if (beca.getCreditosSuperadosAnt() == null)
        {
            return null;
        }

        return beca.getCreditosMatriculadosAnt() - beca.getCreditosSuperadosAnt();
    }

    private String extraeAyudasActuales(Beca beca)
    {
        List<CuantiaBeca> listaCuantiasBecas = CuantiaBeca.getCuantiasBecaByBecaId(beca.getId());
        String listaInfoCuantias = "";

        for (CuantiaBeca cuantiaBeca : listaCuantiasBecas)
        {
            String cuantia = String.format("%s %s %n", cuantiaBeca.getCuantia().getCodigo(), cuantiaBeca.getCuantia().getNombre());
            listaInfoCuantias = listaInfoCuantias + cuantia;
        }

        return listaInfoCuantias;
    }

    private String extraeDenegacionesActuales(Beca beca)
    {
        List<BecaDenegacion> listaDenegaciones = BecaDenegacion.getDenegacionesByBecaIdAndOrden(beca.getId(), 1L);
        String listaInfoDenegaciones = "";

        for (BecaDenegacion becaDenegacion : listaDenegaciones)
        {
            String denegacion = String.format("%s%s %s %n", becaDenegacion.getDenegacion().getCausa(), becaDenegacion.getDenegacion().getSubCausa(), becaDenegacion.getDenegacion().getNombre());
            listaInfoDenegaciones = listaInfoDenegaciones + denegacion;
        }

        return listaInfoDenegaciones;
    }

    private String extraeDistancia(Beca beca)
    {
        Domicilio domicilioFamiliar = beca.getDomicilioFamiliar();

        if (domicilioFamiliar == null)
        {
            return null;
        }

        Localidad localidad = domicilioFamiliar.getLocalidad();
        Float distancia = localidad.getDistancia();

        if (distancia == null)
        {
            return null;
        }

        return distancia.toString();
    }

    private Integer extraeSuperaUmbrales(Beca beca)
    {
        Economico economico = beca.getSolicitante().getEconomico();

        if (economico == null)
        {
            return null;
        }

        Integer umbralPatrimonio =  economico.getIndPatrimonio();

        if (umbralPatrimonio == null)
        {
            return null;
        }

        switch (umbralPatrimonio) {
            case 1 : return 0;
            case 0 : return 1;
        }
        return 0;
    }

    private Float extraeSuperaUmbralesCuantia(Beca beca)
    {
        Economico economico = beca.getSolicitante().getEconomico();

        if (economico == null)
        {
            return null;
        }

        Integer umbralPatrimonio =  economico.getIndPatrimonio();

        if (umbralPatrimonio == null)
        {
            return null;
        }
        return economico.getCapitalMobiliario();

    }

    private String extraeCursoAntEstudioRama(Beca beca)
    {
        String estudioMec = beca.getEstudioAnt();
        if (estudioMec == null)
        {
            return null;
        }

        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();
        Long cursoAcademicoId = cursoAcademico.getId();

        Long estudioId = Diccionario.getValor(cursoAcademicoId, "Estudio", beca.getConvocatoria().getOrganismo().getId(), beca.getCodigoEstudioAnt());
        if (estudioId == null)
        {
            return null;
        }

        Estudio estudio = Estudio.getEstudioById(estudioId);
        if (estudio == null)
        {
            return null;
        }
        String rama = estudio.getRama();

        switch (rama) {
            case "SO" : return "SJ";
            case "TE" : return "IA";
            case "EX" : return "CC";
            case "HU" : return "AH";
            case "SA" : return "CS";
            default: return null;
        }
    }

    private Integer extraeCursoActCursoCompleto(Beca beca)
    {
        Boolean matriculaParcial = beca.isMatriculaParcial();

        if (matriculaParcial)
        {
            return 0;
        }
        return 1;
    }


    private Float extraeCursoAntCreditos(Beca beca)
    {
        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();
        Long cursoAcademicoId = cursoAcademico.getId();
        Long estudio = Diccionario.getValor(cursoAcademicoId, "Estudio", beca.getConvocatoria().getOrganismo().getId(), beca.getCodigoEstudioAnt());
        if (estudio == null)
        {
            estudio = DiccionarioExcepciones.getValor(cursoAcademicoId, "Estudio", beca.getConvocatoria().getOrganismo().getId(), beca.getCodigoEstudioAnt());
        }

        if (estudio == null)
        {
            return null;
        }
        Minimo minimo = Minimo.getMinimosByEstudioId(estudio, cursoAcademico.getId());

        return minimo.getCreditosBecaCompleta();
    }

    private Float extraeCursoActCreditos(Beca beca)
    {
        Estudio estudio = beca.getEstudio();
        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();
        Minimo minimo = Minimo.getMinimosByEstudioId(estudio.getId(), cursoAcademico.getId());
        return minimo.getCreditosBecaCompleta();
    }

    private Float extraeRentaFamiliarDisponible(Beca beca)
    {
        Economico economico = beca.getSolicitante().getEconomico();
        if (economico == null)
        {
            return null;
        }

        return economico.getRenta();
    }


    private Float extraeDeducciones(Beca beca)
    {
        Economico economico = beca.getSolicitante().getEconomico();
        if (economico == null)
        {
            return null;
        }

        return economico.getDeducciones();
    }

    private Integer extraeMiembrosComputables(Beca beca)
    {
        Long miembrosComputables = beca.getSolicitante().getNumeroMiembrosComputables();
        if (miembrosComputables == null)
        {
            return null;
        }
        return miembrosComputables.intValue();
    }

    private Integer parseIntegerOrNull(String cadena)
    {
        if (cadena.isEmpty())
        {
            return null;
        }
        return Integer.parseInt(cadena);
    }

    private Integer extraeCursoAntCompleto(Beca beca)
    {
        Float creditosParaBeca = beca.getCreditosMatriculadosAnt();
        if (creditosParaBeca == null)
        {
            return null;
        }
        if (Float.compare(creditosParaBeca, 0) <= 0)
        {
            return null;
        }
        if (creditosParaBeca >= 60) {
            return 1;
        }

        return 0;
    }
}
