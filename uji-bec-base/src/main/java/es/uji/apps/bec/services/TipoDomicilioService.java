package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoDomicilioDAO;
import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoDomicilioService extends CoreBaseService
{
    private TipoDomicilioDAO tipoDomicilioDAO;

    @Autowired
    public TipoDomicilioService(TipoDomicilioDAO tipoDomicilioDAO)
    {
        this.tipoDomicilioDAO = tipoDomicilioDAO;
    }

    public List<UIEntity> getTiposDomicilios()
    {
        List<TipoDomicilio> tiposDomicilios = tipoDomicilioDAO.getTiposDomicilios();
        return UIEntity.toUI(tiposDomicilios);
    }

    public UIEntity insertaTipoDomicilio(UIEntity entity)
    {
        TipoDomicilio tipoDomicilio = preparaTipoDomicilio(entity);
        if (tipoDomicilio.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoDomicilioDAO.insert(tipoDomicilio));
    }

    public ResponseMessage actualizaTipoDomicilio(UIEntity entity)
    {
        TipoDomicilio tipoDomicilio = preparaTipoDomicilio(entity);

        tipoDomicilioDAO.update(tipoDomicilio);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoDomicilio(Long tipoDomicilioId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        List<TipoDomicilio> tiposDomicilio = tipoDomicilioDAO.getTipoDomicilioById(tipoDomicilioId);

        if (listaVacia(tiposDomicilio))
        {
            throw new RegistroNoEncontradoException();
        }

        TipoDomicilio tipoDomicilio = tiposDomicilio.get(0);
        try
        {
            tipoDomicilioDAO.delete(TipoDomicilio.class, tipoDomicilio.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoDomicilio preparaTipoDomicilio(UIEntity entity)
    {
        TipoDomicilio tipoDomicilio = new TipoDomicilio();
        tipoDomicilio.setId(ParamUtils.parseLong(entity.get("id")));
        tipoDomicilio.setNombre(entity.get("nombre"));
        tipoDomicilio.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoDomicilio;
    }

    private boolean listaVacia(List<TipoDomicilio> listaTipoDomicilio)
    {
        return listaTipoDomicilio == null || listaTipoDomicilio.isEmpty();
    }
}
