package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.EstadoCivilService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("estadocivil")
public class EstadoCivilResource extends CoreBaseService
{
    @InjectParam
    private EstadoCivilService estadoCivilService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstadosCiviles()
    {
        return estadoCivilService.getEstadosCiviles();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaEstadoCivil(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return estadoCivilService.insertaEstadoCivil(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaEstadoCivil(@PathParam("id") Long estadoCivilId,
            UIEntity entity)
    {
        return estadoCivilService.actualizaEstadoCivil(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraEstadoCivil(@PathParam("id") Long estadoCivilId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return estadoCivilService.borraEstadoCivil(estadoCivilId);
    }
}
