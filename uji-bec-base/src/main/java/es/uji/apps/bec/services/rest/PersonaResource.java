package es.uji.apps.bec.services.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("persona")
public class PersonaResource extends CoreBaseService
{
    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPersonaByDni(@QueryParam("dni") String dni)
    {
        return personaService.getPersonaByDni(dni);
    }
}
