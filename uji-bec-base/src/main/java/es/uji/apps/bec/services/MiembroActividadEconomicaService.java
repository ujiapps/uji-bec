package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.ActividadEconomicaDAO;
import es.uji.apps.bec.dao.dao.MiembroDAO;
import es.uji.apps.bec.exceptions.ActividadEconomicaException;
import es.uji.apps.bec.model.ActividadEconomica;
import es.uji.apps.bec.model.Miembro;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MiembroActividadEconomicaService
{
    private ActividadEconomicaDAO actividadEconomicaDAO;
    private MiembroDAO miembroDAO;

    @Autowired
    public MiembroActividadEconomicaService(MiembroDAO miembroDAO, ActividadEconomicaDAO actividadEconomicaDAO)
    {
        this.miembroDAO = miembroDAO;
        this.actividadEconomicaDAO = actividadEconomicaDAO;
    }

    public List<ActividadEconomica> getActividadesEconomicasByMiembroId(Long miembroId)
    {
        return actividadEconomicaDAO.getActividadesEconomicasByMiembroId(miembroId);
    }

    public UIEntity insertActividadEconomica(Long miembroId, UIEntity entity)
    {
        ActividadEconomica actividadEconomica = new ActividadEconomica();

        Miembro miembro = miembroDAO.get(Miembro.class, miembroId).get(0);
        actividadEconomica.setMiembro(miembro);
        preparaActividadEconomica(actividadEconomica, entity);

        actividadEconomicaDAO.insert(actividadEconomica);
        return UIEntity.toUI(actividadEconomica);
    }

    public UIEntity updateActividadEconomica(Long actividadEconomicaId, UIEntity entity)
    {
        ActividadEconomica actividadEconomica = actividadEconomicaDAO.get(ActividadEconomica.class, actividadEconomicaId).get(0);

        preparaActividadEconomica(actividadEconomica, entity);

        actividadEconomicaDAO.updateActividadEconomica(actividadEconomica);

        return entity;
    }

    public void deleteActividadEconomica(Long actividadEconomicaId) throws ActividadEconomicaException
    {
        try
        {
            actividadEconomicaDAO.delete(ActividadEconomica.class, actividadEconomicaId);
        }
        catch (Exception e)
        {
            throw new ActividadEconomicaException("No s'ha pogut esborrar el registre");
        }
    }

    private void preparaActividadEconomica(ActividadEconomica actividadEconomica, UIEntity entity)
    {
        actividadEconomica.setCifSociedad(entity.get("cifSociedad"));
        actividadEconomica.setImpParticipacion(Float.valueOf(entity.get("impParticipacion")));
        actividadEconomica.setPorcParticipacion(Float.valueOf(entity.get("porcParticipacion")));
        actividadEconomica.setNumSociedad(Integer.parseInt(entity.get("numSociedad")));
    }
}
