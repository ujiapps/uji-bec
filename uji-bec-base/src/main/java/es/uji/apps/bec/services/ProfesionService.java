package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.ProfesionDAO;
import es.uji.apps.bec.model.Profesion;
import es.uji.commons.rest.UIEntity;

@Service
public class ProfesionService
{
    private ProfesionDAO profesionDAO;

    @Autowired
    public ProfesionService(ProfesionDAO profesionDAO)
    {
        this.profesionDAO = profesionDAO;
    }

    public List<UIEntity> getProfesiones()
    {
        List<Profesion> profesiones = profesionDAO.getProfesiones();
        return UIEntity.toUI(profesiones);
    }
}
