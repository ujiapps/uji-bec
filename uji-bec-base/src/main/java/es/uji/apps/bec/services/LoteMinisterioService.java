package es.uji.apps.bec.services;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.bec.model.LoteMinisterio;
import es.uji.commons.rest.UIEntity;

@Path("loteministerio")
public class LoteMinisterioService
{

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLotesMinisterio(
            @QueryParam("queryParamMap") Map<String, String> queryParamMap)
    {
        List<LoteMinisterio> listaLotes = LoteMinisterio.getLotes(queryParamMap);

        return UIEntity.toUI(listaLotes);
    }
}
