package es.uji.apps.bec.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.model.TituloPosee;
import es.uji.apps.bec.services.TituloPoseeService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tituloPosee")
public class TitulosPoseeResource extends CoreBaseService
{
    @InjectParam
    private TituloPoseeService tituloPoseeService;

    @GET
    @Produces(MediaType.APPLICATION_JSON )
    public List<UIEntity> getTitulosPosee(@QueryParam("solicitanteId") String solicitanteId)
    {
        List<TituloPosee> listaTitulosPosee = tituloPoseeService.getTitulosPoseeBySolicitante(Long.parseLong(solicitanteId));

        return toUI(listaTitulosPosee);
    }

    private List<UIEntity> toUI(List<TituloPosee> listaTitulosPosee) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (TituloPosee tituloPosee: listaTitulosPosee) {
            UIEntity entity = UIEntity.toUI(tituloPosee);
            entity.put("solicitanteId", tituloPosee.getSolicitante().getId());
            listaUI.add(entity);
        }

        return listaUI;
    }
}
