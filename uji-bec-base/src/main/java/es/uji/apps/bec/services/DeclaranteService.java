package es.uji.apps.bec.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import es.uji.apps.bec.exceptions.BecaDelMismoOrganismoYaExisteException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.DemasiadasBecasException;
import es.uji.apps.bec.model.Declarante;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("declarante")
public class DeclaranteService extends CoreBaseService
{
    private List<UIEntity> modelToUI(List<Declarante> listaDeclarantes)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Declarante declarante : listaDeclarantes)
        {
            result.add(UIEntity.toUI(declarante));
        }
        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDeclaranteById(@QueryParam("solicitanteId") String solicitanteId)
    {
        List<UIEntity> listaEntidades = modelToUI(Declarante.getDeclaranteBySolicitanteId(Long
                .parseLong(solicitanteId)));
        return listaEntidades;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertarDeclarante(MultivaluedMap<String, String> params)
            throws DemasiadasBecasException, BecaDuplicadaException, BecaSinEstudioException,
            BecaDelMismoOrganismoYaExisteException, ParseException
    {
        String solicitanteId = params.getFirst("solicitanteId");
        String declarante = params.getFirst("declarante");
        String indicadorRentasExtranjeroString = params.getFirst("indicadorRentasExtranjero");
        String rentasExtranjeroString = params.getFirst("rentasExtranjero");
        String localidadDeclarante = params.getFirst("localidadDeclarante");
        String fechaDeclaranteString = params.getFirst("fechaDeclarante");
        String indicadorUltCursoString = params.getFirst("indicadorUltCurso");
        String indicadorCursoPendienteString = params.getFirst("indicadorCursoPendiente");
        String indicadorCursoAnteriorString = params.getFirst("indicadorCursoAnterior");
        String indicadorIndependienteString = params.getFirst("indicadorIndependiente");
        String indicadorInemString = params.getFirst("indicadorInem");
        String empleador = params.getFirst("empleador");
        String tipoIdentificacionString = params.getFirst("tipoIdentificacion");
        String identificacion = params.getFirst("identificacion");
        String ingresosAnualesString = params.getFirst("ingresosAnuales");
        String codigoParentesco = params.getFirst("codigoParentesco");
        String decIdentificacionConsen = params.getFirst("decIdentificacionConsen");
        String decTipoIdentificacionConsenIdString = params.getFirst("decTipoIdentificacionConsenId");
        String decCodParentescoConsenIdString = params.getFirst("decCodParentescoConsenId");

        ParamUtils.checkNotNull(solicitanteId, declarante, localidadDeclarante, fechaDeclaranteString);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDeclarante = df.parse(fechaDeclaranteString);

        Integer indicadorRentasExtranjero = null;
        if (indicadorRentasExtranjeroString != null && !indicadorRentasExtranjeroString.isEmpty())
        {
            indicadorRentasExtranjero = Integer.parseInt(indicadorRentasExtranjeroString);
        }

        Integer indicadorUltCurso = null;
        if (indicadorUltCursoString != null && !indicadorUltCursoString.isEmpty())
        {
            indicadorUltCurso = Integer.parseInt(indicadorUltCursoString);
        }

        Integer indicadorCursoPendiente = null;
        if (indicadorCursoPendienteString != null && !indicadorCursoPendienteString.isEmpty())
        {
            indicadorCursoPendiente = Integer.parseInt(indicadorCursoPendienteString);
        }

        Integer indicadorCursoAnterior = null;
        if (indicadorCursoAnteriorString != null && !indicadorCursoAnteriorString.isEmpty())
        {
            indicadorCursoAnterior = Integer.parseInt(indicadorCursoAnteriorString);
        }

        Integer indicadorIndependiente = null;
        if (indicadorIndependienteString != null && !indicadorIndependienteString.isEmpty())
        {
            indicadorIndependiente = Integer.parseInt(indicadorIndependienteString);
        }

        Integer indicadorInem = null;
        if (indicadorInemString != null && !indicadorInemString.isEmpty())
        {
            indicadorInem = Integer.parseInt(indicadorInemString);
        }

        Float rentasExtranjero = null;
        if (rentasExtranjeroString != null && !rentasExtranjeroString.isEmpty())
        {
            rentasExtranjero = ParamUtils.parseFloat(rentasExtranjeroString);
        }

        Long tipoIdentificacion = null;
        if (tipoIdentificacionString != null && !tipoIdentificacionString.isEmpty())
        {
            tipoIdentificacion = Long.parseLong(tipoIdentificacionString);
        }

        Long decTipoIdentificacionConsenId = null;
        if (decTipoIdentificacionConsenIdString != null && !decTipoIdentificacionConsenIdString.isEmpty())
        {
            decTipoIdentificacionConsenId = Long.parseLong(decTipoIdentificacionConsenIdString);
        }

        Long decCodParentescoConsenId = null;
        if (decCodParentescoConsenIdString != null && !decCodParentescoConsenIdString.isEmpty())
        {
            decCodParentescoConsenId = Long.parseLong(decCodParentescoConsenIdString);
        }

        Float ingresosAnuales = null;
        if (ingresosAnualesString != null && !ingresosAnualesString.isEmpty())
        {
            ingresosAnuales = ParamUtils.parseFloat(ingresosAnualesString);
        }

        Declarante registroDeclarante = Declarante.insertRegistro(Long.parseLong(solicitanteId),
                declarante, indicadorRentasExtranjero, rentasExtranjero, localidadDeclarante,
                fechaDeclarante, indicadorUltCurso, indicadorCursoPendiente,
                indicadorCursoAnterior, indicadorIndependiente, indicadorInem, empleador,
                tipoIdentificacion, identificacion, ingresosAnuales, codigoParentesco,
                decIdentificacionConsen, decTipoIdentificacionConsenId, decCodParentescoConsenId);

        return UIEntity.toUI(registroDeclarante);

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("{declaranteId}")
    public UIEntity modificarDeclarante(@PathParam("declaranteId") String declaranteId,
            MultivaluedMap<String, String> params) throws DemasiadasBecasException,
            BecaDuplicadaException, BecaSinEstudioException, BecaDelMismoOrganismoYaExisteException,
            ParseException
    {
        String solicitanteId = params.getFirst("solicitanteId");
        String declarante = params.getFirst("declarante");
        String indicadorRentasExtranjeroString = params.getFirst("indicadorRentasExtranjero");
        String rentasExtranjeroString = params.getFirst("rentasExtranjero");
        String localidadDeclarante = params.getFirst("localidadDeclarante");
        String fechaDeclaranteString = params.getFirst("fechaDeclarante");
        String indicadorUltCursoString = params.getFirst("indicadorUltCurso");
        String indicadorCursoPendienteString = params.getFirst("indicadorCursoPendiente");
        String indicadorCursoAnteriorString = params.getFirst("indicadorCursoAnterior");
        String indicadorIndependienteString = params.getFirst("indicadorIndependiente");
        String indicadorInemString = params.getFirst("indicadorInem");
        String empleador = params.getFirst("empleador");
        String tipoIdentificacionString = params.getFirst("tipoIdentificacionId");
        String identificacion = params.getFirst("identificacion");
        String ingresosAnualesString = params.getFirst("ingresosAnuales");
        String codigoParentesco = params.getFirst("codigoParentesco");
        String decIdentificacionConsen = params.getFirst("decIdentificacionConsen");
        String decTipoIdentificacionConsenIdString = params.getFirst("decTipoIdentificacionConsenId");
        String decCodParentescoConsenIdString = params.getFirst("decCodParentescoConsenId");

        ParamUtils.checkNotNull(solicitanteId, declarante, localidadDeclarante, fechaDeclaranteString);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDeclarante = df.parse(fechaDeclaranteString);

        Integer indicadorRentasExtranjero = null;
        if (indicadorRentasExtranjeroString != null && !indicadorRentasExtranjeroString.isEmpty())
        {
            indicadorRentasExtranjero = Integer.parseInt(indicadorRentasExtranjeroString);
        }

        Integer indicadorUltCurso = null;
        if (indicadorUltCursoString != null && !indicadorUltCursoString.isEmpty())
        {
            indicadorUltCurso = Integer.parseInt(indicadorUltCursoString);
        }

        Integer indicadorCursoPendiente = null;
        if (indicadorCursoPendienteString != null && !indicadorCursoPendienteString.isEmpty())
        {
            indicadorCursoPendiente = Integer.parseInt(indicadorCursoPendienteString);
        }

        Integer indicadorCursoAnterior = null;
        if (indicadorCursoAnteriorString != null && !indicadorCursoAnteriorString.isEmpty())
        {
            indicadorCursoAnterior = Integer.parseInt(indicadorCursoAnteriorString);
        }

        Integer indicadorIndependiente = null;
        if (indicadorIndependienteString != null && !indicadorIndependienteString.isEmpty())
        {
            indicadorIndependiente = Integer.parseInt(indicadorIndependienteString);
        }

        Integer indicadorInem = null;
        if (indicadorInemString != null && !indicadorInemString.isEmpty())
        {
            indicadorInem = Integer.parseInt(indicadorInemString);
        }

        Float rentasExtranjero = null;
        if (rentasExtranjeroString != null && !rentasExtranjeroString.isEmpty())
        {
            rentasExtranjero = ParamUtils.parseFloat(rentasExtranjeroString);
        }

        Long tipoIdentificacionId = null;
        if (tipoIdentificacionString != null && !tipoIdentificacionString.isEmpty())
        {
            tipoIdentificacionId = Long.parseLong(tipoIdentificacionString);
        }

        Float ingresosAnuales = null;
        if (ingresosAnualesString != null && !ingresosAnualesString.isEmpty())
        {
            ingresosAnuales = ParamUtils.parseFloat(ingresosAnualesString);
        }

        Long decTipoIdentificacionConsenId = null;
        if (decTipoIdentificacionConsenIdString != null && !decTipoIdentificacionConsenIdString.isEmpty())
        {
            decTipoIdentificacionConsenId = Long.parseLong(decTipoIdentificacionConsenIdString);
        }

        Long decCodParentescoConsenId = null;
        if (decCodParentescoConsenIdString != null && !decCodParentescoConsenIdString.isEmpty())
        {
            decCodParentescoConsenId = Long.parseLong(decCodParentescoConsenIdString);
        }

        Declarante registroDeclarante = Declarante.getDeclaranteById(Long.parseLong(declaranteId)).get(0);

        registroDeclarante.updateRegistro(declarante, indicadorRentasExtranjero, rentasExtranjero,
                localidadDeclarante, fechaDeclarante, indicadorUltCurso, indicadorCursoPendiente,
                indicadorCursoAnterior, indicadorIndependiente, indicadorInem, empleador,
                tipoIdentificacionId, identificacion, ingresosAnuales, codigoParentesco,
                decIdentificacionConsen, decTipoIdentificacionConsenId, decCodParentescoConsenId);

        return UIEntity.toUI(registroDeclarante);

    }
}
