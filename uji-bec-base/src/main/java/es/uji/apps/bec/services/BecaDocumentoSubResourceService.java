package es.uji.apps.bec.services;

import com.mysema.query.Tuple;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Documento;
import es.uji.apps.bec.model.QDocumento;
import es.uji.apps.bec.model.TipoDocumento;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BecaDocumentoSubResourceService
{
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDocumentos()
    {
        List<Tuple> listaDocumentos= Documento.getDocumentosByBecaId(Long.parseLong(becaId));

        return tuplaDocumentosToUI(listaDocumentos);
    }

    private List<UIEntity> tuplaDocumentosToUI(List<Tuple> listaDocumentos) {
        QDocumento qDocumento = QDocumento.documento;

        List<UIEntity> listaUI = new ArrayList<>();

        for (Tuple tupla: listaDocumentos) {
            UIEntity ui = new UIEntity();

            ui.setBaseClass("Documento");
            ui.put("tipoDocumentoId", tupla.get(qDocumento.tipoDocumento.id));
            ui.put("fechaActualiza", tupla.get(qDocumento.fechaActualiza));
            ui.put("becaId", tupla.get(qDocumento.beca.id));
            ui.put("validado", tupla.get(qDocumento.validado));
            ui.put("id", tupla.get(qDocumento.id));
            ui.put("mimeType", tupla.get(qDocumento.mimeType));
            ui.put("nombre", tupla.get(qDocumento.nombre));
            ui.put("fechaSolicita", tupla.get(qDocumento.fechaSolicita));
            listaUI.add(ui);
        }

        return listaUI;
    }

    @GET
    @Path("{id}/binario")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response publicacion(@PathParam("id") Long documentoId)
    {
        Documento documento = Documento.getDocumentoById(documentoId);

        if (documento.getFichero() == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(documento.getFichero())
                .type(documento.getMimeType())
                .header("Content-Disposition", documento.getMimeType() + "; filename=\"" + documento.getNombre() + "\"")
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaDocumento(UIEntity entity)
    {
        Documento documento = new Documento();
        preparaDocumento(entity, documento);

        if (documento.getTipoDocumento() != null)
        {
            return UIEntity.toUI(documento.insertaDocumento());

        }
        return UIEntity.toUI(new Documento());
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateTanda(@PathParam("id") Long id, UIEntity entity)
    {
        Documento documento = Documento.getDocumentoById(id);

        preparaDocumento(entity, documento);
        documento.updateDocumento();

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long documentoId)
    {
        Documento documento = Documento.getDocumentoById(documentoId);
        documento.borrarDocumento();
        return new ResponseMessage(true);
    }


    private void preparaDocumento(UIEntity entity, Documento documento)
    {
        documento.setBeca(Beca.getBecaById(Long.parseLong(entity.get("becaId"))));

        if (entity.get("tipoDocumentoId") != null)
        {
            documento.setTipoDocumento(TipoDocumento.getTipoDocumentoById(Long.parseLong(entity.get("tipoDocumentoId"))));
        }

        documento.setNombre(entity.get("nombre"));

        if (entity.get("fechaSolicita") == null)
        {
            documento.setFechaSolicita(null);
        }
        else
        {
            documento.setFechaSolicita(entity.getDate("fechaSolicita"));
        }

        if (entity.get("fechaActualiza") == null)
        {
            documento.setFechaActualiza(null);
        }
        else
        {
            documento.setFechaActualiza(entity.getDate("fechaActualiza"));
        }

        documento.setValidado(Boolean.parseBoolean(entity.get("validado")));
    }
}