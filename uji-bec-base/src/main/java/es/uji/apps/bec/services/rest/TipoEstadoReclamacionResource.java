package es.uji.apps.bec.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.TipoEstadoReclamacion;
import es.uji.apps.bec.services.TipoDocumentoService;
import es.uji.apps.bec.services.TipoEstadoReclamacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

@Path("tipoestadoreclamacion")
public class TipoEstadoReclamacionResource extends CoreBaseService
{
    @InjectParam
    private TipoEstadoReclamacionService tipoEstadoReclamacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEstadosReclamacion()
    {
        return tipoEstadoReclamacionService.getTiposEstadosReclamaciones();
    }
}
