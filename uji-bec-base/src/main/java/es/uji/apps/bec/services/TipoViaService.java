package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoViaDAO;
import es.uji.apps.bec.model.TipoVia;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoViaService
{
    private TipoViaDAO tipoViaDAO;

    @Autowired
    public TipoViaService(TipoViaDAO tipoViaDAO)
    {
        this.tipoViaDAO = tipoViaDAO;
    }

    public List<UIEntity> getTiposVias()
    {
        List<TipoVia> tiposVias = tipoViaDAO.getTiposVias();
        return UIEntity.toUI(tiposVias);
    }

    public UIEntity insertaTipoVia(UIEntity entity)
    {
        TipoVia tipoVia = preparaTipoVia(entity);
        if (tipoVia.getNombre() == null)
        {
            return new UIEntity();
        }
        return UIEntity.toUI(tipoViaDAO.insert(tipoVia));
    }

    public ResponseMessage actualizaTipoVia(UIEntity entity)
    {
        TipoVia tipoVia = preparaTipoVia(entity);
        tipoViaDAO.update(tipoVia);
        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoVia(Long tipoViaId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        TipoVia tipoVia = tipoViaDAO.getTipoViaById(tipoViaId);

        if (tipoVia == null)
        {
            throw new RegistroNoEncontradoException();
        }
        try
        {
            tipoViaDAO.deleteTipoVia(tipoVia.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }
        return new ResponseMessage(true);
    }

    private TipoVia preparaTipoVia(UIEntity entity)
    {
        TipoVia tipoVia = new TipoVia();
        tipoVia.setId(ParamUtils.parseLong(entity.get("id")));
        tipoVia.setNombre(entity.get("nombre"));
        tipoVia.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoVia;
    }
}
