package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.ConvocatoriaService;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("convocatoria")
public class ConvocatoriaResource extends CoreBaseService
{
    @InjectParam
    private ConvocatoriaService convocatoriaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatorias()
    {
        return convocatoriaService.getConvocatorias();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/activa")
    public List<UIEntity> getConvocatoriasActivas()
    {
        return convocatoriaService.getConvocatoriasActivas();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaConvocatoria(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        String convocatoriaId = entity.get("id");
        if (!ParamUtils.isNotNull(convocatoriaId))
        {
            return new UIEntity();
        }

        return convocatoriaService.insertaConvocatoria(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long convocatoriaId)
            throws RegistroNoEncontradoException, RegistroConHijosException, GeneralBECException
    {
        return convocatoriaService.borraConvocatoria(convocatoriaId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaConvocatoria(@PathParam("id") Long convocatoriaId,
            UIEntity entity) throws GeneralBECException, ParseException
    {
        return convocatoriaService.actualizaConvocatoria(entity);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ministerio")
    public List<UIEntity> getConvocatoriasMinisterio()
    {
        return convocatoriaService.getConvocatoriasMinisterio();
    }

    @GET
    @Path("{convocatoriaId}/estados")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<Resumen> getResumenEstadosPorConvocatoria(@PathParam("convocatoriaId") Long convocatoriaId)
    {
        return convocatoriaService.getResumenEstadosPorConvocatoria(convocatoriaId);
    }

    @GET
    @Path("{convocatoriaId}/procesos")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<Resumen> getResumenProcesosPorConvocatoria(@PathParam("convocatoriaId") Long convocatoriaId)
    {
        return convocatoriaService.getResumenProcesosPorConvocatoria(convocatoriaId);
    }
}
