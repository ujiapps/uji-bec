package es.uji.apps.bec.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.services.CursoAcademicoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("cursoacademico")
public class CursoAcademicoResource extends CoreBaseService
{
    @InjectParam
    private CursoAcademicoService cursoAcademicoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursos(@QueryParam("inicial") String cursoAcademicoId)
    {
        List<CursoAcademico> listaCursosAcademicos = new ArrayList<CursoAcademico>();
        if (ParamUtils.isNotNull(cursoAcademicoId))
        {
            listaCursosAcademicos = cursoAcademicoService.getCursosMayoresOIgualesQue(Long
                    .parseLong(cursoAcademicoId));
        }
        else
        {
            listaCursosAcademicos = cursoAcademicoService.getCursos();
        }

        return UIEntity.toUI(listaCursosAcademicos);
    }

    @GET
    @Path("activo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursoActivo()
    {
        return cursoAcademicoService.getCursoActivo();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertCursoAcademico(UIEntity entity) throws GeneralBECException,
            ParseException
    {

        if (!ParamUtils.isNotNull(entity.get("id")))
        {
            return new UIEntity();
        }

        UIEntity ui = cursoAcademicoService.insertCursoAcademico(Long.parseLong(entity.get("id")));
        return ui;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long cursoAcademicoId)
            throws RegistroNoEncontradoException, RegistroConHijosException, GeneralBECException
    {
        return cursoAcademicoService.delete(cursoAcademicoId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage updateCursoAcademico(@PathParam("id") Long cursoAcademicoId,
            UIEntity entity) throws GeneralBECException, ParseException
    {
        if (!ParamUtils.parseLong(entity.get("id")).equals(cursoAcademicoId))
        {
            throw new GeneralBECException(
                    "No pots modificar el any del curs acadèmic, crea un de nou.");
        }

        return cursoAcademicoService.activaCursoAcademico(cursoAcademicoId);
    }

}
