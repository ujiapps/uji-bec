package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.PersonaEstudioDAO;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.model.PersonaEstudio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.commons.rest.UIEntity;

@Service
public class PersonaEstudioService
{
    private PersonaEstudioDAO personaEstudioDAO;

    @Autowired
    public PersonaEstudioService(PersonaEstudioDAO personaEstudioDAO)
    {
        this.personaEstudioDAO = personaEstudioDAO;
    }

    public PersonaEstudio getPersonaEstudioByBecaId(Long becaId) throws BecaSinEstudioException {
        return personaEstudioDAO.getPersonaEstudioByBecaId(becaId);
    }
}
