package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.exceptions.BecaDenegacionDuplicadaException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.Denegacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

public class BecaDenegacionSubResourceService
{
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDenegacionesByBeca()
    {
        List<BecaDenegacion> listaDenegacionesBecas = BecaDenegacion.getDenegacionesByBecaId(Long
                .parseLong(becaId));

        return preparaListaUIEntityDenegaciones(listaDenegacionesBecas);
    }

    private List<UIEntity> preparaListaUIEntityDenegaciones(
            List<BecaDenegacion> listaDenegacionesBecas)
    {
        ArrayList<UIEntity> listaUI = new ArrayList<UIEntity>();
        for (BecaDenegacion becaDenegacion : listaDenegacionesBecas)
        {

            UIEntity ui = UIEntity.toUI(becaDenegacion);
            Denegacion denegacion = becaDenegacion.getDenegacion();

            ui.put("causa", denegacion.getCausa());
            ui.put("subCausa", denegacion.getSubCausa());
            listaUI.add(ui);
        }
        return listaUI;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertDenegacion(UIEntity entity)
            throws BecaDenegacionDuplicadaException, BecaEnEstadoNoModificableException
    {
        Long becaId = ParamUtils.parseLong(entity.get("becaId"));
        compruebaSiBecaEsModificable(becaId);

        Long denegacionId = ParamUtils.parseLong(entity.get("denegacionId"));
        Long ordenDenegacion = ParamUtils.parseLong(entity.get("ordenDenegacion"));
        if (denegacionId == null || becaId == null || ordenDenegacion == null)
        {
            return Collections.singletonList(new UIEntity());
        }
        BecaDenegacion becaDenegacion = new BecaDenegacion();
        becaDenegacion.setBeca(Beca.getBecaById(becaId));
        becaDenegacion.setDenegacion(Denegacion.getDenegacionById(denegacionId));
        becaDenegacion.setOrdenDenegacion(ordenDenegacion);
        becaDenegacion.insert();

        entity = UIEntity.toUI(becaDenegacion);
        entity.put("causa", becaDenegacion.getDenegacion().getCausa());
        entity.put("subCausa", becaDenegacion.getDenegacion().getSubCausa());
        return Collections.singletonList(entity);
    }

    private void compruebaSiBecaEsModificable(Long becaId) throws BecaEnEstadoNoModificableException
    {
        Beca beca = Beca.getBecaById(becaId);

        if (!beca.esModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }
    }

    @DELETE
    @Path("{denegacionId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public ResponseMessage deleteDenegacion(@PathParam("denegacionId") String denegacionId)
            throws BecaEnEstadoNoModificableException
    {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (!beca.esModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }

        BecaDenegacion.borraDenegacion(Long.parseLong(denegacionId));

        return new ResponseMessage(true);
    }
}