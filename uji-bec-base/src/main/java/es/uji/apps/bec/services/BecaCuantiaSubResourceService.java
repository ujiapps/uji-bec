package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.bec.exceptions.BecaCuantiaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaBeca;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

public class BecaCuantiaSubResourceService {
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCuantias() {
        List<CuantiaBeca> listaCuantiasBecas = CuantiaBeca.getCuantiasBecaByBecaId(Long
                .parseLong(becaId));

        return preparaListaUIEntityCuantias(listaCuantiasBecas);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertCuantia(UIEntity entity) throws BecaCuantiaDuplicadaException,
            BecaEnEstadoNoModificableException {
        Long becaId = ParamUtils.parseLong(entity.get("becaId"));
        Beca beca = Beca.getBecaById(becaId);

        if (!beca.esModificable()) {
            throw new BecaEnEstadoNoModificableException();
        }

        Long cuantiaId = ParamUtils.parseLong(entity.get("cuantiaId"));
        if (cuantiaId == null || becaId == null) {
            return Collections.singletonList(new UIEntity());
        }

        CuantiaBeca cuantiaBeca = preparaCuantiaBeca(becaId, cuantiaId, entity);

        cuantiaBeca.insert();

        entity = UIEntity.toUI(cuantiaBeca);
        entity.put("codigo", cuantiaBeca.getCuantia().getCodigo());

        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{cuantiaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteCuantia(@PathParam("cuantiaId") String cuantiaId)
            throws BecaEnEstadoNoModificableException {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (!beca.esModificable()) {
            throw new BecaEnEstadoNoModificableException();
        }

        CuantiaBeca.delete(Long.parseLong(cuantiaId));

        return new ResponseMessage(true);
    }

    private List<UIEntity> preparaListaUIEntityCuantias(List<CuantiaBeca> listaCuantiasBecas) {
        List<UIEntity> listaUIEntity = new ArrayList<UIEntity>();
        for (CuantiaBeca cuantiaBeca : listaCuantiasBecas) {
            UIEntity entity = UIEntity.toUI(cuantiaBeca);
            entity.put("codigo", cuantiaBeca.getCuantia().getCodigo());
            entity.put("nombre", cuantiaBeca.getCuantia().getNombre());
            listaUIEntity.add(entity);
        }
        return listaUIEntity;
    }

    private CuantiaBeca preparaCuantiaBeca(Long becaId, Long cuantiaId, UIEntity entity) {
        CuantiaBeca cuantiaBeca = new CuantiaBeca();

        Beca beca = new Beca();
        beca.setId(becaId);

        Cuantia cuantia = Cuantia.getCuantiaById(cuantiaId);

        cuantiaBeca.setBeca(beca);
        cuantiaBeca.setCuantia(cuantia);

        Float importe = ParamUtils.parseFloat(entity.get("importe").replace(".", ","));
        cuantiaBeca.setImporte(importe);

        return cuantiaBeca;
    }
}