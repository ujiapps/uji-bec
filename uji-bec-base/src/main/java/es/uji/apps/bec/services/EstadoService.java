package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.EstadoDAO;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class EstadoService
{
    private EstadoDAO estadoDAO;

    @Autowired
    public EstadoService(EstadoDAO estadoDAO)
    {
        this.estadoDAO = estadoDAO;
    }

    public List<UIEntity> getEstados()
    {
        List<Estado> estados = estadoDAO.getEstados();
        return UIEntity.toUI(estados);
    }

    public List<UIEntity> getEstadoById(Long estadoId)
    {
        if (estadoId == null)
        {
            return null;
        }

        Estado estado = estadoDAO.getEstadoById(estadoId);
        if (estado == null)
        {
            return null;
        }

        return UIEntity.toUI(Collections.singletonList(estado));
    }

    public UIEntity insertaEstado(UIEntity entity)
    {
        Estado estado = preparaEstado(entity);
        if (estado.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(estadoDAO.insert(estado));
    }

    public ResponseMessage actualizaEstado(UIEntity entity)
    {
        Estado estado = preparaEstado(entity);

        estadoDAO.update(estado);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraEstado(Long estadoId) throws RegistroNoEncontradoException, RegistroConHijosException
    {
        Estado estado = estadoDAO.getEstadoById(estadoId);

        if (estado == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            estadoDAO.deleteEstado(estado.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private Estado preparaEstado(UIEntity entity)
    {
        Estado estado = new Estado();
        estado.setId(ParamUtils.parseLong(entity.get("id")));
        estado.setNombre(entity.get("nombre"));
        estado.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return estado;
    }

    public List<Resumen> getResumenEstadosPorTanda(Long tandaId)
    {

        return estadoDAO.getResumenEstadosPorTanda(tandaId);
    }
}
