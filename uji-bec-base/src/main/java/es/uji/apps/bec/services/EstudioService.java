package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.EstudioDAO;
import es.uji.apps.bec.model.Estudio;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EstudioService
{
    private EstudioDAO estudioDAO;

    @Autowired
    public EstudioService(EstudioDAO estudioDAO)
    {
        this.estudioDAO = estudioDAO;
    }

    public List<UIEntity> getEstudiosByPersonaIdAndCursoAcademicoId(Long personaId,
            Long cursoAcademicoId)
    {
        Map filtros = new HashMap<String, Long>();

        filtros.put("personaId", personaId);
        filtros.put("cursoAcademicoId", cursoAcademicoId);

        List<Estudio> estudios = estudioDAO.getEstudiosFiltrados(filtros);
        return UIEntity.toUI(estudios);
    }
}
