package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.CuantiaDAO;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.Cuantia;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class CuantiaService extends CoreBaseService
{
    private CuantiaDAO cuantiaDAO;

    @Autowired
    public CuantiaService(CuantiaDAO cuantiaDAO)
    {
        this.cuantiaDAO = cuantiaDAO;
    }

    public List<UIEntity> getCuantias()
    {
        return UIEntity.toUI(cuantiaDAO.getCuantias());
    }

    public List<UIEntity> getCuantiasActivas()
    {
        return UIEntity.toUI(cuantiaDAO.getCuantiasActivas());
    }

    public UIEntity insertaCuantia(UIEntity entity)
    {
        Cuantia cuantia = preparaCuantia(entity);
        if (cuantia.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(cuantiaDAO.insert(cuantia));
    }

    public ResponseMessage borraCuantia(Long cuantiaId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        Cuantia cuantia = cuantiaDAO.getCuantiaById(cuantiaId);
        if (cuantia == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            cuantiaDAO.deleteCuantia(cuantia.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    public ResponseMessage actualizaCuantia(UIEntity entity)
    {
        Cuantia cuantia = preparaCuantia(entity);

        cuantiaDAO.update(cuantia);

        return new ResponseMessage(true);
    }

    private Cuantia preparaCuantia(UIEntity entity)
    {
        Cuantia cuantia = new Cuantia();
        cuantia.setId(ParamUtils.parseLong(entity.get("id")));
        cuantia.setNombre(entity.get("nombre"));
        cuantia.setCodigo(entity.get("codigo"));
        cuantia.setActiva(Boolean.parseBoolean(entity.get("activa")));

        Convocatoria convocatoria = preparaConvocatoria(entity);
        cuantia.setConvocatoria(convocatoria);

        return cuantia;
    }

    private Convocatoria preparaConvocatoria(UIEntity entity)
    {
        Long convocatoriaId = ParamUtils.parseLong(entity.get("convocatoriaId"));
        return Convocatoria.getConvocatoriaById(convocatoriaId);
    }
}
