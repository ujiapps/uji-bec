package es.uji.apps.bec.services;

import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.MiembroDuplicadoException;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.BecaReclamacion;
import es.uji.apps.bec.model.Denegacion;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BecaReclamacionSubResourceService
{
    @PathParam("becaId")
    Long becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReclamacionesByBeca()
    {
        List<BecaReclamacion> reclamaciones = BecaReclamacion.getReclamacionesByBecaId(becaId);
        return UIEntity.toUI(reclamaciones);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReclamacionById(@PathParam("id") Long reclamacionId)
    {
        BecaReclamacion reclamacion = BecaReclamacion.getReclamacionById(reclamacionId);
        return Collections.singletonList(UIEntity.toUI(reclamacion));
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long reclamacionId) throws RegistroNoEncontradoException, BecaException {

        BecaReclamacion reclamacion = BecaReclamacion.getReclamacionById(reclamacionId);
        if (reclamacion == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            reclamacion.delete();
        }
        catch (Exception e)
        {
            throw new BecaException("Error al borrar la reclamación");
        }

        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity actualiza(@PathParam("id") Long reclamacionId, MultivaluedMap<String, String> params)
            throws GeneralBECException, ParseException
    {
        BecaReclamacion reclamacion = BecaReclamacion.getReclamacionById(reclamacionId);

        return reclamacion.actualiza(params);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertMiembro(MultivaluedMap<String, String> params)
            throws MiembroDuplicadoException, ParseException
    {
        BecaReclamacion reclamacion = new BecaReclamacion();

        return reclamacion.inserta(params);
    }
}