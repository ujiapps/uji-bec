package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoIdentificacionDAO;
import es.uji.apps.bec.model.TipoIdentificacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoIdentificacionService
{
    private TipoIdentificacionDAO tipoIdentificacionDAO;

    @Autowired
    public TipoIdentificacionService(TipoIdentificacionDAO tipoIdentificacionDAO)
    {
        this.tipoIdentificacionDAO = tipoIdentificacionDAO;
    }

    public List<UIEntity> getTiposIdentificaciones()
    {
        List<TipoIdentificacion> identificaciones = tipoIdentificacionDAO.getTiposIdentificaciones();
        return UIEntity.toUI(identificaciones);
    }

    public UIEntity insertaTipoIdentificacion(UIEntity entity)
    {
        TipoIdentificacion tipoIdentificacion = preparaTipoIdentificacion(entity);
        if (tipoIdentificacion.getNombre() == null)
        {
            return new UIEntity();
        }
        return UIEntity.toUI(tipoIdentificacionDAO.insert(tipoIdentificacion));
    }

    public ResponseMessage actualizaTipoIdentificacion(UIEntity entity)
    {
        TipoIdentificacion tipoIdentificacion = preparaTipoIdentificacion(entity);
        tipoIdentificacionDAO.update(tipoIdentificacion);
        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoIdentificacion(Long tipoIdentificacionId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoIdentificacion tipoIdentificacion = tipoIdentificacionDAO.getTipoIdentificacionById(tipoIdentificacionId);

        if (tipoIdentificacion == null )
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoIdentificacionDAO.deleteTipoIdentificacion(tipoIdentificacion.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoIdentificacion preparaTipoIdentificacion(UIEntity entity)
    {
        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
        tipoIdentificacion.setId(ParamUtils.parseLong(entity.get("id")));
        tipoIdentificacion.setNombre(entity.get("nombre"));
        tipoIdentificacion.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoIdentificacion;
    }
}
