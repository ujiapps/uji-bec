package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.PaisDAO;
import es.uji.apps.bec.model.Pais;
import es.uji.commons.rest.UIEntity;

@Service
public class PaisService
{
    private PaisDAO paisDAO;

    @Autowired
    public PaisService(PaisDAO paisDAO)
    {
        this.paisDAO = paisDAO;
    }

    public List<UIEntity> getPaises()
    {
        List<Pais> paises = paisDAO.getPaises();
        return UIEntity.toUI(paises);
    }
}
