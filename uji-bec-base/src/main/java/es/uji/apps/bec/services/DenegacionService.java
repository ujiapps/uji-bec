package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.DenegacionDAO;
import es.uji.apps.bec.dao.dao.OrganismoDAO;
import es.uji.apps.bec.model.Denegacion;
import es.uji.apps.bec.model.Organismo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class DenegacionService extends CoreBaseService
{
    private DenegacionDAO denegacionDAO;
    private OrganismoDAO organismoDAO;

    @Autowired
    public DenegacionService(DenegacionDAO denegacionDAO, OrganismoDAO organismoDAO)
    {
        this.denegacionDAO = denegacionDAO;
        this.organismoDAO = organismoDAO;
    }

    public List<UIEntity> getDenegaciones()
    {
        return UIEntity.toUI(denegacionDAO.getDenegaciones());
    }

    public List<UIEntity> getDenegacionesActivas()
    {
        return UIEntity.toUI(denegacionDAO.getDenegacionesActivas());
    }

    public UIEntity insertaDenegacion(UIEntity entity)
    {
        Denegacion denegacion = preparaDenegacion(entity);
        if (denegacion.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(denegacionDAO.insert(denegacion));
    }

    public ResponseMessage borraDenegacion(Long denegacionId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        Denegacion denegacion = denegacionDAO.getDenegacionById(denegacionId);
        if (denegacion == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            denegacionDAO.deleteDenegacion(denegacion.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    public ResponseMessage actualizaDenegacion(UIEntity entity)
    {
        Denegacion denegacion = preparaDenegacion(entity);

        denegacionDAO.update(denegacion);

        return new ResponseMessage(true);
    }

    private Denegacion preparaDenegacion(UIEntity entity)
    {
        Denegacion denegacion = new Denegacion();
        denegacion.setId(ParamUtils.parseLong(entity.get("id")));
        denegacion.setNombre(entity.get("nombre"));
        denegacion.setActiva(Boolean.parseBoolean(entity.get("activa")));
        denegacion.setEstado(entity.get("estado"));
        denegacion.setCausa(entity.get("causa"));
        denegacion.setSubCausa(entity.get("subCausa"));

        Organismo organismo = preparaOrganismo(entity);
        denegacion.setOrganismo(organismo);

        return denegacion;
    }

    private Organismo preparaOrganismo(UIEntity entity)
    {
        Long organismoId = ParamUtils.parseLong(entity.get("organismoId"));
        return organismoDAO.getOrganismoById(organismoId);
    }

    private List<UIEntity> modelToUI(List<Denegacion> listaDenegaciones)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Denegacion denegacion : listaDenegaciones)
        {
            result.add(modelToUI(denegacion));
        }
        return result;
    }

    private UIEntity modelToUI(Denegacion denegacion)
    {
        UIEntity entity = UIEntity.toUI(denegacion);

        String subcausa = denegacion.getSubCausa();
        if (subcausa == null || subcausa.equals("null"))
        {
            subcausa = "";
        }

        entity.put("nombreListaValores", denegacion.getCausa() + subcausa + " - " + denegacion.getNombre());
        return entity;
    }

    public List<UIEntity> getDenegacionesActivasByOrganismo(Long organismoId)
    {
        List<Denegacion> denegacionesByOrganismo = denegacionDAO.getDenegacionesActivasByOrganismo(organismoId);
        return modelToUI(denegacionesByOrganismo);
    }
}
