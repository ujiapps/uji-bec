package es.uji.apps.bec.notificaciones;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Documento;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Solicitante;

import java.util.List;

public class NotificacionDocumentacionPendiente extends NotificacionBase
{
    @Override
    public void prepara()
    {
        String cuerpoMensaje = preparaCuerpoMensaje();
        Solicitante solicitante = this.getBeca().getSolicitante();
        Persona persona = Persona.getPersonaByPersonaId(solicitante.getPersona().getId());
        String correoUji = persona.getCorreoUJI();

        setTexto(cuerpoMensaje);
        setAsunto("[e-ujier@becas] Beca MEC documentació pendent. Localitzador temporal MEC: " + this.getBeca().getCodigoArchivoTemporal());
        setDestinatarioCorreoUJI(correoUji);
        setDestinatarioCorreoMEC(solicitante.getEmail());
    }

    private String preparaCuerpoMensaje() {
        String cuerpoCabecera = preparaCabecera(this.getBeca());
        String cuerpoTextoGenerico = preparaTextoGenerico();
        String cuerpoDocumentosPendientes = preparaDocumentacionPendiente(this.getBeca());
        String cuerpoTextoAdicional = preparaTextoAdicional(this.getTextoAdicional());
        String cuerpoPie = preparaPie();

        return cuerpoCabecera + cuerpoTextoGenerico + cuerpoDocumentosPendientes + cuerpoTextoAdicional + cuerpoPie;
    }

    private String preparaTextoGenerico() {
        return "\n" +
               "En aplicació del que disposa la normativa reguladora de beques i ajudes a l'estudi i en les bases de la corresponent convocatòria de beca general per al present curs acadèmic, després de revisar la vostra sol·licitud i de comprovar que algun dels requisits d'aquesta no estan ben justificats, en el termini de deu dies, comptadors a partir de la recepció d'aquesta notificació, heu de pujar o adjuntar a l'aplicació, la documentació escanejada que s'indica a continuació. \n" +
               "Si no ho feu, es tindrà per desistida la vostra petició en els termes de l''article 68 de la LLei 39/2015, del procediment administratiu comú d'administracions públiques.\n" +
               "\n" +
               "La documentació següent s'ha de pujar o adjuntar a través de l'aplicació: \n" +
               "https://e-ujier.uji.es/pls/www/!gri_www.euji23293";
    }

    private String preparaTextoAdicional(String observaciones) {
        String cuerpoObservaciones = null;
        if (observaciones != null)
        {
            cuerpoObservaciones =  "\n\n" +
                    observaciones;
        }
        return cuerpoObservaciones;
    }

    private String preparaDocumentacionPendiente(Beca beca) {
        String cuerpoDocumentosPendientes = "\n\n" +
                                            "Documentació pendent:";

        List<Documento> listaDocumentosPendientes = beca.getDocumentosPendientesNotificacion();
        for (Documento documentoPendiente : listaDocumentosPendientes)
        {
            cuerpoDocumentosPendientes = cuerpoDocumentosPendientes + "\n" + " - " + documentoPendiente.getTipoDocumento().getNombre();
        }
        return cuerpoDocumentosPendientes;
    }
}
