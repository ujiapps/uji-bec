package es.uji.apps.bec.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.ProfesionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("profesion")
public class ProfesionResource extends CoreBaseService
{
    @InjectParam
    private ProfesionService profesionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProfesiones()
    {
        return profesionService.getProfesiones();
    }
}
