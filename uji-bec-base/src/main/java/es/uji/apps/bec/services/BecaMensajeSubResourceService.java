package es.uji.apps.bec.services;

import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Documento;
import es.uji.apps.bec.model.Mensaje;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.notificaciones.NotificacionDocumentacionCorrecta;
import es.uji.apps.bec.notificaciones.NotificacionDocumentacionPendiente;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class BecaMensajeSubResourceService
{
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMensajes()
    {
        List<Mensaje> listaMensajes= Mensaje.getMensajesByBecaId(Long.parseLong(becaId));

        return UIEntity.toUI(listaMensajes);
    }

    @PUT
    @Path("/documentacion/pendiente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity notificaDocumentacionPendiente(MultivaluedMap<String, String> params) throws BecaException
    {
        Beca beca = Beca.getBecaById(Long.parseLong(params.getFirst("becaId")));

        if (!beca.isEnEstado(CodigoEstado.DOCUMENTACION_PENDIENTE))
        {
            throw new BecaException("La beca ha d'estar en estat pendent de documentació.");
        }

        NotificacionDocumentacionPendiente mensaje = new NotificacionDocumentacionPendiente();
        mensaje.setBeca(beca);
        mensaje.setTextoAdicional(params.getFirst("observaciones"));
        mensaje.setAsunto("[e-ujier@becas] Beca MEC documentació pendent. Localitzador temporal MEC: " + beca.getCodigoArchivoTemporal());
        mensaje.prepara();
        mensaje.envia();

        actualizaFechaNotificacionDocumentos(beca);

        return UIEntity.toUI(mensaje);
    }

    private void actualizaFechaNotificacionDocumentos(Beca beca) {
        List<Documento> documentosPendientesNotificacion = beca.getDocumentosPendientesNotificacion();
        for (Documento documento : documentosPendientesNotificacion)
        {
            documento.setFechaSolicita(new Timestamp(new Date().getTime()));
            documento.updateDocumento();
        }
    }

    @PUT
    @Path("/documentacion/correcta")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity notificaDocumentacionCorrecta(MultivaluedMap<String, String> params)
    {
        NotificacionDocumentacionCorrecta mensaje = new NotificacionDocumentacionCorrecta();
        mensaje.setBeca(Beca.getBecaById(Long.parseLong(params.getFirst("becaId"))));
        mensaje.setTextoAdicional(params.getFirst("observaciones"));
        mensaje.prepara();
        mensaje.envia();

        return UIEntity.toUI(mensaje);
    }
}