package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.model.Beca;
import es.uji.commons.rest.UIEntity;

public class BecaObservacionSubResourceService
{
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHistoricoObservacionesByBeca()
    {
        List<Beca> listaObservacionesBecas = Beca.getHistoricoObservacionesByBeca(Long
                .parseLong(becaId));

        return preparaListaUIEntityObservaciones(listaObservacionesBecas);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response guardaObservacionesUji(MultivaluedMap<String, String> params)
            throws BecaDuplicadaException
    {
        String becaId = params.getFirst("id");
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        beca.setObservacionesUji(params.getFirst("observacionesUji"));
        beca.updateObservacionesUji();

        return Response.ok(new UIEntity()).build();
    }

    private List<UIEntity> preparaListaUIEntityObservaciones(List<Beca> listaObservacionesBecas)
    {
        ArrayList<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Beca beca : listaObservacionesBecas)
        {
            UIEntity ui = new UIEntity();
            ui.put("id", beca.getId());
            ui.put("cursoAcademico", beca.getSolicitante().getCursoAcademico().getId());
            ui.put("convocatoria", beca.getConvocatoria().getNombre());
            ui.put("observaciones", beca.getObservacionesUji());

            listaUI.add(ui);
        }

        return listaUI;
    }
}