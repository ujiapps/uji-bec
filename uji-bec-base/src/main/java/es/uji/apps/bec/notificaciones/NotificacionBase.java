package es.uji.apps.bec.notificaciones;

import javax.ws.rs.core.MediaType;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Solicitante;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

public abstract class NotificacionBase implements Notificacion {
    private Beca beca;
    private String asunto;
    private String texto;
    private String textoAdicional;
    private String destinatarioCorreoMEC;
    private String destinatarioCorreoUJI;

    @Override
    public void envia() {
        MailMessage email = new MailMessage("BEC");
        email.setTitle(this.asunto);
        email.setSender("noreply@uji.es");
        email.setContent(this.texto);
        email.setContentType(MediaType.TEXT_PLAIN);
        email.setReference(beca.getId().toString());

        email.addToRecipient(destinatarioCorreoMEC);
        email.addToRecipient(destinatarioCorreoUJI);

        try {
            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String preparaCabecera(Beca beca) {
        Solicitante solicitante = beca.getSolicitante();
        Persona persona = solicitante.getPersona();
        return "\n" +
                persona.getNombre() + "\n" +
                "Localitzador temporal MEC: " + beca.getCodigoArchivoTemporal() + "\n" +
                "Codi UJI: " + solicitante.getNumeroBecaUji() + "\n";
    }

    public String preparaPie() {
        return "\n\n\n" +
                "Negociat de Beques\n" +
                "Servei de Gestió de la Docència i Estudiants\n" +
                "Universitat Jaume I";
    }

    public Beca getBeca() {
        return beca;
    }

    public void setBeca(Beca beca) {
        this.beca = beca;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTextoAdicional() {
        return textoAdicional;
    }

    public void setTextoAdicional(String textoAdicional) {
        this.textoAdicional = textoAdicional;
    }

    public String getDestinatarioCorreoMEC() {
        return destinatarioCorreoMEC;
    }

    public void setDestinatarioCorreoMEC(String destinatarioCorreoMEC) {
        this.destinatarioCorreoMEC = destinatarioCorreoMEC;
    }

    public String getDestinatarioCorreoUJI() {
        return destinatarioCorreoUJI;
    }

    public void setDestinatarioCorreoUJI(String destinatarioCorreoUJI) {
        this.destinatarioCorreoUJI = destinatarioCorreoUJI;
    }
}
