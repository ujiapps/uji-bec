package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoIdentificacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipoidentificacion")
public class TipoIdentificacionResource extends CoreBaseService
{
    @InjectParam
    private TipoIdentificacionService tipoIdentificacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposIdentificaciones()
    {
        return tipoIdentificacionService.getTiposIdentificaciones();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoIdentificacion(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return tipoIdentificacionService.insertaTipoIdentificacion(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoIdentificacion(@PathParam("id") Long tipoIdentificacionId,
            UIEntity entity)
    {
        return tipoIdentificacionService.actualizaTipoIdentificacion(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoIdentificacion(@PathParam("id") Long tipoIdentificacionId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoIdentificacionService.borraTipoIdentificacion(tipoIdentificacionId);
    }
}
