package es.uji.apps.bec.services.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.text.ParseException;
import java.util.List;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoPropiedadService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipopropiedad")
public class TipoPropiedadResource
{
    @InjectParam
    private TipoPropiedadService tipoPropiedadService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposPropiedades()
    {
        return tipoPropiedadService.getTiposPropiedades();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoPropiedad(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return tipoPropiedadService.insertaTipoPropiedad(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoPropiedad(@PathParam("id") Long tipoPropiedadId,
            UIEntity entity)
    {
        return tipoPropiedadService.actualizaTipoPropiedad(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoPropiedad(@PathParam("id") Long tipoPropiedadId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoPropiedadService.borraTipoPropiedad(tipoPropiedadId);
    }
}
