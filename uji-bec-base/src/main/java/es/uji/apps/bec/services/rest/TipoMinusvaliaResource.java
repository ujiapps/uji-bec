package es.uji.apps.bec.services.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.text.ParseException;
import java.util.List;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoMinusvaliaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipominusvalia")
public class TipoMinusvaliaResource extends CoreBaseService
{
    @InjectParam
    private TipoMinusvaliaService tipoMinusvaliaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposMinusvalias()
    {
        return tipoMinusvaliaService.getTiposMinusvalias();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoMinusvalia(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return tipoMinusvaliaService.insertaTipoMinusvalia(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoMinusvalia(@PathParam("id") Long tipoMinusvaliaId,
            UIEntity entity)
    {
        return tipoMinusvaliaService.actualizaTipoMinusvalia(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoMinusvalia(@PathParam("id") Long tipoMinusvaliaId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoMinusvaliaService.borraTipoMinusvalia(tipoMinusvaliaId);
    }
}
