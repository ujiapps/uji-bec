package es.uji.apps.bec.services;

import java.util.List;

import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.LocalidadDAO;
import es.uji.apps.bec.model.Localidad;
import es.uji.commons.rest.UIEntity;

@Service
public class LocalidadService
{
    private LocalidadDAO localidadDAO;

    @Autowired
    public LocalidadService(LocalidadDAO localidadDAO)
    {
        this.localidadDAO = localidadDAO;
    }

    public List<UIEntity> getLocalidades()
    {
        List<Localidad> localidades = localidadDAO.getLocalidades();
        return UIEntity.toUI(localidades);
    }

    public List<UIEntity> getLocalidadesByProvincia(@PathParam("id") Long provinciaId)
    {
        List<Localidad> localidades = localidadDAO.getLocalidadesByProvincia(provinciaId);
        return UIEntity.toUI(localidades);
    }
}
