package es.uji.apps.bec.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.PersonaDAO;
import es.uji.apps.bec.model.Persona;
import es.uji.commons.rest.UIEntity;

@Service
public class PersonaService
{
    private PersonaDAO personaDAO;

    @Autowired
    public PersonaService(PersonaDAO personaDAO)
    {
        this.personaDAO = personaDAO;
    }

    public UIEntity getPersonaByDni(String dni)
    {
        if (dni == null || dni.isEmpty())
        {
            return null;
        }

        Persona persona = personaDAO.getPersonaByDNI(dni);
        return UIEntity.toUI(persona);
    }
}
