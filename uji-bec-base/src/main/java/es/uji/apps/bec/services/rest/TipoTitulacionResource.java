package es.uji.apps.bec.services.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.TipoTitulacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import java.util.List;

@Path("tipotitulacion")
public class TipoTitulacionResource extends CoreBaseService
{
    @InjectParam
    private TipoTitulacionService tipoTitulacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTitulacion()
    {
        return tipoTitulacionService.getTiposTitulacion();
    }
}
