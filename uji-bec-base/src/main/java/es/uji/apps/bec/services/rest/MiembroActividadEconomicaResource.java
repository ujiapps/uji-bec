package es.uji.apps.bec.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.ActividadEconomicaException;
import es.uji.apps.bec.model.ActividadEconomica;
import es.uji.apps.bec.services.MiembroActividadEconomicaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

public class MiembroActividadEconomicaResource extends CoreBaseService
{
    @PathParam("id")
    Long miembroId;
    @InjectParam
    private MiembroActividadEconomicaService miembroActividadEconomicaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividadesEconomicasByMiembroId()
    {
        List<ActividadEconomica> actividadesEconomicas = miembroActividadEconomicaService.
                getActividadesEconomicasByMiembroId(miembroId);

        return UIEntity.toUI(actividadesEconomicas);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertActividadEconomica(UIEntity entity)
    {
        return miembroActividadEconomicaService.insertActividadEconomica(miembroId, entity);
    }

    @PUT
    @Path("{actividadEconomicaId}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public UIEntity updateActividadEconomica(
            @PathParam("actividadEconomicaId") Long actividadEconomicaId, UIEntity entity) throws
            ActividadEconomicaException
    {
        return miembroActividadEconomicaService.updateActividadEconomica(actividadEconomicaId,
                entity);
    }

    @DELETE
    @Path("{actividadEconomicaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteActividadEconomica(
            @PathParam("actividadEconomicaId") Long actividadEconomicaId) throws
            ActividadEconomicaException
    {
        miembroActividadEconomicaService.deleteActividadEconomica(actividadEconomicaId);
        return new ResponseMessage(true);
    }
}
