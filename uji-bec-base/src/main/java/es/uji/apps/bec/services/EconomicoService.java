package es.uji.apps.bec.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import es.uji.apps.bec.exceptions.BecaDelMismoOrganismoYaExisteException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.DemasiadasBecasException;
import es.uji.apps.bec.model.Economico;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("economico")
public class EconomicoService extends CoreBaseService
{
    private List<UIEntity> modelToUI(List<Economico> listaEconomicos)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Economico economico : listaEconomicos)
        {
            result.add(UIEntity.toUI(economico));
        }
        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDeclaranteById(@QueryParam("solicitanteId") String solicitanteId)
    {
        List<UIEntity> listaEntidades = modelToUI(Economico.getEconomicoBySolicitanteId(Long
                .parseLong(solicitanteId)));
        return listaEntidades;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertarEconomico(MultivaluedMap<String, String> params)
            throws DemasiadasBecasException, BecaDuplicadaException, BecaSinEstudioException,
            BecaDelMismoOrganismoYaExisteException, ParseException
    {
        Long solicitanteId = ParamUtils.parseLong(params.getFirst("solicitanteId"));
        Float capitalMobiliario = ParamUtils.parseFloat(params.getFirst("capitalMobiliario"));
        Float umbralMovGeneral = ParamUtils.parseFloat(params.getFirst("umbralMovGeneral"));
        Float umbralCompensa = ParamUtils.parseFloat(params.getFirst("umbralCompensa"));
        Float renta = ParamUtils.parseFloat(params.getFirst("renta"));
        Float umbralBecario = ParamUtils.parseFloat(params.getFirst("umbralBecario"));
        Float umbralResidMov = ParamUtils.parseFloat(params.getFirst("umbralResidMov"));
        Float umbralPatrimonio = ParamUtils.parseFloat(params.getFirst("umbralPatrimonio"));
        Float coeficientePrelacion = ParamUtils.parseFloat(params.getFirst("coeficientePrelacion"));
        Float umbralTasas = ParamUtils.parseFloat(params.getFirst("umbralTasas"));
        Float volumenNegocio = ParamUtils.parseFloat(params.getFirst("volumenNegocio"));
        Float umbralMovEspecial = ParamUtils.parseFloat(params.getFirst("umbralMovEspecial"));
        Float deducciones = ParamUtils.parseFloat(params.getFirst("deducciones"));
        String codRepesca = params.getFirst("codRepesca");

        Integer indMovEspecial = null;
        if (!params.getFirst("indMovEspecial").isEmpty())
        {
            indMovEspecial = Integer.parseInt(params.getFirst("indMovEspecial"));
        }

        Integer indPatrimonio = null;
        if (!params.getFirst("indPatrimonio").isEmpty())
        {
            indPatrimonio = Integer.parseInt(params.getFirst("indPatrimonio"));
        }

        Integer indCompensa = null;
        if (!params.getFirst("indCompensa").isEmpty())
        {
            indCompensa = Integer.parseInt(params.getFirst("indCompensa"));
        }

        Integer indBecario = null;
        if (!params.getFirst("indBecario").isEmpty())
        {
            indBecario = Integer.parseInt(params.getFirst("indBecario"));
        }

        Integer indMovGeneral = null;
        if (!params.getFirst("indMovGeneral").isEmpty())
        {
            indMovGeneral = Integer.parseInt(params.getFirst("indMovGeneral"));
        }

        Integer indTasas = null;
        if (!params.getFirst("indTasas").isEmpty())
        {
            indTasas = Integer.parseInt(params.getFirst("indTasas"));
        }

        Integer indResidMov = null;
        if (!params.getFirst("indResidMov").isEmpty())
        {
            indResidMov = Integer.parseInt(params.getFirst("indResidMov"));
        }

        Economico registroEconomico = Economico.insertRegistro(solicitanteId, capitalMobiliario,
                umbralMovGeneral, indMovEspecial, indPatrimonio, umbralCompensa, indMovGeneral,
                indTasas, renta, umbralBecario, indBecario, umbralResidMov, umbralPatrimonio,
                coeficientePrelacion, indResidMov, codRepesca, umbralTasas, indCompensa,
                volumenNegocio, umbralMovEspecial, deducciones);

        return UIEntity.toUI(registroEconomico);

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("{economicoId}")
    public UIEntity modificarEconomico(@PathParam("declaranteId") String declaranteId,
            MultivaluedMap<String, String> params) throws DemasiadasBecasException,
            BecaDuplicadaException, BecaSinEstudioException, BecaDelMismoOrganismoYaExisteException,
            ParseException
    {
        Long economicoId = ParamUtils.parseLong(params.getFirst("id"));
        Float capitalMobiliario = ParamUtils.parseFloat(params.getFirst("capitalMobiliario"));
        Float umbralMovGeneral = ParamUtils.parseFloat(params.getFirst("umbralMovGeneral"));
        Float umbralCompensa = ParamUtils.parseFloat(params.getFirst("umbralCompensa"));
        Float renta = ParamUtils.parseFloat(params.getFirst("renta"));
        Float umbralBecario = ParamUtils.parseFloat(params.getFirst("umbralBecario"));
        Float umbralResidMov = ParamUtils.parseFloat(params.getFirst("umbralResidMov"));
        Float umbralPatrimonio = ParamUtils.parseFloat(params.getFirst("umbralPatrimonio"));
        Float coeficientePrelacion = ParamUtils.parseFloat(params.getFirst("coeficientePrelacion"));
        Float umbralTasas = ParamUtils.parseFloat(params.getFirst("umbralTasas"));
        Float volumenNegocio = ParamUtils.parseFloat(params.getFirst("volumenNegocio"));
        Float umbralMovEspecial = ParamUtils.parseFloat(params.getFirst("umbralMovEspecial"));
        Float deducciones = ParamUtils.parseFloat(params.getFirst("deducciones"));
        String codRepesca = params.getFirst("codRepesca");

        Integer indMovEspecial = null;
        if (!params.getFirst("indMovEspecial").isEmpty())
        {
            indMovEspecial = Integer.parseInt(params.getFirst("indMovEspecial"));
        }

        Integer indPatrimonio = null;
        if (!params.getFirst("indPatrimonio").isEmpty())
        {
            indPatrimonio = Integer.parseInt(params.getFirst("indPatrimonio"));
        }

        Integer indCompensa = null;
        if (!params.getFirst("indCompensa").isEmpty())
        {
            indCompensa = Integer.parseInt(params.getFirst("indCompensa"));
        }

        Integer indBecario = null;
        if (!params.getFirst("indBecario").isEmpty())
        {
            indBecario = Integer.parseInt(params.getFirst("indBecario"));
        }

        Integer indMovGeneral = null;
        if (!params.getFirst("indMovGeneral").isEmpty())
        {
            indMovGeneral = Integer.parseInt(params.getFirst("indMovGeneral"));
        }

        Integer indTasas = null;
        if (!params.getFirst("indTasas").isEmpty())
        {
            indTasas = Integer.parseInt(params.getFirst("indTasas"));
        }

        Integer indResidMov = null;
        if (!params.getFirst("indResidMov").isEmpty())
        {
            indResidMov = Integer.parseInt(params.getFirst("indResidMov"));
        }

        Economico registroEconomico = Economico.getEconomicoById(economicoId).get(0);

        registroEconomico.updateRegistro(capitalMobiliario, umbralMovGeneral, indMovEspecial,
                indPatrimonio, umbralCompensa, indMovGeneral, indTasas, renta, umbralBecario,
                indBecario, umbralResidMov, umbralPatrimonio, coeficientePrelacion, indResidMov,
                codRepesca, umbralTasas, indCompensa, volumenNegocio, umbralMovEspecial,
                deducciones);

        return UIEntity.toUI(registroEconomico);

    }
}
