package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.DomicilioDuplicadoException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.DomicilioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("domicilio")
public class DomicilioResource extends CoreBaseService
{
    @InjectParam
    private DomicilioService domicilioService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getDomicilio(@PathParam("id") Long domicilioId)
    {
        return domicilioService.getDomicilio(domicilioId).get(0);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage updateDomicilio(@PathParam("id") Long domicilioId, MultivaluedMap<String, String> params)
            throws DomicilioDuplicadoException, ParseException
    {
        return domicilioService.updateDomicilio(domicilioId, params);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertaDomicilio(MultivaluedMap<String, String> params)
            throws DomicilioDuplicadoException, ParseException
    {
        return domicilioService.insertaDomicilio(params);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraDomicilio(@PathParam("id") Long domicilioId) throws RegistroNoEncontradoException,
            NumberFormatException
    {
        return domicilioService.borraDomicilio(domicilioId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDomiciliosByBecaId(@QueryParam("becaId") Long becaId)
    {
        return domicilioService.getDomiciliosByBecaId(becaId);
    }

    @GET
    @Path("{domicilioId}/arrendatarios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getArrendatariosByDomicilio(@PathParam("domicilioId") Long domicilioId)
    {
        return domicilioService.getArrendatariosByDomicilio(domicilioId);
    }

    @PUT
    @Path("{domicilioId}/arrendatarios/{arrendatarioId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateArrendatario(@PathParam("domicilioId") Long domicilioId,
                                       @PathParam("arrendatarioId") Long arrendatarioId, UIEntity entity)
    {
        return domicilioService.updateArrendatario(domicilioId, arrendatarioId, entity);
    }

    @POST
    @Path("{domicilioId}/arrendatarios")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaArrendatario(@PathParam("domicilioId") Long domicilioId, UIEntity entity)
    {
        return domicilioService.insertaArrendatario(domicilioId, entity);
    }

    @DELETE
    @Path("{domicilioId}/arrendatarios/{arrendatarioId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteArrendatario(@PathParam("domicilioId") Long domicilioId,
                                              @PathParam("arrendatarioId") Long arrendatarioId)
            throws GeneralBECException
    {
        return domicilioService.deleteArrendatario(domicilioId, arrendatarioId);
    }
}
