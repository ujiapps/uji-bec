package es.uji.apps.bec.services;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.dao.dao.TextoDAO;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Texto;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("texto")
public class TextoService extends CoreBaseService
{
    @InjectParam
    public TextoDAO textoDAO;

    @GET
    @Path("proceso/{procesoId}/organismo/{organismoId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTextosPorProcesoYOrganismo(@PathParam("procesoId") Long procesoId,
            @PathParam("organismoId") Long organismoId)
    {
        return UIEntity.toUI(textoDAO.getTextosPorProcesoYOrganismo(procesoId, organismoId));
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> actualizaTexto(@FormParam("organismoId") Long organismoId,
                                         @FormParam("procesoId") Long procesoId, @FormParam("nombre") String nombre,
                                         @FormParam("cabeceraCa") String cabeceraCa, @FormParam("cabeceraEs") String cabeceraEs,
                                         @FormParam("pieCa") String pieCa, @FormParam("pieEs") String pieEs,
                                         @FormParam("id") Long id)
    {
        Texto texto =
                preparaTexto(organismoId, procesoId, nombre, cabeceraCa, cabeceraEs, pieCa, pieEs);

        texto.setId(id);
        textoDAO.updateTexto(texto);

        UIEntity uiEntity = UIEntity.toUI(texto);

        return Collections.singletonList(uiEntity);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> insertaTexto(@FormParam("organismoId") Long organismoId,
            @FormParam("procesoId") Long procesoId, @FormParam("nombre") String nombre,
            @FormParam("cabeceraCa") String cabeceraCa, @FormParam("cabeceraEs") String cabeceraEs,
            @FormParam("pieCa") String pieCa, @FormParam("pieEs") String pieEs)
    {
        Texto texto =
                preparaTexto(organismoId, procesoId, nombre, cabeceraCa, cabeceraEs, pieCa, pieEs);

        textoDAO.insert(texto);

        UIEntity uiEntity = UIEntity.toUI(texto);

        return Collections.singletonList(uiEntity);
    }

    private Texto preparaTexto(Long organismoId, Long procesoId, String nombre,
            String cabeceraCa, String cabeceraEs, String pieCa, String pieEs)
    {
        Texto texto = new Texto();
        texto.setOrganismo(preparaOrganismo(organismoId));
        texto.setProceso(preparaProceso(procesoId));
        texto.setCabeceraCa(cabeceraCa);
        texto.setCabeceraEs(cabeceraEs);
        texto.setPieCa(pieCa);
        texto.setPieEs(pieEs);
        texto.setNombre(nombre);
        return texto;
    }

    private Proceso preparaProceso(Long procesoId)
    {
        Proceso proceso = new Proceso();
        proceso.setId(procesoId);
        return proceso;
    }

    private Organismo preparaOrganismo(Long organismoId)
    {
        Organismo organismo = new Organismo();
        organismo.setId(organismoId);
        return organismo;
    }
}