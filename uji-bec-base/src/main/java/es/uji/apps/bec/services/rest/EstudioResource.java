package es.uji.apps.bec.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.EstudioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("estudio")
public class EstudioResource extends CoreBaseService
{
    @InjectParam
    private EstudioService estudioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<UIEntity> getEstudiosByPersonaIdAndCursoAcademicoId(
            @QueryParam("personaId") Long personaId,
            @QueryParam("cursoAcademicoId") Long cursoAcademicoId)
    {
            return estudioService.getEstudiosByPersonaIdAndCursoAcademicoId(personaId,
                cursoAcademicoId);
    }
}
