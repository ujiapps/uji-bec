package es.uji.apps.bec.services;

import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.*;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("propuestabeca")
public class PropuestaBecaService extends CoreBaseService
{
    @POST
    @Path("{becaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity deniegaAcademicos(@PathParam("becaId") String becaId)
            throws BecaDenegacionDuplicadaException, BecaDuplicadaException,
            EstadoBecaNoPermiteCalcularDenegacionesException, BecaCuantiaDuplicadaException,
            AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, DistanciaLocalidadNoEncontradaException,
            AcademicosNoCargadosException, BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException, BecaEnEstadoNoModificableException,
            BecaDeDistintoOrganismoYaExisteException,
            DistanciaDomicilioFamiliarNoEncontradaException, BecaException, EstudioNoEncontradoException, EstudioAnteriorFaltanDatosException {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        PropuestaBeca.controles(beca);

        if (!beca.esModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }

        Beca becaDataBase = Beca.getBecaById(Long.parseLong(becaId));

        if (beca.isDeOrganismo(CodigoOrganismo.MINISTERIO))
        {
            PropuestaBecaMinisterio.calculaPropuesta(beca);
        }
        if (beca.isDeOrganismo(CodigoOrganismo.CONSELLERIA))
        {
            PropuestaBecaConselleria.calculaPropuesta(beca);
        }
        if (beca.isDeOrganismo(CodigoOrganismo.GVA_ALTRES))
        {
            PropuestaBecaGvaAltres.calculaPropuesta(beca);
        }
        HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, AccessManager.getConnectedUser(request).getName());


        return UIEntity.toUI(beca);
    }
}
