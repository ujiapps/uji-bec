package es.uji.apps.bec.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.BecaDenegacion;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CuantiaCurso;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.HistoricoBeca;
import es.uji.apps.bec.model.PersonaEstudio;
import es.uji.apps.bec.model.Proceso;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.apps.bec.webservices.DescargaSolicitudes;
import es.uji.apps.bec.webservices.WebserviceMinisterio;
import es.uji.apps.bec.webservices.ministerio.solicitudes.SolicitudType;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("beca")
public class BecaService extends CoreBaseService
{
    public static final String CONVOCATORIA_GENERAL = "AE";
    public static Logger log = Logger.getLogger(BecaService.class);
    @InjectParam
    private WebserviceMinisterio ministerioWebService;

    @InjectParam
    private DescargaSolicitudes descargaSolicitudes;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getBeca(@PathParam("id") String becaId)
    {
        UIEntity uiEntity = new UIEntity();

        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        uiEntity = UIEntity.toUI(beca);
        addAtributtesNoPrimitives(uiEntity, beca);

        return uiEntity;
    }

    private void addAtributtesNoPrimitives(UIEntity uiEntity, Beca beca)
    {
        uiEntity.put("solicitanteId", beca.getSolicitante().getId());
        uiEntity.put("cursoAcademicoId", beca.getSolicitante().getCursoAcademico().getId());
        uiEntity.put("convocatoriaId", beca.getConvocatoria().getId());
        uiEntity.put("procesoId", beca.getProceso().getId());
        uiEntity.put("estadoId", beca.getEstado().getId());
        if (beca.getTanda() != null)
        {
            uiEntity.put("tandaId", beca.getTanda().getId());
        }
        uiEntity.put("tipoIdentificacionId", beca.getTipoIdentificacion().getId());
        if (beca.getEstudio() != null)
        {
            uiEntity.put("estudioId", beca.getEstudio().getId());
            uiEntity.put("estudioNombre", beca.getEstudio().getNombre());

            preparaDatosPersonaEstudio(uiEntity, beca);
        }
        uiEntity.put("personaId", beca.getSolicitante().getPersona().getId());
        uiEntity.put("nombrePersona", beca.getSolicitante().getPersona().getNombre());
        uiEntity.put("identificacionUji", beca.getSolicitante().getPersona().getIdentificacion());
        uiEntity.put("numeroBecaUji", beca.getSolicitante().getNumeroBecaUji());
        uiEntity.put("nombreConvocatoria", beca.getConvocatoria().getNombre());
    }

    private void preparaDatosPersonaEstudio(UIEntity uiEntity, Beca beca)
    {
        Solicitante solicitante = beca.getSolicitante();
        Long personaId = solicitante.getPersona().getId();
        Long estudioId = beca.getEstudio().getId();
        Long cursoAcademicoId = solicitante.getCursoAcademico().getId();
        PersonaEstudio personaEstudio = PersonaEstudio
                .getAcademicosCursoActualbyPersonaEstudioYCursoAca(personaId, estudioId, cursoAcademicoId);

        if (personaEstudio != null)
        {
            uiEntity.put("creditosParaFinalizar", personaEstudio.getCreditosFaltantes());
            uiEntity.put("creditosComplementos", personaEstudio.getCreditosComplementos());
        }
    }

    @PUT
    @Path("{becaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response guardaBeca(MultivaluedMap<String, String> params)
            throws BecaDuplicadaException, BecaEnEstadoNoModificableException
    {
        String becaId = params.getFirst("id");
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (!beca.esModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }

        beca.setTieneTituloUniversitario(Boolean.parseBoolean(params.getFirst("tieneTituloUniversitario")));
        beca.setTituloNombre(params.getFirst("tituloNombre"));
        beca.setCodigoEstudioAnt(params.getFirst("codigoEstudioAnt"));
        beca.setEstudioAnt(params.getFirst("estudioAnt"));
        beca.setTipoTitulacion(params.getFirst("tipoTitulacion"));
        beca.setCursoAcademicoAnt(ParamUtils.parseLong(params.getFirst("cursoAcademicoAnt")));
        beca.setCausaOtros(params.getFirst("causaOtros"));
        beca.setCodigoCentroAnt(params.getFirst("codigoCentroAnt"));
        beca.setNombreCentroAnt(params.getFirst("nombreCentroAnt"));
        beca.setCodTituAcceso(params.getFirst("codTituAcceso"));
        beca.setTituloEspanol(Boolean.parseBoolean(params.getFirst("tituloEspanol")));
        beca.setEstudiosHomologados(Boolean.parseBoolean(params.getFirst("estudiosHomologados")));
        beca.setUnivTitulacion(params.getFirst("univTitulacion"));

        beca.setNotaMediaAnt(ParamUtils.parseFloat(params.getFirst("notaMediaAnt")));
        beca.setNotaMediaUltimoCurso(ParamUtils.parseFloat(params.getFirst("notaMediaUltimoCurso")));
        beca.setCreditosMatriculadosAnt(ParamUtils.parseFloat(params.getFirst("creditosMatriculadosAnt")));
        beca.setCreditosSuperadosAnt(ParamUtils.parseFloat(params.getFirst("creditosSuperadosAnt")));
        beca.setCreditosSuspensosAnt(ParamUtils.parseFloat(params.getFirst("creditosSuspensosAnt")));

        beca.setCreditosConvalidadosAnt(ParamUtils.parseFloat(params.getFirst("creditosConvalidadosAnt")));
        beca.setCreditosCovid19(ParamUtils.parseFloat(params.getFirst("creditosCovid19")));
        beca.setBecasConcedidasAnt(ParamUtils.parseLong(params.getFirst("becasConcedidasAnt")));
        beca.setMatriculaParcialAnt(Boolean.parseBoolean(params.getFirst("matriculaParcialAnt")));
        beca.setCumpleAcadBecaAnt(Boolean.parseBoolean(params.getFirst("cumpleAcadBecaAnt")));
        beca.setRendimientoAcademico(Boolean.parseBoolean(params.getFirst("rendimientoAcademico")));

        beca.setBecaCursoAnt(Boolean.parseBoolean(params.getFirst("becaCursoAnt")));
        beca.setBecaCursoAnt2(Boolean.parseBoolean(params.getFirst("becaCursoAnt2")));
        beca.setBecaCursoAnt3(Boolean.parseBoolean(params.getFirst("becaCursoAnt3")));

        beca.setCumpleAcademicos(Boolean.parseBoolean(params.getFirst("cumpleAcademicos")));
        beca.setCreditosMatriculados(ParamUtils.parseFloat(params.getFirst("creditosMatriculados")));
        beca.setCreditosConvalidados(ParamUtils.parseFloat(params.getFirst("creditosConvalidados")));
        beca.setCreditosPendientesConvalidacion(ParamUtils.parseFloat(params.getFirst("creditosPendientesConvalidacion")));
        beca.setCreditosFueraBeca(ParamUtils.parseFloat(params.getFirst("creditosFueraBeca")));
        beca.setCreditosParaBeca(ParamUtils.parseFloat(params.getFirst("creditosParaBeca")));
        beca.setNumeroSemestres(ParamUtils.parseLong(params.getFirst("numeroSemestres")));
        beca.setCurso(ParamUtils.parseLong(params.getFirst("curso")));
        beca.setBecaParcial(Boolean.parseBoolean(params.getFirst("becaParcial")));
        beca.setLimitarCreditos(Boolean.parseBoolean(params.getFirst("limitarCreditos")));
        beca.setMatriculaParcial(Boolean.parseBoolean(params.getFirst("matriculaParcial")));
        beca.setLimitarCreditosFinEstudios(Boolean.parseBoolean(params.getFirst("limitarCreditosFinEstudios")));
        beca.setPoseeTitulo(Boolean.parseBoolean(params.getFirst("poseeTitulo")));
        beca.setAlgunaBecaParcial(Boolean.parseBoolean(params.getFirst("algunaBecaParcial")));
        beca.setCumpleAcademicos(Boolean.parseBoolean(params.getFirst("cumpleAcademicos")));
        beca.setIbanPais(params.getFirst("ibanPais"));
        beca.setIbanDC(params.getFirst("ibandc"));
        beca.setProcedenciaNota(params.getFirst("procedenciaNota"));
        beca.setNotaPruebaEspecifica(ParamUtils.parseFloat(params.getFirst("notaPruebaEspecifica")));
        beca.setNotaMediaSinSuspenso(ParamUtils.parseFloat(params.getFirst("notaMediaSinSuspenso")));

        beca.setCreditosPrimeraMatricula(ParamUtils.parseFloat(params.getFirst("creditosPrimeraMatricula")));
        beca.setCreditosSegundaMatricula(ParamUtils.parseFloat(params.getFirst("creditosSegundaMatricula")));
        beca.setCreditosTerceraMatricula(ParamUtils.parseFloat(params.getFirst("creditosTerceraMatricula")));
        beca.setCreditosTerceraMatriculaPosteriores(ParamUtils.parseFloat(params.getFirst("creditosTerceraMatriculaPosteriores")));
        beca.setCreditosCuartaMatriculaPosteriores(ParamUtils.parseFloat(params.getFirst("creditosCuartaMatriculaPosteriores")));

        beca.setOtraBecaConcedida(Boolean.parseBoolean(params.getFirst("otraBecaConcedida")));

        beca.updateAcademicos();

        return Response.ok(new UIEntity()).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> updateBeca(@FormParam("id") String becaId,
                                     @FormParam("solicitanteId") String solicitanteId,
                                     @FormParam("convocatoria2Id") String convocatoriaId,
                                     @FormParam("identificacion") String identificacion,
                                     @FormParam("cursoAcademicoId") String cursoAcademicoId,
                                     @FormParam("codigoArchivoTemporal") String codigoArchivoTemporal)
            throws BecaDuplicadaException
    {

        this.checkMandatoryParams(becaId, solicitanteId, convocatoriaId, identificacion,
                cursoAcademicoId);

        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (codigoArchivoTemporal != null)
        {
            beca.setCodigoArchivoTemporal(codigoArchivoTemporal);
        }
        beca.setIdentificacion(identificacion);

        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(Long.parseLong(convocatoriaId));

        beca.setConvocatoria(convocatoria);

        beca = beca.update();

        UIEntity uiEntity = new UIEntity();
        uiEntity = UIEntity.toUI(beca);
        addAtributtesNoPrimitives(uiEntity, beca);

        return Collections.singletonList(uiEntity);
    }

    private void checkMandatoryParams(String becaId, String solicitanteId, String convocatoriaId,
                                      String identificacion, String cursoAcademicoId)
    {
        if (becaId == null || becaId.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        if (solicitanteId == null || solicitanteId.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        if (convocatoriaId == null || convocatoriaId.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        if (identificacion == null || identificacion.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        if (cursoAcademicoId == null || cursoAcademicoId.isEmpty())
        {
            throw new IllegalArgumentException();
        }
    }

    @Path("search")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getBecas(@QueryParam("filterParams") Map<String, String> filterParams)
            throws DemasiadasBecasException
    {
        if (existeAlgunCriterioDeBusquedaEnLosParametros(filterParams))
        {
            try
            {
                List<Beca> listaBecas = Solicitante.getSolicitantesConFiltros(filterParams);
                return modelToUI(listaBecas);
            }
            catch (PersistenceException e)
            {
                throw new DemasiadasBecasException();
            }
        }
        else
        {
            throw new DemasiadasBecasException();
        }
    }

    private List<UIEntity> modelToUI(List<Beca> listaBecas)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Beca beca : listaBecas)
        {
            result.add(modelToUI(beca));
        }
        return result;
    }

    private UIEntity modelToUI(Beca beca)
    {
        UIEntity entity = UIEntity.toUI(beca);

        entity.put("solicitanteId", beca.getSolicitante().getId());
        entity.put("cursoAca", beca.getSolicitante().getCursoAcademico().getId());
        entity.put("numeroBecaUji", beca.getSolicitante().getNumeroBecaUji());
        entity.put("nombreApellidos", beca.getSolicitante().getPersona().getNombre());
        entity.put("identificacion", beca.getSolicitante().getPersona().getIdentificacion());
        if (beca.getEstudio() != null)
        {
            entity.put("titulacion", beca.getEstudio().getId() + " - " + beca.getEstudio().getNombre());
        }
        else
        {
            entity.put("titulacion", "");
        }
        entity.put("convocatoria", beca.getConvocatoria().getNombre());
        entity.put("proceso", beca.getProceso().getNombre());
        entity.put("estado", beca.getEstado().getNombre());
        entity.put("organismoId", beca.getConvocatoria().getOrganismo().getId());
        if (beca.getTanda() != null)
        {
            entity.put("tandaOrdenId", beca.getTanda().getTandaId());
        }

        return entity;
    }

    private boolean existeAlgunCriterioDeBusquedaEnLosParametros(Map<String, String> filterParams)
    {
        boolean existe = false;
        List<String> excepciones = new ArrayList<String>();
        excepciones.add("_dc");
        excepciones.add("sinTanda");
        excepciones.add("reclamada");
        for (String key : filterParams.keySet())
        {
            if (!excepciones.contains(key))
            {
                String cadena = filterParams.get(key);
                if (cadena != null && !cadena.trim().isEmpty())
                {
                    existe = true;
                    break;
                }
            }
        }

        return existe;
    }

    @GET
    @Path("matricula")
    @Produces(MediaType.TEXT_PLAIN)
    public String esBecarioMinisterio(@QueryParam("curso") String cursoAcademico,
                                      @QueryParam("convocatoria") String convocatoria,
                                      @QueryParam("secuencia") String secuencia, @QueryParam("dni") String identificacion,
                                      @QueryParam("archivoTemporal") String codigoArchivoTemporal)
            throws NumberFormatException, RemoteException, MalformedURLException,
            WsDatosEnviadosParaBajarBecaErroneosException, SOAPException {
        byte[] result = ministerioWebService.getXmlSolicitudCiudadano(Integer.parseInt(cursoAcademico), convocatoria, Integer.parseInt(secuencia), identificacion, Long.parseLong(codigoArchivoTemporal));

        return new String(result).contains("<Error>") ? "N" : "S";
    }

    @GET
    @Path("{id}/estados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHistoricoEstadosByBeca(@PathParam("id") String becaId)
    {
        List<HistoricoBeca> listaEstados = Beca.getHistoricoEstadosByBeca(Long.parseLong(becaId));

        return preparaListaUIEntityEstados(listaEstados);
    }

    private List<UIEntity> preparaListaUIEntityEstados(List<HistoricoBeca> listaEstados)
    {
        ArrayList<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (HistoricoBeca historico : listaEstados)
        {
            UIEntity ui = UIEntity.toUI(historico);
            ui.put("convocatoria", Convocatoria.getConvocatoriaById(historico.getConvocatoriaId()).getNombre());
            ui.put("proceso", Proceso.getProcesoById(historico.getProcesoId()).getNombre());
            ui.put("estado", Estado.getEstadoById(historico.getEstadoId()).getNombre());
            if (historico.getTandaId() != null)
            {
                Tanda tanda = Tanda.getTandaById(historico.getTandaId());
                if (tanda != null)
                {
                    ui.put("tanda", Tanda.getTandaById(historico.getTandaId()).getTandaId());
                }
                else
                {
                    ui.put("tanda", "Esborrada");
                }
            }

            listaUI.add(ui);
        }

        return listaUI;
    }

    @GET
    @Path("{id}/cuantiacursoconvocatoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCuantiasCurso(@PathParam("id") String becaId)
    {
        if (Long.parseLong(becaId) < 0) {
            return new ArrayList<UIEntity>();
        }
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        Long convocatoriaId = beca.getConvocatoria().getId();
        Long cursoAcademicoId = beca.getSolicitante().getCursoAcademico().getId();
        List<CuantiaCurso> cuantiasPorCurso = CuantiaCurso.getCuantiasCursoByCursoAcademicoyConvocatoria(cursoAcademicoId, convocatoriaId);
        ArrayList<UIEntity> listaUI = preparaListaEntidades(cuantiasPorCurso);
        return listaUI;
    }

    private ArrayList<UIEntity> preparaListaEntidades(List<CuantiaCurso> cuantiasPorCurso)
    {
        ArrayList<UIEntity> listaUI = new ArrayList<UIEntity>();
        for (CuantiaCurso cuantiaPorCurso : cuantiasPorCurso)
        {
            UIEntity ui = UIEntity.toUI(cuantiaPorCurso);
            ui.put("nombre", cuantiaPorCurso.getCuantia().getNombre());
            ui.put("codigo", cuantiaPorCurso.getCuantia().getCodigo());
            listaUI.add(ui);
        }
        return listaUI;
    }

    @POST
    @Path("{id}/propuestaayuda")
    @Produces(MediaType.TEXT_XML)
    public Response InsertaPropuestaCuantia(@PathParam("id") String becaId)
            throws BecaCuantiaDuplicadaException, BecaDuplicadaException,
            AyudaNoAplicableException, BecaSinEstudioException,
            SolicitanteSinDomicilioFamiliarException, BecaConDenegacionesException,
            DistanciaLocalidadNoEncontradaException, AcademicosNoCargadosException,
            BecaConTandaNoPuedeCambiarConvocatoriaException,
            ConvocatoriaDeTandaYDeBecaDiferentesException,
            DistanciaDomicilioFamiliarNoEncontradaException
    {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        Beca becaDataBase = SerializationUtils.clone(beca);

        List<BecaDenegacion> denegacionesByBeca = BecaDenegacion.getDenegacionesByBecaId(beca.getId());

        if (!denegacionesByBeca.isEmpty())
        {
            throw new BecaConDenegacionesException();
        }

        beca.insertaAyudas();

        HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase,
                AccessManager.getConnectedUser(request).getName());

        return Response.ok().build();
    }

    @POST
    @Path("{id}/ministerio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getBecaMinisterio(@PathParam("id") String becaId, MultivaluedMap<String, String> params)
            throws JAXBException, ServiceException, SAXException, IOException, ParserConfigurationException,
            BecaDuplicadaException, EstudioSolicitadoNoMatriculadoException, RegistroNoEncontradoException,
            WsFaltaCodigoDeArchivoTemporalException, WsDatosEnviadosParaBajarBecaErroneosException,
            EstudioMinisterioMayores25Exception, BecaException,
            EstadoBecaNoPermiteBajarDelMinisteriorException, SolicitudTypeDatosDuplicadosException, BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException, SOAPException {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (beca.getEstado().getId() != CodigoEstado.PENDIENTE.getId()
                && beca.getEstado().getId() != CodigoEstado.INCORRECTA.getId())
        {
            throw new EstadoBecaNoPermiteBajarDelMinisteriorException();
        }

        Long solicitanteId = ParamUtils.parseLong(params.getFirst("solicitanteId"));
        Integer cursoAcademico = Integer.parseInt(params.getFirst("cursoAcademico"));
        Long convocatoriaId = ParamUtils.parseLong(params.getFirst("convocatoriaId"));
        Integer secuencia = 1;
        String identificacion = params.getFirst("identificacion");
        Long codigoArchivoTemporal = ParamUtils.parseLong(params.getFirst("codigoArchivoTemporal"));

        Convocatoria convocatoria = Convocatoria.getConvocatoriaById(convocatoriaId);

        if (convocatoria.getOrganismo().getId() == CodigoOrganismo.MINISTERIO.getId())
        {
            SolicitudType solicitudType = ministerioWebService.getXmlSolicitudCiudadanoSolicitudType(cursoAcademico, CONVOCATORIA_GENERAL, secuencia, identificacion, codigoArchivoTemporal);

            descargaSolicitudes.insertaNuevaSolicitud(solicitudType);

            recuperaDatosSolicitante(solicitanteId, solicitudType);
            recuperaDatosBeca(becaId, solicitudType);
        }
        else
        {
            throw new BecaException("La beca es de Conselleria");
        }

        return Response.ok(new UIEntity()).build();
    }

    private void recuperaDatosBeca(String becaId, SolicitudType solicitudType)
            throws BecaDuplicadaException, EstudioSolicitadoNoMatriculadoException,
            EstudioMinisterioMayores25Exception, RegistroNoEncontradoException, BecaDenegacionDuplicadaException, BecaDenegacionNoEncontradaException {
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        boolean completadoParcialmente = false;

        try
        {
            beca.importarDatosMinisterio(solicitudType);
        }
        catch (EstudioMinisterioMayores25Exception e)
        {
            completadoParcialmente = true;
        }

        beca.update();

        if (completadoParcialmente)
        {
            throw new EstudioMinisterioMayores25Exception();
        }
    }

    private void recuperaDatosSolicitante(Long solicitanteId, SolicitudType solicitudType)
            throws RegistroNoEncontradoException
    {
        Solicitante solicitante = Solicitante.getSolicitanteById(solicitanteId);
        solicitante.importarDatosMinisterio(solicitudType);
        solicitante.update();
    }

    /*
     * Subrecursos externos
     */
    @Path("{becaId}/denegacion")
    public BecaDenegacionSubResourceService getBecaDenegacionSubResourceService(
            @InjectParam BecaDenegacionSubResourceService becaDenegacionSubResourceService)
    {
        return becaDenegacionSubResourceService;
    }

    @Path("{becaId}/cuantia")
    public BecaCuantiaSubResourceService getBecaCuantiaSubResourceService(
            @InjectParam BecaCuantiaSubResourceService becaCuantiaSubResourceService)
    {
        return becaCuantiaSubResourceService;
    }

    @Path("{becaId}/documento")
    public BecaDocumentoSubResourceService getBecaDocumentoSubResourceService(
            @InjectParam BecaDocumentoSubResourceService becaDocumentoSubResourceService)
    {
        return becaDocumentoSubResourceService;
    }

    @Path("{becaId}/mensaje")
    public BecaMensajeSubResourceService getBecaMensajeSubResourceService(
            @InjectParam BecaMensajeSubResourceService becaMensajeSubResourceService)
    {
        return becaMensajeSubResourceService;
    }

    @Path("{becaId}/observaciones")
    public BecaObservacionSubResourceService getBecaObservacionSubResourceService(
            @InjectParam BecaObservacionSubResourceService becaObservacionSubResourceService)
    {
        return becaObservacionSubResourceService;
    }

    @Path("{becaId}/recurso/pdf")
    public BecaPDFRecursoSubResourceService getBecaPDF(
            @InjectParam BecaPDFRecursoSubResourceService becaPDFRecursoSubResourceService)
    {
        return becaPDFRecursoSubResourceService;
    }

    @Path("{becaId}/reclamacion")
    public BecaReclamacionSubResourceService getBecaReclamacionSubResourceService(
            @InjectParam BecaReclamacionSubResourceService becaReclamacionSubResourceService)
    {
        return becaReclamacionSubResourceService;
    }
}
