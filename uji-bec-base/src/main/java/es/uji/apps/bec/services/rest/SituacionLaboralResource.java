package es.uji.apps.bec.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.SituacionLaboralService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("situacionlaboral")
public class SituacionLaboralResource extends CoreBaseService
{
    @InjectParam
    private SituacionLaboralService situacionLaboralService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSituacionesLaborales()
    {
        return situacionLaboralService.getSituacionesLaborales();
    }
}
