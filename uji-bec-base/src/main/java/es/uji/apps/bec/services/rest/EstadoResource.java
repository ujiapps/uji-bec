package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.EstadoService;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("estado")
public class EstadoResource extends CoreBaseService
{
    @InjectParam
    private EstadoService estadoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstados()
    {
        return estadoService.getEstados();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstadoById(@PathParam("id") Long estadoId)
    {
        return estadoService.getEstadoById(estadoId);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaEstado(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return estadoService.insertaEstado(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaEstado(@PathParam("id") Long estadoId,
            UIEntity entity)
    {
        return estadoService.actualizaEstado(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraEstado(@PathParam("id") Long estadoId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return estadoService.borraEstado(estadoId);
    }

    @GET
    @Path("resumenportanda/{tandaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Resumen> getResumenEstadosPorTanda(@PathParam("tandaId") Long tandaId)
    {

        return estadoService.getResumenEstadosPorTanda(tandaId);
    }
}
