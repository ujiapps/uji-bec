package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoFamiliaDAO;
import es.uji.apps.bec.model.TipoFamilia;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoFamiliaService extends CoreBaseService
{
    private TipoFamiliaDAO tipoFamiliaDAO;

    @Autowired
    public TipoFamiliaService(TipoFamiliaDAO tipoFamiliaDAO)
    {
        this.tipoFamiliaDAO = tipoFamiliaDAO;
    }

    public List<UIEntity> getTiposFamilias()
    {
        List<TipoFamilia> tiposFamilias = tipoFamiliaDAO.getTiposFamilias();
        return UIEntity.toUI(tiposFamilias);
    }

    public UIEntity insertaTipoFamilia(UIEntity entity)
    {
        TipoFamilia tipoFamilia = preparaTipoFamilia(entity);
        if (tipoFamilia.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoFamiliaDAO.insert(tipoFamilia));
    }

    public ResponseMessage actualizaTipoFamilia(UIEntity entity)
    {
        TipoFamilia tipoFamilia = preparaTipoFamilia(entity);

        tipoFamiliaDAO.update(tipoFamilia);

        return new ResponseMessage(true);
    }

    private TipoFamilia preparaTipoFamilia(UIEntity entity)
    {
        TipoFamilia tipoFamilia = new TipoFamilia();
        tipoFamilia.setId(ParamUtils.parseLong(entity.get("id")));
        tipoFamilia.setNombre(entity.get("nombre"));
        tipoFamilia.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoFamilia;
    }

    public ResponseMessage borraTipoFamilia(Long tipoFamiliaId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoFamilia tipoFamilia = tipoFamiliaDAO.getTipoFamiliaById(tipoFamiliaId);

        if (tipoFamilia == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoFamiliaDAO.deleteTipoFamilia(tipoFamilia.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }
}
