package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoMiembroDAO;
import es.uji.apps.bec.model.TipoMiembro;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoMiembroService
{
    private TipoMiembroDAO tipoMiembroDAO;

    @Autowired
    public TipoMiembroService(TipoMiembroDAO tipoMiembroDAO)
    {
        this.tipoMiembroDAO = tipoMiembroDAO;
    }

    public List<UIEntity> getTiposMiembros()
    {
        List<TipoMiembro> tiposMiembros = tipoMiembroDAO.getTiposMiembros();
        return UIEntity.toUI(tiposMiembros);
    }

    public UIEntity insertaTipoMiembro(UIEntity entity)
    {
        TipoMiembro tipoMiembro = preparaTipoMiembro(entity);
        if (tipoMiembro.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoMiembroDAO.insert(tipoMiembro));
    }

    public ResponseMessage actualizaTipoMiembro(UIEntity entity)
    {
        TipoMiembro tipoMiembro = preparaTipoMiembro(entity);

        tipoMiembroDAO.update(tipoMiembro);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoMiembro(Long tipoMiembroId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoMiembro tipoMiembro = tipoMiembroDAO.getTipoMiembroById(tipoMiembroId);

        if (tipoMiembro == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoMiembroDAO.deleteTipoMiembro(tipoMiembro.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoMiembro preparaTipoMiembro(UIEntity entity)
    {
        TipoMiembro tipoMiembro = new TipoMiembro();
        tipoMiembro.setId(ParamUtils.parseLong(entity.get("id")));
        tipoMiembro.setNombre(entity.get("nombre"));
        tipoMiembro.setOrden(ParamUtils.parseLong(entity.get("orden")));
        tipoMiembro.setSolicitante(Boolean.parseBoolean(entity.get("solicitante")));
        return tipoMiembro;
    }
}
