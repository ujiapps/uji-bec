package es.uji.apps.bec.services;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import es.uji.apps.bec.dao.dao.PreparaTandaParaEnvioAcademicos;
import es.uji.apps.bec.exceptions.*;
import es.uji.apps.bec.model.*;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.apps.bec.webservices.*;
import es.uji.apps.bec.webservices.ministerio.academicos.DatosAcademicos;
import es.uji.apps.bec.webservices.ministerio.errores.MultienvioType;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.dao.dao.PreparaTandaParaEnvio;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Service
@Path("ministerio")
public class MinisterioService extends CoreBaseService {
    private static final Logger log = Logger.getLogger(MinisterioService.class);
    private static final long ESTADO_INCORRECTO = -1L;
    public static final String CONVOCATORIA_INCIDENCIAS = "IN";
    public static final String CONVOCATORIA_FAMILIASNUMEROSAS = "FN";
    public static final String CONVOCATORIA_GENERAL = "AE";

    @InjectParam
    private ResultadosCargaSolicitudes resultadosCargaSolicitudes;

    @InjectParam
    private ResultadosCargaAcademicos resultadosCargaAcademicos;

    @InjectParam
    private DatosSolicitudes datosSolicitudes;

    @InjectParam
    private EnvioDenegaciones envioDenegaciones;

    @InjectParam
    private DatosSolicitudesAcademicos datosAcademicos;

    @InjectParam
    private LotesResueltosMinisterio lotesResueltosMinisterio;

    @InjectParam
    private DescargaSolicitudes descargaSolicitudes;

    @InjectParam
    private AcademicosService academicosService;


    @POST
    @Path("webservices/tanda/{tandaId}/calcular/academicos")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage calcularAcademicosTanda(@PathParam("tandaId") Long tandaId) throws Exception {
        List<Beca> listaBecas = Beca.getBecasByTandaId(tandaId);
        if (listaBecas.size() == 0) {
            return new ResponseMessage(false);
        }

        int becasAcademicosCalculados = 0;
        int becasIncorrectas = 0;
        for (Beca beca : listaBecas) {
            ResponseMessage responseMessage = null;
            try {
                responseMessage = academicosService.actualizaAcademicosCursoActual(beca.getId());
                responseMessage = academicosService.actualizaAcademicosCursoAnterior(beca.getId());
                beca.setEstado(Estado.getEstadoById(CodigoEstado.ACADEMICOS_CALCULADOS.getId()));
                beca.updateEstado();
                becasAcademicosCalculados = becasAcademicosCalculados + 1;
            } catch (Exception e) {
                beca.setEstado(Estado.getEstadoById(CodigoEstado.INCORRECTA.getId()));
                beca.setTanda(null);
                beca.updateTanda();
                beca.updateEstado();
                System.out.println(e);
                becasIncorrectas = becasIncorrectas + 1;
                if (responseMessage != null) {
                    System.out.println(responseMessage.getMessage());
                }
            }
        }

        String mensaje = String.valueOf(becasAcademicosCalculados)  + " beques calculades correctament\n"
                + String.valueOf(becasIncorrectas) + " beques marcades com a incorrectes i extretes de la tanda.";
        return new ResponseMessage(true, mensaje);
    }

    @POST
    @Path("webservices/tanda/{tandaId}/preparar/academicos")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage prepararEnvioAcademicos(@PathParam("tandaId") String tandaId)
            throws WsErrorAlPrepararMultienvioException, NumberFormatException {
        Tanda tanda = Tanda.getTandaById(Long.parseLong(tandaId));

        Long personaId = AccessManager.getConnectedUserId(request);
        String funcionPLSQL = "pack_multienvio_academicos";
        RegistroEnvio registroEnvio = preparaRegistroEnvioAcademicos(tanda, personaId);
        return preparaAcademicos(tanda, registroEnvio.getId(), funcionPLSQL);
    }

    @POST
    @Path("webservices/tanda/{tandaId}/preparar")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage prepararEnvioTanda(@PathParam("tandaId") String tandaId)
            throws WsErrorAlPrepararMultienvioException, NumberFormatException {
        Tanda tanda = Tanda.getTandaById(Long.parseLong(tandaId));

        if (tanda.isCorrectaParaEnviar()) {
            Long personaId = AccessManager.getConnectedUserId(request);
            if (tanda.isTrabajada()) {
                String funcionPLSQL = "pack_multienvio";
                RegistroEnvio registroEnvio = preparaRegistroEnvio(tanda, personaId, funcionPLSQL);
                return preparaMultienvio(tanda, registroEnvio.getId(), funcionPLSQL);
            }

            if (tanda.isDenegadaAcademicos()) {
                String funcionPLSQL = "pack_multienvio_denegaciones";
                RegistroEnvio registroEnvio = preparaRegistroEnvio(tanda, personaId, funcionPLSQL);
                return preparaMultienvio(tanda, registroEnvio.getId(), funcionPLSQL);
            }
        }

        throw new WsErrorAlPrepararMultienvioException("Tanda no correcta per a ser enviada");
    }

    @POST
    @Path("webservices/tanda/{tandaId}/cargar")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage cargarEnvioTanda(@PathParam("tandaId") Long tandaId) throws Exception {
        Tanda tanda = Tanda.getTandaById(tandaId);
        if (tanda.isTrabajada()) {
            datosSolicitudes.cargar(tanda.getId());
        }

        if (tanda.isDenegadaAcademicos()) {
            envioDenegaciones.cargar(tanda.getId());
        }

        return new ResponseMessage(true);
    }

    @POST
    @Path("webservices/tanda/{tandaId}/cargar/academicos")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage cargarEnvioTandaAcademicos(@PathParam("tandaId") Long tandaId) throws Exception {
        Tanda tanda = Tanda.getTandaById(tandaId);

        Long loteAcademicosMEC = datosAcademicos.cargar(tanda.getId());
        log.info("Lote cargado en el MEC de académicos: " + String.valueOf(loteAcademicosMEC));
        marcaBecasComoAcademicosEnviados(tanda);
        marcaTandaComoAcademicosEnviados(tanda);
        guardaNumeroLoteAcademicos(tanda, loteAcademicosMEC);

        return new ResponseMessage(true);
    }

    private void guardaNumeroLoteAcademicos(Tanda tanda, Long loteAcademicosMEC) {
        Long tandaId = tanda.getId();
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tandaId);

        registroEnvio.setEnvioId(loteAcademicosMEC);
        registroEnvio.update();
    }

    private void marcaBecasComoAcademicosEnviados(Tanda tanda) {
        List<Beca> listaBecas = Beca.getBecasByTandaId(tanda.getId());
        if (listaBecas.size() > 0)
        {
            for (Iterator<Beca> iterator = listaBecas.iterator(); iterator.hasNext(); )
            {
                Beca beca = (Beca) iterator.next();
                Beca becaDataBase = SerializationUtils.clone(beca);

                if (laBecaNoEstaEnEstadoAcademicosEnviado(beca))
                {
                    beca.setEstado(Estado.getEstadoById(CodigoEstado.ACADEMICOS_ENVIADOS.getId()));
                    beca.updateEstado();
                    String usuarioConectado = AccessManager.getConnectedUser(request).getName();
                    HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
                }
            }
        }
    }

    private boolean laBecaNoEstaEnEstadoAcademicosEnviado(Beca beca)
    {
        return beca.getEstado().getId() != CodigoEstado.ACADEMICOS_ENVIADOS.getId();
    }

    public void marcaTandaComoAcademicosEnviados(Tanda tanda)
    {
        tanda.setActiva(Boolean.FALSE);
        Date ahora = new Date();
        tanda.setFecha(ahora);
        tanda.updateTanda();
    }

    @GET
    @Path("webservices/tanda/{tandaId}/multienvio")
    @Produces(MediaType.APPLICATION_XML)
    public String descargaXmlMultienvioTanda(@PathParam("tandaId") Long tandaId) throws Exception {
        Tanda tanda = Tanda.getTandaById(tandaId);
        if (tanda.isTrabajada()) {
            return datosSolicitudes.getXmlMultienvio(tanda.getId());
        }

        if (tanda.isDenegadaAcademicos()) {
            return envioDenegaciones.getXmlMultienvio(tanda.getId());
        }

        return null;
    }

    @POST
    @Path("webservices/tanda/{tandaId}/procesar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> procesarTanda(@PathParam("tandaId") Long tandaId) throws Exception {
        String resultadoExtraccion = null;
        Tanda tanda = Tanda.getTandaById(tandaId);

        if (tandaNoPreparadaParaProcesar(tanda)) {
            throw new TandaNoPreparadaParaProcesarException();
        }

        MultienvioType resultados = resultadosCargaSolicitudes.obtenerResultados(tandaId);

        if (tanda.isTrabajada()) {
            resultadosCargaSolicitudes.marcarBecasCorrectasComoEnviadas(tandaId);
            resultadoExtraccion = resultadosCargaSolicitudes.marcaBecasIncorrectas(resultados);
        }

        if (tanda.isDenegadaAcademicos()) {
            resultadoExtraccion = resultadosCargaSolicitudes.marcaBecasIncorrectas(resultados);
        }

        resultadosCargaSolicitudes.setFechaEnvioEnTanda(tandaId);

        UIEntity uiEntity = preparaUIEntityBecasExtraidas(resultadoExtraccion);
        return Collections.singletonList(uiEntity);
    }

    @POST
    @Path("webservices/tanda/{tandaId}/procesar/academicos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> procesarTandaAcademicos(@PathParam("tandaId") Long tandaId) throws Exception {
        String resultadoExtraccion = null;
        Tanda tanda = Tanda.getTandaById(tandaId);

        DatosAcademicos resultados = resultadosCargaAcademicos.obtenerResultados(tandaId);

        UIEntity uiEntity = preparaUIEntityBecasExtraidas(resultadoExtraccion);
        return Collections.singletonList(uiEntity);
/*
        MultienvioType resultados = resultadosCargaSolicitudes.obtenerResultados(tandaId);

        if (tanda.isTrabajada()) {
            resultadosCargaSolicitudes.marcarBecasCorrectasComoEnviadas(tandaId);
            resultadoExtraccion = resultadosCargaSolicitudes.marcaBecasIncorrectas(resultados);
        }

        if (tanda.isDenegadaAcademicos()) {
            resultadoExtraccion = resultadosCargaSolicitudes.marcaBecasIncorrectas(resultados);
        }

        resultadosCargaSolicitudes.setFechaEnvioEnTanda(tandaId);

        UIEntity uiEntity = preparaUIEntityBecasExtraidas(resultadoExtraccion);
        return Collections.singletonList(uiEntity);

 */
    }

    private boolean tandaNoPreparadaParaProcesar(Tanda tanda)
            throws WsErrorAlPrepararMultienvioException {
        return !tanda.isTrabajada() && !tanda.isDenegadaAcademicos();
    }

    @GET
    @Path("webservices/tanda/{tandaId}/errores/pdf")
    @Produces("application/pdf")
    public byte[] obtenerPDFResultadosTanda(@PathParam("tandaId") String tandaId) throws Exception {
        try {
            return resultadosCargaSolicitudes.obtenerListadoErroresPDF(tandaId);
        } catch (Exception e) {
            throw new WsNoSeEncuentraPdfErroresMecException();
        }
    }

    @GET
    @Path("webservices/descargasolicitudes")
    @Produces(MediaType.APPLICATION_XML)
    public List<ResultadoDescarga> descargaSolicitudes(@QueryParam("cursoaca") Integer cursoAca,
                                                       @QueryParam("convocatoria") String convocatoria,
                                                       @QueryParam("descargadas") String descargadas) throws Exception {
        List<ResultadoDescarga> resultadosDescarga = descargaSolicitudes.descarga(cursoAca, convocatoria, Boolean.parseBoolean(descargadas));
        return resultadosDescarga;
    }

    @POST
    @Path("denegaciones/{multienvioId}")
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    public ResponseMessage generaDenegaciones(@PathParam("multienvioId") String multienvioId)
            throws Exception {
        byte[] datosDenegaciones = envioDenegaciones.getDenegaciones(Long.parseLong(multienvioId));

        FileOutputStream output = new FileOutputStream("/tmp/denegaciones.xml");
        output.write(datosDenegaciones);
        output.flush();
        output.close();

        return new ResponseMessage(true);
    }

    @GET
    @Path("resoluciones/listalotes")
    @Produces(MediaType.APPLICATION_XML)
    public String obtenerListaLotesResolucionesXML(@QueryParam("cursoaca") String cursoAca,
                                                   @QueryParam("convocatoria") String convocatoria)
            throws NumberFormatException, IOException, SOAPException {
        return lotesResueltosMinisterio.obtenerListaLotesResolucionesXML(Long.parseLong(cursoAca), convocatoria);
    }

    @GET
    @Path("resoluciones/bajalote/{lote}")
    @Produces(MediaType.APPLICATION_XML)
    public ResponseMessage descargaLoteResolucionesMec(@PathParam("lote") String lote,
                                                       @QueryParam("convocatoria") String convocatoria, @QueryParam("cursoaca") String cursoAca)
            throws Exception {
        lotesResueltosMinisterio.descargarLoteResoluciones(lote, convocatoria, cursoAca);

        return new ResponseMessage(true);
    }

    @GET
    @Path("resoluciones/procesalote/{multienvio}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage procesaLoteResolucionesMec(@PathParam("multienvio") String multienvio)
            throws BecaDenegacionDuplicadaException, BecaCuantiaDuplicadaException,
            NumberFormatException, ConvocatoriaDeTandaYDeBecaDiferentesException, BecaCuantiaNoExisteException {
        String usuario = AccessManager.getConnectedUser(request).getName();

        lotesResueltosMinisterio.procesaLoteResoluciones(Long.parseLong(multienvio), usuario);

        return new ResponseMessage(true);
    }

    private byte[] generaFicheroEnvio(byte[] datos, Map<String, String> fichero) {
        try {
            datos = fichero.get("datos").getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            log.error("Error decoding ISO-8859-1 data", e);
        }
        return datos;
    }

    @GET
    @Path("compensacion/conselleria/{cursoAcademicoId}")
    public Response generaArchivoMinisterioCompensacionConselleria(@PathParam("cursoAcademicoId") String cursoAcademicoId) {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);

        byte[] datos = null;

        Map<String, String> fichero = ficheroPreciosPublicos.getLineaRegistroCompensatoria(ParamUtils.parseLong(cursoAcademicoId));
        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/conselleria/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generaPDFMinisterioCompensacionConselleria(@PathParam("cursoAcademicoId") String cursoAcademicoId) throws DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);

        //List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraCompensacionGVA(ParamUtils.parseLong(cursoAcademicoId), "G", "T");
        Map<String, Object> datosBecas = ficheroPreciosPublicos.getEstructuraCompensacionGVA(ParamUtils.parseLong(cursoAcademicoId));


        // La plantilla debería de ser como esta: https://ujiapps.uji.es/bec/rest/conselleria/todas/concedidas/2020/pdf

        Template template = new PDFTemplate("bec/importesMECCompensacionGVA", new Locale("ES"), "bec");
        template.put("datos", datosBecas);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("compensacion/familiasNumerosas/{cursoAcademicoId}")
    public Response generaArchivoMinisterioFamiliasNumerosas(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws DNIIncorrectoException {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_FAMILIASNUMEROSAS);
        byte[] datos = null;
        Map<String, String> fichero = ficheroPreciosPublicos.generaFicheroFamiliasNumerosasParaCursoAcademico(Long.parseLong(cursoAcademicoId), CONVOCATORIA_FAMILIASNUMEROSAS, "F");

        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/familiasNumerosas/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFPreciosPublicosFamiliasNumerosas(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_FAMILIASNUMEROSAS);

        List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraTxtPorCursoAcademicoFamiliasNumerosas(Long.parseLong(cursoAcademicoId), CONVOCATORIA_FAMILIASNUMEROSAS, "F");

        Template template = new PDFTemplate("bec/importesMEC", new Locale("ES"), "bec");
        template.put("convocatorias", convocatorias);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("compensacion/incidencias/{cursoAcademicoId}")
    public Response generaArchivoMinisterioIncidencias(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws DNIIncorrectoException {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_INCIDENCIAS);
        byte[] datos = null;
        Map<String, String> fichero = ficheroPreciosPublicos.generaFicheroIncidenciasParaCursoAcademico(Long.parseLong(cursoAcademicoId), CONVOCATORIA_INCIDENCIAS, "I");

        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/incidencias/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFPreciosPublicosIncidencias(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_INCIDENCIAS);

        List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraTxtPorCursoAcademico(Long.parseLong(cursoAcademicoId), CONVOCATORIA_INCIDENCIAS, "I");

        Template template = new PDFTemplate("bec/importesMEC", new Locale("ES"), "bec");
        template.put("convocatorias", convocatorias);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("compensacion/{cursoAcademicoId}")
    public Response generaArchivoMinisterioPreciosPublicos(
            @PathParam("cursoAcademicoId") String cursoAcademicoId) throws DNIIncorrectoException {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);
        byte[] datos = null;
        Map<String, String> fichero = ficheroPreciosPublicos.generaFicheroParaCursoAcademico(Long.parseLong(cursoAcademicoId), "GM", CONVOCATORIA_GENERAL, "T");

        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/grados/{cursoAcademicoId}")
    public Response generaArchivoMinisterioPreciosPublicosGrados(
            @PathParam("cursoAcademicoId") String cursoAcademicoId) throws DNIIncorrectoException {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);
        byte[] datos = null;
        Map<String, String> fichero = ficheroPreciosPublicos.generaFicheroParaCursoAcademico(Long.parseLong(cursoAcademicoId), "G", CONVOCATORIA_GENERAL, "T");

        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/master/{cursoAcademicoId}")
    public Response generaArchivoMinisterioPreciosPublicosMaster(
            @PathParam("cursoAcademicoId") String cursoAcademicoId) throws DNIIncorrectoException {

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);
        byte[] datos = null;
        Map<String, String> fichero = ficheroPreciosPublicos.generaFicheroParaCursoAcademico(Long.parseLong(cursoAcademicoId), "M", CONVOCATORIA_GENERAL, "T");

        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);

        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }

    @GET
    @Path("compensacion/grados/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFPreciosPublicosMinisterioGrados(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);

        List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraPorCursoAcademicoYTipoEstudioOrdenadosPorConvocatoria(ParamUtils.parseLong(cursoAcademicoId), "G", "T");

        Template template = new PDFTemplate("bec/importesMEC", new Locale("ES"), "bec");
        template.put("convocatorias", convocatorias);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("compensacion/master/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFPreciosPublicosMinisterioMaster(
            @PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);

        List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraPorCursoAcademicoYTipoEstudioOrdenadosPorConvocatoria(ParamUtils.parseLong(cursoAcademicoId), "M", "T");

        Template template = new PDFTemplate("bec/importesMEC", new Locale("ES"), "bec");
        template.put("convocatorias", convocatorias);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("compensacion/estudios/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFPreciosPublicosMinisterioEstudios(@PathParam("cursoAcademicoId") String cursoAcademicoId)
            throws DNIIncorrectoException {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroPreciosPublicosMinisterio ficheroPreciosPublicos = new FicheroPreciosPublicosMinisterio(CONVOCATORIA_GENERAL);

        List<Map<String, Object>> convocatorias = ficheroPreciosPublicos.getEstructuraPorCursoAcademicoOrdenadosPorConvocatoria(ParamUtils.parseLong(cursoAcademicoId), "T");

        Template template = new PDFTemplate("bec/importesMEC", new Locale("ES"), "bec");
        template.put("convocatorias", convocatorias);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }


    private RegistroEnvio preparaRegistroEnvio(Tanda tanda, Long personaId, String funcionPLSQL)
            throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioByTanda(tanda.getId());
        registroEnvio.preparar(tanda, personaId);
        return registroEnvio;
    }

    private RegistroEnvio preparaRegistroEnvioAcademicos(Tanda tanda, Long personaId) throws WsErrorAlPrepararMultienvioException {
        RegistroEnvio registroEnvio = new RegistroEnvio();

        return registroEnvio.prepararAcademicos(tanda, personaId);
    }

    private ResponseMessage preparaMultienvio(Tanda tanda, Long registroId, String funcion)
            throws WsErrorAlPrepararMultienvioException {
        String mensaje;
        Long multiEnvio = null;

        PreparaTandaParaEnvio preparaTandaParaEnvio = new PreparaTandaParaEnvio(funcion);
        preparaTandaParaEnvio.init();

        try {
            multiEnvio = preparaTandaParaEnvio.getMultiEnvio(tanda.getId(), registroId);
            if (multiEnvio != null) {
                mensaje = "Dades de tanda preparades correctament";
            } else {
                mensaje = "Hi errors en la tanda";
            }
        } catch (Exception e) {
            RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioById(registroId);
            registroEnvio.setEstado(ESTADO_INCORRECTO);
            registroEnvio.update();

            throw new WsErrorAlPrepararMultienvioException("Error al generar les dades.\n"
                    + e.getMessage());
        }

        return new ResponseMessage(true, mensaje);
    }

    private ResponseMessage preparaAcademicos(Tanda tanda, Long registroId, String funcion)
            throws WsErrorAlPrepararMultienvioException {
        String mensaje;
        Long multiEnvioAcademicos = null;

        PreparaTandaParaEnvioAcademicos preparaTandaParaEnvioAcademicos = new PreparaTandaParaEnvioAcademicos(funcion);
        preparaTandaParaEnvioAcademicos.init();

        try {
            multiEnvioAcademicos = preparaTandaParaEnvioAcademicos.getMultiEnvioAcademicos(tanda.getId(), registroId);
            if (multiEnvioAcademicos != null) {
                mensaje = "Dades acadèmics de la tanda preparades correctament per a enviar";
            } else {
                mensaje = "Hi errors en la tanda";
            }
        } catch (Exception e) {
            RegistroEnvio registroEnvio = RegistroEnvio.getRegistroEnvioById(registroId);
            registroEnvio.setEstado(ESTADO_INCORRECTO);
            registroEnvio.update();

            throw new WsErrorAlPrepararMultienvioException("Error al generar les dades acadèmics.\n" + e.getMessage());
        }

        return new ResponseMessage(true, mensaje);
    }

    private UIEntity preparaUIEntityBecasExtraidas(String resultadoExtraccion) {
        UIEntity uiEntity = new UIEntity();
        uiEntity.setBaseClass("BecasExtraidas");
        uiEntity.put("resultado", resultadoExtraccion);
        return uiEntity;
    }
}
