package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.OrganismoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("organismo")
public class OrganismoResource extends CoreBaseService
{
    @InjectParam
    private OrganismoService organismoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganismos()
    {
        return organismoService.getOrganismos();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaOrganismo(UIEntity entity) throws GeneralBECException, ParseException
    {
        return organismoService.insertaOrganismo(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaOrganismo(@PathParam("id") Long organismoId, UIEntity entity)
    {
        return organismoService.actualizaOrganismo(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraOrganismo(@PathParam("id") Long organismoId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return organismoService.borraOrganismo(organismoId);
    }
}
