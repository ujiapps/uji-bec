package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.CuantiaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("cuantia")
public class CuantiaResource extends CoreBaseService
{
    @InjectParam
    private CuantiaService cuantiaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCuantias()
    {
        return cuantiaService.getCuantias();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/activa")
    public List<UIEntity> getCuantiasActivas()
    {
        return cuantiaService.getCuantiasActivas();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaCuantia(UIEntity entity) throws GeneralBECException, ParseException
    {
        String cuantiaId = entity.get("id");
        if (!ParamUtils.isNotNull(cuantiaId))
        {
            return new UIEntity();
        }

        return cuantiaService.insertaCuantia(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long cuantiaId) throws RegistroNoEncontradoException,
            RegistroConHijosException, GeneralBECException
    {
        return cuantiaService.borraCuantia(cuantiaId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaCuantia(@PathParam("id") Long cuantiaId, UIEntity entity)
            throws GeneralBECException, ParseException
    {
        return cuantiaService.actualizaCuantia(entity);
    }
}
