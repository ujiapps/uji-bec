package es.uji.apps.bec.services.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.model.PersonaEstudio;
import es.uji.apps.bec.services.PersonaEstudioService;
import es.uji.apps.bec.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("personaEstudio")
public class PersonaEstudioResource extends CoreBaseService
{
    @InjectParam
    private PersonaEstudioService personaEstudioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPersonaEstudioByBecaId(@QueryParam("becaId") Long becaId) throws BecaSinEstudioException {
        if (!ParamUtils.isNotNull(becaId))
        {
            return new UIEntity("PersonaEstudio");
        }

        PersonaEstudio personaEstudio = personaEstudioService.getPersonaEstudioByBecaId(becaId);
        return UIEntity.toUI(personaEstudio);
    }
}
