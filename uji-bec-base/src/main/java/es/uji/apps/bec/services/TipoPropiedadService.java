package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoPropiedadDAO;
import es.uji.apps.bec.model.TipoPropiedad;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoPropiedadService extends CoreBaseService
{
    private TipoPropiedadDAO tipoPropiedadDAO;

    @Autowired
    public TipoPropiedadService(TipoPropiedadDAO tipoPropiedadDAO)
    {
        this.tipoPropiedadDAO = tipoPropiedadDAO;
    }

    public List<UIEntity> getTiposPropiedades()
    {
        List<TipoPropiedad> tiposPropiedades = tipoPropiedadDAO.getTiposPropiedades();
        return UIEntity.toUI(tiposPropiedades);
    }

    public UIEntity insertaTipoPropiedad(UIEntity entity)
    {
        TipoPropiedad tipoPropiedad = preparaTipoPropiedad(entity);
        if (tipoPropiedad.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoPropiedadDAO.insert(tipoPropiedad));
    }

    public ResponseMessage actualizaTipoPropiedad(UIEntity entity)
    {
        TipoPropiedad tipoPropiedad = preparaTipoPropiedad(entity);

        tipoPropiedadDAO.update(tipoPropiedad);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoPropiedad(Long tipoPropiedadId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoPropiedad tipoPropiedad = tipoPropiedadDAO.getTipoPropiedadById(tipoPropiedadId);

        if (tipoPropiedad == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoPropiedadDAO.deleteTipoPropiedad(tipoPropiedad.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoPropiedad preparaTipoPropiedad(UIEntity entity)
    {
        TipoPropiedad tipoPropiedad = new TipoPropiedad();
        tipoPropiedad.setId(ParamUtils.parseLong(entity.get("id")));
        tipoPropiedad.setNombre(entity.get("nombre"));
        tipoPropiedad.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoPropiedad;
    }
}
