package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoMonedaService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipomoneda")
public class TipoMonedaResource
{
    @InjectParam
    private TipoMonedaService tipoMonedaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposMonedas()
    {
        return tipoMonedaService.getTiposMonedas();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoMoneda(UIEntity entity) throws GeneralBECException, ParseException
    {
        return tipoMonedaService.insertaTipoMoneda(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoMoneda(@PathParam("id") Long tipoMonedaId, UIEntity entity)
    {
        return tipoMonedaService.actualizaTipoMoneda(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoMoneda(@PathParam("id") Long tipoMonedaId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoMonedaService.borraTipoMoneda(tipoMonedaId);
    }
}
