package es.uji.apps.bec.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.bec.services.CuantiaCursoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("cuantiacurso")
public class CuantiaCursoResource extends CoreBaseService
{
    @InjectParam
    private CuantiaCursoService cuantiaCursoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCuantiasCurso()
    {
        return cuantiaCursoService.getCuantiasCurso();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaCuantiaCurso(UIEntity entity)
    {
        return cuantiaCursoService.insertaCuantiaCurso(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long cuantiaCursoId) throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return cuantiaCursoService.borraCuantiaCurso(cuantiaCursoId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaCuantiaCurso(@PathParam("id") Long cuantiaCursoId, UIEntity entity)
    {
        return cuantiaCursoService.actualizaCuantiaCurso(entity);
    }
}
