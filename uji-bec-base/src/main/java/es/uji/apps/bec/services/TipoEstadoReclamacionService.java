package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.TipoEstadoReclamacionDAO;
import es.uji.apps.bec.model.TipoEstadoReclamacion;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoEstadoReclamacionService
{
    private TipoEstadoReclamacionDAO tipoEstadoReclamacionDAO;

    @Autowired
    public TipoEstadoReclamacionService(TipoEstadoReclamacionDAO tipoEstadoReclamacionDAO)
    {
        this.tipoEstadoReclamacionDAO = tipoEstadoReclamacionDAO;
    }

    public List<UIEntity> getTiposEstadosReclamaciones()
    {
        List<TipoEstadoReclamacion> tiposDocumentos = tipoEstadoReclamacionDAO.getTiposEstadosReclamaciones();
        return UIEntity.toUI(tiposDocumentos);
    }
}
