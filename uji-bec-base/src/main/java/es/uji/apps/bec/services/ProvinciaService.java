package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.ProvinciaDAO;
import es.uji.apps.bec.model.Provincia;
import es.uji.commons.rest.UIEntity;

@Service
public class ProvinciaService
{
    private ProvinciaDAO provinciaDAO;

    @Autowired
    public ProvinciaService(ProvinciaDAO provinciaDAO)
    {
        this.provinciaDAO = provinciaDAO;
    }

    public List<UIEntity> getProvincias()
    {
        List<Provincia> provincias = provinciaDAO.getProvincias();
        return UIEntity.toUI(provincias);
    }
}
