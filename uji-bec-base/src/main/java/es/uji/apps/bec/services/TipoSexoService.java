package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoSexoDAO;
import es.uji.apps.bec.model.TipoSexo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Service
public class TipoSexoService extends CoreBaseService
{
    private TipoSexoDAO tipoSexoDAO;

    @Autowired
    public TipoSexoService(TipoSexoDAO tipoSexoDAO)
    {
        this.tipoSexoDAO = tipoSexoDAO;
    }

    public List<UIEntity> getTiposSexos()
    {
        List<TipoSexo> tiposSexos = tipoSexoDAO.getTiposSexos();
        return UIEntity.toUI(tiposSexos);
    }
}
