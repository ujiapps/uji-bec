package es.uji.apps.bec.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.RegistroErroresEnvio;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.Tanda;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("registroerroresenvio")
public class RegistroErroresEnvioService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getErroresEnvio(@QueryParam("queryParamMap") Map<String, String> filtros)
    {
        List<RegistroErroresEnvio> listaRegistroErroresEnvio = RegistroErroresEnvio
                .getErroresEnvioFiltrado(filtros);

        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        if (listaRegistroErroresEnvio == null)
        {
            return listaUI;
        }

        for (RegistroErroresEnvio registroErroresEnvio : listaRegistroErroresEnvio)
        {
            UIEntity entity = preparaEntityRegistroErrores(registroErroresEnvio);
            listaUI.add(entity);
        }

        return listaUI;
    }

    private UIEntity preparaEntityRegistroErrores(RegistroErroresEnvio registroErroresEnvio)
    {
        UIEntity entity = UIEntity.toUI(registroErroresEnvio);

        Beca beca = registroErroresEnvio.getBeca();
        Estudio estudio = beca.getEstudio();
        Tanda tanda = registroErroresEnvio.getTanda();
        CursoAcademico cursoAcademico = tanda.getCursoAcademico();
        Convocatoria convocatoria = tanda.getConvocatoria();
        Organismo organismo = convocatoria.getOrganismo();
        Solicitante solicitante = beca.getSolicitante();
        Persona persona = solicitante.getPersona();

        if (estudio != null)
        {
            entity.put("titulacion", estudio.getId() + " - " + estudio.getNombre());
        }
        entity.put("tanda", tanda.getTandaId());
        entity.put("cursoAca", cursoAcademico.getId());
        entity.put("organismoId", organismo.getId());
        entity.put("convocatoria", convocatoria.getNombre());
        entity.put("becaUji", solicitante.getNumeroBecaUji());
        entity.put("solicitanteId", solicitante.getId());
        entity.put("nombreApellidos", persona.getNombre());

        return entity;
    }
}
