package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.stereotype.Service;

import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.RamaNoExisteException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Diccionario;
import es.uji.apps.bec.model.Estudio;
import es.uji.apps.bec.model.Minimo;
import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.PersonaEstudio;
import es.uji.apps.bec.model.Solicitante;
import es.uji.apps.bec.model.domains.CodigoOrganismo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;

@Service
public class AcademicosService extends CoreBaseService {
    public static final Long CONVOCATORIA_BECA_SALARIO = 11L;

    public ResponseMessage actualizaAcademicosCursoActual(Long becaId) throws GeneralBECException {
        checkCodigoBecaProporcionado(becaId);

        Beca beca = Beca.getBecaById(becaId);
        checkBecaSePuedeModificar(beca);

        Persona persona = beca.getSolicitante().getPersona();
        Estudio estudio = beca.getEstudio();
        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();
        beca.getConvocatoria().getId();

        checkDatosBecaSuficientes(persona, estudio, cursoAcademico);

        PersonaEstudio academicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(persona.getId(), estudio.getId(), cursoAcademico.getId());

        checkDatosAcademicosSuficientes(academicos);

        beca.setCreditosMatriculados(academicos.getCreditosMatriculados());
        beca.setCreditosConvalidados(academicos.getCreditosConvalidados());
        beca.setCreditosPendientesConvalidacion(academicos.getCreditosPendientesConva());

        beca.setCreditosFueraBeca(academicos.getCreditosFueraBeca());
        beca.setCreditosParaBeca(academicos.getCreditosParaBeca());

        beca.setNumeroSemestres(academicos.getNumeroSemestres());
        beca.setCurso(academicos.getCurso());
        beca.setPoseeTitulo(academicos.isPoseeTitulo());
        beca.setAlgunaBecaParcial(academicos.isAlgunaBecaParcial());
        beca.setCreditosPrimeraMatricula(academicos.getCreditosPrimeraMatricula());
        beca.setCreditosSegundaMatricula(academicos.getCreditosSegundaMatricula());
        beca.setCreditosTerceraMatricula(academicos.getCreditosTerceraMatricula());
        beca.setCreditosTerceraMatriculaPosteriores(academicos.getCreditosTerceraMatriculaPosteriores());
        beca.setCreditosCuartaMatriculaPosteriores(academicos.getCreditosCuartaMatriculaPosteriores());

        Solicitante solicitante = Solicitante.getSolicitanteConBecasById(beca.getSolicitante().getId());

        if (solicitante.tieneBecasConcedidasDelMinisterio() && beca.getCreditosSegundaMatricula() > 0) {
            beca.setOtraBecaConcedida(true);
        }

        if (solicitante.tieneBecaMecSolicitada() && beca.getConvocatoria().getId() == CONVOCATORIA_BECA_SALARIO) {
            beca.setOtraBecaConcedida(true);
        }
        Minimo minimo = Minimo.getMinimosByEstudioId(estudio.getId(), cursoAcademico.getId());

        preparaBecaParcial(beca, academicos, minimo);
        preparaMatriculaParcial(beca, minimo);
        preparaLimitacionesCreditos(beca, academicos, minimo);

        beca.updateAcademicos();

        return new ResponseMessage(true);
    }

    private void checkCodigoBecaProporcionado(Long becaId) throws BecaException {
        if (becaId == null) {
            throw new BecaException("Cal indicar una beca");
        }
    }

    private void checkBecaSePuedeModificar(Beca beca) throws BecaEnEstadoNoModificableException {
        if (beca.noEsModificable()) {
            throw new BecaEnEstadoNoModificableException();
        }
    }

    private void checkDatosBecaSuficientes(Persona persona, Estudio estudio, CursoAcademico cursoAcademico)
            throws BecaException {
        if (persona == null || estudio == null || cursoAcademico == null) {
            throw new BecaException("No hi ha dades suficients per a actualitzar acadèmics del curs actual");
        }
    }

    private void checkDatosAcademicosSuficientes(PersonaEstudio academicos) throws BecaException {
        if (academicos.getId() == null) {
            throw new BecaException("No hi ha dades del curs actual");
        }
    }

    private void preparaBecaParcial(Beca beca, PersonaEstudio academicos, Minimo minimo) {
        if (esMayorOIgual(beca.getCreditosParaBeca(), minimo.getCreditosBecaCompleta())) {
            beca.setBecaParcial(false);
        }

        if (academicos.isLimitarCreditos()) {
            beca.setBecaParcial(false);
        }

        if (academicos.noLimitaCreditos() && esMenor(beca.getCreditosParaBeca(), minimo.getCreditosBecaCompleta())) {
            beca.setBecaParcial(false);
            if (esMayorOIgual(beca.getCreditosParaBeca(), minimo.getCreditosBecaParcial())) {
                beca.setBecaParcial(true);
            }
        }
    }

    private void preparaMatriculaParcial(Beca beca, Minimo minimo) {
        beca.setMatriculaParcial(true);

        if (esMayorOIgual(beca.getCreditosParaBeca(), minimo.getCreditosBecaCompleta())) {
            beca.setMatriculaParcial(false);
        }
    }

    private void preparaLimitacionesCreditos(Beca beca, PersonaEstudio academicos, Minimo minimo) {
        beca.setLimitarCreditos(academicos.isLimitarCreditos());
        beca.setLimitarCreditosFinEstudios(false);

        if (academicos.getCreditosParaBeca() >= academicos.getCreditosFaltantes()
                && academicos.getCreditosParaBeca() < minimo.getCreditosBecaCompleta()) {
            beca.setLimitarCreditosFinEstudios(true);
            beca.setBecaParcial(false);
            beca.setMatriculaParcial(false);
        }
    }

    private boolean esMayorOIgual(float valor1, float valor2) {
        return Float.compare(valor1, valor2) >= 0;
    }

    private boolean esMenor(float valor1, float valor2) {
        return !esMayorOIgual(valor1, valor2);
    }

    public ResponseMessage actualizaAcademicosCursoAnterior(Long becaId)
            throws BecaDuplicadaException, BecaException, RamaNoExisteException,
            BecaEnEstadoNoModificableException, BecaSinEstudioException {
        checkCodigoBecaProporcionado(becaId);

        Beca beca = Beca.getBecaById(becaId);
        checkBecaSePuedeModificar(beca);

        Persona persona = beca.getSolicitante().getPersona();
        Estudio estudio = beca.getEstudio();
        CursoAcademico cursoAcademico = beca.getSolicitante().getCursoAcademico();

        checkDatosBecaSuficientes(persona, estudio, cursoAcademico);

        PersonaEstudio academicos = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(persona.getId(), estudio.getId(), cursoAcademico.getId());

        checkDatosAcademicosSuficientes(academicos);
        numeroBecasConcedidas(beca, academicos);

        if (academicos.isPrimeraVez())
        // ES PRIMERO POR PRIMERA VEZ O ES TRASLADO O ES PRIMERO DE MASTER
        {
            valoresPorDefectoEnCursoAnterior(beca, academicos);

            // Quitamos el coeficiente corrector para los masters en abril de 2022 por orden de Leti
            beca.setNotaMediaAnt(academicos.getNotaAcceso());
            if (academicos.getNotaAcceso() == null) {
                throw new BecaException("No hi ha dades de la titulació d'accés");
            }
            beca.setNotaMediaAnt(academicos.getNotaAcceso());

            if (estudio.isTipoMaster()) {
                valoresPorDefectoEnCursoAnteriorMasterConselleria(beca, academicos);
            } else {
                beca.setNotaPruebaEspecifica(academicos.getNotaEspecifica());
                beca.setProcedenciaNota(academicos.getProcedenciaNotaId());
            }
        } else
        // NO ES PRIMERA VEZ
        {
            Long cursoAcademicoAnteriorUJI = academicos.getCursoAcademicoAnteriorUJI();

            if (beca.getCursoAcademicoAnt() == null) {
                beca.setCursoAcademicoAnt(cursoAcademicoAnteriorUJI);
            }

            if (cursoAcademicoAnteriorUJI == null || cursoAcademicoAnteriorUJI < beca.getCursoAcademicoAnt()) {
                throw new BecaException("No hi ha dades en la UJI del curs acadèmic anterior que s\'indica.");
            }

            PersonaEstudio academicosAnterior = PersonaEstudio.getAcademicosCursoActualbyPersonaEstudioYCursoAca(persona.getId(), estudio.getId(), cursoAcademicoAnteriorUJI);

            if (academicosAnterior.getId() == null) {
                academicosAnterior = buscaUltimoCursoUJI(persona.getId(), cursoAcademicoAnteriorUJI);
            }

            valoresNoPrimeraVezCursoAnterior(beca, academicosAnterior, academicos);
        }

        beca.updateAcademicos();

        return new ResponseMessage(true);
    }

    private PersonaEstudio buscaUltimoCursoUJI(Long personaId, Long cursoAcademicoAnteriorUjiId)
            throws BecaException {
        List<PersonaEstudio> listaAcademicosAnt = PersonaEstudio.getAcademicosByPersonaYCursoAca(personaId, cursoAcademicoAnteriorUjiId);

        if (listaAcademicosAnt.isEmpty()) {
            throw new BecaException("No hi ha dades en la UJI del curs acadèmic anterior.");
        }

        if (listaAcademicosAnt.size() > 1) {
            {
                throw new BecaException("Hi ha més d\'un estudi en el curs anterior. Ompli les dades manualment.");
            }
        } else {
            return listaAcademicosAnt.get(0);
        }
    }

    private void valoresNoPrimeraVezCursoAnterior(Beca beca, PersonaEstudio academicosAnterior, PersonaEstudio academicosActual)
            throws RamaNoExisteException, BecaException {
        Estudio estudio = Estudio.getEstudioById(academicosAnterior.getEstudio().getId());
        Long cursoAcademicoAnteriorId = academicosAnterior.getCursoAcademico().getId();
        beca.setCursoAcademicoAnt(cursoAcademicoAnteriorId);
        beca.setEstudioAnt(estudio.getNombre());

        beca.setCodigoEstudioAnt(estudio.getRuct());
        beca.setCreditosEstudioAnt(estudio.getCreditosTotales());
        beca.setPlanAnteriorNumCursos(estudio.getNumeroCursos());
        beca.setCodigoEstudioUjiAnt(estudio.getId().toString());
        beca.setNombreCentroAnt(estudio.getNombreCentro());
        beca.setEstudioAnt(estudio.getNombre());

        beca.setCodigoCentroAnt(estudio.getCodigoCentro());
        beca.setNotaMediaAnt(academicosAnterior.getNotaMedia());
        beca.setNotaMediaSinSuspenso(academicosAnterior.getMediaSinSuspensos());
        beca.setCreditosMatriculadosAnt(academicosAnterior.getCreditosParaBeca());
        beca.setCreditosSuperadosAnt(academicosAnterior.getCreditosSuperados());
        beca.setCreditosSuspensosAnt(porcentajeDeSuspensosAnterior(beca));
        beca.setCreditosConvalidadosAnt(academicosAnterior.getCreditosConvalidados());
        beca.setMatriculaParcialAnt(academicosAnterior.isMatriculaParcial());
        beca.setCumpleAcadBecaAnt(academicosAnterior.isCumpleAcadBecaAnt());

        if (beca.getConvocatoria().getOrganismo().getId() == 1L) {
            beca.setBecasConcedidasAnt(academicosAnterior.getNumeroBecasMec());
            beca.setBecaCursoAnt(academicosAnterior.isTieneBecaMec());
        } else {
            beca.setBecasConcedidasAnt(academicosAnterior.getNumeroBecasCons());
        }

        beca.setRendimientoAcademico(PersonaEstudio.isRendimientoAcademico(academicosActual, academicosAnterior));
    }

    private Float porcentajeDeSuspensosAnterior(Beca beca) {
        if (beca.getCreditosMatriculadosAnt() != null && beca.getCreditosMatriculadosAnt().compareTo(0F) != 0) {
            return (beca.getCreditosMatriculadosAnt() - beca.getCreditosSuperadosAnt()) * 100 / beca.getCreditosMatriculadosAnt();
        }
        return null;
    }

    private void valoresPorDefectoEnCursoAnterior(Beca beca, PersonaEstudio academicosActual)
            throws RamaNoExisteException, BecaException, BecaSinEstudioException {
        beca.setNotaMediaAnt(5F);
        beca.setCreditosMatriculadosAnt(0F);
        beca.setCreditosSuperadosAnt(0F);
        beca.setCreditosSuspensosAnt(0F);
        beca.setCreditosConvalidadosAnt(0F);


        beca.setMatriculaParcialAnt(false);
        beca.setCumpleAcadBecaAnt(true);
        beca.setRendimientoAcademico(PersonaEstudio.isRendimientoAcademico(academicosActual, null));
        beca.setBecaCursoAnt(false);
        beca.setBecaCursoAnt2(false);
        beca.setBecaCursoAnt3(false);

        if (beca.getEstudio() != null && beca.getEstudio().getTipo().equals("M")
                && beca.getCursoAcademicoAnt() == null && beca.getCurso() == 1L) {
            beca.setCursoAcademicoAnt(beca.getSolicitante().getCursoAcademico().getId() - 1L);
        }
    }

    private void numeroBecasConcedidas(Beca beca, PersonaEstudio academicosActual) throws BecaSinEstudioException {
        if (beca.getConvocatoria().getOrganismo().getId() == 1L) {
            beca.setBecasConcedidasAnt(academicosActual.getNumeroBecasMec());
        } else {
            beca.setBecasConcedidasAnt(academicosActual.getNumeroBecasCons());
        }
    }

    private void valoresPorDefectoEnCursoAnteriorMasterConselleria(Beca beca, PersonaEstudio academicosActual) throws
            BecaSinEstudioException {
        if (beca.getConvocatoria().getOrganismo().getId() == 2L) {
            if (academicosActual.getEstudioAcceso() == null) {
                throw new BecaSinEstudioException("No hi ha dades de l'estudi d'accés. Introduir la nota a mà");
            }
            beca.setCodigoEstudioUjiAnt(academicosActual.getEstudioAcceso().getId().toString());
            String valor = Diccionario.getValorOrigen(academicosActual.getCursoAcademico().getId(), "Estudio", 1L, academicosActual.getEstudioAcceso().getId().toString());
            beca.setCodigoEstudioAnt(valor);
            beca.setCodTituAcceso(valor);
        }
    }
}
