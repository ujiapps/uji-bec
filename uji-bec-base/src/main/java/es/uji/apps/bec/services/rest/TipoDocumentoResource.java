package es.uji.apps.bec.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoDocumentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

@Path("tipodocumento")
public class TipoDocumentoResource extends CoreBaseService
{
    @InjectParam
    private TipoDocumentoService tipoDocumentoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposDocumentos()
    {
        return tipoDocumentoService.getTiposDocumentos();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoDocumento(UIEntity entity) throws GeneralBECException, ParseException
    {
        return tipoDocumentoService.insertaTipoDocumento(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoDocumento(@PathParam("id") Long tipoDocumentoId, UIEntity entity)
    {
        return tipoDocumentoService.actualizaTipoDocumento(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoDocumento(@PathParam("id") Long tipoDocumentoId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoDocumentoService.borraTipoDocumento(tipoDocumentoId);
    }

}
