package es.uji.apps.bec.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TandaDAO;
import es.uji.apps.bec.exceptions.NotificacionesException;
import es.uji.apps.bec.exceptions.TandaConBecasAsignadasException;
import es.uji.apps.bec.exceptions.TandaDuplicadaParaConvocatoriaAndCursoAcaException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Tanda;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Service
public class TandaService extends CoreBaseService
{
    private TandaDAO tandaDAO;

    @Autowired
    public TandaService(TandaDAO tandaDAO)
    {
        this.tandaDAO = tandaDAO;
    }

    public List<UIEntity> getTandas(Map<String, String> queryParamMap)
    {
        List<Tanda> listaTandas = tandaDAO.getTandas(queryParamMap);
        ArrayList<UIEntity> listaUi = new ArrayList<UIEntity>();

        for (Tanda tanda : listaTandas)
        {
            UIEntity entity = UIEntity.toUI(tanda);
            String descripcion = tanda.getDescripcion();
            if (descripcion == null)
            {
                descripcion = "";
            }
            entity.put("tandaTexto", tanda.getTandaId() + " - " + descripcion);

            listaUi.add(entity);
        }
        return listaUi;
    }

    public ResponseMessage delete(Long id) throws RegistroNoEncontradoException,
            NumberFormatException, TandaConBecasAsignadasException
    {
        try
        {
            tandaDAO.delete(Tanda.class, id);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new TandaConBecasAsignadasException();
        }

        return new ResponseMessage(true);
    }

    public UIEntity updateTanda(Long id, UIEntity entity) throws ParseException,
            TandaDuplicadaParaConvocatoriaAndCursoAcaException
    {
        String usuario = AccessManager.getConnectedUser(request).getName();
        Tanda tanda = Tanda.getTandaById(id);
        preparaTanda(entity, tanda, usuario);
        try
        {
            tanda.updateTanda();
        }
        catch (DataIntegrityViolationException e)
        {
            throw new TandaDuplicadaParaConvocatoriaAndCursoAcaException();
        }

        return entity;
    }

    public UIEntity insertTanda(UIEntity entity) throws ParseException,
            TandaDuplicadaParaConvocatoriaAndCursoAcaException
    {
        if (entity.get("tandaId") == null)
        {
            return entity;
        }

        Tanda tanda = new Tanda();
        String usuario = AccessManager.getConnectedUser(request).getName();
        preparaTanda(entity, tanda, usuario);

        if (tanda.isTandaDuplicadaParaOrganismo())
        {
            throw new TandaDuplicadaParaConvocatoriaAndCursoAcaException(
                    "Tanda duplicada per a un organisme i curs acadèmic.");
        }

        try
        {
            tanda.insert();
        }
        catch (DataIntegrityViolationException e)
        {
            throw new TandaDuplicadaParaConvocatoriaAndCursoAcaException();
        }

        entity.put("id", tanda.getId());

        return entity;
    }

    public ResponseMessage enviaNotificaciones(Long id)
            throws NotificacionesException
    {
        String mensaje;
        Tanda tanda = Tanda.getTandaById(id);
        String usuario = AccessManager.getConnectedUser(request).getName();
        tanda.enviaNotificacion(usuario);

        Tanda tandaActualizada = Tanda.getTandaById(id);
        if (tandaActualizada.getFechaNotificacion() == null)
        {
            mensaje = "Hi ha becas que no han pogut ser notificades";
        }

        else
        {
            mensaje = "Notificacions enviades correctament";
        }
        return new ResponseMessage(true, mensaje);

    }

    public List<UIEntity> getBecasSinNotificar(Long tandaId)
    {
        Long numBecasSinNotificar = Beca.numeroBecasNoNotificadasPorTanda(tandaId);
        ArrayList<UIEntity> listaUi = new ArrayList<UIEntity>();

        UIEntity cuenta = new UIEntity();
        cuenta.setBaseClass("resumen");
        cuenta.put("etiqueta", "Becas sense notificar");
        cuenta.put("valor", numBecasSinNotificar);

        listaUi.add(cuenta);

        return listaUi;
    }

    private void preparaTanda(UIEntity entity, Tanda tanda, String usuario)
            throws NumberFormatException, ParseException
    {
        tanda.setTandaId(Long.parseLong(entity.get("tandaId")));
        tanda.setCursoAcademico(CursoAcademico.getCursoAcademicoById(Long.parseLong(entity.get("cursoAcademicoId"))));
        tanda.setConvocatoria(Convocatoria.getConvocatoriaById(Long.parseLong(entity.get("convocatoriaId"))));
        tanda.setDescripcion(entity.get("descripcion"));
        tanda.setUsuario(usuario);

        if (entity.get("fecha") == null)
        {
            tanda.setFecha(null);
        }
        else
        {
            tanda.setFecha(entity.getDate("fecha"));
        }
        if (entity.get("fechaReunionJurado") == null)
        {
            tanda.setFechaReunionJurado(null);
        }
        else
        {
            tanda.setFechaReunionJurado(entity.getDate("fechaReunionJurado"));
        }
        tanda.setActiva(Boolean.parseBoolean(entity.get("activa")));
        entity.setBaseClass("Tanda");
    }

    public ResponseMessage reiniciaFechaYEstadosBeca(Long id) throws NotificacionesException
    {
        String mensaje;
        Tanda tanda = Tanda.getTandaById(id);
        String usuario = AccessManager.getConnectedUser(request).getName();
        tanda.reiniciaFechaYEstadosBeca(usuario);

        Tanda tandaActualizada = Tanda.getTandaById(id);
        if (tandaActualizada.getFecha() == null)
        {
            mensaje = "Tanda reiniciada amb èxit";
        }
        else
        {
            mensaje = "Tanda NO reiniciada";
        }
        return new ResponseMessage(true, mensaje);
    }
}
