package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.ComunidadDAO;
import es.uji.apps.bec.model.Comunidad;
import es.uji.commons.rest.UIEntity;

@Service
public class ComunidadService
{
    private ComunidadDAO comunidadDAO;

    @Autowired
    public ComunidadService(ComunidadDAO comunidadDAO)
    {
        this.comunidadDAO = comunidadDAO;
    }

    public List<UIEntity> getComunidades()
    {
        List<Comunidad> comunidades = comunidadDAO.getComunidades();
        return UIEntity.toUI(comunidades);
    }
}
