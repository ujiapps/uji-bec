package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.ConvocatoriaDAO;
import es.uji.apps.bec.dao.dao.EstadoDAO;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.Organismo;
import es.uji.apps.bec.ui.Resumen;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class ConvocatoriaService extends CoreBaseService
{
    private EstadoDAO estadoDAO;

    private ConvocatoriaDAO convocatoriaDAO;

    @Autowired
    public ConvocatoriaService(ConvocatoriaDAO convocatoriaDAO, EstadoDAO estadoDAO)
    {
        this.convocatoriaDAO = convocatoriaDAO;
        this.estadoDAO = estadoDAO;
    }

    public List<UIEntity> getConvocatorias()
    {
        return UIEntity.toUI(convocatoriaDAO.get(Convocatoria.class));
    }

    public List<UIEntity> getConvocatoriasActivas()
    {
        return UIEntity.toUI(convocatoriaDAO.getConvocatoriasActivas());
    }

    public UIEntity insertaConvocatoria(UIEntity entity)
    {
        Convocatoria convocatoria = preparaConvocatoria(entity);
        if (convocatoria.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(convocatoriaDAO.insert(convocatoria));
    }

    public ResponseMessage borraConvocatoria(Long convocatoriaId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        Convocatoria convocatoria = convocatoriaDAO.getConvocatoriaById(convocatoriaId);
        if (convocatoria == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            convocatoriaDAO.deleteConvocatoria(convocatoria.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    public ResponseMessage actualizaConvocatoria(UIEntity entity)
    {
        Convocatoria convocatoria = preparaConvocatoria(entity);

        convocatoriaDAO.update(convocatoria);

        return new ResponseMessage(true);
    }

    private Convocatoria preparaConvocatoria(UIEntity entity)
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(ParamUtils.parseLong(entity.get("id")));
        convocatoria.setNombre(entity.get("nombre"));
        convocatoria.setAcronimo(entity.get("acronimo"));
        convocatoria.setActiva(Boolean.parseBoolean(entity.get("activa")));

        Long organismoId = ParamUtils.parseLong(entity.get("organismoId"));
        Organismo organismo = Organismo.getOrganismoById(organismoId);
        convocatoria.setOrganismo(organismo);

        return convocatoria;
    }

    public List<UIEntity> getConvocatoriasMinisterio()
    {
        return UIEntity.toUI(Convocatoria.getConvocatoriasActivasPorOrganismoId(1L));
    }

    public List<Resumen> getResumenEstadosPorConvocatoria(Long convocatoriaId)
    {
        return estadoDAO.getResumenEstadosPorConvocatoria(convocatoriaId);
    }

    public List<Resumen> getResumenProcesosPorConvocatoria(Long convocatoriaId)
    {
        return estadoDAO.getResumenProcesosPorConvocatoria(convocatoriaId);
    }
}
