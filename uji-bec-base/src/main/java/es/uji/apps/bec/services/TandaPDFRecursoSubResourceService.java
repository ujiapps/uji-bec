package es.uji.apps.bec.services;

import es.uji.apps.bec.exceptions.RecursoDeTandaNoEncontradoException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.PDFCommon;
import es.uji.apps.bec.model.Recurso;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

public class TandaPDFRecursoSubResourceService extends CoreBaseService
{
    @PathParam("tandaId")
    String tandaId;

    @GET
    @Produces("application/pdf")
    public Template generateCertificate()
            throws UnauthorizedUserException, RecursoDeTandaNoEncontradoException, RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Map<String, Object>> listaRecursos = new ArrayList<Map<String, Object>>();
        List<Beca> listaBecas = Beca.getBecasByTandaId(ParamUtils.parseLong(tandaId));

        Calendar calendario = Calendar.getInstance(new Locale("CA"));

        if (listaBecas == null) {
            throw new RegistroNoEncontradoException();
        }

        for (Beca beca: listaBecas) {
            Recurso recurso = Recurso.getRecursoByBecaId(beca.getId());

            if (recurso == null) {
                throw new RecursoDeTandaNoEncontradoException();
            }

            Map<String, Object> infoRecurso = PDFCommon.getInfoRecurso(recurso, beca);
            listaRecursos.add(infoRecurso);
        }

        List<List<String>> parejasAlumnos = new ArrayList<List<String>>();

        List<String> pareja = new ArrayList<String>();
        for (Map<String, Object> recurso: listaRecursos) {
            if (pareja.size() == 2) {
                parejasAlumnos.add(pareja);
                pareja = new ArrayList<String>();
            }
            pareja.add(recurso.get("estudiante").toString());
        }

        if (pareja.size() == 1) {
            pareja.add("");
        }
        parejasAlumnos.add(pareja);

        CursoAcademico academico = listaBecas.get(0).getSolicitante().getCursoAcademico();
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", academico.getId(), (academico.getId() + 1));
        Template template = new PDFTemplate("bec/recurso", new Locale("ES"), "bec");
        template.put("listaRecursos", listaRecursos);
        template.put("tanda", true);
        template.put("parejasAlumnos", parejasAlumnos);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

}
