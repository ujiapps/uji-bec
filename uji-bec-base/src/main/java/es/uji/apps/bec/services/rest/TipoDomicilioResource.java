package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoDomicilioService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipodomicilio")
public class TipoDomicilioResource
{
    @InjectParam
    private TipoDomicilioService tipoDomicilioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposDomicilios()
    {
        return tipoDomicilioService.getTiposDomicilios();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoDomicilio(UIEntity entity) throws GeneralBECException,
            ParseException
    {
        return tipoDomicilioService.insertaTipoDomicilio(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoDomicilio(@PathParam("id") Long tipoDomicilioId,
            UIEntity entity)
    {
        return tipoDomicilioService.actualizaTipoDomicilio(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoDomicilio(@PathParam("id") Long tipoDomicilioId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoDomicilioService.borraTipoDomicilio(tipoDomicilioId);
    }
}
