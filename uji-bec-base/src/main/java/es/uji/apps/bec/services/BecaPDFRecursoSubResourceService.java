package es.uji.apps.bec.services;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.PDFCommon;
import es.uji.apps.bec.model.Recurso;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

public class BecaPDFRecursoSubResourceService extends CoreBaseService
{
    @PathParam("becaId")
    String becaId;

    @GET
    @Produces("application/pdf")
    public Template generatePDFRecursoMinisterio()
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Calendar calendario = Calendar.getInstance(new Locale("CA"));

        Recurso recurso = Recurso.getRecursoByBecaId(ParamUtils.parseLong(becaId));
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (beca == null)
        {
            throw new RegistroNoEncontradoException();
        }

        Map<String, Object> infoRecurso = PDFCommon.getInfoRecurso(recurso, beca);

        List<Map<String, Object>> listaRecursos = new ArrayList<Map<String, Object>>();
        listaRecursos.add(infoRecurso);

        Template template = new PDFTemplate("bec/recurso", new Locale("ES"), "bec");
        template.put("listaRecursos", listaRecursos);
        template.put("calendario", calendario);
        template.put("cursoAcademico", beca.getSolicitante().getCursoAcademico().getId());

        return template;
    }
}
