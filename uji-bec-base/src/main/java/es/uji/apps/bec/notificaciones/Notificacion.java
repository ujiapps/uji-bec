package es.uji.apps.bec.notificaciones;


public interface Notificacion
{
    void prepara();
    void envia();
}
