package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.MiembroDuplicadoException;
import es.uji.apps.bec.services.MiembroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("miembro")
public class MiembroResource extends CoreBaseService
{
    @InjectParam
    private MiembroService miembroService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMiembrosByBeca(@QueryParam("becaId") Long becaId)
    {
        return miembroService.getMiembrosByBeca(becaId);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getMiembro(@PathParam("id") Long miembroId)
    {
        return miembroService.getMiembro(miembroId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage updateMiembro(@PathParam("id") Long miembroId, MultivaluedMap<String, String> params)
            throws MiembroDuplicadoException, ParseException
    {
        return miembroService.updateMiembro(miembroId, params);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertMiembro(MultivaluedMap<String, String> params)
            throws MiembroDuplicadoException, ParseException
    {
        return miembroService.insertMiembro(params);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long miembroId)
            throws RegistroNoEncontradoException, NumberFormatException, RegistroConHijosException
    {
        return miembroService.delete(miembroId);
    }

    /*
     * Subrecursos externos
     */

    @Path("{id}/actividadeconomica")
    public MiembroActividadEconomicaResource getActividadEconomica(
            @InjectParam MiembroActividadEconomicaResource miembroActividadEconomicaResource)
    {
        return miembroActividadEconomicaResource;
    }
}
