package es.uji.apps.bec.services.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.exceptions.BecaException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.exceptions.RamaNoExisteException;
import es.uji.apps.bec.services.AcademicosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;



@Path("academicos")
public class AcademicosResource extends CoreBaseService
{
    @InjectParam
    private AcademicosService academicosService;

    @PUT
    @Path("actual/{becaId}")
    public ResponseMessage actualizaAcademicosCursoActual(@PathParam("becaId") Long becaId) throws GeneralBECException
    {
        return academicosService.actualizaAcademicosCursoActual(becaId);
    }

    @PUT
    @Path("anterior/{becaId}")
    public ResponseMessage actualizaAcademicosCursoAnterior(@PathParam("becaId") Long becaId) throws
            BecaDuplicadaException, BecaException, RamaNoExisteException, BecaEnEstadoNoModificableException,
            BecaSinEstudioException
    {
        return academicosService.actualizaAcademicosCursoAnterior(becaId);
    }
}
