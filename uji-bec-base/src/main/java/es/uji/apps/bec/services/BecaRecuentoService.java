package es.uji.apps.bec.services;

import com.mysema.query.Tuple;
import es.uji.apps.bec.dao.dao.BecaRecuentoDAO;
import es.uji.apps.bec.model.BecaRecuento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.ws.rs.QueryParam;

@Service
public class BecaRecuentoService
{
    private BecaRecuentoDAO becaRecuentoDAO;

    @Autowired
    public BecaRecuentoService(BecaRecuentoDAO becaRecuentoDAO)
    {
        this.becaRecuentoDAO = becaRecuentoDAO;
    }

    public List<BecaRecuento> getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(
            @QueryParam("cursoAcademicoId") String cursoAcademicoId,
            @QueryParam("organismodId") String organismoId,
            @QueryParam("convocatoriaId") String convocatoriaId)
    {
        return becaRecuentoDAO.getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(
                Long.parseLong(cursoAcademicoId), Long.parseLong(organismoId),
                Long.parseLong(convocatoriaId));
    }

    public List<Tuple> getDistinctOrganismoConvocatoriaByCursoAcademico(Long cursoAcademicoId)
    {
        return becaRecuentoDAO.getDistinctOrganismoConvocatoriaByCursoAcademico(cursoAcademicoId);
    }
}
