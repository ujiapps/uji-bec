package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.MiembroDAO;
import es.uji.apps.bec.dao.dao.TipoMiembroDAO;
import es.uji.apps.bec.exceptions.MiembroDuplicadoException;
import es.uji.apps.bec.model.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

@Service
public class MiembroService extends CoreBaseService
{
    private MiembroDAO miembroDAO;
    private TipoMiembroDAO tipoMiembroDAO;

    @Autowired
    public MiembroService(MiembroDAO miembroDAO, TipoMiembroDAO tipoMiembroDAO)
    {
        this.miembroDAO = miembroDAO;
        this.tipoMiembroDAO = tipoMiembroDAO;
    }

    public List<UIEntity> getMiembrosByBeca(Long becaId)
    {
        List<Miembro> miembrosByBeca = miembroDAO.getMiembrosByBeca(becaId);
        return modelToUI(miembrosByBeca);
    }

    public UIEntity getMiembro(Long miembroId)
    {
        Miembro miembro = miembroDAO.get(Miembro.class, miembroId).get(0);
        UIEntity entity = UIEntity.toUI(miembro);

        Date fechaNacimiento = miembro.getFechaNacimiento();
        if (fechaNacimiento != null)
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            entity.put("fechaNacimiento", dateFormat.format(fechaNacimiento));
        }

        Date fechaCadNif = miembro.getFechaCadNif();
        if (fechaCadNif != null)
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            entity.put("fechaCadNif", dateFormat.format(fechaCadNif));
        }

        Date fechaFinMinus = miembro.getFechaFinMinus();
        if (fechaFinMinus != null)
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            entity.put("fechaFinMinus", dateFormat.format(fechaFinMinus));
        }

        Date fechaResMinus = miembro.getFechaResMinus();
        if (fechaResMinus != null)
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            entity.put("fechaResMinus", dateFormat.format(fechaResMinus));
        }

        if (miembro.isNoValidaIdentificacion())
        {
            entity.put("noValidaIdentificacion", true);
        }
        else
        {
            entity.put("noValidaIdentificacion", false);
        }

        if (miembro.isEstudiaFuera())
        {
            entity.put("estudiaFuera", true);
        }
        else
        {
            entity.put("estudiaFuera", false);
        }

        if (miembro.isFirmaSolicitud())
        {
            entity.put("firmaSolicitud", true);
        }
        else
        {
            entity.put("firmaSolicitud", false);
        }

        if (miembro.isResidencia())
        {
            entity.put("residencia", true);
        }
        else
        {
            entity.put("residencia", false);
        }

        if (miembro.isCustodia())
        {
            entity.put("custodia", true);
        }
        else
        {
            entity.put("custodia", false);
        }

        if (miembro.isCustodiaCompartida())
        {
            entity.put("custodiaCompartida", true);
        }
        else
        {
            entity.put("custodiaCompartida", false);
        }

        return entity;
    }

    public ResponseMessage updateMiembro(Long miembroId, MultivaluedMap<String, String> params)
            throws MiembroDuplicadoException, ParseException
    {
        Miembro miembro = miembroDAO.get(Miembro.class, miembroId).get(0);

        preparaMiembro(params, miembro);

        try
        {
            miembro.update();
            return new ResponseMessage(true);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new MiembroDuplicadoException();
        }
    }

    public UIEntity insertMiembro(MultivaluedMap<String, String> params)
            throws MiembroDuplicadoException, ParseException
    {
        Miembro miembro = new Miembro();

        preparaMiembro(params, miembro);

        try
        {
            Miembro nuevoMiembro = miembro.insert();
            return UIEntity.toUI(nuevoMiembro);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new MiembroDuplicadoException();
        }
    }

    public ResponseMessage delete(Long miembroId) throws RegistroNoEncontradoException, NumberFormatException,
            RegistroConHijosException
    {
        Miembro miembro = miembroDAO.getMiembroById(miembroId);

        if (miembro == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            miembroDAO.deleteMiembro(miembro.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private List<UIEntity> modelToUI(List<Miembro> listaMiembros)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Miembro miembro : listaMiembros)
        {
            result.add(modelToUI(miembro));
        }
        return result;
    }

    private UIEntity modelToUI(Miembro miembro)
    {
        UIEntity entity = UIEntity.toUI(miembro);
        TipoMiembro tipoMiembro = miembro.getTipoMiembro();
        if (tipoMiembro != null)
        {
            entity.put("tipoMiembro", tipoMiembro.getNombre());
            entity.put("tipoMiembroOrden", tipoMiembro.getOrden());
        }
        entity.put("nombreMiembro",
                noMuestraNull(miembro.getNombre()) + " " + noMuestraNull(miembro.getApellido1())
                        + " " + noMuestraNull(miembro.getApellido2()));

        TipoSustentador tipoSustentador = miembro.getTipoSustentador();
        if (tipoSustentador != null)
        {
            entity.put("tipoSustentador", tipoSustentador.getNombre());
        }

        return entity;
    }

    private String noMuestraNull(String cadena)
    {
        if (cadena == null)
        {
            return "";
        }
        return cadena;
    }

    private void preparaMiembro(MultivaluedMap<String, String> params, Miembro miembro)
            throws ParseException
    {
        miembro.setBeca(Beca.getBecaById(Long.parseLong(params.getFirst("becaId"))));

        if (!params.getFirst("tipoMiembroId").isEmpty())
        {
            miembro.setTipoMiembro(tipoMiembroDAO.getTipoMiembroById(Long.parseLong(params.getFirst("tipoMiembroId"))));
        }
        else
        {
            miembro.setTipoMiembro(null);
        }

        if (!params.getFirst("tipoMinusvaliaId").isEmpty())
        {
            miembro.setTipoMinusvalia(TipoMinusvalia.getTipoMinusvaliaById(Long.parseLong(params.getFirst(
                    "tipoMinusvaliaId"))));
        }
        else
        {
            miembro.setTipoMinusvalia(null);
        }

        if (!params.getFirst("tipoSustentadorId").isEmpty())
        {
            miembro.setTipoSustentador(TipoSustentador.getTipoSustentadorById(Long.parseLong(params
                    .getFirst("tipoSustentadorId"))));
        }
        else
        {
            miembro.setTipoSustentador(null);
        }
        miembro.setApellido1(params.getFirst("apellido1"));
        miembro.setApellido2(params.getFirst("apellido2"));
        miembro.setNombre(params.getFirst("nombre"));
        miembro.setNifArren(params.getFirst("nifArren"));
        miembro.setPorcentajeActividadesEconomicas(ParamUtils.parseFloat(params.getFirst("porcentajeActividadesEconomicas")));
        miembro.setImporteActividadesEconomicas(ParamUtils.parseFloat(params.getFirst("importeActividadesEconomicas")));
        miembro.setImporteRentasExtranjero(ParamUtils.parseFloat(params.getFirst("importeRentasExtranjero")));
        miembro.setPrecioAlquiler(ParamUtils.parseFloat(params.getFirst("precioAlquiler")));

        if (!params.getFirst("tipoMonedaId").isEmpty())
        {
            miembro.setTipoMoneda(TipoMoneda.getTipoMonedaById(Long.parseLong(params.getFirst("tipoMonedaId"))));
        } else
        {
            miembro.setTipoMoneda(null);
        }

        if (!params.getFirst("tipoIdentificacionId").isEmpty())
        {
            miembro.setTipoIdentificacion(TipoIdentificacion.getTipoIdentificacionById(Long
                    .parseLong(params.getFirst("tipoIdentificacionId"))));
        }
        else
        {
            miembro.setTipoIdentificacion(null);
        }
        miembro.setIdentificacion(params.getFirst("identificacion"));
        miembro.setIdentificacionIdesp(params.getFirst("identificacionIdesp"));

        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

        String fechaNacimientoParam = params.getFirst("fechaNacimiento");
        if (fechaNacimientoParam != null && fechaNacimientoParam.length() > 0)
        {
            Date fechaNacimiento = formatoFecha.parse(fechaNacimientoParam);
            miembro.setFechaNacimiento(fechaNacimiento);
        }
        else
        {
            miembro.setFechaNacimiento(null);
        }

        String fechaCadNifParam = params.getFirst("fechaCadNif");
        if (fechaCadNifParam != null && fechaCadNifParam.length() > 0)
        {
            Date fechaCadNif = formatoFecha.parse(fechaCadNifParam);
            miembro.setFechaCadNif(fechaCadNif);
        }
        else
        {
            miembro.setFechaCadNif(null);
        }

        String fechaResMinusParam = params.getFirst("fechaResMinus");
        if (fechaResMinusParam != null && fechaResMinusParam.length() > 0)
        {
            Date fechaResMinus = formatoFecha.parse(fechaResMinusParam);
            miembro.setFechaResMinus(fechaResMinus);
        }
        else
        {
            miembro.setFechaResMinus(null);
        }

        String fechaFinMinusParam = params.getFirst("fechaFinMinus");
        if (fechaFinMinusParam != null && fechaFinMinusParam.length() > 0)
        {
            Date fechaFinMinus = formatoFecha.parse(fechaFinMinusParam);
            miembro.setFechaFinMinus(fechaFinMinus);
        }
        else
        {
            miembro.setFechaFinMinus(null);
        }

        miembro.setNoValidaIdentificacion(extraeParametrosCheck(params, "noValidadIdentificacion"));
        miembro.setCodigoEstudioHermano(params.getFirst("codigoEstudioHermano"));
        miembro.setCodigoUnivHermano(params.getFirst("codigoUnivHermano"));
        miembro.setEstudiaFuera(extraeParametrosCheck(params, "estudiaFuera"));
        miembro.setFirmaSolicitud(extraeParametrosCheck(params, "firmaSolicitud"));

        if (!params.getFirst("tipoSexoId").isEmpty())
        {
            miembro.setTipoSexo(TipoSexo.getTipoSexoById(Long.parseLong(params.getFirst("tipoSexoId"))));
        }
        else
        {
            miembro.setTipoSexo(null);
        }

        if (!params.getFirst("ccaaMinusId").isEmpty())
        {
            miembro.setCcaaMinus(Comunidad.getComunidadById(Long.parseLong(params.getFirst("ccaaMinusId"))));
        }
        else
        {
            miembro.setCcaaMinus(null);
        }

        if (!params.getFirst("paisId").isEmpty())
        {
            Pais paisById = Pais.getPaisById(params.getFirst("paisId"));
            miembro.setPais(paisById);
        }
        else
        {
            miembro.setPais(null);
        }

        if (!params.getFirst("estadoCivilId").isEmpty())
        {
            miembro.setEstadoCivil(EstadoCivil.getEstadoCivilById(Long.parseLong(params
                    .getFirst("estadoCivilId"))));
        }
        else
        {
            miembro.setEstadoCivil(null);
        }

        if (!params.getFirst("padronProvinciaId").isEmpty())
        {
            miembro.setPadronProvinciaId(Provincia.getProvinciaById(
                    Long.parseLong(params.getFirst("padronProvinciaId"))).getId());
        }
        else
        {
            miembro.setPadronProvinciaId(null);
        }

        if (!params.getFirst("padronLocalidadId").isEmpty())
        {
            miembro.setPadronLocalidadId(Localidad.getLocalidadById(
                    Long.parseLong(params.getFirst("padronLocalidadId"))).getId());
        }
        else
        {
            miembro.setPadronLocalidadId(null);
        }

        if (!params.getFirst("situacionLaboralId").isEmpty())
        {
            miembro.setSituacionLaboral(SituacionLaboral.getSituacionLaboralById(Long
                    .parseLong(params.getFirst("situacionLaboralId"))));
        }
        else
        {
            miembro.setSituacionLaboral(null);
        }

        if (!params.getFirst("profesionId").isEmpty())
        {
            miembro.setProfesion(Profesion.getProfesionById(Long.parseLong(params
                    .getFirst("profesionId"))));
        }
        else
        {
            miembro.setProfesion(null);
        }

        String noValidaIdentificacion = params.getFirst("noValidaIdentificacion");
        if (noValidaIdentificacion != null && noValidaIdentificacion.equals("on"))
        {
            miembro.setNoValidaIdentificacion(true);
        }
        else
        {
            miembro.setNoValidaIdentificacion(false);
        }

        String estudiaFuera = params.getFirst("estudiaFuera");
        if (estudiaFuera != null && estudiaFuera.equals("on"))
        {
            miembro.setEstudiaFuera(true);
        }
        else
        {
            miembro.setEstudiaFuera(false);
        }

        String firmaSolicitud = params.getFirst("firmaSolicitud");
        if (firmaSolicitud != null && firmaSolicitud.equals("on"))
        {
            miembro.setFirmaSolicitud(true);
        }
        else
        {
            miembro.setFirmaSolicitud(false);
        }

        String residencia = params.getFirst("residencia");
        if (residencia != null && residencia.equals("on"))
        {
            miembro.setResidencia(true);
        }
        else
        {
            miembro.setResidencia(false);
        }

        String custodia = params.getFirst("custodia");
        if (custodia != null && custodia.equals("on"))
        {
            miembro.setCustodia(true);
        }
        else
        {
            miembro.setCustodia(false);
        }

        String custodiaCompartida = params.getFirst("custodiaCompartida");
        if (custodiaCompartida != null && custodiaCompartida.equals("on"))
        {
            miembro.setCustodiaCompartida(true);
        }
        else
        {
            miembro.setCustodiaCompartida(false);
        }

        miembro.setLocalidadTrabajo(params.getFirst("localidadTrabajo"));

        String indDeclRenta = params.getFirst("indDeclRenta");
        if (indDeclRenta != null && indDeclRenta.equals("on"))
        {
            miembro.setIndDeclRenta(true);
        }
        else
        {
            miembro.setIndDeclRenta(false);
        }

        String indRentasEspanna = params.getFirst("indRentasEspanna");
        if (indRentasEspanna != null && indRentasEspanna.equals("on"))
        {
            miembro.setIndRentasEspanna(true);
        }
        else
        {
            miembro.setIndRentasEspanna(false);
        }

        String indRentasNavarra = params.getFirst("indRentasNavarra");
        if (indRentasNavarra != null && indRentasNavarra.equals("on"))
        {
            miembro.setIndRentasNavarra(true);
        }
        else
        {
            miembro.setIndRentasNavarra(false);
        }

        String indRentasPaisVasco = params.getFirst("indRentasPaisVasco");
        if (indRentasPaisVasco != null && indRentasPaisVasco.equals("on"))
        {
            miembro.setIndRentasPaisVasco(true);
        }
        else
        {
            miembro.setIndRentasPaisVasco(false);
        }

        String indConvivePareja = params.getFirst("indConvivePareja");
        miembro.setIndConvivePareja(indConvivePareja != null && indConvivePareja.equals("on"));

        String indIngresosPropios = params.getFirst("indIngresosPropios");
        miembro.setIndIngresosPropios(indIngresosPropios != null && indIngresosPropios.equals("on"));

        String indRevMinusMec = params.getFirst("indRevMinusMec");
        miembro.setIndRevMinusMec(indRevMinusMec != null && indRevMinusMec.equals("on"));

        String indRevMinusUt = params.getFirst("indRevMinusUt");
        miembro.setIndRevMinusUt(indRevMinusUt != null && indRevMinusUt.equals("on"));

        String indArrenUnidFamiliar = params.getFirst("indArrenUnidFamiliar");
        miembro.setIndArrenUnidFamiliar(indArrenUnidFamiliar != null && indArrenUnidFamiliar.equals("on"));
    }

    private Boolean extraeParametrosCheck(MultivaluedMap<String, String> params, String parametro)
    {
        String valor = params.getFirst(parametro);
        return valor != null && valor.equals("on");
    }
}
