package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoMinusvaliaDAO;
import es.uji.apps.bec.model.TipoMinusvalia;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoMinusvaliaService
{
    private TipoMinusvaliaDAO tipoMinusvaliaDAO;

    @Autowired
    public TipoMinusvaliaService(TipoMinusvaliaDAO tipoMinusvaliaDAO)
    {
        this.tipoMinusvaliaDAO = tipoMinusvaliaDAO;
    }

    public List<UIEntity> getTiposMinusvalias()
    {
        List<TipoMinusvalia> tiposMinusvalias = tipoMinusvaliaDAO.getTiposMinusvalias();
        return UIEntity.toUI(tiposMinusvalias);
    }

    public UIEntity insertaTipoMinusvalia(UIEntity entity)
    {
        TipoMinusvalia tipoMinusvalia = preparaTipoMinusvalia(entity);
        if (tipoMinusvalia.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoMinusvaliaDAO.insert(tipoMinusvalia));
    }

    public ResponseMessage actualizaTipoMinusvalia(UIEntity entity)
    {
        TipoMinusvalia tipoMinusvalia = preparaTipoMinusvalia(entity);
        tipoMinusvaliaDAO.update(tipoMinusvalia);
        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoMinusvalia(Long tipoMinusvaliaId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoMinusvalia tiposMinusvalia =  tipoMinusvaliaDAO.getTipoMinusvaliaById(tipoMinusvaliaId);

        if (tiposMinusvalia == null )
        {
            throw new RegistroNoEncontradoException();
        }
        try
        {
            tipoMinusvaliaDAO.deleteTipoMinusvalia( tiposMinusvalia.getId());

        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoMinusvalia preparaTipoMinusvalia(UIEntity entity)
    {
        TipoMinusvalia tipoMinusvalia = new TipoMinusvalia();
        tipoMinusvalia.setId(ParamUtils.parseLong(entity.get("id")));
        tipoMinusvalia.setNombre(entity.get("nombre"));
        tipoMinusvalia.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return tipoMinusvalia;
    }    
}
