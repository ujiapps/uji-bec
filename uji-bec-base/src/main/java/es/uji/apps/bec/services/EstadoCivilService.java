package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.EstadoCivilDAO;
import es.uji.apps.bec.model.EstadoCivil;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class EstadoCivilService
{
    private EstadoCivilDAO estadoCivilDAO;

    @Autowired
    public EstadoCivilService(EstadoCivilDAO estadoCivilDAO)
    {
        this.estadoCivilDAO = estadoCivilDAO;
    }

    public List<UIEntity> getEstadosCiviles()
    {
        List<EstadoCivil> estadosCiviles = estadoCivilDAO.getEstadosCiviles();
        return UIEntity.toUI(estadosCiviles);
    }

    public UIEntity insertaEstadoCivil(UIEntity entity)
    {
        EstadoCivil estadoCivil = preparaEstadoCivil(entity);
        if (estadoCivil.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(estadoCivilDAO.insert(estadoCivil));
    }

    public ResponseMessage actualizaEstadoCivil(UIEntity entity)
    {
        EstadoCivil estadoCivil = preparaEstadoCivil(entity);

        estadoCivilDAO.update(estadoCivil);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraEstadoCivil(Long estadoCivilId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        EstadoCivil estadoCivil = estadoCivilDAO.getEstadoCivilById(estadoCivilId);

        if (estadoCivil == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            estadoCivilDAO.deleteEstadoCivil(estadoCivil.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private EstadoCivil preparaEstadoCivil(UIEntity entity)
    {
        EstadoCivil estadoCivil = new EstadoCivil();
        estadoCivil.setId(ParamUtils.parseLong(entity.get("id")));
        estadoCivil.setNombre(entity.get("nombre"));
        estadoCivil.setOrden(ParamUtils.parseLong(entity.get("orden")));
        return estadoCivil;
    }
}
