package es.uji.apps.bec.services.rest;

import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("test")
public class TestResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTest() {
        UIEntity entity = new UIEntity();
        entity.put("fecha", "29112024 15:55");
        return entity;
    }
}
