package es.uji.apps.bec.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.DomicilioDAO;
import es.uji.apps.bec.exceptions.DomicilioDuplicadoException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.Arrendatario;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Domicilio;
import es.uji.apps.bec.model.Localidad;
import es.uji.apps.bec.model.Pais;
import es.uji.apps.bec.model.Provincia;
import es.uji.apps.bec.model.TipoDomicilio;
import es.uji.apps.bec.model.TipoIdentificacion;
import es.uji.apps.bec.model.TipoResidencia;
import es.uji.apps.bec.model.TipoVia;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Service
public class DomicilioService
{
    public static Logger log = Logger.getLogger(SolicitanteService.class);
    private DomicilioDAO domicilioDAO;
    private SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");

    @Autowired
    public DomicilioService(DomicilioDAO domicilioDAO)
    {
        this.domicilioDAO = domicilioDAO;
    }

    public List<UIEntity> getDomiciliosByBecaId(Long becaId)
    {
        return modelToUI(domicilioDAO.getDomiciliosByBecaId(becaId));
    }

    private List<UIEntity> modelToUI(List<Domicilio> listaDomicilios)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        for (Domicilio domicilio : listaDomicilios)
        {
            if ((domicilio.getLocalidadNombre() == "" || domicilio.getLocalidadNombre() == null)
                    && domicilio.getLocalidad() != null)
            {
                domicilio.setLocalidadNombre(Localidad.getLocalidadById(
                        domicilio.getLocalidad().getId()).getNombre());
            }
            result.add(modelToUI(domicilio));
        }

        return result;
    }

    private UIEntity modelToUI(Domicilio domicilio)
    {
        UIEntity entity = UIEntity.toUI(domicilio);

        TipoDomicilio tipoDomicilio = domicilio.getTipoDomicilio();

        if (tipoDomicilio != null)
        {
            entity.put("tipoDomicilio", tipoDomicilio.getNombre());
            entity.put("tipoDomicilioOrden", tipoDomicilio.getOrden());
        }

        TipoVia tipoVia = domicilio.getTipoVia();

        if (tipoVia != null)
        {
            entity.put(
                    "adreca",
                    noMuestraNull(domicilio.getTipoVia().getNombre()) + " "
                            + noMuestraNull(domicilio.getNombreVia()) + " "
                            + noMuestraNull(domicilio.getNumero()) + " "
                            + noMuestraNull(domicilio.getEscalera()) + " "
                            + noMuestraNull(domicilio.getPiso()) + " "
                            + noMuestraNull(domicilio.getPuerta()));
        } else
        {
            entity.put("adreca", domicilio.getNombreVia() + " " + domicilio.getNumero());
        }

        return entity;
    }

    private String noMuestraNull(String cadena)
    {
        if (cadena == null)
        {
            return "";
        } else
        {
            return cadena;
        }
    }

    public List<UIEntity> getDomicilio(Long domicilioId)
    {
        Domicilio domicilio = domicilioDAO.getDomicilioById(domicilioId);

        UIEntity entity = UIEntity.toUI(domicilio);

        preparaFechaInicioContrato(domicilio, entity);
        preparaFechaFinContrato(domicilio, entity);
        preparaFechaAdquisicionVivienda(domicilio, entity);
        preparaResidenciaEsp(domicilio, entity);
        preparaGratuito(domicilio, entity);
        preparaCorrespondencia(domicilio, entity);

        return Collections.singletonList(entity);
    }

    private void preparaCorrespondencia(Domicilio domicilio, UIEntity entity)
    {
        if (domicilio.isCorrespondencia())
        {
            entity.put("correspondencia", true);
        } else
        {
            entity.put("correspondencia", false);
        }
    }

    private void preparaGratuito(Domicilio domicilio, UIEntity entity)
    {
        if (domicilio.isGratuito())
        {
            entity.put("gratuito", true);
        } else
        {
            entity.put("gratuito", false);
        }
    }

    private void preparaResidenciaEsp(Domicilio domicilio, UIEntity entity)
    {
        if (domicilio.isResidenciaEsp())
        {
            entity.put("residenciaEsp", true);
        } else
        {
            entity.put("residenciaEsp", false);
        }
    }

    private void preparaFechaFinContrato(Domicilio domicilio, UIEntity entity)
    {
        Date fechaFinContrato = domicilio.getFechaFinContrato();
        if (fechaFinContrato != null)
        {
            entity.put("fechaFinContrato", formateadorFecha.format(fechaFinContrato));
        }
    }

    private void preparaFechaInicioContrato(Domicilio domicilio, UIEntity entity)
    {
        Date fechaInicioContrato = domicilio.getFechaInicioContrato();
        if (fechaInicioContrato != null)
        {
            entity.put("fechaInicioContrato", formateadorFecha.format(fechaInicioContrato));
        }
    }

    private void preparaFechaAdquisicionVivienda(Domicilio domicilio, UIEntity entity)
    {
        Date fechaAdquisicionVivienda = domicilio.getFechaAdquisicionVivienda();
        if (fechaAdquisicionVivienda != null)
        {
            entity.put("fechaAdquisicionVivienda", formateadorFecha.format(fechaAdquisicionVivienda));
        }
    }

    public List<UIEntity> getArrendatariosByDomicilio(Long domicilioId)
    {
        List<Arrendatario> listaArrendatarios = Arrendatario.getArrendatariosByDomicilio(domicilioId);
        return UIEntity.toUI(listaArrendatarios);
    }

    public UIEntity updateArrendatario(Long domicilioId, Long arrendatarioId, UIEntity entity)
    {
        String nombre = entity.get("nombre");
        String identificacion = entity.get("identificacion");
        Long tipoIdentificacionId = ParamUtils.parseLong(entity.get("tipoIdentificacionId"));

        TipoIdentificacion tipoIdentificacion = preparaTipoIdentificacion(tipoIdentificacionId);

        Arrendatario arrendatario = Arrendatario.getArrendatarioById(arrendatarioId);
        arrendatario.setIdentificacion(identificacion);
        arrendatario.setNombre(nombre);
        arrendatario.setTipoIdentificacion(tipoIdentificacion);

        arrendatario.updateArrendatario();

        return entity;
    }

    private TipoIdentificacion preparaTipoIdentificacion(Long tipoIdentificacionId)
    {
        TipoIdentificacion tipoIdentificacion = null;
        if (tipoIdentificacionId != null)
        {
            tipoIdentificacion = new TipoIdentificacion();
            tipoIdentificacion.setId(tipoIdentificacionId);
        }
        return tipoIdentificacion;
    }

    public UIEntity insertaArrendatario(Long domicilioId, UIEntity entity)
    {
        if (todosLosCamposArrendatarioVacios(entity))
        {
            return entity;
        }

        String nombre = entity.get("nombre");
        String identificacion = entity.get("identificacion");
        Long tipoIdentificacionId = ParamUtils.parseLong(entity.get("tipoIdentificacionId"));

        TipoIdentificacion tipoIdentificacion = preparaTipoIdentificacion(tipoIdentificacionId);
        Domicilio domicilio = preparaDomicilioArrendatario(domicilioId);

        Arrendatario arrendatario = new Arrendatario();
        arrendatario.setIdentificacion(identificacion);
        arrendatario.setNombre(nombre);
        arrendatario.setTipoIdentificacion(tipoIdentificacion);
        arrendatario.setDomicilio(domicilio);

        arrendatario.insert();

        entity.put("id", arrendatario.getId());
        entity.put("domicilioId", domicilioId);
        return entity;
    }

    private boolean todosLosCamposArrendatarioVacios(UIEntity entity)
    {
        return entity.get("tipoIdentificacionId") == null && entity.get("identificacion") == null
                && entity.get("nombre") == null;
    }

    private Domicilio preparaDomicilioArrendatario(Long domicilioId)
    {
        Domicilio domicilio = new Domicilio();
        domicilio.setId(domicilioId);
        return domicilio;
    }

    public ResponseMessage deleteArrendatario(Long domicilioId, Long arrendatarioId) throws GeneralBECException
    {
        try
        {
            Arrendatario.delete(arrendatarioId);
        }
        catch (Exception e)
        {
            throw new GeneralBECException("No s'ha pogut esborrar el registre");
        }
        return new ResponseMessage(true);
    }

    public ResponseMessage updateDomicilio(Long domicilioId, MultivaluedMap<String, String> params)
            throws DomicilioDuplicadoException, ParseException
    {
        Domicilio domicilio = domicilioDAO.getDomicilioById(domicilioId);

        preparaDomicilio(params, domicilio);

        try
        {
            domicilio.updateDomicilio();
            return new ResponseMessage(true);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new DomicilioDuplicadoException();
        }
    }

    private void preparaDomicilio(MultivaluedMap<String, String> params, Domicilio domicilio)
            throws ParseException
    {
        Long tipoDomicilioId = ParamUtils.parseLong(params.getFirst("tipoDomicilioId"));
        TipoDomicilio tipoDomicilio = preparaTipoDomicilio(tipoDomicilioId);
        domicilio.setTipoDomicilio(tipoDomicilio);

        Long tipoViaId = ParamUtils.parseLong(params.getFirst("tipoViaId"));
        TipoVia tipoVia = preparaTipoVia(tipoViaId);
        domicilio.setTipoVia(tipoVia);

        Long provinciaId = ParamUtils.parseLong(params.getFirst("provinciaId"));
        Provincia provincia = preparaProvincia(provinciaId);
        domicilio.setProvincia(provincia);

        String paisId = params.getFirst("paisId");
        Pais pais = preparaPais(paisId);
        domicilio.setPais(pais);
        
        Long tipoResidenciaId = ParamUtils.parseLong(params.getFirst("tipoResidenciaId"));
        TipoResidencia tipoResidencia = preparaTipoResidencia(tipoResidenciaId);
        domicilio.setTipoResidencia(tipoResidencia);

        Long arrendadorTipoIdentificacionId = ParamUtils.parseLong(params.getFirst("arrendadorTipoIdentificacionId"));
        TipoIdentificacion arrendadorTipoIdentificacion = preparaTipoIdentificacion(arrendadorTipoIdentificacionId);
        domicilio.setArrendadorTipoIdentificacion(arrendadorTipoIdentificacion);

        // TODO: Revisar esta parte intentando no acceder a la BD.
        if (!params.getFirst("localidadId").isEmpty())
        {
            Localidad localidad = Localidad.getLocalidadById(Long.parseLong(params.getFirst("localidadId")));
            domicilio.setLocalidad(localidad);
            domicilio.setLocalidadNombre(localidad.getNombre());
        } else
        {
            domicilio.setLocalidad(null);
            domicilio.setLocalidadNombre("");
        }

        domicilio.setOtraClaseDomicilio(params.getFirst("otraClaseDomicilio"));
        domicilio.setNombreVia(params.getFirst("nombreVia"));
        domicilio.setNumero(params.getFirst("numero"));
        domicilio.setEscalera(params.getFirst("escalera"));
        domicilio.setPiso(params.getFirst("piso"));
        domicilio.setPuerta(params.getFirst("puerta"));
        domicilio.setCodigoPostal(params.getFirst("codigoPostal"));
        domicilio.setArrendadorIdentificacion(params.getFirst("arrendadorIdentificacion"));
        domicilio.setArrendadorNombre(params.getFirst("arrendadorNombre"));
        domicilio.setImporteAlquiler(ParamUtils.parseFloat(params.getFirst("importeAlquiler")));
        domicilio.setResidenciaEsp(extraeParametrosCheck(params, "residenciaEsp"));
        domicilio.setGratuito(extraeParametrosCheck(params, "gratuito"));
        domicilio.setCorrespondencia(extraeParametrosCheck(params, "correspondencia"));
        domicilio.setBeca(Beca.getBecaById(Long.parseLong(params.getFirst("becaId"))));
        domicilio.setImporteFianza(ParamUtils.parseFloat(params.getFirst("importeFianza")));
        domicilio.setNumeroArrendatarios(ParamUtils.parseLong(params.getFirst("numeroArrendatarios")));
        domicilio.setReferenciaCatastral(params.getFirst("referenciaCatastral"));


        String inicioContrato = params.getFirst("fechaInicioContrato");
        if (inicioContrato != null && inicioContrato.length() > 0)
        {
            domicilio.setFechaInicioContrato(formateadorFecha.parse(inicioContrato));
        }

        String finContrato = params.getFirst("fechaFinContrato");
        if (finContrato != null && finContrato.length() > 0)
        {
            domicilio.setFechaFinContrato(formateadorFecha.parse(finContrato));
        }

        String adqVivienda = params.getFirst("fechaAdquisicionVivienda");
        if (adqVivienda != null && adqVivienda.length() > 0)
        {
            domicilio.setFechaAdquisicionVivienda(formateadorFecha.parse(adqVivienda));
        }
    }

    private Boolean extraeParametrosCheck(MultivaluedMap<String, String> params, String parametro)
    {
        String valor = params.getFirst(parametro);
        if (valor != null && valor.equals("on"))
        {
            return true;
        }
        return false;
    }

    private TipoDomicilio preparaTipoDomicilio(Long tipoDomicilioId)
    {
        TipoDomicilio tipoDomicilio = null;
        if (tipoDomicilioId != null)
        {
            tipoDomicilio = new TipoDomicilio();
            tipoDomicilio.setId(tipoDomicilioId);
        }
        return tipoDomicilio;
    }

    private TipoVia preparaTipoVia(Long tipoViaId)
    {
        TipoVia tipoVia = null;
        if (tipoViaId != null)
        {
            tipoVia = new TipoVia();
            tipoVia.setId(tipoViaId);
        }
        return tipoVia;
    }

    private Provincia preparaProvincia(Long provinciaId)
    {
        Provincia provincia = null;
        if (provinciaId != null)
        {
            provincia = new Provincia();
            provincia.setId(provinciaId);
        }
        return provincia;
    }

    private Pais preparaPais(String paisId)
    {
        Pais pais = null;
        if (paisId != null)
        {
            pais = new Pais();
            pais.setId(paisId);
        }
        return pais;
    }
    
    private TipoResidencia preparaTipoResidencia(Long tipoResidenciaId)
    {
        TipoResidencia tipoResidencia = null;
        if (tipoResidenciaId != null)
        {
            tipoResidencia = new TipoResidencia();
            tipoResidencia.setId(tipoResidenciaId);
        }
        return tipoResidencia;
    }

    public UIEntity insertaDomicilio(MultivaluedMap<String, String> params)
            throws DomicilioDuplicadoException, ParseException
    {
        Domicilio domicilio = new Domicilio();

        preparaDomicilio(params, domicilio);

        try
        {
            Domicilio newDomicilio = domicilioDAO.insert(domicilio);
            return UIEntity.toUI(newDomicilio);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new DomicilioDuplicadoException();
        }
    }

    public ResponseMessage borraDomicilio(Long domicilioId)
    {
        domicilioDAO.borraDomicilio(domicilioId);
        return new ResponseMessage(true);
    }
}
