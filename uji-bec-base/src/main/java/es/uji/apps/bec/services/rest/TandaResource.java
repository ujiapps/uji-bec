package es.uji.apps.bec.services.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.NotificacionesException;
import es.uji.apps.bec.exceptions.TandaConBecasAsignadasException;
import es.uji.apps.bec.exceptions.TandaDuplicadaParaConvocatoriaAndCursoAcaException;
import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Convocatoria;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.services.ProcesoService;
import es.uji.apps.bec.services.TandaPDFRecursoSubResourceService;
import es.uji.apps.bec.services.TandaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("tanda")
public class TandaResource extends CoreBaseService
{
    @InjectParam
    private TandaService tandaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTandas(@QueryParam("queryParamMap") @DefaultValue("") Map<String, String> queryParamMap)
    {

        return tandaService.getTandas(queryParamMap);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long id) throws RegistroNoEncontradoException,
            NumberFormatException, TandaConBecasAsignadasException
    {
        return tandaService.delete(id);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateTanda(@PathParam("id") Long id, UIEntity entity) throws ParseException,
            TandaDuplicadaParaConvocatoriaAndCursoAcaException
    {
        return tandaService.updateTanda(id, entity);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertTanda(UIEntity entity) throws ParseException,
            TandaDuplicadaParaConvocatoriaAndCursoAcaException
    {
        return tandaService.insertTanda(entity);
    }

    @POST
    @Path("{id}/notifica")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage enviaNotificaciones(@PathParam("id") Long id)
            throws NotificacionesException
    {
        return  tandaService.enviaNotificaciones(id);

    }

    @POST
    @Path("{id}/reinicia")
    @Produces(MediaType.APPLICATION_XML)
    public ResponseMessage reiniciaFechaYEstadosBeca(@PathParam("id") Long id)
            throws NotificacionesException
    {
        return tandaService.reiniciaFechaYEstadosBeca(id);
    }

    @GET
    @Path("{id}/becassinnotificar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getBecasSinNotificar(@PathParam("id") Long tandaId)
    {
        return tandaService.getBecasSinNotificar(tandaId);
    }

    @Path("{tandaId}/recurso/pdf")
    public TandaPDFRecursoSubResourceService getTandaPDF(
            @InjectParam TandaPDFRecursoSubResourceService tandaPDFRecursoSubResourceService)
    {
        return tandaPDFRecursoSubResourceService;
    }
}
