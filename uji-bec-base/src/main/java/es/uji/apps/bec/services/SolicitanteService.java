package es.uji.apps.bec.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import es.uji.apps.bec.model.*;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.bec.exceptions.BecaDelMismoOrganismoYaExisteException;
import es.uji.apps.bec.exceptions.BecaDuplicadaException;
import es.uji.apps.bec.exceptions.BecaEnEstadoNoModificableException;
import es.uji.apps.bec.exceptions.BecaSinEstudioException;
import es.uji.apps.bec.exceptions.ConvocatoriaDeTandaYDeBecaDiferentesException;
import es.uji.apps.bec.exceptions.DemasiadasBecasException;
import es.uji.apps.bec.exceptions.EstudioNoEncontradoException;
import es.uji.apps.bec.exceptions.TandaAsignadaNoEstaActivaException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("solicitante")
public class SolicitanteService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(SolicitanteService.class);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Transactional
    public UIEntity insertarOBuscarBeca(@FormParam("cursoAcademico") String cursoAcademicoId,
                                        @FormParam("identificacion") String identificacion,
                                        @FormParam("convocatoriaId") String convocatoriaId,
                                        @FormParam("codigoArchivoTemporal") String codigoArchivoTemporal,
                                        @FormParam("estudio") String estudioId) throws DemasiadasBecasException,
            BecaDuplicadaException, BecaSinEstudioException, BecaDelMismoOrganismoYaExisteException
    {
        ParamUtils.checkNotNull(cursoAcademicoId, identificacion, convocatoriaId);
        Long estudioIdParam = ParamUtils.parseLong(estudioId);
        Long convocatoriaIdParam = ParamUtils.parseLong(convocatoriaId);

        Solicitante solicitante = Solicitante.getSolicitanteByCursoAcademicoAndIdentificacion(
                cursoAcademicoId, identificacion);

        Beca beca;
        if (solicitante == null)
        {
            beca = insertarSolicitanteYBeca(Long.parseLong(cursoAcademicoId), identificacion,
                    Long.parseLong(convocatoriaId), codigoArchivoTemporal, estudioIdParam);
        }
        else
        {
            beca = anadirBecaASolicitante(solicitante, identificacion, convocatoriaIdParam,
                    codigoArchivoTemporal, estudioIdParam);
        }

        UIEntity generaUIEntitySolicitanteConBeca = generaUIEntitySolicitanteConBeca(beca);

        return generaUIEntitySolicitanteConBeca;
    }

    private UIEntity generaUIEntitySolicitanteConBeca(Beca beca)
    {
        UIEntity uiEntity = UIEntity.toUI(beca.getSolicitante());

        UIEntity uiEntityBeca = UIEntity.toUI(beca);

        uiEntityBeca.put("becaId", beca.getId());
        uiEntityBeca.remove("id");
        uiEntity.putAll(uiEntityBeca);

        return uiEntity;
    }

    private Beca insertarSolicitanteYBeca(Long cursoAcademicoId, String identificacion,
                                          Long convocatoriaId, String codigoArchivoTemporal, Long estudioId)
            throws BecaDuplicadaException, BecaSinEstudioException
    {
        Persona persona = Persona.getPersonaByDNI(identificacion);

        Solicitante solicitante = new Solicitante(cursoAcademicoId, persona);
        Beca beca = new Beca(solicitante, identificacion, codigoArchivoTemporal, estudioId, convocatoriaId);

        solicitante.addBeca(beca);
        solicitante.insert();

        return solicitante.extraerBeca(convocatoriaId, estudioId);
    }

    private Beca anadirBecaASolicitante(Solicitante solicitante, String identificacion,
                                        Long convocatoriaId, String codigoArchivoTemporal, Long estudioId)
            throws BecaDuplicadaException, BecaSinEstudioException,
            BecaDelMismoOrganismoYaExisteException
    {
        Beca becaExistente = Beca.getBeca(solicitante.getId(), convocatoriaId, estudioId);

        if (becaExistente == null)
        {
            Beca beca = new Beca(solicitante, identificacion, codigoArchivoTemporal, estudioId, convocatoriaId);
            beca.insert();

            solicitante.addBeca(beca);
            solicitante.update();

            return solicitante.extraerBeca(convocatoriaId, estudioId);
        }

        return becaExistente;
    }

    private Boolean compruebaSiExisteBecaDelMismoOrganismo(Long solicitanteId, Long convocatoriaId,
                                                           Long estudioId)
    {
        Long organismoId = Convocatoria.getConvocatoriaById(convocatoriaId).getOrganismo().getId();
        Solicitante solicitante = Solicitante.getSolicitanteById(solicitanteId);

        List<Beca> becas =
                solicitante.getBecasByOrganismoAndSolicitante(solicitanteId, organismoId);

        if (becas.size() > 0)
        {
            return true;
        }

        return false;
    }

    @PUT
    @Path("{solicitanteId}/beca/{becaId}/generales")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response guardaSolicitanteYBeca(MultivaluedMap<String, String> params)
            throws BecaDuplicadaException, TandaAsignadaNoEstaActivaException, ParseException,
            ConvocatoriaDeTandaYDeBecaDiferentesException, BecaEnEstadoNoModificableException
    {
        String becaId = params.getFirst("becaId");
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));
        Beca becaDataBase = SerializationUtils.clone(beca);

        preparaActualizacionBeca(params, beca);

        beca.updateDatosGenerales();

        HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, AccessManager.getConnectedUser(request).getName());

        String solicitanteId = params.getFirst("id");
        Solicitante solicitante = Solicitante.getSolicitanteById(Long.parseLong(solicitanteId));
        preparaActualizacionSolicitante(params, solicitante);
        solicitante.updateDatosGenerales();

        return Response.ok(new UIEntity()).build();
    }

    private void preparaActualizacionBeca(MultivaluedMap<String, String> params, Beca beca)
            throws TandaAsignadaNoEstaActivaException, ParseException
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(ParamUtils.parseLong(params.getFirst("convocatoriaId")));
        beca.setConvocatoria(convocatoria);

        Proceso proceso = new Proceso();
        proceso.setId(ParamUtils.parseLong(params.getFirst("procesoId")));
        beca.setProceso(proceso);

        Estado estado = new Estado();
        estado.setId(ParamUtils.parseLong(params.getFirst("estadoId")));
        beca.setEstado(estado);

        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
        tipoIdentificacion.setId(ParamUtils.parseLong(params.getFirst("tipoIdentificacionId")));
        beca.setTipoIdentificacion(tipoIdentificacion);

        Long tandaId = ParamUtils.parseLong(params.getFirst("tandaId"));
        Tanda tanda = null;
        if (tandaId != null)
        {
            tanda = Tanda.getTandaById(tandaId);
            if (!tanda.isActiva())
            {
                throw new TandaAsignadaNoEstaActivaException();
            }
        }
        beca.setTanda(tanda);

        beca.setCodigoArchivoTemporal(params.getFirst("codigoArchivoTemporal"));
        beca.setIdentificacion(params.getFirst("identificacion"));
        beca.setNumeroTransportes(ParamUtils.parseLong(params.getFirst("numeroTransportes")));
        beca.setTransporteUrbano(Boolean.parseBoolean(params.getFirst("transporteUrbano")));
        beca.setAutorizacionRenta(Boolean.parseBoolean(params.getFirst("autorizacionRenta")));
        beca.setBecaConcedida(Boolean.parseBoolean(params.getFirst("becaConcedida")));
        beca.setReclamada(Boolean.parseBoolean(params.getFirst("reclamada")));

        if (params.getFirst("fechaNotificacion").hashCode() == 0)
        {
            beca.setFechaNotificacion(null);
        }
    }

    private void preparaActualizacionSolicitante(MultivaluedMap<String, String> params,
                                                 Solicitante solicitante) throws ParseException
    {
        Long tipoFamiliaId = ParamUtils.parseLong(params.getFirst("tipoFamiliaId"));
        TipoFamilia tipoFamilia = preparaTipoFamilia(tipoFamiliaId);
        solicitante.setTipoFamilia(tipoFamilia);

        Long profesionSustentadorId = ParamUtils.parseLong(params.getFirst("profesionSustentadorId"));
        Profesion profesion = preparaProfesion(profesionSustentadorId);
        solicitante.setProfesionSustentador(profesion);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String fechaOrfandadString = params.getFirst("fechaOrfandad");
        String fechaFallPrimProgenitorString = params.getFirst("fechaFallPrimProgenitor");
        String fechaIniFamiliaNumerosaString = params.getFirst("fechaIniFamiliaNumerosa");
        String fechaFinFamiliaNumerosaString = params.getFirst("fechaFinFamiliaNumerosa");
        String fechaVictimaVgString = params.getFirst("fechaVictimaVg");
        String fechaHijoVictimaVgString = params.getFirst("fechaHijoVictimaVg");


        Date fechaOrfandad = null;
        Date fechaFallPrimProgenitor = null;
        Date fechaIniFamiliaNumerosa = null;
        Date fechaFinFamiliaNumerosa = null;
        Date fechaVictimaVg = null;
        Date fechaHijoVictimaVg = null;

        if (noEsNuloNiVacio(fechaOrfandadString))
        {
            fechaOrfandad = df.parse(fechaOrfandadString);
        }

        if (noEsNuloNiVacio(fechaFallPrimProgenitorString))
        {
            fechaFallPrimProgenitor = df.parse(fechaFallPrimProgenitorString);
        }

        if (noEsNuloNiVacio(fechaIniFamiliaNumerosaString))
        {
            fechaIniFamiliaNumerosa = df.parse(fechaIniFamiliaNumerosaString);
        }

        if (noEsNuloNiVacio(fechaFinFamiliaNumerosaString))
        {
            fechaFinFamiliaNumerosa = df.parse(fechaFinFamiliaNumerosaString);
        }

        if (noEsNuloNiVacio(fechaVictimaVgString))
        {
            fechaVictimaVg = df.parse(fechaVictimaVgString);
        }

        if (noEsNuloNiVacio(fechaHijoVictimaVgString))
        {
            fechaHijoVictimaVg = df.parse(fechaHijoVictimaVgString);
        }

        String comunidadFamiliaNumerosaString = params.getFirst("comunidadFamiliaNumerosaId");
        Comunidad comunidadFamiliaNumerosa = null;
        if (noEsNuloNiVacio(comunidadFamiliaNumerosaString))
        {
            comunidadFamiliaNumerosa = Comunidad.getComunidadById(Long.parseLong(comunidadFamiliaNumerosaString));
        }

        solicitante.setNumeroMiembrosComputables(ParamUtils.parseLong(params.getFirst("numeroMiembrosComputables")));
        solicitante.setNumeroHermanos(ParamUtils.parseLong(params.getFirst("numeroHermanos")));
        solicitante.setNumeroHermanosFuera(ParamUtils.parseLong(params.getFirst("numeroHermanosFuera")));
        solicitante.setNumeroMinusvalia33(ParamUtils.parseLong(params.getFirst("numeroMinusvalia33")));
        solicitante.setNumeroMinusvalia65(ParamUtils.parseLong(params.getFirst("numeroMinusvalia65")));
        solicitante.setOrfandad(Boolean.parseBoolean(params.getFirst("orfandad")));
        solicitante.setIndVictimaVg(Boolean.parseBoolean(params.getFirst("indVictimaVg")));
        solicitante.setIndHijoVictimaVg(Boolean.parseBoolean(params.getFirst("indHijoVictimaVg")));
        solicitante.setIndFamiMonoparental(Boolean.parseBoolean(params.getFirst("indFamiMonoparental")));
        solicitante.setViolenciaGenero(Boolean.parseBoolean(params.getFirst("violenciaGenero")));
        solicitante.setResidenciaPermanente(Boolean.parseBoolean(params.getFirst("residenciaPermanente")));
        solicitante.setResidenciaTrabajo(Boolean.parseBoolean(params.getFirst("residenciaTrabajo")));
        solicitante.setResidenciaEstanciaEstudios(Boolean.parseBoolean(params.getFirst("residenciaEstanciaEstudios")));
        solicitante.setFechaOrfandad(fechaOrfandad);
        solicitante.setFechaFallPrimProgenitor(fechaFallPrimProgenitor);
        solicitante.setFechaIniFamiliaNumerosa(fechaIniFamiliaNumerosa);
        solicitante.setFechaFinFamiliaNumerosa(fechaFinFamiliaNumerosa);
        solicitante.setFechaVictimaVg(fechaVictimaVg);
        solicitante.setFechaHijoVictimaVg(fechaHijoVictimaVg);

        solicitante.setCarnetFamiliaNumerosa(params.getFirst("carnetFamiliaNumerosa"));
        solicitante.setComunidadFamiliaNumerosa(comunidadFamiliaNumerosa);
        solicitante.setOtraBeca(params.getFirst("otraBeca"));
    }

    private TipoFamilia preparaTipoFamilia(Long tipoFamiliaId)
    {
        if (tipoFamiliaId != null)
        {
            TipoFamilia tipoFamilia = new TipoFamilia();
            tipoFamilia.setId(tipoFamiliaId);
            return tipoFamilia;
        }
        return null;
    }

    private Profesion preparaProfesion(Long profesionSustentadorId)
    {
        if (profesionSustentadorId != null)
        {
            Profesion profesion = new Profesion();
            profesion.setId(profesionSustentadorId);
            return profesion;
        }
        return null;
    }

    private boolean noEsNuloNiVacio(String codigoEstudioUjiAnt)
    {
        return codigoEstudioUjiAnt != null && codigoEstudioUjiAnt.length() > 0;
    }

    @GET
    @Path("{solicitanteId}/beca/{becaId}/generales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDatosGenerales(@PathParam("solicitanteId") Long solicitanteId, @PathParam("becaId") Long becaId)
    {
        Solicitante solicitante = Solicitante.getDatosGenerales(becaId);
        Beca beca = Beca.getBecaById(becaId);

        UIEntity uiEntity = UIEntity.toUI(solicitante);

        creaEntidadDatosBeca(solicitante, uiEntity);
        recogeDatosMiembros(beca, uiEntity);
        recogeDatosDomicilios(beca, uiEntity);
        recogeDatosDeclarante(solicitante, uiEntity);

        return Collections.singletonList(uiEntity);
    }

    private void recogeDatosDomicilios(Beca beca, UIEntity uiEntity)
    {
        Domicilio domicilioResidencia = Domicilio.getDomicilioResidencia(beca.getId());
        if (domicilioResidencia != null)
        {
            if (domicilioResidencia.getTipoResidencia() != null)
            {
                uiEntity.put("tipoPropiedad", domicilioResidencia.getTipoResidencia().getNombre());
            }
            uiEntity.put("importeAlquiler", domicilioResidencia.getImporteAlquiler());
        }
    }

    private void recogeDatosMiembros(Beca beca, UIEntity uiEntity)
    {
        Miembro miembroSolicitante = Miembro.getMiembroSolicitante(beca.getId());

        if (miembroSolicitante != null && miembroSolicitante.getTipoMinusvalia() != null)
        {
            uiEntity.put("tipoMinusvalia", miembroSolicitante.getTipoMinusvalia().getNombre());
        }
    }

    private void recogeDatosDeclarante(Solicitante solicitante, UIEntity uiEntity)
    {
        Declarante declarante = solicitante.getDeclarante();

        if (declarante != null && declarante.getIngresosAnuales() != null)
        {
            uiEntity.put("ingresosNetos", declarante.getIngresosAnuales());
        }
        if (declarante != null && declarante.getRentasExtranjero() != null)
        {
            uiEntity.put("ingresosExtranjero", declarante.getRentasExtranjero());
        }
        if (declarante != null && declarante.getIndicadorIndependiente() != null)
        {
            String valorUnidadFamiliarIndependiente = (declarante.getIndicadorIndependiente()
                    .equals(1) ? "true" : "false");
            uiEntity.put("unidadFamiliarIndependiente", valorUnidadFamiliarIndependiente);
        }
    }

    private void creaEntidadDatosBeca(Solicitante datosGenerales, UIEntity uiEntity)
    {
        Set<Beca> datosBecas = datosGenerales.getBecas();
        Beca beca = extraeUnicaBeca(datosBecas);
        if (beca.getConvocatoria() != null)
        {
            uiEntity.put("convocatoriaId", beca.getConvocatoria().getId());
        }
        if (beca.getProceso() != null)
        {
            uiEntity.put("procesoId", beca.getProceso().getId());
        }
        if (beca.getEstado() != null)
        {
            uiEntity.put("estadoId", beca.getEstado().getId());
        }
        if (beca.getTanda() != null)
        {
            uiEntity.put("tandaId", beca.getTanda().getId());
        }
        if (beca.getTipoIdentificacion() != null)
        {
            uiEntity.put("tipoIdentificacionId", beca.getTipoIdentificacion().getId());
        }

        uiEntity.put("codigoBeca", beca.getCodigoBeca());
        uiEntity.put("codigoArchivoTemporal", beca.getCodigoArchivoTemporal());
        uiEntity.put("codigoExpediente", beca.getCodigoExpediente());
        uiEntity.put("identificacion", beca.getIdentificacion());
        uiEntity.put("becaId", beca.getId());
        uiEntity.put("transporteUrbano", beca.isTransporteUrbano());
        uiEntity.put("autorizacionRenta", beca.isAutorizacionRenta());
        uiEntity.put("numeroTransportes", beca.getNumeroTransportes());
        uiEntity.put("fechaNotificacion", beca.getFechaNotificacion());
        uiEntity.put("becaConcedida", beca.isBecaConcedida());
        uiEntity.put("reclamada", beca.isReclamada());
    }

    private static Beca extraeUnicaBeca(Set<Beca> datosBecas)
    {
        Beca beca = null;
        for (Iterator<Beca> iterator = datosBecas.iterator(); iterator.hasNext(); )
        {
            beca = (Beca) iterator.next();
        }
        return beca;
    }

    @GET
    @Path("{solicitanteId}/beca/{becaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getDatosEstudios(@PathParam("solicitanteId") String solicitanteId,
                                     @PathParam("becaId") String becaId)
    {
        BecaEstudio becaEstudio = BecaEstudio.getBecaEstudioByBecaId(Long.parseLong(becaId));
        Solicitante solicitante = Solicitante.getSolicitanteById(Long.parseLong(solicitanteId));

        UIEntity uiEntity = new UIEntity();
        if (becaEstudio != null)
        {
            Estudio estudio = Estudio.getEstudioById(becaEstudio.getEstudioId());
            uiEntity = UIEntity.toUI(becaEstudio);
            uiEntity.put("personaId", solicitante.getPersona().getId());
            uiEntity.put("dobleTitulacion", estudio.getDobleTitulacion());
            uiEntity.put("rama", estudio.getRama());
        }
        else
        {
            uiEntity.setBaseClass("BecaEstudio");
            Beca beca = Beca.getBecaById(Long.parseLong(becaId));
            if (beca.getTipoMatricula() != null)
            {
                uiEntity.put("tipoMatriculaId", beca.getTipoMatricula().getId());
            }
            uiEntity.put("cursoAcademicoId", solicitante.getCursoAcademico().getId());
            uiEntity.put("personaId", solicitante.getPersona().getId());
        }
        creaDatosMatriculaUji(solicitante, uiEntity);
        creaDatosEstudioAnterior(Long.parseLong(becaId), uiEntity);
        return uiEntity;
    }

    private void creaDatosEstudioAnterior(Long becaId, UIEntity uiEntity)
    {
        Beca beca = Beca.getBecaById(becaId);
        uiEntity.put("cambioEstudios", beca.isCambioEstudios());
        uiEntity.put("codigoEstudioAnt", beca.getCodigoEstudioAnt());
        uiEntity.put("codigoEstudioUjiAnt", beca.getCodigoEstudioUjiAnt());
        uiEntity.put("nombreCentroAnt", beca.getNombreCentroAnt());
        uiEntity.put("codigoCentroAnt", beca.getCodigoCentroAnt());
        uiEntity.put("estudioAnt", beca.getEstudioAnt());
        uiEntity.put("planAnteriorNumCursos", beca.getPlanAnteriorNumCursos());
        uiEntity.put("creditosEstudioAnt", beca.getCreditosEstudioAnt());
    }

    private void creaDatosMatriculaUji(Solicitante solicitante, UIEntity uiEntity)
    {
        Long cursoAcademicoId = solicitante.getCursoAcademico().getId();
        Long personaId = solicitante.getPersona().getId();
        List<PersonaEstudio> estudiosPersona = PersonaEstudio.getAcademicosByPersonaYCursoAca(
                personaId, cursoAcademicoId);

        if (!estudiosPersona.isEmpty())
        {
            PersonaEstudio personaEstudio = estudiosPersona.get(0);

            if (personaEstudio.getTipoFamilia() != null)
            {
                uiEntity.put("familiaNumerosa", personaEstudio.getTipoFamilia().getNombre());
            }
            uiEntity.put("becario", personaEstudio.getBecario());
            uiEntity.put("tipoMatricula", personaEstudio.getTipoMatricula());
        }

    }

    @PUT
    @Path("{solicitanteId}/beca/{becaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateDatosEstudios(@PathParam("solicitanteId") Long solicitanteId,
                                        @PathParam("becaId") Long becaId,
                                        @FormParam("tipoMatriculaId") Long tipoMatriculaId,
                                        @FormParam("estudioId") Long estudioId,
                                        @FormParam("codigoEstudioAnt") String codigoEstudioAnt,
                                        @FormParam("codigoEstudioUjiAnt") String codigoEstudioUjiAnt,
                                        @FormParam("codigoCentroAnt") String codigoCentroAnt,
                                        @FormParam("nombreCentroAnt") String nombreCentroAnt,
                                        @FormParam("estudioAnt") String estudioAnt,
                                        @FormParam("planAnteriorNumCursos") Long planAnteriorNumCursos,
                                        @FormParam("creditosEstudioAnt") String creditosEstudioAnt,
                                        @FormParam("cambioEstudios") String cambioEstudios
    )
            throws BecaDuplicadaException, BecaEnEstadoNoModificableException,
            EstudioNoEncontradoException
    {
        Beca beca = Beca.getBecaById(becaId);

        if (beca.noEsModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }



        //Boolean.parseBoolean(params.getFirst("indFamiMonoparental"))



        preparaTipoMatricula(tipoMatriculaId, beca);
        preparaEstudio(estudioId, beca);

        if (!codigoEstudioUjiAnt.equals("") && codigoEstudioAnt.equals(""))
        {
            Long cursoAcademicoId = beca.getSolicitante().getCursoAcademico().getId();
            Long organismoId = beca.getConvocatoria().getOrganismo().getId();
            String codigoRuct = Diccionario.getValorOrigen(cursoAcademicoId, "Estudio", organismoId, codigoEstudioUjiAnt);
            if (codigoRuct.equals(""))
            {
                codigoRuct = DiccionarioExcepciones.getValorOrigen(cursoAcademicoId, "Estudio", organismoId, codigoEstudioUjiAnt);
                if (codigoRuct.equals(""))
                {
                    throw new EstudioNoEncontradoException();
                }
            }
            beca.setCodigoEstudioAnt(codigoRuct);
        }
        else
        {
            beca.setCodigoEstudioAnt(codigoEstudioAnt);
        }

        beca.setCodigoEstudioUjiAnt(codigoEstudioUjiAnt);
        beca.setNombreCentroAnt(nombreCentroAnt);
        beca.setEstudioAnt(estudioAnt);

        beca.setPlanAnteriorNumCursos(planAnteriorNumCursos);
        beca.setCreditosEstudioAnt(ParamUtils.parseFloat(creditosEstudioAnt));
        beca.setCodigoCentroAnt(codigoCentroAnt);
        if (cambioEstudios != null && cambioEstudios.equals("on"))
        {
            beca.setCambioEstudios(true);
        }
        else
        {
            beca.setCambioEstudios(false);
        }

        if (noEsNuloNiVacio(codigoEstudioUjiAnt))
        {
            Estudio estudio = Estudio.getEstudioById(ParamUtils.parseLong(codigoEstudioUjiAnt));
            if (estudio == null)
            {
                throw new EstudioNoEncontradoException();
            }
            beca.setPlanAnteriorNumCursos(estudio.getNumeroCursos());
            beca.setCreditosEstudioAnt(estudio.getCreditosTotales());
            beca.setCodigoCentroAnt(estudio.getCodigoCentro());
        }

        beca.updateEstudios();

        UIEntity uiEntity = preparaUIEntityBecaEstudio(beca);
        return uiEntity;
    }

    private void preparaTipoMatricula(Long tipoMatriculaId, Beca beca)
    {
        if (tipoMatriculaId != null)
        {
            TipoMatricula tipoMatricula = new TipoMatricula();
            tipoMatricula.setId(tipoMatriculaId);
            beca.setTipoMatricula(tipoMatricula);
        }
    }

    private void preparaEstudio(Long estudioId, Beca beca)
    {
        if (estudioId != null)
        {
            Estudio estudio = new Estudio();
            estudio.setId(estudioId);
            beca.setEstudio(estudio);
        }
    }

    private UIEntity preparaUIEntityBecaEstudio(Beca beca)
    {
        BecaEstudio becaEstudio = BecaEstudio.getBecaEstudioByBecaId(beca.getId());

        UIEntity uiEntity = new UIEntity();

        if (becaEstudio != null)
        {
            uiEntity = UIEntity.toUI(becaEstudio);
        }
        else
        {
            preparaBecaEstudioMinimo(beca, uiEntity);
        }

        uiEntity.put("personaId", beca.getSolicitante().getPersona().getId());
        return uiEntity;
    }

    private void preparaBecaEstudioMinimo(Beca beca, UIEntity uiEntity)
    {
        uiEntity.setBaseClass("BecaEstudio");
        if (beca.getTipoMatricula() != null)
        {
            uiEntity.put("tipoMatriculaId", beca.getTipoMatricula().getId());
        }
        uiEntity.put("cursoAcademicoId", beca.getSolicitante().getCursoAcademico().getId());
    }

    @GET
    @Path("{solicitanteId}/beca/{becaId}/otrosdatos")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getOtrosDatos(@PathParam("solicitanteId") String solicitanteId,
                                  @PathParam("becaId") String becaId)
    {
        Solicitante solicitante = Solicitante.getSolicitanteAndBeca(Long.parseLong(solicitanteId),
                Long.parseLong(becaId));
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        UIEntity uiEntity = UIEntity.toUI(solicitante);
        uiEntity.put("entidad", beca.getEntidad());
        uiEntity.put("sucursal", beca.getSucursal());
        uiEntity.put("digitosControl", beca.getDigitosControl());
        uiEntity.put("cuentaBancaria", beca.getCuentaBancaria());
        uiEntity.put("observaciones", beca.getObservaciones());
        uiEntity.put("ibanPais", beca.getIbanPais());
        uiEntity.put("ibanDC", beca.getIbanDC());

        if (beca.getEstudio() != null)
        {
            uiEntity.put("estudioId", beca.getEstudio().getId());
        }

        String activeSession = AccessManager.getConnectedUser(request).getActiveSession();
        uiEntity.put("session", activeSession);

        return uiEntity;
    }

    @PUT
    @Path("{solicitanteId}/beca/{becaId}/otrosdatos")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage updateOtrosDatos(@PathParam("solicitanteId") String solicitanteId,
                                            @PathParam("becaId") String becaId, MultivaluedMap<String, String> params)
            throws NumberFormatException, BecaEnEstadoNoModificableException
    {
        Solicitante solicitante = Solicitante.getSolicitanteAndBeca(Long.parseLong(solicitanteId),
                Long.parseLong(becaId));
        Beca beca = Beca.getBecaById(Long.parseLong(becaId));

        if (beca.noEsModificable())
        {
            throw new BecaEnEstadoNoModificableException();
        }

        if (solicitante == null)
        {
            return new ResponseMessage(false, "El solicitante no existe");
        }

        preparaOtrosDatosSolicitante(params, solicitante);
        solicitante.updateOtrosDatos();

        preparaOtrosDatosBeca(params, beca);
        beca.updateOtrosDatos();

        return new ResponseMessage(true);
    }

    private void preparaOtrosDatosSolicitante(MultivaluedMap<String, String> params, Solicitante solicitante)
    {
        solicitante.setTelefono1(params.getFirst("telefono1"));
        solicitante.setTelefono2(params.getFirst("telefono2"));
        solicitante.setEmail(params.getFirst("email"));
    }

    private void preparaOtrosDatosBeca(MultivaluedMap<String, String> params, Beca beca)
    {
        beca.setEntidad(params.getFirst("entidad"));
        beca.setSucursal(params.getFirst("sucursal"));
        beca.setDigitosControl(params.getFirst("digitosControl"));
        beca.setCuentaBancaria(params.getFirst("cuentaBancaria"));
        beca.setObservaciones(params.getFirst("observaciones"));
        beca.setIbanPais(params.getFirst("ibanPais"));
        beca.setIbanDC(params.getFirst("ibanDC"));
    }

    @GET
    @Path("{solicitanteId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getSolicitante(@PathParam("solicitanteId") String solicitanteId)
    {
        Solicitante solicitante = Solicitante.getSolicitanteById(Long.parseLong(solicitanteId));

        UIEntity entity = UIEntity.toUI(solicitante);

        TipoFamilia tipoFamilia = solicitante.getTipoFamilia();

        if (tipoFamilia != null)
        {
            entity.put("tipoFamilia", tipoFamilia.getNombre());
        }
        return entity;
    }

}
