package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.OrganismoDAO;
import es.uji.apps.bec.model.Organismo;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class OrganismoService
{
    private OrganismoDAO organismoDAO;

    @Autowired
    public OrganismoService(OrganismoDAO organismoDAO)
    {
        this.organismoDAO = organismoDAO;
    }

    public List<UIEntity> getOrganismos()
    {
        List<Organismo> organismos = organismoDAO.getOrganismos();
        return UIEntity.toUI(organismos);
    }

    public UIEntity insertaOrganismo(UIEntity entity)
    {
        Organismo organismo = preparaOrganismo(entity);
        if (organismo.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(organismoDAO.insert(organismo));
    }

    public ResponseMessage actualizaOrganismo(UIEntity entity)
    {
        Organismo organismo = preparaOrganismo(entity);

        organismoDAO.update(organismo);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraOrganismo(Long organismoId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        Organismo organismo = organismoDAO.getOrganismoById(organismoId);

        if (organismo == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            organismoDAO.deleteOrganismo(organismo.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private Organismo preparaOrganismo(UIEntity entity)
    {
        Organismo organismo = new Organismo();
        organismo.setId(ParamUtils.parseLong(entity.get("id")));
        organismo.setNombre(entity.get("nombre"));
        return organismo;
    }
}
