package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoMonedaDAO;
import es.uji.apps.bec.model.TipoMoneda;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class TipoMonedaService extends CoreBaseService
{
    private TipoMonedaDAO tipoMonedaDAO;

    @Autowired
    public TipoMonedaService(TipoMonedaDAO tipoMonedaDAO)
    {
        this.tipoMonedaDAO = tipoMonedaDAO;
    }

    public List<UIEntity> getTiposMonedas()
    {
        List<TipoMoneda> tiposMonedas = tipoMonedaDAO.getTiposMonedas();
        return UIEntity.toUI(tiposMonedas);
    }

    public UIEntity insertaTipoMoneda(UIEntity entity)
    {
        TipoMoneda tipoMoneda = preparaTipoMoneda(entity);
        if (tipoMoneda.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoMonedaDAO.insert(tipoMoneda));
    }

    public ResponseMessage actualizaTipoMoneda(UIEntity entity)
    {
        TipoMoneda tipoMoneda = preparaTipoMoneda(entity);

        tipoMonedaDAO.update(tipoMoneda);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoMoneda(Long tipoMonedaId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoMoneda tipoMoneda = tipoMonedaDAO.getTipoMonedaById(tipoMonedaId);

        if (tipoMoneda == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoMonedaDAO.deleteTipoMoneda(tipoMoneda.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoMoneda preparaTipoMoneda(UIEntity entity)
    {
        TipoMoneda tipoMoneda = new TipoMoneda();
        tipoMoneda.setId(ParamUtils.parseLong(entity.get("id")));
        tipoMoneda.setNombre(entity.get("nombre"));
        tipoMoneda.setCodigo(entity.get("codigo"));
        return tipoMoneda;
    }
}
