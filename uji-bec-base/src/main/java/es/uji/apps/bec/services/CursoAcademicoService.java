package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.CursoAcademicoDAO;
import es.uji.apps.bec.exceptions.CursoAcademicoException;
import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class CursoAcademicoService
{
    private CursoAcademicoDAO cursoAcademicoDAO;

    @Autowired
    public CursoAcademicoService(CursoAcademicoDAO cursoAcademicoDAO)
    {
        this.cursoAcademicoDAO = cursoAcademicoDAO;
    }

    public List<CursoAcademico> getCursos()
    {
        return cursoAcademicoDAO.getCursos();
    }

    public List<UIEntity> getCursoActivo()
    {
        List<CursoAcademico> cursoActivo = cursoAcademicoDAO.getCursoActivo();
        return UIEntity.toUI(cursoActivo);
    }

    public UIEntity insertCursoAcademico(Long cursoAcademicoId) throws CursoAcademicoException, ParseException
    {
        CursoAcademico cursoAcademico = new CursoAcademico();

        cursoAcademico.setId(cursoAcademicoId);
        cursoAcademico.setActivo(false);

        try
        {
            CursoAcademico nuevoCursoAcademico = cursoAcademico.insert();
            return UIEntity.toUI(nuevoCursoAcademico);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new CursoAcademicoException("No es poden inserir dos anys acadèmics iguals");
        }
    }

    public ResponseMessage delete(Long cursoAcademicoId) throws RegistroNoEncontradoException,
            NumberFormatException, RegistroConHijosException, GeneralBECException
    {
        CursoAcademico cursoAcademico = cursoAcademicoDAO.getCursoAcademicoById(cursoAcademicoId);

        if (cursoAcademico == null)
        {
            throw new RegistroNoEncontradoException();
        }

        if (cursoAcademico.isActivo())
        {
            throw new GeneralBECException("No pots esborrar el curs actualment actiu");
        }
        try
        {
            cursoAcademicoDAO.deleteCursoAcademico(cursoAcademicoId);
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    public ResponseMessage activaCursoAcademico(Long cursoAcademicoId)
    {
        CursoAcademico cursoAcademico = cursoAcademicoDAO.get(CursoAcademico.class, cursoAcademicoId).get(0);

        cursoAcademicoDAO.desactivaCursosActivos();

        cursoAcademico.setActivo(true);
        cursoAcademico.update();

        return new ResponseMessage(true);
    }

    public List<CursoAcademico> getCursosMayoresOIgualesQue(Long cursoAcademicoId)
    {
        return cursoAcademicoDAO.getCursosMayoresOIgualesQue(cursoAcademicoId);
    }
}
