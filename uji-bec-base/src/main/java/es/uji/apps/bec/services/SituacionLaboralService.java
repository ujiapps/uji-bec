package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.SituacionLaboralDAO;
import es.uji.apps.bec.model.SituacionLaboral;
import es.uji.commons.rest.UIEntity;

@Service
public class SituacionLaboralService
{
    private SituacionLaboralDAO situacionLaboralDAO;

    @Autowired
    public SituacionLaboralService(SituacionLaboralDAO situacionLaboralDAO)
    {
        this.situacionLaboralDAO = situacionLaboralDAO;
    }

    public List<UIEntity> getSituacionesLaborales()
    {
        List<SituacionLaboral> situtacionesLaborales = situacionLaboralDAO.getSituacionesLaborales();
        return UIEntity.toUI(situtacionesLaborales);
    }
}
