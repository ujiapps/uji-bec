package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoFamiliaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipofamilia")
public class TipoFamiliaResource extends CoreBaseService
{
    @InjectParam
    private TipoFamiliaService tipoFamiliaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposFamilias()
    {
        return tipoFamiliaService.getTiposFamilias();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoFamilia(UIEntity entity) throws GeneralBECException, ParseException
    {
        return tipoFamiliaService.insertaTipoFamilia(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoFamilia(@PathParam("id") Long tipoFamiliaId, UIEntity entity)
    {
        return tipoFamiliaService.actualizaTipoFamilia(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoFamilia(@PathParam("id") Long tipoFamiliaId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoFamiliaService.borraTipoFamilia(tipoFamiliaId);
    }
}
