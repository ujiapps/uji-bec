package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoSustentadorDAO;
import es.uji.apps.bec.model.TipoSustentador;
import es.uji.commons.rest.UIEntity;

@Service
public class TipoSustentadorService
{
    private TipoSustentadorDAO tipoSustentadorDAO;

    @Autowired
    public TipoSustentadorService(TipoSustentadorDAO tipoSustentadorDAO)
    {
        this.tipoSustentadorDAO = tipoSustentadorDAO;
    }

    public List<UIEntity> getTiposSustentadores()
    {
        List<TipoSustentador> tiposSustentadores = tipoSustentadorDAO.getTiposSustentadores();
        return UIEntity.toUI(tiposSustentadores);
    }
}
