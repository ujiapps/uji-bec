package es.uji.apps.bec.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.ProcesoDAO;
import es.uji.apps.bec.model.Proceso;
import es.uji.commons.rest.UIEntity;

@Service
public class ProcesoService
{
    private ProcesoDAO procesoDAO;

    @Autowired
    public ProcesoService(ProcesoDAO procesoDAO)
    {
        this.procesoDAO = procesoDAO;
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getProcesos()
    {
        List<Proceso> procesos = procesoDAO.getProcesos();
        return UIEntity.toUI(procesos);
    }
}
