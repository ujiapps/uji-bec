package es.uji.apps.bec.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.bec.model.Listado;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("listado")
public class ListadoService extends CoreBaseService
{
    @GET
    @Path("/organismo/{organismoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getListadosByOrganismoId(@PathParam("organismoId") String organismoId)
    {
        return UIEntity.toUI(Listado.getListadosByOrganismoId(Long.parseLong(organismoId)));

    }
}
