package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.CuantiaCursoDAO;
import es.uji.apps.bec.model.Cuantia;
import es.uji.apps.bec.model.CuantiaCurso;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CuantiaCursoService extends CoreBaseService
{
    private CuantiaCursoDAO cuantiaCursoDAO;

    @Autowired
    public CuantiaCursoService(CuantiaCursoDAO cuantiaCursoDAO)
    {
        this.cuantiaCursoDAO = cuantiaCursoDAO;
    }

    public List<UIEntity> getCuantiasCurso() {
        return UIEntity.toUI(cuantiaCursoDAO.getCuantiasCurso());
    }


    public UIEntity insertaCuantiaCurso(UIEntity entity)
    {
        CuantiaCurso cuantiaCurso = preparaCuantiaCurso(entity);
        if (cuantiaCurso.getCuantia() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(cuantiaCursoDAO.insert(cuantiaCurso));
    }

    public ResponseMessage borraCuantiaCurso(Long cuantiaCursoId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        CuantiaCurso cuantiaCurso = cuantiaCursoDAO.getCuantiaCursoById(cuantiaCursoId);
        if (cuantiaCurso == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            cuantiaCursoDAO.deleteCuantiaCurso(cuantiaCurso.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    public ResponseMessage actualizaCuantiaCurso(UIEntity entity)
    {
        CuantiaCurso cuantiaCurso = preparaCuantiaCurso(entity);

        cuantiaCursoDAO.update(cuantiaCurso);

        return new ResponseMessage(true);
    }

    private CuantiaCurso preparaCuantiaCurso(UIEntity entity)
    {
        CuantiaCurso cuantiaCurso = new CuantiaCurso();
        cuantiaCurso.setImporte(Long.parseLong(entity.get("importe")));

        CursoAcademico cursoAcademico = preparaCursoAcademico(entity);
        cuantiaCurso.setCursoAcademico(cursoAcademico);

        Cuantia cuantia = preparaCuantia(entity);
        cuantiaCurso.setCuantia(cuantia);

        return cuantiaCurso;
    }

    private CursoAcademico preparaCursoAcademico(UIEntity entity) {
        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setId(Long.parseLong(entity.get("cursoAcademicoId")));

        return cursoAcademico;
    }

    private Cuantia preparaCuantia(UIEntity entity)
    {
        Cuantia cuantia = new Cuantia();
        cuantia.setId(ParamUtils.parseLong(entity.get("cuantiaId")));
        cuantia.setActiva(Boolean.TRUE);

        return cuantia;
    }
}
