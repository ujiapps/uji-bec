package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.TipoMiembroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("tipomiembro")
public class TipoMiembroResource extends CoreBaseService
{
    @InjectParam
    private TipoMiembroService tipoMiembroService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposMiembros()
    {
        return tipoMiembroService.getTiposMiembros();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaTipoMiembro(UIEntity entity) throws GeneralBECException, ParseException
    {
        return tipoMiembroService.insertaTipoMiembro(entity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaTipoMiembro(@PathParam("id") Long tipoMiembroId, UIEntity entity)
    {
        return tipoMiembroService.actualizaTipoMiembro(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage borraTipoMiembro(@PathParam("id") Long tipoMiembroId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        return tipoMiembroService.borraTipoMiembro(tipoMiembroId);
    }
}
