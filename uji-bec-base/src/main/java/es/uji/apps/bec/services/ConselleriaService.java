package es.uji.apps.bec.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.bec.exceptions.*;
import es.uji.commons.rest.*;
import org.apache.commons.lang3.SerializationUtils;

import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.bec.model.Beca;
import es.uji.apps.bec.model.Estado;
import es.uji.apps.bec.model.FicheroConselleria;
import es.uji.apps.bec.model.HistoricoBeca;
import es.uji.apps.bec.model.Tanda;
import es.uji.apps.bec.model.domains.CodigoEstado;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;


@Path("conselleria")
public class ConselleriaService extends CoreBaseService
{

    public static final long BECA_MANUELA = 11L;
    public static final long BECA_FINALIZACION = 12L;
    public static final long BECA_GENERAL = 3L;

    @GET
    @Path("tanda/{tandaId}/propuesta")
    public Response generaArchivoPropuestaGVA(@PathParam("tandaId") String tandaId)
            throws WsErrorAlPrepararMultienvioException, DNIIncorrectoException,
            DomicilioNoEncontradoException, DomicilioIncorrectoException,
            MiembroIncorrectoException, FaltaNotaMediaException, BecaException, ParseException {
        Tanda tanda = Tanda.getTandaById(Long.parseLong(tandaId));
        Boolean notaMediaObligatoria = true;

        if (tanda.isCorrectaParaEnviar())
        {
            FicheroConselleria ficheroEnvio = new FicheroConselleria();

            byte[] datos = null;

            if (tanda.getConvocatoria().getId() == BECA_FINALIZACION) {
                if (tanda.isTrabajada())
                {
                    Map<String, String> fichero = ficheroEnvio.getFicheroCSVPropuestaGVAFinalizacionParaTandaId(ParamUtils.parseLong(tandaId), notaMediaObligatoria);
                    String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

                    datos = generaFicheroEnvio(datos, fichero);

                    marcarBecasCorrectasComoEnviadas(Long.parseLong(tandaId));

                    setFechaEnvioEnTanda(Long.parseLong(tandaId));
                    return Response.ok(datos).header("Content-disposition", contentDisposition).build();
                }

                if (tanda.isDenegadaAcademicos())
                {
                    Map<String, String> fichero = ficheroEnvio.getFicheroDenegacionesFinalizacionByTandaId(ParamUtils.parseLong(tandaId));
                    String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));
                    datos = generaFicheroEnvio(datos, fichero);
                    setFechaEnvioEnTanda(Long.parseLong(tandaId));
                    return Response.ok(datos).header("Content-disposition", contentDisposition).build();
                }

            }

            if (tanda.getConvocatoria().getId() == BECA_MANUELA) {
                Map<String, String> fichero = ficheroEnvio.getFicheroCSVPropuestaGVAManuelaParaTandaId(ParamUtils.parseLong(tandaId), notaMediaObligatoria);
                String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

                datos = generaFicheroEnvio(datos, fichero);

                marcarBecasCorrectasComoEnviadas(Long.parseLong(tandaId));

                setFechaEnvioEnTanda(Long.parseLong(tandaId));
                return Response.ok(datos).header("Content-disposition", contentDisposition).build();
            }

            if (tanda.getConvocatoria().getId() == BECA_GENERAL) {
                if (tanda.isTrabajada()) {
                    Map<String, String> fichero;
                    if (tanda.compruebaTipoEnvioConselleria().equals("MEC"))
                    {
                        fichero = ficheroEnvio.getFicheroPropuestaMec2CrdParaTandaId(ParamUtils.parseLong(tandaId), notaMediaObligatoria);
                    }
                    else if (tanda.compruebaTipoEnvioConselleria().equals("NO_MEC"))
                    {
                        fichero = ficheroEnvio.getFicheroPropuestaParaTandaId(ParamUtils.parseLong(tandaId), notaMediaObligatoria);
                    }
                    else
                    {
                        throw new WsErrorAlPrepararMultienvioException("La tanda te beques amb MEC i sense MEC");
                    }

                    String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));
                    datos = generaFicheroEnvio(datos, fichero);
                    marcarBecasCorrectasComoEnviadas(Long.parseLong(tandaId));
                    setFechaEnvioEnTanda(Long.parseLong(tandaId));
                    return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
                }

                if (tanda.isDenegadaAcademicos()){
                    Map<String, String> fichero = ficheroEnvio.getFicheroDenegacionesByTandaId(ParamUtils.parseLong(tandaId));
                    String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));
                    datos = generaFicheroEnvio(datos, fichero);
                    setFechaEnvioEnTanda(Long.parseLong(tandaId));
                    return Response.ok(datos).header("Content-disposition", contentDisposition).build();
                }
            }
        }
        throw new WsErrorAlPrepararMultienvioException("Tanda no correcta per a ser enviada");
    }

    @POST
    @Path("/cargar/respuesta")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage procesaArchivoRespuestaConselleria(FormDataMultiPart multiPart)
            throws IOException, BecaException, BecaDuplicadaException,
            BecaDenegacionDuplicadaException
    {
        if (multiPart.getField("ficheroConselleria") == null)
        {
            return new ResponseMessage(false, "No hi ha un fitxer correcte.");
        }

        FicheroConselleria ficheroRespuestaConselleria = new FicheroConselleria();
        byte[] ficheroConselleria = null;
        ficheroConselleria = StreamUtils.inputStreamToByteArray(multiPart.getField("ficheroConselleria").getEntityAs(InputStream.class));

        String usuarioConectado = AccessManager.getConnectedUser(request).getName();
        String mensaje = ficheroRespuestaConselleria.procesaRespuesta(ficheroConselleria, usuarioConectado);

        return new ResponseMessage(true, mensaje);
    }

    @POST
    @Path("/manuelasolis/cargar/concedidas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage procesaArchivoManuelaSolisConcedidas(FormDataMultiPart multiPart)
            throws IOException, BecaCuantiaDuplicadaException, BecaException {
        if (multiPart.getField("ficheroConselleria") == null)
        {
            return new ResponseMessage(false, "No hi ha un fitxer correcte.");
        }

        FicheroConselleria ficheroRespuestaConselleria = new FicheroConselleria();
        byte[] ficheroConselleria = null;
        ficheroConselleria = StreamUtils.inputStreamToByteArray(multiPart.getField("ficheroConselleria").getEntityAs(InputStream.class));

        String usuarioConectado = AccessManager.getConnectedUser(request).getName();
        String mensaje = ficheroRespuestaConselleria.procesaRespuestaManuelaSolisConcedidas(ficheroConselleria, usuarioConectado);

        return new ResponseMessage(true, mensaje);
    }

    @POST
    @Path("/manuelasolis/cargar/denegadas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage procesaArchivoManuelaSolisDenegadas(FormDataMultiPart multiPart)
            throws IOException, BecaException, BecaDuplicadaException,
            BecaDenegacionDuplicadaException
    {
        if (multiPart.getField("ficheroConselleria") == null)
        {
            return new ResponseMessage(false, "No hi ha un fitxer correcte.");
        }

        FicheroConselleria ficheroRespuestaConselleria = new FicheroConselleria();
        byte[] ficheroConselleria = null;
        ficheroConselleria = StreamUtils.inputStreamToByteArray(multiPart.getField("ficheroConselleria").getEntityAs(InputStream.class));

        String usuarioConectado = AccessManager.getConnectedUser(request).getName();
        String mensaje = ficheroRespuestaConselleria.procesaRespuestaManuelaSolisDenegadas(ficheroConselleria, usuarioConectado);

        return new ResponseMessage(true, mensaje);
    }

    @GET
    @Path("todas/{cursoAcademicoId}")
    public Response generaArchivoTodasGVA(@PathParam("cursoAcademicoId") String cursoAcademicoId)
    {
        FicheroConselleria ficheroEnvio = new FicheroConselleria();
        byte[] datos = null;

        Map<String, String> fichero = ficheroEnvio.getFicheroTodas(ParamUtils.parseLong(cursoAcademicoId));
        String contentDisposition = MessageFormat.format("attachment; filename=\"{0}\"", fichero.get("nombre"));

        datos = generaFicheroEnvio(datos, fichero);
        return Response.ok(datos).type("text/plain; charset=ISO-8859-1").header("Content-disposition", contentDisposition).build();
    }


    @GET
    @Path("todas/denegadas/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFTodasDenegadasGVA(@PathParam("cursoAcademicoId") String cursoAcademicoId)
    {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroConselleria ficheroEnvio = new FicheroConselleria();

        Map<String, Object> datosBecas = ficheroEnvio.getEstructuraBecasDenegadasPorCursoAcademico(ParamUtils.parseLong(cursoAcademicoId));

        Template template = new PDFTemplate("bec/todasDenegadasGVA", new Locale("ES"), "bec");
        template.put("datos", datosBecas);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }

    @GET
    @Path("todas/concedidas/{cursoAcademicoId}/pdf")
    @Produces("application/pdf")
    public Template generatePDFTodasConcedidasGVA(@PathParam("cursoAcademicoId") String cursoAcademicoId)
    {
        Calendar calendario = Calendar.getInstance(new Locale("CA"));
        String cursoAcademico = MessageFormat.format("{0,number,#}/{1,number,#}", Integer.parseInt(cursoAcademicoId), (Integer.parseInt(cursoAcademicoId) + 1));

        FicheroConselleria ficheroEnvio = new FicheroConselleria();

        Map<String, Object> datosBecas = ficheroEnvio.getEstructuraBecasConcedidasPorCursoAcademico(ParamUtils.parseLong(cursoAcademicoId));

        Template template = new PDFTemplate("bec/todasConcedidasGVA", new Locale("ES"), "bec");
        template.put("datos", datosBecas);
        template.put("calendario", calendario);
        template.put("cursoAcademico", cursoAcademico);

        return template;
    }


    private byte[] generaFicheroEnvio(byte[] datos, Map<String, String> fichero)
    {
        try
        {
            datos = fichero.get("datos").getBytes("ISO-8859-1");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return datos;
    }

    public void marcarBecasCorrectasComoEnviadas(Long tandaId)
    {
        List<Beca> listaBecas = Beca.getBecasByTandaId(tandaId);
        if (listaBecas.size() > 0)
        {
            for (Iterator<Beca> iterator = listaBecas.iterator(); iterator.hasNext(); )
            {
                Beca beca = (Beca) iterator.next();
                Beca becaDataBase = SerializationUtils.clone(beca);

                if (laBecaNoEstaEnEstadoEnviado(beca))
                {
                    beca.setEstado(Estado.getEstadoById(CodigoEstado.ENVIADA.getId()));
                    beca.updateEstado();
                    String usuarioConectado = AccessManager.getConnectedUser(request).getName();
                    HistoricoBeca.insertHistoricoSiCambios(beca, becaDataBase, usuarioConectado);
                }
            }
        }
    }

    private boolean laBecaNoEstaEnEstadoEnviado(Beca beca)
    {
        return beca.getEstado().getId() != CodigoEstado.ENVIADA.getId();
    }

    public void setFechaEnvioEnTanda(Long tandaId)
    {
        Tanda tanda = Tanda.getTandaById(tandaId);
        if (tanda.getFecha() == null)
        {
            Date ahora = new Date();
            tanda.setFecha(ahora);
            tanda.updateTanda();
        }
    }
}
