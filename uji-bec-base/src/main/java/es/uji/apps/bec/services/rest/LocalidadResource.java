package es.uji.apps.bec.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.services.LocalidadService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("localidad")
public class LocalidadResource extends CoreBaseService
{
    @InjectParam
    private LocalidadService localidadService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLocalidades()
    {
        return localidadService.getLocalidades();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("provincia/{id}")
    public List<UIEntity> getLocalidadesByProvincia(@PathParam("id") Long provinciaId)
    {
        return localidadService.getLocalidadesByProvincia(provinciaId);
    }
}
