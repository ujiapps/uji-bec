package es.uji.apps.bec.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.bec.exceptions.GeneralBECException;
import es.uji.apps.bec.services.DenegacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("denegacion")
public class DenegacionResource extends CoreBaseService
{
    @InjectParam
    private DenegacionService denegacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDenegaciones()
    {
        return denegacionService.getDenegaciones();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("activa")
    public List<UIEntity> getDenegacionesActivas()
    {
        return denegacionService.getDenegacionesActivas();
    }

    @GET
    @Path("activa/{organismoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDenegacionesActivasByOrganismo(@PathParam("organismoId") Long organismoId, @QueryParam("query") String query)
    {
        List<UIEntity> denegaciones = denegacionService.getDenegacionesActivasByOrganismo(organismoId);
        return filtraListaDenegaciones(denegaciones, query);
    }

    private List<UIEntity> filtraListaDenegaciones(List<UIEntity> denegaciones, String query) {

        if (query == null || query.isEmpty()) {
            return denegaciones;
        }

        List<UIEntity> listaFiltradaDenegaciones = new ArrayList<>();

        for (UIEntity denegacion: denegaciones) {
            if (denegacion.get("nombreListaValores").contains(query)) {
                listaFiltradaDenegaciones.add(denegacion);
            }
        }
        return listaFiltradaDenegaciones;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertaDenegacion(UIEntity entity) throws GeneralBECException, ParseException
    {
        String denegacionId = entity.get("id");
        if (!ParamUtils.isNotNull(denegacionId))
        {
            return new UIEntity();
        }

        return denegacionService.insertaDenegacion(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long denegacionId) throws RegistroNoEncontradoException,
            RegistroConHijosException, GeneralBECException
    {
        return denegacionService.borraDenegacion(denegacionId);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaDenegacion(@PathParam("id") Long denegacionId, UIEntity entity)
            throws GeneralBECException, ParseException
    {
        return denegacionService.actualizaDenegacion(entity);
    }
}
