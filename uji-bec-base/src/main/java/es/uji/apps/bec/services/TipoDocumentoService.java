package es.uji.apps.bec.services;

import es.uji.apps.bec.dao.dao.TipoDocumentoDAO;
import es.uji.apps.bec.model.TipoDocumento;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoDocumentoService
{
    private TipoDocumentoDAO tipoDocumentoDAO;

    @Autowired
    public TipoDocumentoService(TipoDocumentoDAO tipoDocumentoDAO)
    {
        this.tipoDocumentoDAO = tipoDocumentoDAO;
    }

    public List<UIEntity> getTiposDocumentos()
    {
        List<TipoDocumento> tiposDocumentos = tipoDocumentoDAO.getTiposDocumentos();
        return UIEntity.toUI(tiposDocumentos);
    }

    public UIEntity insertaTipoDocumento(UIEntity entity)
    {
        TipoDocumento tipoDocumento = preparaTipoDocumento(entity);
        if (tipoDocumento.getNombre() == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(tipoDocumentoDAO.insert(tipoDocumento));
    }

    public ResponseMessage actualizaTipoDocumento(UIEntity entity)
    {
        TipoDocumento tipoDocumento = preparaTipoDocumento(entity);

        tipoDocumentoDAO.update(tipoDocumento);

        return new ResponseMessage(true);
    }

    public ResponseMessage borraTipoDocumento(Long tipoDocumentoId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        TipoDocumento tipoDocumento = tipoDocumentoDAO.getTipoDocumentoById(tipoDocumentoId);

        if (tipoDocumento == null)
        {
            throw new RegistroNoEncontradoException();
        }

        try
        {
            tipoDocumentoDAO.deleteTipoDocumento(tipoDocumento.getId());
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }

        return new ResponseMessage(true);
    }

    private TipoDocumento preparaTipoDocumento(UIEntity entity)
    {
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ParamUtils.parseLong(entity.get("id")));
        tipoDocumento.setNombre(entity.get("nombre"));
        return tipoDocumento;
    }

}
