package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TituloPoseeDAO;
import es.uji.apps.bec.model.TituloPosee;

@Service
public class TituloPoseeService
{
    private TituloPoseeDAO tituloPoseeDAO;

    @Autowired
    public TituloPoseeService(TituloPoseeDAO tituloPoseeDAO)
    {
        this.tituloPoseeDAO = tituloPoseeDAO;
    }

    public List<TituloPosee> getTitulosPoseeBySolicitante(Long solicitanteId) {
        return tituloPoseeDAO.getTitulosPoseeBySolicitante(solicitanteId);
    }
}
