package es.uji.apps.bec.services.rest;

import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.bec.model.BecaRecuento;
import es.uji.apps.bec.model.QBecaRecuento;
import es.uji.apps.bec.services.BecaRecuentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("becaRecuento")
public class BecaRecuentoResource extends CoreBaseService
{
    @InjectParam
    private BecaRecuentoService becaRecuentoService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(
            @QueryParam("cursoAcademicoId") String cursoAcademicoId,
            @QueryParam("organismoId") String organismoId,
            @QueryParam("convocatoriaId") String convocatoriaId)
    {

        ParamUtils.checkNotNull(cursoAcademicoId, organismoId, convocatoriaId);

        List<BecaRecuento> listaBecaRecuento = becaRecuentoService
                .getRecuentoByCursoAcademicoAndyOrganismoAndConvocatoria(cursoAcademicoId,
                        organismoId, convocatoriaId);

        List<UIEntity> lista = convierteBecaRecuentoaListadoMatriz(listaBecaRecuento);
        return lista;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganismoConvocatoriaByCursoAcademico(
            @PathParam("id") Long cursoAcademicoId)
    {

        List<Tuple> listaOrganismoConvocatoria = becaRecuentoService
                .getDistinctOrganismoConvocatoriaByCursoAcademico(cursoAcademicoId);

        List<UIEntity> lista = convierteBecaRecuentoaOrganismoConvocatoriaUI(listaOrganismoConvocatoria);
        return lista;
    }

    private List<UIEntity> convierteBecaRecuentoaOrganismoConvocatoriaUI(
            List<Tuple> listaOrganismoConvocatoria)
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (Tuple organismoConvocatoria : listaOrganismoConvocatoria)
        {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("OrganismoConvocatoria");

            entity.put("organismo", organismoConvocatoria.get(QBecaRecuento.becaRecuento.organismo));
            entity.put("organismoId",
                    organismoConvocatoria.get(QBecaRecuento.becaRecuento.organismoId));
            entity.put("convocatoria",
                    organismoConvocatoria.get(QBecaRecuento.becaRecuento.convocatoria));
            entity.put("convocatoriaId",
                    organismoConvocatoria.get(QBecaRecuento.becaRecuento.convocatoriaId));
            lista.add(entity);
        }

        return lista;
    }

    private List<UIEntity> convierteBecaRecuentoaListadoMatriz(List<BecaRecuento> listaBecaRecuento)
    {
        Map<String, UIEntity> mapa = new HashMap<String, UIEntity>();

        UIEntity entity;

        for (BecaRecuento becaRecuento : listaBecaRecuento)
        {
            if (becaRecuento.getCuantos() == 0)
            {
                continue;
            }

            String key = MessageFormat.format("{0}{1}",
                    becaRecuento.getProcesoId(),
                    becaRecuento.getBecaConcedida());


            String estado = becaRecuento.getEstado();
            if (estado.equals("Revocacions voluntaries")) {
                estado = "RevocVoluntaries";
            } else if (estado.equals("Revocacions crédits")) {
                estado = "RevocCredits";
            }

            if (!mapa.containsKey(key))
            {
                entity = new UIEntity();
                entity.setBaseClass("BecaRecuento");
                entity.put("cursoAcademicoId", becaRecuento.getCursoAcademicoId());
                entity.put("organismoId", becaRecuento.getOrganismoId());
                entity.put("organismo", becaRecuento.getOrganismo());
                entity.put("convocatoriaId", becaRecuento.getConvocatoriaId());
                entity.put("convocatoria", becaRecuento.getConvocatoria());
                entity.put("procesoId", becaRecuento.getProcesoId());
                entity.put("proceso", becaRecuento.getProceso());
                entity.put("becaConcedida", becaRecuento.getBecaConcedida());
                entity.put(estado, becaRecuento.getCuantos());
                mapa.put(key, entity);
            }
            else
            {
                entity = mapa.get(key);
                entity.put(estado, becaRecuento.getCuantos());
            }
        }
        return new ArrayList<UIEntity>(mapa.values());
    }
}
