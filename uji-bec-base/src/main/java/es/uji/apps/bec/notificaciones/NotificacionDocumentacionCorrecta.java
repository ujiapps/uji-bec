package es.uji.apps.bec.notificaciones;


import es.uji.apps.bec.model.Persona;
import es.uji.apps.bec.model.Solicitante;

public class NotificacionDocumentacionCorrecta extends NotificacionBase
{
    @Override
    public void prepara()
    {
        String cuerpoMensaje = preparaCuerpoMensaje();
        Solicitante solicitante = this.getBeca().getSolicitante();
        Persona persona = Persona.getPersonaByPersonaId(solicitante.getPersona().getId());
        String correoUji = persona.getCorreoUJI();

        setTexto(cuerpoMensaje);
        setAsunto("[e-ujier@becas] Beca MEC documentació correcta. Localitzador temporal MEC: " + this.getBeca().getCodigoArchivoTemporal());
        setDestinatarioCorreoUJI(correoUji);
        setDestinatarioCorreoMEC(solicitante.getEmail());
    }

    private String preparaCuerpoMensaje() {
        String cuerpoCabecera = preparaCabecera(this.getBeca());
        String cuerpoTextoGenerico = preparaTextoGenerico();
        String cuerpoTextoAdicional = preparaTextoAdicional(this.getTextoAdicional());
        String cuerpoPie = preparaPie();

        return cuerpoCabecera + cuerpoTextoGenerico + cuerpoTextoAdicional + cuerpoPie;
    }

    private String preparaTextoGenerico() {
        return "\n" +
               "La documentació enviada és correcta.\n";
    }

    private String preparaTextoAdicional(String observaciones) {
        String cuerpoObservaciones = null;
        if (observaciones != null)
        {
            cuerpoObservaciones =  "\n\n" +
                    observaciones;
        }
        return cuerpoObservaciones;
    }
}
