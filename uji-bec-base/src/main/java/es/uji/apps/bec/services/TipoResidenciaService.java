package es.uji.apps.bec.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.bec.dao.dao.TipoResidenciaDAO;
import es.uji.apps.bec.model.TipoResidencia;
import es.uji.commons.rest.UIEntity;

@Service
public class TipoResidenciaService
{
    private TipoResidenciaDAO tipoResidenciaDAO;

    @Autowired
    public TipoResidenciaService(TipoResidenciaDAO tipoResidenciaDAO)
    {
        this.tipoResidenciaDAO = tipoResidenciaDAO;
    }

    public List<UIEntity> getTiposResidencias()
    {
        List<TipoResidencia> tiposResidencias = tipoResidenciaDAO.getTiposResidencias();
        return UIEntity.toUI(tiposResidencias);
    }
}
