package es.uji.apps.bec.services;


import es.uji.apps.bec.model.domains.TipoTitulacion;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


@Service
public class TipoTitulacionService
{
    public List<UIEntity> getTiposTitulacion()
    {

        List<UIEntity> uiEntities = new ArrayList<>();

        Stream.of(TipoTitulacion.values()).forEach(v -> {
            UIEntity ui = new UIEntity();
            ui.put("id", v.getCodigo());
            ui.put("nombre", v.getDescripcion());
            uiEntities.add(ui);
        });
        return uiEntities;
    }

}
