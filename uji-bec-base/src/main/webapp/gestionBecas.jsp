<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Gestion Becas</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
	
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationPanel/0.0.1/ApplicationPanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationViewport/0.0.1/ApplicationViewport.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>

<script type="text/javascript" src="js/gestionBecaPanel.js"></script>
<script type="text/javascript" src="js/datosGeneralesTab.js"></script>
<script type="text/javascript" src="js/academicosTab.js"></script>
<script type="text/javascript" src="js/estudiosTab.js"></script>
<script type="text/javascript" src="js/domiciliosTab.js"></script>
<script type="text/javascript" src="js/miembrosTab.js"></script>
<script type="text/javascript" src="js/otrosDatosTab.js"></script>
<script type="text/javascript" src="js/cuantiasTab.js"></script>
<script type="text/javascript" src="js/denegacionesTab.js"></script>
<script type="text/javascript" src="js/historicoTab.js"></script>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var becaId = "<%=request.getParameter("becaId")%>";
        var solicitanteId = "<%=request.getParameter("solicitanteId")%>";

        var gestionBecaPanel = new UJI.BEC.GestionBecaPanel(
        {
            becaId : becaId,
            solicitanteId : solicitanteId
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ gestionBecaPanel ]
        });
    });
</script>
</head>

<body>
</body>
</html>