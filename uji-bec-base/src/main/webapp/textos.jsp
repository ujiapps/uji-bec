<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Becas</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/RowEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationPanel/0.0.1/ApplicationPanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>

<script type="text/javascript" src="js/textos.js"></script>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelTextos = new UJI.BEC.Textos({});

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelTextos ]
        });
    });
</script>
</head>

<body>
</body>
</html>