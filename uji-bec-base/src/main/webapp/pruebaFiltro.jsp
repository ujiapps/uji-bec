<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Becas</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/RowEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>


<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var altaButton = new Ext.Button(
        {
            text : 'Alta/Recerca',
            handler : function(button, event)
            {
                panelFiltro.getForm().submit(
                {
                    method : 'POST',
                    url : '/bec/rest/beca/search',
                    success : function(form, action)
                    {

                    }
                });
            }
        });

        var store = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/bec/rest/beca'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'Asignatura',
                id : 'id'
            }, [ 'id', 'cursoAcademico', 'convocatoriaId', 'procesoId', 'estadoId', 'numeroBecaUji',
                    'codigoArchivoTemporal', 'tandaId', 'sinTanda','reclamada', 'personaId', 'identificacion', 'nombreApellidos', 'estudioId' ]),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Beca',
                id : 'id',
                writeAllFields : true
            })
        });

        var panelFiltro = new Ext.form.FormPanel(
        {
            reader : new Ext.data.XmlReader({}),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            buttons : [ altaButton ],
            items : [
            {
                xtype : 'textfield',
                fieldLabel : 'Curso',
                name : 'cursoAca'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Convocatoria',
                name : 'convocatoriaId'
            },            {
                xtype : 'textfield',
                fieldLabel : 'Proceso',
                name : 'procesoId'
            },            {
                xtype : 'textfield',
                fieldLabel : 'Estado',
                name : 'estadoId'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Beca UJI',
                name : 'numeroBecaUji'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Archivo Temporal',
                name : 'codigoArchivoTemporal'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Tanda',
                name : 'tandaId'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Sin Tanda',
                name : 'sinTanda'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'reclamada',
                name : 'reclamada'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Persona',
                name : 'personaId'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Identificacion',
                name : 'identificacion'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Apellidos',
                name : 'nombreApellidos'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Estudio',
                name : 'estudioId'
            } ]
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelFiltro ]
        });
    });
</script>
</head>

<body>
</body>
</html>