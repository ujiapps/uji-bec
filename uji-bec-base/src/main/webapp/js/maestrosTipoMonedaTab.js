Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoMoneda = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Monedas',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosTipoMoneda.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreTipoMoneda();

        this.buildEditor();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildGridTipoMoneda();

        this.add(this.gridTipoMoneda);
    },

    loadData: function() {
        this.storeTipoMoneda.reload();
    },


    buildStoreTipoMoneda : function()
    {
        this.storeTipoMoneda = this.getStore(
            {
                url : '/bec/rest/tipomoneda',
                record : 'TipoMoneda',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre', 'codigo' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var tipoMonedaSeleccionado = ref.gridTipoMoneda.getSelectionModel().getSelected();
                        if (!tipoMonedaSeleccionado)
                        {
                            return false;
                        }
                        ref.storeTipoMoneda.remove(tipoMonedaSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroTipoMoneda = new ref.storeTipoMoneda.recordType({});
                ref.editor.stopEditing();
                ref.storeTipoMoneda.insert(0, registroTipoMoneda);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridTipoMoneda;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridTipoMoneda : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Codi',
                width : 15,
                hidden : false,
                dataIndex : 'codigo',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Nom',
                width : 55,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            } ]
        });

        this.gridTipoMoneda = new Ext.grid.GridPanel(
        {
            store : this.storeTipoMoneda,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});