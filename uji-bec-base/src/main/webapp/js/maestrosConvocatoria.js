Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosConvocatoria = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Convocatorias',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosConvocatoria.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreConvocatoria();
        this.buildStoreOrganismo();

        this.buildCheckBoxActiva();
        this.buildComboOrganismo();

        this.buildEditor();

        this.buildAddButton();
        this.buildDeleteButton();

        this.buildGridConvocatoria();

        this.add(this.gridConvocatoria);
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
            {
                url : '/bec/rest/convocatoria',
                record : 'Convocatoria',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre', 'acronimo', 'organismoId', 'activa' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    buildStoreOrganismo : function()
    {
        this.storeOrganismo = this.getStore(
            {
                url : '/bec/rest/organismo',
                record : 'Organismo',
                id : 'id',
                fieldList : [ 'id', 'nombre' ]
            });
    },

    buildCheckBoxActiva : function()
    {
        this.checkBoxActiva = new Ext.form.Checkbox(
            {
                name : 'activa',
                width : 80,
                fieldLabel : 'Activa'
            });
    },

    buildComboOrganismo : function()
    {
        this.comboOrganismo = new Ext.form.ComboBox(
            {
                triggerAction : 'all',
                editable : false,
                forceSelection : true,
                width : 80,
                name : 'organismoId',
                valueField : 'id',
                displayField : 'nombre',
                store : this.storeOrganismo
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var ConvocatoriaSeleccionado = ref.gridConvocatoria.getSelectionModel().getSelected();
                        if (!ConvocatoriaSeleccionado)
                        {
                            return false;
                        }
                        ref.storeConvocatoria.remove(ConvocatoriaSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroConvocatoria = new ref.storeConvocatoria.recordType({});
                ref.editor.stopEditing();
                ref.storeConvocatoria.insert(0, registroConvocatoria);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridConvocatoria;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    loadData: function() {
        this.storeConvocatoria.reload();
        this.storeOrganismo.reload();
    },

    buildGridConvocatoria : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Acronim',
                width : 55,
                hidden : false,
                dataIndex : 'acronimo',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Organisme',
                width : 80,
                dataIndex : 'organismoId',
                editor : ref.comboOrganismo,
                renderer : ref.formatComboRenderer(ref.comboOrganismo)
            },
            {
                header : 'Convocatoria',
                width : 80,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Activa',
                dataIndex : 'activa',
                xtype : 'booleancolumn',
                trueText : 'Sí',
                falseText : 'No',
                width : 40,
                editor : ref.checkBoxActiva
            } ]
        });

        this.gridConvocatoria = new Ext.grid.GridPanel(
        {
            store : this.storeConvocatoria,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});