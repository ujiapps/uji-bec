Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosCuantia = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Cuantias',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosCuantia.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreCuantia();
        this.buildStoreConvocatoria();

        this.buildCheckBoxActiva();
        this.buildComboConvocatoria();

        this.buildEditor();

        this.buildAddButton();
        this.buildDeleteButton();

        this.buildGridCuantia();

        this.add(this.gridCuantia);
    },

    buildStoreCuantia : function()
    {
        this.storeCuantia = this.getStore(
            {
                url : '/bec/rest/cuantia',
                record : 'Cuantia',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre', 'codigo', 'convocatoriaId', 'activa' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    loadData: function() {
        this.storeConvocatoria.reload();
        this.storeCuantia.reload();
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
            {
                url : '/bec/rest/convocatoria',
                record : 'Convocatoria',
                id : 'id',
                fieldList : [ 'id', 'nombre' ]
            });
    },

    buildCheckBoxActiva : function()
    {
        this.checkBoxActiva = new Ext.form.Checkbox(
            {
                name : 'activa',
                width : 80,
                fieldLabel : 'Activa'
            });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
            {
                triggerAction : 'all',
                editable : false,
                forceSelection : true,
                width : 80,
                name : 'convocatoriaId',
                valueField : 'id',
                displayField : 'nombre',
                store : this.storeConvocatoria
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var CuantiaSeleccionado = ref.gridCuantia.getSelectionModel().getSelected();
                        if (!CuantiaSeleccionado)
                        {
                            return false;
                        }
                        ref.storeCuantia.remove(CuantiaSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroCuantia = new ref.storeCuantia.recordType({});
                ref.editor.stopEditing();
                ref.storeCuantia.insert(0, registroCuantia);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridCuantia;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridCuantia : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Convocatoria',
                width : 40,
                dataIndex : 'convocatoriaId',
                editor : ref.comboConvocatoria,
                renderer : ref.formatComboRenderer(ref.comboConvocatoria)
            },
            {
                header : 'Codi',
                width : 35,
                hidden : false,
                dataIndex : 'codigo',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Cuantia',
                width : 120,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Activa',
                dataIndex : 'activa',
                xtype : 'booleancolumn',
                trueText : 'Sí',
                falseText : 'No',
                width : 40,
                editor : ref.checkBoxActiva
            } ]
        });

        this.gridCuantia = new Ext.grid.GridPanel(
        {
            store : this.storeCuantia,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});