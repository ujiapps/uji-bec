Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoMiembro = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Membres',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosTipoMiembro.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreTipoMiembro();

        this.buildEditor();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildGridTipoMiembro();

        this.add(this.gridTipoMiembro);
    },

    loadData: function() {
        this.storeTipoMiembro.reload();
    },

    buildStoreTipoMiembro : function()
    {
        this.storeTipoMiembro = this.getStore(
            {
                url : '/bec/rest/tipomiembro',
                record : 'TipoMiembro',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre', 'orden', 'solicitante' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var tipoMiembroSeleccionado = ref.gridTipoMiembro.getSelectionModel().getSelected();
                        if (!tipoMiembroSeleccionado)
                        {
                            return false;
                        }
                        ref.storeTipoMiembro.remove(tipoMiembroSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroTipoMiembro = new ref.storeTipoMiembro.recordType({});
                ref.editor.stopEditing();
                ref.storeTipoMiembro.insert(0, registroTipoMiembro);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridTipoMiembro;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridTipoMiembro : function()
    {
        var ref = this;


        var checkBoxSolicitante = new Ext.form.Checkbox(
        {
            name : 'solicitante',
            width : 100,
            fieldLabel : 'Sol·licitant'
        });

        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Nom',
                width : 55,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Ordre',
                width : 15,
                hidden : false,
                dataIndex : 'orden',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Sol·licitant',
                dataIndex : 'solicitante',
                xtype : 'booleancolumn',
                trueText : 'Sí',
                falseText : 'No',
                width : 25,
                editor : checkBoxSolicitante
            } ]
        });

        this.gridTipoMiembro = new Ext.grid.GridPanel(
        {
            store : this.storeTipoMiembro,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});