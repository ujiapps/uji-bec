Ext.ns('UJI.BEC');

UJI.BEC.DomiciliosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Domicilis',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,
    domicilioId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.DomiciliosTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreDomicilios();
        this.buildStoreTipoDomicilio();
        this.buildStoreTipoVia();
        this.buildStoreProvincia();
        this.buildStoreTipoResidencia();
        this.buildStoreLocalidad();
        this.buildStorePais();
        this.buildStoreTipoIdentificacion();
        this.buildStoreArrendatarios();
        this.buildComboProvincia();
        this.buildComboLocalidad();
        this.buildComboPais();
        this.buildComboTipoDomicilio();
        this.buildComboTipoVia();
        this.buildComboTipoResidencia();
        this.buildComboArrendadorTipoIdentificacion();
        this.buildComboArrendatarioTipoIdentificacion();

        this.buildTextFieldId();
        this.buildTextFieldOtraClaseDomicilio();
        this.buildTextFieldNombreVia();
        this.buildTextFieldNumero();
        this.buildTextFieldEscalera();
        this.buildTextFieldPiso();
        this.buildTextFieldPuerta();
        this.buildTextFieldCodigoPostal();
        this.buildTextFieldSolicitanteId();
        this.buildTextFieldBecaId();
        this.buildTextFieldArrendadorNombre();
        this.buildTextFieldArrendadorIdentificacion();
        this.buildTextFieldArrendatarioNombre();
        this.buildTextFieldArrendatarioIdentificacion();
        this.buildTextFieldReferenciaCatastral();
        this.buildTextFieldDepartamento();

        this.buildNumberFieldImporteAlquiler();
        this.buildNumberFieldImporteFianza();
        this.buildNumberFieldNumeroArrendatarios();

        this.buildDateFieldFechaInicioContrato();
        this.buildDateFieldFechaFinContrato();
        this.buildDateFieldFechaAdquisicionVivienda();

        this.buildCompositeFieldArrendadorIdentificacion();
        this.buildCompositeFieldFechasContrato();

        this.buildCheckBoxResidenciaEsp();
        this.buildCheckBoxGratuito();
        this.buildCheckBoxCorrespondencia();

        this.buildButtonGuardar();
        this.buildButtonInsert();
        this.buildButtonDelete();
        this.buildButtonInsertArrendatario();
        this.buildButtonDeleteArrendatario();

        this.buildEditorArrendatario();

        this.buildPanelGridDomicilios();
        this.buildPanelForm();
        this.buildPanelGridArrendatarios();

        this.add(this.panelGridDomicilios);
        this.add(this.panelForm);
        this.add(this.panelGridArrendatarios);
    },

    loadStore: function() {
        syncStoreLoad([ this.storeTipoDomicilio, this.storeTipoVia, this.storeProvincia, this.storeTipoResidencia, this.storeDomicilios, this.storeTipoIdentificacion, this.storePais, this.storeArrendatarios ]);
    },

    buildStoreDomicilios : function()
    {
        this.storeDomicilios = new Ext.data.Store(
        {
            baseParams :
            {
                becaId : this.becaId
            },
            restful : true,
            url : '/bec/rest/domicilio',
            reader : new Ext.data.XmlReader(
            {
                record : 'Domicilio',
                id : 'id'
            }, [ 'id', 'tipoDomicilio', 'otraClaseDomicilio', 'nombreVia', 'adreca', 'tipoDomicilioId', 'tipoViaId', 'nombreVia', 'numero', 'escalera', 'piso', 'puerta', 'localidadId',
                    'localidadNombre', 'paisId', 'paisNombre', 'codigoPostal', 'provinciaId', 'residenciaEsp', 'gratuito', 'correspondencia', 'becaId', 'arrendadorNombre', 'departamento', 'arrendadorIdentificacion',
                    'arrendadorTipoIdentificacionId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                datachanged : function()
                {
                    var cuenta = {};
                    for ( var i = 0; i < this.data.length; i++)
                    {
                        var item = this.data.items[i];
                        var cantidad = cuenta[item.data.tipoDomicilioId];
                        if (cantidad)
                        {
                            Ext.MessageBox.show(
                            {
                                title : 'Atenció!',
                                msg : 'Hi ha almenys dos domicilis amb el mateix tipus.',
                                buttons : Ext.MessageBox.OK,
                                animEl : 'samplebutton',
                                icon : Ext.MessageBox.WARNING
                            });
                            break;
                        }
                        cuenta[item.data.tipoDomicilioId] = 1;
                    }
                }
            }
        });
    },

    buildStoreTipoDomicilio : function()
    {
        this.storeTipoDomicilio = this.getStore(
        {
            url : '/bec/rest/tipodomicilio/',
            record : 'TipoDomicilio',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreTipoVia : function()
    {
        this.storeTipoVia = this.getStore(
        {
            url : '/bec/rest/tipovia/',
            record : 'TipoVia',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreProvincia : function()
    {
        this.storeProvincia = this.getStore(
        {
            url : '/bec/rest/provincia/',
            record : 'Provincia',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreTipoResidencia : function()
    {
        this.storeTipoResidencia = this.getStore(
        {
            url : '/bec/rest/tiporesidencia/',
            record : 'TipoResidencia',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreLocalidad : function()
    {
        this.storeLocalidad = this.getStore(
        {
            url : '/bec/rest/localidad/provincia/0',
            record : 'Localidad',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStorePais : function()
    {
        this.storePais = this.getStore(
            {
                url : '/bec/rest/pais/',
                record : 'Pais',
                fieldList : [ 'id', 'nombre' ]
            });
    },
    
    buildStoreTipoIdentificacion : function()
    {
        this.storeTipoIdentificacion = this.getStore(
        {
            url : '/bec/rest/tipoidentificacion/',
            record : 'TipoIdentificacion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreArrendatarios : function()
    {
        var ref = this;
        this.storeArrendatarios = this.getStore(
        {
            url : '/bec/rest/domicilio/' + ref.domicilioId + '/arrendatarios',
            record : 'Arrendatario',
            id : 'id',
            fieldList : [ 'id', 'nombre', 'identificacion', 'tipoIdentificacionId' ]
        });
    },

    deshabilitarGridArrendatarios : function()
    {
        this.domicilioId = '';
        this.buttonInsertArrendatario.setDisabled(true);
        this.buttonDeleteArrendatario.setDisabled(true);
        this.storeArrendatarios.reload();
    },

    cargaDomicilio : function(selectedId)
    {
        var miForm = this.panelForm.getForm();
        miForm.load(
        {
            url : '/bec/rest/domicilio/' + selectedId,
            method : 'get',
            waitMsg : 'Per favor, espereu...',
            waitTitle : 'Carregant domicili',
            success : function(e)
            {
            },
            failure : function(form, action)
            {
                Ext.Msg.alert('Error', 'Error al carregar les dades.');
            }
        });
    },

    buildTextFieldId : function()
    {
        this.textFieldId = new Ext.form.TextField(
        {
            hidden : true,
            name : 'id'
        });
    },

    buildTextFieldOtraClaseDomicilio : function()
    {
        this.textFieldOtraClaseDomicilio = new Ext.form.TextField(
        {
            width : 150,
            fieldLabel : 'Altre tipus Domicili',
            name : 'otraClaseDomicilio'
        });
    },

    buildTextFieldDepartamento : function()
    {
        this.textFieldDepartamento = new Ext.form.TextField(
        {
            width : 120,
            fieldLabel : 'Departament',
            name : 'departamento'
        });
    },

    buildComboTipoVia : function()
    {
        this.comboTipoVia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus via',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 200,
            name : 'tipoViaId',
            hiddenName : 'tipoViaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoVia
        });
    },

    buildTextFieldNombreVia : function()
    {
        this.textFieldNombreVia = new Ext.form.TextField(
        {
            width : 300,
            fieldLabel : 'Carrer',
            name : 'nombreVia'
        });
    },

    buildTextFieldNumero : function()
    {
        this.textFieldNumero = new Ext.form.TextField(
        {
            width : 60,
            fieldLabel : 'Número',
            name : 'numero'
        });
    },

    buildTextFieldEscalera : function()
    {
        this.textFieldEscalera = new Ext.form.TextField(
        {
            width : 50,
            fieldLabel : 'Escala',
            name : 'escalera'
        });
    },

    buildTextFieldPiso : function()
    {
        this.textFieldPiso = new Ext.form.TextField(
        {
            width : 50,
            fieldLabel : 'Piso',
            name : 'piso'
        });
    },

    buildTextFieldPuerta : function()
    {
        this.textFieldPuerta = new Ext.form.TextField(
        {
            width : 50,
            fieldLabel : 'Porta',
            name : 'puerta'
        });
    },

    buildTextFieldCodigoPostal : function()
    {
        this.textFieldCodigoPostal = new Ext.form.TextField(
        {
            width : 60,
            fieldLabel : 'Codi Postal',
            name : 'codigoPostal'
        });
    },

    buildTextFieldSolicitanteId : function()
    {
        this.textFieldSolicitanteId = new Ext.form.TextField(
        {
            hidden : true,
            value : this.solicitanteId,
            name : 'solicitanteId'
        });
    },

    buildTextFieldBecaId : function()
    {
        this.textFieldBecaId = new Ext.form.TextField(
            {
                hidden : true,
                value : this.becaId,
                name : 'becaId'
            });
    },

    buildTextFieldArrendadorNombre : function()
    {
        this.textFieldArrendadorNombre = new Ext.form.TextField(
        {
            width : 300,
            fieldLabel : 'Nom arrendador',
            name : 'arrendadorNombre'
        });
    },

    buildTextFieldArrendadorIdentificacion : function()
    {
        this.textFieldArrendadorIdentificacion = new Ext.form.TextField(
        {
            width : 130,
            fieldLabel : 'Identificació arrendador',
            name : 'arrendadorIdentificacion'
        });
    },

    buildTextFieldArrendatarioNombre : function()
    {
        this.textFieldArrendatarioNombre = new Ext.form.TextField(
        {
            name : 'arrendatarioNombre',
            allowBlank : false

        });
    },

    buildTextFieldArrendatarioIdentificacion : function()
    {
        this.textFieldArrendatarioIdentificacion = new Ext.form.TextField(
        {
            name : 'arrendatarioIdentificacion',
            allowBlank : false

        });
    },

    buildTextFieldReferenciaCatastral : function()
    {
        this.textFieldReferenciaCatastral = new Ext.form.TextField(
            {
                name : 'referenciaCatastral',
                width : 300,
                fieldLabel : 'Referència catastral'
            });
    },

    buildComboProvincia : function()
    {
        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);

                return record ? record.get(combo.displayField) : combo.valueNotFoundText;

            };
        };

        var ref = this;

        this.comboProvincia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Provincia',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 200,
            name : 'provinciaId',
            hiddenName : 'provinciaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeProvincia,
            listeners :
            {
                change : function(store)
                {
                    ref.comboLocalidad.reset();
                    ref.storeLocalidad.proxy.setUrl('/bec/rest/localidad/provincia/' + this.getValue());
                    ref.storeLocalidad.load();
                }
            }
        });
    },

    buildComboLocalidad : function()
    {
        this.comboLocalidad = new Ext.form.ComboBox(
        {
            fieldLabel : 'Localitat',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 300,
            name : 'localidadId',
            hiddenName : 'localidadId',
            valueField : 'id',
            displayField : 'nombre',
            mode : 'local',
            store : this.storeLocalidad
        });
    },

    buildComboPais : function()
    {
        this.comboPais = new Ext.form.ComboBox(
            {
                fieldLabel : 'Pais',
                triggerAction : 'all',
                editable : true,
                forceSelection : true,
                width : 200,
                name : 'paisId',
                hiddenName : 'paisId',
                valueField : 'id',
                displayField : 'nombre',
                mode : 'local',
                store : this.storePais
            });
    },

    buildComboTipoDomicilio : function()
    {
        this.comboTipoDomicilio = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus domicili',
            triggerAction : 'all',
            editable : true,
            allowBlank : false,
            forceSelection : true,
            width : 200,
            name : 'tipoDomicilioId',
            hiddenName : 'tipoDomicilioId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoDomicilio
        });
    },

    buildComboTipoResidencia : function()
    {
        this.comboTipoResidencia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus residència',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 200,
            name : 'tipoResidenciaId',
            hiddenName : 'tipoResidenciaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoResidencia
        });
    },

    buildComboArrendadorTipoIdentificacion : function()
    {
        this.comboArrendadorTipoIdentificacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 65,
            name : 'arrendadorTipoIdentificacionId',
            hiddenName : 'arrendadorTipoIdentificacionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoIdentificacion
        });
    },

    buildComboArrendatarioTipoIdentificacion : function()
    {
        this.comboArrendatarioTipoIdentificacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 50,
            allowBlank : false,
            name : 'tipoIdentificacionId',
            hiddenName : 'tipoIdentificacionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoIdentificacion
        });
    },

    buildNumberFieldImporteAlquiler : function()
    {
        this.numberFieldImporteAlquiler = new Ext.form.NumberField(
        {
            fieldLabel : 'Import lloguer',
            width : 60,
            name : 'importeAlquiler',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldImporteFianza : function()
    {
        this.numberFieldImporteFianza = new Ext.form.NumberField(
        {
            fieldLabel : 'Import fiança',
            width : 60,
            name : 'importeFianza',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldNumeroArrendatarios : function()
    {
        this.numberFieldNumeroArrendatarios = new Ext.form.NumberField(
        {
            fieldLabel : 'Nombre arrendataris',
            width : 60,
            name : 'numeroArrendatarios',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildDateFieldFechaInicioContrato : function()
    {
        this.dateFieldFechaInicioContrato = new Ext.form.DateField(
        {
            fieldLabel : 'Dates inici-fi contracte',
            name : 'fechaInicioContrato',
            width : 146,
            startDay : 1,
            format : 'd/m/Y'
        });
    },

    buildDateFieldFechaFinContrato : function()
    {
        this.dateFieldFechaFinContrato = new Ext.form.DateField(
        {
            name : 'fechaFinContrato',
            width : 146,
            startDay : 1,
            format : 'd/m/Y'
        });
    },

    buildDateFieldFechaAdquisicionVivienda : function()
    {
        this.dateFieldFechaAdquisicionVivienda = new Ext.form.DateField(
            {
                name : 'fechaAdquisicionVivienda',
                width : 146,
                startDay : 1,
                fieldLabel : 'Adquisició habitatge',
                format : 'd/m/Y'

            });
    },

    buildCompositeFieldArrendadorIdentificacion : function()
    {
        this.compositeFieldArrendadorIdentificacion = new Ext.form.CompositeField(
        {
            items : [ this.comboArrendadorTipoIdentificacion, this.textFieldArrendadorIdentificacion ]
        });
    },

    buildCompositeFieldFechasContrato : function()
    {
        this.compositeFieldFechasContrato = new Ext.form.CompositeField(
        {
            items : [ this.dateFieldFechaInicioContrato, this.dateFieldFechaFinContrato ]
        });
    },

    buildCheckBoxResidenciaEsp : function()
    {
        this.checkBoxResidenciaEsp = new Ext.form.Checkbox(
        {
            name : 'residenciaEsp',
            labelAlign : 'left',
            width : 200,
            value : 'S',
            fieldLabel : 'Residència espanyola'
        });
    },

    buildCheckBoxGratuito : function()
    {
        this.checkBoxGratuito = new Ext.form.Checkbox(
        {
            name : 'gratuito',
            labelAlign : 'left',
            width : 200,
            value : 'S',
            fieldLabel : 'Gratuït'
        });
    },

    buildCheckBoxCorrespondencia : function()
    {
        this.checkBoxCorrespondencia = new Ext.form.Checkbox(
        {
            name : 'correspondencia',
            labelAlign : 'left',
            width : 200,
            value : 'S',
            fieldLabel : 'Correspondència'
        });
    },

    buildButtonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                var id = ref.panelForm.getForm().getValues().id;

                if (id != '' && id != null)
                {
                    ref.panelForm.getForm().submit(
                    {
                        url : '/bec/rest/domicilio/' + id,
                        method : 'put',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.storeDomicilios.reload();
                            ref.buttonInsertArrendatario.setDisabled(false);
                            ref.buttonDeleteArrendatario.setDisabled(false);
                        },
                        failure : function(form, action)
                        {
                        }
                    });

                }
                else
                {
                    ref.panelForm.getForm().submit(
                    {
                        url : '/bec/rest/domicilio/',
                        method : 'post',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            var domicilio = Ext.DomQuery.selectNode('Domicilio', action.response.responseXML);
                            ref.domicilioId = Ext.DomQuery.selectValue('id', domicilio);
                            ref.storeDomicilios.reload();

                            ref.cargaDomicilio(ref.domicilioId);
                            ref.buttonInsertArrendatario.setDisabled(false);
                            ref.buttonDeleteArrendatario.setDisabled(false);
                            ref.storeArrendatarios.proxy.conn.url = '/bec/rest/domicilio/' + ref.domicilioId + '/arrendatarios';
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
            }
        });
    },

    buildButtonInsert : function()
    {
        var ref = this;

        this.buttonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                ref.panelGridDomicilios.getSelectionModel().clearSelections();
                ref.panelForm.getForm().reset();
                ref.panelForm.focus();
                ref.deshabilitarGridArrendatarios();
            }
        });
    },

    buildButtonDelete : function()
    {
        var ref = this;

        this.buttonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.panelGridDomicilios.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                Ext.MessageBox.show(
                {
                    title : 'Esborrar domicili?',
                    msg : 'Atenció!\n ' + 'Vols esborrar el domicili seleccionat?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.panelGridDomicilios.store.remove(rec);
                            ref.panelForm.getForm().reset();
                            ref.deshabilitarGridArrendatarios();
                        }
                    }
                });
            }
        });
    },

    buildButtonInsertArrendatario : function()
    {
        var ref = this;

        this.buttonInsertArrendatario = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function(button, event)
            {
                var rec = new ref.panelGridArrendatarios.store.recordType(
                {
                    id : '',
                    nombre : '',
                    identificacion : '',
                    tipoIdentificacionId : ''
                });

                ref.editorArrendatario.stopEditing();
                ref.panelGridArrendatarios.store.insert(0, rec);
                ref.editorArrendatario.startEditing(0, true);
            }
        });
    },

    buildButtonDeleteArrendatario : function()
    {
        var ref = this;

        this.buttonDeleteArrendatario = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                var rec = ref.panelGridArrendatarios.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                Ext.MessageBox.show(
                {
                    title : 'Esborrar arrendatari?',
                    msg : 'Atenció!\n ' + 'Vols esborrar el arrendatari seleccionat?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.storeArrendatarios.proxy.conn.url = '/bec/rest/domicilio/' + ref.domicilioId + '/arrendatarios';
                            ref.panelGridArrendatarios.store.remove(rec);
                        }
                    }
                });
            }
        });
    },

    recargaFormularios : function()
    {
        this.cargaDomicilio(this.panelGridDomicilios.selModel.getSelected().id);
        this.domicilioId = this.panelGridDomicilios.selModel.getSelected().id;
        this.buttonDeleteArrendatario.setDisabled(false);
        this.buttonInsertArrendatario.setDisabled(false);
        this.storeArrendatarios.proxy.setUrl('/bec/rest/domicilio/' + this.domicilioId + '/arrendatarios');
        this.storeArrendatarios.load();
    },

    buildEditorArrendatario : function()
    {
        var ref = this;
        this.editorArrendatario = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    ref.storeArrendatarios.proxy.conn.url = '/bec/rest/domicilio/' + ref.domicilioId + '/arrendatarios';
                },
                afteredit : function(rowEditor, rowIndex)
                {
                },
                canceledit : function(rowEditor, rowIndex)
                {
                }
            }
        });
    },

    buildPanelGridDomicilios : function()
    {
        var ref = this;

        this.panelGridDomicilios = new Ext.grid.GridPanel(
        {
            store : ref.storeDomicilios,
            height : 120,
            flex : 1,
            frame : true,
            autoScroll : true,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,

            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Tipus Domicili',
                    width : 60,
                    dataIndex : 'tipoDomicilio'
                },
                {
                    header : 'Domicili',
                    width : 100,
                    dataIndex : 'adreca'
                },
                {
                    header : 'Provincia',
                    width : 60,
                    dataIndex : 'provinciaId',
                    renderer : Ext.util.Format.comboRenderer(ref.comboProvincia)
                },
                {
                    header : 'Localitat',
                    width : 60,
                    dataIndex : 'localidadNombre'
                } ]
            }),
            tbar :
            {
                items : [ this.buttonInsert, this.buttonDelete ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, event)
                {
                    var provinciaId = ref.panelGridDomicilios.selModel.getSelected().data.provinciaId;

                    if (provinciaId)
                    {
                        ref.storeLocalidad.proxy.setUrl('/bec/rest/localidad/provincia/' + provinciaId, true);
                        ref.storeLocalidad.load(
                        {
                            callback : function(r)
                            {
                                ref.recargaFormularios();
                            }
                        });
                    }
                    else
                    {
                        ref.recargaFormularios();
                    }
                }
            }
        });
    },

    buildPanelForm : function()
    {
        this.panelForm = new Ext.form.FormPanel(
        {
            layout : 'table',
            frame : true,
            flex : 1,
            labelAlign : 'top',
            labelWidth : '100',
            padding : '5px 0px',
            reader : new Ext.data.XmlReader(
            {
                record : 'Domicilio'
            }, [ 'id', 'tipoDomicilio', 'otraClaseDomicilio', 'nombreVia', 'adreca', 'tipoDomicilioId', 'tipoViaId', 'nombreVia', 'numero', 'escalera', 'piso', 'puerta', 'localidadId',
                    'localidadNombre', 'paisId', 'paisNombre', 'codigoPostal', 'provinciaId', 'provinciaNombre', 'tipoResidenciaId', 'solicitanteId', 'becaId', 'residenciaEsp', 'gratuito', 'correspondencia', 'importeAlquiler',
                    'arrendadorNombre', 'arrendadorIdentificacion', 'arrendadorTipoIdentificacionId', 'importeFianza', 'numeroArrendatarios', 'fechaInicioContrato', 'fechaFinContrato',
                    'referenciaCatastral', 'fechaAdquisicionVivienda' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'respuestaForm',
                success : 'success'
            }, [ 'success', 'msg' ]),
            defaults :
            {
                bodyStyle : 'padding:0px 5px'
            },
            layoutConfig :
            {
                columns : 6
            },
            items : [
            {
                layout : 'form',
                colspan : 6,
                items : [ this.textFieldId ]
            },
            {
                layout : 'form',
                items : [ this.comboTipoDomicilio ]
            },
            {
                layout : 'form',
                items : [ this.textFieldOtraClaseDomicilio ]
            },
            {
                layout : 'form',
                colspan : 4,
                items : [ this.textFieldDepartamento ]
            },
            {
                layout : 'form',
                items : [ this.comboTipoVia ]
            },
            {
                layout : 'form',
                items : [ this.textFieldNombreVia ]
            },
            {
                layout : 'form',
                items : [ this.textFieldNumero ]
            },
            {
                layout : 'form',
                items : [ this.textFieldEscalera ]
            },
            {
                layout : 'form',
                items : [ this.textFieldPiso ]
            },
            {
                layout : 'form',
                items : [ this.textFieldPuerta ]
            },
            {
                layout : 'form',
                items : [ this.comboProvincia ]
            },
            {
                layout : 'form',
                items : [ this.comboLocalidad ]
            },
            {
                layout : 'form',
                items : [ this.textFieldCodigoPostal ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.comboPais ]
            },
            {
                layout : 'form',
                items : [ this.comboTipoResidencia ]
            },
            {
                layout : 'form',
                items : [ this.compositeFieldFechasContrato ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.numberFieldImporteFianza ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.numberFieldImporteAlquiler ]
            },
            {
                layout : 'form',
                items : [ this.compositeFieldArrendadorIdentificacion ]
            },
            {
                layout : 'form',
                items : [ this.textFieldArrendadorNombre ]
            },
            {
                layout : 'form',
                colspan : 4,
                items : [ this.numberFieldNumeroArrendatarios ]
            },
            {
                layout : 'form',
                labelAlign : 'left',
                labelSeparator : ' ',
                labelWidth : 180,
                items : [ this.checkBoxResidenciaEsp ]
            },
            {
                layout : 'form',
                items : [ this.textFieldReferenciaCatastral ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.dateFieldFechaAdquisicionVivienda ]
            },
            {},
            {
                layout : 'form',
                colspan : 5,
                labelAlign : 'left',
                labelSeparator : ' ',
                labelWidth : 180,
                items : [ this.checkBoxGratuito ]
            }, {},
            {
                layout : 'form',
                colspan : 4,
                labelAlign : 'left',
                labelSeparator : ' ',
                labelWidth : 180,
                items : [ this.checkBoxCorrespondencia ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.buttonGuardar ]
            },
            {
                layout : 'form',
                colspan : 6,
                items : [ this.textFieldSolicitanteId, this.textFieldBecaId ]
            } ]
        });
    },

    buildPanelGridArrendatarios : function()
    {
        var ref = this;

        this.panelGridArrendatarios = new Ext.grid.GridPanel(
        {
            title : 'Arrendataris',
            store : ref.storeArrendatarios,
            height : 180,
            flex : 1,
            frame : true,
            autoScroll : true,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,

            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editorArrendatario ],
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Tipus',
                    width : 30,
                    dataIndex : 'tipoIdentificacionId',
                    editor : ref.comboArrendatarioTipoIdentificacion,
                    renderer : Ext.util.Format.comboRenderer(ref.comboArrendatarioTipoIdentificacion)
                },
                {
                    header : 'Identificació',
                    width : 100,
                    dataIndex : 'identificacion',
                    editor : ref.textFieldArrendatarioIdentificacion
                },
                {
                    header : 'Nom',
                    dataIndex : 'nombre',
                    editor : ref.textFieldArrendatarioNombre
                } ]
            }),
            tbar :
            {
                items : [ ref.buttonInsertArrendatario, ref.buttonDeleteArrendatario ]
            },
            listeners : {

            }
        });
    }
});