Ext.ns('UJI.BEC');

UJI.BEC.DenegacionesTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Denegacions',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    organismoId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.DenegacionesTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRowEditor();

        this.buildStoreDenegaciones();
        this.buildStoreBecasDenegaciones();
        this.buildComboDenegaciones();

        this.buildButtonAnadir();
        this.buildButtonBorrar();

        this.buildGridDenegaciones();
        this.add(this.gridDenegaciones);
    },

    loadStore: function() {
        syncStoreLoad([ this.storeDenegaciones, this.storeBecasDenegaciones ]);
    },

    buildRowEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            focusDelay : 0,
            floating :
            {
                zindex : 7000
            },
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    if (rowIndex == 0)
                    {
                        ref.gridDenegaciones.getSelectionModel().selectFirstRow();
                        if (ref.gridDenegaciones.getSelectionModel().getSelected().dirty)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
        });
    },

    buildStoreBecasDenegaciones : function()
    {
        this.storeBecasDenegaciones = this.getStore(
        {
            url : '/bec/rest/beca/' + this.becaId + '/denegacion',
            record : 'BecaDenegacion',
            id : 'id',
            fieldList : [ 'id', 'becaId', 'denegacionId', 'codigo', 'ordenDenegacion' ]
        });
    },

    buildStoreDenegaciones : function()
    {
        this.storeDenegaciones = this.getStore(
        {
            url : '/bec/rest/denegacion/activa/' + this.organismoId,
            record : 'Denegacion',
            id : 'id',
            fieldList : [ 'id', 'nombreListaValores' ]
        });
    },
    buildComboDenegaciones : function()
    {
        this.comboDenegaciones = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            allowBlank : false,
            typeAhead : true,
            minChars : 2,
            editable : true,
            queryMode : 'local',
            displayField : 'nombreListaValores',
            valueField : 'id',
            hiddenValue : 'id',
            store : this.storeDenegaciones
        });
    },
    buildButtonAnadir : function()
    {
        var ref = this;

        this.buttonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                var rec = new ref.gridDenegaciones.store.recordType(
                {
                    id : '',
                    denegacionId : '',
                    becaId : ref.becaId,
                    codigo : '',
                    ordenDenegacion : ''
                });

                ref.editor.stopEditing();
                ref.gridDenegaciones.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },
    buildButtonBorrar : function()
    {
        var ref = this;
        this.buttonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(button, event)
            {
                var rec = ref.gridDenegaciones.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                Ext.MessageBox.show(
                {
                    title : 'Esborrar denegació?',
                    msg : 'Atenció!\n ' + 'vols esborrar la denegació seleccionada?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.gridDenegaciones.store.remove(rec);
                        }
                    }
                });
            }
        });
    },
    buildGridDenegaciones : function()
    {
        var ref = this;
        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };
        this.gridDenegaciones = new Ext.grid.GridPanel(
        {
            store : this.storeBecasDenegaciones,
            frame : true,
            autoScroll : true,
            height : 400,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,
            plugins : [ this.editor ],
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Beca',
                    dataIndex : 'becaId',
                    hidden : true
                },
                {
                    header : 'Codi',
                    dataIndex : 'codigo',
                    width : 40,
                    hidden : true
                },
                {
                    header : 'Denegació',
                    dataIndex : 'denegacionId',
                    editor : ref.comboDenegaciones,
                    renderer : Ext.util.Format.comboRenderer(ref.comboDenegaciones)
                },
                {
                    header : 'Ordre',
                    dataIndex : 'ordenDenegacion',
                    width : 10,
                    editor : new Ext.form.NumberField(
                    {
                        allowBlank : false,
                        allowNegative : false,
                        blankText : 'El camp Ordre es requerit'
                    })
                } ]
            }),
            tbar :
            {
                items : [ this.buttonAnadir, this.buttonBorrar ]
            }
        });
    }
});