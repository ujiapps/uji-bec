Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosCursoAcademico = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Cursos Acadèmics',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,
    domicilioId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.DomiciliosTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildEditor();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildActivarCursoButton();
        this.buildStoreCursoAcademico();
        this.buildGridCursoAcademico();

        this.add(this.gridCursoAcademico);
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rec = ref.gridCursoAcademico.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        ref.storeCursoAcademico.remove(rec);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var u = new ref.storeCursoAcademico.recordType({});
                ref.editor.stopEditing();
                ref.storeCursoAcademico.insert(0, u);
                ref.editor.startEditing(0);
            }
        });
    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico',
            record : 'CursoAcademico',
            id : 'id',
            fieldList : [ { name : 'id', type : 'int'}, 'activo' ],
            listeners :
            {
                save : function(store, batch, data)
                {
                    if (!data.create) {
                        store.reload();
                    }
                }
            }
        });
    },

    loadData: function() {
        this.storeCursoAcademico.reload();
    },

    buildActivarCursoButton : function()
    {
        var ref = this;

        this.activarCursoButton = new Ext.Button(
        {
            text : "Activar curs",
            iconCls : 'accept',
            handler : function(btn, ev)
            {
                var sm = ref.gridCursoAcademico.getSelectionModel();
                var record = sm.getSelected();
                if (record.get("activo") !== "true")
                {
                    record.set("activo", "true");
                    //ref.storeCursoAcademico.save();
                    //ref.storeCursoAcademico.reload();
                }

            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                canceledit : function(rowEditor)
                {
                    // para que funcione, alguno de los campos del Store debe ser obligatorio
                    var record = rowEditor.record;

                    if ((record.phantom))
                    {
                        ref.storeCursoAcademico.remove(rowEditor.record);
                    }
                },

                beforeedit : function(editor, rowIndex)
                {
                    var administradorRecord = editor.grid.getStore().getAt(rowIndex);
                    if (!ref.allowEdit && !administradorRecord.phantom)
                    {
                        return false;
                    }
                }
            }
        });
    },

    buildGridCursoAcademico : function()
    {
        var ref = this;

        var userColumns = [
        {
            header : 'Any',
            width : 15,
            hidden : false,
            dataIndex : 'id',
            editable : true,
            sortable : true,
            editor : new Ext.form.NumberField(
            {
                allowBlank : false
            })
        },
        {
            header : 'Actiu',
            dataIndex : 'activo',
            width : 15,
            sortable : true,
            editable : false,
            renderer : function(value)
            {
                if (value === "true")
                {
                    return "Sí";
                }
                else
                {
                    return "No";
                }
            }
        } ];

        this.gridCursoAcademico = new Ext.grid.GridPanel(
        {
            frame : true,
            store : this.storeCursoAcademico,
            width : 500,
            height : 400,
            loadMask : true,
            autoScroll : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            columns : userColumns,
            tbar : [ this.addButton, "-", this.deleteButton, "->",  this.activarCursoButton ]
        });
    }

});