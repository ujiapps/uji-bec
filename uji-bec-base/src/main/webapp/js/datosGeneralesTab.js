Ext.ns('UJI.BEC');

UJI.BEC.DatosGeneralesTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Dades generals',
    layout : 'table',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.DatosGeneralesTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreEstudio();
        this.buildStoreConvocatoria();
        this.buildStoreProceso();
        this.buildStoreEstado();
        this.buildStoreTanda();
        this.buildStoreComunidad();
        this.buildStoreTipoFamilia();
        this.buildStoreTipoIdentificacion();
        this.buildStoreProfesion();
        this.buildStorePersonaEstudio();

        syncStoreLoad([ this.storeEstudio, this.storeTanda, this.storeConvocatoria, this.storeProceso, this.storeEstado, this.storeTipoFamilia, this.storeComunidad, this.storeTipoIdentificacion, this.storeProfesion ]);
        this.cargaDatosFinales(this.storeTipoFamilia);

        this.buildComboConvocatoria();
        this.buildComboProceso();
        this.buildComboEstado();
        this.buildComboTanda();
        this.buildComboTipoFamilia();
        this.buildComboTipoIdentificacion();
        this.buildComboProfesion();
        this.buildComboComunidad();

        this.buildTextFieldCarnetFamiliaNumerosa();

        this.buildInputFechaOrfandad();
        this.buildInputFechaVictimaVg();
        this.buildInputFechaHijoVictimaVg();
        this.buildInputFechaIniFamiliaNumerosa();
        this.buildInputFechaFinFamiliaNumerosa();
        this.buildTextFieldSolicitudId();
        this.buildTextFieldBecaId();
        this.buildTextFieldCursoAcademicoId();
        this.buildTextFieldNumeroBecaUji();
        this.buildTextFieldCodigoArchivoTemporal();
        this.buildTextFieldCodigoExpediente();
        this.buildTextFieldIdentificacion();
        this.buildTextFieldPersonaId();
        this.buildTextFieldFechaNotificacion();
        this.buildTextFieldTipoPropiedad();
        this.buildTextFieldOtraBeca();

        this.buildNumberFieldNumeroMiembrosComputables();
        this.buildNumberFieldNumeroHermanos();
        this.buildNumberFieldNumeroHermanosFuera();

        this.buildDisplayFieldTipoMinusvalia();

        this.buildNumberFieldNumeroMinusvalia33();
        this.buildNumberFieldNumeroMinusvalia65();
        this.buildNumberFieldIngresosNetos();
        this.buildNumberFieldIngresosExtranjero();
        this.buildNumberFieldImporteAlquiler();
        this.buildNumberFieldNumeroTransportes();

        this.buildCompositeFieldIdentificacion();

        this.buildCheckBoxOrfandad();
        this.buildCheckBoxIndVictimaVg();
        this.buildCheckBoxIndHijoVictimaVg();
        this.buildCheckBoxUnidadFamiliarIndependiente();
        this.buildCheckBoxTransporteUrbano();
        this.buildCheckBoxAutorizacionRenta();
        this.buildCheckBoxBecaConcedida();
        this.buildCheckBoxReclamada();
        this.buildInfoCircunstanciaBecario();
        this.buildCheckBoxViolenciaGenero();
        this.buildCheckBoxResidenciaPermanente();
        this.buildCheckBoxResidenciaTrabajo();
        this.buildButtonGuardar();

        this.buildPanelForm();

        this.add(this.panelForm);

        //this.addEvents('UJI.BEC.DatosGeneralesTab.buttonGuardar.click');
    },

    loadStore: function() {
        this.cargaFormulario();
    },

    buildInputFechaOrfandad : function()
    {
        this.inputFechaOrfandad = new Ext.form.DateField(
        {
            width : 140,
            format : 'd/m/Y',
            altFormats : 'd/m/Y H:i:s',
            fieldLabel : 'Data orfandat',
            name : 'fechaOrfandad'
        });
    },

    buildInputFechaVictimaVg : function()
    {
        this.inputFechaVictimaVg = new Ext.form.DateField(
            {
                width : 140,
                format : 'd/m/Y',
                altFormats : 'd/m/Y H:i:s',
                fieldLabel : 'Data víctima VG',
                name : 'fechaVictimaVg'
            });
    },

    buildInputFechaHijoVictimaVg : function()
    {
        this.inputFechaHijoVictimaVg = new Ext.form.DateField(
            {
                width : 140,
                format : 'd/m/Y',
                altFormats : 'd/m/Y H:i:s',
                fieldLabel : 'Data fill víctima VG',
                name : 'fechaHijoVictimaVg'
            });
    },

    buildInputFechaIniFamiliaNumerosa : function()
    {
        this.inputFechaIniFamiliaNumerosa = new Ext.form.DateField(
          {
              width : 140,
              format : 'd/m/Y',
              altFormats : 'd/m/Y H:i:s',
              fieldLabel : 'Data inici carnet',
              name : 'fechaIniFamiliaNumerosa'
          });
    },

    buildInputFechaFinFamiliaNumerosa : function()
    {
        this.inputFechaFinFamiliaNumerosa = new Ext.form.DateField(
        {
            width : 140,
            format : 'd/m/Y',
            altFormats : 'd/m/Y H:i:s',
            fieldLabel : 'Data fi carnet',
            name : 'fechaFinFamiliaNumerosa'
        });
    },

    cargaDatosFinales : function(ultimoStoreCargado)
    {
        ultimoStoreCargado.on('load', function(store, records, options)
        {
            this.cargaFormulario();
        }, this);
    },

    cargaFormulario : function()
    {
        var ref = this;
        this.panelForm.getForm().load(
        {
            url : '/bec/rest/solicitante/' + this.solicitanteId + '/beca/' + this.becaId + '/generales',
            method : 'GET',
            success : function(form)
            {
                var cursoAcademicoId = form.getValues().cursoAcademicoId;
                var convocatoriaId = form.getValues().convocatoriaId;

                ref.storeTanda.baseParams =
                {
                    'cursoAcademicoId' : cursoAcademicoId,
                    'convocatoriaId' : convocatoriaId,
                    'activa' : true
                };
                ref.storeTanda.load();
                var personaId = form.getValues().personaId;
                ref.storeEstudio.baseParams =
                {
                    'cursoAcademicoId' : cursoAcademicoId,
                    'personaId' : personaId
                };
                ref.storeEstudio.load();

                ref.storePersonaEstudio.baseParams =
                {
                    'becaId' : ref.becaId
                };
                ref.storePersonaEstudio.load({
                    callback: function(records) {
                        var rec = records[0];

                        if (rec.data.becario === "1") {
                            ref.estadoCircunstanciaBecario.setText("Sí");
                            ref.estadoCircunstanciaBecario.addClass("green");
                        } else {
                            ref.estadoCircunstanciaBecario.setText("No");
                            ref.estadoCircunstanciaBecario.addClass("red");
                        }
                    }
                });

            }
        });
    },

    buildStoreEstudio : function()
    {
        this.storeEstudio = this.getStore(
        {
            url : '/bec/rest/estudio/',
            record : 'Estudio',
            id : 'id',
            fieldList : [ 'id', 'nombre' ],
            listeners :
            {
                load : function()
                {
                    if (this.getCount() > 1 && this.getCount() < 5)
                    {
                        alert('Alumne que simultanea estudis');
                    }
                }
            }
        });
    },

    buildStorePersonaEstudio: function()
    {
        var ref = this;
        this.storePersonaEstudio= this.getStore(
            {
                url : '/bec/rest/personaEstudio/',
                record : 'PersonaEstudio',
                baseParams: {
                    becaId: ref.becaId
                },
                id : 'id',
                fieldList : [ 'personaId', 'becario' ]
            });
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
        {
            url : '/bec/rest/convocatoria/activa',
            record : 'Convocatoria',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreProceso : function()
    {
        this.storeProceso = this.getStore(
        {
            url : '/bec/rest/proceso/',
            record : 'Proceso',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreEstado : function()
    {
        this.storeEstado = this.getStore(
        {
            url : '/bec/rest/estado/',
            record : 'Estado',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreTanda : function()
    {
        this.storeTanda = this.getStore(
        {
            url : '/bec/rest/tanda',
            record : 'Tanda',
            id : 'id',
            fieldList : [ 'id', 'tandaTexto' ]
        });
    },

    buildStoreTipoFamilia : function()
    {
        this.storeTipoFamilia = this.getStore(
        {
            url : '/bec/rest/tipofamilia/',
            record : 'TipoFamilia',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreTipoIdentificacion : function()
    {
        this.storeTipoIdentificacion = this.getStore(
        {
            url : '/bec/rest/tipoidentificacion/',
            record : 'TipoIdentificacion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreProfesion : function()
    {
        this.storeProfesion = this.getStore(
        {
            url : '/bec/rest/profesion/',
            record : 'Profesion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            hiddenName : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildComboProceso : function()
    {
        this.comboProceso = new Ext.form.ComboBox(
        {
            fieldLabel : 'Procés',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 140,
            name : 'procesoId',
            hiddenName : 'procesoId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeProceso
        });
    },

    buildComboTanda : function()
    {
        this.comboTanda = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tanda',
            triggerAction : 'all',
            editable : true,
            width : 180,
            name : 'tandaId',
            hiddenName : 'tandaId',
            valueField : 'id',
            displayField : 'tandaTexto',
            store : this.storeTanda
        });
    },

    buildComboEstado : function()
    {
        this.comboEstado = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estat',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 155,
            name : 'estadoId',
            hiddenName : 'estadoId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeEstado
        });
    },

    buildComboTipoFamilia : function()
    {
        this.comboTipoFamilia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus familia',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'tipoFamiliaId',
            hiddenName : 'tipoFamiliaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoFamilia
        });
    },

    buildComboTipoIdentificacion : function()
    {
        this.comboTipoIdentificacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 50,
            name : 'tipoIdentificacionId',
            hiddenName : 'tipoIdentificacionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoIdentificacion
        });
    },

    buildComboProfesion : function()
    {
        this.comboProfesion = new Ext.form.ComboBox(
        {
            fieldLabel : 'Professió sustentador',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 350,
            name : 'profesionSustentadorId',
            hiddenName : 'profesionSustentadorId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeProfesion
        });
    },

    buildStoreComunidad : function()
    {
        this.storeComunidad = this.getStore(
        {
            url : '/bec/rest/comunidad',
            record : 'Comunidad',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboComunidad : function()
    {
        this.comboComunidad = new Ext.form.ComboBox(
        {
            fieldLabel : 'Com.autònoma fam.nombrosa',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            listWidth : 400,
            name : 'comunidadFamiliaNumerosaId',
            hiddenName : 'comunidadFamiliaNumerosaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeComunidad
        });
    },

    buildTextFieldSolicitudId : function()
    {
        this.textFieldSolicitudId = new Ext.form.TextField(
        {
            hidden : true,
            name : 'id'
        });
    },

    buildTextFieldNumeroBecaUji : function()
    {
        this.textFieldNumeroBecaUji = new Ext.form.TextField(
        {
            width : 80,
            readOnly : true,
            fieldLabel : 'Beca UJI',
            name : 'numeroBecaUji',
            readOnly : true
        });
    },

    buildTextFieldCodigoArchivoTemporal : function()
    {
        this.textFieldCodigoArchivoTemporal = new Ext.form.TextField(
        {
            width : 100,
            readOnly : true,
            fieldLabel : 'Arxiu temp.',
            name : 'codigoArchivoTemporal'
        });
    },

    buildTextFieldCodigoExpediente : function()
    {
        this.textFieldCodigoExpediente = new Ext.form.TextField(
        {
            width : 100,
            readOnly : true,
            fieldLabel : 'Cod. Expediente',
            name : 'codigoExpediente'
        });
    },
    
    buildTextFieldBecaId : function()
    {
        this.textFieldBecaId = new Ext.form.TextField(
        {
            width : 80,
            readOnly : true,
            fieldLabel : 'Id Beca',
            name : 'becaId'
        });
    },

    buildTextFieldCursoAcademicoId : function()
    {
        this.textFieldCursoAcademicoId = new Ext.form.TextField(
        {
            name : 'cursoAcademicoId',
            hidden : true
        });
    },

    buildTextFieldIdentificacion : function()
    {
        this.textFieldIdentificacion = new Ext.form.TextField(
        {
            width : 85,
            fieldLabel : 'Identificació',
            name : 'identificacion'
        });
    },

    buildTextFieldPersonaId : function()
    {
        this.textFieldPersonaId = new Ext.form.TextField(
        {
            width : 80,
            fieldLabel : 'Id persona',
            name : 'personaId',
            readOnly : true
        });
    },

    buildTextFieldFechaNotificacion : function()
    {
        this.textFieldFechaNotificacion = new Ext.form.TextField(
        {
            name : 'fechaNotificacion',
            width : 140,
            fieldLabel : 'Data Notificació'
        });
    },

    buildTextFieldOtraBeca : function()
    {
        this.textFieldOtraBeca = new Ext.form.TextField(
        {
            name : 'otraBeca',
            width : 160,
            fieldLabel : 'Otra beca'
        });
    },

    buildNumberFieldNumeroMiembrosComputables : function()
    {
        this.numberFieldNumeroMiembrosComputables = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Membres',
            name : 'numeroMiembrosComputables',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldNumeroHermanos : function()
    {
        this.numberFieldNumeroHermanos = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Germans',
            name : 'numeroHermanos',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldNumeroHermanosFuera : function()
    {
        this.numberFieldNumeroHermanosFuera = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Germans fora',
            name : 'numeroHermanosFuera',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildDisplayFieldTipoMinusvalia : function()
    {
        this.displayFieldTipoMinusvalia = new Ext.form.TextField(
        {
            width : 160,
            fieldLabel : 'Minusvalidesa Solicitant',
            name : 'tipoMinusvalia',
            readOnly : true
        });
    },

    buildNumberFieldNumeroMinusvalia33 : function()
    {
        this.numberFieldNumeroMinusvalia33 = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Minusvalidesa 33%',
            name : 'numeroMinusvalia33',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldNumeroMinusvalia65 : function()
    {
        this.numberFieldNumeroMinusvalia65 = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Minusvalidesa 65%',
            name : 'numeroMinusvalia65',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldIngresosNetos : function()
    {
        this.numberFieldIngresosNetos = new Ext.form.NumberField(
        {
            width : 60,
            readOnly : true,
            fieldLabel : 'Ingressos nets anuals',
            name : 'ingresosNetos',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldIngresosExtranjero : function()
    {
        this.numberFieldIngresosExtranjero = new Ext.form.NumberField(
        {
            width : 60,
            readOnly : true,
            fieldLabel : 'Ingressos estranger',
            name : 'ingresosExtranjero',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildTextFieldCarnetFamiliaNumerosa : function()
    {
        this.textFieldCarnetFamiliaNumerosa = new Ext.form.TextField(
        {
            width : 160,
            fieldLabel : 'Carnet familia nombrosa',
            name : 'carnetFamiliaNumerosa'
        });
    },

    buildTextFieldTipoPropiedad : function()
    {
        this.textFieldTipoPropiedad = new Ext.form.TextField(
        {
            width : 340,
            fieldLabel : 'Tipus Propietat',
            name : 'tipoPropiedad',
            readOnly : true

        });
    },

    buildNumberFieldImporteAlquiler : function()
    {
        this.numberFieldImporteAlquiler = new Ext.form.NumberField(
        {
            width : 60,
            fieldLabel : 'Import lloguer',
            name : 'importeAlquiler',
            readOnly : true,
            allowDecimals : true,
            allowNegative : false,
            decimalSeparator : ','
        });
    },

    buildCheckBoxOrfandad : function()
    {
        this.checkBoxOrfandad = new Ext.form.Checkbox(
        {
            name : 'orfandad',
            width : 100,
            inputValue : true,
            fieldLabel : 'Orfandat'
        });
    },
    buildCheckBoxIndVictimaVg : function()
    {
        this.checkBoxIndVictimaVg = new Ext.form.Checkbox(
            {
                name : 'indVictimaVg',
                width : 100,
                inputValue : true,
                fieldLabel : 'Víctima VG'
            });
    },
    buildCheckBoxIndHijoVictimaVg : function()
    {
        this.checkBoxIndHijoVictimaVg = new Ext.form.Checkbox(
            {
                name : 'indHijoVictimaVg',
                width : 100,
                inputValue : true,
                fieldLabel : 'Víctima fill VG'
            });
    },
    buildCheckBoxViolenciaGenero : function()
    {
        this.checkBoxViolenciaGenero = new Ext.form.Checkbox(
        {
            name : 'violenciaGenero',
            width : 100,
            inputValue : true,
            fieldLabel : 'Violència Gènere'
        });
    },
    buildCheckBoxResidenciaPermanente : function()
    {
        this.checkBoxResidenciaPermanente = new Ext.form.Checkbox(
        {
            name : 'residenciaPermanente',
            width : 100,
            inputValue : true,
            fieldLabel : 'Residència permanent'
        });
    },
    buildCheckBoxResidenciaTrabajo : function()
    {
        this.checkBoxResidenciaTrabajo = new Ext.form.Checkbox(
        {
            name : 'residenciaTrabajo',
            width : 100,
            inputValue : true,
            fieldLabel : 'Residència treball'
        });
    },
    buildCheckBoxUnidadFamiliarIndependiente : function()
    {
        this.checkBoxUnidadFamiliarIndependiente = new Ext.form.Checkbox(
        {
            disabled : true,
            name : 'unidadFamiliarIndependiente',
            width : 100,
            inputValue : true,
            fieldLabel : 'Unitat familiar independent'
        });
    },

    buildCheckBoxTransporteUrbano : function()
    {
        this.checkBoxTransporteUrbano = new Ext.form.Checkbox(
        {
            name : 'transporteUrbano',
            width : 100,
            inputValue : true,
            fieldLabel : 'Transport Urbà'
        });
    },

    buildCheckBoxAutorizacionRenta : function()
    {
        this.checkBoxAutorizacionRenta = new Ext.form.Checkbox(
        {
            name : 'autorizacionRenta',
            width : 100,
            inputValue : true,
            fieldLabel : 'Autorització renda'
        });
    },

    buildCheckBoxBecaConcedida : function()
    {
        this.checkBoxBecaConcedida = new Ext.form.Checkbox(
        {
            name : 'becaConcedida',
            width : 100,
            inputValue : true,
            fieldLabel : 'Beca concedida'
        });
    },
    buildCheckBoxReclamada : function()
    {
        this.checkBoxReclamada = new Ext.form.Checkbox(
            {
                name : 'reclamada',
                width : 100,
                inputValue : true,
                fieldLabel : 'Reclamada'
            });
    },
    buildInfoCircunstanciaBecario: function()
    {
        this.estadoCircunstanciaBecario = new Ext.form.Label({
            html: 'Carregant...',
            style: {
                "font-size": '14px',
                "font-weight": "bold",
                "margin-left": "5px"
            }

        })
        this.infoCircunstanciaBecario = new Ext.Panel(
            {
                name : 'circunstanciaBecario',
                type: 'hbox',
                items: [
                    {
                        xtype: 'label',
                        html: 'Circumstància becari/matrícula:',
                        style: {
                            "font-size": '12px'
                        },
                        width: 120
                    },
                    this.estadoCircunstanciaBecario
                ],
                width : 180,
            });
    },

    buildNumberFieldNumeroTransportes : function()
    {
        this.numberFieldNumeroTransportes = new Ext.form.NumberField(
        {
            width : 30,
            fieldLabel : 'Núm. Transports',
            name : 'numeroTransportes',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildCompositeFieldIdentificacion : function()
    {
        this.compositeFieldIdentificacion = new Ext.form.CompositeField(
        {
            items : [ this.comboTipoIdentificacion, this.textFieldIdentificacion ]
        });
    },

    buildButtonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 80,
            handler : function(button, event)
            {
                ref.panelForm.getForm().submit(
                {
                    url : '/bec/rest/solicitante/' + ref.solicitanteId + '/beca/' + ref.becaId + '/generales',
                    method : 'put',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Guardant Dades',
                    success : function(form, action)
                    {
                        ref.fireEvent('UJI.BEC.DatosGeneralesTab.buttonGuardar.click');
                    },
                    failure : function()
                    {
                    }
                });
            }
        });
    },

    buildPanelForm : function()
    {
        this.panelForm = new Ext.form.FormPanel(
        {
            layout : 'table',
            frame : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Solicitante'
            }, [ 'id', 'becaId', 'cursoAcademicoId', 'codigoArchivoTemporal', 'codigoExpediente', 'convocatoriaId', 'estadoId', 'numeroHermanos', 'numeroHermanosFuera', 'identificacion', 'importeAlquiler',
                    'ingresosExtranjero', 'ingresosNetos', 'numeroMiembrosComputables', 'numeroMinusvalia33', 'numeroMinusvalia65', 'numeroBecaUji', 'procesoId', 'tandaId', 'tipoFamiliaId',
                    'tipoPropiedad', 'orfandad', 'indVictimaVg', 'indHijoVictimaVg', 'tipoMinusvalia', 'unidadFamiliarIndependiente', 'comunidadFamiliaNumerosaId', 'fechaOrfandad', 'fechaVictimaVg', 'fechaHijoVictimaVg',
                    'fechaIniFamiliaNumerosa', 'fechaFinFamiliaNumerosa', 'carnetFamiliaNumerosa',
                    'transporteUrbano', 'numeroTransportes', 'fechaNotificacion', 'tipoIdentificacionId', 'profesionSustentadorId', 'autorizacionRenta', 'becaConcedida','reclamada',
                    'violenciaGenero','personaId', 'otraBeca', 'residenciaPermanente', 'residenciaTrabajo' ]),
            padding : '5px 0px',
            labelWidth : 90,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.textFieldCursoAcademicoId, this.textFieldSolicitudId, this.comboConvocatoria ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxBecaConcedida ]
            },
            {
                 layout : 'form',
                 items : [ this.infoCircunstanciaBecario ]
            },
            {
                 layout : 'form',
                 items : [ this.comboProceso ]
            },
            {
                 layout : 'form',
                 items : [ this.comboEstado ]
            },
            {
                 layout : 'form',
                 labelWidth : 40,
                 items : [ this.comboTanda ]
            },

            {
                layout : 'form',
                items : [ this.checkBoxReclamada ]
            },
            {
                layout : 'form',
                items : [ this.textFieldPersonaId ]
            },
            {
                layout : 'form',
                items : [ this.textFieldCodigoArchivoTemporal ]
            },

            {
                layout : 'form',
                items : [ this.textFieldNumeroBecaUji ]
            },
            {
               layout : 'form',
               items : [ this.textFieldBecaId ]
            },
            {
                layout : 'form',
                items : [ this.compositeFieldIdentificacion ]
            },
            {
                layout : 'form',
                items : [ this.textFieldOtraBeca ]
            },
            {
                layout : 'form',
                items : [ this.textFieldCodigoExpediente ]
            },
            {
                layout : 'form',
                items : [ this.textFieldFechaNotificacion ]
            },

            {
                style : 'margin:15px 0px;border-top:1px solid lightblue;',
                colspan : 3
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroMiembrosComputables ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroHermanos ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroHermanosFuera ]
            },
            {
                layout : 'form',
                items : [ this.comboTipoFamilia ]
            },
                {},
                {},
            {
                layout : 'form',
                items : [ this.textFieldCarnetFamiliaNumerosa ]
            },
            {
                layout : 'form',
                items : [ this.inputFechaIniFamiliaNumerosa ]
            },
            {
                layout : 'form',
                items : [ this.inputFechaFinFamiliaNumerosa ]
            },
            {
                layout : 'form',
                items : [ this.comboComunidad ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxOrfandad ]
            },
            {
                layout : 'form',
                items : [ this.inputFechaOrfandad ]
            },
            {
                layout : 'form',
                items : [ this.displayFieldTipoMinusvalia ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroMinusvalia33 ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroMinusvalia65 ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxTransporteUrbano ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldNumeroTransportes ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxViolenciaGenero ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndVictimaVg ]
            },
            {
                layout : 'form',
                items : [ this.inputFechaVictimaVg ],
                colspan : 2
            },

            {
                layout : 'form',
                items : [ this.checkBoxIndHijoVictimaVg ]
            },
            {
                layout : 'form',
                items : [ this.inputFechaHijoVictimaVg ],
                colspan : 2
            },
            {
                layout : 'form',
                items : [ this.comboProfesion ],
                colspan : 2
            },
            {
                layout : 'form',
                items : [ this.checkBoxAutorizacionRenta ]
            },
            {
                style : 'margin:15px 0px;border-top:1px solid lightblue;',
                colspan : 3
            },
            {
                layout : 'form',
                items : [ this.checkBoxUnidadFamiliarIndependiente ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldIngresosNetos ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldIngresosExtranjero ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxResidenciaPermanente ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.checkBoxResidenciaTrabajo ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.textFieldTipoPropiedad ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldImporteAlquiler ]
            }, {}, {},
            {
                layout : 'form',
                style : 'width:120px; margin-left:auto;',
                items : [ this.buttonGuardar ]
            } ]
        });
    }
});