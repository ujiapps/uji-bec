Ext.ns('UJI.BEC');
UJI.BEC.ErroresTanda = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Errors de tandes',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.ErroresTanda.superclass.initComponent.call(this);
        this.initUI();
        this.addEvents('newtab');
    },

    initUI : function()
    {
        this.buildStoreListaErroresTanda();
        this.buildTextFieldBecaId();
        this.buildTextFieldTandaId();

        this.buildButtonBuscar();
        this.buildButtonIrABeca();

        this.buildPanelFiltros();
        this.buildPanelGrid();

        this.add(this.panelFiltros);
        this.add(this.panelGrid);
    },

    buildStoreListaErroresTanda : function()
    {
        this.storeListaErroresTanda = this.getStore(
        {
            url : '/bec/rest/registroerroresenvio',
            record : 'RegistroErroresEnvio',
            id : 'id',
            fieldList : [ 'id', 'becaId', 'becaUji', 'tandaId', 'error', 'tanda', 'solicitanteId', 'cursoAca', 'nombreApellidos', 'titulacion', 'convocatoria', 'organismoId' ]
        });
    },

    buildTextFieldBecaId : function()
    {
        this.textFieldBecaId = new Ext.form.TextField(
        {
            width : 80,
            fieldLabel : 'Beca',
            name : 'becaId'
        });
    },

    buildTextFieldTandaId : function()
    {
        this.textFieldTandaId = new Ext.form.TextField(
        {
            width : 100,
            fieldLabel : 'Tanda',
            name : 'tandaId'
        });
    },

    buildButtonBuscar : function()
    {
        var ref = this;
        this.buttonBuscar = new Ext.Button(
        {
            text : 'Recerca',
            width : 80,
            iconCls : 'magnifier',
            handler : function(button, event)
            {
                ref.storeListaErroresTanda.proxy.setUrl('/bec/rest/registroerroresenvio', true);
                ref.storeListaErroresTanda.load(
                {
                    params : ref.panelFiltros.getForm().getFieldValues()
                });
            }
        });
    },

    abrirGestionBecas : function()
    {
        var filaSeleccionada = this.panelGrid.getSelectionModel().getSelected();

        if (filaSeleccionada)
        {
            var becaId = filaSeleccionada.get('becaId');
            var solicitanteId = filaSeleccionada.get('solicitanteId');
            var cursoAca = filaSeleccionada.get('cursoAca');
            var nombreApellidos = filaSeleccionada.get('nombreApellidos');
            var titulacion = filaSeleccionada.get('titulacion');
            var convocatoria = filaSeleccionada.get('convocatoria');
            var organismoId = filaSeleccionada.get('organismoId');
            this.fireEvent('newtab',
            {
                becaId : becaId,
                solicitanteId : solicitanteId,
                cursoAca : cursoAca,
                nombreApellidos : nombreApellidos,
                titulacion : titulacion,
                convocatoria : convocatoria,
                organismoId : organismoId,
                pantalla : 'UJI.BEC.GestionBecaPanel'
            });
        }
    },
    buildButtonIrABeca : function()
    {
        var ref = this;
        
        this.buttonIrABeca = new Ext.Button(
        {
            text : 'Anar a la beca',
            iconCls : 'building-go',
            handler : function(button, event)
            {
                ref.abrirGestionBecas();
            }
        });
    },
    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Filtres',
            layout : 'table',
            frame : true,
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.textFieldTandaId ]
            },
            {
                layout : 'form',
                items : [ this.textFieldBecaId ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonBuscar ]
            } ]
        });
    },

    buildPanelGrid : function()
    {
        var ref = this;
        this.panelGrid = new Ext.grid.GridPanel(
        {
            store : ref.storeListaErroresTanda,
            autoScroll : true,
            flex : 1,
            loadMask : true,
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Tanda',
                    width : 40,
                    dataIndex : 'tanda'
                },
                {
                    header : 'Id Beca',
                    width : 50,
                    dataIndex : 'becaId'
                },
                {
                    header : 'Beca Uji',
                    width : 50,
                    dataIndex : 'becaUji'
                },
                {
                    header : 'Error',
                    width : 200,
                    dataIndex : 'error'
                } ]

            }),
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            tbar :
            {
                items : [ this.buttonIrABeca ]
            },
            listeners :
            {
                dblclick : function(dv, record, item, index, e)
                {
                    ref.abrirGestionBecas();
                }
            }

        });
    }
});