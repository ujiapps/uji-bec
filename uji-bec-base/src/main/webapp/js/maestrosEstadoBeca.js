Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosEstadoBeca = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title: 'Estats beca',
    url: '/bec/rest/estado',
    record: 'Estado'
});