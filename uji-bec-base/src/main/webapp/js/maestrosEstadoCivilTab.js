Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosEstadoCivil = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title: 'Estats civils',
    url: '/bec/rest/estadocivil',
    record: 'EstadoCivil'
});