Ext.ns('UJI.BEC');

UJI.BEC.Textos = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Textos',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Textos.superclass.initComponent.call(this);

        this.initUI();

        this.initListeners();
    },

    initListeners : function()
    {
        var ref = this;
        this.on('procesochange', function(procesoId)
        {
            ref.comboOrganismo.enable();
            ref.comboOrganismo.clearValue();
            ref.panelForm.getForm().reset();
        });

        this.on('organismochange', function(organismoId, procesoId)
        {
            ref.panelForm.getForm().reset();
            if (organismoId && procesoId)
            {
                ref.loadForm(organismoId, procesoId);
                ref.textFieldOrganismoId.setValue(organismoId);
                ref.textFieldProcesoId.setValue(procesoId);
            }
        });
    },

    initUI : function()
    {
        this.buildStoreProceso();
        this.buildStoreOrganismo();

        this.buildComboProceso();
        this.buildComboOrganismo();

        this.buildTextFieldId();
        this.buildTextFieldProcesoId();
        this.buildTextFieldOrganismoId();
        this.buildTextFieldNombre();

        this.buildButtonGuardar();

        this.buildPanelFiltro();
        this.buildPanelMultiIdioma();
        this.buildPanelForm();

        this.add(this.panelFiltro);
        this.add(this.panelForm);
    },

    loadForm : function(organismoId, procesoId)
    {
        var ref = this;

        this.panelForm.getForm().load(
        {
            url : '/bec/rest/texto/proceso/' + procesoId + '/organismo/' + organismoId,
            method : 'GET',
            success : function(form, action)
            {
                ref.panelForm.doLayout();
                ref.panelForm.render();
            },
            failure : function(form, action)
            {
            }
        });
    },

    buildStoreProceso : function()
    {
        this.storeProceso = this.getStore(
        {
            url : '/bec/rest/proceso/',
            record : 'Proceso',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreOrganismo : function()
    {
        this.storeOrganismo = this.getStore(
        {
            url : '/bec/rest/organismo/',
            record : 'Organismo',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboProceso : function()
    {
        var ref = this;
        this.comboProceso = new Ext.form.ComboBox(
        {
            fieldLabel : 'Procés',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 250,
            name : 'procesoId',
            hiddenName : 'procesoId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeProceso,
            listeners :
            {
                'select' : function(combo, value)
                {
                    ref.fireEvent('procesochange', value.id);
                }
            }
        });
    },

    buildComboOrganismo : function()
    {
        var ref = this;
        this.comboOrganismo = new Ext.form.ComboBox(
        {
            fieldLabel : 'Organisme',
            triggerAction : 'all',
            editable : false,
            disabled : true,
            forceSelection : true,
            width : 250,
            name : 'organismoId',
            hiddenName : 'organismoId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeOrganismo,
            listeners :
            {
                'select' : function(combo, value)
                {
                    procesoId = ref.comboProceso.getValue();
                    ref.fireEvent('organismochange', value.id, procesoId);
                }
            }
        });
    },

    buildTextFieldNombre : function()
    {
        this.textFieldNombre = new Ext.form.TextField(
        {
            width : 350,
            fieldLabel : 'Nom',
            name : 'nombre',
            style :
            {
                marginBottom : '15px'
            }
        });
    },

    buildTextFieldId : function()
    {
        this.textFieldId = new Ext.form.TextField(
        {
            name : 'id',
            hidden : true
        });
    },

    buildTextFieldProcesoId : function()
    {
        this.textFieldProcesoId = new Ext.form.TextField(
        {
            name : 'procesoId',
            hidden : true
        });
    },

    buildTextFieldOrganismoId : function()
    {
        this.textFieldOrganismoId = new Ext.form.TextField(
        {
            name : 'organismoId',
            hidden : true
        });
    },

    buildButtonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 70,
            handler : function(button, event)
            {
                if (ref.panelForm.getForm().isValid())
                {
                    var id = ref.textFieldId.getValue();
                    var metodo = 'post';
                    if (id)
                    {
                        metodo = 'put';
                    }

                    ref.panelForm.getForm().submit(
                    {
                        url : '/bec/rest/texto/',
                        method : metodo,
                        success : function(form, action)
                        {
                        },
                        failure : function()
                        {
                            alert('Se ha producido un error al guardar los datos.');
                        }
                    });
                }
            },
            listeners : {}
        });
    },

    buildPanelFiltro : function()
    {
        this.panelFiltro = new Ext.form.FormPanel(
        {
            layout : 'form',
            frame : false,
            padding : '10px 7px',
            labelWidth : 65,
            items : [ this.comboProceso, this.comboOrganismo ]
        });
    },

    buildPanelForm : function()
    {
        this.panelForm = new Ext.FormPanel(
        {
            layout : 'form',
            frame : true,
            margins : '5 0',
            padding : '10px 5px',
            items : [ this.textFieldId, this.textFieldProcesoId, this.textFieldOrganismoId, this.textFieldNombre, this.panelMultiIdioma, this.buttonGuardar ],
            reader : new Ext.data.XmlReader(
            {
                record : 'Texto'
            }, [ 'id', 'procesoId', 'organismoId', 'nombre', 'cabeceraCa', 'pieCa', 'cabeceraEs', 'pieEs' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ])
        });
    },

    buildPanelMultiIdioma : function()
    {
        this.panelMultiIdioma = new Ext.ux.uji.LanguagePanel(
        {
            language : [
            {
                lang : 'Ca',
                title : 'Valencià'
            },
            {
                lang : 'Es',
                title : 'Espanyol'
            } ]
        });

        this.panelMultiIdioma.addComponent(new Ext.form.TextArea(
        {
            name : 'cabecera',
            fieldLabel : 'Capçalera',
            anchor : '100%',
            height : '100',
            maxLength : '4000',
            maxLengthText : "El quadre de texte no pot superar els 4000 caràcters"
        }));

        this.panelMultiIdioma.addComponent(new Ext.form.TextArea(
        {
            name : 'pie',
            fieldLabel : 'Peu',
            anchor : '100%',
            height : '100',
            maxLength : '4000',
            maxLengthText : "El quadre de texte no pot superar els 4000 caràcters"
        }));
    }
});