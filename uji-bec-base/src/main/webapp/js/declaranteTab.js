Ext.ns('UJI.BEC');

UJI.BEC.DeclaranteTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Declarant',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 600,
    frame : true,
    padding : '2px 0px',

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.DeclaranteTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreDeclarante();
        this.buildStoreTipoIdentificacion();
        this.buildComboTipoIdentificacion();

        this.buildDeclaranteId();
        this.buildSolicitanteId();

        this.buildInputDeclarante();
        this.buildInputLocalidadDeclarante();
        this.buildInputFechaDeclarante();

        this.buildComboIndicadorUltCurso();
        this.buildComboIndicadorCursoPendiente();
        this.buildComboIndicadorCursoAnterior();
        this.buildComboIndicadorIndependiente();
        this.buildComboIndicadorInem();
        this.buildComboIndicadorRentasExtranjero();

        this.buildBotonGuardar();
        this.buildInputEmpleador();
        this.buildInputIdentificacion();
        this.buildInputIngresosAnuales();
        this.buildInputCodigoParentesco();
        this.buildInputRentasExtranjero();

        this.buildCompositeIdentificacion();
        this.buildCompositeRentasExtranjero();
        this.buildPanelFormulario();

        this.add(this.panelFormulario);
    },

    buildDeclaranteId : function()
    {
        this.declaranteId = new Ext.form.NumberField(
        {
            name : 'id',
            hidden : true
        });
    },

    buildSolicitanteId : function()
    {
        this.solicitanteId = new Ext.form.NumberField(
        {
            name : 'solicitanteId',
            value : this.solicitanteId,
            hidden : true
        });
    },

    buildStoreDeclarante : function()
    {
        this.storeDeclarante = new Ext.data.Store(
        {
            baseParams :
            {
                solicitanteId : this.solicitanteId
            },
            restful : true,
            url : '/bec/rest/declarante',
            reader : new Ext.data.XmlReader(
            {
                record : 'Declarante',
                id : 'id'
            }, [ 'id', 'solicitanteId', 'declarante', 'indicadorRentasExtranjero', 'rentasExtranjero', 'localidadDeclarante', 'fechaDeclarante', 'indicadorUltCurso', 'indicadorCursoPendiente',
                    'indicadorRentasExtranjero', 'indicadorCursoAnterior', 'indicadorIndependiente', 'indicadorInem', 'empleador', 'tipoIdentificacionId', 'identificacion', 'ingresosAnuales',
                    'codigoParentesco' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    loadStore: function() {
        this.storeTipoIdentificacion.reload();
    },

    buildStoreTipoIdentificacion : function()
    {
        this.storeTipoIdentificacion = this.getStore(
        {
            url : '/bec/rest/tipoidentificacion',
            record : 'TipoIdentificacion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboTipoIdentificacion : function()
    {
        var ref = this;
        this.comboTipoIdentificacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            width : 80,
            name : 'tipoIdentificacionId',
            hiddenName : 'tipoIdentificacionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoIdentificacion,
            listeners :
            {
                select : function(combo, record)
                {
                    if (record.get("nombre"))
                    {
                        ref.inputIdentificacion.enable();
                    }
                    else
                    {
                        ref.inputIdentificacion.reset();
                        ref.inputIdentificacion.disable();
                    }
                }
            }
        });
    },

    buildComboIndicadorUltCurso : function()
    {
        this.comboIndicadorUltCurso = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            forceSelection : true,
            lazyRender : true,
            hiddenName : 'indicadorUltCurso',
            width : 90,
            fieldLabel : 'Últim curs',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorCursoPendiente : function()
    {
        this.comboIndicadorCursoPendiente = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 90,
            hiddenName : 'indicadorCursoPendiente',
            fieldLabel : 'Curs pendent',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorCursoAnterior : function()
    {
        this.comboIndicadorCursoAnterior = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 90,
            hiddenName : 'indicadorCursoAnterior',
            fieldLabel : 'Curs anterior',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorIndependiente : function()
    {
        this.comboIndicadorIndependiente = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 90,
            fieldLabel : 'Independent',
            hiddenName : 'indicadorIndependiente',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorRentasExtranjero : function()
    {
        var ref = this;
        this.comboIndicadorRentasExtranjero = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 90,
            fieldLabel : 'Rentes extranger',
            hiddenName : 'indicadorRentasExtranjero',
            mode : 'local',
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name',
            listeners :
            {
                select : function(combo, record)
                {
                    if (record.get("name") == "Sí")
                    {
                        ref.inputRentasExtranjero.enable();
                    }
                    else
                    {
                        ref.inputRentasExtranjero.reset();
                        ref.inputRentasExtranjero.disable();
                    }
                }
            }
        });
    },

    buildComboIndicadorInem : function()
    {
        this.comboIndicadorInem = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 90,
            fieldLabel : 'Inem',
            hiddenName : 'indicadorInem',
            mode : 'local',
            allowBlank : true,
            forceSelection : false,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildInputIdentificacion : function()
    {
        this.inputIdentificacion = new Ext.form.TextField(
        {
            width : 115,
            disabled : true,
            fieldLabel : 'Identificació',
            name : 'identificacion'
        });
    },

    buildInputEmpleador : function()
    {
        this.inputEmpleador = new Ext.form.TextField(
        {
            width : 200,
            fieldLabel : 'Empleant',
            name : 'empleador'
        });
    },

    buildInputDeclarante : function()
    {
        this.inputDeclarante = new Ext.form.TextField(
        {
            width : 420,
            allowBlank : false,
            fieldLabel : 'Declarant',
            name : 'declarante'
        });
    },

    buildInputLocalidadDeclarante : function()
    {
        this.inputLocalidadDeclarante = new Ext.form.TextField(
        {
            width : 200,
            allowBlank : false,
            fieldLabel : 'Localitat',
            name : 'localidadDeclarante'
        });
    },

    buildInputFechaDeclarante : function()
    {
        this.inputFechaDeclarante = new Ext.form.DateField(
        {
            width : 200,
            allowBlank : false,
            format : 'd/m/Y',
            altFormats : 'd/m/Y H:i:s',
            fieldLabel : 'Data',
            name : 'fechaDeclarante'
        });
    },

    buildCompositeIdentificacion : function()
    {
        this.compositeIdentificacion = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Identificació',
            items : [ this.comboTipoIdentificacion, this.inputIdentificacion ]
        });
    },

    buildCompositeRentasExtranjero : function()
    {
        this.compositeRentasExtranjero = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Rentes Extranger',
            items : [ this.comboIndicadorRentasExtranjero, this.inputRentasExtranjero ]
        });
    },

    buildInputCodigoParentesco : function()
    {
        this.inputCodigoParentesco = new Ext.form.TextField(
        {
            width : 200,
            fieldLabel : 'Codi parentiu',
            name : 'codigoParentesco'
        });
    },

    buildInputIngresosAnuales : function()
    {
        this.inputIngresosAnuales = new Ext.form.NumberField(
        {
            width : 200,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Ingresos anuals',
            name : 'ingresosAnuales'
        });
    },

    buildInputRentasExtranjero : function()
    {
        this.inputRentasExtranjero = new Ext.form.NumberField(
        {
            width : 100,
            allowDecimals : true,
            decimalSeparator : ",",
            disabled : true,
            name : 'rentasExtranjero'
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                var id = ref.panelFormulario.getForm().getValues().id;

                if (id != '' && id != null)
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/declarante/' + id,
                        method : 'put',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.storeDeclarante.reload();
                            //ref.fireEvent('UJI.BEC.DeclaranteTab.botonGuardar.click');
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
                else
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/declarante/',
                        method : 'post',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.load(ref.solicitanteId);
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
            }
        });

    },

    buildPanelFormulario : function()
    {
        this.panelFormulario = new Ext.FormPanel(
        {
            layout : 'table',
            flex : 1,
            width : 700,
            frame : true,
            style : 'margin-right:16px;',
            labelAlign : 'top',
            reader : new Ext.data.XmlReader(
            {
                record : 'Miembro',
                id : 'id'
            }, [ 'id', 'solicitanteId', 'inputDeclarante', 'comboIndicadorRentasExtranjero', 'inputRentasExtranjero', 'inputLocalidadDeclarante', 'inputFechaDeclarante', 'comboIndicadorUltCurso',
                    'comboIndicadorCursoPendiente', 'comboIndicadorCursoAnterior', 'comboIndicadorIndependiente', 'comboIndicadorInem', 'inputEmpleador', 'comboTipoIdentificacion',
                    'inputIdentificacion', 'inputIngresosAnuales', 'inputCodigoParentesco' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 3
            },
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            buttons : [ this.botonGuardar ],
            items : [
            {
                colspan : 3,
                layout : 'form',
                items : [ this.inputDeclarante ]
            },
            {
                layout : 'form',
                colspan : 1,
                items : [ this.inputLocalidadDeclarante ]
            },

            {
                layout : 'form',
                items : [ this.inputFechaDeclarante ]
            },
            {
                layout : 'form',
                items : [ this.inputIngresosAnuales ]
            },
            /*
             * { layout : 'form', colspan : 1, items : [ this.inputCodigoParentesco ] },
             */
            {
                xtype : "fieldset",
                title : 'Informació del empleant',
                layout : 'table',
                padding : '5px 0px',
                layoutConfig :
                {
                    columns : 3
                },
                defaults :
                {
                    bodyStyle : 'padding:0px 15px 0px 0px',
                    layout : 'form'
                },
                colspan : 3,
                items : [
                {
                    colspan : 2,
                    items : [ this.inputEmpleador ]
                },
                {
                    colspan : 1,
                    items : [ this.compositeIdentificacion ]
                }

                ]
            },
            {
                layout : 'form',
                items : [ this.comboIndicadorIndependiente ]
            },
            {
                layout : 'form',
                items : [ this.comboIndicadorInem ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.compositeRentasExtranjero ]
            },
            {
                layout : 'form',
                items : [ this.comboIndicadorUltCurso ]
            },
            {
                layout : 'form',
                items : [ this.comboIndicadorCursoPendiente ]
            },
            {
                layout : 'form',
                items : [ this.comboIndicadorCursoAnterior ]
            },
            {
                layout : 'form',
                items : [ this.declaranteId ]
            },
            {
                layout : 'form',
                items : [ this.solicitanteId ]
            } ]
        });
    },

    load : function(solicitanteId)
    {
        this.solicitanteId = solicitanteId;
        var ref = this;
        this.storeDeclarante.load(
        {
            solicitanteId : solicitanteId,
            callback : function(r)
            {
                if (r[0])
                {
                    ref.panelFormulario.getForm().loadRecord(r[0]);
                    if (r[0].data.indicadorRentasExtranjero === "1")
                    {
                        ref.inputRentasExtranjero.setDisabled(false);
                    }
                    else
                    {
                        ref.inputRentasExtranjero.setDisabled(true);
                    }

                    if (r[0].data.tipoIdentificacionId)
                    {
                        ref.inputIdentificacion.setDisabled(false);
                    }
                    else
                    {
                        ref.inputIdentificacion.setDisabled(true);
                    }
                }
            }
        });
    }

});