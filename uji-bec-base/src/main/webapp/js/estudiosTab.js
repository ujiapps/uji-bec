Ext.ns('UJI.BEC');

UJI.BEC.EstudiosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Estudis',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.EstudiosTab.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.formPanel);
        this.cargaFormulario();
    },

    initUI : function()
    {
        this.buildBotonGuardar();

        this.buildStoreTipoMatricula();
        this.buildStoreEstudio();

        this.buildComboTipoMatricula();
        this.buildComboEstudio();

        this.buildNumberFieldEstudioId();
        this.buildNumberFieldNumeroCursos();
        this.buildNumberFieldPlanAnteriorNumCursos();
        this.buildNumberFieldCreditosEstudioAnt();
        this.buildNumberFieldCreditosEstudio();
        this.buildNumberFieldCreditosBeca();
        this.buildNumberFieldCreditosBecaParcial();
        this.buildNumberFieldCodigoEstudioAnt();
        this.buildNumberFieldCodigoEstudioUjiAnt();

        this.buildTextFieldCodigoCentroAnt();
        this.buildTextFieldNombreCentroAnt();
        this.buildTextFieldTipoFamilia();
        this.buildTextFieldEstudioAnt();
        this.buildTextFieldTipoMatricula();
        this.buildTextFieldRama();

        this.buildCheckBoxDobleTitulacion();
        this.buildCheckBoxTecnica();
        this.buildCheckBoxPresencial();
        this.buildCheckBoxSinDocencia();
        this.buildCheckBoxBecario();

        this.buildCompositeEstudio();

        this.buildFieldsetTipoMatricula();
        this.buildFieldsetDadesEstudi();
        this.buildFieldsetDadesMatriculaUji();
        this.buildFieldsetDadesPlanAnterior();

        this.buildFormPanel();
    },

    cargaFormulario : function()
    {
        var ref = this;

        this.formPanel.getForm().load(
        {
            url : '/bec/rest/solicitante/' + this.solicitanteId + '/beca/' + this.becaId,
            waitMsg : 'Per favor espere ...',
            waitTitle : 'Carregant formulari',
            method : 'GET',
            success : function(form, action)
            {
                var cursoAcademicoId = action.result.data.cursoAcademicoId;
                var personaId = action.result.data.personaId;

                ref.storeEstudio.setBaseParam('cursoAcademicoId', cursoAcademicoId);
                ref.storeEstudio.setBaseParam('personaId', personaId);
                ref.storeEstudio.load();
            }
        });
    },

    buildFormPanel : function()
    {
        this.formPanel = new Ext.form.FormPanel(
        {
            width : 810,
            reader : new Ext.data.XmlReader(
            {
                record : 'BecaEstudio'
            }, [ 'estudioId', 'numeroCursos', 'creditosTotales', 'creditosBecaCompleta', 'creditosBecaParcial', 'tipoMatriculaId', 'personaId', 'cursoAcademicoId', 'familiaNumerosa', 'tipoMatricula',
                    'codigoEstudioAnt', 'codigoEstudioUjiAnt', 'estudioAnt', 'codigoCentroAnt', 'nombreCentroAnt', 'planAnteriorNumCursos', 'creditosEstudioAnt', 'dobleTitulacion', 'rama',
                    {
                        name : 'titulacionTecnica',
                        convert : function(value, record)
                        {
                            return (value == 'true') ? 'Sí' : 'No';
                        }
                    },
                    {
                        name : 'presencial',
                        convert : function(value, record)
                        {
                            if (value == 'S') {
                                return 'Si';
                            }
                            else if (value == 'N') {
                                return 'No';
                            }
                            else if (value == 'P')
                            {
                                return 'Semi'
                            }
                        }
                    },
                    {
                        name : 'sinDocencia',
                        convert : function(value, record)
                        {
                            return (value == 'true') ? 'Sí' : 'No';
                        }
                    },
                    {
                        name : 'becario',
                        convert : function(value, record)
                        {
                            return (value == 'true') ? 'Sí' : 'No';
                        }
                    },
                    {
                        name : 'valueEstudioId',
                        mapping : 'estudioId'
                    } ]),
            writer : new Ext.data.XmlWriter({

            }),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            buttons : [ this.botonGuardar ],
            items : [ this.fieldsetTipoMatricula, this.fieldsetDadesEstudi, this.fieldsetDadesMatriculaUji, this.fieldsetDadesPlanAnterior ]
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 80,
            handler : function(button, event)
            {
                ref.formPanel.getForm().submit(
                {
                    url : '/bec/rest/solicitante/' + ref.solicitanteId + '/beca/' + ref.becaId,
                    method : 'put',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Guardant Dades',
                    success : function(form, action)
                    {
                        ref.cargaFormulario();
                    },
                    failure : function(form, action)
                    {
                    }
                });
            }
        });
    },

    loadStore: function() {
        this.storeTipoMatricula.reload();
        this.storeEstudio.reload();
    },

    buildStoreTipoMatricula : function()
    {
        this.storeTipoMatricula = this.getStore(
        {
            url : '/bec/rest/tipomatricula/',
            record : 'TipoMatricula',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreEstudio : function()
    {
        this.storeEstudio = this.getStore(
        {
            url : '/bec/rest/estudio/',
            record : 'Estudio',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildCheckBoxDobleTitulacion : function()
    {
        this.checkBoxDobleTitulacion = new Ext.form.Checkbox(
        {
            name : 'dobleTitulacion',
            readOnly : true,
            width : 145,
            boxLabel : 'Doble titulació'
        });
    },

    buildComboTipoMatricula : function()
    {
        this.comboTipoMatricula = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus matrícula',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'tipoMatriculaId',
            hiddenName : 'tipoMatriculaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoMatricula
        });
    },

    buildFieldsetTipoMatricula : function()
    {
        this.fieldsetTipoMatricula = new Ext.form.FieldSet(
        {
            title : 'Matricula',
            autoHeight : true,
            width: 800,
            items : [ this.comboTipoMatricula ]
        });
    },

    buildNumberFieldEstudioId : function()
    {
        this.numberFieldEstudioId = new Ext.form.NumberField(
        {
            width : 60,
            name : 'valueEstudioId',
            readOnly : true
        });
    },

    buildComboEstudio : function()
    {
        this.comboEstudio = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estudi',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 500,
            name : 'estudioId',
            hiddenName : 'estudioId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeEstudio
        });
    },

    buildCompositeEstudio : function()
    {
        this.compositeEstudio = new Ext.form.CompositeField(
        {
            items : [ this.comboEstudio, this.numberFieldEstudioId, this.checkBoxDobleTitulacion ]
        });
    },

    buildFieldsetDadesEstudi : function()
    {
        this.fieldsetDadesEstudi = new Ext.form.FieldSet(
        {
            title : 'Dades Estudi',
            autoHeight : true,
            width: 800,
            items : [ this.compositeEstudio,
            {
                layout : 'table',
                layoutConfig :
                {
                    columns : 4
                },
                padding : '5px 0px',
                defaults :
                {
                    bodyStyle : 'padding:0px 15px 0px 0px',
                    layout : 'form'
                },
                items : [
                {
                    items : [ this.numberFieldNumeroCursos ]
                },
                {
                    items : [ this.numberFieldCreditosEstudio ]
                },
                {
                    items : [ this.numberFieldCreditosBeca ]
                },
                {
                    items : [ this.textNumberCreditosBecaParcial ]
                },
                {
                    items : [ this.checkBoxTecnica ]
                },
                {
                    items : [ this.checkBoxPresencial ]
                },
                {
                    items : [ this.checkBoxSinDocencia ]
                },
                {
                    items : [ this.textFieldRama ]
                } ]
            } ]
        });
    },

    buildNumberFieldCreditosBecaParcial : function()
    {
        this.textNumberCreditosBecaParcial = new Ext.form.NumberField(
        {
            width : 60,
            fieldLabel : 'Crd. Beca Parcial',
            name : 'creditosBecaParcial',
            allowDecimals : true,
            decimalSeparator : ',',
            readOnly : true
        });
    },

    buildNumberFieldCreditosBeca : function()
    {
        this.numberFieldCreditosBeca = new Ext.form.NumberField(
        {
            width : 60,
            fieldLabel : 'Crd. Beca Completa',
            name : 'creditosBecaCompleta',
            allowDecimals : true,
            decimalSeparator : ',',
            readOnly : true
        });
    },

    buildNumberFieldCreditosEstudio : function()
    {
        this.numberFieldCreditosEstudio = new Ext.form.NumberField(
        {
            width : 60,
            fieldLabel : 'Cred. Estudi',
            name : 'creditosTotales',
            allowDecimals : true,
            decimalSeparator : ',',
            readOnly : true
        });
    },

    buildNumberFieldNumeroCursos : function()
    {
        this.numberFieldNumeroCursos = new Ext.form.TextField(
        {
            width : 60,
            fieldLabel : 'Nombre cursos',
            name : 'numeroCursos',
            readOnly : true
        });
    },

    buildCheckBoxTecnica : function()
    {
        this.checkBoxTecnica = new Ext.form.TextField(
        {
            name : 'titulacionTecnica',
            width : 60,
            margins : '0 10',
            fieldLabel : 'Tècnica',
            readOnly : true
        });
    },

    buildCheckBoxPresencial : function()
    {
        this.checkBoxPresencial = new Ext.form.TextField(
        {
            name : 'presencial',
            width : 60,
            margins : '0 10',
            fieldLabel : 'Presencial',
            readOnly : true
        });
    },

    buildCheckBoxSinDocencia : function()
    {
        this.checkBoxSinDocencia = new Ext.form.TextField(
        {
            name : 'sinDocencia',
            width : 60,
            margins : '0 10',
            fieldLabel : 'Sense docencia',
            readOnly : true
        });
    },



    buildFieldsetDadesMatriculaUji : function()
    {
        this.fieldsetDadesMatriculaUji = new Ext.form.FieldSet(
        {
            title : 'Dades Matricula Uji',
            autoHeight : true,
            width: 800,
            layout : 'table',
            labelWidth : 80,
            layoutConfig :
            {
                columns : 3
            },
            padding : '5px 0px',
            defaults :
            {
                bodyStyle : 'padding:0px 15px 0px 0px',
                layout : 'form'
            },
            items : [
            {
                items : [ this.textFieldTipoFamilia ]
            },
            {
                items : [ this.checkBoxBecario ]
            },
            {
                items : [ this.textFieldTipoMatricula ]
            } ]
        });
    },

    buildTextFieldTipoFamilia : function()
    {
        this.textFieldTipoFamilia = new Ext.form.TextField(
        {
            width : 170,
            fieldLabel : 'Tipus Familia',
            name : 'familiaNumerosa',
            readOnly : true
        });
    },

    buildTextFieldRama : function()
    {
        this.textFieldRama = new Ext.form.TextField(
            {
                name : 'rama',
                width : 60,
                margins : '0 10',
                fieldLabel : 'Branca',
                readOnly : true
            });
    },

    buildCheckBoxBecario : function()
    {
        this.checkBoxBecario = new Ext.form.TextField(
        {
            name : 'becario',
            width : 30,
            fieldLabel : 'Becari',
            readOnly : true
        });
    },

    buildTextFieldTipoMatricula : function()
    {
        this.textFieldTipoMatricula = new Ext.form.TextField(
        {
            name : 'tipoMatricula',
            width : 200,
            margins : '0 10',
            fieldLabel : 'Matrícula',
            readOnly : true
        });
    },

    buildFieldsetDadesPlanAnterior : function()
    {
        this.fieldsetDadesPlanAnterior = new Ext.form.FieldSet(
        {
            title : 'Dades Estudi Anterior',
            autoHeight : true,
            width: 800,
            items : [
            {
                layout : 'table',
                layoutConfig :
                {
                    columns : 3
                },
                padding : '5px 0px',
                defaults :
                {
                    bodyStyle : 'padding:0px 10px 0px 0px',
                    layout : 'form'
                },
                items : [
                    {
                        items : [ this.numberFieldCodigoEstudioUjiAnt ]
                    },
                    {
                        items : [ this.numberFieldCodigoEstudioAnt ]
                    },
                    {
                        items : [ this.textFieldEstudioAnt ]
                    },
                    {
                        items : [ this.numberFieldCreditosEstudioAnt ]
                    },
                    {
                        colspan: 2,
                        items : [ this.numberFieldPlanAnteriorNumCursos ]
                    },
                    {
                        items : [ this.textFieldCodigoCentroAnt ]
                    },
                    {
                        colspan: 2,
                        items : [ this.textFieldNombreCentroAnt ]
                    }
                ]
            } ]
        });
    },

    buildNumberFieldCodigoEstudioAnt : function()
    {
        this.numberFieldCodigoEstudioAnt  = new Ext.form.NumberField(
            {
                width : 80,
                fieldLabel : 'Codi Ruct',
                name : 'codigoEstudioAnt',
                allowDecimals : false,
                readOnly : false
            });
    },

    buildNumberFieldCodigoEstudioUjiAnt : function()
    {
        this.numberFieldCodigoEstudioUjiAnt  = new Ext.form.NumberField(
            {
                width : 60,
                fieldLabel : 'Codi Uji',
                name : 'codigoEstudioUjiAnt',
                allowDecimals : false,
                readOnly : false
            });
    },

    buildTextFieldCodigoCentroAnt : function()
    {
        this.textFieldCodigoCentroAnt = new Ext.form.TextField(
            {
                width : 80,
                fieldLabel : 'Codi centre',
                name : 'codigoCentroAnt'
            });
    },

    buildTextFieldNombreCentroAnt : function()
    {
        this.textFieldNombreCentroAnt = new Ext.form.TextField(
            {
                width : 445,
                fieldLabel : 'Nom centre',
                name : 'nombreCentroAnt'
            });
    },

    buildTextFieldEstudioAnt : function()
    {
        this.textFieldEstudioAnt = new Ext.form.TextField(
        {
            width : 250,
            fieldLabel : 'Nom',
            name : 'estudioAnt',
            readOnly : false
        });
    },

    buildNumberFieldCreditosEstudioAnt : function()
    {
        this.numberFieldCreditosEstudioAnt = new Ext.form.NumberField(
        {
            width : 60,
            fieldLabel : 'Crèdits',
            name : 'creditosEstudioAnt',
            allowDecimals : true,
            decimalSeparator : ',',
            readOnly : false
        });
    },

    buildNumberFieldPlanAnteriorNumCursos : function()
    {
        this.numberFieldPlanAnteriorNumCursos = new Ext.form.NumberField(
        {
            width : 40,
            fieldLabel : 'Cursos',
            name : 'planAnteriorNumCursos',
            readOnly : false
        });
    }
});