Ext.ns('UJI.BEC');

UJI.BEC.HistoricoTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Històric',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.HistoricoTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildFormObservaUji();
        this.buildStoreHistoricoObserva();
        this.buildStoreHistoricoEstados();
        this.buildGridHistoricoObserva();
        this.buildGridHistoricoEstados();

        this.add(this.formObservaUji);
        this.add(this.gridHistoricoObserva);
        this.add(this.gridHistoricoEstados);

        this.formObservaUji.getForm().load(
        {
            url : '/bec/rest/beca/' + this.becaId,
            waitMsg : 'Per favor espere...',
            waitTitle : 'Carregant formulari',
            method : 'GET'
        });
    },

    buildFormObservaUji : function()
    {
        var ref = this;

        this.botonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                ref.formObservaUji.getForm().submit(
                {
                    url : '/bec/rest/beca/' + ref.becaId + '/observaciones',
                    method : 'put',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Guardant Dades',
                    failure : function(form, action)
                    {
                        alert('Error al guardar les dades.');
                    }
                });
            }
        });

        this.formObservaUji = new Ext.form.FormPanel(
        {
            width : 750,
            reader : new Ext.data.XmlReader(
            {
                record : 'Beca',
                id : 'id'
            }, [ 'id', 'observacionesUji' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            padding : '5px 0px',
            labelAlign : 'top',
            items : [
            {
                xtype : 'textfield',
                hidden : true,
                name : 'id'
            },
            {
                xtype : 'textarea',
                fieldLabel : 'Observacions UJI',
                width : 750,
                height : 80,
                name : 'observacionesUji'
            } ],
            buttons : [ this.botonGuardar ]
        });

    },

    cargaObservacion : function()
    {
        var id = this.gridHistoricoObserva.selModel.getSelected().id;

        var f = this.formObservacion.getForm();
        f.load(
        {
            url : '/bec/rest/beca/' + id,
            method : 'get'
        });
    },

    loadStore: function() {
        this.storeHistoricoEstados.reload();
        this.storeHistoricoObserva.reload();
    },

    buildStoreHistoricoObserva : function()
    {
        this.storeHistoricoObserva = this.getStore(
        {
            url : '/bec/rest/beca/' + this.becaId + '/observaciones',
            record : 'RecordUI',
            fieldList : [ 'convocatoria', 'cursoAcademico', 'observaciones' ]
        });
    },

    buildGridHistoricoObserva : function()
    {
        var ref = this;

        this.gridHistoricoObserva = new Ext.grid.GridPanel(
        {
            title : 'Històric d\'observacions',
            store : this.storeHistoricoObserva,
            frame : true,
            autoScroll : true,
            height : 150,
            viewConfig :
            {
                forceFit : true
            },
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Curs Acadèmic',
                    dataIndex : 'cursoAcademico',
                    width : 30
                },
                {
                    header : 'Convocatòria',
                    dataIndex : 'convocatoria',
                    width : 40
                },
                {
                    header : 'Observacions UJI',
                    dataIndex : 'observaciones',
                    width : 200
                } ]
            }),
            listeners :
            {
                rowdblclick : function(grid, rowIndex, event)
                {
                    ref.formObservacion = new Ext.FormPanel(
                    {
                        frame : true,
                        flex : 1,
                        name : 'observaciones',
                        labelAlign : 'top',
                        url : '/gdo/rest/beca/',
                        reader : new Ext.data.XmlReader(
                        {
                            record : 'Beca',
                            id : 'id'
                        }, [ 'id', 'observacionesUji' ]),
                        items : [
                        {
                            xtype : 'textarea',
                            name : 'observacionesUji',
                            width : 450,
                            height : 140,
                            readOnly : true
                        } ],
                        buttonAlign : 'center'
                    });

                    ventanaModal = new Ext.Window(
                    {
                        title : 'Observació',
                        layout : 'fit',
                        modal : true,
                        width : 500,
                        height : 250,
                        plain : true,
                        items : new Ext.Panel(
                        {
                            layout : 'vbox',
                            layoutConfig :
                            {
                                align : 'stretch'
                            },
                            items : [ ref.formObservacion ]
                        })
                    });

                    ventanaModal.show();
                    ref.cargaObservacion();
                }
            }
        });
    },

    buildStoreHistoricoEstados : function()
    {
        this.storeHistoricoEstados = this.getStore(
        {
            url : '/bec/rest/beca/' + this.becaId + '/estados',
            record : 'HistoricoBeca',
            fieldList : [
            {
                name : 'fecha',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }, 'usuario', 'convocatoria', 'proceso', 'estado', 'tanda', {
                name : 'fechaNotificacion',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }, 'concedida', 'reclamada' ]
        });
    },

    buildGridHistoricoEstados : function()
    {
        this.gridHistoricoEstados = new Ext.grid.GridPanel(
        {
            title : 'Històric estats Beca',
            store : this.storeHistoricoEstados,
            frame : true,
            autoScroll : true,
            height : 150,
            viewConfig :
            {
                forceFit : true
            },
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Data',
                    dataIndex : 'fecha',
                    width : 30,
                    xtype : 'datecolumn',
                    format : 'd/m/Y H:i:s'
                },
                {
                    header : 'Usuari',
                    dataIndex : 'usuario',
                    width : 30
                },
                {
                    header : 'Convocatòria',
                    dataIndex : 'convocatoria',
                    width : 30
                },
                {
                    header : 'Procés',
                    dataIndex : 'proceso',
                    width : 30
                },
                {
                    header : 'Estat',
                    dataIndex : 'estado',
                    width : 30
                },
                {
                    xtype: 'booleancolumn',
                    header : 'Concedida',
                    dataIndex : 'concedida',
                    trueText : 'Si',
                    falseText : 'No',
                    width : 30
                },
                {
                    xtype: 'booleancolumn',
                    header : 'Reclamada',
                    dataIndex : 'reclamada',
                    trueText : 'Si',
                    falseText : 'No',
                    width : 30
                },
                {
                    header : 'Tanda',
                    dataIndex : 'tanda',
                    width : 30
                },
                {
                    header : 'Data Notificació',
                    dataIndex : 'fechaNotificacion',
                    width : 30,
                    xtype : 'datecolumn',
                    format : 'd/m/Y H:i:s'
                }]
            })
        });
    }
});
