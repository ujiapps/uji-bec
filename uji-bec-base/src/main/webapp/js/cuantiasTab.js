Ext.ns('UJI.BEC');

UJI.BEC.CuantiasTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Quanties',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.CuantiasTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRowEditor();

        this.buildStoreCuantiasBecas();
        this.buildStoreCuantias();
        this.buildComboCuantias();
        this.buildButtonAnadir();
        this.buildButtonBorrar();
        this.buildButtonAyudas();

        this.buildGridCuantias();
        this.add(this.gridCuantias);

        //this.addEvents('UJI.BEC.CuantiasTab.botonCuantias.click');
    },

    loadStore: function() {
        syncStoreLoad([ this.storeCuantias, this.storeCuantiasBecas ]);
    },

    buildRowEditor : function()
    {
        var ref = this;

        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desa',
            cancelText : 'Cancel·la',
            errorSummary : false,
            focusDelay : 0,
            floating :
            {
                zindex : 7000
            },
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    if (rowIndex == 0)
                    {
                        ref.gridCuantias.getSelectionModel().selectFirstRow();
                        if (ref.gridCuantias.getSelectionModel().getSelected().dirty)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
        });
    },

    buildStoreCuantiasBecas : function()
    {
        this.storeCuantiasBecas = this.getStore(
        {
            url : '/bec/rest/beca/' + this.becaId + '/cuantia',
            record : 'CuantiaBeca',
            id : 'id',
            fieldList : [ 'id', 'becaId', 'cuantiaId', 'codigo', 'importe' ]
        });
    },

    buildStoreCuantias : function()
    {
        this.storeCuantias = this.getStore(
        {
            url : '/bec/rest/beca/' + this.becaId + '/cuantiacursoconvocatoria',
            record : 'CuantiaCurso',
            id : 'cuantiaId',
            fieldList : [ 'cuantiaId', 'nombre', 'codigo', 'importe' ]
        });
    },

    buildComboCuantias : function()
    {
        var ref = this;
        this.comboCuantias = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombre',
            valueField : 'cuantiaId',
            store : this.storeCuantias,
            listeners :
            {
                select : function(combo, record, index)
                {
                    var importe = record.data.importe;
                    posicion = ref.gridCuantias.getColumnModel().findColumnIndex('importe');
                    ref.gridCuantias.getColumnModel().getColumnAt(posicion).getEditor().setValue(importe);
                }
            }
        });
    },

    buildButtonAnadir : function()
    {
        var ref = this;
        this.buttonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                var rec = new ref.gridCuantias.store.recordType(
                {
                    id : '',
                    cuantiaId : '',
                    becaId : ref.becaId,
                    codigo : '',
                    importe : ''
                });

                ref.editor.stopEditing();
                ref.gridCuantias.store.insert(0, rec);
                ref.editor.startEditing(0, true);
            }
        });
    },

    buildButtonBorrar : function()
    {
        var ref = this;
        this.buttonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(button, event)
            {
                var rec = ref.gridCuantias.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                Ext.MessageBox.show(
                {
                    title : 'Esborrar quantia?',
                    msg : 'Atenció!\n ' + 'vols esborrar la quantia seleccionada?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.gridCuantias.store.remove(rec);
                        }
                    }
                });
            }
        });
    },
    buildButtonAyudas : function()
    {
        var ref = this;
        this.buttonAyudas = new Ext.Button(
        {
            text : 'Ajudes',
            iconCls : 'money-add',
            width : 80,
            style :
            {
                marginLeft : '550px'
            },
            handler : function(button, event)
            {
                var becaId = ref.becaId;
                Ext.Ajax.request(
                {
                    url : '/bec/rest/beca/' + becaId + '/propuestaayuda',
                    method : 'post',
                    success : function(result, request)
                    {
                        ref.storeCuantiasBecas.reload();
                        //ref.fireEvent('UJI.BEC.CuantiasTab.botonCuantias.click');
                    },
                    failure : function(result, request)
                    {
                        ref.storeCuantiasBecas.reload();
                    }
                });
            }
        });
    },

    buildGridCuantias : function()
    {
        var ref = this;
        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };
        this.gridCuantias = new Ext.grid.GridPanel(
        {
            store : this.storeCuantiasBecas,
            frame : true,
            autoScroll : true,
            height : 400,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,
            plugins : [ this.editor ],
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Beca',
                    dataIndex : 'becaId',
                    hidden : true
                },
                {
                    header : 'Codi',
                    dataIndex : 'codigo',
                    width : 40

                },
                {
                    header : 'Quantia',
                    dataIndex : 'cuantiaId',
                    editor : ref.comboCuantias,
                    renderer : Ext.util.Format.comboRenderer(ref.comboCuantias)
                },
                {
                    header : 'Import',
                    dataIndex : 'importe',
                    renderer : function(v)
                    {
                        v = (Math.round((v - 0) * 100)) / 100;
                        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
                        v = String(v);
                        return v.replace(/\./, ',');
                    },
                    width : 40,
                    editor : new Ext.form.NumberField(
                    {
                        readOnly : false,
                        decimalSeparator : ',',
                        allowDecimals : true,
                        allowNegative : false
                    })
                } ]
            }),
            tbar :
            {
                items : [ this.buttonAnadir, this.buttonBorrar, this.buttonAyudas ]
            }
        });
    }
});