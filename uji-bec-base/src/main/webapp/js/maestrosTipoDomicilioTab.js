Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoDomicilio = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title: 'Domicilis',
    url: '/bec/rest/tipodomicilio',
    record: 'TipoDomicilio'

});