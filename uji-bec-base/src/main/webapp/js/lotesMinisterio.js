Ext.ns('UJI.BEC');

UJI.BEC.LotesMinisterio = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Lotes Ministerio',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.LotesMinisterio.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreCursoAcademico();
        this.buildStoreListaLotes();

        this.buildComboCursoAcademico();

        this.buildGridComboCursoAcademico();
        this.buildGridComboConvocatoria();

        this.buildButtonBuscar();
        this.buildButtonLimpiarFiltros();
        this.buildButtonProcesarLote();

        this.buildPanelFiltros();
        this.buildPanelGrid();
    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico/',
            record : 'CursoAcademico',
            autoLoad : true,
            id : 'id',
            fieldList : [ 'id', 'activo' ]
        });
    },

    buildStoreListaLotes : function()
    {
        var barraProgreso = new Object();

        this.storeListaLotes = new Ext.data.Store(
        {
            restful : true,
            url : '/bec/rest/loteMinisterio',
            reader : new Ext.data.XmlReader(
            {
                record : 'LoteMinisterio',
                idProperty : 'id'
            }, [ 'id',
            {
                name : 'loteId',
                type : 'int'
            }, 'cursoAcademicoId', 'convocatoriaId',
            {
                name : 'fecha',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'multienvioId',
                type : 'int'
            }, 'estado' ]),
            writer : new Ext.ux.uji.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'id', 'loteId', 'cursoAcademicoId', 'convocatoriaId', 'fecha', 'multienvioId', 'estado' ]),
            listeners :
            {
                beforesave : function(store, data)
                {
                    barraProgreso = Ext.MessageBox.show(
                    {
                        msg : 'Guardant dades, per favor espera...',
                        width : 300,
                        wait : true,
                        waitConfig :
                        {
                            interval : 100
                        }
                    });

                },
                save : function(store, batch, data)
                {
                    barraProgreso.hide();
                }
            }
        });
    },

    buildComboCursoAcademico : function()
    {
        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            fieldLabel : 'Curs',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 80,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            store : this.storeCursoAcademico
        });
    },

    buildGridComboCursoAcademico : function()
    {
        this.comboGridCursoAcademico = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 80,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            store : this.storeCursoAcademico
        });
    },

    buildGridComboConvocatoria : function()
    {
        this.comboGridConvocatoria = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildButtonBuscar : function()
    {
        var ref = this;
        this.buttonBuscar = new Ext.Button(
        {
            text : 'Recerca',
            width : 80,
            iconCls : 'magnifier',
            handler : function(button, event)
            {
                ref.storeListaLotes.load(
                {
                    params : ref.panelFiltros.getForm().getFieldValues(),
                    callback : function(record, options, success)
                    {
                        if (success && this.getTotalCount() == 0)
                        {
                            Ext.Msg.show(
                            {
                                title : 'Filtres',
                                msg : 'No s\'ha trobat cap registre',
                                modal : false,
                                icon : Ext.Msg.INFO,
                                buttons : Ext.Msg.OK
                            });
                        }
                        else if (!success)
                        {
                            Ext.Msg.show(
                            {
                                title : 'Filtres',
                                msg : 'Nombre excessiu de lots. Defineix mes filtres',
                                modal : false,
                                icon : Ext.Msg.INFO,
                                buttons : Ext.Msg.OK
                            });
                        }
                    }
                });
            }
        });
    },

    buildButtonLimpiarFiltros : function()
    {
        var ref = this;
        this.buttonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            margins : '0 0 0 25px',
            handler : function(button, event)
            {
                ref.panelFiltros.getForm().reset();
            }
        });
    },

    buildButtonProcesarLote : function()
    {
        var ref = this;
        this.buttonProcesarLote = new Ext.Button(
        {
            text : 'Processar Lot',
            iconCls : 'email',
            disabled : true,
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                loteId = rec.get('id');
                ref.procesarLoteMinisiterio(loteId);
            }
        });
    },

    procesarLoteMinisterio : function(loteId)
    {
        Ext.Msg.show(
        {
            title : 'Resultats',
            msg : 'Processar lot',
            modal : false,
            icon : Ext.Msg.INFO,
            buttons : Ext.Msg.OK
        });
    },

    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Lots Ministeri',
            layout : 'table',
            frame : true,
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.comboCursoAcademico ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonLimpiarFiltros ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonBuscar ]
            } ]
        });
    },

    buildPanelGrid : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.panelGrid = new Ext.grid.GridPanel(
        {
            store : ref.storeListaTandas,
            frame : true,
            autoScroll : true,
            flex : 1,
            loadMask : true,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true,
                listeners :
                {
                    rowselect : function(sm, row, rec)
                    {
                        ref.habilitaOdeshabilitaBotones(rec);
                    }
                }
            }),
            plugins : [ this.editor ],
            viewConfig :
            {
                forceFit : true
            },
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Lot',
                    width : 40,
                    dataIndex : 'loteId',
                    editor : new Ext.form.NumberField(
                    {
                        allowDecimals : false
                    })
                },
                {
                    header : 'Curs',
                    id : 'cursoAca',
                    width : 50,
                    sortable : true,
                    dataIndex : 'cursoAcademicoId',
                    editor : ref.comboGridCursoAcademico
                },
                {
                    header : 'Convocatoria',
                    width : 80,
                    dataIndex : 'convocatoriaId',
                    editor : ref.comboGridConvocatoria,
                    renderer : Ext.util.Format.comboRenderer(ref.comboGridConvocatoria)
                },
                {
                    header : 'Data recepció',
                    width : 95,
                    dataIndex : 'fecha',
                    xtype : 'datecolumn',
                    format : 'd/m/Y',
                    editor : new Ext.form.DateField(
                    {
                        startDay : 1,
                        format : 'd/m/Y'
                    })
                },
                {
                    header : 'Multienvio',
                    width : 40,
                    dataIndex : 'multienvioId',
                    editor : new Ext.form.NumberField(
                    {
                        allowDecimals : false
                    })
                },
                {
                    header : 'Estat',
                    width : 100,
                    dataIndex : 'estado',
                    xtype : 'textfield'
                } ]
            }),
            tbar :
            {
                items : [ this.buttonProcesar ]
            }
        });
    }
});