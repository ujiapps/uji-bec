Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosDenegacion = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Denegaciones',
    layout : 'form',
    closable : false,
    autoScroll : true,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosDenegacion.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreDenegacion();
        this.buildStoreOrganismo();

        this.buildCheckBoxActiva();
        this.buildComboOrganismo();

        this.buildEditor();

        this.buildAddButton();
        this.buildDeleteButton();

        this.buildGridDenegacion();

        this.add(this.gridDenegacion);
    },

    loadData: function() {
        this.storeDenegacion.reload();
        this.storeOrganismo.reload();
    },

    buildStoreDenegacion : function()
    {
        this.storeDenegacion = this.getStore(
            {
                url : '/bec/rest/denegacion',
                record : 'Denegacion',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'},
                              'nombre', 'organismoId', 'activa', 'estado', 'causa', 'subCausa' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    buildStoreOrganismo : function()
    {
        this.storeOrganismo = this.getStore(
            {
                url : '/bec/rest/organismo',
                record : 'Organismo',
                autoLoad : false,
                id : 'id',
                fieldList : [ 'id', 'nombre' ]
            });
    },

    buildCheckBoxActiva : function()
    {
        this.checkBoxActiva = new Ext.form.Checkbox(
            {
                name : 'activa',
                width : 80,
                fieldLabel : 'Activa'
            });
    },

    buildComboOrganismo : function()
    {
        this.comboOrganismo = new Ext.form.ComboBox(
            {
                triggerAction : 'all',
                editable : false,
                forceSelection : true,
                width : 80,
                name : 'organismoId',
                valueField : 'id',
                displayField : 'nombre',
                store : this.storeOrganismo
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var DenegacionSeleccionado = ref.gridDenegacion.getSelectionModel().getSelected();
                        if (!DenegacionSeleccionado)
                        {
                            return false;
                        }
                        ref.storeDenegacion.remove(DenegacionSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroDenegacion = new ref.storeDenegacion.recordType({});
                ref.editor.stopEditing();
                ref.storeDenegacion.insert(0, registroDenegacion);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridDenegacion;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridDenegacion : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 35,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
                {
                header : 'Organismo',
                width : 50,
                dataIndex : 'organismoId',
                editor : ref.comboOrganismo,
                renderer : ref.formatComboRenderer(ref.comboOrganismo)
            },
            {
                header : 'Estat',
                width : 35,
                hidden : false,
                dataIndex : 'estado',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Causa',
                width : 35,
                hidden : false,
                dataIndex : 'causa',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Subcausa',
                width : 35,
                hidden : false,
                dataIndex : 'subCausa',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
                {
                header : 'Nom',
                width : 250,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Activa',
                dataIndex : 'activa',
                xtype : 'booleancolumn',
                trueText : 'Sí',
                falseText : 'No',
                width : 40,
                editor : ref.checkBoxActiva
            } ]
        });

        this.gridDenegacion = new Ext.grid.GridPanel(
        {
            store : this.storeDenegacion,
            frame : true,
            autoScroll : true,
            width : 700,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});