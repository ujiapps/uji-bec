Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoVia = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Vias',
    url : '/bec/rest/tipovia',
    record : 'TipoVia'
});