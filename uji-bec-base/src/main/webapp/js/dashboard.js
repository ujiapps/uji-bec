function initBECDashboard()
{
    var ref = this;

    var storeCursoAcademico = new Ext.data.Store(
    {
        url : '/bec/rest/cursoacademico',
        record : 'CursoAcademico',
        id : 'id',
        restful : true,
        reader : new Ext.data.XmlReader(
        {
            record : 'CursoAcademico',
            id : 'id'
        }, [ 'id', 'activo' ]),

        fieldList : [ 'id', 'activo' ]
    });

    var comboCursoAcademico = new Ext.form.ComboBox(
    {
        triggerAction : 'all',
        fieldLabel : 'Curs Acadèmic',
        fieldLabel : 'Curs Acadèmic',
        width : 120,
        valueField : 'id',
        displayField : 'id',
        value : cursoAcademicoActivoId,
        store : storeCursoAcademico,
        listeners :
        {
            select : function()
            {
                var c = Ext.ComponentMgr.get('panelGrids');
                panelResumen.getEl().mask("Carregant dades...", "x-loading-mask");
                panelResumen.remove(c);
                createPanelGridsContadorBecas(panelResumen, comboCursoAcademico.getValue());
            }
        }
    });

    var storeOrganismoConvocatoria = new Ext.data.Store(
    {
        proxy : new Ext.data.HttpProxy(
        {
            url : '/bec/rest/becaRecuento/' + cursoAcademicoActivoId

        }),
        record : 'OrganismoConvocatoria',
        restful : true,
        reader : new Ext.data.XmlReader(
        {
            record : 'OrganismoConvocatoria',
            id : 'id'
        }, [ 'organismo', 'organismoId', 'convocatoria', 'convocatoriaId' ]),
        fieldList : [ 'organismo', 'organismoId', 'convocatoria', 'convocatoriaId' ],
        listeners :
        {
            beforeload : function(editor, index)
            {
                this.proxy.setUrl('/bec/rest/becaRecuento/' + comboCursoAcademico.getValue());
            }
        }
    });

    var existeColumna = function(cm, column)
    {

        for ( var i in cm)
        {
            if (cm.hasOwnProperty(i))
            {
                if (cm[i].dataIndex === column)
                {
                    return true;
                }
            }
        }
        return false;
    }

    var validColumns = [ "Baixa", "Concedida", "Denegada", "DenegadaUJI", "Enviada", "Exclosa", "Incorrecta", "Pendent", "Traslladada", "Treballada", "RevocVoluntaries", "RevocCredits", "DocPendent", "DocEnviada", "DocCorrecta"  ];
    var generateColumnModel = function(storeContadorBecas)
    {
        var cm = [
        {
            header : 'Concedida',
            dataIndex : 'becaConcedida',
            width : 70,
            align : 'center'
        },
        {
            header : 'Procés',
            dataIndex : 'proceso',
            width : 110
        } ];

        var records = storeContadorBecas.data.items;
        for ( var i in records)
        {
            if (records.hasOwnProperty(i))
            {
                var record = records[i];
                for (var j = 0; j < validColumns.length; j++)
                {
                    var column = validColumns[j];
                    var value = record.data[column];
                    if (value > 0 && !existeColumna(cm, column))
                    {
                        cm.push(
                        {
                            header : column,
                            dataIndex : column,
                            type : 'Integer',
                            align : 'right',
                            width : 85
                        })
                    }
                }
            }
        }
        cm.push(
        {
            header : '',
            dataIndex : false
        })
        return cm;
    };

    var createGridContadorBecas = function(panelGrid, cursoAcademicoId, organismoId, convocatoriaId)
    {
        var storeContadorBecas = new Ext.data.Store(
        {
            url : '/bec/rest/becaRecuento',
            record : 'CursoAcademico',
            id : 'id',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'BecaRecuento',
                id : '@cursoAcademicoId+@organismoId+@convocatoriaId'
            }, [ 'cursoAcademicoId', 'convocatoriaId', 'procesoId', 'convocatoria', 'proceso', 'Exclosa', 'Baixa', 'Traslladada', 'RevocVoluntaries', 'RevocCredits', 'Treballada', 'Incorrecta', 'organismo',
                    'organismoId', 'Pendent', 'becaConcedida', 'Concedida', 'DenegadaUJI', 'Enviada', 'Denegada', 'DocPendent', 'DocEnviada', 'DocCorrecta' ]),

            fieldList : [ 'cursoAcademicoId', 'convocatoriaId', 'procesoId', 'convocatoria', 'proceso', 'Exclosa', 'Baixa', 'Traslladada', 'RevocVoluntaries', 'RevocCredits', 'Treballada', 'Incorrecta', 'organismo',
                    'organismoId', 'Pendent', 'becaConcedida', 'Concedida', 'DenegadaUJI', 'Enviada', 'Denegada', 'DocPendent', 'DocEnviada', 'DocCorrecta' ],
            baseParams :
            {
                cursoAcademicoId : cursoAcademicoId,
                organismoId : organismoId,
                convocatoriaId : convocatoriaId
            }
        });

        storeContadorBecas.sort([
        {
            field : "becaConcedida",
            direction : "DESC"
        },
        {
            field : "procesoId",
            direction : "ASC"
        } ]);

        var columns = generateColumnModel(storeContadorBecas);
        storeContadorBecas.load(
        {
            params :
            {
                cursoAcademicoId : cursoAcademicoId,
                organismoId : organismoId,
                convocatoriaId : convocatoriaId
            },
            callback : function(records)
            {
                var columns = generateColumnModel(storeContadorBecas);
                var gridBecas = new Ext.grid.GridPanel(
                {
                    frame : true,
                    autoHeight : true,
                    columnLines : true,
                    store : storeContadorBecas,
                    columns : columns,
                    enableHdMenu : false,
                    enableColumnResize : false,
                    enableColumnMove : false,
                    enableColumnHide : false
                });
                panelGrid.add(gridBecas);
                panelGrid.doLayout(false, true);
            }
        });
    };

    var createPanelGridsContadorBecas = function(panelResumen, cursoAcademicoId)
    {
        storeOrganismoConvocatoria.load(
        {
            callback : function(records)
            {
                var panelGrids = new Ext.Panel(
                {
                    flex : 1,
                    autoHeight : true,
                    autoScroll : true,
                    name : 'panelGrids',
                    id: 'panelGrids'
                });

                for (var i = 0; i < records.length; i++)
                {
                    var record = records[i];
                    var organismo = record.data.organismo;
                    var organismoId = record.data.organismoId;
                    var convocatoria = record.data.convocatoria;
                    var convocatoriaId = record.data.convocatoriaId;

                    var panelGrid = new Ext.Panel(
                    {
                        flex : 1
                    });

                    var label = new Ext.form.Label(
                    {
                        html : '<h2 style="font-size: 1.8em; margin: 1em 0;">' + organismo + '-' + convocatoria + '</h2>'
                    });

                    panelGrid.add(label);
                    createGridContadorBecas(panelGrid, cursoAcademicoId, organismoId, convocatoriaId);
                    panelGrids.add(panelGrid);
                }
                panelResumen.add(panelGrids);
                panelResumen.doLayout(false, true);
                panelResumen.getEl().unmask();
            }
        });
    }

    var panelResumen = new Ext.FormPanel(
    {
        title : 'Dashboard',
        frame : true,
        padding : '5',
        title : 'Resum de beques',
        height: 800,
        autoHeight: false,
        autoScroll : true,
        items : [
                {
                    xtype : 'label',
                    width : '100%',
                    text : 'Aplicació de Beques',
                    style : 'font-size : 30px; font-weight : bold; margin-top : 30px;'
                },
                {
                    xtype : 'label',
                    width : '100%',
                    text : 'Versió ' + appversion,
                    style : 'font-size : 12px; display: block; margin-bottom: 2em;'
                }, comboCursoAcademico ],
        listeners: {
            afterrender: function() {
                panelResumen.getEl().mask("Carregant dades...");
            }
        }
    });

    createPanelGridsContadorBecas(panelResumen, cursoAcademicoActivoId);
    return panelResumen;
}