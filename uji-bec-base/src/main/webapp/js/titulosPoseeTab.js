Ext.ns('UJI.BEC');

UJI.BEC.TitulosPoseeTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Títols',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TitulosPoseeTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildTitulosPoseeStore();
        this.buildPanelFormulario();
        this.buildPanelGridTitulosPosee();
        this.add(this.panelGridTitulosPosee);
        this.add(this.panelFormulario);
    },

    buildTitulosPoseeStore : function()
    {
        this.storeTitulosPosee = new Ext.data.Store(
        {
            baseParams :
            {
                solicitanteId : this.solicitanteId
            },
            restful : true,
            autoLoad : true,
            url : '/bec/rest/tituloPosee',
            reader : new Ext.data.XmlReader(
            {
                record : 'TituloPosee',
                id : 'id'
            }, [ 'id', 'codigoTitulo', 'solicitanteId', 'nombreTitulo', 'secTitulo', 'nombreNivelTipo', 'codigoNivelTipo', 'nombreTituloLargo' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    loadData : function(id)
    {
        var record = this.storeTitulosPosee.getById(id);
        this.panelFormulario.getForm().loadRecord(record);
    },

    buildPanelGridTitulosPosee : function()
    {
        var ref = this;

        this.panelGridTitulosPosee = new Ext.grid.GridPanel(
        {
            store : ref.storeTitulosPosee,
            height : 120,
            flex : 1,
            frame : true,
            autoScroll : true,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,

            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Nom tipus nivell',
                    width : 30,
                    dataIndex : 'nombreNivelTipo'
                },
                {
                    header : 'Nom Títol',
                    width : 100,
                    dataIndex : 'nombreTitulo'
                } ]
            }),
            listeners :
            {
                rowclick : function(grid, rowIndex, event)
                {
                    var id = ref.panelGridTitulosPosee.selModel.getSelected().data.id;

                    ref.loadData(id);
                }
            }
        });
    },

    buildPanelFormulario : function()
    {
        this.panelFormulario = new Ext.FormPanel(
        {
            layout : 'table',
            frame : true,
            height : 450,
            flex : 1,
            style : 'margin-right:16px;',
            labelAlign : 'top',
            reader : new Ext.data.XmlReader(
            {
                record : 'TituloPosee',
                id : 'id'
            }, [ 'id', 'codigoTitulo', 'solicitanteId', 'nombreTitulo', 'secTitulo', 'nombreNivelTipo', 'codigoNivelTipo', 'nombreTituloLargo' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 2
            },
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            items : [
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    width : 150,
                    fieldLabel : 'Codi de títol',
                    name : 'codigoTitulo'

                } ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    width : 400,
                    fieldLabel : 'Nom de títol',
                    name : 'nombreTitulo'

                } ]
            },
            {
                layout : 'form',
                colspan: 2,
                items : [
                {
                    xtype : 'textfield',
                    width : 570,
                    fieldLabel : 'Nom de títol llarg',
                    name : 'nombreTituloLargo'

                } ]
            },

            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    width : 150,
                    fieldLabel : 'Codi tipus nivell',
                    name : 'codigoNivelTipo'

                } ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    width : 400,
                    fieldLabel : 'Nom tipos nivell',
                    name : 'nombreNivelTipo'

                } ]
            } ]
        });
    }
});