Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoIdentificacion = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Identificacions',
    url : '/bec/rest/tipoidentificacion',
    record : 'TipoIdentificacion'
});