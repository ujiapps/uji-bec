Ext.ns('UJI.BEC');

UJI.BEC.OtrosDatosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Altres Dades',
    layout : 'form',
    closable : false,
    autoScroll : true,
    frame : true,
    padding : '5px 0px',

    formPanel : {},
    enlacesExternos : {},
    botonGuardar : {},
    fieldTelefono1 : {},
    fieldTelefono2 : {},
    fieldEmail : {},
    fieldEntidad : {},
    fieldSucursal : {},
    fieldDigitoControl : {},
    fieldNumeroCuenta : {},
    fieldCuentaBancaria : {},
    fieldIBAN : {},
    fieldObservaciones : {},
    personaId : '',
    estudioId : '',
    session : '',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.OtrosDatosTab.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.formPanel);
        this.add(this.enlacesExternos);

        var ref = this;
        this.formPanel.getForm().load(
        {
            url : '/bec/rest/solicitante/' + this.solicitanteId + '/beca/' + this.becaId + '/otrosdatos',
            waitMsg : 'Per favor epere ...',
            waitTitle : 'Carregant formulari',
            method : 'GET',
            success : function(form, action)
            {
                var solicitante = Ext.DomQuery.selectNode('Solicitante', action.response.responseXML);
                ref.personaId = Ext.DomQuery.selectValue('personaId', solicitante);
                ref.estudioId = Ext.DomQuery.selectValue('estudioId', solicitante);
                ref.session = Ext.DomQuery.selectValue('session', solicitante);
            }
        });
    },
    initUI : function()
    {
        this.buildForm();
        this.buildEnlacesExternos();
    },

    buildEnlacesExternos : function()
    {
        var ref = this;

        var horario = new Ext.Button(
        {
            text : 'Horari del alumne',
            handler : function()
            {
                if (ref.estudioId && ref.personaId && ref.estudioId > 0 && ref.estudioId <= 9999)
                {
                    var url = 'https://e-ujier.uji.es/pls/www/!gri_www.euji22956?p_persona=' + ref.personaId + '&p_estudio=' + ref.estudioId;
                    window.open(url);
                }
                else
                {
                    alert('La beca no tiene un estudio asociado');
                }
            }
        });

        var certificadoAcademico = new Ext.Button(
        {
            text : 'Certificat Acadèmic',
            handler : function()
            {
                if (ref.estudioId && ref.personaId)
                {
                    if (ref.estudioId >= 1 && ref.estudioId <= 200)
                    {
                        var url = 'https://e-ujier.uji.es/cocoon/CA/' + ref.session + '/' + ref.personaId + '/' + ref.estudioId + '/P/S/expediente.pdf';
                        window.open(url);
                    }
                    else if (ref.estudioId >= 201 && ref.estudioId <= 9999)
                    {
                        var url = 'https://e-ujier.uji.es/cocoon/CA/' + ref.session + '/' + ref.personaId + '/' + ref.estudioId + '/P/S/expediente-grado.pdf';
                        window.open(url);
                    }
                    else if ((ref.estudioId >= 42000 && ref.estudioId <= 50000) || (ref.estudioId >= 55000 && ref.estudioId <= 59999))
                    {
                        var url = 'http://e-ujier.uji.es/cocoon/POP/CA/' + ref.session + '/' + ref.personaId + '/' + ref.estudioId + '/expediente.pdf';
                        window.open(url);
                    }
                }
                else
                {
                    alert('La beca no tiene un estudio asociado');
                }
            }
        });
        var rendimientoAcademico = new Ext.Button(
        {
            text : 'Rendiment Acadèmic',
            handler : function()
            {
                var url = '//e-ujier.uji.es/pls/www/!gri_www.euji23117?p_accion=S&pQuery=' + ref.personaId;
                window.open(url);
            }
        });
        var CertificadoAcademicoBecas = new Ext.Button(
                {
                    text : 'Certificat Acadèmic Becas',
                    handler : function()
                    {
                        var url = '//e-ujier.uji.es/pls/www/!gri_www.euji23123?p_accion=S&pQuery=' + ref.personaId;
                        window.open(url);
                    } 
                });
        this.enlacesExternos = new Ext.form.FieldSet(
        {
            title : 'Enllaços externs',
            autoHeight : true,
            items : [ horario, certificadoAcademico, rendimientoAcademico,CertificadoAcademicoBecas ]
        });
    },

    buildForm : function()
    {
        this.buildFieldTelefono1();
        this.buildFieldTelefono2();
        this.buildFieldEmail();
        this.buildFieldCuentaBancaria();
        this.buildFieldIBAN();
        this.buildFieldObservaciones();
        this.buildBotonGuardar();

        this.formPanel = new Ext.form.FormPanel(
        {
            title : 'Altres dades',
            layout : 'table',
            frame : true,
            flex : 1,
            reader : new Ext.data.XmlReader(
            {
                record : 'Solicitante'
            }, [ 'id', 'telefono1', 'telefono2', 'email', 'entidad', 'sucursal', 'digitosControl', 'cuentaBancaria', 'observaciones', 'estudioId', 'personaId', 'ibanPais', 'ibanDC' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:5px 10px'
            },
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                labelWidth : 100,
                items : [ this.fieldTelefono1 ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.fieldTelefono2 ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.fieldEmail ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.fieldCuentaBancaria ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.fieldIBAN ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.fieldObservaciones ]
            },
            {
                layout : 'form',
                colspan : 3,
                style : 'width:120px; margin-left:auto;',
                items : [ this.botonGuardar ]
            } ]
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 80,
            handler : function(button, event)
            {
                ref.formPanel.getForm().submit(
                {
                    url : '/bec/rest/solicitante/' + ref.solicitanteId + '/beca/' + ref.becaId + '/otrosdatos',
                    method : 'put',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Guardant Dades',
                    success : function(form, action)
                    {
                    },
                    failure : function()
                    {
                    }
                });
            }
        });
    },

    buildFieldTelefono1 : function()
    {
        this.fieldTelefono1 = new Ext.form.NumberField(
        {
            fieldLabel : 'Telèfon 1',
            name : 'telefono1',
            width : 110,
            maxLength : 20,
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildFieldTelefono2 : function()
    {
        this.fieldTelefono2 = new Ext.form.NumberField(
        {
            fieldLabel : 'Telèfon 2',
            name : 'telefono2',
            width : 110,
            maxLength : 20,
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildFieldEmail : function()
    {
        this.fieldEmail = new Ext.form.TextField(
        {
            vtype : 'email',
            fieldLabel : 'Email',
            name : 'email',
            width : 250
        });
    },

    buildFieldCuentaBancaria : function()
    {
        this.fieldCuentaBancaria = new Ext.form.CompositeField(
        {
            hideLabel : true,
            defaultMargins : '0 20 0 0',
            items : [
            {
                xtype : 'displayfield',
                value : 'Entitat',
                width : 85
            },
            {
                xtype : 'textfield',
                name : 'entidad',
                mask : '9999',
                maxLength : 4,
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                nanText : 'Sols es permeten números: {value}',
                width : 50
            },
            {
                xtype : 'displayfield',
                value : 'Sucursal',
                width : 50
            },
            {
                xtype : 'textfield',
                name : 'sucursal',
                maxLength : 4,
                mask : '9999',
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                nanText : 'Sols es permeten números: {value}',
                width : 50
            },
            {
                xtype : 'displayfield',
                value : 'DC'
            },
            {
                xtype : 'textfield',
                name : 'digitosControl',
                maxLength : 2,
                mask : '99',
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                nanText : 'Sols es permeten números: {value}',
                width : 30
            },
            {
                xtype : 'displayfield',
                value : 'Compte'
            },
            {
                xtype : 'textfield',
                name : 'cuentaBancaria',
                mask : '9999999999',
                maxLength : 10,
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                nanText : 'Sols es permeten números: {value}',
                width : 200
            } ]
        });
    },

    buildFieldIBAN : function()
    {
        this.fieldIBAN = new Ext.form.CompositeField(
        {
            hideLabel : true,
            defaultMargins : '0 20 0 0',
            items : [
            {
                xtype : 'displayfield',
                value : 'IBAN País',
                width : 85
            },
            {
                xtype : 'textfield',
                name : 'ibanPais',
                maskRe : /^([A-Z])$/,
                maxLength : 2,
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                autoCreate :
                {
                    tag : 'input',
                    type : 'text',
                    size : '2',
                    autocomplete : 'off',
                    maxlength : '2'
                },
                width : 50
            },
            {
                xtype : 'displayfield',
                value : 'IBAN DC',
                width : 50
            },
            {
                xtype : 'textfield',
                name : 'ibanDC',
                maxLength : 2,
                autoCreate :
                {
                    tag : 'input',
                    type : 'text',
                    size : '2',
                    autocomplete : 'off',
                    maxlength : '2'
                },
                maxLengthText : 'No poden haber més de {maxLength} caràcters',
                width : 50
            } ]
        });
    },

    buildFieldObservaciones : function()
    {
        this.fieldObservaciones = new Ext.form.TextArea(
        {
            fieldLabel : 'Observacions',
            name : 'observaciones',
            width : 500,
            height : 100,
            labelStyle : 'width: 100px;'
        });
    }

});