Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoPropiedad = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Propietats',
    url : '/bec/rest/tipopropiedad',
    record : 'TipoPropiedad'
});