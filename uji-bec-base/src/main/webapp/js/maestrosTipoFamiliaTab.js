Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoFamilia = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Familias',
    url : '/bec/rest/tipofamilia',
    record : 'TipoFamilia'
});