Ext.ns('UJI.BEC');

UJI.BEC.MiembrosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Membres',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,
    miembroId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.MiembrosTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreMiembros();
        this.buildStoreTipoMiembro();
        this.buildStoreTipoMinusvalia();
        this.buildStoreTipoSustentador();
        this.buildStoreTipoIdentificacion();
        this.buildStoreTipoSexo();
        this.buildStoreEstadoCivil();
        this.buildStorePais();
        this.buildStoreSituacionLaboral();
        this.buildStoreProfesion();
        this.buildStoreProvincia();
        this.buildStoreLocalidad();
        this.buildStoreComunidad();
        this.buildStoreTipoMoneda();
        this.buildStoreActividadesEconomicas();
        this.buildEditorActividadEconomica();

        this.buildNumberFieldId();
        this.buildNumberFieldSolicitanteId();
        this.buildNumberFieldBecaId();
        this.buildNumberFieldActividadEconomicaNumero();
        this.buildNumberFieldActividadEconomicaPorcentaje();
        this.buildNumberFieldActividadEconomicaImporte();
        this.buildNumberFieldPrecioAlquiler();

        this.buildTextFieldNombre();
        this.buildTextFieldApellido1();
        this.buildTextFieldApellido2();
        this.buildTextFieldIdentificacion();
        this.buildTextFieldLocalidadTrabajo();
        this.buildTextFieldIdentificacionIdesp();
        this.buildTextFieldCodigoEstudioHermano();
        this.buildTextFieldCodigoUnivHermano();
        this.buildTextFieldActividadEconomicaCif();
        this.buildTextFieldNifArren();

        this.buildNumberFieldPorcentajeActividadesEconomicas();
        this.buildNumberFieldImporteActividadesEconomicas();
        this.buildNumberFieldImporteRentasExtranjeras();

        this.buildDateFieldFechaNacimiento();
        this.buildDateFieldCaducidadNif();
        this.buildDateFieldResMinus();
        this.buildDateFieldFinMinus();

        this.buildComboTipoMiembro();
        this.buildComboSustentador();
        this.buildComboTipoMinusvalia();
        this.buildComboTipoIdentificacion();
        this.buildComboSexo();
        this.buildComboEstadoCivil();
        this.buildComboNacionalidad();
        this.buildComboSituacionLaboral();
        this.buildComboPadronProvincia();
        this.buildComboPadronLocalidad();
        this.buildComboProfesion();
        this.buildComboComunidad();
        this.buildComboTipoMoneda();

        this.buildCompositeIdentificacion();

        this.buildCheckBoxNoValidaIdentificacion();
        this.buildCheckBoxEstudiaFuera();
        this.buildCheckBoxFirmaSolicitud();
        this.buildCheckBoxResidencia();
        this.buildCheckBoxCustodia();
        this.buildCheckBoxCustodiaCompartida();
        this.buildCheckBoxIndDeclRenta();
        this.buildCheckBoxIndRentasEspanna();
        this.buildCheckBoxIndRentasNavarra();
        this.buildCheckBoxIndRentasPaisVasco();
        this.buildCheckBoxIndConvivePareja();
        this.buildCheckBoxIndIngresosPropios();
        this.buildCheckBoxIndRevMinusMec();
        this.buildCheckBoxIndRevMinusUt();
        this.buildCheckBoxIndArrenUnidFamiliar();

        this.buildBotonDelete();
        this.buildBotonInsert();
        this.buildBotonGuardar();
        this.buildBotonInsertActividadEconomica();
        this.buildBotonDeleteActividadEconomica();

        this.buildCompositeChecks();

        this.buildGridMiembros();
        this.buildPanelFormulario();
        this.buildGridActividadesEconomicas();

        this.add(this.gridMiembros);
        this.add(this.panelFormulario);
        this.add(this.gridActividadesEconomicas);

        //this.addEvents('UJI.BEC.MiembrosTab.botonGuardar.click');
    },

    loadStore: function() {
        syncStoreLoad([ this.storeTipoMiembro, this.storeTipo, this.storeTipoSustentador, this.storeTipoIdentificacion, this.storeTipoSexo, this.storeEstadoCivil,
            this.storeSituacionLaboral, this.storeProfesion, this.storeProvincia, this.storeComunidad, this.storePais, this.storeMiembros, this.storeActividadesEconomicas,
            this.storeTipoMoneda ]);

    },

    buildStoreMiembros : function()
    {
        this.storeMiembros = new Ext.data.Store(
        {
            baseParams :
            {
                becaId : this.becaId
            },
            restful : true,
            url : '/bec/rest/miembro',
            reader : new Ext.data.XmlReader(
            {
                record : 'Miembro',
                id : 'id'
            }, [ 'id', 'tipoMiembro', 'nombreMiembro', 'identificacion', 'tipoSustentador', 'padronProvinciaId', 'residencia' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonInsert : function()
    {
        var ref = this;

        this.botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            width : 60,
            iconCls : 'application-add',
            handler : function()
            {
                ref.panelFormulario.getForm().reset();
                ref.panelFormulario.focus();
            }
        });
    },

    buildBotonDelete : function()
    {
        var ref = this;

        this.botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.gridMiembros.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                Ext.MessageBox.show(
                {
                    title : 'Esborrar membre?',
                    msg : 'Atenció!\n ' + 'Vols esborrar el membre seleccionat?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.gridMiembros.store.remove(rec);
                            ref.panelFormulario.getForm().reset();
                            //ref.fireEvent('UJI.BEC.MiembrosTab.botonGuardar.click');
                        }
                    }
                });
            }
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                var id = ref.panelFormulario.getForm().getValues().id;

                if (id != '' && id != null)
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/miembro/' + id,
                        method : 'put',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.storeMiembros.reload();
                            //ref.fireEvent('UJI.BEC.MiembrosTab.botonGuardar.click');
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
                else
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/miembro/',
                        method : 'post',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            var miembro = Ext.DomQuery.selectNode('Miembro', action.response.responseXML);
                            ref.miembroId = Ext.DomQuery.selectValue('id', miembro);
                            ref.storeMiembros.reload();

                            ref.recargaFormularios();
                            //ref.fireEvent('UJI.BEC.MiembrosTab.botonGuardar.click');
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
            }
        });

    },

    buildBotonInsertActividadEconomica : function()
    {
        var ref = this;
        this.botonInsertActividadEconomica = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function(button, event)
            {
                var rec = new ref.gridActividadesEconomicas.store.recordType(
                {
                    id : '',
                    porcParticipacion : '',
                    cifSociedad : '',
                    impParticipacion : '',
                    numSociedad : ''
                });

                ref.storeActividadesEconomicas.proxy.setUrl('/bec/rest/miembro/' + ref.miembroId + '/actividadeconomica');
                ref.editorActividadEconomica.stopEditing();
                ref.gridActividadesEconomicas.store.insert(0, rec);
                ref.editorActividadEconomica.startEditing(0, true);
            }
        });
    },

    buildBotonDeleteActividadEconomica : function()
    {
        var ref = this;
        this.botonDeleteActividadEconomica = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(button, event)
            {
                var actividadEconomicaSeleccionada = ref.gridActividadesEconomicas.getSelectionModel().getSelected();
                if (!actividadEconomicaSeleccionada)
                {
                    return false;
                }

                ref.storeActividadesEconomicas.proxy.conn.url = '/bec/rest/miembro/' + ref.miembroId + '/actividadeconomica';
                ref.gridActividadesEconomicas.store.remove(actividadEconomicaSeleccionada);
            }
        });
    },

    buildStoreTipoMiembro : function()
    {
        this.storeTipoMiembro = this.getStore(
        {
            url : '/bec/rest/tipomiembro',
            record : 'TipoMiembro',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboTipoMiembro : function()
    {
        this.comboTipoMiembro = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus membre',
            triggerAction : 'all',
            editable : true,
            allowBlank : false,
            forceSelection : true,
            width : 160,
            name : 'tipoMiembroId',
            hiddenName : 'tipoMiembroId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoMiembro
        });
    },

    buildStoreTipoMinusvalia : function()
    {
        this.storeTipo = this.getStore(
        {
            url : '/bec/rest/tipominusvalia',
            record : 'TipoMinusvalia',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboTipoMinusvalia : function()
    {
        this.comboTipoMinusvalia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus minusvalidesa',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'tipoMinusvaliaId',
            hiddenName : 'tipoMinusvaliaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipo
        });
    },

    buildStoreTipoSustentador : function()
    {
        this.storeTipoSustentador = this.getStore(
        {
            url : '/bec/rest/tiposustentador',
            record : 'TipoSustentador',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboSustentador : function()
    {
        this.comboSustentador = new Ext.form.ComboBox(
        {
            fieldLabel : 'Sustentador',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            allowBlank : false,
            width : 160,
            name : 'tipoSustentadorId',
            hiddenName : 'tipoSustentadorId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoSustentador
        });
    },

    buildNumberFieldId : function()
    {
        this.numberFieldId = new Ext.form.NumberField(
        {
            name : 'id',
            hidden : true
        });
    },

    buildNumberFieldSolicitanteId : function()
    {
        this.numberFieldSolicitanteId = new Ext.form.NumberField(
        {
            name : 'solicitanteId',
            value : this.solicitanteId,
            hidden : true
        });
    },

    buildNumberFieldBecaId : function()
    {
        this.numberFieldBecaId = new Ext.form.NumberField(
            {
                name : 'becaId',
                value : this.becaId,
                hidden : true
            });
    },

    buildNumberFieldActividadEconomicaNumero : function()
    {
        this.numberFieldActividadEconomicaNumero = new Ext.form.NumberField(
        {
            name : 'numSociedad'
        });
    },

    buildTextFieldNombre : function()
    {
        this.textFieldNombre = new Ext.form.TextField(
        {
            width : 200,
            fieldLabel : 'Nom',
            name : 'nombre'
        });
    },

    buildTextFieldApellido1 : function()
    {
        this.textFieldApellido1 = new Ext.form.TextField(
        {
            width : 150,
            fieldLabel : 'Cognom1',
            name : 'apellido1'
        });
    },

    buildTextFieldApellido2 : function()
    {
        this.textFieldApellido2 = new Ext.form.TextField(
        {
            width : 150,
            fieldLabel : 'Cognom2',
            name : 'apellido2'
        });
    },

    buildStoreTipoIdentificacion : function()
    {
        this.storeTipoIdentificacion = this.getStore(
        {
            url : '/bec/rest/tipoidentificacion',
            record : 'TipoIdentificacion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboTipoIdentificacion : function()
    {
        this.comboTipoIdentificacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 80,
            name : 'tipoIdentificacionId',
            hiddenName : 'tipoIdentificacionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoIdentificacion
        });
    },

    buildTextFieldIdentificacion : function()
    {
        this.textFieldIdentificacion = new Ext.form.TextField(
        {
            width : 115,
            name : 'identificacion'
        });
    },

    buildNumberFieldPorcentajeActividadesEconomicas : function()
    {
        this.numberFieldPorcentajeActividadesEconomicas = new Ext.form.NumberField(
        {
            width : 150,
            fieldLabel : 'Percentatge activitats econòmiques',
            name : 'porcentajeActividadesEconomicas',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldImporteActividadesEconomicas : function()
    {
        this.numberFieldImporteActividadesEconomicas = new Ext.form.NumberField(
        {
            width : 150,
            fieldLabel : 'Import activitats econòmiques',
            name : 'importeActividadesEconomicas',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldImporteRentasExtranjeras : function()
    {
        this.numberFieldImporteRentasExtranjeras = new Ext.form.NumberField(
        {
            width : 150,
            fieldLabel : 'Import rendes extrangeres',
            name : 'importeRentasExtranjero',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildCompositeIdentificacion : function()
    {
        this.compositeIdentificacion = new Ext.form.CompositeField(
        {
            fieldLabel : 'Identificació',
            items : [ this.comboTipoIdentificacion, this.textFieldIdentificacion ]
        });
    },

    buildDateFieldFechaNacimiento : function()
    {
        this.dateFieldFechaNacimiento = new Ext.form.DateField(
        {
            fieldLabel : 'Data Naixement',
            name : 'fechaNacimiento',
            width : 160,
            startDay : 1,
            format : 'd/m/Y',
            allowBlank : false
        });
    },

    buildDateFieldCaducidadNif : function()
    {
        this.dateFieldCaducidadNif = new Ext.form.DateField(
        {
            fieldLabel : 'Caducitat NIF',
            name : 'fechaCadNif',
            width : 160,
            startDay : 1,
            format : 'd/m/Y'
        });
    },

    buildDateFieldResMinus : function()
    {
        this.dateFieldResMinus = new Ext.form.DateField(
        {
            fieldLabel : 'Data resolució minusvalidesa',
            name : 'fechaResMinus',
            width : 160,
            startDay : 1,
            format : 'd/m/Y'
        });
    },

    buildDateFieldFinMinus : function()
    {
        this.dateFieldFinMinus = new Ext.form.DateField(
        {
            fieldLabel : 'Data fi minusvalidesa',
            name : 'fechaFinMinus',
            width : 160,
            startDay : 1,
            format : 'd/m/Y'
        });
    },

    buildStoreTipoSexo : function()
    {
        this.storeTipoSexo = this.getStore(
        {
            url : '/bec/rest/tiposexo',
            record : 'TipoSexo',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboSexo : function()
    {
        this.comboSexo = new Ext.form.ComboBox(
        {
            fieldLabel : 'Sexe',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'tipoSexoId',
            hiddenName : 'tipoSexoId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoSexo
        });
    },

    buildStoreEstadoCivil : function()
    {
        this.storeEstadoCivil = this.getStore(
        {
            url : '/bec/rest/estadocivil',
            record : 'EstadoCivil',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboEstadoCivil : function()
    {
        this.comboEstadoCivil = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estat Civil',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'estadoCivilId',
            hiddenName : 'estadoCivilId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeEstadoCivil
        });
    },

    buildStorePais : function()
    {
        this.storePais = this.getStore(
        {
            url : '/bec/rest/pais',
            record : 'Pais',
            id : 'id',
            fieldList : [ 'id', 'nombre', 'nacionalidad' ]
        });
    },

    buildComboNacionalidad : function()
    {
        this.comboNacionalidad = new Ext.form.ComboBox(
        {
            fieldLabel : 'Nacionalitat',
            triggerAction : 'all',
            editable : true,
            allowBlank : false,
            forceSelection : true,
            width : 150,
            name : 'paisId',
            hiddenName : 'paisId',
            valueField : 'id',
            displayField : 'nacionalidad',
            store : this.storePais
        });
    },

    buildStoreSituacionLaboral : function()
    {
        this.storeSituacionLaboral = this.getStore(
        {
            url : '/bec/rest/situacionlaboral',
            record : 'SituacionLaboral',
            id : 'id',
            width : 200,
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboSituacionLaboral : function()
    {
        this.comboSituacionLaboral = new Ext.form.ComboBox(
        {
            fieldLabel : 'Situació Laboral',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'situacionLaboralId',
            hiddenName : 'situacionLaboralId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeSituacionLaboral
        });
    },

    buildStoreProfesion : function()
    {
        this.storeProfesion = this.getStore(
        {
            url : '/bec/rest/profesion',
            record : 'Profesion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboProfesion : function()
    {
        this.comboProfesion = new Ext.form.ComboBox(
        {
            fieldLabel : 'Professió',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 330,
            name : 'profesionId',
            hiddenName : 'profesionId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeProfesion
        });
    },

    buildStoreComunidad : function()
    {
        this.storeComunidad = this.getStore(
        {
            url : '/bec/rest/comunidad',
            record : 'Comunidad',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboComunidad : function()
    {
        this.comboComunidad = new Ext.form.ComboBox(
        {
            fieldLabel : 'Comunitat expedidora del certificat de discapacitat',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 360,
            name : 'ccaaMinusId',
            hiddenName : 'ccaaMinusId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeComunidad
        });
    },

    buildStoreActividadesEconomicas : function()
    {
        var ref = this;
        this.storeActividadesEconomicas = this.getStore(
        {
            url : '/bec/rest/miembro/' + ref.miembroId + '/actividadeconomica',
            record : 'ActividadEconomica',
            id : 'id',
            fieldList : [ 'id', 'porcParticipacion', 'cifSociedad', 'impParticipacion', 'numSociedad' ]
        });
    },

    buildCheckBoxNoValidaIdentificacion : function()
    {
        this.checkBoxNoValidaIdentificacion = new Ext.form.Checkbox(
        {
            name : 'noValidaIdentificacion',
            labelAlign : 'left',
            value : 'S',
            boxLabel : 'No validar identificació'
        });
    },

    buildCheckBoxEstudiaFuera : function()
    {
        this.checkBoxEstudiaFuera = new Ext.form.Checkbox(
        {
            name : 'estudiaFuera',
            labelAlign : 'left',
            value : 'S',
            boxLabel : 'Estudia fora'
        });
    },

    buildCheckBoxCustodia : function()
    {
        this.checkBoxCustodia = new Ext.form.Checkbox(
            {
                name : 'custodia',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Custodia'
            });
    },

    buildCheckBoxIndDeclRenta : function()
    {
        this.checkBoxIndDeclRenta = new Ext.form.Checkbox(
            {
                name : 'indDeclRenta',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Presenta renta'
            });
    },

    buildCheckBoxIndRentasEspanna : function()
    {
        this.checkBoxIndRentasEspanna = new Ext.form.Checkbox(
            {
                name : 'indRentasEspanna',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Rentas Espanya'
            });
    },

    buildCheckBoxIndRentasNavarra : function()
    {
        this.checkBoxIndRentasNavarra = new Ext.form.Checkbox(
            {
                name : 'indRentasNavarra',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Rentas Navarra'
            });
    },

    buildCheckBoxIndRentasPaisVasco : function()
    {
        this.checkBoxIndRentasPaisVasco = new Ext.form.Checkbox(
            {
                name : 'indRentasPaisVasco',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Rentas Pais Vasco'
            });
    },

    buildCheckBoxIndConvivePareja : function()
    {
        this.checkBoxIndConvivePareja = new Ext.form.Checkbox(
            {
                name : 'indConvivePareja',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Conviu parella'
            });
    },

    buildCheckBoxIndIngresosPropios : function()
    {
        this.checkBoxIndIngresosPropios = new Ext.form.Checkbox(
            {
                name : 'indIngresosPropios',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Ingressos propis'
            });
    },

    buildCheckBoxIndRevMinusMec : function()
    {
        this.checkBoxIndRevMinusMec = new Ext.form.Checkbox(
            {
                name : 'indRevMinusMec',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Revisió minusvalidesa MEC'
            });
    },

    buildCheckBoxIndRevMinusUt : function()
    {
        this.checkBoxIndRevMinusUt = new Ext.form.Checkbox(
            {
                name : 'indRevMinusUt',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Revisió minusvalidesa UT'
            });
    },

    buildCheckBoxIndArrenUnidFamiliar : function()
    {
        this.checkBoxIndArrenUnidFamiliar = new Ext.form.Checkbox(
            {
                name : 'indArrenUnidFamiliar',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Arrendatari de familiar'
            });
    },

    buildCheckBoxCustodiaCompartida : function()
    {
        this.checkBoxCustodiaCompartida = new Ext.form.Checkbox(
            {
                name : 'custodiaCompartida',
                labelAlign : 'left',
                value : 'S',
                boxLabel : 'Custodia compartida'
            });
    },

    buildCheckBoxFirmaSolicitud : function()
    {
        this.checkBoxFirmaSolicitud = new Ext.form.Checkbox(
        {
            name : 'firmaSolicitud',
            labelAlign : 'left',
            value : 'S',
            boxLabel : 'Firma vàlida'
        });
    },

    buildCheckBoxResidencia : function()
    {
        this.checkBoxResidencia = new Ext.form.Checkbox(
        {
            name : 'residencia',
            labelAlign : 'left',
            value : 'S',
            boxLabel : 'Resident Espanya'
        });
    },

    buildCompositeChecks : function()
    {
        this.compositeChecks = new Ext.form.CompositeField(
        {
            fieldLabel : '',
            items : [this.checkBoxFirmaSolicitud, this.checkBoxEstudiaFuera ]
        });
    },

    buildTextFieldLocalidadTrabajo : function()
    {
        this.textFieldLocalidadTrabajo = new Ext.form.TextField(
        {
            width : 160,
            fieldLabel : 'Localitat treball',
            name : 'localidadTrabajo'
        });
    },

    buildTextFieldIdentificacionIdesp : function()
    {
        this.textFieldIdentificacionIdesp = new Ext.form.TextField(
        {
            width : 150,
            fieldLabel : 'Num suport (IDESP/IXESP)',
            name : 'identificacionIdesp'
        });
    },
    buildTextFieldCodigoEstudioHermano : function()
    {
        this.textFieldCodigoEstudioHermano = new Ext.form.TextField(
            {
                width : 150,
                fieldLabel : 'Codi Estudi',
                name : 'codigoEstudioHermano'
            });
    },
    buildTextFieldCodigoUnivHermano : function()
    {
        this.textFieldCodigoUnivHermano = new Ext.form.TextField(
            {
                width : 150,
                fieldLabel :'Universitat',
                name : 'codigoUnivHermano'
            });
    },

    buildTextFieldActividadEconomicaCif : function()
    {
        this.textFieldActividadEconomicaCif = new Ext.form.TextField(
        {
            name : 'cifSociedad'
        });
    },

    buildTextFieldNifArren : function()
    {
        this.textFieldNifArren = new Ext.form.TextField(
            {
                name : 'nifArren',
                width : 150,
                fieldLabel : 'Nif arrendatari'
            });
    },

    buildNumberFieldActividadEconomicaPorcentaje : function()
    {
        this.numberFieldActividadEconomicaPorcentaje = new Ext.form.NumberField(
        {
            name : 'porcParticipacion',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldActividadEconomicaImporte : function()
    {
        this.numberFieldActividadEconomicaImporte = new Ext.form.NumberField(
        {
            name : 'impParticipacion',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldPrecioAlquiler : function()
    {
        this.numberFieldPrecioAlquiler = new Ext.form.NumberField(
            {
                width : 150,
                fieldLabel : 'Import preu alquiler',
                name : 'precioAlquiler',
                decimalSeparator : ',',
                allowDecimals : true,
                allowNegative : false
            });
    },
    
    buildStoreProvincia : function()
    {
        this.storeProvincia = this.getStore(
        {
            url : '/bec/rest/provincia',
            record : 'Provincia',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildComboPadronProvincia : function()
    {
        var ref = this;
        this.comboPadronProvincia = new Ext.form.ComboBox(
        {
            fieldLabel : 'Província empadronament',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'padronProvinciaId',
            hiddenName : 'padronProvinciaId',
            valueField : 'id',
            displayField : 'nombre',
            editable : false,
            store : this.storeProvincia,
            listeners :
            {
                change : function(store)
                {
                    ref.comboPadronLocalidad.reset();
                    ref.storeLocalidad.proxy.setUrl('/bec/rest/localidad/provincia/' + this.getValue());
                    ref.storeLocalidad.load();
                }
            }
        });
    },

    buildComboTipoMoneda : function()
    {
        var ref = this;
        this.comboTipoMoneda = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus moneda',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'tipoMonedaId',
            hiddenName : 'tipoMonedaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoMoneda
        });
    },

    buildStoreTipoMoneda : function()
    {
        this.storeTipoMoneda = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/bec/rest/tipomoneda'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'TipoMoneda',
                id : 'id'
            }, [ 'id', 'nombre' ])
        });
    },

    buildStoreLocalidad : function()
    {
        this.storeLocalidad = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/bec/rest/localidad/provincia/0'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'Localidad',
                id : 'id'
            }, [ 'id', 'nombre' ])
        });
    },

    buildComboPadronLocalidad : function()
    {
        this.comboPadronLocalidad = new Ext.form.ComboBox(
        {
            fieldLabel : 'Localitat empadronament',
            triggerAction : 'all',
            editable : true,
            forceSelection : true,
            width : 160,
            name : 'padronLocalidadId',
            hiddenName : 'padronLocalidadId',
            valueField : 'id',
            displayField : 'nombre',
            editable : false,
            mode : 'local',
            store : this.storeLocalidad
        });
    },

    buildEditorActividadEconomica : function()
    {
        var ref = this;
        this.editorActividadEconomica = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    ref.storeActividadesEconomicas.proxy.conn.url = '/bec/rest/miembro/' + ref.miembroId + '/actividadeconomica';
                }
            }
        });
    },

    recargaFormularios : function()
    {
        this.cargaMiembro(this.miembroId);
        this.cargaActividadEconomica(this.miembroId);
        this.botonDeleteActividadEconomica.setDisabled(false);
        this.botonInsertActividadEconomica.setDisabled(false);
    },

    cargaMiembro : function(selectedId)
    {
        var formularioMiembro = this.panelFormulario.getForm();
        formularioMiembro.load(
        {
            url : '/bec/rest/miembro/' + selectedId,
            method : 'get',
            waitMsg : 'Per favor, espereu...',
            waitTitle : 'Carregant Membre',
            success : function(e)
            {
            },
            failure : function(form, action)
            {
                Ext.Msg.alert('Error', 'Error al cargar los datos');
            }
        });
    },

    cargaActividadEconomica : function(miembroId)
    {
        this.storeActividadesEconomicas.proxy.setUrl('/bec/rest/miembro/' + miembroId + '/actividadeconomica');
        this.storeActividadesEconomicas.load();
    },

    buildGridMiembros : function()
    {
        var ref = this;
        this.gridMiembros = new Ext.grid.GridPanel(
        {
            store : ref.storeMiembros,
            frame : true,
            height : 140,
            width : 850,
            style : 'margin-right:16px;',
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    header : 'Tipus membre',
                    width : 100,
                    dataIndex : 'tipoMiembro'
                },
                {
                    header : 'Nom i Cognoms',
                    width : 160,
                    dataIndex : 'nombreMiembro'
                },
                {
                    header : 'Identificació',
                    width : 90,
                    dataIndex : 'identificacion'
                },
                {
                    header : 'Sustentador',
                    width : 90,
                    dataIndex : 'tipoSustentador'
                } ]
            }),
            tbar :
            {
                items : [ this.botonInsert, this.botonDelete ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, e)
                {
                    var provId = ref.gridMiembros.selModel.getSelected().data.padronProvinciaId;
                    if (provId)
                    {
                        ref.storeLocalidad.proxy.setUrl('/bec/rest/localidad/provincia/' + provId, true);
                        ref.storeLocalidad.load(
                        {
                            callback : function(r)
                            {
                                ref.miembroId = ref.gridMiembros.selModel.getSelected().id;
                                ref.recargaFormularios();
                            }
                        });
                    }
                    else
                    {
                        ref.miembroId = ref.gridMiembros.selModel.getSelected().data.id;
                        ref.recargaFormularios();
                    }
                }
            }
        });
    },

    buildPanelFormulario : function()
    {
        this.panelFormulario = new Ext.FormPanel(
        {
            layout : 'table',
            frame : true,
            height : 700,
            width : 850,
            style : 'margin-right:16px;',
            labelAlign : 'top',
            reader : new Ext.data.XmlReader(
            {
                record : 'Miembro',
                id : 'id'
            }, [ 'id', 'solicitanteId', 'becaId', 'tipoMiembroId', 'nombre', 'apellido1', 'apellido2', 'tipoIdentificacionId', 'identificacion', 'fechaNacimiento', 'tipoSexoId', 'paisId', 'estadoCivilId',
                    'profesionId', 'noValidaIdentificacion', 'localidadTrabajo', 'tipoSustentadorId', 'situacionLaboralId', 'tipoMinusvaliaId', 'padronLocalidadId', 'padronProvinciaId',
                    'fechaCadNif', 'fechaResMinus', 'fechaFinMinus', 'identificacionIdesp', 'ccaaMinusId', 'estudiaFuera', 'firmaSolicitud', 'porcentajeActividadesEconomicas',
                    'importeActividadesEconomicas', 'importeRentasExtranjero', 'tipoMonedaId', 'residencia', 'custodia', 'custodiaCompartida', 'indDeclRenta', 'indRentasEspanna','indRentasNavarra', 'indRentasPaisVasco',
                    'indConvivePareja', 'indIngresosPropios', 'indRevMinusMec', 'indRevMinusUt', 'codigoEstudioHermano','codigoUnivHermano', 'indArrenUnidFamiliar', 'nifArren', 'precioAlquiler' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 4
            },
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            items : [
            {
                layout : 'form',
                items : [ this.comboTipoMiembro ]
            },
            {
                layout : 'form',
                items : [ this.textFieldNombre ]
            },
            {
                layout : 'form',
                items : [ this.textFieldApellido1 ]
            },
            {
                layout : 'form',
                items : [ this.textFieldApellido2 ]
            },
            {
                layout : 'form',
                items : [ this.comboSustentador ]
            },
            {
                layout : 'form',
                items : [ this.compositeIdentificacion ]
            },
            {
                layout : 'form',
                items : [ this.dateFieldCaducidadNif ]
            },
            {
                layout : 'form',
                items : [ this.textFieldIdentificacionIdesp ]
            },
            {
                layout : 'form',
                items : [ this.dateFieldFechaNacimiento ]
            },
            {
                layout : 'form',
                items : [ this.comboSexo ]
            },
            {
                layout : 'form',
                items : [ this.comboEstadoCivil ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxNoValidaIdentificacion ]

            },
            {
                layout : 'form',
                items : [ this.comboNacionalidad ]
            },
            {
                layout : 'form',
                items : [ this.comboPadronProvincia ]
            },
            {
                layout : 'form',
                items : [ this.comboPadronLocalidad ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxResidencia ]
            },
            {
                style : 'margin:15px 0px;border-top:1px solid lightblue;',
                colspan : 4
            },
            {
                layout : 'form',
                items : [ this.textFieldCodigoUnivHermano ]
            },
            {
                layout : 'form',
                items : [ this.textFieldCodigoEstudioHermano ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxEstudiaFuera ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxFirmaSolicitud ]
            },
            {
                layout : 'form',
                items : [ this.comboSituacionLaboral ]
            },
            {
                layout : 'form',
                labelAling : 'left',
                labelWidth : 130,
                items : [ this.textFieldLocalidadTrabajo ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.comboProfesion ]

            },
            {
                layout : 'form',
                items : [ this.comboTipoMinusvalia ]
            },
            {
                layout : 'form',
                items : [ this.dateFieldResMinus ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.dateFieldFinMinus ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ this.comboComunidad ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxCustodia ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxCustodiaCompartida ]
            },
            {
                style : 'margin:15px 0px;border-top:1px solid lightblue;',
                colspan : 4
            },
            {
                layout : 'form',
                items : [ this.numberFieldPorcentajeActividadesEconomicas ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldImporteActividadesEconomicas ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldImporteRentasExtranjeras ]
            },
            {
                layout : 'form',
                items : [ this.comboTipoMoneda ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndDeclRenta ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndRentasEspanna ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndRentasNavarra ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndRentasPaisVasco ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndConvivePareja ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndIngresosPropios ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndRevMinusMec ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndRevMinusUt ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxIndArrenUnidFamiliar ]
            },
            {
                layout : 'form',
                items : [ this.textFieldNifArren ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldPrecioAlquiler ],
                colspan : 2
            },
            {
                layout : 'form',
                items : [  ],
                colspan : 3
            },
            {
                layout : 'form',
                items : [ this.buttonGuardar ]
            },
            {
                items : [ this.numberFieldId ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldSolicitanteId ]
            },
            {
                layout : 'form',
                items : [ this.numberFieldBecaId ]
            } ]

        });
    },

    buildNumberFieldNotaPruebaEspecifica : function()
    {
        this.numberFieldNotaPruebaEspecifica = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nota prova específica',
            name : 'notaPruebaEspecifica',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildGridActividadesEconomicas : function()
    {
        function porcentaje(valor)
        {
            return cambiaCaracterDecimal(valor) + ' %';
        }

        function cambiaCaracterDecimal(valor)
        {
            return valor.toString().replace(".", ",");
        }

        var ref = this;
        this.gridActividadesEconomicas = new Ext.grid.GridPanel(
        {
            title : 'Dades econòmics',
            store : ref.storeActividadesEconomicas,
            width : 850,
            height : 140,
            style : 'margin-right:16px;',
            frame : true,
            autoScroll : true,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,
            autoScroll : true,

            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editorActividadEconomica ],
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Nombre Societat',
                    width : 30,
                    dataIndex : 'numSociedad',
                    editor : ref.numberFieldActividadEconomicaNumero
                },
                {
                    header : 'CIF Societat',
                    width : 30,
                    dataIndex : 'cifSociedad',
                    editor : ref.textFieldActividadEconomicaCif
                },
                {
                    header : '% Participació',
                    width : 30,
                    dataIndex : 'porcParticipacion',
                    renderer : porcentaje,
                    editor : ref.numberFieldActividadEconomicaPorcentaje
                },
                {
                    header : 'Import Participació',
                    width : 30,
                    renderer : cambiaCaracterDecimal,
                    dataIndex : 'impParticipacion',
                    editor : ref.numberFieldActividadEconomicaImporte
                } ]
            }),
            tbar :
            {
                items : [ ref.botonInsertActividadEconomica, ref.botonDeleteActividadEconomica ]
            }
        });
    }
});