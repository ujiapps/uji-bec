Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoMinusvalia = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Minusvalidesas',
    url : '/bec/rest/tipominusvalia',
    record : 'TipoMinusvalia'
});