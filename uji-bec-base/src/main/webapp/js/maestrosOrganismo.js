Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosOrganismo = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Organismes',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestrosOrganismo.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreOrganismo();

        this.buildEditor();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildGridOrganismo();

        this.add(this.gridOrganismo);
    },

    loadData: function() {
        this.storeOrganismo.reload();
    },

    buildStoreOrganismo : function()
    {
        this.storeOrganismo = this.getStore(
            {
                url : '/bec/rest/organismo',
                record : 'Organismo',
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var OrganismoSeleccionado = ref.gridOrganismo.getSelectionModel().getSelected();
                        if (!OrganismoSeleccionado)
                        {
                            return false;
                        }
                        ref.storeOrganismo.remove(OrganismoSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroOrganismo = new ref.storeOrganismo.recordType({});
                ref.editor.stopEditing();
                ref.storeOrganismo.insert(0, registroOrganismo);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridOrganismo;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridOrganismo : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Nom',
                width : 55,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            } ]
        });

        this.gridOrganismo = new Ext.grid.GridPanel(
        {
            store : this.storeOrganismo,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});