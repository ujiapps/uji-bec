Ext.ns('UJI.BEC');

UJI.BEC.ReclamacionesTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Reclamacions',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,
    reclamacionId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.ReclamacionesTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreReclamaciones();
        this.buildStoreEstado();

        syncStoreLoad([ this.storeEstado, this.storeReclamaciones ]);

        this.buildButtonInsert();
        this.buildButtonDelete();

        this.buildTextFieldBecaId();
        this.buildTextFieldSolicitanteId();

        this.buildTextFieldReclamacionId();
        this.buildTextFieldSolicitudId();
        this.buildTextFieldRegistroId();
        this.buildTextFieldRegistroEjercicioId();
        this.buildDateFieldFecha();
        this.buildTextAreaObservaciones();
        this.buildComboEstado();
        this.buildDateFieldFechaNotificacion();
        this.buildDateFieldFechaValidacion();

        this.buildButtonGuardar();

        this.buildPanelGridReclamaciones();
        this.buildPanelForm();

        this.add(this.panelGridReclamaciones);
        this.add(this.panelForm);
    },

    loadStore: function() {
        syncStoreLoad([ this.storeReclamaciones ]);
    },

    buildStoreReclamaciones : function()
    {
        this.storeReclamaciones = this.getStore(
            {
                url : '/bec/rest/beca/' + this.becaId + '/reclamacion',
                record : 'BecaReclamacion',
                id : 'id',
                fieldList : [ 'id', 'fecha','solicitudId', 'becaId', 'observaciones', 'estadoId', 'fechaNotificacion','fechaValidacion', 'registroId', 'registroEjercicioId' ]
            });
    },

    buildStoreEstado : function()
    {
        this.storeEstado = this.getStore(
            {
                url : '/bec/rest/tipoestadoreclamacion',
                record : 'TipoEstadoReclamacion',
                id : 'id',
                fieldList : [ 'id', 'nombre', 'orden' ]
            });
    },

    cargaReclamacion : function(selectedId)
    {
        var miForm = this.panelForm.getForm();
        miForm.load(
        {
            url : '/bec/rest/beca/' + this.becaId + '/reclamacion/' + selectedId,
            method : 'get',
            waitMsg : 'Per favor, espereu...',
            waitTitle : 'Carregant reclamació',
            success: function (e) {
            },
            failure : function(form, action)
            {
                Ext.Msg.alert('Error', 'Error al carregar les dades.');
            }
        });
    },

    buildTextFieldBecaId : function()
    {
        this.textFieldBecaId = new Ext.form.TextField(
            {
                name : 'becaId',
                value : this.becaId,
                hidden : true
            });
    },

    buildTextFieldSolicitanteId : function()
    {
        this.textFieldSolicitanteId = new Ext.form.TextField(
        {
            name : 'solicitanteId',
            value : this.solicitanteId,
            hidden : true
        });
    },

    buildTextFieldReclamacionId : function()
    {
        this.textFieldReclamacionId = new Ext.form.TextField(
            {
                fieldLabel : 'Reclamació',
                name : 'id',
                readOnly : true,
                hidden: true,
                width : 60
            });
    },

    buildTextFieldSolicitudId : function()
    {
        this.textFieldSolicitudId = new Ext.form.TextField(
            {
                fieldLabel : 'Sol·licitud',
                name : 'solicitudId',
                value : this.solicitudId,
                width : 60
            });
    },

    buildTextFieldRegistroId : function()
    {
        this.textFieldRegistroId = new Ext.form.TextField(
            {
                fieldLabel : 'Registre',
                name : 'registroId',
                width : 60
            });
    },

    buildTextFieldRegistroEjercicioId : function()
    {
        this.textFieldRegistroEjercicioId = new Ext.form.TextField(
            {
                fieldLabel : 'Exercici',
                name : 'registroEjercicioId',
                width : 60
            });
    },

    buildDateFieldFecha : function()
    {
        this.dateFieldFecha = new Ext.form.DateField(
            {
                fieldLabel : 'Sol·licitud',
                name : 'fecha',
                width : 150,
                startDay : 1,
                format : 'd/m/Y H:i:s'
            });
    },

    buildTextAreaObservaciones : function()
    {
        this.textAreaObservaciones = new Ext.form.TextArea(
            {
                fieldLabel : 'Observacions',
                name : 'observaciones',
                width : 582,
                height : 200,
                labelStyle : 'width: 100px;'
            });
    },

    buildComboEstado : function()
    {
        this.comboEstado = new Ext.form.ComboBox(
            {
                fieldLabel : 'Estat',
                name : 'estadoId',
                triggerAction : 'all',
                editable : true,
                forceSelection : true,
                width : 200,
                hiddenName : 'estadoId',
                valueField : 'id',
                displayField : 'nombre',
                mode : 'local',
                store : this.storeEstado
            });
    },

    buildDateFieldFechaNotificacion : function()
    {
        this.dateFieldFechaNotificacion = new Ext.form.DateField(
            {
                fieldLabel : 'Data notificació',
                name : 'fechaNotificacion',
                width : 150,
                startDay : 1,
                format : 'd/m/Y H:i:s'
            });
    },

    buildDateFieldFechaValidacion : function()
    {
        this.dateFieldFechaValidacion = new Ext.form.DateField(
            {
                fieldLabel : 'Sol·licitud Validada',
                name : 'fechaValidacion',
                width : 150,
                startDay : 1,
                format : 'd/m/Y H:i:s'
            });
    },

    buildButtonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                var id = ref.panelForm.getForm().getValues().id;

                if (id != '' && id != null)
                {
                    ref.panelForm.getForm().submit(
                    {
                        url : '/bec/rest/beca/' + ref.becaId + '/reclamacion/' + id,
                        method : 'put',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.storeReclamaciones.reload();
                        }
                    });
                }
                else
                {
                    ref.panelForm.getForm().submit(
                    {
                        url : '/bec/rest/beca/' + ref.becaId + '/reclamacion',
                        method : 'post',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            var reclamacion = Ext.DomQuery.selectNode('Reclamacion', action.response.responseXML);
                            ref.reclamacionId = Ext.DomQuery.selectValue('id', reclamacion);
                            ref.storeReclamaciones.reload();
                            ref.panelForm.getForm().reset();
                            ref.panelForm.focus();
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
            }
        });
    },

    buildButtonInsert : function()
    {
        var ref = this;

        this.buttonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                ref.panelGridReclamaciones.getSelectionModel().clearSelections();
                ref.panelForm.getForm().reset();
                ref.panelForm.focus();
            }
        });
    },

    buildButtonDelete : function()
    {
        var ref = this;

        this.buttonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.panelGridReclamaciones.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                Ext.MessageBox.show(
                {
                    title : 'Esborrar reclamació?',
                    msg : 'Atenció!\n ' + 'Vols esborrar la reclamació seleccionada?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.panelGridReclamaciones.store.remove(rec);
                            ref.panelForm.getForm().reset();
                        }
                    }
                });
            }
        });
    },

    recargaFormularios : function()
    {
        this.cargaReclamacion(this.panelGridReclamaciones.selModel.getSelected().id);
        this.reclamacionId = this.panelGridReclamaciones.selModel.getSelected().id;
    },

    buildPanelGridReclamaciones : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);

                return record ? record.get(combo.displayField) : combo.valueNotFoundText;

            };
        };

        this.panelGridReclamaciones = new Ext.grid.GridPanel(
        {
            store : ref.storeReclamaciones,
            height : 250,
            width : 850,
            flex : 1,
            frame : true,
            autoScroll : true,
            viewConfig :
            {
                forceFit : true
            },
            loadMask : true,

            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Sol·licitud',
                    width : 50,
                    dataIndex : 'solicitudId'
                },
                {
                    header: 'Registre',
                    width: 50,
                    dataIndex: 'registroId'
                },
                {
                    header : 'Exercici',
                    width : 50,
                    dataIndex : 'registroEjercicioId'
                },
                {
                    header : 'Presentació Sol·licitud',
                    width : 90,
                    dataIndex : 'fecha'
                },
                {
                    header : 'Sol·licitud Validada',
                    width : 90,
                    dataIndex : 'fechaValidacion'
                },
                {
                    header : 'Estat',
                    width : 100,
                    dataIndex : 'estadoId',
                    renderer : Ext.util.Format.comboRenderer(ref.comboEstado)
                },
                {
                    header : 'Data notificació',
                    width : 90,
                    dataIndex : 'fechaNotificacion'
                },
                    {
                        header: 'Web',
                        width: 30,
                        dataIndex: 'solicitudId',
                        renderer: function (solicitudId, type, full, meta)
                    {
                        if (solicitudId > 0) {
                            var url = 'https://e-ujier.uji.es/pls/www/!gri_www.euji23162?pBuscar=&pOpcion=Gestion&pTipo=25&pEstado=0&pId=' + solicitudId + '&pAccion=Detalle';
                            return '<a target="_blank" href = "' + url + '"> <img src=" http://static.uji.es/js/extjs/uji-commons-extjs/img/building_go.png"> </a>';

                        }
                    }
                    }
                ]
            }),
            tbar :
            {
                items: [this.buttonInsert, this.buttonDelete]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, event)
                {
                    ref.recargaFormularios();
                }
            }
        });
    },

    buildPanelForm : function()
    {
        this.panelForm = new Ext.form.FormPanel(
        {
            layout : 'table',
            width : 850,
            frame : true,
            flex : 1,
            labelAlign : 'top',
            labelWidth : '100',
            padding : '5px 0px',
            reader : new Ext.data.XmlReader(
            {
                record : 'BecaReclamacion'
            }, [ 'id','fecha', 'solicitudId', 'becaId', 'observaciones', 'estadoId', 'fechaNotificacion','fechaValidacion', 'registroId', 'registroEjercicioId'  ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'respuestaForm',
                success : 'success'
            }, [ 'success', 'msg' ]),
            defaults :
            {
                bodyStyle : 'padding:0px 5px'
            },
            layoutConfig :
            {
                columns :5
            },
            items : [
                {
                    layout : 'form',
                    items : [ this.textFieldSolicitudId ]
                },
                {
                    layout : 'form',
                    items : [ this.textFieldRegistroEjercicioId ]
                },
                {
                    layout : 'form',
                    items : [ this.dateFieldFechaValidacion ]
                },
                {
                    layout : 'form',
                    items : [ this.dateFieldFechaNotificacion ]
                },
                {
                    layout : 'form',
                    items : [ this.comboEstado ]
                },
                {
                    layout : 'form',
                    items : [ this.textAreaObservaciones ],
                    colspan : 4
                },
                {
                    layout : 'form',
                    items : [ this.buttonGuardar ]
                },
                {
                    layout : 'form',
                    items : [ this.textFieldBecaId, this.textFieldReclamacionId ]
                }
            ]
        });
    }
});