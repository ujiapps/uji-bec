Ext.ns('UJI.BEC');

UJI.BEC.PanelConvocatoria = Ext.extend(Ext.Panel,
{
    title : 'Convocatoria',
    layout : 'hbox',
    frame : true,
    convocatoriaId : 1,
    style : 'padding-bottom:15px;',
    convocatoriaNombre : 'Convocatoria XXX',
    panelConvocatoriaEstados : {},
    panelConvocatoriaProcesos : {},
    autoHeight : true,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.PanelConvocatoria.superclass.initComponent.call(this);

        this.title = 'Convocatoria ' + this.convocatoriaNombre;

        this.buildPanelConvocatoriaEstados();
        this.buildPanelConvocatoriaProcesos();

        this.add(this.panelConvocatoriaEstados);
        this.add(this.panelConvocatoriaProcesos);
    },

    buildPanelConvocatoriaEstados : function()
    {
        this.panelConvocatoriaEstados = new UJI.BEC.PanelDetalle(
        {
            convocatoriaId : this.convocatoriaId,
            textoTitulo : 'Estats',
            tipo : 'estados'
        });
    },

    buildPanelConvocatoriaProcesos : function()
    {
        this.panelConvocatoriaProcesos = new UJI.BEC.PanelDetalle(
        {
            convocatoriaId : this.convocatoriaId,
            textoTitulo : 'Processos',
            tipo : 'procesos'
        });
    }
});

UJI.BEC.PanelDetalle = Ext.extend(Ext.Panel,
{
    title : '',
    textoTitulo : '',
    layout : 'form',
    convocatoriaId : 0,
    tipo : '',
    labelWidth : 200,
    autoHeight : true,
    flex : 1,
    initComponent : function()
    {
        var ref = this;
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.PanelDetalle.superclass.initComponent.call(this);
        var request = Ext.Ajax.request(
        {
            url : '/bec/rest/convocatoria/' + this.convocatoriaId + '/' + this.tipo,
            success : function(response, opts)
            {
                titulo = new Ext.form.Label(
                {
                    text : ref.textoTitulo,
                    style : 'font-weight:bold;font-size:115%;display:block;margin-bottom:8px;'

                });
                ref.add(titulo);

                var xml = response.responseXML;
                var resumen = Ext.DomQuery.jsSelect('resumen', xml);
                for (var i = 0; i < resumen.length; i++)
                {
                    var etiqueta = Ext.DomQuery.selectValue('etiqueta', resumen[i]);
                    var valor = Ext.DomQuery.selectValue('valor', resumen[i]);
                    var label = new Ext.form.Label(
                    {
                        fieldLabel : etiqueta,
                        fieldWidth : 100,
                        text : valor
                    });
                    ref.add(label);
                }
                ref.ownerCt.doLayout();
            },
            failure : function(response, opts)
            {
                console.log(response.status);
            }
        });
    }
});
