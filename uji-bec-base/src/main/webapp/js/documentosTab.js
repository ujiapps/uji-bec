Ext.ns('UJI.BEC');

UJI.BEC.DocumentosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
    {
        title: 'Documents',
        layout: 'form',
        closable: false,
        autoScroll: true,
        width: 700,
        frame: true,
        padding: '5px 0px',

        solicitanteId: 0,
        becaId: 0,

        initComponent: function () {
            var config = {};
            Ext.apply(this, Ext.apply(this.initialConfig, config));

            UJI.BEC.DocumentosTab.superclass.initComponent.call(this);

            this.initUI();
        },

        initUI: function () {
            this.buildRowEditor();

            this.buildStoreTipoDocumento();
            this.buildStoreDocumentos();
            this.buildStoreHistorico();
            this.buildCheckBoxValida();
            this.buildComboTipoDocumento();
            this.buildButtonGuardaDocumento();
            this.buildButtonInsertaDocumento();
            this.buildButtonBorraDocumento();
            this.buildPanelGridDocumentos();

            this.buildButtonNotificaDocumentacionPendiente();
            this.buildButtonNotificaDocumentacionCorrecta();
            this.buildTextareaTextoAdicional();
            this.buildTextFieldBecaId();
            this.buildPanelFormularioNotificacion();

            this.buildPanelGridHistorico();

            this.add(this.panelGridDocumentos);
            this.add(this.panelFormularioNotificacion);
            this.add(this.panelGridHistorico);
        },

        buildRowEditor: function () {
            var ref = this;

            this.editor = new Ext.ux.grid.RowEditor(
                {
                    saveText: 'Desa',
                    cancelText: 'Cancel·la',
                    errorSummary: false
                });
        },

        buildStoreDocumentos: function () {
            this.storeDocumentos = new Ext.data.Store(
                 {
                     restful : true,
                     url : '/bec/rest/beca/' + this.becaId + '/documento',
                     reader : new Ext.data.XmlReader(
                         {
                             record : 'Documento',
                             idProperty : 'id'
                         }, [ 'id', 'becaId', 'tipoDocumentoId', 'nombre', 'validado',
                             {
                                 name : 'fechaSolicita',
                                 type : 'date',
                                 dateFormat : 'd/m/Y H:i:s'
                             }, 'activa',
                             {
                                 name : 'fechaActualiza',
                                 type : 'date',
                                 dateFormat : 'd/m/Y H:i:s'
                             } ]),
                     writer : new Ext.ux.uji.data.XmlWriter(
                         {
                             xmlEncoding : 'UTF-8',
                             writeAllFields : true
                         }, [ 'id', 'becaId', 'tipoDocumentoId', 'nombre', 'validado', 'fechaSolicita', 'fechaActualiza' ])
                 });
        },

        loadStore: function() {
            syncStoreLoad([this.storeTipoDocumento, this.storeDocumentos, this.storeHistorico]);
        },

        buildStoreTipoDocumento: function () {
            this.storeTipoDocumento = this.getStore(
                {
                    url: '/bec/rest/tipodocumento',
                    record: 'TipoDocumento',
                    id: 'id',
                    fieldList: ['id', 'nombre', 'orden']
                });
        },

        buildStoreHistorico: function () {
            this.storeHistorico = this.getStore(
                {
                    url: '/bec/rest/beca/' + this.becaId + '/mensaje',
                    record: 'Mensaje',
                    id: 'id',
                    fieldList: ['id', 'cuerpo', 'fecha', 'enviadoA', 'asunto']
                });
        },

        buildCheckBoxValida: function () {
            this.checkBoxValida = new Ext.form.Checkbox(
                {
                    name: 'valida',
                    width: 100,
                    fieldLabel: 'Valida'
                });
        },

        buildComboTipoDocumento: function () {
            this.comboTipoDocumento = new Ext.form.ComboBox(
                {
                    triggerAction: 'all',
                    editable: false,
                    forceSelection: true,
                    width: 100,
                    name: 'tipoDocumentoId',
                    valueField: 'id',
                    displayField: 'nombre',
                    store: this.storeTipoDocumento
                });
        },

        buildButtonGuardaDocumento: function () {
            var ref = this;
            this.buttonGuardaDocumento = new Ext.Button(
                {
                    text: 'Desa',
                    width: 60,
                    handler: function (button, event) {

                    }
                });
        },

        buildButtonInsertaDocumento: function () {
            var ref = this;

            this.ButtonInsertaDocumento = new Ext.Button(
                {
                    text: 'Afegir',
                    iconCls: 'application-add',
                    handler: function () {
                        var rec = new ref.panelGridDocumentos.store.recordType(
                            {
                                id: '',
                                becaId: ref.becaId,
                                nombre: '',
                                mimeType: '',
                                tipoDocumentoId: '',
                                fechaActualiza: '',
                                fechaSolicita: '',
                                validado: ''
                            });

                        ref.editor.stopEditing();
                        ref.panelGridDocumentos.store.insert(0, rec);
                        ref.editor.startEditing(0, true);
                    }
                });
        },

        buildButtonBorraDocumento: function () {
            var ref = this;

            this.buttonBorraDocumento = new Ext.Button(
                {
                    text: 'Esborrar',
                    iconCls: 'application-delete',
                    handler: function ()
                    {
                        var rec = ref.panelGridDocumentos.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        Ext.MessageBox.show(
                            {
                                title : 'Esborrar document?',
                                msg : 'Atenció!\n ' + 'vols esborrar el document sel·leccionat?',
                                buttons : Ext.Msg.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(button, text, opt)
                                {
                                    if (button == 'yes')
                                    {
                                        ref.panelGridDocumentos.store.remove(rec);
                                    }
                                }
                            });
                    }
                });
        },

        buildButtonNotificaDocumentacionPendiente: function () {
            var ref = this;

            function hayDocumentosPendientes(grid) {
                var contador = 0;
                grid.each(function(fila){
                    var validado= fila.get('validado');
                    if ((validado.toString() === "false"))
                    {
                        contador++;
                    }
                });
                return (contador > 0);
            }

            this.buttonNotificaDocumentacionPendiente = new Ext.Button(
                {
                    text: 'Notificar adjuntar documentació',
                    width: 260,
                    handler: function (button, event) {

                        var grid = ref.panelGridDocumentos.getStore();

                        if (hayDocumentosPendientes(grid))
                        {
                            Ext.MessageBox.show(
                                {
                                    title : 'Notificar documentació?',
                                    msg : 'Vols notificar els documents pendents?',
                                    buttons : Ext.Msg.YESNO,
                                    icon : Ext.MessageBox.QUESTION,
                                    fn : function(button, text, opt)
                                    {
                                        if (button == 'yes')
                                        {
                                            ref.panelFormularioNotificacion.getForm().submit(
                                            {
                                                url : '/bec/rest/beca/' + ref.becaId + '/mensaje/documentacion/pendiente',
                                                method : 'PUT',
                                                waitMsg : 'Per favor, espereu...',
                                                waitTitle : 'Enviant notificació',
                                                success : function(form, action)
                                                {
                                                    ref.storeDocumentos.reload();
                                                }
                                            });
                                        }
                                    }
                                });
                        } else {
                            Ext.MessageBox.show(
                                {
                                    title : 'Notificar documentació pendent',
                                    msg : 'No hi ha documents pendents de notificació',
                                    buttons : Ext.Msg.OK,
                                    icon : Ext.MessageBox.WARNING
                                });
                        }
                    }
                });
        },

        buildButtonNotificaDocumentacionCorrecta: function () {
            var ref = this;

            function todosLosDocumentosSonCorrectos(grid) {
                var contador = 0;
                grid.each(function(fila){
                    var validado= fila.get('validado');
                    if ((validado.toString() === "false"))
                    {
                        contador++;
                        console.log(contador);
                    }
                });
                console.log(contador);

                return (contador == 0);
            }

            this.buttonNotificaDocumentacionCorrecta = new Ext.Button(
                {
                    text: 'Notificar documentació correcta',
                    width: 260,
                    handler: function (button, event) {
                        var grid = ref.panelGridDocumentos.getStore();
                        if (todosLosDocumentosSonCorrectos(grid))
                        {
                            Ext.MessageBox.show(
                                {
                                    title : 'Notificar documentació correcta',
                                    msg : 'Vols notificar la validació del documents?',
                                    buttons : Ext.Msg.YESNO,
                                    icon : Ext.MessageBox.QUESTION,
                                    fn : function(button, text, opt)
                                    {
                                        if (button == 'yes')
                                        {
                                            ref.panelFormularioNotificacion.getForm().submit(
                                            {
                                                url : '/bec/rest/beca/' + ref.becaId + '/mensaje/documentacion/correcta',
                                                method : 'PUT',
                                                waitMsg : 'Per favor, espereu...',
                                                waitTitle : 'Enviant notificació',
                                                success : function(form, action)
                                                {
                                                    ref.storeHistorico.reload();
                                                }
                                            });
                                        }
                                    }
                                });
                        } else {
                            Ext.MessageBox.show(
                                {
                                    title : 'Notificar documentació correcta',
                                    msg : "Hi ha documents pendents de validació. Tots els documents han d'estar validats per poder notificar.",
                                    buttons : Ext.Msg.OK,
                                    icon : Ext.MessageBox.WARNING
                                });
                        }
                    }
                });
        },

        buildTextareaTextoAdicional: function () {
            this.textareaTextoAdicional = new Ext.form.TextArea(
                {
                    fieldLabel: 'Text opcional',
                    name: 'observaciones',
                    width: 500,
                    height: 100,
                    labelStyle: 'width: 100px;'
                });
        },

        buildTextFieldBecaId : function()
        {
            this.textFieldBecaId = new Ext.form.TextField(
                {
                    readOnly : true,
                    hidden : true,
                    name : 'becaId',
                    value : this.becaId
                });
        },

        buildPanelGridDocumentos: function () {
            var ref = this;
            Ext.util.Format.comboRenderer = function (combo) {
                return function (value) {
                    var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            };

            this.panelGridDocumentos = new Ext.grid.GridPanel(
                {
                    height: 250,
                    store: ref.storeDocumentos,
                    frame: true,
                    autoScroll: true,
                    flex: 1,
                    loadMask: true,
                    sm: new Ext.grid.RowSelectionModel(
                        {
                            singleSelect: true
                        }),
                    plugins: [this.editor],
                    viewConfig: {
                        forceFit: true
                    },
                    colModel: new Ext.grid.ColumnModel(
                        {
                            defaults: {
                                sortable: true
                            },
                            columns: [
                                {
                                    header: 'Id',
                                    dataIndex: 'id',
                                    hidden : true
                                },
                                {
                                    header: 'BecaId',
                                    dataIndex: 'becaId',
                                    hidden: true
                                },
                                {
                                    header: 'Tipus de document',
                                    width: 120,
                                    dataIndex: 'tipoDocumentoId',
                                    editor: ref.comboTipoDocumento,
                                    renderer: Ext.util.Format.comboRenderer(ref.comboTipoDocumento)
                                },
                                {   header: 'Fitxer',
                                    xtype: 'templatecolumn',
                                    tpl: '<a target="_blank" href="/bec/rest/beca/{becaId}/documento/{id}/binario">{nombre}</a>'
                                },
                                {
                                    header : 'Data sol·licita',
                                    width : 50,
                                    dataIndex : 'fechaSolicita',
                                    xtype : 'datecolumn',
                                    format : 'd/m/Y',
                                    editor : new Ext.form.DateField(
                                    {
                                       startDay : 1,
                                       format : 'd/m/Y',
                                        readOnly : true
                                    })
                                },
                                {
                                    header : 'Data adjunta',
                                    width : 50,
                                    dataIndex : 'fechaActualiza',
                                    xtype : 'datecolumn',
                                    format : 'd/m/Y',
                                    editor : new Ext.form.DateField(
                                    {
                                       startDay : 1,
                                       format : 'd/m/Y',
                                       readOnly : true
                                    })
                                },
                                {
                                    header : 'Validat',
                                    dataIndex : 'validado',
                                    xtype : 'booleancolumn',
                                    trueText : 'Sí',
                                    falseText : 'No',
                                    width : 20,
                                    editor : this.checkBoxValida
                                 }]
                        }),
                    tbar: {
                        items: [this.ButtonInsertaDocumento, this.buttonBorraDocumento]
                    }
                });
        },

        buildPanelFormularioNotificacion: function () {
            this.panelFormularioNotificacion = new Ext.form.FormPanel(
                {
                    layout: 'table',
                    frame: true,
                    flex: 1,
                    labelAlign: 'top',
                    labelWidth: '100',
                    padding: '5px 0px',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Domicilio'
                        }, ['id']),
                    errorReader: new Ext.data.XmlReader(
                        {
                            record: 'respuestaForm',
                            success: 'success'
                        }, ['success', 'msg']),
                    defaults: {
                        bodyStyle: 'padding:0px 5px'
                    },
                    layoutConfig: {
                        columns: 2
                    },
                    items: [{
                                layout: 'form',
                                items: [ this.buttonNotificaDocumentacionPendiente ]
                            },
                            {
                                layout: 'form',
                                rowspan: 2,
                                items: [ this.textareaTextoAdicional ]
                            },
                            {
                                layout: 'form',
                                items: [ this.buttonNotificaDocumentacionCorrecta ]
                            }, this.textFieldBecaId]
                });
        },

        buildPanelGridHistorico: function () {
            var ref = this;

            this.panelGridHistorico = new Ext.grid.GridPanel(
                {
                    title: 'Històric',
                    store: ref.storeHistorico,
                    height: 300,
                    flex: 1,
                    frame: true,
                    autoScroll: true,
                    viewConfig: {
                        forceFit: true
                    },
                    loadMask: true,

                    sm: new Ext.grid.RowSelectionModel(
                        {
                            singleSelect: true
                        }),
                    colModel: new Ext.grid.ColumnModel(
                        {
                            defaults: {
                                sortable: true
                            },
                            columns: [
                                {
                                    header: 'Info',
                                    dataIndex: 'information',
                                    width: 10,
                                    renderer: function(value, metadata, record) {
                                        metadata.attr = 'ext:qwidth="500" ext:qclass="tooltip" ext:qtitle="' + record.data.asunto + '" ext:qtip="<pre>' + record.data.cuerpo + '</pre>"';
                                        return '<img class="info" src="http://static.uji.es/js/extjs/uji-commons-extjs/img/information.png" />'
                                    }
                                },
                                {
                                    header: 'Id',
                                    dataIndex: 'id',
                                    hidden: true
                                },
                                {
                                    header: 'Data',
                                    dataIndex: 'fecha',
                                    width : 50
                                },
                                {
                                    header: 'Assumpte',
                                    dataIndex: 'asunto',
                                    width : 100
                                },
                                {
                                    header: 'Enviat a',
                                    dataIndex: 'enviadoA'
                                },
                                {
                                    header: 'Text',
                                    dataIndex: 'cuerpo'
                                }]
                        })
                });
        }
    });