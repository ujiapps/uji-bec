Ext.ns('UJI.BEC');

UJI.BEC.AcademicosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Acadèmics',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 700,
    frame : true,
    padding : '5px 0px',

    becaId : 0,
    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.AcademicosTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreTipoTitulacion();
        this.buidComboTipoTitulacion();

        this.buildNumberFieldBecaId();
        this.buildNumberFieldCursoAnteriorNotaMedia();
        this.buildNumberFieldCursoAnteriorCrdMatriculados();
        this.buildNumberFieldCursoAnteriorCrdAprobados();
        this.buildNumberFieldCursoAnteriorCrdSuspendidos();
        this.buildNumberFieldCursoAnteriorCrdConvalidados();
        this.buildNumberFieldNotaPruebaEspecifica();
        this.buildTextFieldProcedenciaNota();
        this.buildNumberFieldNotaMediaSinSuspenso();
        this.buildNumberFieldCursoAnteriorBecasConcedidasAnt();
        this.buildNumberFieldCursoAnteriorNotaMediaUltimoCurso();

        this.buildNumberFieldCursoActualCrdMatriculados();
        this.buildNumberFieldCursoActualCrdConvalidados();
        this.buildNumberFieldCursoActualCrdPendientesConvalidacion();
        this.buildNumberFieldCursoActualCrdFueraBeca();
        this.buildNumberFieldCursoActualCrdParaBeca();
        this.buildNumberFieldCursoActualNumeroSemestre();
        this.buildNumberFieldCursoActualCursoNumero();
        this.buildNumberFieldCreditosFinalizar();
        this.buildNumberFieldCreditosComplementos();
        this.buildNumberFieldCursoActualCrdPrimera();
        this.buildNumberFieldCursoActualCrdSegunda();
        this.buildNumberFieldCursoActualCrdTercera();
        this.buildNumberFieldCursoActualCrdTerceraPosteriores();
        this.buildNumberFieldCursoActualCrdCuartaPosteriores();


        this.buildTextFieldNombreTitulo();
        this.buildTextFieldCodigosEstudiosAnteriores();
        this.buildTextFieldCodigosEstudiosUjiAnteriores();
        this.buildTextFieldEstudiosAnteriores();
        this.buildTextFieldCursoAcademicoAnt();
        this.buildTextFieldCausaOtros();
        this.buildTextFieldCodigoTituAcceso();
        this.buildTextFieldCodigoUnivAcceso();

        this.buildCheckBoxCumpleAcademicos();
        this.buildCheckBoxTieneTituloUniversitario();
        this.buildCheckBoxTieneTituloNacional();
        this.buildCheckBoxEstudiosHomologados();
        this.buildCheckBoxAlgunaBecaParcial();
        this.buildCheckBoxCursoAnteriorMatriculaParcial();
        this.buildCheckBoxCursoAnteriorBeca1();
        this.buildCheckBoxCursoAnteriorBeca2();
        this.buildCheckBoxCursoAnteriorBeca3();
        this.buildCheckBoxCursoAnteriorBecaCumpleAcademicos();
        this.buildCheckBoxCursoAnteriorRendimiento();
        this.buildCheckBoxCursoActualLimitacionCreditos();
        this.buildCheckBoxCursoActualLimitacionCreditosFinEst();
        this.buildCheckBoxCursoActualBecaReducida();
        this.buildCheckBoxCursoActualMatriculaParcial();
        this.buildCheckBoxCursoActualPoseeTitulo();
        this.buildCheckBoxCursoActualOtraBecaConcedida();

        this.buildButtonCalcularAcademicos();
        this.buildButtonGuardar();
        this.buildButtonCargaCursoActual();
        this.buildButtonCargaCursoAnterior();

        this.buildFieldSetEstudiosAnteriores();
        this.buildFieldSetAccesoMaster();

        this.buildPanelCursoActual();
        this.buildPanelCursoAnterior();
        this.buildPanelCursos();
        this.buildPanelForm();

        this.add(this.form);
        //this.addEvents('UJI.BEC.AcademicosTab.buttonCalcularAcademicos.click');
        //this.addEvents('UJI.BEC.AcademicosTab.buttonCalcularAcademicosAnterior.click');
    },

    buildStoreTipoTitulacion : function()
    {
        this.storeTipoTitulacion = this.getStore(
        {
            url : '/bec/rest/tipotitulacion',
            record : 'tipoTitulacion',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    loadStore: function() {
        syncStoreLoad([ this.storeTipoTitulacion ]);
    },

    buidComboTipoTitulacion : function()
    {
        this.comboTipoTitulacion = new Ext.form.ComboBox(
        {
            fieldLabel : 'Tipus titulació',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 150,
            name : 'tipoTitulacion',
            hiddenName : 'tipoTitulacion',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeTipoTitulacion
        });
    },

    load: function() {
        this.storeTipoTitulacion.reload();
    },

    buildNumberFieldBecaId : function()
    {
        this.numberFieldBecaId = new Ext.form.NumberField(
        {
            name : 'id',
            hidden : true,
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorNotaMedia : function()
    {
        this.numberFieldCursoAnteriorNotaMedia = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nota mitjana/accés',
            name : 'notaMediaAnt',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorCrdMatriculados : function()
    {
        this.numberFieldCursoAnteriorCrdMatriculados = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits per a beca',
            name : 'creditosMatriculadosAnt',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorCrdAprobados : function()
    {
        this.numberFieldCursoAnteriorCrdAprobados = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits superats',
            name : 'creditosSuperadosAnt',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorCrdSuspendidos : function()
    {
        this.numberFieldCursoAnteriorCrdSuspendidos = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : '% Crèdits suspesos',
            name : 'creditosSuspensosAnt',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldNotaPruebaEspecifica : function()
    {
        this.numberFieldNotaPruebaEspecifica = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nota específica',
            name : 'notaPruebaEspecifica',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldNotaMediaSinSuspenso : function()
    {
        this.numberFieldNotaMediaSinSuspenso = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nota mitjana sense suspens',
            name : 'notaMediaSinSuspenso',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorCrdConvalidados : function()
    {
        this.numberFieldCursoAnteriorCrdConvalidados = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits convalidats',
            name : 'creditosConvalidadosAnt',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorBecasConcedidasAnt : function()
    {
        this.numberFieldCursoAnteriorBecasConcedidasAnt = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Concedides',
            name : 'becasConcedidasAnt',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldCursoAnteriorNotaMediaUltimoCurso : function()
    {
        this.numberFieldCursoAnteriorNotaMediaUltimoCurso = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nota mitjana/masters',
            name : 'notaMediaUltimoCurso',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCrdMatriculados : function()
    {
        this.numberFieldCursoActualCrdMatriculados = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits matriculats',
            name : 'creditosMatriculados',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCrdPrimera : function()
    {
        this.numberFieldCursoActualCrdPrimera  = new Ext.form.NumberField(
          {
              width : 50,
              fieldLabel : 'Crèdits primera',
              name : 'creditosPrimeraMatricula',
              decimalSeparator : ',',
              allowDecimals : true,
              allowNegative : false
          });
    },

    buildNumberFieldCursoActualCrdSegunda : function()
    {
        this.numberFieldCursoActualCrdSegunda  = new Ext.form.NumberField(
          {
              width : 50,
              fieldLabel: 'Crèdits Segona',
              name : 'creditosSegundaMatricula',
              decimalSeparator : ',',
              allowDecimals : true,
              allowNegative : false
          });
    },

    buildNumberFieldCursoActualCrdTercera : function()
    {
        this.numberFieldCursoActualCrdTercera  = new Ext.form.NumberField(
          {
              width : 50,
              fieldLabel: 'Crèdits Tercera',
              name : 'creditosTerceraMatricula',
              decimalSeparator : ',',
              allowDecimals : true,
              allowNegative : false
          });
    },

    buildNumberFieldCursoActualCrdTerceraPosteriores: function () {
        this.numberFieldCursoActualCrdTerceraPosteriores = new Ext.form.NumberField(
            {
                width: 50,
                fieldLabel: 'Crèdits Tercera i Posteriors',
                name: 'creditosTerceraMatriculaPosteriores',
                decimalSeparator: ',',
                allowDecimals: true,
                allowNegative: false
            });
    },

    buildNumberFieldCursoActualCrdCuartaPosteriores: function () {
        this.numberFieldCursoActualCrdCuartaPosteriores = new Ext.form.NumberField(
            {
                width: 50,
                fieldLabel: 'Crèdits Quarta i Posteriors',
                name: 'creditosCuartaMatriculaPosteriores',
                decimalSeparator: ',',
                allowDecimals: true,
                allowNegative: false
            });
    },

    buildNumberFieldCursoActualCrdConvalidados : function()
    {
        this.numberFieldCursoActualCrdConvalidados = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits convalidats',
            name : 'creditosConvalidados',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCrdPendientesConvalidacion : function()
    {
        this.numberFieldCursoActualCrdPendientesConvalidacion = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits pendents conval.',
            name : 'creditosPendientesConvalidacion',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCrdFueraBeca : function()
    {
        this.numberFieldCursoActualCrdFueraBeca = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits fora beca',
            name : 'creditosFueraBeca',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCrdParaBeca : function()
    {
        this.numberFieldCursoActualCrdParaBeca = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Crèdits per a beca',
            name : 'creditosParaBeca',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualNumeroSemestre : function()
    {
        this.numberFieldCursoActualNumeroSemestre = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nombre semestres',
            name : 'numeroSemestres',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldCursoActualCursoNumero : function()
    {
        this.numberFieldCursoActualCursoNumero = new Ext.form.NumberField(
        {
            width : 50,
            fieldLabel : 'Nombre del curs',
            name : 'curso',
            allowDecimals : false,
            allowNegative : false
        });
    },

    buildNumberFieldCreditosFinalizar : function()
    {
        this.numberFieldCreditosFinalizar = new Ext.form.NumberField(
        {
            width : 45,
            name : 'creditosParaFinalizar',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : true,
            readOnly : true
        });
    },

    buildNumberFieldCreditosComplementos : function()
    {
        this.numberFieldCreditosComplementos = new Ext.form.NumberField(
        {
            width : 45,
            readOnly : true,
            name : 'creditosComplementos',
            decimalSeparator : ',',
            allowDecimals : true,
            allowNegative : true
        });
    },

    buildTextFieldNombreTitulo : function()
    {
        this.textFieldNombreTitulo = new Ext.form.TextField(
        {
            width : 350,
            margins : '0 10',
            name : 'tituloNombre'
        });
    },

    buildTextFieldCodigosEstudiosAnteriores : function()
    {
        this.textFieldCodigosEstudiosAnteriores = new Ext.form.TextField(
        {
            width : 100,
            margins : '0 10',
            name : 'codigoEstudioAnt',
            readOnly : true
        });
    },

    buildTextFieldCodigosEstudiosUjiAnteriores : function()
    {
        this.textFieldCodigosEstudiosUjiAnteriores = new Ext.form.TextField(
            {
                width : 100,
                margins : '0 10',
                name : 'codigoEstudioUjiAnt',
                readOnly : true
            });
    },

    buildTextFieldEstudiosAnteriores : function()
    {
        this.textFieldEstudiosAnteriores = new Ext.form.TextField(
        {
            width : 240,
            margins : '0 10',
            name : 'estudioAnt',
            readOnly : true
        });
    },

    buildTextFieldProcedenciaNota : function()
    {
        this.textFieldProcedenciaNota = new Ext.form.TextField(
        {
            name : 'procedenciaNota',
            fieldLabel : 'Procedència nota',
            maskRe : /^[0-9]{0,2}$/,
            maxLength : 2,
            maxLengthText : 'No poden haber més de {maxLength} caràcters',
            autoCreate :
            {
                tag : 'input',
                type : 'text',
                size : '2',
                autocomplete : 'off',
                maxlength : '2'
            },
            width : 50
        });
    },

    buildTextFieldCursoAcademicoAnt : function()
    {
        this.textFieldCursoAcademicoAnt = new Ext.form.TextField(
        {
            width : 100,
            margins : '0 10',
            name : 'cursoAcademicoAnt'
        });
    },

    buildTextFieldCausaOtros : function()
    {
        this.textFieldCausaOtros = new Ext.form.TextField(
        {
            width : 400,
            margins : '0 10',
            name : 'causaOtros'
        });
    },

    buildTextFieldCodigoTituAcceso : function()
    {
        this.textFieldCodigoTituAcceso = new Ext.form.TextField(
        {
            width : 100,
            margins : '0 10',
            name : 'codTituAcceso'
        });
    },

    buildTextFieldCodigoUnivAcceso : function()
    {
        this.textFieldCodigoUnivAcceso = new Ext.form.TextField(
        {
            width : 50,
            margins : '0 10',
            name : 'univTitulacion'
        });
    },

    buildCheckBoxCumpleAcademicos : function()
    {
        this.checkBoxCumpleAcademicos = new Ext.form.Checkbox(
        {
            name : 'cumpleAcademicos',
            inputValue : true,
            boxLabel : 'Bloqueja acadèmics'
        });
    },

    buildCheckBoxTieneTituloUniversitario : function()
    {
        this.checkBoxTieneTituloUniversitario = new Ext.form.Checkbox(
        {
            name : 'tieneTituloUniversitario',
            inputValue : true,
            width : 130,
            boxLabel : 'Té títol universitari'
        });
    },

    buildCheckBoxTieneTituloNacional : function()
    {
        this.checkBoxTieneTituloNacional = new Ext.form.Checkbox(
        {
            name : 'tituloEspanol',
            inputValue : true,
            width : 110,
            boxLabel : 'Títol Espanyol'
        });
    },

    buildCheckBoxEstudiosHomologados : function()
    {
        this.checkBoxEstudiosHomologados = new Ext.form.Checkbox(
        {
            name : 'estudiosHomologados',
            inputValue : true,
            width : 200,
            boxLabel : 'Estudi no universitari homologat'
        });
    },

    buildCheckBoxAlgunaBecaParcial : function()
    {
        this.checkBoxAlgunaBecaParcial = new Ext.form.Checkbox(
        {
            name : 'algunaBecaParcial',
            inputValue : true,
            width : 145,
            boxLabel : 'Té alguna beca parcial'
        });
    },

    buildCheckBoxCursoAnteriorMatriculaParcial : function()
    {
        this.checkBoxCursoAnteriorMatriculaParcial = new Ext.form.Checkbox(
        {
            name : 'matriculaParcialAnt',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Beca parcial'
        });
    },

    buildCheckBoxCursoAnteriorBeca1 : function()
    {
        this.checkBoxCursoAnteriorBeca1 = new Ext.form.Checkbox(
        {
            name : 'becaCursoAnt',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Beca curs ant'
        });
    },

    buildCheckBoxCursoAnteriorBeca2 : function()
    {
        this.checkBoxCursoAnteriorBeca2 = new Ext.form.Checkbox(
        {
            name : 'becaCursoAnt2',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Beca curs ant 2'
        });
    },

    buildCheckBoxCursoAnteriorBeca3 : function()
    {
        this.checkBoxCursoAnteriorBeca3 = new Ext.form.Checkbox(
        {
            name : 'becaCursoAnt3',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Beca curs ant 3'
        });
    },

    buildCheckBoxCursoAnteriorBecaCumpleAcademicos : function()
    {
        this.checkBoxCursoAnteriorBecaCumpleAcademicos = new Ext.form.Checkbox(
        {
            name : 'cumpleAcadBecaAnt',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Condició becari'
        });
    },

    buildCheckBoxCursoAnteriorRendimiento : function()
    {
        this.checkBoxCursoAnteriorRendimiento = new Ext.form.Checkbox(
        {
            name : 'rendimientoAcademico',
            width : 125,
            hideLabel : true,
            inputValue : true,
            boxLabel : 'Rend. Acadèmic'
        });
    },

    buildCheckBoxCursoActualLimitacionCreditos : function()
    {
        this.checkBoxCursoActualLimitacionCreditos = new Ext.form.Checkbox(
        {
            name : 'limitarCreditos',
            width : 150,
            inputValue : true,
            boxLabel : 'Limitació crèdits'
        });
    },

    buildCheckBoxCursoActualLimitacionCreditosFinEst : function()
    {
        this.checkBoxcursoActualLimitacionCreditosFinEst = new Ext.form.Checkbox(
        {
            name : 'limitarCreditosFinEstudios',
            width : 150,
            inputValue : true,
            boxLabel : 'Limitació crèdits fi est'
        });
    },

    buildCheckBoxCursoActualBecaReducida : function()
    {
        this.checkBoxCursoActualBecaReducida = new Ext.form.Checkbox(
        {
            name : 'becaParcial',
            width : 150,
            inputValue : true,
            boxLabel : 'Beca parcial'
        });
    },

    buildCheckBoxCursoActualMatriculaParcial : function()
    {
        this.checkBoxCursoActualMatriculaParcial = new Ext.form.Checkbox(
        {
            name : 'matriculaParcial',
            width : 150,
            inputValue : true,
            boxLabel : 'Matrícula parcial'
        });
    },

    buildCheckBoxCursoActualPoseeTitulo : function()
    {
        this.checkBoxCursoActualPoseeTitulo = new Ext.form.Checkbox(
        {
            name : 'poseeTitulo',
            width : 150,

            inputValue : true,
            boxLabel : 'Té títol'
        });
    },

    buildCheckBoxCursoActualOtraBecaConcedida : function()
    {
        this.checkBoxCursoActualOtraBecaConcedida = new Ext.form.Checkbox(
          {
              name : 'otraBecaConcedida',
              width : 150,

              inputValue : true,
              boxLabel: 'MECD i segona matricula'
          });
    },

    buildButtonCalcularAcademicos : function()
    {
        var ref = this;
        this.buttonCalcularAcademicos = new Ext.Button(
        {
            text : 'Calcula proposta',
            width : 125,
            handler : function(button, event)
            {
                var becaId = ref.becaId;
                var barraProgreso = Ext.MessageBox.show(
                {
                    msg : 'Calculant proposta, per favor espera...',
                    width : 300,
                    wait : true,
                    waitConfig :
                    {
                        interval : 100
                    }
                });

                Ext.Ajax.request(
                {
                    url : '/bec/rest/propuestabeca/' + becaId,
                    method : 'POST',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Calculant',
                    success : function(result, request)
                    {
                        var nodeBeca = Ext.DomQuery.selectNode('Beca', result.responseXML);
                        var estadoId = Ext.DomQuery.selectValue('estadoId', nodeBeca);

                        Ext.Ajax.request(
                        {
                            url : '/bec/rest/estado/' + estadoId,
                            method : 'GET',
                            success : function(result)
                            {
                                var nodeEstado = Ext.DomQuery.selectNode('Estado', result.responseXML);
                                var nombreEstado = Ext.DomQuery.selectValue('nombre', nodeEstado);

                                barraProgreso.hide();
                                alert('La beca ha quedat en l\'estat: ' + nombreEstado);
                            },
                            failure : function()
                            {
                                barraProgreso.hide();
                            }
                        });

                        ref.recargaFormAcademicos();
                        //ref.fireEvent('UJI.BEC.AcademicosTab.buttonCalcularAcademicos.click');
                    },
                    failure : function()
                    {
                        barraProgreso.hide();
                    }
                });
            }
        });
    },

    buildButtonGuardar : function()
    {
        var ref = this;
        this.buttonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 80,
            handler : function(button, event)
            {
                ref.form.getForm().submit(
                {
                    url : '/bec/rest/beca/' + ref.becaId,
                    method : 'PUT',
                    waitMsg : 'Per favor, espereu...',
                    waitTitle : 'Guardant dades',
                    success : function(form, action)
                    {
                    },
                    failure : function()
                    {
                        alert('Error al guardar les dades acadèmiques');
                    }
                });
            }
        });
    },

    cargaAcademicos : function(queCurso)
    {
        var ref = this;

        var cumpleAcademicos = ref.form.getForm().getValues()['cumpleAcademicos'];

        if (cumpleAcademicos)
        {
            alert('Dades acadèmiques bloquejades');
        }
        else
        {
            var barraProgreso = Ext.MessageBox.show(
            {
                msg : 'Actualitzant dades, per favor espera...',
                width : 300,
                wait : true,
                waitConfig :
                {
                    interval : 100
                }
            });

            ref.form.getForm().submit(
            {
                url : '/bec/rest/beca/' + ref.becaId,
                method : 'put',
                success : function(form, action)
                {
                    var laUrl = '';

                    if (queCurso == 'actual')
                    {
                        laUrl = '/bec/rest/academicos/actual/';
                    }
                    else
                    {
                        laUrl = '/bec/rest/academicos/anterior/';
                    }

                    Ext.Ajax.request(
                    {
                        url : laUrl + ref.becaId,
                        method : 'PUT',
                        success : function(result, request)
                        {
                            ref.recargaFormAcademicos();
                            barraProgreso.hide();
                        },
                        failure : function()
                        {
                            barraProgreso.hide();

                        }
                    });
                },
                failure : function()
                {
                    alert('Error al guardar les dades acadèmiques');
                    barraProgreso.hide();
                }
            });

        }
    },

    buildButtonCargaCursoActual : function()
    {
        var ref = this;

        this.buttonCargaCursoActual = new Ext.Button(
        {
            text : 'Actualitza Curs Actual',
            width : 125,
            handler : function(button, event)
            {
                ref.cargaAcademicos('actual');
            }
        });
    },

    buildButtonCargaCursoAnterior : function()
    {
        var ref = this;

        this.buttonCargaCursoAnterior = new Ext.Button(
        {

            text : 'Actualitza Curs Anterior',
            width : 125,
            handler : function(button, event)
            {
                ref.cargaAcademicos('anterior');
            }
        });
    },

    buildFieldSetEstudiosAnteriores : function()
    {
        this.fieldSetEstudiosAnteriores = new Ext.form.FieldSet(
        {
            columnWidth : 0.5,
            title : 'Estudis anteriors',
            autoHeight : true,
            defaultType : 'textfield',
            items : [ this.numberFieldBecaId,
            {
                xtype : 'compositefield',
                hideLabel : true,
                items : [ this.checkBoxTieneTituloUniversitario, this.textFieldNombreTitulo,
                {
                    xtype : 'label',
                    width : 80,
                    text : 'Tipus titulació',
                    margins : '3 10 0 0'
                }, this.comboTipoTitulacion ]
            },
            {
                xtype : 'compositefield',
                hideLabel : true,
                items : [
                {
                    xtype : 'label',
                    width : 125,
                    text : 'Ruct',
                    margins : '3 10 0 0'
                }, this.textFieldCodigosEstudiosAnteriores,
                {
                    xtype : 'label',
                    width : 70,
                    text : 'Uji',
                    margins : '3 10 0 0'
                }, this.textFieldCodigosEstudiosUjiAnteriores,
                {
                    xtype : 'label',
                    text : 'Estudi',
                    width : 30,
                    margins : '3 10 0 0'
                }, this.textFieldEstudiosAnteriores ]
            },
            {
                xtype : 'compositefield',
                hideLabel : true,
                items : [
                {
                    xtype : 'label',
                    text : 'Curs acadèmic',
                    width : 125,
                    margins : '3 10 0 0'
                }, this.textFieldCursoAcademicoAnt,
                {
                    xtype : 'label',
                    text : 'Causa altres',
                    width : 70,
                    margins : '3 10 0 0'
                }, this.textFieldCausaOtros ]
            },
            {
                xtype : 'compositefield',
                hideLabel : true,
                items : [ this.checkBoxAlgunaBecaParcial ]
            } ]
        });
    },

    buildFieldSetAccesoMaster : function()
    {
        this.fieldSetAccesoMaster = new Ext.form.FieldSet(
        {
            columnWidth : 0.5,
            title : 'Accés a Master',
            autoHeight : true,
            defaultType : 'textfield',
            items : [
            {
                xtype : 'compositefield',
                hideLabel : true,
                defaultMargins : '0 20px 0 0',
                items : [
                {
                    xtype : 'label',
                    width : 120,
                    text : 'Codi Titulació Accés',
                    margins : '3 10 0 0'
                }, this.textFieldCodigoTituAcceso, this.checkBoxEstudiosHomologados, this.checkBoxTieneTituloNacional,
                {
                    xtype : 'label',
                    width : 100,
                    text : 'Universitat Accés',
                    margins : '3 0 0 0'
                }, this.textFieldCodigoUnivAcceso ]
            } ]
        });
    },

    buildPanelForm : function()
    {
        var ref = this;
        this.form = new Ext.form.FormPanel(
        {
            reader : new Ext.data.XmlReader(
            {
                record : 'Beca'
            }, [ 'id', 'solicitanteId', 'tieneTituloUniversitario', 'tituloNombre', 'codigoEstudioAnt', 'codigoEstudioUjiAnt', 'estudioAnt', 'cursoAcademicoAnt', 'causaOtros',
                    'notaMediaAnt', 'notaMediaUltimoCurso', 'creditosMatriculadosAnt', 'creditosSuperadosAnt', 'creditosSuspensosAnt', 'creditosConvalidadosAnt', 'becasConcedidasAnt',
                    'becasConcedidasCons', 'matriculaParcialAnt', 'cumpleAcadBecaAnt', 'rendimientoAcademico', 'becaCursoAnt', 'becaCursoAnt2', 'becaCursoAnt3', 'cumpleAcademicos',
                    'creditosMatriculados', 'creditosConvalidados', 'creditosPendientesConvalidacion', 'creditosFueraBeca', 'creditosParaBeca', 'numeroSemestres', 'curso', 'matriculaParcial',
                    'becaParcial', 'poseeTitulo', 'algunaBecaParcial', 'limitarCreditos', 'limitarCreditosFinEstudios', 'tituloEspanol', 'estudiosHomologados', 'univTitulacion', 'indCoefCorrector',
                    'notaCorregida', 'codTituAcceso', 'matriculaParcial', 'creditosParaFinalizar', 'creditosComplementos', 'tipoTitulacion', 'notaPruebaEspecifica', 'procedenciaNota',
                    'notaMediaSinSuspenso', 'creditosPrimeraMatricula', 'creditosSegundaMatricula', 'creditosTerceraMatricula', 'creditosTerceraMatriculaPosteriores', 'creditosCuartaMatriculaPosteriores', 'otraBecaConcedida']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            width : 800,
            items : [ this.fieldSetEstudiosAnteriores, this.fieldSetAccesoMaster, this.panelCursos,
            {
                layout : 'form',
                buttons : [ this.buttonCargaCursoAnterior, this.buttonCargaCursoActual, this.buttonCalcularAcademicos, this.checkBoxCumpleAcademicos, this.buttonGuardar ]
            } ],
            listeners :
            {
                'afterrender' : function(obj)
                {
                    ref.recargaFormAcademicos();
                }
            }
        });
    },

    recargaFormAcademicos : function()
    {
        var ref = this;

        ref.form.getForm().load(
        {
            url : '/bec/rest/beca/' + ref.becaId,
            method : 'GET',
            success : function(form)
            {
                //ref.fireEvent('UJI.BEC.AcademicosTab.buttonCalcularAcademicosAnterior.click');
            }
        });
    },

    buildPanelCursos : function()
    {
        this.panelCursos = new Ext.Panel(
        {
            layout : 'hbox',
            autoScroll : true,
            height : 370,
            items : [ this.panelCursoAnterior, this.panelCursoActual ]
        });
    },

    buildPanelCursoAnterior : function()
    {
        this.panelCursoAnterior = new Ext.Panel(
        {
            layout : 'form',
            flex : 1,
            labelWidth : 120,
            padding : '0 0 0 5px',
            width : 350,
            height : 370,
            margins : '0 3px 0 0',
            frame : true,
            title : 'Curs anterior',
            items : [
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoAnteriorNotaMedia, this.checkBoxCursoAnteriorBeca1 ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldNotaMediaSinSuspenso, this.checkBoxCursoAnteriorMatriculaParcial ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldNotaPruebaEspecifica, this.checkBoxCursoAnteriorBecaCumpleAcademicos ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.textFieldProcedenciaNota, this.checkBoxCursoAnteriorRendimiento ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoAnteriorCrdMatriculados ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoAnteriorCrdAprobados ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoAnteriorCrdSuspendidos ]
            }, this.numberFieldCursoAnteriorCrdConvalidados, this.numberFieldCursoAnteriorBecasConcedidasAnt, this.numberFieldCursoAnteriorNotaMediaUltimoCurso ]
        });
    },

    buildPanelCursoActual : function()
    {
        this.panelCursoActual = new Ext.Panel(
        {
            layout : 'form',
            flex : 1,
            labelWidth : 150,
            padding : '0 0 0 5px',
            height : 370,
            margins : '0 3px 0 0',
            frame : true,
            title : 'Curs actual',
            items : [
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.checkBoxCumpleAcademicos, this.numberFieldCursoActualCrdMatriculados, this.checkBoxCursoActualBecaReducida ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCrdConvalidados, this.checkBoxCursoActualPoseeTitulo ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCrdPendientesConvalidacion, this.checkBoxCursoActualLimitacionCreditos ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCrdFueraBeca, this.checkBoxcursoActualLimitacionCreditosFinEst ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCrdParaBeca, this.checkBoxCursoActualMatriculaParcial ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualNumeroSemestre,
                {
                    xtype : 'label',
                    text : 'Crd. complements form.',
                    width : 120,
                    margins : '3 10 0 0'
                }, this.numberFieldCreditosComplementos ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCursoNumero,
                {
                    xtype : 'label',
                    text : 'Crd. per finalitzar',
                    width : 120,
                    margins : '3 10 0 0'
                }, this.numberFieldCreditosFinalizar ]
            },
            {
                xtype : 'compositefield',
                defaultMargins : '0 20px 0 0',
                items : [ this.numberFieldCursoActualCrdPrimera, this.checkBoxCursoActualOtraBecaConcedida ]
            },
                this.numberFieldCursoActualCrdSegunda, this.numberFieldCursoActualCrdTercera, this.numberFieldCursoActualCrdTerceraPosteriores, this.numberFieldCursoActualCrdCuartaPosteriores]
        });
    }
});
