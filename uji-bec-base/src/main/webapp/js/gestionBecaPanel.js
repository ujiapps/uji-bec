Ext.ns('UJI.BEC.BecaPanel');

UJI.BEC.GestionBecaPanel = Ext.extend(Ext.Panel,
{
    title : 'xxx',
    layout : 'vbox',
    region : 'center',
    becaId : 0,
    solicitanteId : 0,
    autoScroll: true,
    organismoId : 0,
    cursoAca : 0,
    convocatoria : '',
    nombreApellidos : '',
    titulacion : '',
    frame : true,
    closable : true,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.GestionBecaPanel.superclass.initComponent.call(this);

        this.initUI();

        this.relayEvents(this.datosGeneralesTab, [ 'UJI.BEC.DatosGeneralesTab.buttonGuardar.click' ]);
        this.relayEvents(this.academicosTab, [ 'UJI.BEC.AcademicosTab.buttonCalcularAcademicos.click' ]);
        this.relayEvents(this.miembrosTab, [ 'UJI.BEC.MiembrosTab.botonGuardar.click' ]);
        this.relayEvents(this.cuantiasTab, [ 'UJI.BEC.CuantiasTab.botonCuantias.click' ]);
        this.relayEvents(this.academicosTab, [ 'UJI.BEC.AcademicosTab.buttonCalcularAcademicosAnterior.click' ]);
    },

    initUI : function()
    {
        this.buildDatosGeneralesTab();
        this.buildAcademicosTab();
        this.buildEstudiosTab();
        this.buildDomiciliosTab();
        this.buildMiembrosTab();
        this.buildRecursosTab();
        this.buildOtrosDatosTab();
        this.buildDeclaranteTab();
        this.buildEconomicoTab();
        this.buildCuantiasTab();
        this.buildDenegacionesTab();
        this.buildHistoricoTab();
        this.buildTitulosPoseeTab();
        this.buildDocumentosTab();
        this.buildReclamacionesTab();

        this.buildPanelTitulo();
        this.buildPanelTabs();

        this.add(this.panelTitulo);
        this.add(this.panelTabs);
    },

    listenersDisabled :
    {
        'UJI.BEC.DatosGeneralesTab.buttonGuardar.click' : function()
        {
            this.historicoTab.storeHistoricoEstados.reload();
        },
        'UJI.BEC.AcademicosTab.buttonCalcularAcademicos.click' : function()
        {
            this.datosGeneralesTab.cargaFormulario();
            this.denegacionesTab.storeBecasDenegaciones.reload();
            this.cuantiasTab.storeCuantiasBecas.reload();
            this.historicoTab.storeHistoricoEstados.reload();
        },
        'UJI.BEC.MiembrosTab.botonGuardar.click' : function()
        {
            this.datosGeneralesTab.cargaFormulario();
        },
        'UJI.BEC.CuantiasTab.botonCuantias.click' : function()
        {
            this.datosGeneralesTab.cargaFormulario();
            this.historicoTab.storeHistoricoEstados.reload();
        },
        'UJI.BEC.AcademicosTab.buttonCalcularAcademicosAnterior.click' : function()
        {
            this.estudiosTab.cargaFormulario();
        }
    },

    buildPanelTitulo : function()
    {
        var titulo = this.cursoAca + ' - ' + this.convocatoria + ' - ' + this.nombreApellidos;
        this.title = titulo;
        titulo = titulo + ' - ' + this.titulacion;

        this.panelTitulo = new Ext.Panel(
        {
            height : 40,
            padding : 10,
            html : '<h3>' + titulo + '</h3>'
        });
    },

    buildPanelTabs : function()
    {
        var ref = this;
        this.panelTabs = new Ext.TabPanel(
        {
            activeTab : 0,
            flex : 1,
            enableTabScroll : true,
            items : [ this.datosGeneralesTab, this.academicosTab, this.titulosPoseeTab, this.estudiosTab, this.domiciliosTab, this.miembrosTab, this.declaranteTab,
                      this.economicoTab, this.otrosDatosTab, this.documentosTab, this.cuantiasTab, this.denegacionesTab, this.historicoTab, this.reclamacionesTab, this.recursosTab ],
            listeners :
            {
                tabChange : function(panel, tab)
                {
                    if (typeof tab.loadStore === 'function') {
                        tab.loadStore();
                    }

                    if (tab.name === "declaranteTab")
                    {
                        tab.load(ref.solicitanteId);
                    }
                    else if (tab.name === "recursosTab")
                    {
                        tab.load(ref.becaId);
                    }
                    else if (tab.name === "economicoTab")
                    {
                        tab.load(ref.solicitanteId, ref.convocatoria);
                    }
                }
            }
        });
    },

    buildDatosGeneralesTab : function()
    {
        this.datosGeneralesTab = new UJI.BEC.DatosGeneralesTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildTitulosPoseeTab : function()
    {
        this.titulosPoseeTab = new UJI.BEC.TitulosPoseeTab(
            {
                becaId : this.becaId,
                solicitanteId : this.solicitanteId
            });
    },

    buildRecursosTab : function()
    {
        this.recursosTab = new UJI.BEC.RecursosTab(
        {
            becaId : this.becaId
        });
    },

    buildAcademicosTab : function()
    {
        this.academicosTab = new UJI.BEC.AcademicosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildEstudiosTab : function()
    {
        this.estudiosTab = new UJI.BEC.EstudiosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildDomiciliosTab : function()
    {
        this.domiciliosTab = new UJI.BEC.DomiciliosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildDocumentosTab : function()
    {
        this.documentosTab = new UJI.BEC.DocumentosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildReclamacionesTab : function()
    {
        this.reclamacionesTab = new UJI.BEC.ReclamacionesTab(
            {
                becaId : this.becaId,
                solicitanteId : this.solicitanteId
            });
    },

    buildMiembrosTab : function()
    {
        this.miembrosTab = new UJI.BEC.MiembrosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildDeclaranteTab : function()
    {
        this.declaranteTab = new UJI.BEC.DeclaranteTab(
        {
            name : 'declaranteTab',
            solicitanteId : this.solicitanteId
        });
    },

    buildEconomicoTab : function()
    {
        this.economicoTab = new UJI.BEC.EconomicoTab(
        {
            name : 'economicoTab',
            solicitanteId : this.solicitanteId
        });
    },

    buildOtrosDatosTab : function()
    {
        this.otrosDatosTab = new UJI.BEC.OtrosDatosTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildCuantiasTab : function()
    {
        this.cuantiasTab = new UJI.BEC.CuantiasTab(
        {
            becaId : this.becaId,
            solicitanteId : this.solicitanteId
        });
    },

    buildDenegacionesTab : function()
    {
        this.denegacionesTab = new UJI.BEC.DenegacionesTab(
        {
            becaId : this.becaId,
            organismoId : this.organismoId
        });
    },

    buildHistoricoTab : function()
    {
        this.historicoTab = new UJI.BEC.HistoricoTab(
        {
            becaId : this.becaId
        });
    }
});