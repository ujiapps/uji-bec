Ext.ns('UJI.BEC');

UJI.BEC.TabMaestroTipo = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : '',
    url : '',
    record : '',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 300,
    frame : true,

    solicitanteId : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.TabMaestroTipo.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreTipo();

        this.buildEditor();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildGridTipo();

        this.add(this.gridTipo);
    },


    buildStoreTipo : function()
    {
        this.storeTipo = this.getStore(
            {
                url : this.url,
                record : this.record,
                id : 'id',
                fieldList : [ { name : 'id', type : 'int'}, 'nombre', 'orden' ],
                listeners :
                {
                    save : function(store, batch, data)
                    {
                        if (!data.create) {
                            store.reload();
                        }
                    }
                }
            });
    },

    loadData: function() {
        this.storeTipo.reload();
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            itemId : 'delete',
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var tipoSeleccionado = ref.gridTipo.getSelectionModel().getSelected();
                        if (!tipoSeleccionado)
                        {
                            return false;
                        }
                        ref.storeTipo.remove(tipoSeleccionado);
                    }
                });
            }
        });
    },

    buildAddButton : function(ref)
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : "Afegir",
            iconCls : 'application-add',
            handler : function(btn, ev)
            {
                var registroTipo = new ref.storeTipo.recordType({});
                ref.editor.stopEditing();
                ref.storeTipo.insert(0, registroTipo);
                ref.editor.startEditing(0);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var grid = ref.gridTipo;
                    var fila = grid.getStore().getAt(rowIndex);
                    var estoyActualizando = (fila.get('id'));
                    if (estoyActualizando)
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildGridTipo : function()
    {
        var ref = this;
        var columnModel =  new Ext.grid.ColumnModel({
            defaults :
            {
                width : 120,
                sortable : true
            },
            columns : [ {
                header : 'Id',
                width : 15,
                hidden : false,
                dataIndex : 'id',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            },
            {
                header : 'Nom',
                width : 55,
                hidden : false,
                dataIndex : 'nombre',
                editable : true,
                sortable : true,
                editor : new Ext.form.TextField({})
            },
            {
                header : 'Ordre',
                width : 15,
                hidden : false,
                dataIndex : 'orden',
                editable : true,
                sortable : true,
                editor : new Ext.form.NumberField({})
            } ]
        });

        this.gridTipo = new Ext.grid.GridPanel(
        {
            store : this.storeTipo,
            frame : true,
            autoScroll : true,
            width : 500,
            height : 400,
            loadMask : true,
            plugins: [ this.editor ],
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            colModel : columnModel,
            tbar : [ this.addButton, "-", this.deleteButton ]
        });
    }
});