Ext.ns('UJI.BEC');
UJI.BEC.FicherosUpload = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Resultats Organismes',
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.FicherosUpload.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildFicheroConselleriaField();
        this.buildPanelUploadConselleria();

        this.buildStoreConvocatoria();
        this.buildStoreCursoAcademico();
        this.buildStoreCursoAcademicoActivo();
        this.buildStoreListaLotes();

        this.buildComboCursoAcademico();
        this.buildComboConvocatoria();
        this.buildGridComboConvocatoria();

        this.buildButtonBuscar();
        this.buildButtonLimpiarFiltros();
        this.buildButtonProcesarLote();

        this.buildPanelFiltros();
        this.buildPanelGrid();

        this.add(this.panelUploadConselleria);
        this.add(this.panelFiltros);
        this.add(this.panelGrid)
    },

    buildFicheroConselleriaField : function()
    {
        this.ficheroConselleriaField = new Ext.form.TextField(
        {
            fieldLabel : 'Carregar',
            name : 'ficheroConselleria',
            allowBlank : false,
            inputType : 'file'
        });
    },

    buildPanelUploadConselleria : function()
    {
        var ref = this;
        this.panelUploadConselleria = new Ext.form.FormPanel(
        {
            fileUpload : true,
            enctype : 'multipart/form-data',
            frame : true,
            title : 'Fitxers Conselleria',
            bodyStyle : 'padding: 10px 10px 0 10px;',
            labelWidth : 100,
            defaults :
            {
                anchor : '50%',
                allowBlank : false
            },
            items : [ this.ficheroConselleriaField ],
            buttons : [
            {
                text : 'Enviar',
                handler : function()
                {
                    var formulario = ref.panelUploadConselleria.getForm();
                    formulario.errorReader = new Ext.data.XmlReader(
                    {
                        record : 'responseMessage',
                        success : 'success'
                    }, [ 'success', 'msg' ]);

                    formulario.submit(
                    {
                        method : 'post',
                        url : '/bec/rest/conselleria/respuesta',
                        waitMsg : 'Pujant arxiu',
                        success : function(form, action)
                        {
                            var xml = action.response.responseXML;
                            var responseMessage = Ext.DomQuery.jsSelect('responseMessage', xml);
                            var mensaje = Ext.DomQuery.selectValue('msg', responseMessage[0]);
                            Ext.MessageBox.alert('Estat', 'S\'han processat ' + mensaje + ' beques correctament');
                        },
                        failure : function(form, action)
                        {
                            var xml = action.response.responseXML;
                            var responseMessage = Ext.DomQuery.jsSelect('responseMessage', xml);
                            var mensaje = Ext.DomQuery.selectValue('msg', responseMessage[0]);

                            Ext.MessageBox.alert('Estat', mensaje);
                        }
                    });
                }
            } ]
        });
    },

    buildStoreCursoAcademicoActivo : function()
    {
        var ref = this;

        this.storeCursoAcademicoActivo = new Ext.data.Store(
        {
            url : '/bec/rest/cursoacademico/activo',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAcademico',
                id : 'id'
            }, [ 'id' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    ref.comboCursoAcademico.setValue(records[0].id);
                }
            }
        });

    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico/',
            record : 'CursoAcademico',
            autoLoad : true,
            id : 'id',
            fieldList : [ 'id', 'activo' ]
        });
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
        {
            url : '/bec/rest/convocatoria/ministerio',
            record : 'Convocatoria',
            autoLoad : true,
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreListaLotes : function()
    {
        var barraProgreso = new Object();

        this.storeListaLotes = new Ext.data.Store(
        {
            restful : true,
            url : '/bec/rest/loteministerio',
            reader : new Ext.data.XmlReader(
            {
                record : 'LoteMinisterio',
                idProperty : 'id'
            }, [ 'id',
            {
                name : 'loteId',
                type : 'int'
            }, 'cursoAcademicoId', 'convocatoriaId',
            {
                name : 'fecha',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'multienvioId',
                type : 'int'
            }, 'estado' ]),
            writer : new Ext.ux.uji.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'id', 'loteId', 'cursoAcademicoId', 'convocatoriaId', 'fecha', 'multienvioId', 'estado' ]),
            listeners :
            {
                beforesave : function(store, data)
                {
                    barraProgreso = Ext.MessageBox.show(
                    {
                        msg : 'Guardant dades, per favor espera...',
                        width : 300,
                        wait : true,
                        waitConfig :
                        {
                            interval : 100
                        }
                    });

                },
                save : function(store, batch, data)
                {
                    barraProgreso.hide();
                }
            }
        });
    },

    buildComboCursoAcademico : function()
    {
        var ref = this;

        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            fieldLabel : 'Curs',
            triggerAction : 'all',
            editable : false,
            allowBlank : false,
            forceSelection : true,
            width : 80,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            hiddenName : 'cursoAcademicoId',
            store : ref.storeCursoAcademico
        });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildGridComboConvocatoria : function()
    {
        this.comboGridConvocatoria = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildButtonBuscar : function()
    {
        var ref = this;
        this.buttonBuscar = new Ext.Button(
        {
            text : 'Recerca',
            width : 80,
            iconCls : 'magnifier',
            handler : function(button, event)
            {
                ref.recargaBusqueda();
            }
        });
    },

    recargaBusqueda : function()
    {
        var ref = this;

        ref.storeListaLotes.load(
        {
            params : ref.panelFiltros.getForm().getFieldValues(),
            callback : function(record, options, success)
            {
                if (success && this.getTotalCount() == 0)
                {
                    Ext.Msg.show(
                    {
                        title : 'Filtres',
                        msg : 'No s\'ha trobat cap registre',
                        modal : false,
                        icon : Ext.Msg.INFO,
                        buttons : Ext.Msg.OK
                    });
                }
                else if (!success)
                {
                    Ext.Msg.show(
                    {
                        title : 'Filtres',
                        msg : 'Nombre excessiu de lots. Defineix mes filtres',
                        modal : false,
                        icon : Ext.Msg.INFO,
                        buttons : Ext.Msg.OK
                    });
                }
            }
        });
    },

    buildButtonLimpiarFiltros : function()
    {
        var ref = this;
        this.buttonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            margins : '0 0 0 25px',
            handler : function(button, event)
            {
                ref.panelFiltros.getForm().reset();
            }
        });
    },

    buildButtonProcesarLote : function()
    {
        var ref = this;
        this.buttonProcesarLote = new Ext.Button(
        {
            text : 'Processar Lot',
            iconCls : 'email',
            disabled : true,
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                loteId = rec.get('id');
                multienvioId = rec.get('multienvioId');

                ref.procesarLoteMinisterio(multienvioId);
            }
        });
    },

    procesarLoteMinisterio : function(multienvioId)
    {
        var ref = this;

        var barraProgreso = Ext.MessageBox.show(
        {
            msg : 'Processant enviament, per favor espera...',
            width : 300,
            wait : true,
            waitConfig :
            {
                interval : 100
            }
        });

        Ext.Ajax.request(
        {
            url : '/bec/rest/ministerio/resoluciones/procesalote/' + multienvioId,
            method : 'GET',
            success : function(response, opts)
            {
                barraProgreso.hide();

                if (response.responseXML)
                {
                    var msgList = response.responseXML.getElementsByTagName("msg");

                    if (msgList && msgList[0] && msgList[0].firstChild)
                    {
                        alert(msgList[0].firstChild.nodeValue);
                    }
                }

                ref.recargaBusqueda();
                ref.buttonProcesarLote.setDisabled(true);
            },
            failure : function()
            {
                barraProgreso.hide();
            }
        });
    },

    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Lots Ministeri',
            layout : 'table',
            frame : true,
            padding : '5px 0px',
            labelWidth : 60,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 4
            },
            items : [
            {
                layout : 'form',
                items : [ this.comboCursoAcademico ]
            },
            {
                layout : 'form',
                labelWidth : 90,
                items : [ this.comboConvocatoria ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonBuscar ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonLimpiarFiltros ]
            } ]
        });
    },

    buildPanelGrid : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.panelGrid = new Ext.grid.GridPanel(
        {
            store : ref.storeListaLotes,
            frame : true,
            autoScroll : true,
            flex : 1,
            loadMask : true,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            viewConfig :
            {
                forceFit : true
            },
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Lot',
                    width : 40,
                    dataIndex : 'loteId'
                },
                {
                    header : 'Curs',
                    width : 50,
                    sortable : true,
                    dataIndex : 'cursoAcademicoId'
                },
                {
                    header : 'Convocatoria',
                    width : 80,
                    dataIndex : 'convocatoriaId',
                    renderer : Ext.util.Format.comboRenderer(ref.comboGridConvocatoria)
                },
                {
                    header : 'Data recepció',
                    width : 95,
                    dataIndex : 'fecha',
                    xtype : 'datecolumn',
                    format : 'd/m/Y'
                },
                {
                    header : 'Multienvio',
                    width : 40,
                    dataIndex : 'multienvioId'
                },
                {
                    header : 'Estat',
                    width : 100,
                    dataIndex : 'estado'
                } ]
            }),
            tbar :
            {
                items : [ this.buttonProcesarLote ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, event)
                {
                    var filaSeleccionada = ref.panelGrid.getSelectionModel().getSelected();
                    var loteId = filaSeleccionada.get("id");

                    var estadoFila = filaSeleccionada.get("estado");

                    if (estadoFila == 'CARREGAT')
                    {
                        ref.buttonProcesarLote.setDisabled(false);
                    }
                    else
                    {
                        ref.buttonProcesarLote.setDisabled(true);
                    }
                }
            }
        });
    }
});