Ext.ns('UJI.BEC');

UJI.BEC.Alta = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Altes',
    closable : true,
    autoScroll : true,
    layout : 'vbox',
    becaId : '',
    altaForm : '',
    altaButton : '',
    guardarButton : '',
    bajarButton : '',
    irABecaButton : '',
    storeCursoAcademicoActivo : '',
    comboCursoAcademico : '',
    storeCursoAcademico : '',
    comboConvocatoria : '',
    storeConvocatoria : '',
    comboEstudio : '',
    storeEstudio : '',
    comboConvocatoriaDatosBeca : '',
    datosBecaForm : '',
    datosAlumnoForm : '',
    isValidDNI : false,
    fieldIdentificacionAlta : '',
    fieldIconoPersonaValida : '',
    fieldArchivoTemporalAlta : '',
    iconoUsuarioOk : '/bec/img/icon/user-exist-16x16.png',
    iconoUsuarioNoOk : '/bec/img/icon/user-not-exist-16x16.png',
    mensajeUsuarioValido : '',
    mensajeUsuarioNoValido : 'Usuari no vàlid',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Alta.superclass.initComponent.call(this);

        this.addEvents('validateDNI');

        this.initUI();

        this.addEvents('newtab');

        this.comboEstudio.relayEvents(this, [ 'validateDNI' ]);
        this.altaButton.relayEvents(this, [ 'validateDNI' ]);
        this.fieldIconoPersonaValida.relayEvents(this, [ 'validateDNI' ]);
    },

    initUI : function()
    {
        this.buildStoreCursoAcademicoActivo();
        this.buildStoreCursoAcademico();
        this.buildStoreConvocatoria();
        this.buildStoreEstudio();
        this.buildComboConvocatoria();
        this.buildComboCursoAcademico();
        this.buildComboEstudio();
        this.buildComboConvocatoriaDatosBeca();
        this.buildAltaForm();
        this.buildDatosBecaForm();
        this.buildDatosAlumnoForm();

        this.add(this.altaForm);
        this.add(this.datosBecaForm);
        this.add(this.datosAlumnoForm);
    },

    buildStoreCursoAcademicoActivo : function()
    {
        var ref = this;

        this.storeCursoAcademicoActivo = new Ext.data.Store(
        {
            url : '/bec/rest/cursoacademico/activo',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAcademico',
                id : 'id'
            }, [ 'id' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    ref.comboCursoAcademico.setValue(records[0].id);
                }
            }
        });

    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = new Ext.data.Store(
        {
            url : '/bec/rest/cursoacademico',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAcademico',
                id : 'id'
            }, [ 'id' ])
        });
    },

    buildComboCursoAcademico : function()
    {
        var ref = this;

        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            fieldLabel : 'Curs Acadèmic',
            name : 'cursoAcademico',
            forceSelection : true,
            allowBlank : false,
            blankText : 'Camp obligatori',
            valueField : 'id',
            displayField : 'id',
            hiddenName : 'cursoAcademicoId',
            triggerAction : 'all',
            store : this.storeCursoAcademico,
            oldValue : '',
            listeners :
            {
                beforeselect : function()
                {
                    this.oldValue = this.getValue();
                },
                select : function(combo, newValue, oldValue)
                {
                    if (this.oldValue != this.getValue())
                    {
                        if (ref.isValidDNI)
                        {
                            ref.comboEstudio.reset();
                            ref.storeEstudio.setBaseParam('cursoAcademicoId', ref.comboCursoAcademico.getValue());
                            ref.storeEstudio.load();
                        }
                    }
                }
            }
        });
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = new Ext.data.Store(
        {
            url : '/bec/rest/convocatoria/activa',
            restful : true,
            autoload : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Convocatoria',
                id : 'id'
            }, [ 'id', 'nombre' ])
        });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            forceSelection : true,
            allowBlank : false,
            blankText : 'Camp obligatori',
            name : 'convocatoria',
            valueField : 'id',
            displayField : 'nombre',
            hiddenName : 'convocatoriaId',
            triggerAction : 'all',
            store : this.storeConvocatoria
        });
    },

    buildComboConvocatoriaDatosBeca : function()
    {
        this.comboConvocatoriaDatosBeca = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            forceSelection : true,
            allowBlank : false,
            blankText : 'Camp obligatori',
            name : 'convocatoria',
            valueField : 'id',
            displayField : 'nombre',
            hiddenName : 'convocatoriaId',
            triggerAction : 'all',
            store : this.storeConvocatoria
        });
    },

    buildStoreEstudio : function()
    {
        var ref = this;
        this.storeEstudio = new Ext.data.Store(
        {
            url : '/bec/rest/estudio',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Estudio',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            listeners :
            {
                beforeload : function(store, options)
                {
                    return ref.isValidDNI;
                },
                load : function(store, records, options)
                {
                    if (store.getTotalCount() == 1)
                    {
                        ref.comboEstudio.setValue(records[0].id);
                    }
                }
            }
        });
    },

    buildComboEstudio : function()
    {
        var ref = this;

        this.comboEstudio = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estudi',
            name : 'estudio',
            width : 440,
            valueField : 'id',
            displayField : 'nombre',
            hiddenName : 'estudioId',
            triggerAction : 'all',
            store : this.storeEstudio,
            clearData : function()
            {
                this.store.removeAll();
                this.clearValue();
            },
            listeners :
            {
                validateDNI : function(params)
                {
                    if (ref.isValidDNI)
                    {
                        this.store.setBaseParam('personaId', params.persona.id);

                        if (ref.comboCursoAcademico.getValue())
                        {
                            this.store.setBaseParam('cursoAcademicoId', ref.comboCursoAcademico.getValue());
                        }

                        this.store.load();
                    }
                    else
                    {
                        this.clearData();
                    }
                }
            }
        });
    },

    buildAltaButton : function()
    {
        var ref = this;

        this.altaButton = new Ext.Button(
        {
            text : 'Alta/Recerca',
            disabled : true,
            handler : function(button, event)
            {
                if (!ref.isValidDNI)
                {
                    return false;
                }

                if (ref.altaForm.getForm().isValid())
                {
                    ref.altaForm.getForm().submit(
                    {
                        url : '/bec/rest/solicitante/',
                        method : 'post',
                        success : function(form, action)
                        {
                            var beca = Ext.DomQuery.selectNode('Solicitante', action.response.responseXML);
                            ref.becaId = Ext.DomQuery.selectValue('becaId', beca);
                            var solicitanteId = Ext.DomQuery.selectValue('solicitanteId', beca);

                            ref.datosBecaForm.getForm().load(
                            {
                                url : '/bec/rest/beca/' + ref.becaId,
                                waitMsg : 'Per favor, espereu...',
                                waitTitle : 'Carregant formulari',
                                method : 'GET'
                            });

                            ref.altaForm.clearFields();
                            ref.altaButton.disable();
                            ref.bajarButton.enable();

                            ref.cargaFormularioDatosAlumno(ref, solicitanteId);

                        },
                        failure : function()
                        {
                            alert('Error al guardar les dades.');
                        }
                    });
                }
            },
            listeners :
            {
                validateDNI : function(params)
                {
                    this.setDisabled(!ref.isValidDNI);
                }
            }
        });
    },

    cargaFormularioDatosAlumno : function(referencia, solicitanteId)
    {
        referencia.datosAlumnoForm.getForm().load(
        {
            url : '/bec/rest/solicitante/' + solicitanteId,
            waitMsg : 'Per favor, espereu...',
            waitTitle : 'Carregant formulari',
            method : 'GET'
        });

        referencia.irABecaButton.enable();
    },

    guardaDatosBeca : function()
    {
        var ref = this;

        if (ref.datosBecaForm.getForm().isValid())
        {
            ref.datosBecaForm.getForm().submit(
            {
                url : '/bec/rest/beca',
                method : 'post',
                waitMsg : 'Per favor, espereu...',
                waitTitle : 'Guardant Dades',
                success : function(form, action)
                {
                    ref.fieldIdentificacionAlta.focus();
                },
                failure : function()
                {
                    alert('Error al guardar les dades.');
                }
            });
        }
    },

    buildGuardarButton : function()
    {
        var ref = this;

        this.guardarButton = new Ext.Button(
        {
            text : 'Desa',
            width : 70,
            handler : function(button, event)
            {
                ref.guardaDatosBeca();
            }
        });
    },

    buildBajarButton : function()
    {
        var ref = this;
        this.bajarButton = new Ext.Button(
        {
            text : 'Baixar dades',
            disabled : true,
            handler : function(button, event)
            {
                var form = ref.datosBecaForm.getForm();
                var solicitanteId = form.getValues()['solicitanteId'];
                var convocatoriaId = form.getValues()['convocatoriaId'];
                var codigoArchivoTemporal = form.getValues()['codigoArchivoTemporal'];
                var identificacion = form.getValues()['identificacion'];
                var cursoAcademico = form.getValues()['cursoAcademicoId'];

                ref.guardaDatosBeca();

                var barraProgreso = Ext.MessageBox.show(
                {
                    msg : 'Baixant dades del Ministeri, per favor espera...',
                    width : 300,
                    wait : true,
                    waitConfig :
                    {
                        interval : 100
                    }
                });

                Ext.Ajax.request(
                {
                    url : '/bec/rest/beca/' + ref.becaId + '/ministerio',
                    method : 'POST',
                    params :
                    {
                        solicitanteId : solicitanteId,
                        convocatoriaId : convocatoriaId,
                        codigoArchivoTemporal : codigoArchivoTemporal,
                        identificacion : identificacion,
                        cursoAcademico : cursoAcademico
                    },
                    success : function(result, request)
                    {
                        barraProgreso.hide();
                        ref.cargaFormularioDatosAlumno(ref, solicitanteId);
                    },
                    failure : function()
                    {
                        barraProgreso.hide();
                    }
                });
            }
        });
    },

    buildIrABecaButton : function()
    {
        var ref = this;

        this.irABecaButton = new Ext.Button(
        {
            text : 'Anar a la beca',
            disabled : true,
            handler : function(button, event)
            {
                var becaId = ref.datosBecaForm.getForm().getValues().id;
                var solicitanteId = ref.datosBecaForm.getForm().getValues().solicitanteId;
                var convocatoria = ref.datosBecaForm.getForm().getValues().nombreConvocatoria;
                var nombreApellidos = ref.datosBecaForm.getForm().getValues().nombrePersona;
                var cursoAca = ref.datosBecaForm.getForm().getValues().cursoAcademicoId;
                var titulacion = ref.datosBecaForm.getForm().getValues().estudioNombre;

                ref.fireEvent('newtab',
                {
                    becaId : becaId,
                    solicitanteId : solicitanteId,
                    convocatoria : convocatoria,
                    nombreApellidos : nombreApellidos,
                    cursoAca : cursoAca,
                    titulacion : titulacion,
                    pantalla : 'UJI.BEC.GestionBecaPanel'
                });
            }
        });
    },

    buildFieldArchivoTemporalAlta : function()
    {
        this.fieldArchivoTemporalAlta = new Ext.form.TextField(
        {
            fieldLabel : 'Arxiu temporal',
            name : 'codigoArchivoTemporal'
        });
    },

    buildFieldIconoPersonaValida : function()
    {
        var ref = this;

        this.fieldIconoPersonaValida = new Ext.form.Label(
        {
            data :
            {
                iconUser : this.iconoUsuarioNoOk,
                textUser : this.mensajeUsuarioNoValido
            },
            tpl : new Ext.Template('<img style="vertical-align: middle" src="{iconUser}" /> {textUser}'),
            listeners :
            {
                validateDNI : function(params)
                {
                    this.update(
                    {
                        iconUser : (ref.isValidDNI) ? ref.iconoUsuarioOk : ref.iconoUsuarioNoOk,
                        textUser : (ref.isValidDNI) ? ref.mensajeUsuarioValido : ref.mensajeUsuarioNoValido
                    });
                }
            }
        });
    },

    buildFieldIdentificacionAlta : function()
    {
        var ref = this;

        this.buildFieldIconoPersonaValida();

        this.fieldIdentificacionAlta = new Ext.form.TextField(
        {
            fieldLabel : 'Identificació UJI',
            name : 'identificacion',
            allowBlank : false,
            blankText : 'Camp obligatori',
            enableKeyEvents : true,
            listeners :
            {
                keyup : function(field)
                {
                    if (field.getValue().length > 5)
                    {
                        Ext.Ajax.request(
                        {
                            url : '/bec/rest/persona',
                            method : 'GET',
                            success : function(response)
                            {
                                var nodePersona = Ext.DomQuery.selectNode('Persona', response.responseXML);
                                var params = {};

                                if (nodePersona != undefined)
                                {
                                    var dni = '';
                                    var personaId = '';

                                    dni = Ext.DomQuery.selectValue('identificacion', nodePersona);
                                    personaId = Ext.DomQuery.selectValue('id', nodePersona);
                                    ref.mensajeUsuarioValido = Ext.DomQuery.selectValue('nombre', nodePersona);

                                    ref.isValidDNI = (dni.toUpperCase() == field.getValue().toUpperCase());
                                    params =
                                    {
                                        'persona' :
                                        {
                                            'id' : personaId,
                                            'dni' : dni
                                        }
                                    };
                                }
                                else
                                {
                                    ref.isValidDNI = false;
                                }

                                ref.fireEvent('validateDNI', params);
                            },
                            failure : function(response)
                            {
                                alert('No s\'ha pogut validar el DNI');

                                ref.isValidDNI = false;
                                ref.fireEvent('validateDNI');
                            },
                            params :
                            {
                                dni : field.getValue()
                            }
                        });
                    }
                    else
                    {
                        ref.isValidDNI = false;
                        ref.fireEvent('validateDNI');
                    }

                },

                blur : function(field)
                {
                    if (field.getValue().length > 5)
                    {
                        Ext.Ajax.request(
                        {
                            url : '/bec/rest/persona',
                            method : 'GET',
                            success : function(response)
                            {
                                var nodePersona = Ext.DomQuery.selectNode('Persona', response.responseXML);
                                var params = {};

                                if (nodePersona != undefined)
                                {
                                    var dni = '';
                                    var personaId = '';

                                    dni = Ext.DomQuery.selectValue('identificacion', nodePersona);
                                    personaId = Ext.DomQuery.selectValue('id', nodePersona);
                                    ref.mensajeUsuarioValido = Ext.DomQuery.selectValue('nombre', nodePersona);

                                    ref.isValidDNI = (dni.toUpperCase() == field.getValue().toUpperCase());
                                    params =
                                    {
                                        'persona' :
                                        {
                                            'id' : personaId,
                                            'dni' : dni
                                        }
                                    };
                                }
                                else
                                {
                                    ref.isValidDNI = false;
                                }

                                ref.fireEvent('validateDNI', params);
                            },
                            failure : function(response)
                            {
                                alert('No s\'ha pogut validar el DNI');

                                ref.isValidDNI = false;
                                ref.fireEvent('validateDNI');
                            },
                            params :
                            {
                                dni : field.getValue()
                            }
                        });
                    }
                    else
                    {
                        ref.isValidDNI = false;
                        ref.fireEvent('validateDNI');
                    }

                }
            }
        });
    },

    buildAltaForm : function()
    {
        var ref = this;

        this.buildAltaButton();
        this.buildFieldIdentificacionAlta();
        this.buildFieldArchivoTemporalAlta();

        this.altaForm = new Ext.form.FormPanel(
        {
            title : 'Alta/Recerca beques',
            layout : 'table',
            frame : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI'
            }, [ 'cursoAcademicoId', 'identificacion', 'convocatoriaId', 'codigoArchivoTemporal', 'estudioId' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                tableAttrs :
                {
                    style :
                    {
                        width : '750px'
                    }
                },
                columns : 2
            },
            items : [
            {
                layout : 'form',
                items : [ ref.comboCursoAcademico ]
            },
            {
                layout : 'form',
                items : [ ref.comboConvocatoria ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ new Ext.form.CompositeField(
                {
                    items : [ this.fieldIdentificacionAlta, this.fieldIconoPersonaValida ]
                }) ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ ref.comboEstudio ]
            },
            {
                layout : 'form',
                colspan : 2,
                items : [ ref.fieldArchivoTemporalAlta ]
            },
            {
                layout : 'form',
                colspan : 2,
                style : 'width:90px; margin-left:auto;',
                items : [ ref.altaButton ]
            } ],
            clearFields : function()
            {
                ref.comboEstudio.clearData();
                ref.fieldArchivoTemporalAlta.reset();
                ref.fieldIdentificacionAlta.reset();
                ref.isValidDNI = false;
                ref.fieldIconoPersonaValida.update(
                {
                    iconUser : this.iconoUsuarioNoOk,
                    textUser : this.mensajeUsuarioNoValido
                });
            }
        });
    },

    buildDatosBecaForm : function()
    {
        var ref = this;

        this.buildBajarButton();
        this.buildGuardarButton();

        this.datosBecaForm = new Ext.form.FormPanel(
        {
            title : 'Dades beca',
            layout : 'table',
            frame : true,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },

            layoutConfig :
            {
                tableAttrs :
                {
                    style :
                    {
                        width : '750px'
                    }
                },
                columns : 3
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'Beca'
            }, [ 'cursoAcademicoId', 'convocatoriaId', 'identificacion', 'estudioNombre', 'codigoArchivoTemporal', 'numeroBecaUji', 'nombrePersona', 'id', 'solicitanteId', 'identificacionUji',
                    'nombreConvocatoria' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            items : [
            {
                layout : 'form',
                defaultType : 'textfield',
                items : [
                {
                    name : 'id',
                    hidden : true,
                    readOnly : true
                },
                {
                    name : 'solicitanteId',
                    hidden : true,
                    readOnly : true
                },
                {
                    name : 'nombreConvocatoria',
                    hidden : true,
                    readOnly : true
                },
                {
                    fieldLabel : 'Curs acadèmic',
                    name : 'cursoAcademicoId',
                    width : '50',
                    readOnly : true,
                    fieldClass : 'x-item-disabled'
                } ]
            },
            {
                layout : 'form',
                items : [ ref.comboConvocatoriaDatosBeca ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    name : 'identificacionUji',
                    fieldLabel : 'Identificació UJI',
                    width : 100,
                    readOnly : true,
                    fieldClass : 'x-item-disabled'
                } ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'N. beca',
                    name : 'numeroBecaUji',
                    width : '50',
                    readOnly : true,
                    fieldClass : 'x-item-disabled'
                } ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Arxiu temporal',
                    name : 'codigoArchivoTemporal',
                    width : 175
                } ]
            },
            {
                layout : 'form',
                items : [
                {
                    xtype : 'textfield',
                    name : 'identificacion',
                    fieldLabel : 'Identificació Beca',
                    width : 100,
                    allowBlank : false,
                    blankText : 'Campo obligatorio'
                } ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [
                {
                    xtype : 'textfield',
                    name : 'nombrePersona',
                    width : 450,
                    fieldLabel : 'Alumne',
                    readOnly : true,
                    fieldClass : 'x-item-disabled'
                } ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [
                {
                    xtype : 'textfield',
                    name : 'estudioNombre',
                    width : 450,
                    fieldLabel : 'Estudi',
                    readOnly : true,
                    fieldClass : 'x-item-disabled'
                } ]
            },
            {
                colspan : 3,
                style : 'width:180px; margin-left:auto;',
                items : [
                {
                    xtype : 'compositefield',
                    items : [ ref.guardarButton, ref.bajarButton ]
                } ]
            } ]
        });
    },

    buildDatosAlumnoForm : function()
    {
        var ref = this;

        this.buildIrABecaButton();

        this.datosAlumnoForm = new Ext.form.FormPanel(
        {
            title : 'Dades alumne',
            layout : 'table',
            frame : true,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                tableAttrs :
                {
                    style :
                    {
                        width : '750px'
                    }
                },
                columns : 3
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'Solicitante'
            }, [ 'email', 'telefono1', 'telefono2', 'numeroMiembrosComputables', 'numeroHermanosFuera', 'profesionSustentador', 'numeroHermanos', 'minusvalia', 'orfandad', 'tipoFamiliaId',
                    'tipoFamilia', 'id' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            items : [
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 70px',
                    width : '150'
                },
                items : [
                {
                    xtype : 'textfield',
                    vtype : 'email',
                    name : 'email',
                    fieldLabel : 'Email',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 70px',
                    width : '90'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'telefono1',
                    fieldLabel : 'Telèfon 1',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 70px',
                    width : '90'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'telefono2',
                    fieldLabel : 'Telèfon 2',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 90px',
                    width : '50'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'numeroMiembrosComputables',
                    fieldLabel : 'Membres comp.',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 90px',
                    width : '50'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'numeroHermanos',
                    fieldLabel : 'Num. Germans',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 80px',
                    width : '50'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'numeroHermanosFuera',
                    fieldLabel : 'Germans fora',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 80px',
                    width : '150'
                },
                items : [
                {
                    xtype : 'textfield',
                    name : 'tipoFamilia',
                    fieldLabel : 'Tipus Familia',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 70px',
                    width : '85'
                },
                items : [
                {
                    xtype : 'checkbox',
                    name : 'orfandad',
                    fieldLabel : 'Orfandat',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                defaults :
                {
                    labelStyle : 'width: 70px',
                    width : '85'
                },
                items : [
                {
                    xtype : 'checkbox',
                    name : 'minusvalia',
                    fieldLabel : 'Minusvalidesa',
                    disabled : true
                } ]
            },
            {
                layout : 'form',
                colspan : 3,
                style : 'width: 110px; margin-left: auto;',
                items : [ ref.irABecaButton ]
            } ]
        });
    }
});
