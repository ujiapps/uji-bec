Ext.ns('UJI.BEC');

UJI.BEC.Tandas = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Tandes',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Tandas.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreCursoAcademico();
        this.buildStoreConvocatoria();
        this.buildStoreListaTandas();

        this.buildComboCursoAcademico();
        this.buildComboConvocatoria();

        this.buildGridComboCursoAcademico();
        this.buildGridComboConvocatoria();

        this.buildTextFieldTandaId();
        this.buildTextFieldDescripcion();

        this.buildButtonBuscar();
        this.buildButtonLimpiarFiltros();
        this.buildButtonAnadir();
        this.buildButtonBorrar();
        this.buildButtonEnviarNotificacion();
        this.buildButtonPrepararEnvio();
        this.buildButtonReiniciarTanda();
        this.buildButtonEnviarTandaAOrganismo();
        this.buildButtonProcesarResultadoEnvio();
        this.buildButtonPdfErrores();
        this.buildButtonPdfRecursos();

        this.buildCheckBoxActiva();
        this.buildCheckBoxGridActiva();

        this.buildEditor();

        this.buildPanelFiltros();
        this.buildPanelGrid();
        this.buildPanelResumen();

        this.add(this.panelFiltros);
        this.add(this.panelGrid);
        this.add(this.panelResumen);
    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico/',
            record : 'CursoAcademico',
            autoLoad : true,
            id : 'id',
            fieldList : [ 'id', 'activo' ]
        });
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
        {
            url : '/bec/rest/convocatoria',
            record : 'Convocatoria',
            autoLoad : true,
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreListaTandas : function()
    {
        var barraProgreso = new Object();

        this.storeListaTandas = new Ext.data.Store(
        {
            restful : true,
            url : '/bec/rest/tanda',
            reader : new Ext.data.XmlReader(
            {
                record : 'Tanda',
                idProperty : 'id'
            }, [ 'id',
            {
                name : 'tandaId',
                type : 'int'
            }, 'cursoAcademicoId', 'convocatoriaId', 'descripcion',
            {
                name : 'fecha',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }, 'activa',
            {
                name : 'fechaNotificacion',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaReunionJurado',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }]),
            writer : new Ext.ux.uji.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'id', 'tandaId', 'cursoAcademicoId', 'convocatoriaId', 'descripcion', 'fecha', 'activa', 'fechaNotificacion' ,'fechaReunionJurado']),
            listeners :
            {
                beforesave : function(store, data)
                {
                    barraProgreso = Ext.MessageBox.show(
                    {
                        msg : 'Guardant dades, per favor espera...',
                        width : 300,
                        wait : true,
                        waitConfig :
                        {
                            interval : 100
                        }
                    });

                },
                save : function(store, batch, data)
                {
                    barraProgreso.hide();
                }
            }
        });
    },

    buildComboCursoAcademico : function()
    {
        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            fieldLabel : 'Curs',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 60,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            store : this.storeCursoAcademico
        });
    },

    buildGridComboCursoAcademico : function()
    {
        this.comboGridCursoAcademico = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 80,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            store : this.storeCursoAcademico
        });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildGridComboConvocatoria : function()
    {
        this.comboGridConvocatoria = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeConvocatoria
        });
    },

    buildTextFieldTandaId : function()
    {
        this.textFieldTandaId = new Ext.form.TextField(
            {
                width : 60,
                fieldLabel : 'Tanda Id',
                name : 'tandaId'
            });
    },

    buildTextFieldDescripcion : function()
    {
        this.textFieldDescripcion = new Ext.form.TextField(
        {
            width : 200,
            fieldLabel : 'Descripció',
            name : 'descripcion'
        });
    },

    buildCheckBoxActiva : function()
    {
        this.checkBoxActiva = new Ext.form.Checkbox(
        {
            name : 'activa',
            fieldLabel : 'Activa'
        });
    },

    buildCheckBoxGridActiva : function()
    {
        this.checkBoxGridActiva = new Ext.form.Checkbox(
        {
            name : 'activa',
            width : 100,
            fieldLabel : 'Activa'
        });
    },

    buildButtonBuscar : function()
    {
        var ref = this;
        this.buttonBuscar = new Ext.Button(
        {
            text : 'Recerca',
            width : 80,
            iconCls : 'magnifier',
            handler : function(button, event)
            {
                ref.storeListaTandas.load(
                {
                    params : ref.panelFiltros.getForm().getFieldValues(),
                    callback : function(record, options, success)
                    {
                        if (success && this.getTotalCount() == 0)
                        {
                            Ext.Msg.show(
                            {
                                title : 'Filtres',
                                msg : 'No s\'ha trobat cap registre',
                                modal : false,
                                icon : Ext.Msg.INFO,
                                buttons : Ext.Msg.OK
                            });
                        }
                        else if (!success)
                        {
                            Ext.Msg.show(
                            {
                                title : 'Filtres',
                                msg : 'Nombre excessiu de tandes. Defineix mes filtres',
                                modal : false,
                                icon : Ext.Msg.INFO,
                                buttons : Ext.Msg.OK
                            });
                        }
                    }
                });
            }
        });
    },

    buildButtonLimpiarFiltros : function()
    {
        var ref = this;
        this.buttonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            margins : '0 0 0 25px',
            handler : function(button, event)
            {
                ref.panelFiltros.getForm().reset();
            }
        });
    },

    buildButtonAnadir : function()
    {
        var ref = this;
        this.buttonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                var rec = new ref.panelGrid.store.recordType(
                {
                    id : '',
                    tandaId : '',
                    convocatoriaId : '',
                    cursoAcademicoId : '',
                    descripcion : '',
                    fecha : '',
                    activa : ''
                });

                ref.editor.stopEditing();
                ref.panelGrid.store.insert(0, rec);
                ref.editor.startEditing(0, true);
            }
        });
    },

    buildButtonBorrar : function()
    {
        var ref = this;
        this.buttonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                Ext.MessageBox.show(
                {
                    title : 'Esborrar tanda?',
                    msg : 'Atenció!\n ' + 'vols esborrar la tanda seleccionada?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            ref.panelGrid.store.remove(rec);
                        }
                    }
                });
            }
        });
    },

    buildButtonEnviarNotificacion : function()
    {
        var ref = this;
        this.buttonEnviarNotificacion = new Ext.Button(
        {
            text : 'Enviar Notificacions',
            iconCls : 'email',
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                Ext.MessageBox.show(
                {
                    title : 'Notificacions',
                    msg : 'Atenció!\n ' + 'Vols fer les notificaciones de aquesta tanda?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            tandaId = rec.get('id');
                            ref.enviaNotificacion(tandaId);
                        }
                    }
                });
            }
        });
    },

    enviaNotificacion : function(tandaId)
    {
        var ref = this;
        var barraProgreso = Ext.MessageBox.show(
        {
            msg : 'Processant notificacions per favor espere...',
            width : 300,
            wait : true,
            waitConfig :
            {
                interval : 100
            }
        });
        Ext.Ajax.request(
        {
            url : '/bec/rest/tanda/' + tandaId + '/notifica',
            method : 'POST',
            success : function(response, opts)
            {
                barraProgreso.hide();
                if (response.responseXML)
                {
                    var msgList = response.responseXML.getElementsByTagName("msg");
                    if (msgList && msgList[0] && msgList[0].firstChild)
                    {
                        alert(msgList[0].firstChild.nodeValue);
                    }
                }

                ref.storeListaTandas.reload();

            },
            failure : function()
            {
                barraProgreso.hide();
            }
        });
    },

    buildButtonReiniciarTanda : function()
    {
        var ref = this;
        this.buttonReiniciarTanda = new Ext.Button(
            {
                text : 'Reiniciar tanda',
                iconCls : 'building-go',
                disabled : true,
                width : 80,
                style :
                    {
                        marginLeft : '20px'
                    },
                handler : function(button, event)
                {
                    var rec = ref.panelGrid.getSelectionModel().getSelected();
                    if (!rec)
                    {
                        return false;
                    }

                    var convocatoriaConselleriaGeneralId = "3";
                    var convocatoriaId = rec.get("convocatoriaId");

                    if (convocatoriaId == convocatoriaConselleriaGeneralId)
                    {
                        Ext.MessageBox.show(
                            {
                                title : 'Tandas',
                                msg : 'Atenció!\n La data de la tanda s\'eliminarà, i totes les beques es passaran a estat treballat. Està segur?',
                                buttons : Ext.Msg.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(button, text, opt)
                                {
                                    if (button == 'yes')
                                    {
                                        var tandaId = rec.get('id');
                                        ref.reiniciarTanda(tandaId);
                                    }
                                }
                            });
                    }
                    else
                    {
                        Ext.MessageBox.alert('Estat', 'Només poden reiniciar-se tandes de conselleria');
                    }
                }
            });
    },

    buildButtonPrepararEnvio : function()
    {
        var ref = this;
        this.buttonPrepararEnvio = new Ext.Button(
        {
            text : 'Preparar enviament',
            iconCls : 'email',
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }

                var convocatoriaId = rec.get("convocatoriaId");
                var convocatoriasConselleria= [3, 9, 7, 10, 12, 11];

                if (convocatoriasConselleria.includes(convocatoriaId))
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Tandas',
                        msg : 'Atenció!\n ' + 'Es prepararà l\'enviament per a enviar a l\'organisme. Està segur?',
                        buttons : Ext.Msg.YESNO,
                        icon : Ext.MessageBox.QUESTION,
                        fn : function(button, text, opt)
                        {
                            if (button == 'yes')
                            {
                                tandaId = rec.get('id');

                                ref.prepararEnvio(tandaId);
                            }
                        }
                    });
                }
                else
                {
                    // TANDA DE CONSELLERIA General y Beca Salario

                    var tandaId = rec.get('id');
                    if (tandaId != null && tandaId != '')
                    {
                        // QUITAR!!!!!
                        window.open('/bec/rest/conselleria/tanda/' + tandaId + '/propuesta');
                        return;

                        if (rec.get("fecha"))
                        {
                            Ext.Msg.show(
                            {
                                title : 'Error',
                                msg : 'La tanda seleccionada ja ha sigut emesa amb data ' + rec.get("fecha").format("d/m/Y"),
                                modal : true,
                                icon : Ext.Msg.ERROR,
                                buttons : Ext.Msg.OK
                            });

                        }
                        else
                        {
                            if (rec.get("activa") !== "false")
                            {
                                Ext.Msg.show(
                                {
                                    title : 'Error',
                                    msg : 'La tanda seleccionada encara és activa',
                                    modal : true,
                                    icon : Ext.Msg.ERROR,
                                    buttons : Ext.Msg.OK
                                });
                            }
                            else
                            {
                                window.open('/bec/rest/conselleria/tanda/' + tandaId + '/propuesta');
                            }
                        }
                    }
                }
            }
        });
    },

    prepararEnvio : function(tandaId)
    {
        var barraProgreso = Ext.MessageBox.show(
        {
            msg : 'Calculant enviament, per favor espera...',
            width : 300,
            wait : true,
            waitConfig :
            {
                interval : 100
            }
        });

        Ext.Ajax.request(
        {
            url : '/bec/rest/ministerio/webservices/tanda/' + tandaId + '/preparar',
            method : 'POST',
            success : function(response, opts)
            {
                barraProgreso.hide();

                if (response.responseXML)
                {
                    var msgList = response.responseXML.getElementsByTagName("msg");

                    if (msgList && msgList[0] && msgList[0].firstChild)
                    {
                        alert(msgList[0].firstChild.nodeValue);
                    }
                }
            },
            failure : function()
            {
                barraProgreso.hide();
            }
        });
    },

    buildButtonEnviarTandaAOrganismo : function()
    {
        var ref = this;
        this.buttonEnviarTandaAOrganismo = new Ext.Button(
        {
            text : 'Enviar tanda',
            disabled : true,
            iconCls : 'email',
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                Ext.MessageBox.show(
                {
                    title : 'Tandas',
                    msg : 'Atenció!\n ' + 'La tanda s\'enviarà al ministeri. Està segur?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(button, text, opt)
                    {
                        if (button == 'yes')
                        {
                            tandaId = rec.get('id');

                            ref.enviaTandaAOrganismo(tandaId);
                        }
                    }
                });
            }
        });
    },

    enviaTandaAOrganismo : function(tandaId)
    {
        var barraProgreso = Ext.MessageBox.show(
        {
            msg : 'Enviant tanda, per favor espera...',
            width : 300,
            wait : true,
            waitConfig :
            {
                interval : 100
            }
        });

        Ext.Ajax.request(
        {
            url : '/bec/rest/ministerio/webservices/tanda/' + tandaId + '/cargar',
            method : 'POST',
            success : function(response, opts)
            {
                barraProgreso.hide();

                Ext.Msg.show(
                {
                    title : 'Enviament',
                    msg : 'Enviament realitzat',
                    modal : false,
                    icon : Ext.Msg.INFO,
                    buttons : Ext.Msg.OK
                });
            },
            failure : function()
            {
                barraProgreso.hide();
            }
        });
    },

    buildButtonProcesarResultadoEnvio : function()
    {
        var ref = this;
        this.buttonProcesarResultadoEnvio = new Ext.Button(
        {
            text : 'Processar resultats',
            iconCls : 'email',
            disabled : true,
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                tandaId = rec.get('id');
                ref.procesarResultadoEnvio(tandaId);
            }
        });
    },

    procesarResultadoEnvio : function(tandaId)
    {
        var barraProgreso = Ext.MessageBox.show(
        {
            msg : 'Processant resultat, per favor espera...',
            width : 300,
            wait : true,
            waitConfig :
            {
                interval : 100
            }
        });

        Ext.Ajax.request(
        {
            url : '/bec/rest/ministerio/webservices/tanda/' + tandaId + '/procesar',
            method : 'POST',
            success : function(response, opts)
            {
                barraProgreso.hide();
                var resultado = Ext.DomQuery.selectNode('BecasExtraidas', response.responseXML);
                var resultadoTexto = Ext.DomQuery.selectValue('resultado', resultado);
                Ext.Msg.show(
                {
                    title : 'Resultats',
                    msg : resultadoTexto,
                    modal : false,
                    icon : Ext.Msg.INFO,
                    buttons : Ext.Msg.OK
                });
            },
            failure : function()
            {
                barraProgreso.hide();
            }
        });
    },

    reiniciarTanda : function(tandaId)
    {
        var ref = this;

        var barraProgreso = Ext.MessageBox.show(
            {
                msg : 'Processant resultat, per favor espera...',
                width : 300,
                wait : true,
                waitConfig :
                    {
                        interval : 100
                    }
            });

        Ext.Ajax.request(
            {
                url : '/bec/rest/tanda/' + tandaId + '/reinicia',
                method : 'POST',
                success : function(response, opts)
                {
                    barraProgreso.hide();
                    ref.storeListaTandas.reload();
                },
                failure : function()
                {
                    barraProgreso.hide();
                }
            });
    },

    buildButtonPdfErrores : function()
    {
        var ref = this;
        this.buttonPdfErrores = new Ext.Button(
        {
            text : 'Llistat Errors',
            iconCls : 'printer',
            disabled : true,
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.show(
                    {
                        title : 'Error',
                        msg : 'Cal seleccionar una tanda',
                        modal : false,
                        icon : Ext.Msg.INFO,
                        buttons : Ext.Msg.OK
                    });
                }

                var tandaId = rec.get('id');

                if (tandaId != null && tandaId != '')
                {
                    window.open('http://' + window.location.host + '/bec/rest/ministerio/webservices/tanda/' + tandaId + '/errores/pdf');
                }
            }
        });
    },

    buildButtonPdfRecursos : function()
    {
        var ref = this;
        this.buttonPdfRecursos = new Ext.Button(
        {
            text : 'Llistat recursos',
            iconCls : 'printer',
            disabled : true,
            width : 80,
            style :
            {
                marginLeft : '20px'
            },
            handler : function(button, event)
            {
                var rec = ref.panelGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.show(
                    {
                        title : 'Error',
                        msg : 'Cal seleccionar una tanda',
                        modal : false,
                        icon : Ext.Msg.INFO,
                        buttons : Ext.Msg.OK
                    });
                }

                var tandaId = rec.get('id');

                if (tandaId != null && tandaId != '')
                {
                    window.open('/bec/rest/tanda/' + tandaId + '/recurso/pdf');
                }
            }
        });
    },

    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Filtres',
            layout : 'table',
            frame : true,
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 4
            },
            items : [
            {
                layout : 'form',
                items : [ this.comboCursoAcademico ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.comboConvocatoria ]
            },
            {
                layout : 'form',
                items : [ this.textFieldTandaId ]
            },
            {
                layout : 'form',
                items : [ this.textFieldDescripcion ]
            },
            {
                layout : 'form',
                items : [ this.checkBoxActiva ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonLimpiarFiltros ]
            },
            {
                layout : 'form',
                style : 'width:100px; margin-left:auto;',
                items : [ this.buttonBuscar ]
            } ]
        });
    },

    buildPanelResumen : function()
    {
        this.panelResumen = new Ext.Panel(
        {
            title : 'Resum estats',
            frame : true,
            height : 150
        });
    },

    cambiaUrlConvocatoria : function(activas)
    {
        if (activas == "si")
        {
            this.storeConvocatoria.proxy.conn.url = '/bec/rest/convocatoria/activa';
        }
        else
        {
            this.storeConvocatoria.proxy.conn.url = '/bec/rest/convocatoria';
        }
        this.storeConvocatoria.load();
    },

    buildEditor : function()
    {
        var ref = this;

        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var insertando = (ref.panelGrid.getStore().getAt(rowIndex).get('id') == '');
                    if (insertando)
                    {
                        ref.panelGrid.getColumnModel().columns[1].editor.setDisabled(false);
                        ref.panelGrid.getColumnModel().columns[2].editor.setDisabled(false);
                        ref.panelGrid.getColumnModel().columns[3].editor.setDisabled(false);
                    }
                    else
                    {
                        ref.panelGrid.getColumnModel().columns[1].editor.setDisabled(true);
                        ref.panelGrid.getColumnModel().columns[2].editor.setDisabled(true);
                        ref.panelGrid.getColumnModel().columns[3].editor.setDisabled(true);
                    }
                    ref.cambiaUrlConvocatoria("si");
                },
                afteredit : function(rowEditor, rowIndex)
                {
                    ref.cambiaUrlConvocatoria("no");
                },
                canceledit : function(rowEditor, rowIndex)
                {
                    ref.cambiaUrlConvocatoria("no");
                }
            }
        });
    },

    habilitaOdeshabilitaBotones : function(rec)
    {
        var convocatoriasConselleria= ["3", "9", "7", "10", "12", "11"];
        var convocatoriaId = rec.get("convocatoriaId");

        if (convocatoriasConselleria.includes(convocatoriaId))
        {
            this.buttonEnviarTandaAOrganismo.setDisabled(true);
            this.buttonProcesarResultadoEnvio.setDisabled(true);
            this.buttonPdfErrores.setDisabled(true);
            this.buttonPdfRecursos.setDisabled(true);
            this.buttonReiniciarTanda.setDisabled(false);
        }
        else
        {
            this.buttonEnviarTandaAOrganismo.setDisabled(false);
            this.buttonProcesarResultadoEnvio.setDisabled(false);
            this.buttonPdfErrores.setDisabled(false);
            this.buttonPdfRecursos.setDisabled(false);
            this.buttonReiniciarTanda.setDisabled(true);
        }
    },

    buildPanelGrid : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.panelGrid = new Ext.grid.GridPanel(
        {
            store : ref.storeListaTandas,
            frame : true,
            autoScroll : true,
            flex : 1,
            loadMask : true,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true,
                listeners :
                {
                    rowselect : function(sm, row, rec)
                    {
                        ref.habilitaOdeshabilitaBotones(rec);

                        var tandaId = rec.get("id");

                        var resumenesJson = [];

                        function actualizaPanelResumen(resumenesJson)
                        {
                            var resumenPlantilla = new Ext.XTemplate('<table>', '<tpl for=".">', '<tr>', '<td width="160">{etiqueta}:</td>', '<td>{valor}</td>', '</tr>', '</tpl>', '</table>');
                            if (resumenesJson && resumenesJson.length > 0)
                            {
                                var contenido = resumenPlantilla.apply(resumenesJson);
                                ref.panelResumen.update(contenido);
                                ref.panelResumen.getEl().unmask();
                            }
                            else
                            {
                                ref.panelResumen.update("");
                                ref.panelResumen.getEl().unmask();
                            }
                        }

                        function setResumenJson(resumenEtiqueta, resumenValor)
                        {
                            if (resumenEtiqueta && resumenValor)
                            {
                                resumenesJson.push(
                                    {
                                        etiqueta : resumenEtiqueta,
                                        valor : resumenValor
                                    });
                            }
                        }

                        function convierteXMLaJSON(response)
                        {
                            var xml = response.responseXML;
                            var resumenesXml = Ext.DomQuery.jsSelect('resumen', xml);

                            for ( var i = 0, len = resumenesXml.length; i < len; i++)
                            {
                                var resumenEtiqueta = Ext.DomQuery.selectValue('etiqueta', resumenesXml[i]);
                                var resumenValor = Ext.DomQuery.selectValue('valor', resumenesXml[i]);
                                setResumenJson(resumenEtiqueta, resumenValor);
                            }
                        }

                        if (tandaId != null && tandaId != "")
                        {
                            ref.panelResumen.getEl().mask("Carregant dades...");
                            Ext.Ajax.request(
                                {
                                    url : '/bec/rest/estado/resumenportanda/' + tandaId,
                                    success : function(response, opts)
                                    {
                                        convierteXMLaJSON(response);

                                        Ext.Ajax.request(
                                            {
                                                url : '/bec/rest/tanda/' + tandaId + '/becassinnotificar',
                                                success : function(response, opts)
                                                {
                                                    convierteXMLaJSON(response);

                                                    actualizaPanelResumen(resumenesJson);
                                                }
                                            });
                                    }
                                });
                        }
                    }
                }
            }),
            plugins : [ this.editor ],
            viewConfig :
            {
                forceFit : true
            },
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Tanda',
                    width : 40,
                    dataIndex : 'tandaId',
                    editor : new Ext.form.NumberField(
                    {
                        allowDecimals : false
                    })
                },
                {
                    header : 'Curs',
                    id : 'cursoAca',
                    width : 50,
                    sortable : true,
                    dataIndex : 'cursoAcademicoId',
                    editor : ref.comboGridCursoAcademico
                },
                {
                    header : 'Convocatoria',
                    width : 80,
                    dataIndex : 'convocatoriaId',
                    editor : ref.comboGridConvocatoria,
                    renderer : Ext.util.Format.comboRenderer(ref.comboGridConvocatoria)
                },
                {
                    header : 'Descripció',
                    width : 200,
                    dataIndex : 'descripcion',
                    editor : new Ext.form.TextField({})
                },
                {
                    header : 'Data enviament',
                    width : 95,
                    dataIndex : 'fecha',
                    xtype : 'datecolumn',
                    format : 'd/m/Y',
                    editor : new Ext.form.DateField(
                    {
                        startDay : 1,
                        format : 'd/m/Y'
                    })
                },
                {
                    header : 'Activa',
                    dataIndex : 'activa',
                    xtype : 'booleancolumn',
                    trueText : 'Sí',
                    falseText : 'No',
                    width : 80,
                    editor : this.checkBoxGridActiva
                },
                {
                    header : 'Data Notificació',
                    width : 95,
                    dataIndex : 'fechaNotificacion',
                    xtype : 'datecolumn',
                    format : 'd/m/Y H:i:s'
                },
                {
                    header : 'Data Reunió Jurat',
                    width : 95,
                    dataIndex : 'fechaReunionJurado',
                    xtype : 'datecolumn',
                    format : 'd/m/Y',
                    editor : new Ext.form.DateField(
                    {
                        startDay : 1,
                        format : 'd/m/Y'
                    })
                }]
            }),
            tbar :
            {
                items : [ this.buttonAnadir, this.buttonBorrar, this.buttonEnviarNotificacion, this.buttonPrepararEnvio, this.buttonEnviarTandaAOrganismo, this.buttonProcesarResultadoEnvio,
                        this.buttonPdfErrores, this.buttonPdfRecursos, this.buttonReiniciarTanda ]
            }
        });
    }
});