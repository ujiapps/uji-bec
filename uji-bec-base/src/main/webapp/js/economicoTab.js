Ext.ns('UJI.BEC');

UJI.BEC.EconomicoTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Econòmics',
    layout : 'form',
    closable : false,
    autoScroll : true,
    width : 600,
    frame : true,
    padding : '2px 0px',

    solicitanteId : 0,
    economicoId : 0,
    convocatoria : 0,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.EconomicoTab.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreEconomico();

        this.buildEconomicoId();
        this.buildSolicitanteId();

        this.buildComboIndicadorBecario();
        this.buildComboIndicadorCompensatoria();
        this.buildComboIndicadorTasas();
        this.buildComboIndicadorMovilidadGeneral();
        this.buildComboIndicadorMovilidadEspecial();
        this.buildComboIndicadorResidencia();
        this.buildComboIndicadorPatrimonio();
        this.buildComboIndicadorSinResidencia();

        this.buildInputUmbralBecario();
        this.buildInputUmbralCompensatoria();
        this.buildInputTasas();
        this.buildInputResidencia();
        this.buildInputPatrimonio();
        this.buildInputSinResidencia();
        this.buildInputMovilidadEspecial();
        this.buildInputRenta();
        this.buildInputCapitalMobiliario();
        this.buildInputVolumenNegocio();
        this.buildInputDeducciones();

        this.buildCompositeBecario();
        this.buildCompositeCompensatoria();
        this.buildCompositeResidencia();
        this.buildCompositePatrimonio();
        this.buildCompositeTasas();
        this.buildCompositeSinResidencia();
        this.buildCompositeMovilidadEspecial();

        this.buildFielSetGeneral();
        this.buildFielSetComunes();
        this.buildFielSetMovilidad();

        this.buildBotonGuardar();

        this.buildPanelFormulario();

        this.add(this.panelFormulario);
    },

    buildEconomicoId : function()
    {
        this.economicoId = new Ext.form.NumberField(
        {
            name : 'id',
            hidden : true
        });
    },

    buildSolicitanteId : function()
    {
        this.solicitanteId = new Ext.form.NumberField(
        {
            name : 'solicitanteId',
            value : this.solicitanteId,
            hidden : true
        });
    },

    buildStoreEconomico : function()
    {
        this.storeEconomico = new Ext.data.Store(
        {
            baseParams :
            {
                solicitanteId : this.solicitanteId
            },
            restful : true,
            url : '/bec/rest/economico',
            reader : new Ext.data.XmlReader(
            {
                record : 'Economico',
                id : 'id'
            }, [ 'id', 'capitalMobiliario', 'umbralMovGeneral', 'solicitanteId', 'indMovEspecial', 'indPatrimonio', 'umbralCompensa', 'indMovGeneral', 'indTasas', 'renta', 'umbralBecario',
                    'indBecario', 'umbralResidMov', 'umbralPatrimonio', 'coeficientePrelacion', 'indResidMov', 'codRepesca', 'umbralTasas', 'indCompensa', 'volumenNegocio', 'umbralMovEspecial',
                    'deducciones' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboIndicadorBecario : function()
    {
        this.comboIndicadorBecario = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            forceSelection : true,
            lazyRender : true,
            hiddenName : 'indBecario',
            width : 60,
            fieldLabel : 'Becari',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorCompensatoria : function()
    {
        this.comboIndicadorCompensatoria = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            hiddenName : 'indCompensa',
            fieldLabel : 'Compensatòria',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorTasas : function()
    {
        this.comboIndicadorTasas = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            hiddenName : 'indTasas',
            fieldLabel : 'Tases',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorMovilidadGeneral : function()
    {
        this.comboIndicadorMovilidadGeneral = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            fieldLabel : 'Mobilitat general',
            hiddenName : 'indMovGeneral',
            mode : 'local',
            allowBlank : true,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorMovilidadEspecial : function()
    {
        this.comboIndicadorMovilidadEspecial = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            fieldLabel : 'Mobilitat especial',
            hiddenName : 'indMovEspecial',
            mode : 'local',
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorResidencia : function()
    {
        this.comboIndicadorResidencia = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            fieldLabel : 'Residència',
            hiddenName : 'indResidMov',
            mode : 'local',
            allowBlank : true,
            forceSelection : false,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorSinResidencia : function()
    {
        this.comboIndicadorSinResidencia = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            fieldLabel : 'Sense Residència',
            hiddenName : 'indMovGeneral',
            mode : 'local',
            allowBlank : true,
            forceSelection : false,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildComboIndicadorPatrimonio : function()
    {
        this.comboIndicadorPatrimonio = new Ext.form.ComboBox(
        {
            typeAhead : true,
            triggerAction : 'all',
            lazyRender : true,
            width : 60,
            fieldLabel : 'Patrimoni',
            hiddenName : 'indPatrimonio',
            mode : 'local',
            allowBlank : true,
            forceSelection : false,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'name' ],
                data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
            }),
            valueField : 'id',
            displayField : 'name'
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Desa',
            width : 60,
            handler : function(button, event)
            {
                var id = ref.panelFormulario.getForm().getValues().id;

                if (id != '' && id != null)
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/economico/' + id,
                        method : 'put',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.load(ref.solicitanteId, ref.convocatoria)
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
                else
                {
                    ref.panelFormulario.getForm().submit(
                    {
                        url : '/bec/rest/economico/',
                        method : 'post',
                        waitMsg : 'Per favor, espereu...',
                        waitTitle : 'Guardant Dades',
                        success : function(form, action)
                        {
                            ref.load(ref.solicitanteId, ref.convocatoria);
                        },
                        failure : function(form, action)
                        {
                        }
                    });
                }
            }
        });

    },

    buildFielSetComunes : function()
    {
        this.fieldSetComunes = new Ext.form.FieldSet(
        {
            title : 'Indicadors comuns',
            layout : 'table',
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.compositeTasas ]
            },
            {
                layout : 'form',
                items : [ this.compositeResidencia ]
            },
            {
                layout : 'form',
                items : [ this.compositePatrimonio ]
            } ]
        });
    },

    buildFielSetGeneral : function()
    {
        this.fieldSetGeneral = new Ext.form.FieldSet(
        {
            title : 'Indicadors beca general',
            layout : 'table',
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.compositeBecario ]
            },
            {
                layout : 'form',
                items : [ this.compositeCompensatoria ]
            } ]
        });
    },

    buildFielSetMovilidad : function()
    {
        this.fieldSetMovilidad = new Ext.form.FieldSet(
        {
            title : 'Indicadors beca mobilitat',
            layout : 'table',
            padding : '5px 0px',

            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.compositeMovilidadEspecial ]
            },
            {
                layout : 'form',
                items : [ this.compositeSinResidencia ]
            } ]
        });
    },

    buildInputPatrimonio : function()
    {
        this.inputPatrimonio = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Patrimonio',
            name : 'umbralPatrimonio'
        });
    },

    buildInputRenta : function()
    {
        this.inputRenta = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Renta',
            name : 'renta'
        });
    },

    buildInputCapitalMobiliario : function()
    {
        this.inputCapitalMobiliario = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Capital mobiliari',
            name : 'capitalMobiliario'
        });
    },

    buildInputVolumenNegocio : function()
    {
        this.inputVolumenNegocio = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Volum de negoci',
            name : 'volumenNegocio'
        });
    },

    buildInputDeducciones : function()
    {
        this.inputDeducciones = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Deduccions',
            name : 'deducciones'
        });
    },

    buildInputResidencia : function()
    {
        this.inputResidencia = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Residencia',
            name : 'umbralResidMov'
        });
    },

    buildInputTasas : function()
    {
        this.inputTasas = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Tasas',
            name : 'umbralTasas'
        });
    },

    buildInputSinResidencia : function()
    {
        this.inputSinResidencia = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Sense residència',
            name : 'umbralMovGeneral'
        });
    },

    buildInputMovilidadEspecial : function()
    {
        this.inputMovilidadEspecial = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Mobilitat especial',
            name : 'umbralMovEspecial'
        });
    },

    buildInputUmbralCompensatoria : function()
    {
        this.inputUmbralCompensatoria = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Umbral compensatoria',
            name : 'umbralCompensa'
        });
    },

    buildCompositeTasas : function()
    {
        this.compositeTasas = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de taxes',
            items : [ this.comboIndicadorTasas, this.inputTasas ]
        });
    },

    buildCompositeSinResidencia : function()
    {
        this.compositeSinResidencia = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de mobilitat general sense residència',
            items : [ this.comboIndicadorSinResidencia, this.inputSinResidencia ]
        });
    },

    buildCompositeMovilidadEspecial : function()
    {
        this.compositeMovilidadEspecial = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de mobilitat especial',
            items : [ this.comboIndicadorMovilidadEspecial, this.inputMovilidadEspecial ]
        });
    },

    buildCompositePatrimonio : function()
    {
        this.compositePatrimonio = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de patrimoni',
            items : [ this.comboIndicadorPatrimonio, this.inputPatrimonio ]
        });
    },

    buildCompositeResidencia : function()
    {
        this.compositeResidencia = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de residència',
            items : [ this.comboIndicadorResidencia, this.inputResidencia ]
        });
    },

    buildCompositeCompensatoria : function()
    {
        this.compositeCompensatoria = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de compensatòria',
            items : [ this.comboIndicadorCompensatoria, this.inputUmbralCompensatoria ]
        });
    },

    buildInputUmbralBecario : function()
    {
        this.inputUmbralBecario = new Ext.form.NumberField(
        {
            width : 120,
            allowDecimals : true,
            decimalSeparator : ",",
            fieldLabel : 'Umbral becari',
            name : 'umbralBecario'
        });
    },

    buildCompositeBecario : function()
    {
        this.compositeBecario = new Ext.form.CompositeField(
        {
            width : 200,
            fieldLabel : 'Umbral de becari',
            items : [ this.comboIndicadorBecario, this.inputUmbralBecario ]
        });
    },

    buildPanelFormulario : function()
    {
        this.panelFormulario = new Ext.FormPanel(
        {
            layout : 'table',
            flex : 1,
            frame : true,
            width: 650,
            style : 'margin-right:16px;',
            labelAlign : 'top',
            reader : new Ext.data.XmlReader(
            {
                record : 'Miembro',
                id : 'id'
            }, [ 'id', 'capitalMobiliario', 'umbralMovGeneral', 'solicitanteId', 'indMovEspecial', 'indPatrimonio', 'umbralCompensa', 'indMovGeneral', 'indTasas', 'renta', 'umbralBecario',
                    'indBecario', 'umbralResidMov', 'umbralPatrimonio', 'coeficientePrelacion', 'indResidMov', 'codRepesca', 'umbralTasas', 'indCompensa', 'volumenNegocio', 'umbralMovEspecial',
                    'deducciones' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            padding : '5px 0px',
            layoutConfig :
            {
                columns : 4
            },
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            buttons : [ this.botonGuardar ],
            items : [
            {
                layout : 'form',
                items : [ this.inputRenta ]
            },
            {
                layout : 'form',
                items : [ this.inputCapitalMobiliario ]
            },
            {
                layout : 'form',
                items : [ this.inputVolumenNegocio ]
            },
            {
                layout : 'form',
                items : [ this.inputDeducciones ]
            },
            {
                colspan : 4,
                padding : 0,
                items : [ this.fieldSetComunes ]
            },
            {
                colspan : 4,
                padding : 0,
                items : [ this.fieldSetMovilidad ]
            },
            {
                colspan : 4,
                padding : 0,
                items : [ this.fieldSetGeneral ]
            },
            {
                layout : 'form',
                items : [ this.economicoId ]
            },
            {
                layout : 'form',
                items : [ this.solicitanteId ]
            } ]
        });
    },

    load : function(solicitanteId, convocatoria)
    {
        this.solicitanteId = solicitanteId;
        this.convocatoria = convocatoria;

        if (this.convocatoria == 'Mobilitat')
        {
            this.fieldSetGeneral.setVisible(false);
            this.fieldSetMovilidad.setVisible(true);
        }
        if (this.convocatoria == 'General')
        {
            this.fieldSetGeneral.setVisible(true);
            this.fieldSetMovilidad.setVisible(false);
        }
        var ref = this;
        this.storeEconomico.load(
        {
            solicitanteId : solicitanteId,
            callback : function(r)
            {
                if (r[0])
                {
                    ref.panelFormulario.getForm().loadRecord(r[0]);
                }
            }
        });
    }
});