Ext.ns('UJI.BEC');
UJI.BEC.Listados = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Listados',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Listados.superclass.initComponent.call(this);
        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreOrganismo();
        this.buildStoreCursoAcademico();
        this.buildStoreListado();

        this.buildComboCursoAcademico();
        this.buildComboOrganismo();
        this.buildComboListado();

        this.buildBotonGenerar();

        this.buildPanelListados();

        this.add(this.panelListados);
    },

    buildStoreListado : function()
    {
        var ref = this;
        this.storeListado = this.getStore(
        {
            url : '/bec/rest/listado/',
            record : 'Listado',
            id : 'id',
            fieldList : [ 'id', 'organismoId', 'nombre', 'url', 'activo' ],
            listeners :
            {
                beforeload : function()
                {
                    var organismoId = ref.comboOrganismo.getValue();
                    if (!organismoId)
                    {
                        Ext.Msg.show(
                        {
                            title : 'Error',
                            msg : 'Cal seleccionar un organisme',
                            modal : false,
                            icon : Ext.Msg.INFO,
                            buttons : Ext.Msg.OK
                        });
                    }
                    else
                    {
                        this.proxy.setUrl('/bec/rest/listado/organismo/' + organismoId);
                    }
                }
            }

        });
    },

    buildStoreOrganismo : function()
    {
        this.storeOrganismo = this.getStore(
        {
            url : '/bec/rest/organismo/',
            record : 'Organismo',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico/',
            record : 'CursoAcademico',
            id : 'id',
            fieldList : [ 'id', 'activo' ]
        });
    },

    buildComboCursoAcademico : function()
    {
        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable: false,
            fieldLabel : 'Curs Acadèmic',
            width : 90,
            name : 'cursoAcademicoId',
            hiddenName : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            store : this.storeCursoAcademico
        });
    },

    buildBotonGenerar : function()
    {
        var ref = this;
        this.botonGenerar = new Ext.Button(
        {
            text : 'Generar',
            width : 80,
            handler : function(button, event)
            {
                var organismoId = ref.comboOrganismo.getValue();
                var cursoAcademicoId = ref.comboCursoAcademico.getValue();
                var listadoId = ref.comboListado.getValue();
                if (!(organismoId&&cursoAcademicoId&&listadoId))
                {
                    Ext.Msg.show(
                        {
                            title : 'Error',
                            msg : 'Les tres dades són obligatòries',
                            modal : false,
                            icon : Ext.Msg.ERROR,
                            buttons : Ext.Msg.OK
                        });
                } else {
                    var registro = ref.storeListado.getById(listadoId);
                    var url = registro.get("url");
                    window.open(url.replace("{cursoAcademicoId}", cursoAcademicoId));
                }


            }
        });

    },

    buildComboOrganismo : function()
    {
        var ref = this;
        this.comboOrganismo = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable: false,
            fieldLabel : 'Organisme',
            width : 180,
            name : 'organismoId',
            hiddenName : 'organismoId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeOrganismo,
            listeners :
            {
                select : function(combo, record)
                {
                    ref.comboListado.reset();
                    ref.storeListado.reload();
                }
            }
        });
    },

    buildComboListado : function()
    {
        var ref = this;
        this.comboListado = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable: false,
            fieldLabel : 'Tipus de llistat',
            width : 300,
            name : 'listadoId',
            hiddenName : 'listadoId',
            valueField : 'id',
            displayField : 'nombre',
            store : this.storeListado
        });
    },

    buildPanelListados : function()
    {
        this.panelListados = new Ext.form.FormPanel(
        {
            title : 'Llistats',
            frame : true,
            padding : '5px 0px',
            labelWidth : 120,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            items : [ this.comboOrganismo, this.comboCursoAcademico, this.comboListado, this.botonGenerar ]
        });
    }

});