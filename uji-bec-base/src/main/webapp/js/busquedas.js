Ext.ns('UJI.BEC');

UJI.BEC.Busquedas = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Recerca',
    closable : true,
    layout : 'vbox',
    panelFiltros : {},
    panelGrid : {},
    comboCursoAcademico : {},
    comboConvocatoria : {},
    comboProceso : {},
    comboEstado : {},
    comboTitulacion : {},
    storeCursoAcademico : {},
    storeConvocatoria : {},
    storeProceso : {},
    storeEstado : {},
    storeTitulacion : {},
    storeBecas : {},
    textFieldNumeroBecaUji : {},
    textFieldCodigoArchivoTemporal : {},
    textFieldTanda : {},
    textFieldBecaId : {},
    textFieldPersonaId : {},
    textFieldIdentificacion : {},
    textFieldNombreApellidos : {},
    checkBoxSinTanda : {},
    checkBoxReclamada : {},
    gridBeca : {},
    buttonBuscar : {},
    buttonLimpiarFiltros : {},
    buttonIrABeca : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Busquedas.superclass.initComponent.call(this);

        this.initUI();
        this.addEvents('newtab');
    },

    initUI : function()
    {
        this.buildStoreCursoAcademico();
        this.buildStoreConvocatoria();
        this.buildStoreProceso();
        this.buildStoreEstado();
        this.buildStoreTitulacion();
        this.buildStoreBecas();

        this.buildButtonExportarGrid();
        this.buildComboCursoAcademico();
        this.buildComboConvocatoria();
        this.buildComboProceso();
        this.buildComboEstado();
        this.buildComboTitulacion();

        this.buildTextFieldNumeroBecaUji();
        this.buildTextFieldCodigoArchivoTemporal();
        this.buildTextFieldTanda();
        this.buildTextFieldBecaId();
        this.buildTextFieldPersonaId();
        this.buildTextFieldIdentificacion();
        this.buildTextFieldNombreApellidos();

        this.buildCheckBoxSinTanda();
        this.buildCheckBoxReclamada();

        this.buildButtonBuscar();
        this.buildButtonLimpiarFiltros();
        this.buildButtonIrABeca();

        this.buildPanelFiltros();
        this.buildPanelGrid();

        this.add(this.panelFiltros);
        this.add(this.panelGrid);
    },

    buildStoreCursoAcademico : function()
    {
        this.storeCursoAcademico = this.getStore(
        {
            url : '/bec/rest/cursoacademico/',
            record : 'CursoAcademico',
            id : 'id',
            fieldList : [ 'id', 'activo' ]
        });
    },

    buildStoreConvocatoria : function()
    {
        this.storeConvocatoria = this.getStore(
        {
            url : '/bec/rest/convocatoria',
            record : 'Convocatoria',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreProceso : function()
    {
        this.storeProceso = this.getStore(
        {
            url : '/bec/rest/proceso/',
            record : 'Proceso',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreEstado : function()
    {
        this.storeEstado = this.getStore(
        {
            url : '/bec/rest/estado/',
            record : 'Estado',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreTitulacion : function()
    {
        this.storeTitulacion = this.getStore(
        {
            url : '/bec/rest/estudio/',
            record : 'Estudio',
            id : 'id',
            fieldList : [ 'id', 'nombre' ]
        });
    },

    buildStoreBecas : function()
    {
        this.storeBecas = this.getStore(
        {
            url : '/bec/rest/beca/search',
            record : 'Beca',
            id : 'id',
            fieldList : [ 'id', 'solicitanteId', 'cursoAca', 'numeroBecaUji', 'nombreApellidos', 'identificacion', 'titulacion', 'convocatoria', 'proceso', 'estado', 'tandaOrdenId', 'organismoId'],
            listeners: {
                load: function() {
                    ref.gridBeca.getTopToolbar().excelButton.setHref('data:application/vnd.ms-excel;base64,' + Base64.encode(ref.gridBeca.getExcelXml()));
                }
            }
        });
    },

    buildComboCursoAcademico : function()
    {
        this.comboCursoAcademico = new Ext.form.ComboBox(
        {
            fieldLabel : 'Curs',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 80,
            name : 'cursoAcademicoId',
            valueField : 'id',
            displayField : 'id',
            editable : true,
            store : this.storeCursoAcademico
        });
    },

    buildComboConvocatoria : function()
    {
        this.comboConvocatoria = new Ext.form.ComboBox(
        {
            fieldLabel : 'Convocatòria',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 100,
            name : 'convocatoriaId',
            hiddenName : 'convocatoriaId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeConvocatoria
        });
    },

    buildComboProceso : function()
    {
        this.comboProceso = new Ext.form.ComboBox(
        {
            fieldLabel : 'Procés',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 150,
            name : 'procesoId',
            hiddenName : 'procesoId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeProceso
        });
    },

    buildComboEstado : function()
    {
        this.comboEstado = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estat',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            width : 160,
            name : 'estadoId',
            hiddenName : 'estadoId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeEstado
        });
    },

    buildComboTitulacion : function()
    {
        this.comboTitulacion = new Ext.form.ComboBox(
        {
            fieldLabel : 'Estudi',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            anchor : '100%',
            name : 'estudioId',
            hiddenName : 'estudioId',
            valueField : 'id',
            displayField : 'nombre',
            editable : true,
            store : this.storeTitulacion
        });
    },

    buildTextFieldNumeroBecaUji : function()
    {
        this.textFieldNumeroBecaUji = new Ext.form.TextField(
        {
            width : 80,
            fieldLabel : 'Beca UJI',
            name : 'numeroBecaUji'
        });
    },

    buildTextFieldCodigoArchivoTemporal : function()
    {
        this.textFieldCodigoArchivoTemporal = new Ext.form.TextField(
        {
            width : 100,
            fieldLabel : 'Arxiu temp.',
            name : 'codigoArchivoTemporal'
        });
    },

    buildTextFieldTanda : function()
    {
        this.textFieldTanda = new Ext.form.TextField(
        {
            width : 100,
            fieldLabel : 'Tanda',
            name : 'tandaOrdenId'
        });
    },

    buildTextFieldBecaId : function()
    {
        this.textFieldBecaId = new Ext.form.TextField(
        {
            width : 80,
            fieldLabel : 'Id Beca',
            name : 'id'
        });
    },
    buildTextFieldPersonaId : function()
    {
        this.textFieldPersonaId = new Ext.form.TextField(
        {
            width : 80,
            fieldLabel : 'Id persona',
            name : 'personaId'
        });
    },

    buildTextFieldIdentificacion : function()
    {
        this.textFieldIdentificacion = new Ext.form.TextField(
        {
            width : 210,
            minLength : 4,
            fieldLabel : 'Identificació',
            name : 'identificacion'
        });
    },

    buildTextFieldNombreApellidos : function()
    {
        this.textFieldNombreApellidos = new Ext.form.TextField(
        {
            width : 610,
            fieldLabel : 'Nom',
            name : 'nombrePersona'
        });
    },

    buildCheckBoxSinTanda : function()
    {
        var ref = this;
        this.checkBoxSinTanda = new Ext.form.Checkbox(
        {
            name : 'sinTanda',
            width : 100,
            boxLabel : 'Sense tanda',
            handler : function()
            {
                if (this.getValue())
                {
                    ref.textFieldTanda.reset();
                }
            }
        });
    },
    buildCheckBoxReclamada : function()
    {
        var ref = this;
        this.checkBoxReclamada = new Ext.form.Checkbox(
            {
                name : 'reclamada',
                width : 100,
                boxLabel : 'Reclamada',
            });
    },
    buildButtonBuscar : function()
    {
        var ref = this;
        this.buttonBuscar = new Ext.Button(
        {
            text : 'Recerca',
            width : 80,
            iconCls : 'magnifier',
            handler : function(button, event)
            {
                if (!ref.panelFiltros.getForm().isValid())
                {
                    return;
                }

                ref.storeBecas.baseParams = ref.panelFiltros.getForm().getFieldValues();
                ref.storeBecas.load(
                {
                    callback : function(record, options, success)
                    {
                        if (success && this.getTotalCount() == 0)
                        {
                            Ext.Msg.show(
                            {
                                title : 'Filtres',
                                msg : 'No s\'ha trobat cap registre',
                                modal : false,
                                icon : Ext.Msg.INFO,
                                buttons : Ext.Msg.OK
                            });
                        }
                    }
                });
            }
        });
    },

    abrirGestionBecas : function(filaSeleccionada)
    {
        if (filaSeleccionada)
        {
            var becaId = filaSeleccionada.get('id');
            var solicitanteId = filaSeleccionada.get('solicitanteId');
            var convocatoria = filaSeleccionada.get('convocatoria');
            var nombreApellidos = filaSeleccionada.get('nombreApellidos');
            var cursoAca = filaSeleccionada.get('cursoAca');
            var titulacion = filaSeleccionada.get('titulacion');
            var organismoId = filaSeleccionada.get('organismoId');
            ref.fireEvent('newtab',
            {
                becaId : becaId,
                solicitanteId : solicitanteId,
                convocatoria : convocatoria,
                organismoId : organismoId,
                nombreApellidos : nombreApellidos,
                cursoAca : cursoAca,
                titulacion : titulacion,
                pantalla : 'UJI.BEC.GestionBecaPanel'
            });
        }
    },

    buildButtonIrABeca : function()
    {
        ref = this;
        this.buttonIrABeca  = new Ext.Button(
        {
            text : 'Anar a la beca',
            iconCls : 'building-go',
            handler : function(bytton, event)
            {
                var filaSeleccionada = ref.gridBeca.getSelectionModel().getSelected();
                ref.abrirGestionBecas(filaSeleccionada);
            }
        });
    },

    buildButtonExportarGrid : function()
    {
        ref = this;
        this.buttonExportarGrid= new Ext.LinkButton(
            {
                text : 'Exportar dades',
                ref: 'excelButton'
            });
    },

    buildButtonLimpiarFiltros : function()
    {
        var ref = this;
        this.buttonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            margins : '0 0 0 25px',
            handler : function(button, event)
            {
                ref.panelFiltros.getForm().reset();
            }
        });
    },

    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Filtres',
            layout : 'table',
            frame : true,
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 3
            },
            items : [
            {
                layout : 'form',
                items : [ this.comboCursoAcademico ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.comboConvocatoria ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.comboProceso ]
            },
            {
                layout : 'form',
                items : [ this.textFieldNumeroBecaUji ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.textFieldCodigoArchivoTemporal ]
            },
            {
                layout : 'hbox',
                colspan : 2,
                items : [
                {
                    layout : 'form',
                    labelWidth : 80,
                    items : [ this.textFieldTanda ]
                },
                {
                    layout : 'form',
                    labelWidth : 20,
                    items : [ this.checkBoxSinTanda ]
                }
                ]
            },

            {
                layout : 'form',
                items : [ this.textFieldBecaId ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.textFieldPersonaId ]
            },
            {
                layout : 'form',
                labelWidth : 80,
                items : [ this.textFieldIdentificacion ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.textFieldNombreApellidos ]
            },
            {
                layout : 'form',
                colspan : 3,
                items : [ this.comboTitulacion ]
            },
        //    {
        //        layout : 'hbox',
       //        colspan : 2,
        //        items :  [

                    {
                        layout: 'form',
                        items: [this.comboEstado]
                    },
                    {
                        layout : 'form',
                        labelWidth : 20,
                        items : [ this.checkBoxReclamada ]
                    },
          //            ]
     //       },
            {
                layout : 'form',
   //             colspan : 2,
               // style : 'width:100px; margin-left:auto;',
                items : [ this.buttonBuscar ]
            },
            {
                    layout :'form',
                    items : [ this.buttonLimpiarFiltros]
            }]
        });
    },

    buildPanelGrid : function()
    {
        var ref = this;
        this.gridBeca = new Ext.grid.GridPanel(
        {
            store : ref.storeBecas,
            loadMask : true,
            title: 'Resultats de la cerca de beques',
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    width : 120,
                    sortable : true
                },
                columns : [
                {
                    id : 'cursoAca',
                    header : 'Curs',
                    width : 50,
                    sortable : true,
                    dataIndex : 'cursoAca'
                },
                {
                    header : 'Beca UJI',
                    width : 50,
                    dataIndex : 'numeroBecaUji'
                },
                {
                    header : 'Nom',
                    width : 200,
                    dataIndex : 'nombreApellidos'
                },
                {
                    header : 'Identificació',
                    width : 80,
                    dataIndex : 'identificacion'
                },
                {
                    header : 'Titulació',
                    width : 200,
                    dataIndex : 'titulacion'
                },
                {
                    header : 'Convocatoria',
                    width : 80,
                    dataIndex : 'convocatoria'
                },
                {
                    header : 'Procès',
                    width : 80,
                    dataIndex : 'proceso'
                },
                {
                    header : 'Estat',
                    width : 80,
                    dataIndex : 'estado'
                },
                {
                    header : 'Tanda',
                    width : 40,
                    dataIndex : 'tandaOrdenId'
                } ]
            }),
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            tbar :
            {
                items : [ this.buttonIrABeca, this.buttonExportarGrid ]
            },
            listeners :
            {
                dblclick : function(dv, record, item, index, e)
                {
                    var filaSeleccionada = ref.gridBeca.getSelectionModel().getSelected();
                    ref.abrirGestionBecas(filaSeleccionada);
                }
            }
        });

        this.panelGrid = new Ext.Panel(
        {
            layout : 'fit',
            frame : true,
            flex : 1,
            margins : '5 0 0 0',
            padding : 0,
            items : [ this.gridBeca ]
        });
    }
});