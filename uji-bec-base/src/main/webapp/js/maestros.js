Ext.ns('UJI.BEC');

UJI.BEC.Maestros = Ext.extend(Ext.ux.uji.ApplicationPanel,
{
    title : 'Mestres',
    closable : true,
    layout : 'vbox',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.BEC.Textos.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildTabMaestrosCursoAcademico();
        this.buildTabMaestrosConvocatoria();

        this.buildTabMaestrosTipoDomicilio();
        this.buildTabMaestrosTipoFamilia();
        this.buildTabMaestrosTipoIdentificacion();
        this.buildTabMaestrosTipoMatricula();
        this.buildTabMaestrosTipoMiembro();
        this.buildTabMaestrosTipoMinusvalia();
        this.buildTabMaestrosTipoMoneda();
        this.buildTabMaestrosTipoPropiedad();
        this.buildTabMaestrosTipoVia();
        this.buildTabMaestrosTipoDocumento();
        this.buildTabMaestrosEstadoCivil();
        this.buildTabMaestrosEstadoBeca();
        this.buildTabMaestrosOrganismo();
        this.buildTabMaestrosCuantia();
        this.buildTabMaestrosDenegacion();
        this.buildTabPanelMaestros();
        this.add(this.tabPanelMaestros);
    },

    buildTabMaestrosCursoAcademico : function()
    {
        this.tabMaestrosCursoAcademico = new UJI.BEC.TabMaestrosCursoAcademico();
    },

    buildTabMaestrosTipoDomicilio : function()
    {
        this.tabMaestrosTipoDomicilio = new UJI.BEC.TabMaestrosTipoDomicilio();
    },

    buildTabMaestrosTipoFamilia : function()
    {
        this.tabMaestrosTipoFamilia = new UJI.BEC.TabMaestrosTipoFamilia();
    },

    buildTabMaestrosTipoIdentificacion : function()
    {
        this.tabMaestrosTipoIdentificacion = new UJI.BEC.TabMaestrosTipoIdentificacion();
    },

    buildTabMaestrosTipoMatricula : function()
    {
        this.tabMaestrosTipoMatricula = new UJI.BEC.TabMaestrosTipoMatricula();
    },

    buildTabMaestrosTipoMiembro : function()
    {
        this.tabMaestrosTipoMiembro = new UJI.BEC.TabMaestrosTipoMiembro();
    },

    buildTabMaestrosTipoMinusvalia : function()
    {
        this.tabMaestrosTipoMinusvalia = new UJI.BEC.TabMaestrosTipoMinusvalia();
    },

    buildTabMaestrosTipoMoneda : function()
    {
        this.tabMaestrosTipoMoneda = new UJI.BEC.TabMaestrosTipoMoneda();
    },

    buildTabMaestrosTipoPropiedad : function()
    {
        this.tabMaestrosTipoPropiedad = new UJI.BEC.TabMaestrosTipoPropiedad();
    },
    
    buildTabMaestrosTipoVia : function()
    {
        this.tabMaestrosTipoVia = new UJI.BEC.TabMaestrosTipoVia();
    },
    
    buildTabMaestrosEstadoCivil : function()
    {
        this.tabMaestrosEstadoCivil = new UJI.BEC.TabMaestrosEstadoCivil();
    },

    buildTabMaestrosEstadoBeca : function()
    {
        this.tabMaestrosEstadoBeca = new UJI.BEC.TabMaestrosEstadoBeca();
    },

    buildTabMaestrosTipoDocumento : function()
    {
        this.tabMaestrosTipoDocumento = new UJI.BEC.TabMaestrosTipoDocumento();
    },

    buildTabMaestrosOrganismo : function()
    {
        this.tabMaestrosOrganismo = new UJI.BEC.TabMaestrosOrganismo();
    },

    buildTabMaestrosConvocatoria : function()
    {
        this.tabMaestrosConvocatoria = new UJI.BEC.TabMaestrosConvocatoria();
    },

    buildTabMaestrosCuantia : function()
    {
        this.tabMaestrosCuantia = new UJI.BEC.TabMaestrosCuantia();
    },

    buildTabMaestrosDenegacion : function()
    {
        this.tabMaestrosDenegacion = new UJI.BEC.TabMaestrosDenegacion();
    },

    buildTabPanelMaestros : function()
    {
        this.tabPanelMaestros = new Ext.TabPanel(
        {
            activeTab : 0,
            flex : 1,
            enableTabScroll : true,
            deferredRender : true,
            items : [ this.tabMaestrosCursoAcademico, this.tabMaestrosConvocatoria, this.tabMaestrosCuantia, this.tabMaestrosDenegacion, this.tabMaestrosTipoDomicilio, this.tabMaestrosEstadoBeca,
                    this.tabMaestrosEstadoCivil, this.tabMaestrosTipoFamilia, this.tabMaestrosTipoIdentificacion, this.tabMaestrosTipoMatricula, this.tabMaestrosTipoMiembro,
                    this.tabMaestrosTipoMinusvalia, this.tabMaestrosTipoMoneda, this.tabMaestrosTipoVia,this.tabMaestrosOrganismo, this.tabMaestrosTipoPropiedad, this.tabMaestrosTipoDocumento ],
            listeners :
            {
                'tabchange' : function(panel, tab)
                {
                    tab.loadData();
                }
            }

        });
    }
});