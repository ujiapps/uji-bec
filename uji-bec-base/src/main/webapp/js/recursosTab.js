Ext.ns('UJI.BEC');

UJI.BEC.RecursosTab = Ext.extend(Ext.ux.uji.ApplicationPanel,
    {
        title : 'Recursos',
        name : 'recursosTab',
        layout : 'form',
        closable : false,
        autoScroll : true,
        width : 600,
        frame : true,
        padding : '2px 0px',
        becaId : 0,
        recursoId : 0,

        initComponent : function()
        {
            var config = {};
            Ext.apply(this, Ext.apply(this.initialConfig, config));

            UJI.BEC.RecursosTab.superclass.initComponent.call(this);

            this.initUI();
        },

        initUI : function()
        {
            this.buildStoreRecursos();
            this.buildStoreAyudas();
            this.buildStoreAyudasReclamadas();
            this.buildStoreAyudasPropuestas();
            this.buildInputRecursoId ();
            this.buildInputBecaId();

            this.buildComboAyudasReclamadas();
            this.buildComboAyudasPropuestas();

            this.buildRowEditorAyudasReclamadas();
            this.buildRowEditorAyudasPropuestas();

            this.buildButtonAyudasReclamadasAnadir();
            this.buildButtonAyudasReclamadasBorrar();

            this.buildButtonAyudasPropuestasAnadir();
            this.buildButtonAyudasPropuestasBorrar();

            this.buildInputJustificacionPropuesta();

            this.buildTextFieldAnyosBecario();

            this.buildComboMatriculaCursoPosterior();
            this.buildComboCambioEstudios();
            this.buildComboEstimacion();

            this.buildBotonEliminar();
            this.buildBotonGuardar();
            this.buildBotonImprimir();
            this.buildBotonRecuperar();

            this.buildGridAyudasReclamadas();
            this.buildGridAyudasPropuestas();

            this.buildPanelFormulario();

            this.add(this.panelFormulario);
            this.add(this.gridAyudasReclamadas);
            this.add(this.gridAyudasPropuestas);

        },

        loadStore: function() {
            syncStoreLoad([ this.storeRecursos, this.storeAyudas, this.storeAyudasReclamadas, this.storeAyudasPropuestas ]);
        },

        buildStoreRecursos : function()
        {
            var ref = this;
            this.storeRecursos = new Ext.data.Store(
                {
                    restful : true,
                    url : '/bec/rest/recurso/beca',
                    reader : new Ext.data.XmlReader(
                        {
                            record : 'Recurso',
                            id : 'id'
                        }, [ 'id', 'becaId', 'estimacion', 'cursoActAniosBecario', 'cursoActCursoPosterior', 'cursoActCambioEstudios', 'textoJustificacion' ]),
                    writer : new Ext.data.XmlWriter(
                        {
                            xmlEncoding : 'UTF-8',
                            writeAllFields : true
                        }),
                    listeners :
                    {
                        beforeload : function() {
                            ref.storeRecursos.proxy.setUrl('/bec/rest/recurso/beca/' + ref.becaId);
                        }
                    }
                });
        },

        buildStoreAyudas : function()
        {
            this.storeAyudas = this.getStore(
                {
                    url : '/bec/rest/beca/' + this.becaId + '/cuantiacursoconvocatoria',
                    record : 'CuantiaCurso',
                    id : 'cuantiaId',
                    fieldList : [ 'cuantiaId', 'nombre', 'codigo', 'importe' ]
                });
        },


        buildStoreAyudasReclamadas : function()
        {
            this.storeAyudasReclamadas = this.getStore(
                {
                    url : '/bec/rest/recurso/' + this.recursoId + '/cuantia',
                    record : 'RecursoDetalle',
                    id : 'id',
                    fieldList : [ 'id', 'recursoId', 'cuantiaId', 'codigo', 'importe', 'texto', 'tipo' ]
                });
        },

        buildStoreAyudasPropuestas : function()
        {
            var ref = this;
            this.storeAyudasPropuestas = this.getStore(
                {
                    url : '/bec/rest/recurso/' + this.recursoId + '/cuantia',
                    record : 'RecursoDetalle',
                    id : 'id',
                    fieldList : [ 'id', 'recursoId', 'cuantiaId', 'codigo', 'importe', 'texto', 'tipo' ]
                });
        },

        buildInputRecursoId : function()
        {
            this.inputRecursoId = new Ext.form.NumberField(
                {
                    name : 'id',
                    hidden : true
                });
        },

        buildInputBecaId : function()
        {
            this.inputBecaId = new Ext.form.NumberField(
                {
                    name : 'becaId',
                    hidden : true
                });
        },

        buildComboAyudasReclamadas : function()
        {
            var ref = this;
            this.comboAyudasReclamadas = new Ext.form.ComboBox(
                {
                    triggerAction : 'all',
                    forceSelection : true,
                    editable : false,
                    displayField : 'nombre',
                    valueField : 'cuantiaId',
                    store : this.storeAyudas,
                    listeners :
                    {
                        select : function(combo, record, index)
                        {
                            var importe = record.data.importe;
                            posicion = ref.gridAyudasReclamadas.getColumnModel().findColumnIndex('importe');
                            ref.gridAyudasReclamadas.getColumnModel().getColumnAt(posicion).getEditor().setValue(importe);
                        }
                    }
                });
        },

        buildComboAyudasPropuestas : function()
        {
            var ref = this;
            this.comboAyudasPropuestas = new Ext.form.ComboBox(
                {
                    triggerAction : 'all',
                    forceSelection : true,
                    editable : false,
                    displayField : 'nombre',
                    valueField : 'cuantiaId',
                    store : this.storeAyudas,
                    listeners :
                    {
                        select : function(combo, record, index)
                        {
                            var importe = record.data.importe;
                            posicion = ref.gridAyudasPropuestas.getColumnModel().findColumnIndex('importe');
                            ref.gridAyudasPropuestas.getColumnModel().getColumnAt(posicion).getEditor().setValue(importe);
                        }
                    }
                });
        },

        buildRowEditorAyudasReclamadas : function()
        {
            var ref = this;

            this.editorAyudasReclamadas = new Ext.ux.grid.RowEditor(
                {
                    saveText : 'Desa',
                    cancelText : 'Cancel·la',
                    errorSummary : false,
                    focusDelay : 0,
                    floating :
                    {
                        zindex : 7000
                    },
                    listeners :
                    {
                        beforeedit : function(rowEditor, rowIndex)
                        {
                            ref.storeAyudasReclamadas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=RE');

                            if (rowIndex == 0)
                            {
                                ref.gridAyudasReclamadas.getSelectionModel().selectFirstRow();
                                if (ref.gridAyudasReclamadas.getSelectionModel().getSelected().dirty)
                                {
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                });
        },

        buildRowEditorAyudasPropuestas : function()
        {
            var ref = this;

            this.editorAyudasPropuestas = new Ext.ux.grid.RowEditor(
                {
                    saveText : 'Desa',
                    cancelText : 'Cancel·la',
                    errorSummary : false,
                    focusDelay : 0,
                    floating :
                    {
                        zindex : 7000
                    },
                    listeners :
                    {
                        beforeedit : function(rowEditor, rowIndex)
                        {
                            ref.storeAyudasPropuestas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=PR');

                            if (rowIndex == 0)
                            {
                                ref.gridAyudasPropuestas.getSelectionModel().selectFirstRow();
                                if (ref.gridAyudasPropuestas.getSelectionModel().getSelected().dirty)
                                {
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                });
        },

        buildButtonAyudasReclamadasAnadir : function()
        {
            var ref = this;
            this.buttonAyudasReclamadasAnadir = new Ext.Button(
                {
                    text : 'Afegir',
                    iconCls : 'application-add',
                    handler : function(button, event)
                    {
                        var rec = new ref.gridAyudasReclamadas.store.recordType(
                            {
                                id : '',
                                cuantiaId : '',
                                becaId : ref.becaId,
                                recursoId : ref.recursoId,
                                codigo : '',
                                importe : '',
                                texto : '',
                                tipo : 'RE'
                            });
                        ref.editorAyudasReclamadas.stopEditing();
                        ref.gridAyudasReclamadas.store.insert(0, rec);
                        ref.editorAyudasReclamadas.startEditing(0, true);
                    }
                });
        },

        buildButtonAyudasPropuestasAnadir : function()
        {
            var ref = this;

            this.buttonAyudasPropuestasAnadir = new Ext.Button(
                {
                    text : 'Afegir',
                    iconCls : 'application-add',
                    handler : function(button, event)
                    {
                        var rec = new ref.gridAyudasPropuestas.store.recordType(
                            {
                                id : '',
                                cuantiaId : '',
                                becaId : ref.becaId,
                                recursoId : ref.recursoId,
                                codigo : '',
                                importe : '',
                                texto : '',
                                tipo : 'PR'
                            });

                        ref.storeAyudasPropuestas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=PR');

                        ref.editorAyudasPropuestas.stopEditing();
                        ref.gridAyudasPropuestas.store.insert(0, rec);
                        ref.editorAyudasPropuestas.startEditing(0, true);
                    }
                });
        },

        buildButtonAyudasReclamadasBorrar : function()
        {
            var ref = this;
            this.buttonAyudasReclamadasBorrar = new Ext.Button(
                {
                    text : 'Esborrar',
                    iconCls : 'application-delete',
                    handler : function(button, event)
                    {
                        var rec = ref.gridAyudasReclamadas.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        Ext.MessageBox.show(
                            {
                                title : 'Esborrar quantia?',
                                msg : 'Atenció!\n ' + 'vols esborrar la quantia seleccionada?',
                                buttons : Ext.Msg.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(button, text, opt)
                                {
                                    if (button == 'yes')
                                    {
                                        ref.gridAyudasReclamadas.store.remove(rec);
                                    }
                                }
                            });
                    }
                });
        },

        buildButtonAyudasPropuestasBorrar : function()
        {
            var ref = this;
            this.buttonAyudasPropuestasBorrar = new Ext.Button(
                {
                    text : 'Esborrar',
                    iconCls : 'application-delete',
                    handler : function(button, event)
                    {
                        var rec = ref.gridAyudasPropuestas.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        Ext.MessageBox.show(
                            {
                                title : 'Esborrar quantia?',
                                msg : 'Atenció!\n ' + 'vols esborrar la quantia seleccionada?',
                                buttons : Ext.Msg.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(button, text, opt)
                                {
                                    if (button == 'yes')
                                    {
                                        ref.gridAyudasPropuestas.store.remove(rec);
                                    }
                                }
                            });
                    }
                });
        },

        buildGridAyudasReclamadas : function()
        {
            var ref = this;
            Ext.util.Format.comboRenderer = function(combo)
            {
                return function(value)
                {
                    var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            };
            this.gridAyudasReclamadas = new Ext.grid.GridPanel(
                {
                    title : 'Ajudes reclamades',
                    store : this.storeAyudasReclamadas,
                    frame : true,
                    autoScroll : true,
                    height : 150,
                    width : 700,
                    viewConfig :
                    {
                        forceFit : true
                    },
                    loadMask : true,
                    autoScroll : true,
                    plugins : [ this.editorAyudasReclamadas ],
                    sm : new Ext.grid.RowSelectionModel(
                        {
                            singleSelect : true
                        }),
                    colModel : new Ext.grid.ColumnModel(
                        {
                            defaults :
                            {
                                sortable : true
                            },
                            columns : [
                                {
                                    header : 'Id',
                                    dataIndex : 'id',
                                    hidden : true
                                },
                                {
                                    header : 'Nom',
                                    dataIndex : 'texto',
                                    hidden : true
                                },
                                {
                                    header : 'Codi',
                                    dataIndex : 'codigo',
                                    width : 40
                                },
                                {
                                    header : 'Quantia',
                                    dataIndex : 'cuantiaId',
                                    editor : ref.comboAyudasReclamadas,
                                    renderer : Ext.util.Format.comboRenderer(ref.comboAyudasReclamadas)
                                },
                                {
                                    header : 'Import',
                                    dataIndex : 'importe',
                                    renderer : function(v)
                                    {
                                        v = (Math.round((v - 0) * 100)) / 100;
                                        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
                                        v = String(v);
                                        return v.replace(/\./, ',');
                                    },
                                    width : 40,
                                    editor : new Ext.form.NumberField(
                                        {
                                            readOnly : false,
                                            decimalSeparator : ',',
                                            allowDecimals : true,
                                            allowNegative : false
                                        })
                                } ]
                        }),
                    tbar :
                    {
                        items : [ this.buttonAyudasReclamadasAnadir, this.buttonAyudasReclamadasBorrar ]
                    }
                });
        },

        buildGridAyudasPropuestas : function()
        {
            var ref = this;
            Ext.util.Format.comboRenderer = function(combo)
            {
                return function(value)
                {
                    var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            };
            this.gridAyudasPropuestas = new Ext.grid.GridPanel(
                {
                    title : 'Ajudes propostes',
                    store : this.storeAyudasPropuestas,
                    frame : true,
                    autoScroll : true,
                    height : 150,
                    width : 700,
                    viewConfig :
                    {
                        forceFit : true
                    },
                    loadMask : true,
                    autoScroll : true,
                    plugins : [ this.editorAyudasPropuestas ],
                    sm : new Ext.grid.RowSelectionModel(
                        {
                            singleSelect : true
                        }),
                    colModel : new Ext.grid.ColumnModel(
                        {
                            defaults :
                            {
                                sortable : true
                            },
                            columns : [
                                {
                                    header : 'Id',
                                    dataIndex : 'id',
                                    hidden : true
                                },
                                {
                                    header : 'Nom',
                                    dataIndex : 'texto',
                                    hidden : true
                                },
                                {
                                    header : 'Codi',
                                    dataIndex : 'codigo',
                                    width : 40

                                },
                                {
                                    header : 'Quantia',
                                    dataIndex : 'cuantiaId',
                                    editor : ref.comboAyudasPropuestas,
                                    renderer : Ext.util.Format.comboRenderer(ref.comboAyudasPropuestas)
                                },
                                {
                                    header : 'Import',
                                    dataIndex : 'importe',
                                    renderer : function(v)
                                    {
                                        v = (Math.round((v - 0) * 100)) / 100;
                                        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
                                        v = String(v);
                                        return v.replace(/\./, ',');
                                    },
                                    width : 40,
                                    editor : new Ext.form.NumberField(
                                        {
                                            readOnly : false,
                                            decimalSeparator : ',',
                                            allowDecimals : true,
                                            allowNegative : false
                                        })
                                } ]
                        }),
                    tbar :
                    {
                        items : [ this.buttonAyudasPropuestasAnadir, this.buttonAyudasPropuestasBorrar ]
                    }
                });
        },

        buildInputJustificacionPropuesta : function()
        {
            this.inputJustificacionPropuesta = new Ext.form.TextArea(
                {
                    width : 600,
                    fieldLabel : 'Justificació de la proposta',
                    name : 'textoJustificacion'
                });
        },

        buildTextFieldAnyosBecario : function()
        {
            this.textFieldAnyosBecario = new Ext.form.TextField(
                {
                    width : 100,
                    fieldLabel : 'Anys becari',
                    name : 'cursoActAniosBecario'
                });
        },

        buildComboMatriculaCursoPosterior : function()
        {
            this.comboMatriculaCursoPosterior = new Ext.form.ComboBox(
                {
                    typeAhead : true,
                    triggerAction : 'all',
                    lazyRender : true,
                    width : 90,
                    fieldLabel : 'Matriculat curs posterior',
                    hiddenName : 'cursoActCursoPosterior',
                    mode : 'local',
                    allowBlank : true,
                    forceSelection : false,
                    store : new Ext.data.ArrayStore(
                        {
                            fields : [ 'id', 'name' ],
                            data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
                        }),
                    valueField : 'id',
                    displayField : 'name'
                });
        },

        buildComboCambioEstudios : function()
        {
            this.comboCambioEstudios = new Ext.form.ComboBox(
                {
                    typeAhead : true,
                    triggerAction : 'all',
                    lazyRender : true,
                    width : 90,
                    fieldLabel : 'Canvi d\'estudis',
                    hiddenName : 'cursoActCambioEstudios',
                    mode : 'local',
                    allowBlank : true,
                    forceSelection : false,
                    store : new Ext.data.ArrayStore(
                        {
                            fields : [ 'id', 'name' ],
                            data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
                        }),
                    valueField : 'id',
                    displayField : 'name'
                });
        },

        buildComboEstimacion : function()
        {
            this.comboEstimacion = new Ext.form.ComboBox(
                {
                    typeAhead : true,
                    triggerAction : 'all',
                    lazyRender : true,
                    width : 90,
                    fieldLabel : 'Estimació',
                    hiddenName : 'estimacion',
                    mode : 'local',
                    allowBlank : true,
                    forceSelection : false,
                    store : new Ext.data.ArrayStore(
                        {
                            fields : [ 'id', 'name' ],
                            data : [ [ "0", 'No' ], [ "1", 'Sí' ] ]
                        }),
                    valueField : 'id',
                    displayField : 'name'
                });
        },

        buildBotonImprimir : function()
        {
            var ref = this;
            this.botonImprimir = new Ext.Button(
                {
                    text : 'Imprimeix',
                    iconCls : 'printer',
                    width : 60,
                    handler : function(button, event)
                    {
                        var becaId = ref.panelFormulario.getForm().getValues().becaId;
                        window.open("/bec/rest/beca/" + becaId + "/recurso/pdf");
                    }
                });
        },

        buildBotonGuardar : function()
        {
            var ref = this;
            this.botonGuardar = new Ext.Button(
                {
                    text : 'Desa',
                    iconCls : 'disk',
                    width : 60,
                    handler : function(button, event)
                    {
                        var id = ref.panelFormulario.getForm().getValues().id;
                        if (id != '' && id != null)
                        {
                            ref.panelFormulario.getForm().submit(
                                {
                                    url : '/bec/rest/recurso/' + id,
                                    method : 'put',
                                    waitMsg : 'Per favor, espereu...',
                                    waitTitle : 'Guardant Dades',
                                    success : function(form, action)
                                    {
                                        ref.storeRecursos.reload();
                                        //ref.fireEvent('UJI.BEC.RecursoTab.botonGuardar.click');
                                    },
                                    failure : function(form, action)
                                    {
                                    }
                                });
                        }
                        else
                        {
                            ref.inputBecaId.setValue(ref.becaId);
                            ref.panelFormulario.getForm().submit(
                                {
                                    url : '/bec/rest/recurso/',
                                    method : 'post',
                                    waitMsg : 'Per favor, espereu...',
                                    waitTitle : 'Guardant Dades',
                                    success : function(form, action)
                                    {
                                        ref.storeRecursos.reload();
                                        var recurso = Ext.DomQuery.selectNode('Recurso', action.response.responseXML);
                                        var recursoId = Ext.DomQuery.selectValue('id', recurso);

                                        ref.inputRecursoId.setValue(recursoId);
                                        ref.recursoId = recursoId;
                                        ref.storeAyudasReclamadas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=RE');
                                        ref.storeAyudasReclamadas.load();
                                        ref.storeAyudasPropuestas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=PR');
                                        ref.storeAyudasPropuestas.load();

                                    },
                                    failure : function(form, action)
                                    {
                                    }
                                });
                        }
                    }
                });
        },

        buildBotonRecuperar : function()
        {
            var ref = this;
            this.botonRecuperar = new Ext.Button(
                {
                    text : 'Recupera informació',
                    width : 60,
                    iconCls : 'arrow-down',
                    handler : function(button, event)
                    {
                        url = '/bec/rest/recurso/beca/' + ref.becaId;
                        Ext.Ajax.request(
                            {
                                url : url + '/recupera',
                                method : 'PUT',
                                success : function(result, request)
                                {
                                    ref.panelFormulario.getForm().load(
                                        {
                                            url : url,
                                            method : 'GET',
                                            success : function(form)
                                            {

                                            }
                                        });
                                }
                            });
                    }
                });
        },

        buildBotonEliminar : function()
        {
            var ref = this;
            this.botonEliminar = new Ext.Button(
                {
                    text : 'Esborrar recurs',
                    width : 60,
                    iconCls : 'application-delete',
                    handler : function(button, event)
                    {
                        var id = ref.panelFormulario.getForm().getValues().id;

                        if (id != '' && id != null)
                        {
                            ref.panelFormulario.getForm().submit(
                                {
                                    url : '/bec/rest/recurso/' + id,
                                    method : 'delete',
                                    waitMsg : 'Per favor, espereu...',
                                    waitTitle : 'Guardant Dades',
                                    success : function(form, action)
                                    {
                                        ref.panelFormulario.getForm().reset();
                                        ref.gridAyudasPropuestas.getStore().removeAll();
                                        ref.gridAyudasReclamadas.getStore().removeAll();
                                    },
                                    failure : function(form, action)
                                    {
                                    }
                                });
                        }
                    }
                });

        },

        buildPanelFormulario : function()
        {
            this.panelFormulario = new Ext.FormPanel(
                {
                    layout : 'table',
                    flex : 1,
                    width : 700,
                    frame : true,
                    style : 'margin-right:16px;',
                    labelAlign : 'top',
                    reader : new Ext.data.XmlReader(
                        {
                            record : 'Recurso',
                            id : 'id'
                        }, [ 'id', 'becaId' ]),
                    errorReader : new Ext.data.XmlReader(
                        {
                            record : 'responseMessage',
                            success : 'success'
                        }, [ 'success', 'msg' ]),
                    padding : '5px 0px',
                    layoutConfig :
                    {
                        columns : 4
                    },
                    defaults :
                    {
                        bodyStyle : 'padding:0px 10px'
                    },
                    buttons : [ this.botonGuardar, this.botonImprimir, this.botonRecuperar, this.botonEliminar ],
                    items : [
                        {
                            layout : 'form',
                            items : [ this.inputRecursoId, this.inputBecaId, this.textFieldAnyosBecario ]
                        },
                        {
                            layout : 'form',
                            items : [ this.comboMatriculaCursoPosterior ]
                        },
                        {
                            layout : 'form',
                            items : [ this.comboCambioEstudios ]
                        },
                        {
                            layout : 'form',
                            items : [ this.comboEstimacion ]
                        },
                        {
                            layout : 'form',
                            items : [ this.inputJustificacionPropuesta ],
                            colspan : 4
                        }
                    ]
                });
        },

        load : function(becaId)
        {
            this.becaId = becaId;
            var ref = this;
            this.storeRecursos.load(
                {
                    becaId : becaId,
                    callback : function(r)
                    {
                        if (r[0])
                        {
                            ref.panelFormulario.getForm().loadRecord(r[0]);

                            ref.recursoId = ref.panelFormulario.getForm().getValues().id;

                            ref.storeAyudasReclamadas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=RE');
                            ref.storeAyudasReclamadas.reload();
                            ref.storeAyudasPropuestas.proxy.setUrl('/bec/rest/recurso/' + ref.recursoId + '/cuantia?tipo=PR');
                            ref.storeAyudasPropuestas.reload();
                        }
                    }
                });
        }
    });