Ext.ns('UJI.BEC');

UJI.BEC.TabMaestrosTipoMatricula = Ext.extend(UJI.BEC.TabMaestroTipo,
{
    title : 'Matrículas',
    url : '/bec/rest/tipomatricula',
    record : 'TipoMatricula'
});