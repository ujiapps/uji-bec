<%@page contentType="text/html; charset=utf-8"%>
<%@page import="es.uji.commons.sso.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Beques</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />

<script type="text/javascript" src="//static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/fileuploadfield/FileUploadField.js"></script>

<link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css" />
<link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />

<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationPanel/0.0.1/ApplicationPanel.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationViewport/0.0.1/ApplicationViewport.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/data/XmlWriter/0.0.1/XmlWriter.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript" src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>

<script type="text/javascript" src="js/dashboard.js"></script>
<script type="text/javascript" src="js/panelConvocatorias.js"></script>
<script type="text/javascript" src="js/alta.js"></script>
<script type="text/javascript" src="js/busquedas.js"></script>
<script type="text/javascript" src="js/tandas.js"></script>
<script type="text/javascript" src="js/listados.js"></script>
<script type="text/javascript" src="js/maestros.js"></script>
<script type="text/javascript" src="js/maestroTipoTab.js"></script>
<script type="text/javascript" src="js/titulosPoseeTab.js"></script>
<script type="text/javascript" src="js/maestrosCursoAcademicoTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoDomicilioTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoFamiliaTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoIdentificacionTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoMatriculaTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoMiembroTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoMinusvaliaTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoMonedaTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoDocumentoTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoPropiedadTab.js"></script>
<script type="text/javascript" src="js/maestrosTipoViaTab.js"></script>
<script type="text/javascript" src="js/maestrosEstadoCivilTab.js"></script>
<script type="text/javascript" src="js/maestrosEstadoBeca.js"></script>
<script type="text/javascript" src="js/maestrosOrganismo.js"></script>
<script type="text/javascript" src="js/maestrosCuantia.js"></script>
<script type="text/javascript" src="js/maestrosDenegacion.js"></script>
<script type="text/javascript" src="js/maestrosConvocatoria.js"></script>
<script type="text/javascript" src="js/textos.js"></script>
<script type="text/javascript" src="js/erroresTanda.js"></script>
<script type="text/javascript" src="js/gestionBecaPanel.js"></script>
<script type="text/javascript" src="js/datosGeneralesTab.js"></script>
<script type="text/javascript" src="js/academicosTab.js"></script>
<script type="text/javascript" src="js/estudiosTab.js"></script>
<script type="text/javascript" src="js/domiciliosTab.js"></script>
<script type="text/javascript" src="js/documentosTab.js"></script>
<script type="text/javascript" src="js/miembrosTab.js"></script>
<script type="text/javascript" src="js/otrosDatosTab.js"></script>
<script type="text/javascript" src="js/declaranteTab.js"></script>
<script type="text/javascript" src="js/economicoTab.js"></script>
<script type="text/javascript" src="js/cuantiasTab.js"></script>
<script type="text/javascript" src="js/recursosTab.js"></script>
<script type="text/javascript" src="js/denegacionesTab.js"></script>
<script type="text/javascript" src="js/reclamacionesTab.js"></script>
<script type="text/javascript" src="js/historicoTab.js"></script>
<script type="text/javascript" src="js/ficherosUpload.js"></script>
<script type="text/javascript" src="js/excelexport.js"></script>

<%
    String login = ((User) session.getAttribute(User.SESSION_USER))
					.getName();
    ServletContext context = getServletContext();
    String appVersion = context.getInitParameter("appVersion");

%>

<style type=text/css>
    /* Style LinkButtons */
    table.x-btn .x-btn-small td.x-btn-mc em a {
        color: inherit;
        text-decoration: none;
        display: block;
        height: 12px;
        padding-bottom: 2px;
        padding-top: 2px;
    }

    body.ext-opera  table.x-btn .x-btn-small td.x-btn-mc em a,
    body.ext-chrome table.x-btn .x-btn-small td.x-btn-mc em a {
        padding-top: 1px;
    }

    body.ext-chrome table.x-btn .x-btn-small td.x-btn-mc em a {
        padding-bottom: 3px;
    }
</style>

<script type="text/javascript">
    function reemplazaCadenasDateField()
    {
        Ext.override(Ext.DatePicker, {
            todayText: 'Avui',
            monthNames : ['gener', 'febrer', 'març', 'abril', 'maig', 'juny', 'juliol', 'agost', 'setembre', 'octubre', 'novembre', 'decembre'],
            dayNames : ['g','l', 'm', 'c', 'j', 'v', 's'],
            nextText : 'Mes següent (Control+Dreta)',
            prevText : 'Mes anterior (Control+Esquerra)',
            monthYearText : 'Tria un mes (Control+Dalt/Baix per canviar l\'any)'
        });
    }

    Ext.BLANK_IMAGE_URL = '//static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

    var login = '<%=login%>';
    var appversion = '<%=appVersion%>';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        reemplazaCadenasDateField();

        Ext.Ajax.request({
            url: '/bec/rest/cursoacademico/activo',
            success: function(response) {
                var cursoAcademico = Ext.DomQuery.selectNode('CursoAcademico', response.responseXML);
                var cursoAcademicoId = Ext.DomQuery.selectValue('id', cursoAcademico);
                cursoAcademicoActivoId = cursoAcademicoId;
                new Ext.ux.uji.ApplicationViewport(
                {
                    codigoAplicacion : 'BEC',
                    tituloAplicacion : 'Beques ' + cursoAcademicoId,
                    treeWidth : 170
                });
            }
        });
    });
</script>
</head>
<body>
</body>
</html>
