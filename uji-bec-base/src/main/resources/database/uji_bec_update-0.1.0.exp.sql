ALTER TABLE UJI_BECAS.BC2_TANDAS
  DROP CONSTRAINT BC2_TANDAS_2_UN;

ALTER TABLE UJI_BECAS.BC2_TANDAS ADD (
  CONSTRAINT BC2_TANDAS_2_UN
 UNIQUE (CURSO_ACADEMICO_ID, TANDA_ID));

 
 
 
 
 
 
 
CREATE TABLE UJI_BECAS.BC2_CCAA
(
  ID      NUMBER,
  NOMBRE  VARCHAR2(400 BYTE),
  CODIGO  VARCHAR2(10 BYTE)
);

ALTER TABLE UJI_BECAS.BC2_CCAA ADD (
  CONSTRAINT BC2_CCAA_PK
 PRIMARY KEY
 (ID));

Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (1, 'Comunidad autónoma de Andalucía', '01');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (2, 'Comunidad autónoma de Aragón', '02');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (3, 'Principado de Asturias', '03');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (4, 'Comunidad autónoma de las Illes Balears', '04');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (5, 'Comunidad autónoma de Canarias', '05');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (6, 'Comunidad autónoma de Cantabria', '06');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (7, 'Comunidad de Castilla y León', '07');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (8, 'Comunidad autónoma de Castilla - La Mancha', '08');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (9, 'Comunidad autónoma de Cataluña', '09');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (10, 'Comunidad autónoma de Extremadura', '10');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (11, 'Comunidad autónoma de Galicia', '11');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (12, 'Comunidad autónoma de La Rioja', '12');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (13, 'Comunidad de Madrid', '13');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (14, 'Región de Murcia', '14');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (15, 'Comunidad foral de Navarra', '15');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (16, 'Comunidad Valenciana', '16');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (17, 'Comunidad autónoma del País Vasco', '17');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (18, 'Ciudad de Ceuta', '18');
Insert into BC2_CCAA
   (ID, NOMBRE, CODIGO)
 Values
   (19, 'Ciudad de Melilla', '19');

   
   
   
   
   
ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (IDENTIFICACION_IDESP  VARCHAR2(100));

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (FECHA_CAD_NIF  DATE);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (FECHA_RES_MINUS  DATE);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (FECHA_FIN_MINUS  DATE);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (CODIGO_ESTUDIO_HERMANO  VARCHAR2(20));

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (CODIGO_UNIV_HERMANO  VARCHAR2(20));
 
ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (CCAA_MINUS  NUMBER); 
 
ALTER TABLE UJI_BECAS.BC2_MIEMBROS ADD 
CONSTRAINT BC2_MIEMBROS_CCAA_FK
 FOREIGN KEY (CCAA_MINUS)
 REFERENCES UJI_BECAS.BC2_CCAA (ID);
   
   
   
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS (ID,
                                                         NOMBRE,
                                                         TIPO,
                                                         OFICIAL,
                                                         NUMERO_CURSOS,
                                                         CREDITOS_TOTALES,
                                                         CREDITOS_POR_CURSO,
                                                         SIN_DOCENCIA,
                                                         EXTINGUIDA,
                                                         PRESENCIAL,
                                                         TITULACION_TECNICA,
                                                         RAMA,
                                                         CICLOS,
                                                         CODIGO_CENTRO,
                                                         TIPO_TITULO
                                                        ) AS
   SELECT id, nombre, tipo, oficial, numero_cursos, creditos_totales,
          DECODE (numero_cursos, NULL, ROUND (creditos_totales / numero_cursos, 0)) creditos_por_curso, sin_docencia,
          extinguida, presencial, titulacion_tecnica, rama, ciclos, codigo_centro, tipo_titulo
   FROM   (SELECT t.id, t.nombre, tipo_estudio tipo,
                  DECODE (tipo_estudio,
                          'G', 1,
                          '12C', DECODE (t.id, 51001, 0, 51002, 0, 51003, 0, 100, 0, 101, 0, 103, 0, 104, 0, 1)
                         ) oficial,
                  (SELECT COUNT (*)
                   FROM   gra_pod.pod_cursos
                   WHERE  tit_id = t.id
                   AND    id <> 7) numero_cursos, crd_tr + crd_ob + crd_op + crd_le + NVL (crd_pfc, 0) creditos_totales,
                  DECODE (SIGN (NVL (curso_sin_docencia, 2013) - 2012), 1, 0, 1) sin_docencia,
                  DECODE (activa, 'S', 0, 1) extinguida, DECODE (activa, 'S', 1, 0) presencial,
                  DECODE (GRA_BEC.PACK_BEC.ES_TECNICA (t.id), 'S', 1, 0) titulacion_tecnica, NVL (t.rama, 'SO') rama,
                  t.tipo ciclos, u.cod_mec codigo_centro,
                  decode (t.tipo_titulo,
                          '0H', '02',
                          '01', '01',
                          '02', '04',
                          '03', '04',
                          '04', '03',
                          '06', '05',
                          null
                         ) tipo_titulo
           FROM   gra_pod.pod_titulaciones t,
                  gri_est.est_ubic_estructurales u,
                  (SELECT 34 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 32 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 4 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 2 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 6 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 23 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 21 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 27 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 28 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 24 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 11 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 12 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 14 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 13 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 16 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 15 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 10 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 1 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 29 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 35 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 7 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 17 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 30 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 25 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 33 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 9 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 26 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 31 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL) SIN
           WHERE  decode(t.id, 17, 2, t.uest_id) = u.id
           AND    t.id = SIN.tit_id(+)
           UNION ALL
           SELECT m.id, nombre, 'M' tipo, DECODE (oficial, 'S', 1, 0),
                  DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2) numero_cursos, creditos creditos_totales,
                  DECODE (activo, 'S', 0, 1) sin_docencia, DECODE (activo, 'S', 0, 1) extinguida,
                  DECODE (presencial, 'P', 1, 0) presencial, 0 titulacion_tecnica, NVL (r.cod, 'SO') rama, 1 ciclos,
                  NULL codigo_centro, '06' tipo_titulo
           FROM   gra_pop.pop_masters m,
                  pod_ramas r
           WHERE  m.rama_id = r.id(+)
           UNION ALL
           SELECT id, nombre, 'DOC' tipo, 1 oficial, 1 numero_cursos, 0 creditos_totales,
                  DECODE (fin_docencia, NULL, 0, 1) sin_docencia, DECODE (fin_docencia, NULL, 0, 1) extinguida,
                  1 presencial, 0 titulacion_tecnica, NULL rama, 1 ciclos, NULL codigo_centro, '07' tipo_titulo
           FROM   gra_doc.doc_programas);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (ESTUDIA_FUERA  NUMBER DEFAULT 0 NOT NULL);		   
		   