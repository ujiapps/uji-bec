DROP VIEW UJI_BECAS.BC2_EXT_MENSAJES;

create or replace force view UJI_BECAS.BC2_EXT_MENSAJES(ID, AGENTE_ID, CATEGORIA_ID, TIPO, FECHA_CREACION, FECHA_INICIO, FECHA_FIN, REFERENCIA, REMITENTE, REPLY_TO, ASUNTO, CUERPO, CONTENT_TYPE, LINK, AGENTE, CATEGORIA, ENVIADO_A) as
   select m.id, m.agente_id, m.categoria_id, m.tipo, m.fecha_creacion, m.fecha_inicio, m.fecha_fin, m.referencia, m.remitente, m.reply_to, m.asunto, m.cuerpo, m.content_type, m.link, a.nombre agente, c.nombre categoria,
          (select listagg(nombre, ',') within group (order by nombre)
             from uji_mensajeria.msg_destinos
            where mensaje_id = m.id)
             enviado_a
     from uji_mensajeria.msg_mensajes m, uji_mensajeria.msg_agentes a, uji_mensajeria.msg_categorias c
    where agente_id = a.id
      and categoria_id = c.id
      and agente_id = 7;

