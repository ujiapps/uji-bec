CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS (ID,
                                                         NOMBRE,
                                                         TIPO,
                                                         OFICIAL,
                                                         NUMERO_CURSOS,
                                                         CREDITOS_TOTALES,
                                                         CREDITOS_POR_CURSO,
                                                         SIN_DOCENCIA,
                                                         EXTINGUIDA,
                                                         PRESENCIAL,
                                                         TITULACION_TECNICA,
                                                         RAMA,
                                                         CICLOS,
                                                         CODIGO_CENTRO,
                                                         TIPO_TITULO,
                                                         DOBLE_TITULACION
                                                        ) AS
   SELECT id, nombre, tipo, oficial, numero_cursos, creditos_totales,
          DECODE (numero_cursos, NULL, ROUND (creditos_totales / numero_cursos, 0)) creditos_por_curso, sin_docencia,
          extinguida, presencial, titulacion_tecnica, rama, ciclos, codigo_centro, tipo_titulo, doble_titulacion
     FROM (SELECT t.id, t.nombre, tipo_estudio tipo,
                  DECODE (tipo_estudio,
                          'G', 1,
                          '12C', DECODE (t.id, 51001, 0, 51002, 0, 51003, 0, 100, 0, 101, 0, 103, 0, 104, 0, 1)
                         ) oficial,
                  (SELECT COUNT (*)
                     FROM gra_pod.pod_cursos
                    WHERE tit_id = t.id
                      AND id <> 7) numero_cursos,
                  crd_tr + crd_ob + crd_op + NVL (crd_pfc, 0) + DECODE (tipo_estudio, 'G', crd_le, 0)
                  - DECODE (t.id, 11, 32, 0) creditos_totales,
                  --DECODE (SIGN (NVL (curso_sin_docencia, 2013) - 2012), 1, 0, 1) 
                  DECODE (sign(t.id-200),-1,1,decode(activa,'S',0,1)) sin_docencia,
                  DECODE (activa, 'S', 0, 1) extinguida, DECODE (activa, 'S', 1, 0) presencial,
                  DECODE (gra_bec.pack_bec.es_tecnica (t.id), 'S', 1, 0) titulacion_tecnica, NVL (t.rama, 'SO') rama,
                  t.tipo ciclos, u.cod_mec codigo_centro,
                  DECODE (t.tipo_titulo,
                          '0H', '02',
                          '01', '01',
                          '02', '04',
                          '03', '04',
                          '04', '03',
                          '06', '05',
                          NULL
                         ) tipo_titulo,
                  0 doble_titulacion
             FROM gra_pod.pod_titulaciones t,
                  gri_est.est_ubic_estructurales u,
                  (SELECT 34 tit_id, 2011 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 32 tit_id, 2011 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 4 tit_id, 2011 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 2 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 6 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 23 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 21 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 27 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 28 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 24 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 11 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 12 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 14 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 13 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 16 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 15 tit_id, 2012 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 10 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 1 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 29 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 35 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 7 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 17 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 30 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 25 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 33 tit_id, 2013 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 9 tit_id, 2014 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 26 tit_id, 2014 curso_sin_docencia
                     FROM DUAL
                   UNION ALL
                   SELECT 31 tit_id, 2014 curso_sin_docencia
                     FROM DUAL) SIN
            WHERE DECODE (t.id, 17, 2, t.uest_id) = u.id
              AND t.id = SIN.tit_id(+)
           UNION ALL
           SELECT m.id, m.nombre, 'M' tipo, DECODE (oficial, 'S', 1, 0),
                  DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2) numero_cursos, creditos creditos_totales,
                  DECODE (activo, 'S', 0, 1) sin_docencia, DECODE (activo, 'S', 0, 1) extinguida,
                  DECODE (presencial, 'P', 1, 0) presencial, 0 titulacion_tecnica, NVL (r.cod, 'SO') rama, 1 ciclos,
                  u.cod_mec codigo_centro, '06' tipo_titulo, 0 doble_titulacion
             FROM gra_pop.pop_masters m,
                  pod_ramas r,
                  gri_est.est_ubic_estructurales u
            WHERE m.rama_id = r.id(+)
              AND u.id = m.uest_id
           UNION ALL
           SELECT m.id, m.nombre, 'M' tipo, DECODE (oficial, 'S', 1, 0),
                  DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2) numero_cursos, creditos creditos_totales,
                  DECODE (activo, 'S', 0, 1) sin_docencia, DECODE (activo, 'S', 0, 1) extinguida,
                  DECODE (presencial, 'P', 1, 0) presencial, 0 titulacion_tecnica, NVL (r.cod, 'SO') rama, 1 ciclos,
                  NULL codigo_centro, '06' tipo_titulo, 0 doble_titulacion
             FROM gra_pop.pop_masters m,
                  pod_ramas r
            WHERE m.rama_id = r.id(+)
              AND m.uest_id IS NULL
           UNION ALL
           SELECT id, nombre, 'DOC' tipo, 1 oficial, 1 numero_cursos, 0 creditos_totales,
                  DECODE (fin_docencia, NULL, 0, 1) sin_docencia, DECODE (fin_docencia, NULL, 0, 1) extinguida,
                  1 presencial, 0 titulacion_tecnica, NULL rama, 1 ciclos, NULL codigo_centro, '07' tipo_titulo,
                  0 doble_titulacion
             FROM gra_doc.doc_programas);

			 
			 
			 