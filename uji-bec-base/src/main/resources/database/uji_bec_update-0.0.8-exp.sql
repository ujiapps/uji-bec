grant select on gra_pod.pod_ramas to uji_becas;

grant select on gra_pod.pod_ramas to uji_becas_usuario;



CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS (ID,
                                                         NOMBRE,
                                                         TIPO,
                                                         OFICIAL,
                                                         NUMERO_CURSOS,
                                                         CREDITOS_TOTALES,
                                                         CREDITOS_POR_CURSO,
                                                         SIN_DOCENCIA,
                                                         EXTINGUIDA,
                                                         PRESENCIAL,
                                                         CODMEC,
                                                         CODMEC_NUEVO,
                                                         CODMEC_BECAS,
                                                         CODMEC_BECAS_NUEVO,
                                                         COD_CONSELLERIA,
                                                         TITULACION_TECNICA,
                                                         RAMA
                                                        ) AS
   select id, nombre, tipo, oficial, numero_cursos, creditos_totales,
          decode (numero_cursos, null, round (creditos_totales / numero_cursos, 0)) creditos_por_curso, sin_docencia,
          extinguida, presencial, codmec, codmec_nuevo, codmec_becas, codmec_becas_nuevo, cod_conselleria,
          titulacion_tecnica, rama
   from   (select id, nombre, tipo_estudio tipo,
                  decode (tipo_estudio,
                          'G', 1,
                          '12C', decode (id, 51001, 0, 51002, 0, 51003, 0, 100, 0, 101, 0, 103, 0, 104, 0, 1)
                         ) oficial,
                  (select count (*)
                   from   gra_pod.pod_cursos
                   where  tit_id = t.id
                   and    id <> 7) numero_cursos, crd_tr + crd_ob + crd_op + crd_le + nvl (crd_pfc, 0) creditos_totales,
                  decode (activa, 'S', 0, 1) sin_docencia, decode (activa, 'S', 0, 1) extinguida,
                  decode (activa, 'S', 1, 0) presencial, codmec, codmec_nuevo, codmec_becas, codmec_becas_nuevo,
                  cod_conselleria, decode (id, 1, 0, 1) titulacion_tecnica, rama
           from   gra_pod.pod_titulaciones t
           union all
           select m.id, nombre, 'POP' tipo, decode (oficial, 'S', 1, 0),
                  decode (sign (creditos - 60), -1, 1, 0, 1, 2) numero_cursos, creditos creditos_totales,
                  decode (activo, 'S', 0, 1) sin_docencia, decode (activo, 'S', 0, 1) extinguida,
                  decode (presencial, 'P', 1, 0) presencial, cod_mec, null codmec_nuevo, null codmec_becas,
                  null codmec_becas_nuevo, null cod_conselleria, 0 titulacion_tecnica, r.cod rama
           from   gra_pop.pop_masters m, pod_ramas r
           where  m.rama_id = r.id
           union all
           select id, nombre, 'DOC' tipo, 1 oficial, 1 numero_cursos, 0 creditos_totales,
                  decode (fin_docencia, null, 0, 1) sin_docencia, decode (fin_docencia, null, 0, 1) extinguida,
                  1 presencial, codigo_mec codmec, null codmec_nuevo, null codmec_becas, null codmec_becas_nuevo,
                  null cod_conselleria, 0 titulacion_tecnica, null rama
           from   gra_doc.doc_programas);

		   

/* Formatted on 20/09/2012 10:07 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS (ID,
                                                                  PERSONA_ID,
                                                                  ESTUDIO_ID,
                                                                  CURSO_ACADEMICO_ID,
                                                                  CREDITOS_MATRICULADOS,
                                                                  CREDITOS_SUPERADOS,
                                                                  CREDITOS_FALTANTES,
                                                                  CREDITOS_PENDIENTES_CONVA,
                                                                  CREDITOS_FUERA_BECA,
                                                                  CREDITOS_PRESENTADOS,
                                                                  LIMITAR_CREDITOS,
                                                                  LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                  MATRICULA_PARCIAL,
                                                                  NUMERO_BECAS,
                                                                  NUMERO_SEMESTRES,
                                                                  SIN_DOCENCIA,
                                                                  PROGRAMA_INTERCAMBIO,
                                                                  CREDITOS_CONVALIDADOS,
                                                                  CREDITOS_PARA_BECA,
                                                                  CURSO,
                                                                  TERCIO_PRESENTADO,
                                                                  RENDIMIENTO_ACADEMICO,
                                                                  NOTA_ACCESO,
                                                                  NOTA_MEDIA,
                                                                  FAMILIA_NUMEROSA_ID,
                                                                  MINUSVALIA,
                                                                  BECARIO,
                                                                  TIPO_MATRICULA,
                                                                  ESTUDIO_ACCESO_ID
                                                                 ) AS


   select m.curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_tit_id estudio_id,
          m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_Aca, exp_per_id, exp_tit_id) creditos_matriculados,
          pack_exp.crd_sup_ca (exp_per_id, exp_tit_id, m.curso_aca) creditos_superados,
          pack_exp.crd_faltantes_ca (exp_per_id, exp_tit_id, m.curso_aca) creditos_faltantes,
          (select nvl (sum (a.creditos), 0)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_asignaturas a
           where  estado in ('A', 'C', 'R')
           and    cco_id = 0
           and    mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    asi_id = a.id) creditos_pendientes_conva,
          decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id),
                  0, 0,
                  pack_exp.imp_creditos_no_beca (exp_tit_id, exp_per_id, m.curso_aca)
                  / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id)
                 ) creditos_fuera_beca,
          (select sum (a.creditos)
           from   (select distinct asc_mat_exp_per_id, asc_mat_exp_tit_id, asc_mat_curso_aca, asc_asi_id
                   from            gra_exp.exp_notas) n,
                  gra_pod.pod_asignaturas a
           where  asc_mat_exp_per_id = m.exp_per_id
           and    asc_mat_exp_tit_id = m.exp_tit_id
           and    asc_mat_curso_aca = m.curso_aca
           and    asc_asi_id = a.id) creditos_presentados,
          0 limitar_creditos, 0 limitar_creditos_fin_estudios,
          (select decode (count (*), 0, 0, 1)
           from   exp_mat_circunstancias
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    cir_id = 3) matricula_parcial,
          (select count (*)
           from   gra_bec.bec_solicitudes
           where  curso_aca < m.curso_Aca
           and    per_id = m.exp_per_id
           and    tit_id = m.exp_tit_id) numero_becas,
          (select count (distinct semestre)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_grupos p
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    ac.asi_id = p.asi_id
           and    mat_curso_aca = p.curso_aca) numero_semestres,
          (select decode (count (*), 0, 0, 1)
           from   exp_asi_cursadas
           where  mat_exp_per_id = exp_per_id
           and    mat_exp_tit_id = exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    grp_id = 'Z') sin_docencia,
          (select decode (count (*), 0, 0, 1)
           from   gra_exp.exp_sol_convocatorias
           where  tipo = 'E'
           and    asc_mat_exp_per_id = exp_per_id
           and    asc_mat_exp_tit_id = exp_tit_id
           and    asc_mat_curso_aca = m.curso_aca) programa_intercambio,
          0 creditos_convalidados, 0 creditos_para_beca, 0 curso, 0 tercio_presentado, 0 rendimiento_academico,
          10 nota_acceso, 10 nota_media, 1 familia_numerosa, 1 minusvalia, 1 becario, tma.nombre tipo_matricula, null estudio_acceso_id
   from   gra_exp.exp_matriculas m,
          exp_tipos_matricula tma
   where  m.curso_aca = tma.curso_aca
   and    m.tma_id = tma.id
   
   /*
   union all
   
   

   select m.curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_pmas_id estudio_id,
          m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_Aca, exp_per_id, exp_pmas_id) creditos_matriculados,
          pack_exp.crd_sup_ca (exp_per_id, exp_pmas_id, m.curso_aca) creditos_superados,
          pack_exp.crd_faltantes_ca (exp_per_id, exp_pmas_id, m.curso_aca) creditos_faltantes,
          (select nvl (sum (a.creditos), 0)
           from   gra_pop.pop_asi_cursadas ac,
                  gra_pod.pop_asignaturas a
           where  estado in ('A', 'C', 'R')
           and    cco_id = 0
           and    pmat_pexp_per_id = m.exp_per_id
           and    pmat_pexp_pmas_id = m.exp_pmas_id
           and    mat_curso_aca = m.curso_aca
           and    asi_id = a.id) creditos_pendientes_conva,
          decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_pmas_id),
                  0, 0,
                  pack_exp.imp_creditos_no_beca (exp_pmas_id, exp_per_id, m.curso_aca)
                  / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_pmas_id)
                 ) creditos_fuera_beca,
          (select sum (a.creditos)
           from   (select distinct asc_pmat_pexp_per_id, asc_pmat_pexp_pmas_id, asc_mat_curso_aca, asc_asi_id
                   from            gra_pop.exp_notas) n,
                  gra_pop.pop_asignaturas a
           where  asc_pmat_pexp_per_id = m.exp_per_id
           and    asc_pmat_pexp_pmas_id = m.exp_pmas_id
           and    asc_mat_curso_aca = m.curso_aca
           and    asc_asi_id = a.id) creditos_presentados,
          0 limitar_creditos, 0 limitar_creditos_fin_estudios,
          (select decode (count (*), 0, 0, 1)
           from   pop_mat_circunstancias
           where  pmat_pexp_per_id = m.exp_per_id
           and    pmat_pexp_pmas_id = m.exp_pmas_id
           and    mat_curso_aca = m.curso_aca
           and    cir_id = 3) matricula_parcial,
          (select count (*)
           from   gra_bec.bec_solicitudes
           where  curso_aca < m.curso_Aca
           and    per_id = m.exp_per_id
           and    tit_id = m.exp_pmas_id) numero_becas,
          (select count (distinct semestre)
           from   gra_pop.pop_asi_cursadas ac,
                  gra_pod.pop_grupos p
           where  pmat_pexp_per_id = m.exp_per_id
           and    pmat_pexp_pmas_id = m.exp_pmas_id
           and    mat_curso_aca = m.curso_aca
           and    ac.asi_id = p.asi_id
           and    mat_curso_aca = p.curso_aca) numero_semestres,
          (select decode (count (*), 0, 0, 1)
           from   pop_asi_cursadas
           where  pmat_pexp_per_id = exp_per_id
           and    pmat_pexp_pmas_id = exp_pmas_id
           and    mat_curso_aca = m.curso_aca
           and    grp_id = 'Z') sin_docencia,
          (select decode (count (*), 0, 0, 1)
           from   gra_pop.exp_sol_convocatorias
           where  tipo = 'E'
           and    asc_pmat_pexp_per_id = exp_per_id
           and    asc_pmat_pexp_pmas_id = exp_pmas_id
           and    asc_mat_curso_aca = m.curso_aca) programa_intercambio,
          0 creditos_convalidados, 0 creditos_para_beca, 0 curso, 0 tercio_presentado, 0 rendimiento_academico,
          10 nota_acceso, 10 nota_media, 1 familia_numerosa, 1 minusvalia, 1 becario, tma.nombre tipo_matricula, null estudio_acceso_id
   from   gra_pop.pop_matriculas m,
          pop_tipos_matricula tma
   where  m.curso_aca = tma.curso_aca
   and    m.ptm_id = tma.id;
*/




ALTER TABLE UJI_BECAS.BC2_MINIMOS
RENAME COLUMN CREDITOS_MINIMOS TO CREDITOS_BECA_PARCIAL;

ALTER TABLE UJI_BECAS.BC2_MINIMOS
RENAME COLUMN CREDITOS TO CREDITOS_BECA_COMPLETA;


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_ESTUDIOS (ID,
                                                             ESTUDIO_ID,
                                                             NUMERO_CURSOS,
                                                             CREDITOS_TOTALES,
                                                             PRESENCIAL,
                                                             SIN_DOCENCIA,
                                                             CREDITOS_BECA_COMPLETA,
                                                             CREDITOS_BECA_PARCIAL,
                                                             TIPO_MATRICULA_ID,
                                                             BECA_ID,
                                                             CURSO_ACADEMICO_ID,
                                                             CURSO_ACADEMICO_ACTIVO,
                                                             TITULACION_TECNICA
                                                            ) AS
   SELECT solicitud.id * 100000 + becas.id AS id, estudio.id AS estudio_Id, estudio.NUMERO_CURSOS AS numero_Cursos,
          estudio.CREDITOS_TOTALES AS creditos_Totales, estudio.presencial AS presencial,
          estudio.SIN_DOCENCIA AS sin_Docencia, minimos.CREDITOS_BECA_COMPLETA AS creditos_beca_completa,
          minimos.CREDITOS_BECA_PARCIAL AS creditos_beca_parcial, becas.TIPO_MATRICULA_ID AS tipo_Matricula_Id,
          becas.id AS beca_Id, CURSO_ACA.id AS curso_Academico_Id, curso_aca.activo AS curso_Academico_Activo,
          estudio.titulacion_tecnica AS titulacion_tecnica
   FROM   BC2_EXT_ESTUDIOS estudio,
          BC2_BECAS becas,
          BC2_SOLICITANTES solicitud,
          BC2_MINIMOS minimos,
          BC2_CURSOS_ACADEMICOS curso_aca
   WHERE  estudio.id = becas.estudio_id
   AND    estudio.id = minimos.estudio_id
   AND    minimos.CURSO_ACADEMICO_ID = curso_aca.id
   AND    solicitud.id = becas.solicitante_id
   AND    solicitud.curso_academico_id = curso_aca.id;

   
   