CREATE TABLE uji_becas.bc2_becas_denegaciones 
    ( 
     id NUMBER  NOT NULL , 
     denegacion_id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     orden_denegacion NUMBER 
    ) 
;



ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_denegaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_denegaciones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL CHECK ( activa IN (0, 1)) , 
     codigo VARCHAR2 (100)  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_denegaciones 
    ADD CONSTRAINT bc2_denegaciones_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_deneg_becas_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_deneg_deneg_FK FOREIGN KEY 
    ( 
     denegacion_id
    ) 
    REFERENCES uji_becas.bc2_denegaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_denegaciones 
    ADD CONSTRAINT bc2_denegaciones_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;







CREATE TABLE uji_becas.bc2_diccionario 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     clase_uji VARCHAR2 (1000)  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     valor_origen VARCHAR2 (1000)  NOT NULL , 
     valor_uji NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_diccionario_PK PRIMARY KEY ( id ) ;



ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_dic_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_dic_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;




Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (1, 'Superar els llindars de patrimoni, despr�s de consultar les dades econ�miques de la seva fam�lia en la AEAT', 1, 1, '0402');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (2, 'Falta el titular principal de la declaraci�n de la renta a la que pertenece el solicitante o algun familiar', 1, 1, '0408');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (3, 'Por existir varias delcaraciones ante la AEAT con respecto algun miembro de la unidad familar', 1, 1, '0411');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (4, 'No complir l''edat permesa que s''estableix en la convocat�ria.', 1, 1, '0113');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (5, 'No trobar-se en situaci� legal d''atur', 1, 1, '0151');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (6, 'Exist�ncia de sol�licituds en les quals concorren circumst�ncies per a una assignaci� preferent d''acord amb els criteris establits en les bases de la convocat�ria.', 1, 1, '0115');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (7, 'Per no estar en possessi� de t�tol universitari', 1, 1, '0153');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (8, 'Per no acreditar el sol�licitant l''edat requerida en les bases de la corresponent convocat�ria', 1, 1, '0154');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (9, 'Per no acreditar el sol�licitant la situaci� legal d''atur requerida en les bases de la corresponent convocat�ria.', 1, 1, '0155');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (10, 'Per no acreditar el sol�licitant la situaci� de perceptor de prestacions d''atur requerida en les bases de la corresponent convocat�ria.', 1, 1, '0156');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (11, 'Per no estar matriculat en estudis emparats per la corresponent convocat�ria', 1, 1, '0157');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (12, 'No estar matriculat en el curs anterior en un curs complet de M�ster oficial.', 1, 1, '0158');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (13, 'Per haver incl�s com membre computable de la seua unitat familiar a un perceptor de Renda B�sica d''Emancipaci�.', 1, 1, '0414');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (14, 'En el cas d''alumnat universitari que realitza estudis no presencials, no tenir el m�nim de cr�dits aprovats o matriculats.', 1, 1, '0305');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (15, 'Haver causat baixa en el centre d''estudis abans de la finalitzaci� del curs.', 1, 1, '0131');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (16, 'Tenir els serveis coberts pel Govern de la comunitat aut�noma', 1, 1, '0132');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (17, 'No est� o no ha estado percibiendo prestaci�n o subsidio por desempleo concargo al presupuestodel SPE a la fecha que exige la convocat�ria', 1, 1, '0416');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (18, 'No estar matriculat en el curs actual en el segon curs dels mateixos estudis per als quals va obtenir beca el curs anterior', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (19, 'No se encuentra en situaci�n laboral de desempleo e inscrito como demandandate de empleo a fecha de la convocat�ria.', 1, 1, '0415');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (20, 'Presumpta ocultaci� de membres computables de la seva fam�lia i dades econ�miques', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (21, 'Superar els llindars en acumulaci� de patrimoni, despr�s de consultar les dades econ�miques de la seva fam�lia en la AEAT', 1, 1, '0417');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (22, 'Per superar els llindars de valors cadastrals, despr�s de consultar les dades que sobre la seva unitat familiar obren en poder de l''Administraci� tribut�ria', 1, 1, '0210');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (23, 'Supera valors cadastrals', 2, 1, '14');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (24, 'Superar els llindars dels valors catastrals, despr�s de consultar les dades econ�miques de la seva fam�lia en la AEAT', 1, 1, '0413');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (25, 'Superar el volum de negoci', 1, 1, '0407');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (26, 'Falten familiars a efectes fiscals', 1, 1, '0409');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (27, 'Sol�licitar ajuda per a estudis no emparats per la convocat�ria', 2, 1, '1');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (28, 'Posseir t�tol que l''habilite per a l''exercici professional o estar en disposici� legal de obtenir-lo i/o no ser la beca o ajuda que sol�licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', 1, 1, '0102');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (29, 'Posseir t�tol que l''habilite per a l''exercici professional o estar en disposici� legal de obtenir-lo i/o no ser la beca o ajuda que sol�licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', 2, 1, '2');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (30, 'Superar els llindars de renda, despr�s de consultar les dades econ�miques de la seva fam�lia en la AEAT', 1, 1, '0401');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (31, 'Superar els llindars de renda establits per a la concesi� de beca', 2, 1, '3.1');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (32, 'Sol�licitar ajuda per a estudis no emparats per la convocat�ria', 1, 1, '0101');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (33, 'Superar els llindars de renda establits per a l''exempci� de preus per serveis acad�mics o beca b�sica', 1, 1, '0202');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (34, 'Superar els llindars establits per a la concessi� de la beca', 1, 1, '0201');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (35, 'No tindre dret a l''excepci� de taxes i superar el primer llindar econ�mic de renda disponible (cent.privat)', 1, 1, '0203');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (36, 'No arribar a la nota mitjana m�nima exigida entre les convocat�ries de juny i setembre.', 1, 1, '0301');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (37, 'No arribar a la nota mitjana m�nima exigida entre les convocat�ries de juny i setembre.', 2, 1, '4');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (38, 'No haver aprovat el nombre m�nim d''assignatures o cr�dits establits en les bases de la  convocat�ria', 1, 1, '0302');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (39, 'No haver aprovat el nombre m�nim d''assignatures o cr�dits establits en les bases de la  convocat�ria', 2, 1, '5');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (40, 'No consignar en la sol�licitud les dades b�siques o no haver aportat la documentaci� necess�ria per a la resoluci� d''aquesta, a pesar d''haver-se-li requerit', 1, 1, '0103');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (41, 'No consignar en la sol�licitud les dades b�siques o no haver aportat la documentaci� necess�ria per a la resoluci� de la convocat�ria, a pesar d''haver-se-li requerit', 2, 1, '6');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (42, 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el r�gim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', 2, 1, '7');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (43, 'Exist�ncia de sol�licituds en qu� concorren circumst�ncies per a una assignaci� preferent d''acord amb el que establixen les bases de la convocat�ria.', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (44, 'Algun membre de la unitat familiar est� obligat a presentar declaraci� de patrimoni', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (45, 'No estar matriculat en el nombre m�nim exigit de cr�dits en el curs anterior o �ltim realitzat.', 1, 1, '0303');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (46, 'No estar matriculat/ada en el nombre m�nim exigit de cr�dits en aquest curs', 1, 1, '0304');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (47, 'No estar matriculat en el nombre m�nim exigit de cr�dits en el curs anterior o �ltim realitzat.', 2, 1, '9.1');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (48, 'P�rdua de curs/os lectiu/s en el sup�sit de canvi d''estudis cursats totalment o parcialment amb la condici� de becari/�ria.', 1, 1, '0104');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (49, 'P�rdua de curs/os lectiu/s en el sup�sit de canvi d''estudis cursats totalment o parcialment amb la condici� debecari/�ria.', 2, 1, '10');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (50, 'Gaudir d''ajuda o beca incompatible', 1, 1, '0106');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (51, 'Gaudir d''ajuda o beca incompatible', 2, 1, '12');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (52, 'No complir els terminis establits per a la presentaci� de la sol�licitut i/o dels documents', 1, 1, '0107');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (53, 'No complir els terminis establits per a la presentaci� de la sol�licitut i/o dels documents', 2, 1, '13');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (54, 'No haver-se matriculat d''un curs complet', 1, 1, '0306');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (55, 'Superar els llindars de renda establits per a la concesi� de beca', 2, 1, '3.2');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (56, 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el l�mit establit en les bases  de la convocat�ria.', 1, 1, '0204');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (57, 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el l�mit establit en l''ordre de convocat�ria', 2, 1, '14.1');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (58, 'La facturaci� del negoci/activitat econ�mica supera el llindar establit en les bases de la de convocat�ria.', 1, 1, '0205');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (59, 'La facturaci� del negoci/activitat econ�mica supera el l�mit establit en l''ordre de convocat�ria', 2, 1, '14.2');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (60, 'El valor cadastral de les finques r�stiques supera els llindars patrimonials establits en les bases de la convocat�ria.', 1, 1, '0206');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (61, 'El valor cadastral de les finques r�stiques  supera el l�mit establit en l''ordre de convocat�ria', 2, 1, '14.3');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (62, 'La suma dels rendiments nets del capital mobiliari m�s el saldo net de guanys i p�rdues patrimonials supera els l�mits establits en les bases de la  convocat�ria.', 1, 1, '0207');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (63, 'La suma dels rendiments nets del capital mobiliari m�s el saldo net de guanys i p�rdues patrimonials supera els l�mits establits en l''ordre de convocat�ria', 2, 1, '14.4');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (64, 'El conjunt d''elements patrimonials supera el l�mit establit en les bases de la convocat�ria.', 1, 1, '0208');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (65, 'La suma d''elements patrimonials supera el l�mit establit en l''ordre de convocat�ria', 2, 1, '14.5');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (66, 'El conjunt d''elements patrimonials supera el l�mit establit en l''ordre de convocat�ria', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (67, 'Per deduir-se de les dades que consten en el seu expedient, que el llindar patrimonial de la unitat familiar supera l''establit per a la concessi� de la beca.', 2, 1, '15');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (68, 'Per haver-se comprovat inexactitud en les dades acad�miques aportades', 1, 1, '0308');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (69, 'No acreditar suficientment, segons el parer de la comissi� competent,  la independ�ncia econ�mica i/o familiar', 1, 1, '0114');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (70, 'No acreditar suficientment, segons el parer de la comissi� competent,  la independ�ncia econ�mica i/o familiar', 2, 1, '17');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (71, 'Haver sigut beneficiari/�ria de beca el nombre m�xim d''anys que permet l''ordre de convocatoria.', 1, 1, '0109');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (72, 'Haver sigut beneficiari/�ria de beca el nombre m�xim d''anys que permet l''orde de convocat�ria', 2, 1, '18');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (73, 'No complir els requisits acad�mics exigits en l''ordre de convocat�ria', 1, 1, '0110');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (74, 'Per obtenir la beca en la convocat�ria del Ministeri d''Educaci� i Ci�ncia.', 2, 1, '27');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (75, 'Per no acreditar la resid�ncia en la CCVV amb anterioritat a la data de finalitzaci� del t�rmini', 2, 1, '28');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (76, 'Per haver renunciat o haver-li sigut anul�lada la matr�cula en el present curs.', 2, 1, '30');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (77, 'No estar matriculat en el nombre m�nim exigit de cr�dits en aquest curs', 2, 1, '9.2');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (78, 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el r�gim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', 1, 1, '0116');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (79, 'Superar els llindars de capital mobiliari o agregaci� de valors cadastrals m�s capital mobiliari, despr�s de consultar les dades que sobre la seva familia obren en poder de la AEAT', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (80, 'L''ag�ncia tribut�ria t� coneixement que s''ha presentat declaraci� renda o liquidaci� 104', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (81, 'Els NIF de la unitat familiar no estan identificats per la  AEAT', 1, 1, '0406');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (82, 'L''Administraci� Tribut�ria no posseeix dades econ�miques de la unitat familiar', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (83, 'L''ag�ncia tribut�ria considera que per les dades que consten en el seu poder deuria haver declaraci� de renda', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (84, 'Per tindre l''obligaci� de presentar declaraci� per l''Impost Extraordinari sobre el Patrimoni, d''acord amb la normativa reguladora del dit impost.', 1, 1, '0209');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (85, 'No estar matriculat en el curs seg�ent segons el pla d''estudis vigent', 1, 1, '0307');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (86, 'Superar la seva renda tots els llindars establerts (AEAT)', 2, 1, '81');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (87, 'Algun membre de la unitat familiar est� obligat a presentar declaraci� de patrimoni', 2, 1, '82');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (88, 'Superar els llindars del cabdal mobiliari', 2, 1, '83');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (89, 'L''ag�ncia tribut�ria t� coneixement que s''ha presentat declaraci� renda o liquidaci� 104', 2, 1, '86');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (90, 'Els NIF de la unitat familiar no estan identificats per la  AEAT', 2, 1, '84');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (91, 'Els membres de la unitat familiar no estan identificats econ�micament per la  AEAT', 2, 1, '85');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (92, 'L''ag�ncia tribut�ria considera que per les dades que consten en el seu poder deuria haver declaraci� de renda', 2, 1, '87');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (93, 'No existir independ�ncia familiar, en apar�ixer incl�s en una altra declaraci� de la renda.', 1, 1, '0405');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (94, 'Alumne independent', 2, 1, '88');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (95, 'Per haver-se comprovat inexactitud en les dades acad�miques aportades', 2, 1, '16');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (96, 'Es deu presentar certificat de Renda de l''exercici anterior del membre indicat', 2, 1, '3');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (97, 'Ocultaci� de valors cadastrals.', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (98, 'Situaci� irregular davant AEAT. Algun membre computable es troba en dues declaracions de la renda.', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (99, 'No complir els requisits acad�mics exigits en l''ordre de convocat�ria', 2, 1, '19');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (100, 'Pendiente de definr', 1, 1, '--');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (101, 'No complir el requisit de trobar-se el sol�licitant o els seus sustentadors treballant a Espanya (art. 1.2.d) de l''Ordre EDU 1901/2009, per la qual es convoquen beques de car�cter general i de mobilitat per a estudiants d''ensenyaments universitaris) (BOE del 15 de juliol).', 1, 1, '0146');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (102, 'Superar els llindars de renda establits per a la concessi� de l''ajuda compensat�ria, beca-salari  o mobilitat especial', 1, 1, '0212');
Insert into UJI_BECAS.BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, CODIGO)
 Values
   (103, 'Falta informaci� fiscal.', 1, 1, '0211');
COMMIT;



ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_convalidados  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_para_beca  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (curso  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tercio_presentado  NUMBER                     DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (rendimiento_academico  NUMBER                 DEFAULT 0);

 
 
 
 
 ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (nota_acceso  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (nota_media  NUMBER);
 
 
 
drop table uji_becas.bc2_ext_localidades cascade constraints;

CREATE TABLE uji_becas.bc2_localidades 
    ( 
     id NUMBER  NOT NULL , 
     provincia_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     codigo VARCHAR2 (100) , 
     codigo_ayuntamiento VARCHAR2 (100) , 
     nombre_ayuntamiento VARCHAR2 (1000) , 
     baja NUMBER DEFAULT 0  NOT NULL CHECK ( baja IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_localidades 
    ADD CONSTRAINT bc2_localidades_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_localidades 
    ADD CONSTRAINT bc2_localidades_pro_FK FOREIGN KEY 
    ( 
     provincia_id
    ) 
    REFERENCES uji_becas.bc2_ext_provincias 
    ( 
     id
    ) 
;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_loc_FK FOREIGN KEY 
    ( 
     localidad_id
    ) 
    REFERENCES uji_becas.bc2_localidades 
    ( 
     id
    ) 
;

alter table uji_becas.bc2_convocatorias add nombre varchar2(100);

