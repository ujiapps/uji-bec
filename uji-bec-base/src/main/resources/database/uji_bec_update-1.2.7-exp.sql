ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (creditos_1a  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (creditos_2a  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (creditos_3a  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
  ADD (OTRA_BECA_CONCEDIDA  NUMBER                   DEFAULT 0                     NOT NULL)

 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_1a  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_2a  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_3a  NUMBER);

CREATE OR REPLACE procedure UJI_BECAS.sincroniza_datos_pop(p_persona in number, p_estudio in number, p_curso in number) is
begin
   declare
      cursor c_masters is
         select *
           from bc2_ext_personas_estudios e
          where persona_id = p_persona
            and estudio_id = p_estudio
            and curso_academico_id = p_curso;

      v_crd_beca               number;
      v_crd_faltan             number;
      v_curso                  number;
      v_curso_estudio          number;
   begin
      for x in c_masters loop
         v_crd_faltan := gra_pop.crd_faltan_becas_ca(x.persona_id, x.estudio_id, x.curso_academico_id);
       begin
         update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_pop.crd_matriculados(x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_superados = gra_pop.crd_sup_becas(x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes = v_crd_faltan,
                creditos_pendientes_conva = gra_pop.crd_pte_convalidacion(x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca = (pack_pop.crd_matriculados(x.persona_id, x.estudio_id, x.curso_academico_id) - gra_pop.crd_mat_becas(x.persona_id, x.estudio_id, x.curso_academico_id)),
                creditos_presentados = gra_pop.CRD_PRESENTADOS(x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_1a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 1, 'C'),
                importes_1a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 1, 'I'),
                creditos_2a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 2, 'C'),
                importes_2a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 2, 'I'),
                creditos_3a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 3, 'C'),
                importes_3a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 3, 'I')
          where x.id = e.id;

         commit;

         update bc2_ext_personas_estudios e
            set limitar_creditos = 0, limitar_creditos_fin_estudios = uji_becas.limitar_creditos_fin_curso(x.persona_id, x.estudio_id, x.curso_academico_id), matricula_parcial = '0'
          where x.id = e.id;

         commit;

         update bc2_ext_personas_estudios e
            set creditos_complementos = gra_pop.crd_complementos_pop(x.persona_id, x.estudio_id)
          where x.id = e.id;

         commit;
         ------------
         update bc2_ext_personas_estudios e
            set numero_becas_mec = gra_bec.pack_bec_2011.numbecas(x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id) + UJI_BECAS.num_becas(x.persona_id, x.curso_academico_id, 1, x.estudio_id),
                numero_becas_cons = gra_bec.pack_bec_2011.numbecas(x.persona_id, x.curso_academico_id, 'CON', x.estudio_id) + UJI_BECAS.num_becas(x.persona_id, x.curso_academico_id, 2, x.estudio_id)
          where x.id = e.id;
         ---------------------
         commit;
         --FALTA CALCULAR MINUSVALIA
         v_crd_beca := GRA_POP.CRD_MAT_BECAS(x.persona_id, x.estudio_id, x.curso_academico_id);

         if v_crd_faltan < v_crd_beca then
            v_crd_beca := v_crd_faltan;

            update bc2_ext_personas_estudios e
               set creditos_fuera_beca = (pack_pop.crd_matriculados(x.persona_id, x.estudio_id, x.curso_academico_id) - v_crd_beca)
             where x.id = e.id;
         end if;

         v_curso := gra_pop.curso_mat_master(x.persona_id, x.estudio_id, x.curso_academico_id);

         -- parece que aunque se de un curso hay que poner el numero de cursos que lleva matriculado.
         --            begin
         --               select numero_cursos
         --               into   v_curso_estudio
         --               from   bc2_ext_estudios
         --               where  id = x.estudio_id;

         --               if v_curso_estudio < v_curso then
         --                  v_curso := v_curso_estudio;
         --               end if;
         --            exception
         --               when no_data_found then
         --                  null;
         --            end;

         update bc2_ext_personas_estudios e
            set minusvalia = decode(gra_pop.minusvalia_pop(x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                numero_semestres = decode(gra_pop.crd_mismo_semestre(x.curso_academico_id, x.persona_id, x.estudio_id), 'N', 2, 1),
                programa_intercambio = decode(grs_ocie.erasmus(x.persona_id, x.curso_academico_id), 'S', 1, 0),
                creditos_convalidados = gra_pop.crd_convalidados(x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = v_crd_beca,
                curso = v_curso
          where x.id = e.id;

         commit;

         update bc2_ext_personas_estudios e
            set nota_acceso = gra_pop.nota_acceso(x.persona_id, x.estudio_id, x.curso_academico_id),
                PROCEDENCIA_NOTA_ID = '03',
                nota_media = gra_pop.media_exp_beca(x.persona_id, x.estudio_id, x.curso_academico_id, 'S', 'S'),
                media_sin_suspensos = gra_pop.media_exp_beca(x.persona_id, x.estudio_id, x.curso_academico_id, 'S', 'N'),
                nota_media_ultimo_curso = decode(v_curso, 1, GRA_POP.MEDIA_ULTIMO_CURSO_TIT_ACCESO(x.persona_id, x.estudio_id, x.curso_academico_id), null) -- conselleria
          where x.id = e.id;

         commit;

         update bc2_ext_personas_estudios e
            set familia_numerosa_id = decode(gra_pop.familia_numerosa_pop(x.persona_id, x.estudio_id, x.curso_academico_id), 'G', 1, 'E', 2, null),
                becario = decode(gra_pop.es_becario_pop(x.persona_id, x.estudio_id, x.curso_academico_id,'B'), 'S', 1, 0),
                tipo_matricula = GRA_POP.TIPO_MATRICULA_POP(x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_POP.ESTUDIO_ACCESO(x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO(x.persona_id, x.estudio_id)
          where x.id = e.id;

         update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode(GRA_BEC.ALGUNA_VEZ_MEDIA_BECA(x.persona_id, x.estudio_id) + UJI_BECAS.alguna_beca_parcial(x.persona_id, x.estudio_id), 0, 0, 1),
                -- tiene_beca_mec =  decode (gra_bec.ES_BECARIO_CURSO (x.persona_id, x.estudio_id, x.curso_academico_id, 'MIN'),'S', 1, 0)
                tiene_beca_mec = decode(uji_becas.ES_BECARIO_CURSO(x.persona_id, x.estudio_id, x.curso_academico_id, 1), 'S', 1, 0)
          where x.id = e.id;

         commit;
       exception
       when others then null;
       end;
      end loop;
   end;
end sincroniza_datos_pop;
/


CREATE OR REPLACE procedure UJI_BECAS.sincroniza_datos_g(p_persona in number, p_estudio in number, p_curso in number, p_parte in number default 999 /*
                                                                                                                                                 NOTA_ESPECIFICA
                                                                                                                                                 PROCEDENCIA_NOTA_ID
                                                                                                                                                 MEDIA_SIN_SUSPENSOS
                                                                                                                                                 */
) is
begin
   declare
      cursor c_titus is
         select e.*
           from bc2_ext_personas_estudios e
          where persona_id = p_persona
            and estudio_id = p_estudio
            and curso_academico_id = p_curso;
           -- and persona_id in (select id from bc2_ext_personas where identificacion = '20905294L');

      vcrd_faltan                   number;
      vcrd_crd_becas                number;
      vcreditos_fuera_beca          number;
      vIdError                      number;
      verror                        varchar2(1000);
      vcontrol                      number;
   begin
      for x in c_titus loop
         select nvl(max(id) + 1, 1)
           into vIdError
           from BC2_ERRORES_SINCRONIZACION;

         if p_parte = 1 then
            begin
               vcrd_crd_becas := gra_exp.crd_mat_beca(x.curso_academico_id, x.persona_id, x.estudio_id);
               vcontrol := 1;
               vcrd_faltan := pack_exp.crd_faltantes_ca(x.persona_id, x.estudio_id, x.curso_academico_id - 1) + pack_exp.crd_faltantes_cf_ca(x.curso_academico_id - 1, x.persona_id, x.estudio_id);
               vcontrol := 2;


               if vcrd_crd_becas > vcrd_faltan then
                  vcreditos_fuera_beca := vcrd_crd_becas - vcrd_faltan;
                  vcrd_crd_becas := vcrd_faltan;
               else
                  vcontrol := 3;
                  vcreditos_fuera_beca := (pack_exp.crd_matriculados(x.curso_academico_id, x.persona_id, x.estudio_id) - gra_exp.crd_mat_beca(x.curso_academico_id, x.persona_id, x.estudio_id));
               end if;

               vcontrol := 4;
               update bc2_ext_personas_estudios e
                  set creditos_matriculados = pack_exp.crd_matriculados(x.curso_academico_id, x.persona_id, x.estudio_id),
                      creditos_superados = GRA_EXP.crd_sup_beca(x.curso_academico_id, x.persona_id, x.estudio_id),
                      creditos_faltantes = vcrd_faltan,
                      creditos_pendientes_conva = gra_exp.crd_pte_convalidacion_exp(x.persona_id, x.estudio_id, x.curso_academico_id),
                      creditos_fuera_beca = vcreditos_fuera_beca,
                      creditos_presentados = pack_exp.crd_presentados(x.curso_academico_id, x.persona_id, x.estudio_id),
                      matricula_parcial = uji_becas.matricula_parcial(x.curso_academico_id, vcrd_crd_becas, x.estudio_id),
                      creditos_1a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 1, 'C'),
                      importes_1a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 1, 'I'),
                      creditos_2a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 2, 'C'),
                      importes_2a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 2, 'I'),
                      creditos_3a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 3, 'C'),
                importes_3a = CREDITOS_POR_NUMERO_MAT(x.persona_id, x.estudio_id, x.curso_academico_id, 3, 'I')
                where x.id = e.id;
               commit;
               vcontrol := 5;
               update bc2_ext_personas_estudios e
                  set creditos_complementos = gra_exp.creditos_cf(x.persona_id, x.estudio_id)
                where x.id = e.id;
               commit;
            exception
               when others then
                  verror := sqlerrm;
                  insert into BC2_ERRORES_SINCRONIZACION(ID, PERSONA_ID, FECHA, ERROR_ID, ERROR, ESTUDIO_ID, CURSO_ACADEMICO_ID, HORA)
                      values (vIdError, x.persona_id, trunc(sysdate), vcontrol, verror, x.estudio_id, x.curso_academico_id, to_char(sysdate, 'HH:MI AM'));
            end;
         end if;

         if p_parte > 1 then
            begin
               vcontrol := 6;
               update bc2_ext_personas_estudios e
                  set limitar_creditos = gra_exp.limitar_creditos(x.persona_id, x.estudio_id, x.curso_academico_id), limitar_creditos_fin_estudios = uji_becas.limitar_creditos_fin_curso(x.persona_id, x.estudio_id, x.curso_academico_id)
                -- beca_parcial = uji.becas.beca_parcial(x.curso_academico_id,vcrd_crd_becas,x.estudio_id)
                where x.id = e.id;

               commit;
               ------------------------  Calculo de las Becas
               vcontrol := 7;
               update bc2_ext_personas_estudios e
                  set numero_becas_mec = gra_bec.pack_bec_2011.numbecas(x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id) + UJI_BECAS.num_becas(x.persona_id, x.curso_academico_id, 1, x.estudio_id),
                      numero_becas_cons = gra_bec.pack_bec_2011.numbecas(x.persona_id, x.curso_academico_id, 'CON', x.estudio_id) + UJI_BECAS.num_becas(x.persona_id, x.curso_academico_id, 2, x.estudio_id)
                where x.id = e.id;
               ----------------------
               commit;
               vcontrol := 8;
               update bc2_ext_personas_estudios e
                  set minusvalia = decode(gra_exp.minusvalia_exp(x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                      numero_semestres = decode(gra_exp.crd_mismo_semestre(x.curso_academico_id, x.persona_id, x.estudio_id), 'N', 2, 1),
                      programa_intercambio = decode(grs_ocie.erasmus(x.persona_id, x.curso_academico_id), 'S', 1, 0),
                      creditos_convalidados = gra_exp.crd_convalidados_exp(x.persona_id, x.estudio_id, x.curso_academico_id),
                      curso = gra_exp.curso_mat_beca(x.persona_id, x.estudio_id, x.curso_academico_id)
                where x.id = e.id;

               commit;
               vcontrol := 9;
               update bc2_ext_personas_estudios e
                  set nota_acceso = gra_exp.nota_acceso_exp(x.persona_id, x.estudio_id, x.curso_academico_id),
                      nota_media = gra_exp.media_curso_beca(x.persona_id, x.estudio_id, x.curso_academico_id),
                      media_sin_suspensos = gra_exp.media_sin_suspensos(x.persona_id, x.estudio_id, x.curso_academico_id),
                      NOTA_ESPECIFICA = gra_exp.NOTA_ESPECIFICA(x.persona_id, x.estudio_id, x.curso_academico_id),
                      PROCEDENCIA_NOTA_ID = gra_exp.PROCEDENCIA_NOTA_BECAS_MEC(x.persona_id, x.estudio_id, x.curso_academico_id)
                where x.id = e.id;

               commit;
               vcontrol := 10;
               update bc2_ext_personas_estudios e
                  set familia_numerosa_id = decode(gra_exp.familia_numerosa_exp(x.persona_id, x.estudio_id, x.curso_academico_id), 'G', 1, 'E', 2, null),
                      becario = decode(gra_exp.es_becario(x.persona_id, x.estudio_id, x.curso_academico_id,'B'), 'S', 1, 0),
                      tipo_matricula = GRA_EXP.TIPO_MATRICULA_EXP(x.persona_id, x.estudio_id, x.curso_academico_id),
                      estudio_acceso_id = GRA_EXP.ESTUDIO_ACCESO_EXP(x.persona_id, x.estudio_id, x.curso_academico_id),
                      posee_titulo = GRA_TIT.POSEE_TITULO(x.persona_id, x.estudio_id)
                where x.id = e.id;

               commit;
               vcontrol := 11;
               update bc2_ext_personas_estudios e
                  set alguna_beca_parcial = decode(gra_bec.alguna_vez_media_beca(x.persona_id, x.estudio_id) + UJI_BECAS.alguna_beca_parcial(x.persona_id, x.estudio_id), 0, 0, 1),
                      tiene_beca_mec = decode(uji_becas.ES_BECARIO_CURSO(x.persona_id, x.estudio_id, x.curso_academico_id, 1), 'S', 1, 0)
                where x.id = e.id;

               commit;
            exception
               when others then
                select nvl(max(id) + 1, 1)
                 into vIdError
                  from BC2_ERRORES_SINCRONIZACION;
                  verror := sqlerrm;
                  insert into BC2_ERRORES_SINCRONIZACION(ID, PERSONA_ID, FECHA, ERROR_ID, ERROR, ESTUDIO_ID, CURSO_ACADEMICO_ID, HORA)
                      values (vIdError, x.persona_id, trunc(sysdate), vcontrol, verror, x.estudio_id, x.curso_academico_id, to_char(sysdate, 'HH:MI AM'));
            end;
         end if;

         select count( * )
           into vcontrol
           from bc2_ext_personas_estudios
          where (CREDITOS_FALTANTES = -1
              or CREDITOS_PARA_BECA = -1
              or NOTA_ACCESO = -1)
            and persona_id = x.persona_id
            and estudio_id = x.estudio_id
            and curso_academico_id = x.curso_academico_id;
         if vcontrol > 0 then
            select nvl(max(id) + 1, 1)
              into vIdError
              from BC2_ERRORES_SINCRONIZACION;

            insert into BC2_ERRORES_SINCRONIZACION(ID, PERSONA_ID, FECHA, ERROR_ID, ERROR, ESTUDIO_ID, CURSO_ACADEMICO_ID, HORA)
                values (vIdError, x.persona_id, trunc(sysdate), vcontrol, 'CREDITOS_FALTANTES or CREDITOS_PARA_BECA or NOTA_ACCESO = -1', x.estudio_id, x.curso_academico_id, to_char(sysdate, 'HH:MI AM'));
         end if;
      end loop;
   end;
end sincroniza_datos_g;
/
-- modificacion de la tabla bc2_ext_personas_estudios
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (importes_1A  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (IMPORTES_2A  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (IMPORTES_3A  NUMBER);

---vista
DROP VIEW UJI_BECAS.BC2_EXT_TODAS_CONSELLERIA;

/* Formatted on 20/07/2016 10:40:35 (QP5 v5.115.810.9015) */
create or replace force view UJI_BECAS.BC2_EXT_TODAS_CONSELLERIA(PERSONA_ID,
SOLICITANTE_ID, BECA_ID, CURSO_ACADEMICO_ID, CONVOCATORIA_ID, PROCESO_ID, ESTADO_ID, UT, APELLIDO1, APELLIDO2, NOMBRE, SEXO, CODIGO_POSTAL, LOCALIDAD, NIF, FECHA_NACIMIENTO, ESTUDIO_ID_MEC, ESTUDIO_ID, CURSO, CICLO, NOTA_MEDIA,
CODIGO_CENTRO, CREDITOS_MATRICULADOS, CREDITOS_1A, IMPORTE_1A, CREDITOS_2A, IMPORTE_2A, TASAS_TOTAL, CREDITOS_CUBIERTOS, TIPOBECA, DEN1, DEN2, DEN3, DEN4, DEN5, DEN6) as
   select s.persona_id, b.solicitante_id, b.id beca_id, S.CURSO_ACADEMICO_ID, B.CONVOCATORIA_ID, b.proceso_id, decode(b.beca_concedida, 1, 5, b.estado_id) estado_id, 'UJI' ut, substr(m.apellido1, 1, 25) apellido1,
          substr(m.apellido2, 1, 25) apellido2, substr(m.nombre, 1, 25) nombre, decode(m.sexo_id, 1, 0, 2) sexo, nvl(domi.codigo_Postal, '00000') codigo_postal, substr(loc.nombre, 1, 25) localidad, B.IDENTIFICACION nif, m.FECHA_NACIMIENTO,
          DIC.VALOR_ORIGEN estudio_id_mec, e.estudio_id, nvl(b.curso, 1) curso, '1' ciclo, b.nota_media_ant, ee.codigo_centro, e.creditos_matriculados,
          e.creditos_1a, e.importes_1a,e.creditos_2a, e.importes_2a,
          e.tasas_total, e.creditos_cubiertos, (select count( * )
                                                  from bc2_becas bb, bc2_solicitantes ss
                                                 where bb.solicitante_id = ss.id
                                                   and ss.curso_academico_id = s.curso_academico_id
                                                   and bb.solicitante_id = b.solicitante_id
                                                   and bb.convocatoria_id <> 3)
                                                  tipobeca, den_1, den_2, den_3, den_4, den_5, den_6
     from bc2_becas b, bc2_convocatorias c, bc2_miembros m, bc2_solicitantes s, bc2_ext_personas_estudios e, bc2_diccionario dic, bc2_ext_estudios ee, bc2_domicilios domi, bc2_localidades loc, (  select beca_id,
                                                                                                                                                                                                           min(case when orden = 1 then denegacion_id else null end) den_1,
                                                                                                                                                                                                           min(case when orden = 2 then denegacion_id else null end) den_2,
                                                                                                                                                                                                           min(case when orden = 3 then denegacion_id else null end) den_3,
                                                                                                                                                                                                           min(case when orden = 4 then denegacion_id else null end) den_4,
                                                                                                                                                                                                           min(case when orden = 5 then denegacion_id else null end) den_5,
                                                                                                                                                                                                           min(case when orden = 6 then denegacion_id else null end)
                                                                                                                                                                                                              den_6
                                                                                                                                                                                                      from (select beca_id, denegacion_id,
                                                                                                                                                                                                                   (row_number()
                                                                                                                                                                                                                       over (partition by beca_id
                                                                                                                                                                                                                             order by
                                                                                                                                                                                                                                denegacion_id))
                                                                                                                                                                                                                      orden
                                                                                                                                                                                                              from bc2_becas_denegaciones)
                                                                                                                                                                                                  group by beca_id) den
    where b.convocatoria_id = c.id
      and c.organismo_id = 2
      and b.id = m.beca_id
      and m.tipo_miembro_id = 1
      and b.solicitante_id = s.id
      and s.persona_id = e.persona_id
      and b.estudio_id = e.estudio_id
      and s.CURSO_ACADEMICO_id = e.curso_academico_id
      and s.curso_academico_id = dic.curso_academico_id
      and e.estudio_id = dic.valor_uji
      and dic.clase_uji = 'Estudio'
      and dic.organismo_id = c.organismo_id
      and e.estudio_id = ee.id
      and domi.beca_id = b.id
      and domi.tipo_domicilio_id in (select min(d.tipo_domicilio_id)
                                       from bc2_domicilios d
                                      where d.beca_id = b.id)
      and domi.localidad_id = loc.id
      and b.id = den.beca_id(+)
   union
   select s.persona_id, b.solicitante_id, b.id beca_id, S.CURSO_ACADEMICO_ID, B.CONVOCATORIA_ID, b.proceso_id, decode(b.beca_concedida, 1, 5, b.estado_id) estado_id, 'UJI' ut, substr(m.apellido1, 1, 25) apellido1,
          substr(m.apellido2, 1, 25) apellido2, substr(m.nombre, 1, 25) nombre, decode(m.sexo_id, 1, 0, 2) sexo, nvl(domi.codigo_Postal, '00000') codigo_postal, substr(loc.nombre, 1, 25) localidad, B.IDENTIFICACION nif, m.FECHA_NACIMIENTO,
          DIC.VALOR_ORIGEN estudio_id_mec, e.estudio_id, nvl(b.curso, 1) curso, '1' ciclo, b.nota_media_ant, ee.codigo_centro, e.creditos_matriculados,
          e.creditos_1a, e.importes_1a,e.creditos_2a, e.importes_2a,
          e.tasas_total, e.creditos_cubiertos, (select count( * )
                                                  from bc2_becas bb, bc2_solicitantes ss
                                                 where bb.solicitante_id = ss.id
                                                   and ss.curso_academico_id = s.curso_academico_id
                                                   and bb.solicitante_id = b.solicitante_id
                                                   and bb.convocatoria_id <> 3)
                                                  tipobeca, den_1, den_2, den_3, den_4, den_5, den_6
     from bc2_becas b, bc2_convocatorias c, bc2_miembros m, bc2_solicitantes s, bc2_ext_personas_estudios e, bc2_diccionario_excepciones dic, bc2_ext_estudios ee, bc2_domicilios domi, bc2_localidades loc,
          (  select beca_id,
                    min(case when orden = 1 then denegacion_id else null end) den_1,
                    min(case when orden = 2 then denegacion_id else null end) den_2,
                    min(case when orden = 3 then denegacion_id else null end) den_3,
                    min(case when orden = 4 then denegacion_id else null end) den_4,
                    min(case when orden = 5 then denegacion_id else null end) den_5,
                    min(case when orden = 6 then denegacion_id else null end)
                       den_6
               from (select beca_id,
                            denegacion_id,
                            (row_number()
                                over (partition by beca_id
                                      order by
                                         denegacion_id))
                               orden
                       from bc2_becas_denegaciones)
           group by beca_id) den
    where b.convocatoria_id = c.id
      and c.organismo_id = 2
      and b.id = m.beca_id
      and m.tipo_miembro_id = 1
      and b.solicitante_id = s.id
      and s.persona_id = e.persona_id
      and b.estudio_id = e.estudio_id
      and s.CURSO_ACADEMICO_id = e.curso_academico_id
      and s.curso_academico_id = dic.curso_academico_id
      and e.estudio_id = dic.valor_uji
      and dic.clase_uji = 'Estudio'
      and dic.organismo_id = c.organismo_id
      and e.estudio_id = ee.id
      and domi.tipo_domicilio_id in (select min(d.tipo_domicilio_id)
                                       from bc2_domicilios d
                                      where d.beca_id = b.id)
      and domi.localidad_id = loc.id
      and domi.beca_id = b.id
      and b.id = den.beca_id(+);


GRANT SELECT ON UJI_BECAS.BC2_EXT_TODAS_CONSELLERIA TO ADM_BEC;

GRANT SELECT ON UJI_BECAS.BC2_EXT_TODAS_CONSELLERIA TO ADM_DOC;


DROP VIEW UJI_BECAS.BC2_EXT_IMPORTES_MEC;

/* Formatted on 20/07/2016 13:26:19 (QP5 v5.115.810.9015) */
create or replace force view UJI_BECAS.BC2_EXT_IMPORTES_MEc (PERSONA_ID, SOLICITANTE_ID, BECA_ID, CURSO_ACADEMICO_ID, CONVOCATORIA_ID, PROCESO_ID, ESTADO_ID, UT, CONV, NIF, SEXO, APELLIDO1, APELLIDO2, NOMBRE, TIPO_FAMILIA_ID, TASAS_TOTAL,
TASAS_MINISTERIO, TASAS_CONSELLERIA, CREDITOS_CUBIERTOS, CREDITOS_1A, IMPORTES_1A,CREDITOS_2A, IMPORTES_2A,CREDITOS_3A, IMPORTES_3A, CURSO_ACA, ESTUDIO_ID, TIPO_ESTUDIO, NOTA_MEDIA, ID_TITULACION) as
   (select s.persona_id, b.solicitante_id, b.id beca_id, s.curso_academico_id, b.convocatoria_id, b.proceso_id, b.estado_id, '40' ut, c.acronimo conv, B.IDENTIFICACION nif, p.sexo, p.persona_apellido1, p.persona_apellido2, p.persona_nombre,
           s.tipo_familia_id, e.tasas_total, e.tasas_ministerio, e.tasas_conselleria, e.creditos_cubiertos, e.creditos_1a, e.importes_1a,e.creditos_2a, e.importes_2a,e.creditos_3a,e.importes_3a,s.curso_academico_id || '/' || (s.curso_academico_id + 1) curso_aca, e.estudio_id, es.tipo tipo_estudio, b.nota_media_ant,
           valor_origen id_titulacion
      from bc2_becas b join bc2_solicitantes s on s.id = b.solicitante_id join bc2_ext_personas p on p.id = s.persona_id join bc2_ext_personas_estudios e
              on e.persona_id = p.id
             and e.curso_academico_id = s.curso_academico_id
           join bc2_ext_estudios es
              on es.id = e.estudio_id
             and es.id = b.estudio_id
           join (select distinct *
                   from bc2_diccionario d1
                 union
                 select *
                   from bc2_diccionario_excepciones d2) d
              on d.valor_uji = e.estudio_id
             and d.curso_academico_id = s.curso_academico_id
             and d.clase_uji = 'Estudio'
           join bc2_convocatorias c
              on b.convocatoria_id = c.id
             and c.organismo_id = d.organismo_id
     where d.organismo_id = 1
       and c.id in (1, 2)
       and b.beca_concedida = 1)
   union
   (select s.persona_id, b.solicitante_id, b.id beca_id, s.curso_academico_id, 99 convocatoria_id, b.proceso_id, b.estado_id, '40' ut, 'IN' conv, B.IDENTIFICACION nif, p.sexo, p.persona_apellido1, p.persona_apellido2, p.persona_nombre,
           s.tipo_familia_id, e.tasas_total, e.tasas_ministerio, e.tasas_conselleria, e.creditos_cubiertos,e.creditos_1a, e.importes_1a,e.creditos_2a, e.importes_2a,e.creditos_3a,e.importes_3a, s.curso_academico_id || '/' || (s.curso_academico_id + 1) curso_aca, e.estudio_id, es.tipo tipo_estudio, b.nota_media_ant,
           valor_origen id_titulacion
      from bc2_becas b join bc2_solicitantes s on s.id = b.solicitante_id join bc2_ext_personas p on p.id = s.persona_id join bc2_ext_personas_estudios e
              on e.persona_id = p.id
             and e.curso_academico_id = s.curso_academico_id
           join bc2_ext_estudios es
              on es.id = e.estudio_id
             and es.id = b.estudio_id
           join (select distinct *
                   from bc2_diccionario d1
                 union
                 select *
                   from bc2_diccionario_excepciones d2) d
              on d.valor_uji = e.estudio_id
             and d.curso_academico_id = s.curso_academico_id
             and d.clase_uji = 'Estudio'
           join bc2_convocatorias c
              on b.convocatoria_id = c.id
             and c.organismo_id = d.organismo_id
     where d.organismo_id = 1
       and c.id in (1, 2)
       and b.beca_concedida = 1
       and b.proceso_id = 4
       and b.estado_id = 5);


GRANT SELECT ON UJI_BECAS.BC2_EXT_IMPORTES_MEC_old TO ADM_DOC;

