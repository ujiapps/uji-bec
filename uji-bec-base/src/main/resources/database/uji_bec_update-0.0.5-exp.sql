ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (convocatoria_id  NUMBER);

ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (activa  NUMBER DEFAULT 1 NOT NULL );

update UJI_BECAS.BC2_CUANTIAS
set convocatoria_id = 1;

commit;

ALTER TABLE uji_becas.bc2_cuantias 
    ADD CONSTRAINT bc2_cuantias_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS (ID,
                                                                  PERSONA_ID,
                                                                  ESTUDIO_ID,
                                                                  CURSO_ACADEMICO_ID,
                                                                  CREDITOS_MATRICULADOS,
                                                                  CREDITOS_SUPERADOS,
                                                                  CREDITOS_FALTANTES,
                                                                  CREDITOS_PENDIENTES_CONVA,
                                                                  CREDITOS_FUERA_BECA,
                                                                  CREDITOS_PRESENTADOS,
                                                                  LIMITAR_CREDITOS,
                                                                  LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                  MATRICULA_PARCIAL,
                                                                  NUMERO_BECAS,
                                                                  NUMERO_SEMESTRES,
                                                                  SIN_DOCENCIA,
                                                                  PROGRAMA_INTERCAMBIO,
                                                                  CREDITOS_CONVALIDADOS,
                                                                  CREDITOS_PARA_BECA,
                                                                  CURSO,
                                                                  TERCIO_PRESENTADO,
                                                                  RENDIMIENTO_ACADEMICO,
                                                                  NOTA_ACCESO,
                                                                  NOTA_MEDIA,
                                                                  FAMILIA_NUMEROSA_ID
                                                                 ) AS
   select curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_tit_id estudio_id,
          curso_aca curso_academico_id,
          pack_exp.crd_matriculados (curso_Aca, exp_per_id, exp_tit_id) creditos_matriculados,
          pack_exp.crd_sup_ca (exp_per_id, exp_tit_id, curso_aca) creditos_superados,
          pack_exp.crd_faltantes_ca (exp_per_id, exp_tit_id, curso_aca) creditos_faltantes,
          (select nvl (sum (a.creditos), 0)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_asignaturas a
           where  estado in ('A', 'C', 'R')
           and    cco_id = 0
           and    mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    asi_id = a.id) creditos_pendientes_conva,
          decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id),
                  0, 0,
                  pack_exp.imp_creditos_no_beca (exp_tit_id, exp_per_id, curso_aca)
                  / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id)
                 ) creditos_fuera_beca,
          (select sum (a.creditos)
           from   (select distinct asc_mat_exp_per_id, asc_mat_exp_tit_id, asc_mat_curso_aca, asc_asi_id
                   from            gra_exp.exp_notas) n,
                  gra_pod.pod_asignaturas a
           where  asc_mat_exp_per_id = m.exp_per_id
           and    asc_mat_exp_tit_id = m.exp_tit_id
           and    asc_mat_curso_aca = m.curso_aca
           and    asc_asi_id = a.id) creditos_presentados,
          0 limitar_creditos, 0 limitar_creditos_fin_estudios,
          (select decode (count (*), 0, 0, 1)
           from   exp_mat_circunstancias
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    cir_id = 3) matricula_parcial,
          (select count (*)
           from   gra_bec.bec_solicitudes
           where  curso_aca < m.curso_Aca
           and    per_id = m.exp_per_id
           and    tit_id = m.exp_tit_id) numero_becas,
          (select count (distinct semestre)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_grupos p
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    ac.asi_id = p.asi_id
           and    mat_curso_aca = p.curso_aca) numero_semestres,
          (select decode (count (*), 0, 0, 1)
           from   exp_asi_cursadas
           where  mat_exp_per_id = exp_per_id
           and    mat_exp_tit_id = exp_tit_id
           and    mat_curso_aca = curso_aca
           and    grp_id = 'Z') sin_docencia,
          (select decode (count (*), 0, 0, 1)
           from   gra_exp.exp_sol_convocatorias
           where  tipo = 'E'
           and    asc_mat_exp_per_id = exp_per_id
           and    asc_mat_exp_tit_id = exp_tit_id
           and    asc_mat_curso_aca = curso_aca) programa_intercambio,
          0 creditos_convalidados, 0 creditos_para_beca, 0 curso, 0 tercio_presentado, 0 rendimiento_academico,
          10 nota_acceso, 10 nota_media, 1 familia_numerosa
   from   gra_exp.exp_matriculas m;

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN MINUSVALIA;

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN PROPIEDAD_DE_ID;

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN IMPORTE_ALQUILER;

ALTER TABLE UJI_BECAS.BC2_DOMICILIOS
 ADD (importe_alquiler  NUMBER);

 ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN CURSO_ACADEMICO_OTROS TO CURSO_ACADEMICO_ant;

ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN CODIGO_CENTRO TO CODIGO_CENTRO_ant;

ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (codigo  VARCHAR2(10));


update UJI_BECAS.BC2_CUANTIAS
set codigo = '0000';

commit;

ALTER TABLE UJI_BECAS.BC2_CUANTIAS
MODIFY(CODIGO  NOT NULL);

ALTER TABLE UJI_BECAS.BC2_DOMICILIOS
 ADD (tipo_residencia_id  NUMBER);


CREATE INDEX uji_becas.bc2_domicilios_res_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_residencia_id ASC 
    ) 
;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS DROP COLUMN TIPO_RESIDENCIA_ID;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipo_res_FK FOREIGN KEY 
    ( 
     tipo_residencia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_residencias 
    ( 
     id
    ) 
;


ALTER TABLE UJI_BECAS.BC2_LOCALIDADES
 ADD (distancia  NUMBER);

 
ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_becas_cuantias__UN UNIQUE ( beca_id , cuantia_id ) ;

ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_denegaciones__UN UNIQUE ( denegacion_id , beca_id , orden_denegacion ) ;

 
