ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (IBANPAIS  VARCHAR2(10)                        DEFAULT 'ES');

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (IBANDC  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (NOTA_PRUEBA_ESPECIFICA  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (PROCEDENCIA_NOTA  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (NOTA_MEDIA_SIN_SUSPENSO  NUMBER);


ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (PORC_ACTIV_ECONOMICAS  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (IMPORT_ACTIV_ECONOMICAS  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (IMPORT_RENTAS_EXTRAN  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (TIPO_MONEDA_ID  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (NOTA_ESPECIFICA  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (PROCEDENCIA_NOTA_ID  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (MEDIA_SIN_SUSPENSOS  NUMBER);


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS (ID,
                                                         NOMBRE,
                                                         TIPO,
                                                         OFICIAL,
                                                         NUMERO_CURSOS,
                                                         CREDITOS_TOTALES,
                                                         CREDITOS_POR_CURSO,
                                                         SIN_DOCENCIA,
                                                         EXTINGUIDA,
                                                         PRESENCIAL,
                                                         TITULACION_TECNICA,
                                                         RAMA,
                                                         CICLOS,
                                                         CODIGO_CENTRO,
                                                         TIPO_TITULO,
                                                         DOBLE_TITULACION
                                                        ) AS
  SELECT id, nombre, tipo, oficial, numero_cursos, creditos_totales,
          DECODE (numero_cursos, NULL, ROUND (creditos_totales / numero_cursos, 0)) creditos_por_curso, sin_docencia,
          extinguida, presencial, titulacion_tecnica, rama, ciclos, codigo_centro, tipo_titulo, doble_titulacion
   FROM   (SELECT t.id, t.nombre, tipo_estudio tipo,
                  DECODE (tipo_estudio,
                          'G', 1,
                          '12C', DECODE (t.id, 51001, 0, 51002, 0, 51003, 0, 100, 0, 101, 0, 103, 0, 104, 0, 1)
                         ) oficial,
                  (SELECT COUNT (*)
                   FROM   gra_pod.pod_cursos
                   WHERE  tit_id = t.id
                   AND    id <> 7) numero_cursos,
                  crd_tr + crd_ob + crd_op + NVL (crd_pfc, 0) + DECODE (tipo_estudio, 'G', crd_le, 0)
                  - DECODE (t.id, 11, 32, 0) creditos_totales,
                  DECODE (SIGN (NVL (curso_sin_docencia, 2013) - 2012), 1, 0, 1) sin_docencia,
                  DECODE (activa, 'S', 0, 1) extinguida, DECODE (activa, 'S', 1, 0) presencial,
                  DECODE (GRA_BEC.PACK_BEC.ES_TECNICA (t.id), 'S', 1, 0) titulacion_tecnica, NVL (t.rama, 'SO') rama,
                  t.tipo ciclos, u.cod_mec codigo_centro,
                  DECODE (t.tipo_titulo,
                          '0H', '02',
                          '01', '01',
                          '02', '04',
                          '03', '04',
                          '04', '03',
                          '06', '05',
                          NULL
                         ) tipo_titulo, 0 doble_titulacion
           FROM   gra_pod.pod_titulaciones t,
                  gri_est.est_ubic_estructurales u,
                  (SELECT 34 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 32 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 4 tit_id, 2011 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 2 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 6 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 23 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 21 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 27 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 28 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 24 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 11 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 12 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 14 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 13 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 16 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 15 tit_id, 2012 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 10 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 1 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 29 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 35 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 7 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 17 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 30 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 25 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 33 tit_id, 2013 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 9 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 26 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL
                   UNION ALL
                   SELECT 31 tit_id, 2014 curso_sin_docencia
                   FROM   DUAL) SIN
           WHERE  DECODE (t.id, 17, 2, 230, 2, t.uest_id) = u.id
           AND    t.id = SIN.tit_id(+)
           UNION ALL
           SELECT m.id, m.nombre, 'M' tipo, DECODE (oficial, 'S', 1, 0),
                  DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2) numero_cursos, creditos creditos_totales,
                  DECODE (activo, 'S', 0, 1) sin_docencia, DECODE (activo, 'S', 0, 1) extinguida,
                  DECODE (presencial, 'P', 1, 0) presencial, 0 titulacion_tecnica, NVL (r.cod, 'SO') rama, 1 ciclos,
                  u.cod_mec codigo_centro, '06' tipo_titulo, 0 doble_titulacion
           FROM   gra_pop.pop_masters m,
                  pod_ramas r,
                  gri_est.est_ubic_estructurales u
           WHERE  m.rama_id = r.id(+)
           and    u.id = m.uest_id
           UNION ALL
           SELECT id, nombre, 'DOC' tipo, 1 oficial, 1 numero_cursos, 0 creditos_totales,
                  DECODE (fin_docencia, NULL, 0, 1) sin_docencia, DECODE (fin_docencia, NULL, 0, 1) extinguida,
                  1 presencial, 0 titulacion_tecnica, NULL rama, 1 ciclos, NULL codigo_centro, '07' tipo_titulo, 0 doble_titulacion
           FROM   gra_doc.doc_programas);


		   CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_VW_EXT_PERSONAS_ESTUDIOS (ID,
                                                                     PERSONA_ID,
                                                                     ESTUDIO_ID,
                                                                     CURSO_ACADEMICO_ID,
                                                                     CREDITOS_MATRICULADOS,
                                                                     CREDITOS_SUPERADOS,
                                                                     CREDITOS_FALTANTES,
                                                                     CREDITOS_PENDIENTES_CONVA,
                                                                     CREDITOS_FUERA_BECA,
                                                                     CREDITOS_PRESENTADOS,
                                                                     LIMITAR_CREDITOS,
                                                                     LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                     MATRICULA_PARCIAL,
                                                                     NUMERO_BECAS_MEC,
                                                                     NUMERO_BECAS_CONS,
                                                                     MINUSVALIA,
                                                                     NUMERO_SEMESTRES,
                                                                     PROGRAMA_INTERCAMBIO,
                                                                     CREDITOS_CONVALIDADOS,
                                                                     CREDITOS_PARA_BECA,
                                                                     CURSO,
                                                                     NOTA_ACCESO,
                                                                     NOTA_MEDIA,
                                                                     FAMILIA_NUMEROSA_ID,
                                                                     BECARIO,
                                                                     TIPO_MATRICULA,
                                                                     ESTUDIO_ACCESO_ID,
                                                                     SIN_DOCENCIA,
                                                                     POSEE_TITULO,
                                                                     CREDITOS_COMPLEMENTOS,
                                                                     NOTA_ESPECIFICA,
                                                                     PROCEDENCIA_NOTA_ID, 
                                                                     MEDIA_SIN_SUSPENSOS
                                                                    ) AS
   select *
   from   (select rownum id, pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
                  pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_exp,
                  
                  -- todos ,
                  GRA_POP.CRD_sup_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_superados_bec,
                  
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  gra_pop.crd_faltan_ca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_faltan_ca,
                  gra_pop.CRD_PTE_CONVALIDACION (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_pendiente_conva,
                  (pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
                   - GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
                  ),                                                                                   -- crd_fuera_becs
                  gra_pop.CRD_PRESENTADOS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) crd_PRESENTADOS,
                  0 Limitar_creditos, '0' limitar_creditos_fin_estudios, '0' matricula_parcial,
                  
                  -- nos deberia dar igual pues es la de matricula
                  gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'MIN', m.pexp_pmas_id) numero_becas_mec,
                  
                  -- modificar para master,
                  gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'CON', m.pexp_pmas_id) numero_becas_cons,
                  decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,                           -- modificar para master,
                  decode (gra_bec.pack_bec_2011.crd_mismo_semestre (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca, 'G'),
                          'N', 2,
                          1
                         ) numero_semestres,
                  decode (gra_bec.pack_bec_2011.erasmus (m.pexp_per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
                  gra_pop.crd_convalidados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_convalidados,
                  GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_bec,
                  
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  gra_pop.curso_mat_master (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) curso,
                  gra_pop.nota_acceso (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) nota_acceso,
                  pack_pop.media_exp_beca (m.pexp_per_id, m.pexp_pmas_id, 'S') nota_media,
                  
                  --decode(gra_bec.pack_bec.media_beca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca),'N',0,1) media_beca, -- se podria calcular en la logica de negocio????
                  decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
                  decode (pack_pop.es_becario (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca), 'S', 1, 0) es_becario,
                  tm.nombre tipo_matricula,
                  GRA_POP.ESTUDIO_ACCESO (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca) estudio_acceso_id, 0 sin_docencia,
                  GRA_TIT.POSEE_TITULO (m.pexp_per_id, m.pexp_pmas_id) posee_titulo,
                  gra_pop.crd_complementos_pop (m.pexp_per_id, m.pexp_pmas_id) crd_complementos, 0 nota_especifica, '  ' procedencia_nota_id, 0 media_sin_suspensos
           from   pop_matriculas m,
                  pop_tipos_matricula tm,
                  pop_est_matriculas em
           where  m.curso_aca = tm.curso_aca
           and    m.ptm_id = tm.id
           and    m.pexp_per_id = em.pmat_pexp_per_id(+)
           and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
           and    m.curso_aca = em.pmat_curso_aca(+)
           union all
           select rownum id, exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
                  pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id) creditos_matriculados_exp,      -- todos ,
                  
                                 --   pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) creditos_matriculados_bec,
                  -- menos adapataso comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  pack_bec.crd_sup (e.per_id, e.tit_id, m.curso_aca) creditos_superados,
                  
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion no se estan calculando),
                  pack_exp.crd_faltantes_ca (e.per_id, e.tit_id, m.curso_aca) creditos_faltantes,
                  gra_exp.crd_pte_convalidacion_exp (e.per_id, e.tit_id, m.curso_aca) creditos_pendientes_conva,
                  (pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id)
                   - pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca)
                  ) creditos_fuera_beca,
                  pack_exp.crd_presentados (m.curso_aca, e.per_id, e.tit_id) creditos_presentados,
                  gra_exp.limitar_creditos (e.per_id, e.tit_id, m.curso_aca) limitar_creditos,
                  '0' limitar_creditos_fin_estudios, '0' matricual_parcial,
                  
                  -- nos deberia dar igual pues es la de matricula,
                  gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'MIN', e.tit_id) numero_becas_min,
                  gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'CON', e.tit_id) numero_becas_cons,
                  decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,
                  decode (gra_bec.pack_bec_2011.crd_mismo_semestre (e.per_id, e.tit_id, m.curso_aca, 'G'),
                          'N', 2,
                          1
                         ) numero_semestres,
                  decode (gra_bec.pack_bec_2011.erasmus (e.per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
                  gra_exp.crd_convalidados_exp (e.per_id, e.tit_id, m.curso_aca) creditos_convalidados,
                  pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) CREDITOS_PARA_BECA,
                  gra_bec.pack_bec.curso_mat (e.per_id, e.tit_id, m.curso_aca) curso,
                  to_number (replace (e.nau, '.', ',')) nota_acceso,
                  gra_exp.media_exp_beca (m.exp_per_id, m.exp_tit_id, m.curso_aca) nota_media,
                  decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
                  
                  --decode(gra_bec.pack_bec.media_beca (e.per_id, e.tit_id, m.curso_aca),'S',1,0) media_beca, -- se podria calcular en la logica de negocio????
                  decode (gra_exp.es_becario (m.exp_per_id, m.exp_tit_id, m.curso_aca), 'S', 1, 0),
                  tm.nombre tipo_matricula, '' estudio_acceso_id, 0 sin_docencia, 0 posee_titulo,
                  gra_exp.crd_complementos (e.per_id, e.tit_id) crd_complementos, 0 nota_especifica, '  ' procedencia_nota_id, 0 media_sin_suspensos
           from   exp_expedientes e,
                  exp_matriculas m,
                  exp_tipos_matricula tm,
                  exp_est_matriculas em
           where  e.per_id = m.exp_per_id
           and    e.tit_id = m.exp_tit_id
           and    m.tma_id = tm.id
           and    m.curso_aca = tm.curso_aca
           and    m.exp_per_id = em.mat_exp_per_id(+)
           and    m.exp_tit_id = em.mat_exp_tit_id(+)
           and    m.curso_aca = em.mat_curso_aca(+));
 
 CREATE TABLE uji_becas.bc2_tipos_moneda 
    ( 
     id NUMBER  NOT NULL , 
     codigo NUMBER , 
     nombre VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_moneda 
    ADD CONSTRAINT bc2_tipos_moneda_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_actividades_economicas 
    ( 
     id NUMBER  NOT NULL , 
     miembro_id NUMBER  NOT NULL , 
     num_sociedad NUMBER , 
     cif_sociedad VARCHAR2(10) , 
     porc_participacion NUMBER , 
     imp_participacion NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_actividades_eco_IDX ON uji_becas.bc2_actividades_economicas 
    ( 
     miembro_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_actividades_economicas_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_aeco_miembros_FK FOREIGN KEY 
    ( 
     miembro_id
    ) 
    REFERENCES uji_becas.bc2_miembros 
    ( 
     id
    ) 
;




CREATE INDEX uji_becas.bc2_miembros_mon_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_moneda_id ASC 
    ) 
;
ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_mon_FK FOREIGN KEY 
    ( 
     tipo_moneda_id
    ) 
    REFERENCES uji_becas.bc2_tipos_moneda 
    ( 
     id
    ) 
;


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
MODIFY(NOTA_ESPECIFICA  DEFAULT 0);



ALTER TABLE UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE
 ADD (SoliIbanCodPais  VARCHAR2(2 CHAR));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE
 ADD (SoliIbanDc  VARCHAR2(2 CHAR));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE
 ADD (SoliIndPropUtConcesion  VARCHAR2(1 CHAR));


ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiCodGrMinus  VARCHAR2(1));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiIndPartActivEcon  VARCHAR2(1));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiPorcPartActivEcon  NUMBER(5,2));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiImpPartActivEcon  NUMBER(13,2));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiIndRentasExtranj  VARCHAR2(1));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiRentasExtranj  NUMBER(13,2));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE
 ADD (FamiMoneda  VARCHAR2(2));


CREATE TABLE UJI_BECAS_WS_MULTIENVIO.ActivEconomicasType
(
  HJID                   NUMBER(19)             NOT NULL,
  AcecNumSociedad        VARCHAR2(1),
  AcecCIFActivEcon       VARCHAR2(1),
  AcecPorcPartActivEcon  NUMBER(5,2),
  AcecImpPartActivEcon   NUMBER(15,2),
  ActivEconomicas_FAMILIARTYPE_0 NUMBER(19)
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ActivEconomicasType ADD (
  PRIMARY KEY
 (HJID));



ALTER TABLE ActivEconomicasType ADD (
  CONSTRAINT FKActivEconomicas_familiar
 FOREIGN KEY (ActivEconomicas_FAMILIARTYPE_0)
 REFERENCES FAMILIARTYPE (HJID));


 GRANT DELETE, INSERT, SELECT, UPDATE ON ActivEconomicasType TO UJI_BECAS;




ALTER TABLE UJI_BECAS_WS_MULTIENVIO.GENERALESTYPE
 ADD (GeneIndDobleTitulacion  VARCHAR2(1 CHAR));
ALTER TABLE UJI_BECAS_WS_MULTIENVIO.GENERALESTYPE
 ADD (GenePorcCredSuperados  NUMBER(5,2));


DROP TABLE TITULACIONESSSCC CASCADE CONSTRAINTS;

CREATE TABLE TITULACIONESSSCC
(
  HJID                            NUMBER(19)    NOT NULL,
  TISCSECTITULO                   VARCHAR2(1 CHAR),
  TISCCODNIVELTIPO                VARCHAR2(2 CHAR),
  TISCDESNIVELTIPO                VARCHAR2(50 CHAR),
  TISCCODTITULO                   VARCHAR2(11 CHAR),
  TISCDESTITULO                   VARCHAR2(1015 CHAR),
  TISCDESTITULOLARGA              VARCHAR2(1015 CHAR),
  TITULACIONESSSCC_SOLICITUDTYPE  NUMBER(19)
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;

ALTER TABLE TITULACIONESSSCC ADD (
  PRIMARY KEY
 (HJID)
    USING INDEX
    TABLESPACE DATOS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

 GRANT DELETE, INSERT, SELECT, UPDATE ON TITULACIONESSSCC TO UJI_BECAS;


ALTER TABLE UJI_BECAS_WS_MULTIENVIO.TITULACIONTYPE
 ADD (TituIndTituloGestor  VARCHAR2(1 CHAR));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE
 ADD (ulcuIndCursoSelectivo  VARCHAR2(1));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE
 ADD (ulcuNotaMedSinSuspensosAnt  NUMBER(4,2));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE
 ADD (ulcuNotaPruebaEspecifica  VARCHAR2(2 CHAR));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE
 ADD (ulcuProcedenciaNota  VARCHAR2(2 CHAR));

ALTER TABLE UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE
 ADD (UlcuTantoPorHoras  NUMBER(5,2));


