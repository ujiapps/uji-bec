-- Generado por Oracle SQL Developer Data Modeler 3.3.0.744
--   en:        2014-05-13 14:00:57 CEST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP VIEW bc2_v_becas_estudios 
;
DROP VIEW uji_becas.bc2_v_becas_recuento 
;
DROP VIEW uji_becas.bc2_v_tandas_convocatoria 
;
DROP TABLE uji_becas.bc2_actividades_economicas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_cuantias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_denegaciones CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_historico CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_recursos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ccaa CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_convocatorias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cuantias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cuantias_por_curso CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cursos_academicos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_declarante CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_denegaciones CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_diccionario CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_dom_arrendatarios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_domicilios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_economicos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_envios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_estados CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_estados_civiles CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_matriculas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_paises CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_personas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_personas_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_provincias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_listados CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_localidades CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_log_notificaciones CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_lotes_ministerio CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_miembros CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_minimos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_organismos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_procesos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_profesiones CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_registro_envios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_registro_errores_envio CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_situaciones_laborales CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_solicitantes CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tandas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_textos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_domicilios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_familias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_identificacion CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_matricula CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_miembros CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_minusvalia CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_moneda CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_propiedad CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_residencias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_sexo CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_sustentador CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_vias CASCADE CONSTRAINTS 
;
CREATE TABLE uji_becas.bc2_actividades_economicas 
    ( 
     id NUMBER  NOT NULL , 
     miembro_id NUMBER  NOT NULL , 
     num_sociedad NUMBER , 
     cif_sociedad VARCHAR2 (10) , 
     porc_participacion NUMBER , 
     imp_participacion NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_actividades_eco_IDX ON uji_becas.bc2_actividades_economicas 
    ( 
     miembro_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_actividades_economicas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_becas 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     estado_id NUMBER  NOT NULL , 
     tanda_id NUMBER , 
     tipo_identificacion_id NUMBER  NOT NULL , 
     identificacion VARCHAR2 , 
     codigo_beca VARCHAR2 , 
     codigo_archivo_temporal VARCHAR2 , 
     tiene_titulo_universitario NUMBER DEFAULT 0  NOT NULL , 
     titulo_nombre VARCHAR2 , 
     codigo_estudio_ant VARCHAR2 , 
     estudio_ant VARCHAR2 , 
     estudio_ant_otros VARCHAR2 , 
     curso_academico_ant NUMBER , 
     causa_otros VARCHAR2 , 
     beca_curso_ant NUMBER DEFAULT 0  NOT NULL , 
     beca_curso_ant_2 NUMBER DEFAULT 0  NOT NULL , 
     beca_curso_ant_3 NUMBER DEFAULT 0  NOT NULL , 
     creditos_estudio_ant NUMBER , 
     creditos_matriculados_ant NUMBER , 
     creditos_superados_ant NUMBER , 
     creditos_suspensos_ant NUMBER , 
     creditos_convalidados_ant NUMBER , 
     matricula_parcial_ant NUMBER DEFAULT 0  NOT NULL , 
     nota_media_ant NUMBER , 
     becas_concedidas_ant NUMBER , 
     tercio_presentados_ant NUMBER DEFAULT 0  NOT NULL , 
     estudio_id NUMBER , 
     tipo_matricula_id NUMBER , 
     codigo_centro_ant VARCHAR2 , 
     creditos_matriculados NUMBER , 
     creditos_convalidados NUMBER , 
     creditos_pend_conva NUMBER , 
     creditos_para_beca NUMBER , 
     creditos_fuera_beca NUMBER , 
     numero_semestres NUMBER , 
     curso NUMBER , 
     matricula_parcial NUMBER DEFAULT 0  NOT NULL , 
     estudios_sin_docencia NUMBER DEFAULT 0  NOT NULL , 
     familia_especialidad VARCHAR2 , 
     presencial NUMBER DEFAULT 1  NOT NULL , 
     limitar_creditos NUMBER DEFAULT 0  NOT NULL , 
     limitar_creditos_fin_estudios NUMBER DEFAULT 0  NOT NULL , 
     beca_parcial NUMBER DEFAULT 0  NOT NULL , 
     distancia_al_centro NUMBER , 
     transporte_urbano NUMBER DEFAULT 0  NOT NULL , 
     numero_transportes NUMBER , 
     transporte_barco_avion NUMBER DEFAULT 0  NOT NULL , 
     observaciones VARCHAR2 , 
     observaciones_uji VARCHAR2 , 
     autorizacion_renta NUMBER DEFAULT 0  NOT NULL , 
     entidad VARCHAR2 , 
     sucursal VARCHAR2 , 
     digitos_control VARCHAR2 , 
     cuenta_bancaria VARCHAR2 , 
     programa_intercambio NUMBER DEFAULT 0  NOT NULL , 
     cumple_academicos NUMBER DEFAULT 0 , 
     rendimiento_academico NUMBER DEFAULT 0  NOT NULL , 
     posee_titulo NUMBER DEFAULT 0 , 
     fecha_notificacion DATE , 
     alguna_beca_parcial NUMBER DEFAULT 0  NOT NULL , 
     sms NUMBER DEFAULT 0 , 
     titulo_espanol NUMBER DEFAULT 0 , 
     estudios_homologados NUMBER DEFAULT 0 , 
     univ_titulacion VARCHAR2 , 
     ind_coef_corrector NUMBER DEFAULT 0 , 
     nota_corregida NUMBER , 
     cod_titu_acceso VARCHAR2 , 
     plan_anterior_num_cursos NUMBER , 
     tipo_titulacion VARCHAR2 (10) , 
     nota_media_ultimo_curso NUMBER , 
     IBanPais VARCHAR2 (10) DEFAULT 'ES' , 
     IBanDc VARCHAR2 (10) , 
     nota_prueba_especifica NUMBER , 
     procedencia_nota VARCHAR2 (10) , 
     nota_media_sin_suspenso NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_sol_IDX ON uji_becas.bc2_becas 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_conv_IDX ON uji_becas.bc2_becas 
    ( 
     convocatoria_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_prov_IDX ON uji_becas.bc2_becas 
    ( 
     proceso_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_est_IDX ON uji_becas.bc2_becas 
    ( 
     estado_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_tan_IDX ON uji_becas.bc2_becas 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_dni_IDX ON uji_becas.bc2_becas 
    ( 
     tipo_identificacion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_est_IDXv1 ON uji_becas.bc2_becas 
    ( 
     estudio_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_mat_IDX ON uji_becas.bc2_becas 
    ( 
     tipo_matricula_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_becas__UN UNIQUE ( solicitante_id , convocatoria_id , estudio_id ) ;



CREATE TABLE uji_becas.bc2_becas_cuantias 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     cuantia_id NUMBER  NOT NULL , 
     importe NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_det_cuan_cuan_FK ON uji_becas.bc2_becas_cuantias 
    ( 
     cuantia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_cuantias__IDX ON uji_becas.bc2_becas_cuantias 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_becas_cuantias__UN UNIQUE ( beca_id , cuantia_id ) ;



CREATE TABLE uji_becas.bc2_becas_denegaciones 
    ( 
     id NUMBER  NOT NULL , 
     denegacion_id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     orden_denegacion NUMBER 
    ) 
;



ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_denegaciones_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_denegaciones__UN UNIQUE ( denegacion_id , beca_id , orden_denegacion ) ;



CREATE TABLE uji_becas.bc2_becas_historico 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     estado_id NUMBER  NOT NULL , 
     tanda_id NUMBER , 
     fecha DATE , 
     usuario VARCHAR2 , 
     convocatoria_id NUMBER , 
     fecha_notificacion DATE , 
     concedida NUMBER DEFAULT 0 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_historico__IDX ON uji_becas.bc2_becas_historico 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_historico 
    ADD CONSTRAINT bc2_detalle_historico_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_becas_recursos 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     causas_desestimacion VARCHAR2 (4000) , 
     texto_ayudas_reclamadas VARCHAR2 (4000) , 
     curso_ant_estudio VARCHAR2 (4000) , 
     curso_ant_completo NUMBER , 
     curso_ant_creditos_matr NUMBER , 
     curso_ant_creditos NUMBER , 
     curso_ant_nota_media NUMBER , 
     curso_ant_creditos_pend NUMBER , 
     curso_ant_creditos_pend_max NUMBER , 
     curso_act_curso_posterior NUMBER , 
     curso_act_curso_posterior_txt VARCHAR2 (4000) , 
     curso_act_curso_completo NUMBER , 
     curso_act_creditos_matr NUMBER , 
     curso_act_creditos NUMBER , 
     curso_act_anios_becario NUMBER , 
     curso_act_perdida_curso NUMBER , 
     curso_Act_perdida_curso_txt VARCHAR2 (4000) , 
     ingresos_familiares NUMBER , 
     deducciones NUMBER , 
     renta_familiar_disponible NUMBER , 
     renta_familiar_per_capita NUMBER , 
     porcentaje_incremento_umbral NUMBER , 
     supera_umbrales NUMBER , 
     supera_umbrales_texto VARCHAR2 (4000) , 
     supera_umbrales_cuantia NUMBER , 
     distancia VARCHAR2 (4000) , 
     existe_centro_mas_cercano NUMBER , 
     circunstancias_transporte VARCHAR2 (4000) , 
     otras_becas NUMBER , 
     otras_circunstancias VARCHAR2 (4000) , 
     texto_propuesta VARCHAR2 (4000) , 
     texto_justificacion VARCHAR2 (4000) , 
     miembros_computables NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_recursos__IDX ON uji_becas.bc2_becas_recursos 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_recursos 
    ADD CONSTRAINT bc2_becas_reclamaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ccaa 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     codigo VARCHAR2 (10)  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_ccaa 
    ADD CONSTRAINT bc2_ccaa_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_convocatorias 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL , 
     acronimo VARCHAR2 , 
     nombre VARCHAR2 
    ) 
;


CREATE INDEX uji_becas.bc2_convocatorias_org_IDX ON uji_becas.bc2_convocatorias 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_convocatorias 
    ADD CONSTRAINT bc2_convocatorias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_cuantias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 , 
     convocatoria_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL , 
     codigo VARCHAR2  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_cuantias 
    ADD CONSTRAINT bc2_cuantias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_cuantias_por_curso 
    ( 
     id NUMBER  NOT NULL , 
     cuantia_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     importe NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_cuantias_por_curso_cu_IDX ON uji_becas.bc2_cuantias_por_curso 
    ( 
     cuantia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_cuantias_por_curso_ca_IDX ON uji_becas.bc2_cuantias_por_curso 
    ( 
     curso_academico_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_curso_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_cursos_academicos 
    ( 
     id NUMBER  NOT NULL , 
     activo NUMBER DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_cursos_academicos 
    ADD CONSTRAINT bc2_cursos_academicos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_declarante 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     declarante VARCHAR2 (100) , 
     indicador_rentas_extranjero NUMBER , 
     rentas_extranjero NUMBER , 
     localidad_declarante VARCHAR2 (100) , 
     fecha_declarante DATE , 
     indicador_ult_curso NUMBER , 
     indicador_curso_pendiente NUMBER , 
     indicador_curso_anterior NUMBER , 
     indicador_independiente NUMBER , 
     indicador_inem NUMBER , 
     empleador VARCHAR2 (100) , 
     tipo_identificacion_id NUMBER , 
     identificacion VARCHAR2 (100) , 
     ingresos_anuales NUMBER , 
     codigo_parentesco VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_becas.bc2_declarante__IDX ON uji_becas.bc2_declarante 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_declarante_tip_id_IDX ON uji_becas.bc2_declarante 
    ( 
     tipo_identificacion_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante__UN UNIQUE ( solicitante_id ) ;



CREATE TABLE uji_becas.bc2_denegaciones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL , 
     codigo VARCHAR2  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_denegaciones 
    ADD CONSTRAINT bc2_denegaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_diccionario 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     clase_uji VARCHAR2  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     valor_origen VARCHAR2  NOT NULL , 
     valor_uji NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_diccionario_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_dom_arrendatarios 
    ( 
     id NUMBER  NOT NULL , 
     domicilio_id NUMBER  NOT NULL , 
     nombre VARCHAR2 , 
     identificacion VARCHAR2 , 
     tipo_identificacion_id NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_domicilios_arr_dom_IDX ON uji_becas.bc2_dom_arrendatarios 
    ( 
     tipo_identificacion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_arr_ident_IDX ON uji_becas.bc2_dom_arrendatarios 
    ( 
     domicilio_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_dom_arrendatarios 
    ADD CONSTRAINT bc2_dom_arrendatarios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_domicilios 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     tipo_domicilio_id NUMBER , 
     tipo_via_id NUMBER , 
     nombre_via VARCHAR2 , 
     numero VARCHAR2 , 
     escalera VARCHAR2 , 
     piso VARCHAR2 , 
     puerta VARCHAR2 , 
     provincia_id VARCHAR2 , 
     localidad_id NUMBER , 
     localidad_nombre VARCHAR2 , 
     codigo_municipio VARCHAR2 , 
     codigo_postal VARCHAR2 , 
     residencia_esp NUMBER DEFAULT 0  NOT NULL , 
     otra_clase_domicilio VARCHAR2 , 
     gratuito NUMBER DEFAULT 0  NOT NULL , 
     correspondencia NUMBER DEFAULT 0  NOT NULL , 
     propiedad_de_id NUMBER , 
     importe_alquiler NUMBER , 
     tipo_residencia_id NUMBER , 
     arrendador_nombre VARCHAR2 , 
     arrendador_tipo_ident_id NUMBER , 
     arrendador_identificacion VARCHAR2 , 
     numero_arrendatarios NUMBER , 
     importe_fianza NUMBER , 
     fecha_incio_contrato DATE , 
     fecha_fin_contrato DATE 
    ) 
;


CREATE INDEX uji_becas.bc2_domicilios_sol_IDX ON uji_becas.bc2_domicilios 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_dom_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_domicilio_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_via_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_via_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_prov_IDX ON uji_becas.bc2_domicilios 
    ( 
     provincia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_loc_IDX ON uji_becas.bc2_domicilios 
    ( 
     localidad_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_prop_IDX ON uji_becas.bc2_domicilios 
    ( 
     propiedad_de_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_res_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_residencia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_ident_IDX ON uji_becas.bc2_domicilios 
    ( 
     arrendador_tipo_ident_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_economicos 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     coeficiente_prelacion NUMBER , 
     ind_becario NUMBER , 
     umbral_becario NUMBER , 
     ind_compensa NUMBER , 
     umbral_compensa NUMBER , 
     ind_tasas NUMBER , 
     umbral_tasas NUMBER , 
     ind_mov_general NUMBER , 
     umbral_mov_general NUMBER , 
     ind_mov_especial NUMBER , 
     umbral_mov_especial NUMBER , 
     ind_resid_mov NUMBER , 
     umbral_resid_mov NUMBER , 
     ind_patrimonio NUMBER , 
     umbral_patrimonio NUMBER , 
     renta NUMBER , 
     capital_mobiliario NUMBER , 
     volumen_negocio NUMBER , 
     cod_repesca VARCHAR2 (10) , 
     deducciones NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_economicos__IDX ON uji_becas.bc2_economicos 
    ( 
     solicitante_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos__UN UNIQUE ( solicitante_id ) ;



CREATE TABLE uji_becas.bc2_envios 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     numero_envio NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_org_curso_UN UNIQUE ( organismo_id , curso_academico_id ) ;



CREATE TABLE uji_becas.bc2_estados 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_estados 
    ADD CONSTRAINT bc2_estados_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_estados_civiles 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_estados_civiles 
    ADD CONSTRAINT bc2_estados_civiles_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ext_estudios 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     tipo VARCHAR2  NOT NULL , 
     oficial VARCHAR2  NOT NULL , 
     numero_cursos NUMBER , 
     creditos_totales NUMBER , 
     creditos_por_curso NUMBER , 
     sin_docencia NUMBER DEFAULT 0  NOT NULL , 
     extinguida NUMBER DEFAULT 0  NOT NULL , 
     presencial NUMBER DEFAULT 0  NOT NULL , 
     titulacion_tecnica NUMBER DEFAULT 0  NOT NULL , 
     rama VARCHAR2 (2 BYTE) , 
     ciclos NUMBER , 
     doble_titulacion NUMBER DEFAULT 0 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_estudios 
    ADD CONSTRAINT bc2_ext_estudios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ext_matriculas 
    ( 
     persona_id NUMBER , 
     estudio_id NUMBER , 
     curso_aca NUMBER , 
     c_archivo_temporal VARCHAR2 , 
     becario VARCHAR2 , 
     matriculas NUMBER 
    ) 
;




CREATE TABLE uji_becas.bc2_ext_paises 
    ( 
     id VARCHAR2  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     nacionalidad VARCHAR2 , 
     codigo_iso VARCHAR2 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_paises 
    ADD CONSTRAINT bc2_ext_paises_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ext_personas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     identificacion VARCHAR2  NOT NULL , 
     persona_nombre VARCHAR2 (100) , 
     persona_apellido1 VARCHAR2 (100) , 
     persona_apellido2 VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_personas 
    ADD CONSTRAINT bc2_ext_personas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ext_personas_estudios 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     creditos_matriculados NUMBER , 
     creditos_superados NUMBER , 
     creditos_faltantes NUMBER , 
     creditos_pendientes_conva NUMBER , 
     creditos_fuera_beca NUMBER , 
     creditos_presentados NUMBER , 
     limitar_creditos NUMBER DEFAULT 0 , 
     limitar_creditos_fin_estudios NUMBER DEFAULT 0 , 
     matricula_parcial NUMBER DEFAULT 0 , 
     numero_becas_mec NUMBER , 
     numero_becas_cons NUMBER , 
     minusvalia NUMBER DEFAULT 0 , 
     numero_semestres NUMBER , 
     programa_intercambio NUMBER DEFAULT 0 , 
     creditos_convalidados NUMBER , 
     creditos_para_beca NUMBER , 
     curso NUMBER , 
     nota_acceso NUMBER , 
     nota_media NUMBER , 
     familia_numerosa_id NUMBER , 
     becario NUMBER DEFAULT 0  NOT NULL , 
     tipo_matricula VARCHAR2 , 
     estudio_acceso_id NUMBER , 
     sin_docencia NUMBER DEFAULT 0 , 
     posee_titulo NUMBER DEFAULT 0 , 
     alguna_beca_parcial NUMBER DEFAULT 0  NOT NULL , 
     tiene_beca_mec NUMBER , 
     creditos_complementos NUMBER , 
     nota_medila_ultimo_curso NUMBER , 
     tasas_total NUMBER , 
     tasas_ministerio NUMBER , 
     tasas_conselleria NUMBER , 
     creditos_cubiertos NUMBER , 
     nota_espedifica NUMBER DEFAULT 0 , 
     procedencia_nota_id VARCHAR2 (10) , 
     media_sin_suspensos NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_ext_per_est_1_idx ON uji_becas.bc2_ext_personas_estudios 
    ( 
     persona_id ASC , 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_ext_per_est_2_idx ON uji_becas.bc2_ext_personas_estudios 
    ( 
     persona_id ASC , 
     estudio_id ASC , 
     curso_academico_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_estudios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_ext_provincias 
    ( 
     id VARCHAR2  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER , 
     codigo VARCHAR2 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_provincias 
    ADD CONSTRAINT bc2_ext_provincias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_listados 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     url VARCHAR2 (4000) , 
     activo NUMBER DEFAULT 1  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_listados__IDX ON uji_becas.bc2_listados 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_localidades 
    ( 
     id NUMBER  NOT NULL , 
     provincia_id VARCHAR2  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     codigo VARCHAR2 , 
     codigo_ayuntamiento VARCHAR2 , 
     nombre_ayuntamiento VARCHAR2 , 
     baja NUMBER DEFAULT 0  NOT NULL , 
     distancia NUMBER 
    ) 
;



ALTER TABLE uji_becas.bc2_localidades 
    ADD CONSTRAINT bc2_localidades_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_log_notificaciones 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     texto VARCHAR2 
    ) 
;


CREATE INDEX uji_becas.LOG_BECA_I ON uji_becas.bc2_log_notificaciones 
    ( 
     beca_id ASC 
    ) 
;
CREATE INDEX uji_becas.LOG_TANDA_I ON uji_becas.bc2_log_notificaciones 
    ( 
     tanda_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_log_notificaciones 
    ADD CONSTRAINT bc2_log_notificaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_lotes_ministerio 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     lote_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER , 
     fecha DATE  NOT NULL , 
     multienvio_id NUMBER  NOT NULL , 
     estado VARCHAR2 (20)  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_lotes_ministerio 
    ADD CONSTRAINT bc2_lotes_ministerio_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_miembros 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     tipo_miembro_id NUMBER , 
     nombre VARCHAR2  NOT NULL , 
     apellido1 VARCHAR2  NOT NULL , 
     apellido2 VARCHAR2 , 
     tipo_identificacion_id NUMBER , 
     identificacion VARCHAR2 , 
     sexo_id NUMBER , 
     fecha_nacimiento DATE , 
     nacionalidad_id VARCHAR2  NOT NULL , 
     estado_civil_id NUMBER , 
     profesion_id NUMBER , 
     localidad_trabajo VARCHAR2 , 
     sustentador_id NUMBER  NOT NULL , 
     situacion_laboral_id NUMBER , 
     tipo_minusvalia_id NUMBER , 
     no_valida_identificacion NUMBER DEFAULT 0  NOT NULL , 
     padron_localidad_id NUMBER , 
     padron_provincia_id NUMBER , 
     firma_solicitud NUMBER DEFAULT 0  NOT NULL , 
     PORC_ACTIV_ECONOMICAS NUMBER , 
     IMPORT_ACTIV_ECOnomicas NUMBER , 
     import_rentas_extran NUMBER , 
     tipo_moneda_id NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_miembros_sol_IDX ON uji_becas.bc2_miembros 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sexo_IDX ON uji_becas.bc2_miembros 
    ( 
     sexo_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sus_IDX ON uji_becas.bc2_miembros 
    ( 
     sustentador_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_ec_IDX ON uji_becas.bc2_miembros 
    ( 
     estado_civil_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_tmiem_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_miembro_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_prof_IDX ON uji_becas.bc2_miembros 
    ( 
     profesion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_minus_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_minusvalia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sitlab_IDX ON uji_becas.bc2_miembros 
    ( 
     situacion_laboral_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_tdni_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_identificacion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_nacion_IDX ON uji_becas.bc2_miembros 
    ( 
     nacionalidad_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_ec_IDXv2 ON uji_becas.bc2_miembros 
    ( 
     estado_civil_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_mon_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_moneda_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_minimos 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     creditos_beca_parcial NUMBER  NOT NULL , 
     creditos_beca_completa NUMBER  NOT NULL , 
     tipo_estudio VARCHAR2  NOT NULL , 
     nota_acceso_mec NUMBER , 
     porc_superados_mec NUMBER , 
     coef_corrector_mec NUMBER , 
     nota_rendimiento_mec_1 NUMBER , 
     nota_rendimiento_mec_2 NUMBER , 
     nota_acceso_cons NUMBER , 
     porc_superados_cons NUMBER , 
     coef_corrector_cons NUMBER , 
     nota_segundos_cons NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_minimos_ca_IDX ON uji_becas.bc2_minimos 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_minimos_est_IDX ON uji_becas.bc2_minimos 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_organismos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     webservice VARCHAR2 
    ) 
;



ALTER TABLE uji_becas.bc2_organismos 
    ADD CONSTRAINT bc2_organismos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_procesos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_procesos 
    ADD CONSTRAINT bc2_procesos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_profesiones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_profesiones 
    ADD CONSTRAINT bc2_profesiones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_registro_envios 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     envio_id NUMBER , 
     tanda_id NUMBER  NOT NULL , 
     fecha DATE  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     estado NUMBER DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_CK 
    CHECK (estado in (0,1,-1))
;

CREATE INDEX uji_becas.bc2_registro_envios__IDXv2 ON uji_becas.bc2_registro_envios 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv3 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC , 
     convocatoria_id ASC , 
     envio_id ASC , 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv3v2 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv4 ON uji_becas.bc2_registro_envios 
    ( 
     convocatoria_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv5 ON uji_becas.bc2_registro_envios 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv6 ON uji_becas.bc2_registro_envios 
    ( 
     persona_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_registro_errores_envio 
    ( 
     id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     error VARCHAR2 
    ) 
;


CREATE INDEX uji_becas.bc2_registro_errores_IDX ON uji_becas.bc2_registro_errores_envio 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv1 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC , 
     beca_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv3 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv4 ON uji_becas.bc2_registro_errores_envio 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_registro_errores_envio_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_situaciones_laborales 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_situaciones_laborales 
    ADD CONSTRAINT bc2_situaciones_laborales_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_solicitantes 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     numero_beca_uji NUMBER , 
     profesion_sustentador NUMBER , 
     telefono1 VARCHAR2 , 
     telefono2 VARCHAR2 , 
     email VARCHAR2 , 
     tipo_familia_id NUMBER , 
     numero_miembros_computables NUMBER , 
     numero_hermanos NUMBER , 
     numero_hermanos_fuera NUMBER , 
     numero_minusvalia_33 NUMBER , 
     numero_minusvalia_65 NUMBER , 
     orfandad NUMBER (1) DEFAULT 0  NOT NULL , 
     unidad_familiar_independiente NUMBER DEFAULT 0  NOT NULL , 
     ingresos_netos NUMBER , 
     ingresos_extranjero NUMBER , 
     carnet_familia_numerosa VARCHAR2 (100) , 
     ccaa_familia_numerosa_id NUMBER , 
     fecha_fin_familia_numerosa DATE , 
     fecha_orfandad DATE , 
     violencia_genero NUMBER DEFAULT 0 
    ) 
;


CREATE INDEX uji_becas.bc2_solicitantes_ca_IDX ON uji_becas.bc2_solicitantes 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_solicitantes_tp_IDX ON uji_becas.bc2_solicitantes 
    ( 
     tipo_familia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_solicitantes_ccaa_IDX ON uji_becas.bc2_solicitantes 
    ( 
     ccaa_familia_numerosa_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_solicitudes_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_solicitantes__UN UNIQUE ( curso_academico_id , persona_id ) ;



CREATE TABLE uji_becas.bc2_tandas 
    ( 
     id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     descripcion VARCHAR2 , 
     fecha DATE , 
     activa NUMBER DEFAULT 1  NOT NULL , 
     fecha_notificacion DATE , 
     usuario VARCHAR2 
    ) 
;


CREATE INDEX uji_becas.bc2_tandas_curso_aca_IDX ON uji_becas.bc2_tandas 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_tandas_con_IDX ON uji_becas.bc2_tandas 
    ( 
     convocatoria_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas__UN UNIQUE ( curso_academico_id , convocatoria_id , tanda_id ) ;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_2_UN UNIQUE ( curso_academico_id , tanda_id ) ;



CREATE TABLE uji_becas.bc2_textos 
    ( 
     id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     nombre VARCHAR2 , 
     cabecera_ca VARCHAR2 , 
     cabecera_es VARCHAR2 , 
     pie_ca VARCHAR2 , 
     pie_es VARCHAR2 
    ) 
;


CREATE INDEX uji_becas.bc2_textos_proc_IDX ON uji_becas.bc2_textos 
    ( 
     proceso_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_textos_org_IDX ON uji_becas.bc2_textos 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_textos 
    ADD CONSTRAINT bc2_textos_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_textos 
    ADD CONSTRAINT bc2_textos__UN UNIQUE ( proceso_id , organismo_id ) ;



CREATE TABLE uji_becas.bc2_tipos_domicilios 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_domicilios 
    ADD CONSTRAINT bc2_tipos_domicilios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_familias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_familias 
    ADD CONSTRAINT bc2_tipos_familias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_identificacion 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_identificacion 
    ADD CONSTRAINT bc2_tipos_identificacion_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_matricula 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_matricula 
    ADD CONSTRAINT bc2_tipos_matricula_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_miembros 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL , 
     solicitante NUMBER DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_miembros 
    ADD CONSTRAINT bc2_tipos_miembros_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_minusvalia 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_minusvalia 
    ADD CONSTRAINT bc2_tipos_minusvalia_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_moneda 
    ( 
     id NUMBER  NOT NULL , 
     codigo NUMBER , 
     nombre VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_moneda 
    ADD CONSTRAINT bc2_tipos_moneda_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_propiedad 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_propiedad 
    ADD CONSTRAINT bc2_tipos_propiedad_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_residencias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_residencias 
    ADD CONSTRAINT bc2_tipos_residencias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_sexo 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_sexo 
    ADD CONSTRAINT bc2_tipos_sexo_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_sustentador 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_sustentador 
    ADD CONSTRAINT bc2_tipos_sustentador_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_tipos_vias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_vias 
    ADD CONSTRAINT bc2_tipos_vias_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_aeco_miembros_FK FOREIGN KEY 
    ( 
     miembro_id
    ) 
    REFERENCES uji_becas.bc2_miembros 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_deneg_becas_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_deneg_deneg_FK FOREIGN KEY 
    ( 
     denegacion_id
    ) 
    REFERENCES uji_becas.bc2_denegaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_recursos 
    ADD CONSTRAINT bc2_becas_recl_becas_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_convocatorias 
    ADD CONSTRAINT bc2_conv_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_cuantias 
    ADD CONSTRAINT bc2_cuantias_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_cuan_FK FOREIGN KEY 
    ( 
     cuantia_id
    ) 
    REFERENCES uji_becas.bc2_cuantias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_tip_ident_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_denegaciones 
    ADD CONSTRAINT bc2_denegaciones_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_cua_FK FOREIGN KEY 
    ( 
     cuantia_id
    ) 
    REFERENCES uji_becas.bc2_cuantias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_det_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_historico 
    ADD CONSTRAINT bc2_det_historico_det_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_estados_FK FOREIGN KEY 
    ( 
     estado_id
    ) 
    REFERENCES uji_becas.bc2_estados 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_estudios_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_procesos_FK FOREIGN KEY 
    ( 
     proceso_id
    ) 
    REFERENCES uji_becas.bc2_procesos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_solicitudes_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tandas_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tipos_id_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tipos_mat_FK FOREIGN KEY 
    ( 
     tipo_matricula_id
    ) 
    REFERENCES uji_becas.bc2_tipos_matricula 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_dic_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_diccionario 
    ADD CONSTRAINT bc2_dic_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_dom_arrendatarios 
    ADD CONSTRAINT bc2_domicilios_arr_dom_FK FOREIGN KEY 
    ( 
     domicilio_id
    ) 
    REFERENCES uji_becas.bc2_domicilios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_dom_arrendatarios 
    ADD CONSTRAINT bc2_domicilios_arr_t_iden_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_loc_FK FOREIGN KEY 
    ( 
     localidad_id
    ) 
    REFERENCES uji_becas.bc2_localidades 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_pro_FK FOREIGN KEY 
    ( 
     provincia_id
    ) 
    REFERENCES uji_becas.bc2_ext_provincias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tip_id_arr_FK FOREIGN KEY 
    ( 
     arrendador_tipo_ident_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipo_res_FK FOREIGN KEY 
    ( 
     tipo_residencia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_residencias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_dom_FK FOREIGN KEY 
    ( 
     tipo_domicilio_id
    ) 
    REFERENCES uji_becas.bc2_tipos_domicilios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_pro_FK FOREIGN KEY 
    ( 
     propiedad_de_id
    ) 
    REFERENCES uji_becas.bc2_tipos_propiedad 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_vias_FK FOREIGN KEY 
    ( 
     tipo_via_id
    ) 
    REFERENCES uji_becas.bc2_tipos_vias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_organismos_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_est_fam_FK FOREIGN KEY 
    ( 
     familia_numerosa_id
    ) 
    REFERENCES uji_becas.bc2_tipos_familias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_localidades 
    ADD CONSTRAINT bc2_localidades_pro_FK FOREIGN KEY 
    ( 
     provincia_id
    ) 
    REFERENCES uji_becas.bc2_ext_provincias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_bc2_ext_paises_FK FOREIGN KEY 
    ( 
     nacionalidad_id
    ) 
    REFERENCES uji_becas.bc2_ext_paises 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_est_civiles_FK FOREIGN KEY 
    ( 
     estado_civil_id
    ) 
    REFERENCES uji_becas.bc2_estados_civiles 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_profesiones_FK FOREIGN KEY 
    ( 
     profesion_id
    ) 
    REFERENCES uji_becas.bc2_profesiones 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_sit_lab_FK FOREIGN KEY 
    ( 
     situacion_laboral_id
    ) 
    REFERENCES uji_becas.bc2_situaciones_laborales 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipo_sexo_FK FOREIGN KEY 
    ( 
     sexo_id
    ) 
    REFERENCES uji_becas.bc2_tipos_sexo 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipo_sust_FK FOREIGN KEY 
    ( 
     sustentador_id
    ) 
    REFERENCES uji_becas.bc2_tipos_sustentador 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_id_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_mi_FK FOREIGN KEY 
    ( 
     tipo_miembro_id
    ) 
    REFERENCES uji_becas.bc2_tipos_miembros 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_min_FK FOREIGN KEY 
    ( 
     tipo_minusvalia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_minusvalia 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_mon_FK FOREIGN KEY 
    ( 
     tipo_moneda_id
    ) 
    REFERENCES uji_becas.bc2_tipos_moneda 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_ext_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_reg_err_env_bec_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_regi_err_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_ccaa_FK FOREIGN KEY 
    ( 
     ccaa_familia_numerosa_id
    ) 
    REFERENCES uji_becas.bc2_ccaa 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_tipo_fami_FK FOREIGN KEY 
    ( 
     tipo_familia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_familias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_conv_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_textos 
    ADD CONSTRAINT bc2_textos_organismos_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_textos 
    ADD CONSTRAINT bc2_textos_procesos_FK FOREIGN KEY 
    ( 
     proceso_id
    ) 
    REFERENCES uji_becas.bc2_procesos 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW bc2_v_becas_estudios  AS
SELECT solicitud.id * 100000 + becas.id AS id,
  estudio.id                            AS estudio_Id,
  estudio.NUMERO_CURSOS                 AS numero_Cursos,
  estudio.CREDITOS_TOTALES              AS creditos_Totales,
  estudio.presencial                    AS presencial,
  estudio.SIN_DOCENCIA                  AS sin_Docencia,
  minimos.creditos_beca_completa        AS creditos_beca_completa,
  minimos.CREDITOS_beca_parcial         AS creditos_beca_parcial,
  becas.TIPO_MATRICULA_ID               AS tipo_Matricula_Id,
  becas.id                              AS beca_Id,
  curso_aca.id                          AS curso_Academico_Id,
  curso_aca.activo                      AS curso_Academico_Activo,
  estudio.titulacion_tecnica            AS titulacion_tecnica
FROM BC2_EXT_ESTUDIOS estudio,
  BC2_BECAS becas,
  BC2_SOLICITANTES solicitud,
  BC2_MINIMOS minimos,
  BC2_CURSOS_ACADEMICOS curso_aca
WHERE estudio.id                 = becas.estudio_id
AND estudio.id                   = minimos.estudio_id
AND minimos.CURSO_ACADEMICO_ID   = curso_aca.id
AND solicitud.id                 = becas.solicitante_id
AND solicitud.curso_academico_id = curso_aca.id ;



CREATE OR REPLACE VIEW uji_becas.bc2_v_becas_recuento  AS
SELECT DISTINCT cur.id curso_academico_id,
  o.id organismo_id,
  o.nombre organismo,
  c.id convocatoria_id,
  c.nombre convocatoria,
  p.id proceso_id,
  p.nombre proceso,
  e.id estado_id,
  e.nombre estado,
  con.id beca_concedida,
  (SELECT COUNT(*)
  FROM bc2_solicitantes s,
    bc2_becas b
  WHERE s.id                = b.solicitante_id
  AND (s.curso_academico_id = cur.id
  AND b.convocatoria_id     = c.id
  AND b.proceso_id          = p.id
  AND b.estado_id           = e.id
  AND b.beca_concedida      = con.id2)
  ) cuantos
FROM bc2_organismos o,
  bc2_convocatorias c,
  bc2_estados e,
  bc2_procesos p,
  bc2_cursos_academicos cur,
  (SELECT 'S' id, 1 id2, 'Si' nombre FROM dual
  UNION ALL
  SELECT 'N' id, 0 id2, 'No' nombre FROM dual
  ) con
WHERE o.id   = c.organismo_id
AND (cur.id >= 2012) ;



CREATE OR REPLACE VIEW uji_becas.bc2_v_tandas_convocatoria  AS
SELECT bc2_organismos.id
  || lpad(bc2_convocatorias.id, 3, '0')
  || bc2_tandas.curso_academico_id
  || lpad(bc2_tandas.id, 4, 0) AS Id,
  bc2_organismos.id            AS organismo_id,
  bc2_organismos.nombre,
  bc2_convocatorias.id     AS convocatoria_id,
  bc2_convocatorias.activa AS convocatoria_activa,
  bc2_convocatorias.acronimo,
  bc2_tandas.curso_academico_id,
  bc2_tandas.tanda_id,
  bc2_tandas.descripcion AS nombre_tanda,
  bc2_tandas.activa      AS tanda_activa
FROM bc2_convocatorias
INNER JOIN bc2_tandas
ON bc2_convocatorias.id = bc2_tandas.convocatoria_id
INNER JOIN bc2_organismos
ON bc2_organismos.id = bc2_convocatorias.organismo_id ;





-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            53
-- CREATE INDEX                            65
-- ALTER TABLE                            133
-- CREATE VIEW                              3
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
