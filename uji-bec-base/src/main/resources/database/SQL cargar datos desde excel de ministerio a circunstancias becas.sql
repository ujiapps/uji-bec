/* Formatted on 24/10/2012 17:47 (Formatter Plus v4.8.8) */
declare
   cursor lista is
      select   *
      from     (select *
                from   (select b.curso_aca, b.per_id, archivo, exp_tit_id tit_id,
                               (select decode (count (*), 0, 'N', 'S')
                                from   exp_mat_circunstancias
                                where  mat_curso_Aca = b.curso_aca
                                and    mat_exp_per_id = b.per_id
                                and    mat_exp_tit_id = exp_tit_id
                                and    cir_id = 2) becario,
                               titulacion, t.nombre, (select count (*)
                                                      from   exp_matriculas
                                                      where  curso_aca = b.curso_aca
                                                      and    exp_per_id = b.per_id) mat_12c,
                               (select count (*)
                                from   pop_matriculas
                                where  curso_aca = b.curso_aca
                                and    pexp_per_id = b.per_id) mat_pop
                        from   gra_exp.exp_bec_soli_temp b,
                               exp_matriculas m,
                               pod_titulaciones t
                        where  b.per_id = m.exp_per_id
                        and    b.curso_Aca = m.curso_aca
                        and    m.exp_tit_id = t.id
                        --and m.exp_per_id= 254283
                        and    titulacion not like 'M%Universita%'
                        union all
                        select b.curso_aca, b.per_id, archivo, pexp_pmas_id,
                               (select decode (count (*), 0, 'N', 'S')
                                from   pop_mat_circunstancias
                                where  pmat_curso_Aca = b.curso_aca
                                and    pmat_pexp_per_id = b.per_id
                                and    pmat_pexp_pmas_id = pexp_pmas_id
                                and    cir_id = 2) becario,
                               titulacion, t.nombre, (select count (*)
                                                      from   exp_matriculas
                                                      where  curso_aca = b.curso_aca
                                                      and    exp_per_id = b.per_id) mat_12c,
                               (select count (*)
                                from   pop_matriculas
                                where  curso_aca = b.curso_aca
                                and    pexp_per_id = b.per_id) mat_pop
                        from   gra_exp.exp_bec_soli_temp b,
                               pop_matriculas m,
                               pop_masters t
                        where  b.per_id = m.pexp_per_id
                        and    b.curso_Aca = m.curso_aca
                        and    m.pexp_pmas_id = t.id
                        --and m.exp_per_id= 254283
                        and    titulacion not like 'M%Universita%')
                where  becario = 'N')
      where    mat_12c + mat_pop = 1
      order by titulacion;
begin
   for x in lista loop
      begin
         if    x.tit_id between 1 and 999
            or x.tit_id between 51000 and 52000 then
            insert into exp_mat_circunstancias
                        (mat_exp_per_id, mat_exp_tit_id, mat_curso_aca, cir_id, fecha
                        )
            values      (x.per_id, x.tit_id, x.curso_aca, 2, sysdate
                        );

            update exp_est_matriculas
            set c_archivo_temporal = x.archivo
            where  mat_exp_tit_id = x.tit_id
            and    mat_exp_per_id = x.per_id
            and    mat_curso_aca = x.curso_aca;
         else
            insert into pop_mat_circunstancias
                        (pmat_pexp_per_id, pmat_pexp_pmas_id, pmat_curso_aca, cir_id, fecha
                        )
            values      (x.per_id, x.tit_id, x.curso_aca, 2, sysdate
                        );

            update pop_est_matriculas
            set c_archivo_temporal = x.archivo
            where  pmat_pexp_pmas_id = x.tit_id
            and    pmat_pexp_per_id = x.per_id
            and    pmat_curso_aca = x.curso_aca;
         end if;
      exception
         when others then
            dbms_output.put_line (x.per_id || ' ' || x.tit_id || ' ' || sqlerrm);
      end;
   end loop;
end;


---------------------------

CONTROLES

/* Formatted on 24/10/2012 18:12 (Formatter Plus v4.8.8) */
select persona_id, estudio_id, curso_academico_id
from   uji_becas.bc2_becas b,
       uji_becas.bc2_solicitantes s
where  s.id = b.solicitante_id
and    (persona_id, estudio_id, curso_academico_id) not in (
          select mat_exp_per_id, mat_exp_tit_id, mat_curso_aca
          from   exp_mat_circunstancias
          where  cir_id = 2
          union all
          select pmat_pexp_per_id, pmat_pexp_pmas_id, pmat_curso_aca
          from   pop_mat_circunstancias
          where  cir_id = 2)
          
          
/* Formatted on 24/10/2012 18:12 (Formatter Plus v4.8.8) */
select persona_id, estudio_id, curso_academico_id
from   uji_becas.bc2_becas b,
       uji_becas.bc2_solicitantes s
where  s.id = b.solicitante_id
and    (persona_id, estudio_id, curso_academico_id) not in (
          select exp_per_id, exp_tit_id, curso_aca
          from   exp_matriculas
          where  curso_aca = 2012
          union all
          select pexp_per_id, pexp_pmas_id, curso_aca
          from   pop_matriculas
          where  curso_Aca = 2012)
          
		  
		  
		  
		  
		  
		  
		  /* Formatted on 05/11/2013 13:44 (Formatter Plus v4.8.8) */

insert into exp_mat_circunstancias

select persona_id, estudio_id, curso_academico_id, 2, sysdate
from   (select persona_id, estudio_id, curso_academico_id
        from   uji_becas.bc2_becas b,
               uji_becas.bc2_solicitantes s
        where  s.id = b.solicitante_id
        and    s.curso_academico_id = 2013
        and    (persona_id, estudio_id, curso_academico_id) not in (
                  select mat_exp_per_id, mat_exp_tit_id, mat_curso_aca
                  from   exp_mat_circunstancias
                  where  cir_id = 2
                  union all
                  select pmat_pexp_per_id, pmat_pexp_pmas_id, pmat_curso_aca
                  from   pop_mat_circunstancias
                  where  cir_id = 2))
where  estudio_id is not null
and    (persona_id, estudio_id, curso_academico_id) in (select exp_per_id, exp_tit_id, curso_aca
                                                        from   exp_matriculas
                                                        where  curso_aca = 2013)
                                                        
                                                        
                                                        
/* Formatted on 05/11/2013 13:44 (Formatter Plus v4.8.8) */
insert into pop_mat_circunstancias
select persona_id, estudio_id, curso_academico_id, 2, sysdate
from   (select persona_id, estudio_id, curso_academico_id
        from   uji_becas.bc2_becas b,
               uji_becas.bc2_solicitantes s
        where  s.id = b.solicitante_id
        and    s.curso_academico_id = 2013
        and    (persona_id, estudio_id, curso_academico_id) not in (
                  select mat_exp_per_id, mat_exp_tit_id, mat_curso_aca
                  from   exp_mat_circunstancias
                  where  cir_id = 2
                  union all
                  select pmat_pexp_per_id, pmat_pexp_pmas_id, pmat_curso_aca
                  from   pop_mat_circunstancias
                  where  cir_id = 2))
where  estudio_id is not null
and    (persona_id, estudio_id, curso_academico_id) in (select pexp_per_id, pexp_pmas_id, curso_aca
                                                        from   pop_matriculas
                                                        where  curso_aca = 2013)                                                        
														
														
														