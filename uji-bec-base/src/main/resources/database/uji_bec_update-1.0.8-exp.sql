CREATE OR REPLACE PROCEDURE UJI_BECAS.pasar_becas_conselleria
IS
BEGIN
   DECLARE
      CURSOR solicitudes
      IS
        SELECT   s.nif,
                  s.personales_id,
                  DECODE (AUTORIZACION, 'S', 1, 0) autorizacion,
                  IDSOLICITUD,
                  DATOSESTUDIOS_SOLICITUD_HJID,
                  DATOSFAMILIARES_SOLICITUD_HJ_0,
                  DATOSPERSONALES_SOLICITUD_HJ_0,
                  SITUACIONESDECLARADAS_SOLICI_0,
                  SOLICITUD_SOLICITUDES_HJID
           FROM   uji_becas_ws_solicitudes_con.ws_v_con_solicitudes s,
                  uji_becas_ws_solicitudes_con.solicitud ss
          WHERE   s.personales_id = SS.DATOSPERSONALES_SOLICITUD_HJ_0
           and s.nif not in (select nif from uji_becas_ws_solicitudes_con.WS_V_CON_SOLICITUDES_VARIAS);

      vnif_soli          VARCHAR2 (100);
      vid_solicitante    NUMBER;
      vid_beca           NUMBER;
      vestudio_id        NUMBER;
      vestudio_mec       NUMBER;
      vcontador          NUMBER;
      vpersona_id        NUMBER;
      vnum_beca_uji      NUMBER;
      vtipo_familia_id   NUMBER;
      vminus33           NUMBER;
      vminus65           NUMBER;
      vorfandad          NUMBER;
      vingresos          NUMBER;
      vtelefono          VARCHAR2 (20);
      vemail             VARCHAR2 (100);
      vmovil             VARCHAR2 (20);
      vid_domicilio      number;
      vlocalidad_id      number;
      v_existe           number;
      vcurso             number;

   BEGIN
     vcontador := 0;
     select id
     into vcurso
     from bc2_cursos_academicos where activo = 1;

     delete bc2_becas
     where convocatoria_id = 3
       and solicitante_id in (select id from bc2_solicitantes where curso_academico_id = vcurso);
     commit;

      FOR x IN solicitudes
      LOOP
        BEGIN
            BEGIN
               SELECT   id
                 INTO   vpersona_id
                 FROM   bc2_ext_personas
                WHERE   identificacion = x.nif;

            EXCEPTION
               WHEN OTHERS
               THEN
                  raise_application_error (-20000,  x.nif || ' nif no encontrdo ');
            END;

            BEGIN -- solicitantes
               SELECT   id
                 INTO   vid_solicitante
                 FROM   bc2_solicitantes
                WHERE   persona_id = vpersona_id
                  and   curso_academico_id = vcurso;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  SELECT   MAX (numero_beca_uji) + 1
                    INTO   vnum_beca_uji
                    FROM   bc2_becas b, bc2_solicitantes s
                   WHERE   b.solicitante_id = s.id
                           AND s.curso_academico_id = vcurso;

                  SELECT   telefono, email, movil
                    INTO   vtelefono, vemail, vmovil
                    FROM   uji_becas_ws_solicitudes_con.datospersonales
                   WHERE   HJID = x.personales_id;

                  SELECT   NVL (tipofamilianumerosa + 1, 1),
                           nvl(nminusvaliasuperior33,0),
                           nvl(nminusvaliasuperior65,0),
                           DECODE (orfandadsolicitante, 'S', 1, 0),
                           DECODE (hjid, 2162, 100, 0) -- ojo ingresos en el extrajero es number y en la solicitud es texto y ponen lo que quieren
                    INTO   vtipo_familia_id,
                           vminus33,
                           vminus65,
                           vorfandad,
                           vingresos
                    FROM   uji_becas_ws_solicitudes_con.situacionesdeclaradas
                   WHERE   hjid = x.situacionesdeclaradas_solici_0;


                   BEGIN

                     vid_solicitante := uji_becas.hibernate_sequence.NEXTVAL;
                     INSERT INTO bc2_solicitantes (id,
                                                      curso_academico_id,
                                                      persona_id,
                                                      numero_beca_uji,
                                                      profesion_sustentador,
                                                      telefono1,
                                                      telefono2,
                                                      email,
                                                      tipo_familia_id,
                                                      numero_miembros_computables,
                                                      numero_hermanos,
                                                      numero_hermanos_fuera,
                                                      numero_minusvalia_33,
                                                      numero_minusvalia_65,
                                                      orfandad,
                                                      unidad_familiar_independiente,
                                                      ingresos_netos,
                                                      ingresos_extranjero
                                )
                       VALUES   (vid_solicitante,
                                 (select id from bc2_cursos_academicos where activo = 1),
                                 vpersona_id,
                                 vnum_beca_uji,
                                 NULL,
                                 vtelefono,
                                 vmovil,
                                 vemail,
                                 vtipo_familia_id,
                                 0,
                                 0,
                                 0,
                                 vminus33,
                                 vminus65,
                                 vorfandad,
                                 0,
                                 0,
                                 vingresos);

                   EXCEPTION
                     WHEN OTHERS
                     THEN
                        raise_application_error (
                           -20000,'error insertar solicitante: '|| x.nif|| '---'
                           || vid_solicitante|| '---'|| vcurso || '---'|| vpersona_id|| '---'
                           || vnum_beca_uji  || '---'|| NULL|| '---'|| vtelefono  || '---'
                           || vmovil       || '---'|| vemail|| '---' || vtipo_familia_id
                           || '---'|| 0 || '---'|| 0 || '---'|| 0|| '--33' || vminus33
                           || '-65' || vminus65|| '---'|| vorfandad|| '-orf'|| 0|| '---'
                           || 0|| '-ing'|| vingresos|| '---'|| SQLERRM );
                   END;


            END;

            BEGIN  -- domicilios
               select  count(*) into v_existe
               from bc2_domicilios
               where solicitante_id = vid_solicitante;
                if v_existe = 0
                then
                      begin
                       select l.id into vlocalidad_id
                       from uji_becas_ws_solicitudes_con.datospersonales p, bc2_localidades l
                      where l.codigo_ayuntamiento = p.codprovincia||p.codmunicipio
                        and p.hjid = x.datospersonales_solicitud_hj_0
                        and upper(descmunicipio) = upper(nombre_Ayuntamiento)
                        and rownum = 1;

                        vid_domicilio :=  uji_becas.hibernate_sequence.nextval;
                        insert into  bc2_domicilios
                          (id, solicitante_id, tipo_domicilio_id, tipo_via_id, nombre_via,
                               numero, escalera, piso, puerta, provincia_id, localidad_id,
                               localidad_nombre, codigo_municipio, codigo_postal,
                               gratuito, correspondencia)
                         (select vid_domicilio,vid_solicitante,10, nvl(to_number(tipovia),1),nomvia,num,esc,
                               piso,pta,codprovincia, vlocalidad_id,
                               descmunicipio, codprovincia||codmunicipio, codpostal,
                               1,1
                          from uji_becas_ws_solicitudes_con.datospersonales
                          where hjid = x.datospersonales_solicitud_hj_0);
                      exception when others then
                         begin
                             insert into  bc2_domicilios
                            (id, solicitante_id, tipo_domicilio_id, tipo_via_id, nombre_via,
                                 numero, escalera, piso, puerta, provincia_id, localidad_id,
                                 localidad_nombre, codigo_municipio, codigo_postal,
                                 gratuito, correspondencia)
                            (select vid_domicilio,vid_solicitante,20,nvl(to_number(tipovia),1),nomvia,num,esc,
                                  piso,pta,codprovincia, vlocalidad_id,
                                  descmunicipio, codprovincia||codmunicipio, codpostal,
                                  1,1
                             from uji_becas_ws_solicitudes_con.datospersonales
                             where hjid = x.datospersonales_solicitud_hj_0);
                         exception when others
                            then null;
                         end;
                      end;
                end if;


            exception when others then null;

            end;

            BEGIN  -- miembros
              select count(*) into v_existe
              from bc2_miembros
              where solicitante_id = vid_solicitante;

              if v_existe = 0
              then
                 begin
                 -- solicitante
                   insert into bc2_MIEMBROS(ID, SOLICITANTE_ID, TIPO_MIEMBRO_ID, NOMBRE, APELLIDO1, APELLIDO2,
                                            TIPO_IDENTIFICACION_ID, IDENTIFICACION, SEXO_ID, FECHA_NACIMIENTO,
                                            NACIONALIDAD_ID, ESTADO_CIVIL_ID, PROFESION_ID, LOCALIDAD_TRABAJO,
                                            SUSTENTADOR_ID, SITUACION_LABORAL_ID, TIPO_MINUSVALIA_ID,
                                            IMPORT_RENTAS_EXTRAN,TIPO_MONEDA_ID)
                    (select  uji_becas.hibernate_sequence.nextval, vid_solicitante,'1', dp.nombre, dp.primerapellido, dp.segundoapellido ,
                             decode (upper(substr(dp.nif,1,length(dp.nif)-1)),lower(substr(dp.nif,1,length(dp.nif)-1)),1,2),dp.nif, decode(dp.sexo,'M',2,'H',1),dp.fechanacimiento,
                             p.id,1,null,null,
                             3,null,decode(s.minusvaliasuperior33solicit,'S',2,decode (minusvaliasuperior65solicit,'S',3,1)),
                             DECODE (s.hjid, 2162, 100, 0),decode(DECODE (s.hjid, 2162, 100, 0),0,3,2)
                      FROM   uji_becas_ws_solicitudes_con.datospersonales dp, uji_becas.bc2_ext_paises p,uji_becas_ws_solicitudes_con.situacionesdeclaradas s
                     WHERE   dp.HJID = x.personales_id
                       and   s.hjid = x.situacionesdeclaradas_solici_0
                       and   dp.codnacionalidad = p.codigo_iso);

                  ----------resto
                   insert into bc2_MIEMBROS(ID, SOLICITANTE_ID, TIPO_MIEMBRO_ID, NOMBRE, APELLIDO1, APELLIDO2,
                                            TIPO_IDENTIFICACION_ID, IDENTIFICACION, SEXO_ID, FECHA_NACIMIENTO,
                                            NACIONALIDAD_ID, ESTADO_CIVIL_ID, PROFESION_ID, LOCALIDAD_TRABAJO,
                                            SUSTENTADOR_ID, SITUACION_LABORAL_ID, TIPO_MINUSVALIA_ID)

                        (select  uji_becas.hibernate_sequence.nextval, vid_solicitante,tp.id, m.nombre, m.primerapellido apellido1, m.segundoapellido apellido2,
                                decode (upper(substr(m.nif,1,length(m.nif)-1)),lower(substr(m.nif,1,length(m.nif)-1)),   1,   2),m.nif,null,fechanacimiento,
                                'E',decode(estadocivil,3,7,estadocivil),null, null,
                                 parentescoaeat sustentador_id,null,decode(m.discapacidad,'N',1,3)
                        from uji_becas_ws_solicitudes_con.datosfamiliares d, uji_becas_ws_solicitudes_con.miembros ms, uji_becas_ws_solicitudes_con.miembro m, uji_becas.bc2_tipos_miembros tp, uji_becas.bc2_tipos_sustentador ts
                        where x. DATOSFAMILIARES_SOLICITUD_HJ_0 = d.hjid
                          and d.miembros_datosfamiliares_hjid = ms.hjid
                          and ms.hjid = m.miembro_miembros_hjid
                           and decode(m.parentesco,1,2,2,3,3,4,4,5,5,8,6,9,7,7,1)= tp.id
                         and m.parentescoaeat= ts.id);
                 EXCEPTION WHEN OTHERS
                   THEN NULL;
                 END;
              end if;
            END;

            BEGIN  --Datos estudio
               SELECT   valor_uji
                 INTO   vestudio_id
                 FROM   uji_becas_ws_solicitudes_con.datosestudios e,
                        bc2_diccionario d
                WHERE       x.DATOSESTUDIOS_SOLICITUD_HJID = e.hjid
                        AND d.clase_uji = 'Estudio'
                        AND e.codtitulacion = d.valor_origen
                        and curso_academico_id =  (select id from bc2_cursos_academicos where activo = 1)
                        and d.organismo_id = 2;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vestudio_id := NULL;
               when others then
                   raise_application_error (-20000, ' nif: '||x.nif||'-- id tabla estudio: '||x.DATOSESTUDIOS_SOLICITUD_HJID || sqlerrm);
            END;

            BEGIN  -- beca
              vid_beca := uji_becas.hibernate_sequence.NEXTVAL;
              INSERT INTO bc2_becas (
                                      id,
                                      solicitante_id,
                                      convocatoria_id,
                                      proceso_id,
                                      estado_id,
                                      tanda_id,
                                      tipo_identificacion_id,
                                      identificacion,
                                      codigo_beca,
                                      codigo_archivo_temporal,
                                      estudio_id,
                                      tipo_matricula_id,
                                      autorizacion_renta
                        )
                 VALUES   (
                           vid_beca,
                           vid_solicitante,
                           3,
                           1,
                           1,
                           NULL,
                           DECODE (
                              UPPER (SUBSTR (x.nif, 1, LENGTH (x.nif) - 1)),
                              LOWER (SUBSTR (x.nif, 1, LENGTH (x.nif) - 1)),
                              1,
                              2
                           ),
                           x.nif,
                           NULL,
                           x.idsolicitud,
                           vestudio_id,
                           1,
                           x.autorizacion
                          );



            exception when others then
                raise_application_error (-20000,'error insertar beca: '|| x.nif|| '---'||sqlerrm);
            end;
            COMMIT;
            vcontador := vcontador + 1;
            insert into bc2_becas_historico (ID, BECA_ID, PROCESO_ID, ESTADO_ID, TANDA_ID, FECHA, USUARIO, CONVOCATORIA_ID, FECHA_NOTIFICACION, CONCEDIDA)
            values(uji_becas.hibernate_sequence.nextval,vid_beca,1,1,null,sysdate,'REVILLO',3,NULL,0);

        END;
      END LOOP;

      raise_application_error (-20000, 'total : ' || vcontador);
   END;
END pasar_becas_conselleria;
/

CREATE OR REPLACE PROCEDURE UJI_BECAS.denegar_cons_por_mec
IS
  BEGIN
    DECLARE
      CURSOR becas (vcurso number)
      IS
        select  id
        from bc2_becas
        where solicitante_id  in (select solicitante_id from bc2_becas
        where convocatoria_id = 1
              and solicitante_id in (select id from bc2_solicitantes where curso_academico_id = vcurso)
              and beca_concedida = 1)
              and convocatoria_id = 3
              and solicitante_id in (select id from bc2_solicitantes where curso_academico_id =vcurso);
      vtanda number;
      vcurso number;
      vtanda_id number;
      vcontador number;


    BEGIN
      vcontador := 0;
      select id into vcurso
      from bc2_cursos_academicos
      where activo = 1;

--"Per obtenir la beca en la convocatòria del Ministeri d'Educació i Ciència"
      select max(tanda_id)+1 into vtanda
      from bc2_tandas
      where curso_academico_id = vcurso;

      insert into bc2_tandas (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, FECHA, ACTIVA, FECHA_NOTIFICACION, USUARIO)
      values (uji_becas.hibernate_sequence.nextval, 3,vcurso,vtanda,'Denegadas por tener concedida la beca del mec',sysdate,0,null,null);

      select id into vtanda_id
      from bc2_tandas
      where curso_academico_id = vcurso
            and tanda_id = vtanda;

      FOR x IN becas(vcurso)
      LOOP
        BEGIN
          insert into bc2_becas_denegaciones(ID, DENEGACION_ID, BECA_ID, ORDEN_DENEGACION)
          values (uji_becas.hibernate_sequence.nextval, 81,x.id,1);

          update bc2_becas set beca_concedida = 0, estado_id =3, tanda_id = vtanda_id
          where id = x.id;

          insert into bc2_becas_historico (ID, BECA_ID, PROCESO_ID, ESTADO_ID, TANDA_ID, FECHA, USUARIO, CONVOCATORIA_ID, FECHA_NOTIFICACION, CONCEDIDA)
          values(uji_becas.hibernate_sequence.nextval,x.id,1,3,vtanda_id,sysdate,'REVILLO',3,NULL,0);

          COMMIT;
          vcontador := vcontador + 1;
          exception when others then
          null;
        END;


      END LOOP;

      raise_application_error (-20000, 'total : ' || vcontador ||' tanda'||vtanda);
    END;
  END;



ALTER TABLE UJI_BECAS.BC2_MINIMOS
ADD (NOTA_SEGUNDOS_CONS  NUMBER);

update bc2_minimos m
   set m.nota_segundos_cons = m.nota_acceso_cons
where curso_academico_id = 2013;
commit;
update bc2_minimos set porc_superados_cons = 80
where curso_academico_id = 2013
      and porc_superados_cons = 85;
commit;
update bc2_minimos set porc_superados_cons = 70
where curso_academico_id = 2013
      and porc_superados_cons = 75;
commit;
update bc2_minimos set nota_acceso_cons = 7
where CURSO_ACADEMICO_ID = 2013
      and  estudio_id > 40000
      and estudio_id not in (42152,42135);
commit;