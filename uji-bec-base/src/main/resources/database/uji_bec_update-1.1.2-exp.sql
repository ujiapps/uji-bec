ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
ADD (AYUDAS_ACTUALES  VARCHAR2(4000 CHAR));

ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (DENEGACIONES_ACTUALES  VARCHAR2(4000 CHAR));

ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (CURSO_ANT_NOTA_MEDIA_SIN  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (CURSO_ANT_ESTUDIO_RAMA  VARCHAR2(2));

ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (CURSO_ACT_CAMBIO_ESTUDIOS  NUMBER(1));

ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
ADD (ESTIMACION  NUMBER(1));



ALTER TABLE BC2_BECAS_RECURSOS_DETALLE
DROP PRIMARY KEY CASCADE;

DROP TABLE BC2_BECAS_RECURSOS_DETALLE CASCADE CONSTRAINTS;

CREATE TABLE BC2_BECAS_RECURSOS_DETALLE
(
  ID          NUMBER                            NOT NULL,
  RECURSO_ID  NUMBER                            NOT NULL,
  CUANTIA_ID  NUMBER,
  IMPORTE     NUMBER,
  TEXTO       VARCHAR2(4000 BYTE),
  TIPO        VARCHAR2(2 BYTE),
  CODIGO      VARCHAR2(10 BYTE)
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
INITIAL          64K
NEXT             1M
MINEXTENTS       1
MAXEXTENTS       UNLIMITED
PCTINCREASE      0
BUFFER_POOL      DEFAULT
)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX BC2_BECAS_RECURSO_DETALLE_PK ON BC2_BECAS_RECURSOS_DETALLE
(ID)
LOGGING
TABLESPACE DATOS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
INITIAL          64K
NEXT             1M
MINEXTENTS       1
MAXEXTENTS       UNLIMITED
PCTINCREASE      0
BUFFER_POOL      DEFAULT
)
NOPARALLEL;


CREATE UNIQUE INDEX BC2_BECAS_RECURSOS_DETALLE_U01 ON BC2_BECAS_RECURSOS_DETALLE
(RECURSO_ID, CUANTIA_ID, TIPO)
LOGGING
TABLESPACE DATOS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
INITIAL          64K
NEXT             1M
MINEXTENTS       1
MAXEXTENTS       UNLIMITED
PCTINCREASE      0
BUFFER_POOL      DEFAULT
)
NOPARALLEL;


ALTER TABLE BC2_BECAS_RECURSOS_DETALLE ADD (
CONSTRAINT REC_DETALLE_TIPO
CHECK (TIPO in ('RE', 'PR')),
CONSTRAINT BC2_BECAS_RECURSO_DETALLE_PK
PRIMARY KEY
  (ID)
  USING INDEX
  TABLESPACE DATOS
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
  INITIAL          64K
  NEXT             1M
  MINEXTENTS       1
  MAXEXTENTS       UNLIMITED
  PCTINCREASE      0
    ),
CONSTRAINT BC2_BECAS_RECURSOS_DETALLE_U01
UNIQUE (RECURSO_ID, CUANTIA_ID, TIPO)
  USING INDEX
  TABLESPACE DATOS
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
  INITIAL          64K
  NEXT             1M
  MINEXTENTS       1
  MAXEXTENTS       UNLIMITED
  PCTINCREASE      0
    ));

ALTER TABLE BC2_BECAS_RECURSOS_DETALLE ADD (
CONSTRAINT BC2_BECAS_RECURSO_DET_REC_FK
FOREIGN KEY (RECURSO_ID)
REFERENCES BC2_BECAS_RECURSOS (ID),
CONSTRAINT BC2_BECAS_RECURSO_DET_CUANT_FK
FOREIGN KEY (CUANTIA_ID)
REFERENCES BC2_CUANTIAS (ID));

DROP VIEW BC2_EXT_IMPORTES_MEC;

/* Formatted on 26/06/2015 9:37:50 (QP5 v5.115.810.9015) */
create or replace force view BC2_EXT_IMPORTES_MEC(PERSONA_ID, SOLICITANTE_ID, BECA_ID, CURSO_ACADEMICO_ID, CONVOCATORIA_ID, PROCESO_ID, ESTADO_ID, UT, CONV, NIF, APELLIDO1, APELLIDO2, NOMBRE, TIPO_FAMILIA_ID, TASAS_TOTAL, TASAS_MINISTERIO,
    TASAS_CONSELLERIA, CREDITOS_CUBIERTOS, CURSO_ACA, ESTUDIO_ID, TIPO_ESTUDIO, NOTA_MEDIA, ID_TITULACION) as
  (select s.persona_id, b.solicitante_id, b.id beca_id, s.curso_academico_id, b.convocatoria_id, b.proceso_id, b.estado_id, '40' ut, c.acronimo conv, B.IDENTIFICACION nif, p.persona_apellido1, p.persona_apellido2, p.persona_nombre,
     s.tipo_familia_id, e.tasas_total, e.tasas_ministerio, e.tasas_conselleria, e.creditos_cubiertos, s.curso_academico_id || '/' || (s.curso_academico_id + 1) curso_aca, e.estudio_id, es.tipo tipo_estudio, b.nota_media_ant,
     valor_origen id_titulacion
   from bc2_becas b join bc2_solicitantes s on s.id = b.solicitante_id join bc2_ext_personas p on p.id = s.persona_id join bc2_ext_personas_estudios e
       on e.persona_id = p.id
          and e.curso_academico_id = s.curso_academico_id
     join bc2_ext_estudios es
       on es.id = e.estudio_id
          and es.id = b.estudio_id
     join (select distinct *
           from bc2_diccionario d1
           union
           select *
           from bc2_diccionario_excepciones d2) d
       on d.valor_uji = e.estudio_id
          and d.curso_academico_id = s.curso_academico_id
          and d.clase_uji = 'Estudio'
     join bc2_convocatorias c
       on b.convocatoria_id = c.id
          and c.organismo_id = d.organismo_id
   where d.organismo_id = 1
         and c.id in (1, 2)
         and b.beca_concedida = 1)
  union
  (select s.persona_id, b.solicitante_id, b.id beca_id, s.curso_academico_id, 99 convocatoria_id, b.proceso_id, b.estado_id, '40' ut, 'IN' conv, B.IDENTIFICACION nif, p.persona_apellido1, p.persona_apellido2, p.persona_nombre,
     s.tipo_familia_id, e.tasas_total, e.tasas_ministerio, e.tasas_conselleria, e.creditos_cubiertos, s.curso_academico_id || '/' || (s.curso_academico_id + 1) curso_aca, e.estudio_id, es.tipo tipo_estudio, b.nota_media_ant,
     valor_origen id_titulacion
   from bc2_becas b join bc2_solicitantes s on s.id = b.solicitante_id join bc2_ext_personas p on p.id = s.persona_id join bc2_ext_personas_estudios e
       on e.persona_id = p.id
          and e.curso_academico_id = s.curso_academico_id
     join bc2_ext_estudios es
       on es.id = e.estudio_id
          and es.id = b.estudio_id
     join (select distinct *
           from bc2_diccionario d1
           union
           select *
           from bc2_diccionario_excepciones d2) d
       on d.valor_uji = e.estudio_id
          and d.curso_academico_id = s.curso_academico_id
          and d.clase_uji = 'Estudio'
     join bc2_convocatorias c
       on b.convocatoria_id = c.id
          and c.organismo_id = d.organismo_id
   where d.organismo_id = 1
         and c.id in (1, 2)
         and b.beca_concedida = 1
         and s.curso_academico_id = 2013
         and b.proceso_id = 4
         and b.estado_id = 5);

SET DEFINE OFF;
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (1, 1, 'Compensació de preus públics grados (Arxiu)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/grados/{cursoAcademicoId}', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (2, 1, 'Compensació de preus públics grados (PDF)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/grados/{cursoAcademicoId}/pdf', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (3, 2, 'Informe final totes les beques', 'http://ujiapps.uji.es/bec/rest/conselleria/todas/{cursoAcademicoId}', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (4, 2, 'Informe final totes les beques (PDF)', 'http://ujiapps.uji.es/bec/rest/conselleria/todas/{cursoAcademicoId}/pdf', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (5, 1, 'Compensació de preus públics màsters (Arxiu)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/masters/{cursoAcademicoId}', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (6, 1, 'Compensació de preus públics màsters (PDF)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/masters/{cursoAcademicoId}/pdf', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (7, 1, 'Compensació incidències (PDF)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/incidencias/{cursoAcademicoId}/pdf', 1);
Insert into BC2_LISTADOS
(ID, ORGANISMO_ID, NOMBRE, URL, ACTIVO)
Values
  (8, 1, 'Compensació incidències (Arxiu)', 'http://ujiapps.uji.es/bec/rest/ministerio/compensacion/incidencias/{cursoAcademicoId}/pdf', 1);
COMMIT;

