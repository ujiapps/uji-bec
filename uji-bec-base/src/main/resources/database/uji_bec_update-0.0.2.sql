
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_ESTUDIOS (ID,
                                                             ESTUDIO_ID,
                                                             NUMERO_CURSOS,
                                                             CREDITOS_TOTALES,
                                                             PRESENCIAL,
                                                             SIN_DOCENCIA,
                                                             CREDITOS,
                                                             CREDITOS_MINIMOS,
                                                             TIPO_MATRICULA_ID,
                                                             BECA_ID,
                                                             CURSO_ACADEMICO_ID,
                                                             CURSO_ACADEMICO_ACTIVO,
                                                             TITULACION_TECNICA
                                                            ) AS
   SELECT solicitud.id * 100000 + becas.id as id, estudio.id AS estudio_Id, estudio.NUMERO_CURSOS AS numero_Cursos,
          estudio.CREDITOS_TOTALES AS creditos_Totales, estudio.presencial AS presencial,
          estudio.SIN_DOCENCIA AS sin_Docencia, minimos.creditos AS creditos,
          minimos.CREDITOS_MINIMOS AS creditos_Minimos, becas.TIPO_MATRICULA_ID AS tipo_Matricula_Id,
          becas.id AS beca_Id, CURSO_ACA.id AS curso_Academico_Id, curso_aca.activo AS curso_Academico_Activo,
          estudio.titulacion_tecnica AS titulacion_tecnica
   FROM   BC2_EXT_ESTUDIOS estudio,
          BC2_BECAS becas,
          BC2_SOLICITANTES solicitud,
          BC2_MINIMOS minimos,
          BC2_CURSOS_ACADEMICOS curso_aca
   WHERE  estudio.id = becas.estudio_id
   AND    estudio.id = minimos.estudio_id
   AND    minimos.CURSO_ACADEMICO_ID = curso_aca.id
   AND    solicitud.id = becas.solicitante_id
   AND    solicitud.curso_academico_id = curso_aca.id;
   
   
   
ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN BLOQUEAR_ACADEMICOS TO cumple_academicos;
   
ALTER TABLE UJI_BECAS.BC2_CONVOCATORIAS
 ADD (nombre  VARCHAR2(100));

update bc2_convocatorias
set nombre = acronimo;

commit;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
  DROP CONSTRAINT BC2_MIEMBROS_EST_CIVILES_FK;

delete UJI_BECAS.bc2_estados_civiles;

commit;

ALTER TABLE UJI_BECAS.BC2_ESTADOS_CIVILES
MODIFY(ID NUMBER);

Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (1, 'Soltero', 10);
Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (2, 'Casado', 20);
Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (3, 'Viudo', 30);
Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (4, 'Separado', 40);
Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (5, 'Divorciado', 50);
Insert into BC2_ESTADOS_CIVILES
   (ID, NOMBRE, ORDEN)
 Values
   (6, 'Religioso', 60);
COMMIT;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
ADD (ESTADO_CIVIL_ID_num NUMBER);

update UJI_BECAS.BC2_MIEMBROS
set estado_civil_id_num = to_number(estado_civil_id);

commit;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS DROP COLUMN ESTADO_CIVIL_ID;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
RENAME COLUMN ESTADO_CIVIL_ID_num TO ESTADO_CIVIL_ID;


ALTER TABLE UJI_BECAS.BC2_MIEMBROS ADD (
  CONSTRAINT BC2_MIEMBROS_EST_CIVILES_FK 
 FOREIGN KEY (ESTADO_CIVIL_ID) 
 REFERENCES UJI_BECAS.BC2_ESTADOS_CIVILES (ID));

 
 
 
   