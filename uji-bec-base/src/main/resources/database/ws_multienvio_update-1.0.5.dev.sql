DROP VIEW UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD;

create or replace force view uji_becas_ws_multienvio.estados_solicitud(id, estado) as
  select '01' id, 'Abonada' estado from dual
  union
  select '02', 'Alta                               ' from dual
  union
  select '03', 'Anulada                            ' from dual
  union
  select '04', 'Asignada Empresa                   ' from dual
  union
  select '06', 'Comunicado Retenida UT             ' from dual
  union
  select '07', 'Concedida                          ' from dual
  union
  select '08', 'Confirmada                         ' from dual
  union
  select '09', 'Correcta                           ' from dual
  union
  select '10', 'Denegada                          ' from dual
  union
  select '11', 'Enviada SSCC                      ' from dual
  union
  select '12', 'Enviada AEAT                      ' from dual
  union
  select '13', 'Errónea                           ' from dual
  union
  select '14', 'Excluida                          ' from dual
  union
  select '15', 'Pagada                            ' from dual
  union
  select '16', 'Pendiente de Cupo sin AEAT        ' from dual
  union
  select '17', 'Pendiente de Datos Complementarios' from dual
  union
  select '18', 'Pendiente de Datos Requeridos     ' from dual
  union
  select '19', 'Pendiente de Concesión            ' from dual
  union
  select '20', 'Pendiente de Reenvío al Tesoro    ' from dual
  union
  select '21', 'Prealta Completa                  ' from dual
  union
  select '22', 'Prealta Inicial                   ' from dual
  union
  select '23', 'Prealta Parcial                   ' from dual
  union
  select '25', 'Predenegada                       ' from dual
  union
  select '26', 'Promociona                        ' from dual
  union
  select '27', 'Rechazada                         ' from dual
  union
  select '29', 'Recibida Pendiente de Cupo        ' from dual
  union
  select '31', 'Renuncia                          ' from dual
  union
  select '33', 'Retenida SSCC                     ' from dual
  union
  select '34', 'Retenida UT                       ' from dual
  union
  select '35', 'Revocada Parcial                  ' from dual
  union
  select '36', 'Revocada Parcial en Proceso       ' from dual
  union
  select '37', 'Revocada Total                    ' from dual
  union
  select '38', 'Revocada Total en Proceso         ' from dual
  union
  select '39', 'Sin AEAT                          ' from dual
  union
  select '40', 'Suplente                          ' from dual
  union
  select '41', 'Pagada Parcial                    ' from dual
  union
  select '42', 'Excluida por Cambio de UT         ' from dual
  union
  select '43', 'En Espera de AEAT                 ' from dual
  union
  select '44', 'Con Datos Bancarios               ' from dual
  union
  select '45', 'Pendiente Valoración Jurado       ' from dual
  union
  select '46', 'Recurso en Proceso                ' from dual
  union
  select '47', 'Recurso Estimado                  ' from dual
  union
  select '48', 'Pendiente Asignación Nota         ' from dual;


DROP PUBLIC SYNONYM ESTADOS_SOLICITUD;

CREATE PUBLIC SYNONYM ESTADOS_SOLICITUD FOR UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD;

GRANT SELECT ON UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD TO UJI_BECAS;
