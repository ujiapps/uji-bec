-- Generado por Oracle SQL Developer Data Modeler 3.1.0.687
--   en:        2012-04-18 14:26:03 CEST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP VIEW uji_becas.bc2_v_tandas_convocatoria 
;
DROP TABLE uji_becas.bc2_becas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_cuantias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_becas_historico CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_convocatorias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cuantias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cuantias_por_curso CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_cursos_academicos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_domicilios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_estados CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_estados_civiles CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_localidades CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_paises CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_personas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_personas_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_ext_provincias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_miembros CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_minimos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_organismos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_procesos CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_profesiones CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_situaciones_laborales CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_solicitantes CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tandas CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_domicilios CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_familias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_identificacion CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_matricula CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_miembros CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_minusvalia CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_propiedad CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_residencias CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_sexo CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_sustentador CASCADE CONSTRAINTS 
;
DROP TABLE uji_becas.bc2_tipos_vias CASCADE CONSTRAINTS 
;
CREATE TABLE uji_becas.bc2_becas 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     estado_id NUMBER  NOT NULL , 
     tanda_id NUMBER , 
     tipo_identificacion_id NUMBER  NOT NULL , 
     identificacion VARCHAR2 (100) , 
     codigo_beca VARCHAR2 (100) , 
     codigo_archivo_temporal VARCHAR2 (100) , 
     tiene_titulo_universitario NUMBER DEFAULT 0  NOT NULL CHECK ( tiene_titulo_universitario IN (0, 1)) , 
     titulo_nombre VARCHAR2 (1000) , 
     codigo_estudio_ant VARCHAR2 (100) , 
     estudio_ant VARCHAR2 (1000) , 
     estudio_ant_otros VARCHAR2 (1000) , 
     curso_academico_otros NUMBER , 
     causa_otros VARCHAR2 (1000) , 
     beca_curso_ant NUMBER DEFAULT 0  NOT NULL CHECK ( beca_curso_ant IN (0, 1)) , 
     beca_curso_ant_2 NUMBER DEFAULT 0  NOT NULL CHECK ( beca_curso_ant_2 IN (0, 1)) , 
     beca_curso_ant_3 NUMBER DEFAULT 0  NOT NULL CHECK ( beca_curso_ant_3 IN (0, 1)) , 
     creditos_estudio_ant NUMBER , 
     creditos_matriculados_ant NUMBER , 
     creditos_superados_ant NUMBER , 
     creditos_suspensos_ant NUMBER , 
     creditos_convalidados_ant NUMBER , 
     matricula_parcial_ant NUMBER DEFAULT 0  NOT NULL CHECK ( matricula_parcial_ant IN (0, 1)) , 
     nota_media_ant NUMBER , 
     becas_concedidas_ant NUMBER , 
     tercio_presentados_ant NUMBER DEFAULT 0  NOT NULL CHECK ( tercio_presentados_ant IN (0, 1)) , 
     estudio_id NUMBER , 
     tipo_matricula_id NUMBER , 
     codigo_centro VARCHAR2 (100) , 
     creditos_matriculados NUMBER , 
     creditos_convalidados NUMBER , 
     creditos_pend_conva NUMBER , 
     creditos_para_beca NUMBER , 
     creditos_fuera_beca NUMBER , 
     numero_semestres NUMBER , 
     curso NUMBER , 
     matricula_parcial NUMBER DEFAULT 0  NOT NULL CHECK ( matricula_parcial IN (0, 1)) , 
     estudios_sin_docencia NUMBER DEFAULT 0  NOT NULL CHECK ( estudios_sin_docencia IN (0, 1)) , 
     familia_especialidad VARCHAR2 (1000) , 
     presencial NUMBER DEFAULT 1  NOT NULL CHECK ( presencial IN (0, 1)) , 
     limitar_creditos NUMBER DEFAULT 0  NOT NULL CHECK ( limitar_creditos IN (0, 1)) , 
     limitar_creditos_fin_estudios NUMBER DEFAULT 0  NOT NULL CHECK ( limitar_creditos_fin_estudios IN (0, 1)) , 
     beca_parcial NUMBER DEFAULT 0  NOT NULL CHECK ( beca_parcial IN (0, 1)) , 
     distancia_al_centro NUMBER , 
     transporte_urbano NUMBER DEFAULT 0  NOT NULL CHECK ( transporte_urbano IN (0, 1)) , 
     numero_transportes NUMBER , 
     transporte_barco_avion NUMBER DEFAULT 0  NOT NULL CHECK ( transporte_barco_avion IN (0, 1)) , 
     observaciones VARCHAR2 (4000) , 
     observaciones_uji VARCHAR2 (4000) , 
     autorizacion_renta NUMBER DEFAULT 0  NOT NULL CHECK ( autorizacion_renta IN (0, 1)) , 
     entidad VARCHAR2 (10) , 
     sucursal VARCHAR2 (10) , 
     digitos_control VARCHAR2 (10) , 
     cuenta_bancaria VARCHAR2 (10) , 
     programa_intercambio NUMBER DEFAULT 0  NOT NULL CHECK ( programa_intercambio IN (0, 1)) , 
     bloquear_academicos NUMBER DEFAULT 0 CHECK ( bloquear_academicos IN (0, 1)) 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_sol_IDX ON uji_becas.bc2_becas 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_conv_IDX ON uji_becas.bc2_becas 
    ( 
     convocatoria_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_prov_IDX ON uji_becas.bc2_becas 
    ( 
     proceso_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_est_IDX ON uji_becas.bc2_becas 
    ( 
     estado_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_tan_IDX ON uji_becas.bc2_becas 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_dni_IDX ON uji_becas.bc2_becas 
    ( 
     tipo_identificacion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_est_IDXv1 ON uji_becas.bc2_becas 
    ( 
     estudio_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_mat_IDX ON uji_becas.bc2_becas 
    ( 
     tipo_matricula_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_becas_cuantias 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     cuantia_id NUMBER  NOT NULL , 
     importe NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_det_cuan_cuan_FK ON uji_becas.bc2_becas_cuantias 
    ( 
     cuantia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_becas_cuantias__IDX ON uji_becas.bc2_becas_cuantias 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_becas_historico 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     estado_id NUMBER  NOT NULL , 
     tanda_id NUMBER , 
     fecha DATE , 
     usuario VARCHAR2 (100) 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_historico__IDX ON uji_becas.bc2_becas_historico 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_historico 
    ADD CONSTRAINT bc2_detalle_historico_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_convocatorias 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL CHECK ( activa IN (0, 1)) , 
     acronimo VARCHAR2 (100) 
    ) 
;


CREATE INDEX uji_becas.bc2_convocatorias_org_IDX ON uji_becas.bc2_convocatorias 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_convocatorias 
    ADD CONSTRAINT bc2_convocatorias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_cuantias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_becas.bc2_cuantias 
    ADD CONSTRAINT bc2_cuantias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_cuantias_por_curso 
    ( 
     id NUMBER  NOT NULL , 
     cuantia_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     importe NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_cuantias_por_curso_cu_IDX ON uji_becas.bc2_cuantias_por_curso 
    ( 
     cuantia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_cuantias_por_curso_ca_IDX ON uji_becas.bc2_cuantias_por_curso 
    ( 
     curso_academico_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_curso_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_cursos_academicos 
    ( 
     id NUMBER  NOT NULL , 
     activo NUMBER DEFAULT 0  NOT NULL CHECK ( activo IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_cursos_academicos 
    ADD CONSTRAINT bc2_cursos_academicos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_domicilios 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     tipo_domicilio_id NUMBER , 
     tipo_via_id NUMBER , 
     nombre_via VARCHAR2 (1000) , 
     numero VARCHAR2 (100) , 
     escalera VARCHAR2 (100) , 
     piso VARCHAR2 (100) , 
     puerta VARCHAR2 (100) , 
     provincia_id NUMBER , 
     localidad_id NUMBER , 
     localidad_nombre VARCHAR2 (1000) , 
     codigo_municipio VARCHAR2 (10) , 
     codigo_postal VARCHAR2 (10) , 
     residencia_esp NUMBER DEFAULT 0  NOT NULL CHECK ( residencia_esp IN (0, 1)) , 
     otra_clase_domicilio VARCHAR2 (1000) , 
     gratuito NUMBER DEFAULT 0  NOT NULL CHECK ( gratuito IN (0, 1)) , 
     correspondencia NUMBER DEFAULT 0  NOT NULL CHECK ( correspondencia IN (0, 1)) , 
     propiedad_de_id NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_domicilios_sol_IDX ON uji_becas.bc2_domicilios 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_dom_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_domicilio_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_via_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_via_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_prov_IDX ON uji_becas.bc2_domicilios 
    ( 
     provincia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_loc_IDX ON uji_becas.bc2_domicilios 
    ( 
     localidad_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_domicilios_prop_IDX ON uji_becas.bc2_domicilios 
    ( 
     propiedad_de_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_PK PRIMARY KEY ( id ) ;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios__UN UNIQUE ( solicitante_id , tipo_domicilio_id ) ;


CREATE TABLE uji_becas.bc2_estados 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_estados 
    ADD CONSTRAINT bc2_estados_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_estados_civiles 
    ( 
     id VARCHAR2 (10)  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_estados_civiles 
    ADD CONSTRAINT bc2_estados_civiles_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_estudios 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     tipo VARCHAR2 (100)  NOT NULL , 
     oficial VARCHAR2 (10)  NOT NULL , 
     numero_cursos NUMBER , 
     creditos_totales NUMBER , 
     creditos_por_curso NUMBER , 
     sin_docencia NUMBER DEFAULT 0  NOT NULL CHECK ( sin_docencia IN (0, 1)) , 
     extinguida NUMBER DEFAULT 0  NOT NULL CHECK ( extinguida IN (0, 1)) , 
     presencial NUMBER DEFAULT 0  NOT NULL CHECK ( presencial IN (0, 1)) , 
     titulacion_texnica NUMBER DEFAULT 0  NOT NULL CHECK ( titulacion_texnica IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_estudios 
    ADD CONSTRAINT bc2_ext_estudios_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_localidades 
    ( 
     id NUMBER  NOT NULL , 
     provincia_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER , 
     codigo VARCHAR2 (100) , 
     codigo_ayuntamiento VARCHAR2 (100) , 
     codigo_mec VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_localidades 
    ADD CONSTRAINT bc2_ext_localidades_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_paises 
    ( 
     id VARCHAR2 (10)  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     nacionalidad VARCHAR2 (100) , 
     codigo_iso VARCHAR2 (10) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_paises 
    ADD CONSTRAINT bc2_ext_paises_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_personas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     identificacion VARCHAR2 (100)  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_personas 
    ADD CONSTRAINT bc2_ext_personas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_personas_estudios 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     creditos_matriculados NUMBER , 
     creditos_superados NUMBER , 
     creditos_faltantes NUMBER , 
     creditos_pendientes_conva NUMBER , 
     creditos_fuera_beca NUMBER , 
     creditos_presentados NUMBER , 
     limitar_creditos NUMBER DEFAULT 0 CHECK ( limitar_creditos IN (0, 1)) , 
     limitar_creditos_fin_estudios NUMBER DEFAULT 0 CHECK ( limitar_creditos_fin_estudios IN (0, 1)) , 
     matricula_parcial NUMBER DEFAULT 0 CHECK ( matricula_parcial IN (0, 1)) , 
     numero_becas NUMBER , 
     minusvalia NUMBER DEFAULT 0 CHECK ( minusvalia IN (0, 1)) , 
     numero_semestres NUMBER , 
     sin_docencia NUMBER DEFAULT 0 CHECK ( sin_docencia IN (0, 1)) , 
     programa_intercambio NUMBER DEFAULT 0 CHECK ( programa_intercambio IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_estudios_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_ext_provincias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER , 
     codigo VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_provincias 
    ADD CONSTRAINT bc2_ext_provincias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_miembros 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     tipo_miembro_id NUMBER , 
     nombre VARCHAR2 (100)  NOT NULL , 
     apellido1 VARCHAR2 (100)  NOT NULL , 
     apellido2 VARCHAR2 (100) , 
     tipo_identificacion_id NUMBER  NOT NULL , 
     identificacion VARCHAR2 (100) , 
     sexo_id NUMBER , 
     fecha_nacimiento DATE , 
     nacionalidad_id VARCHAR2 (10)  NOT NULL , 
     estado_civil_id VARCHAR2 (10) , 
     profesion_id NUMBER , 
     localidad_trabajo VARCHAR2 (1000) , 
     sustentador_id NUMBER  NOT NULL , 
     situacion_laboral_id NUMBER , 
     tipo_minusvalia_id NUMBER , 
     no_valida_identificacion NUMBER DEFAULT 0  NOT NULL CHECK ( no_valida_identificacion IN (0, 1)) , 
     padron_localidad_id NUMBER , 
     padron_provincia_id NUMBER , 
     tipo_residencia_id NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_miembros_sol_IDX ON uji_becas.bc2_miembros 
    ( 
     solicitante_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sexo_IDX ON uji_becas.bc2_miembros 
    ( 
     sexo_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_res_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_residencia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sus_IDX ON uji_becas.bc2_miembros 
    ( 
     sustentador_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_ec_IDX ON uji_becas.bc2_miembros 
    ( 
     estado_civil_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_tmiem_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_miembro_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_prof_IDX ON uji_becas.bc2_miembros 
    ( 
     profesion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_minus_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_minusvalia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_sitlab_IDX ON uji_becas.bc2_miembros 
    ( 
     situacion_laboral_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_tdni_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_identificacion_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_miembros_nacion_IDX ON uji_becas.bc2_miembros 
    ( 
     nacionalidad_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_minimos 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     creditos_minimos NUMBER  NOT NULL , 
     creditos NUMBER  NOT NULL , 
     tipo_estudio VARCHAR2 (10)  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_minimos_ca_IDX ON uji_becas.bc2_minimos 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_minimos_est_IDX ON uji_becas.bc2_minimos 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_organismos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     webservice VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_becas.bc2_organismos 
    ADD CONSTRAINT bc2_organismos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_procesos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_procesos 
    ADD CONSTRAINT bc2_procesos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_profesiones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_profesiones 
    ADD CONSTRAINT bc2_profesiones_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_situaciones_laborales 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_situaciones_laborales 
    ADD CONSTRAINT bc2_situaciones_laborales_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_solicitantes 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     numero_beca_uji NUMBER , 
     profesion_sustentador NUMBER , 
     telefono1 VARCHAR2 (100) , 
     telefono2 VARCHAR2 (100) , 
     email VARCHAR2 (1000) , 
     tipo_familia_id NUMBER , 
     numero_miembros_computables NUMBER , 
     numero_hermanos NUMBER , 
     numero_hermanos_fuera NUMBER , 
     numero_minusvalia_33 NUMBER , 
     numero_minusvalia_65 NUMBER , 
     orfandad NUMBER DEFAULT 0  NOT NULL CHECK ( orfandad IN (0, 1)) , 
     minusvalia NUMBER DEFAULT 0  NOT NULL CHECK ( minusvalia IN (0, 1)) , 
     unidad_familiar_independiente NUMBER DEFAULT 0  NOT NULL CHECK ( unidad_familiar_independiente IN (0, 1)) , 
     ingresos_netos NUMBER , 
     ingresos_extranjero NUMBER , 
     propiedad_de_id NUMBER , 
     importe_alquiler NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_solicitantes_ca_IDX ON uji_becas.bc2_solicitantes 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_solicitantes_tp_IDX ON uji_becas.bc2_solicitantes 
    ( 
     tipo_familia_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_solicitantes_pro_IDX ON uji_becas.bc2_solicitantes 
    ( 
     propiedad_de_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_solicitudes_PK PRIMARY KEY ( id ) ;

ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_solicitantes__UN UNIQUE ( curso_academico_id , persona_id ) ;


CREATE TABLE uji_becas.bc2_tandas 
    ( 
     id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     descripcion VARCHAR2 (1000) , 
     fecha DATE , 
     activa NUMBER DEFAULT 1  NOT NULL CHECK ( activa IN (0, 1)) 
    ) 
;


CREATE INDEX uji_becas.bc2_tandas_curso_aca_IDX ON uji_becas.bc2_tandas 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_tandas_con_IDX ON uji_becas.bc2_tandas 
    ( 
     convocatoria_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_domicilios 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_domicilios 
    ADD CONSTRAINT bc2_tipos_domicilios_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_familias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_familias 
    ADD CONSTRAINT bc2_tipos_familias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_identificacion 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_identificacion 
    ADD CONSTRAINT bc2_tipos_identificacion_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_matricula 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_matricula 
    ADD CONSTRAINT bc2_tipos_matricula_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_miembros 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL , 
     solicitante NUMBER DEFAULT 0  NOT NULL CHECK ( solicitante IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_miembros 
    ADD CONSTRAINT bc2_tipos_miembros_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_minusvalia 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_minusvalia 
    ADD CONSTRAINT bc2_tipos_minusvalia_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_propiedad 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_propiedad 
    ADD CONSTRAINT bc2_tipos_propiedad_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_residencias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_residencias 
    ADD CONSTRAINT bc2_tipos_residencias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_sexo 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_sexo 
    ADD CONSTRAINT bc2_tipos_sexo_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_sustentador 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_sustentador 
    ADD CONSTRAINT bc2_tipos_sustentador_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_tipos_vias 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     orden NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_tipos_vias 
    ADD CONSTRAINT bc2_tipos_vias_PK PRIMARY KEY ( id ) ;



ALTER TABLE uji_becas.bc2_convocatorias 
    ADD CONSTRAINT bc2_conv_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_cuantias_por_curso 
    ADD CONSTRAINT bc2_cuantias_por_cuan_FK FOREIGN KEY 
    ( 
     cuantia_id
    ) 
    REFERENCES uji_becas.bc2_cuantias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_cua_FK FOREIGN KEY 
    ( 
     cuantia_id
    ) 
    REFERENCES uji_becas.bc2_cuantias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_det_cuantias_det_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas_historico 
    ADD CONSTRAINT bc2_det_historico_det_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_estados_FK FOREIGN KEY 
    ( 
     estado_id
    ) 
    REFERENCES uji_becas.bc2_estados 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_estudios_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_procesos_FK FOREIGN KEY 
    ( 
     proceso_id
    ) 
    REFERENCES uji_becas.bc2_procesos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_solicitudes_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tandas_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tipos_id_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_becas 
    ADD CONSTRAINT bc2_detalle_tipos_mat_FK FOREIGN KEY 
    ( 
     tipo_matricula_id
    ) 
    REFERENCES uji_becas.bc2_tipos_matricula 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_loc_FK FOREIGN KEY 
    ( 
     localidad_id
    ) 
    REFERENCES uji_becas.bc2_ext_localidades 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_pro_FK FOREIGN KEY 
    ( 
     provincia_id
    ) 
    REFERENCES uji_becas.bc2_ext_provincias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_dom_FK FOREIGN KEY 
    ( 
     tipo_domicilio_id
    ) 
    REFERENCES uji_becas.bc2_tipos_domicilios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_pro_FK FOREIGN KEY 
    ( 
     propiedad_de_id
    ) 
    REFERENCES uji_becas.bc2_tipos_propiedad 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipos_vias_FK FOREIGN KEY 
    ( 
     tipo_via_id
    ) 
    REFERENCES uji_becas.bc2_tipos_vias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_localidades 
    ADD CONSTRAINT bc2_ext_localidades_pro_FK FOREIGN KEY 
    ( 
     provincia_id
    ) 
    REFERENCES uji_becas.bc2_ext_provincias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_bc2_ext_paises_FK FOREIGN KEY 
    ( 
     nacionalidad_id
    ) 
    REFERENCES uji_becas.bc2_ext_paises 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_est_civiles_FK FOREIGN KEY 
    ( 
     estado_civil_id
    ) 
    REFERENCES uji_becas.bc2_estados_civiles 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_profesiones_FK FOREIGN KEY 
    ( 
     profesion_id
    ) 
    REFERENCES uji_becas.bc2_profesiones 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_sit_lab_FK FOREIGN KEY 
    ( 
     situacion_laboral_id
    ) 
    REFERENCES uji_becas.bc2_situaciones_laborales 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipo_sexo_FK FOREIGN KEY 
    ( 
     sexo_id
    ) 
    REFERENCES uji_becas.bc2_tipos_sexo 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipo_sust_FK FOREIGN KEY 
    ( 
     sustentador_id
    ) 
    REFERENCES uji_becas.bc2_tipos_sustentador 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_id_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_mi_FK FOREIGN KEY 
    ( 
     tipo_miembro_id
    ) 
    REFERENCES uji_becas.bc2_tipos_miembros 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_min_FK FOREIGN KEY 
    ( 
     tipo_minusvalia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_minusvalia 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_res_FK FOREIGN KEY 
    ( 
     tipo_residencia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_residencias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_minimos 
    ADD CONSTRAINT bc2_minimos_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_becas.bc2_ext_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_tipo_fami_FK FOREIGN KEY 
    ( 
     tipo_familia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_familias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_tipos_pro_FK FOREIGN KEY 
    ( 
     propiedad_de_id
    ) 
    REFERENCES uji_becas.bc2_tipos_propiedad 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_conv_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW uji_becas.bc2_v_tandas_convocatoria AS
SELECT bc2_organismos.id
  || lpad(bc2_convocatorias.id, 3, '0')
  || bc2_tandas.curso_academico_id
  || lpad(bc2_tandas.id, 4, 0) AS Id,
  bc2_organismos.id            AS organismo_id,
  bc2_organismos.nombre,
  bc2_convocatorias.id     AS convocatoria_id,
  bc2_convocatorias.activa AS convocatoria_activa,
  bc2_convocatorias.acronimo,
  bc2_tandas.curso_academico_id,
  bc2_tandas.tanda_id,
  bc2_tandas.descripcion AS nombre_tanda,
  bc2_tandas.activa      AS tanda_activa
FROM bc2_convocatorias
INNER JOIN bc2_tandas
ON bc2_convocatorias.id = bc2_tandas.convocatoria_id
INNER JOIN bc2_organismos
ON bc2_organismos.id = bc2_convocatorias.organismo_id ;









































-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            35
-- CREATE INDEX                            38
-- ALTER TABLE                             80
-- CREATE VIEW                              1
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE STRUCTURED TYPE                   0
-- CREATE COLLECTION TYPE                   0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
