ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (convocatoria_id  NUMBER);

ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (activa  NUMBER DEFAULT 1 NOT NULL );

update UJI_BECAS.BC2_CUANTIAS
set convocatoria_id = 1;

commit;

ALTER TABLE uji_becas.bc2_cuantias 
    ADD CONSTRAINT bc2_cuantias_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (familia_numerosa  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (becario  NUMBER                               DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (matricula_honor  NUMBER                       DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN MINUSVALIA;

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN PROPIEDAD_DE_ID;

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES DROP COLUMN IMPORTE_ALQUILER;

ALTER TABLE UJI_BECAS.BC2_DOMICILIOS
 ADD (importe_alquiler  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN CURSO_ACADEMICO_OTROS TO CURSO_ACADEMICO_ant;

ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN CODIGO_CENTRO TO CODIGO_CENTRO_ant;
 
ALTER TABLE UJI_BECAS.BC2_CUANTIAS
 ADD (codigo  VARCHAR2(10));


update UJI_BECAS.BC2_CUANTIAS
set codigo = '0000';

commit;

ALTER TABLE UJI_BECAS.BC2_CUANTIAS
MODIFY(CODIGO  NOT NULL);

 ALTER TABLE UJI_BECAS.bc2_ext_personas_estudios
RENAME COLUMN familia_numerosa TO familia_numerosa_id;


ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_est_fam_FK FOREIGN KEY 
    ( 
     familia_numerosa_id
    ) 
    REFERENCES uji_becas.bc2_tipos_familias 
    ( 
     id
    ) 
;
 
 
 ALTER TABLE UJI_BECAS.BC2_DOMICILIOS
 ADD (tipo_residencia_id  NUMBER);


CREATE INDEX uji_becas.bc2_domicilios_res_IDX ON uji_becas.bc2_domicilios 
    ( 
     tipo_residencia_id ASC 
    ) 
;

ALTER TABLE UJI_BECAS.BC2_MIEMBROS DROP COLUMN TIPO_RESIDENCIA_ID;

ALTER TABLE uji_becas.bc2_domicilios 
    ADD CONSTRAINT bc2_domicilios_tipo_res_FK FOREIGN KEY 
    ( 
     tipo_residencia_id
    ) 
    REFERENCES uji_becas.bc2_tipos_residencias 
    ( 
     id
    ) 
;

ALTER TABLE UJI_BECAS.BC2_LOCALIDADES
 ADD (distancia  NUMBER);

 
ALTER TABLE uji_becas.bc2_becas_cuantias 
    ADD CONSTRAINT bc2_becas_cuantias__UN UNIQUE ( beca_id , cuantia_id ) ;

ALTER TABLE uji_becas.bc2_becas_denegaciones 
    ADD CONSTRAINT bc2_becas_denegaciones__UN UNIQUE ( denegacion_id , beca_id , orden_denegacion ) ;





