CREATE TABLE uji_becas.bc2_economicos 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     coeficiente_prelacion NUMBER , 
     ind_becario NUMBER , 
     umbral_becario NUMBER , 
     ind_compensa NUMBER , 
     umbral_compensa NUMBER , 
     ind_tasas NUMBER , 
     umbral_tasas NUMBER , 
     ind_mov_general NUMBER , 
     umbral_mov_general NUMBER , 
     ind_mov_especial NUMBER , 
     umbral_mov_especial NUMBER , 
     ind_resid_mov NUMBER , 
     umbral_resid_mov NUMBER , 
     ind_patrimonio NUMBER , 
     umbral_patrimonio NUMBER , 
     renta NUMBER , 
     capital_mobiliario NUMBER , 
     volumen_negocio NUMBER , 
     cod_repesca VARCHAR2 (10) , 
     deducciones NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_economicos__IDX ON uji_becas.bc2_economicos 
    ( 
     solicitante_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos__UN UNIQUE ( solicitante_id ) ;




ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


DROP VIEW UJI_BECAS.BC2_V_RESOL_ECONOMICOS;

create or replace force view UJI_BECAS.BC2_V_RESOL_ECONOMICOS
(
   HJID
 , ECONCAPITAL
 , ECONCODREPESCA
 , ECONCOEFPRELACION
 , ECONDEDUCCIONES
 , ECONIMPMODBECARIO
 , ECONIMPMODCOMPEN
 , ECONIMPMODMOVLESPC
 , ECONIMPMODMOVLGEN
 , ECONIMPMODRESID
 , ECONIMPMODTASAS
 , ECONIMPMODI
 , ECONINDICEB
 , ECONINDICEC
 , ECONINDICEE
 , ECONINDICEI
 , ECONINDICEM
 , ECONINDICER
 , ECONINDICET
 , ECONNEGOCIO
 , ECONRENTA
) as
   select e.hjid
        , e.econcapital
        , e.econcodrepesca
        , e.econcoefprelacion
        , e.econdeducciones
        , e.econimpmodbecario
        , e.econimpmodcompen
        , e.econimpmodmovlespc
        , e.econimpmodmovlgen
        , e.econimpmodresid
        , e.econimpmodtasas
        , e.econimpmodi
        , e.econindiceb
        , e.econindicec
        , e.econindicee
        , e.econindicei
        , e.econindicem
        , e.econindicer
        , e.econindicet
        , e.econnegocio
        , e.econrenta
     from uji_becas_ws_multienvio.economicostype e;


ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (carnet_familia_numerosa  VARCHAR2(100));

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (ccaa_familia_numerosa_id  NUMBER);

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (fecha_fin_familia_numerosa  DATE);

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (fecha_orfandad  DATE);


CREATE INDEX uji_becas.bc2_solicitantes_ccaa_IDX ON uji_becas.bc2_solicitantes 
    ( 
     ccaa_familia_numerosa_id ASC 
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_ccaa_FK FOREIGN KEY 
    ( 
     ccaa_familia_numerosa_id
    ) 
    REFERENCES uji_becas.bc2_ccaa 
    ( 
     id
    ) 
;


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_complementos  NUMBER                 DEFAULT 0);

 
 
/* como generar los datos 

      select distinct 'update uji_becas.bc2_ext_personas_estudios set creditos_complementos = '||replace(crd_complementos,',','.')||' where  persona_id = '|| persona_id|| ' and estudio_id = '|| estudio_id || ';' txt
      from   (select pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
                     gra_pop.crd_complementos_pop (m.pexp_per_id, m.pexp_pmas_id) crd_complementos
              from   pop_matriculas m,
                     pop_tipos_matricula tm,
                     pop_est_matriculas em
              where  m.curso_aca = tm.curso_aca
              and    m.ptm_id = tm.id
              and    m.pexp_per_id = em.pmat_pexp_per_id(+)
              and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
              and    m.curso_aca = em.pmat_curso_aca(+)
              union all
              select exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
                     gra_exp.crd_complementos (e.per_id, e.tit_id) crd_complementos
              from   exp_expedientes e,
                     exp_matriculas m,
                     exp_tipos_matricula tm,
                     exp_est_matriculas em
              where  e.per_id = m.exp_per_id
              and    e.tit_id = m.exp_tit_id
              and    m.tma_id = tm.id
              and    m.curso_aca = tm.curso_aca
              and    m.exp_per_id = em.mat_exp_per_id(+)
              and    m.exp_tit_id = em.mat_exp_tit_id(+)
              and    m.curso_aca = em.mat_curso_aca(+))
      where  crd_complementos <> 0;


*/ 


update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 87785 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 16 where  persona_id = 110202 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 205082 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 214608 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 206505 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 117418 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 84247 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 164440 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 23414 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 195098 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 4059 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 6054 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 83614 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 137679 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 212949 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 64136 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 207719 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 107 where  persona_id = 62329 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 218648 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 137347 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 60557 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206837 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214011 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 197899 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 215450 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 242327 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 215868 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206523 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 227875 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 203624 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 36 where  persona_id = 216548 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 89289 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 60109 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 23112 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 117188 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 174 where  persona_id = 241871 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60.1 where  persona_id = 76255 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 206790 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 54 where  persona_id = 231456 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 240208 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 234648 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 234588 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 151551 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240788 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 239660 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 184816 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 110657 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 252218 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 164656 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 35 where  persona_id = 83670 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 240844 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 182077 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 279257 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 165212 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 50 where  persona_id = 161223 and estudio_id = 42120;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 242640 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 275898 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 131608 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 4.5 where  persona_id = 361422 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 283138 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 22.2 where  persona_id = 143364 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 446806 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 184516 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20.8 where  persona_id = 165135 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 384009 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 429403 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 4234 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 176103 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 442665 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 251099 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 425024 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 189852 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 285018 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 2504 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 471104 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 21.3 where  persona_id = 212936 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 55614 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 177064 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 209404 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 450227 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 357463 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 19.7 where  persona_id = 252823 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 155139 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 7.4 where  persona_id = 463325 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 232939 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 142877 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 85301 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 88082 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 93738 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 136394 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 140278 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 97.5 where  persona_id = 140279 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 165323 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 165404 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 196792 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 148832 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 149589 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 151258 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 102212 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 152897 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163901 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 107778 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 192038 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 293240 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 218691 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 17607 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 61.5 where  persona_id = 117371 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 7731 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 62288 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 153068 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 206888 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 214651 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 216455 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 234869 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 16971 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 156508 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 9942 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 75732 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 116361 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 96995 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 7493 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91 where  persona_id = 196598 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60.75 where  persona_id = 20850 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 34 where  persona_id = 16181 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 24 where  persona_id = 57321 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 205960 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 137496 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214317 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 19602 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210914 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214016 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 68 where  persona_id = 91222 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 217 where  persona_id = 195198 and estudio_id = 42004;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214013 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 151219 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 17056 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 76 where  persona_id = 103762 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 23986 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 11838 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210621 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 81 where  persona_id = 195460 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 13411 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 231464 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 232690 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 56439 and estudio_id = 42011;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 103804 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 247228 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240150 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 163987 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 89609 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 257681 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 57343 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 89047 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 78 where  persona_id = 239049 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 240081 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 164233 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 160897 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 237850 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231808 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 235908 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251679 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 259477 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 16758 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 127899 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 89228 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 23386 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 240939 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 237948 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 107263 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 195530 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251681 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251713 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 295757 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 252556 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 378142 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 63.6 where  persona_id = 165312 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 16769 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 250649 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 4.5 where  persona_id = 344917 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 273917 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251379 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 391521 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 228893 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 422359 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 250779 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 136194 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 21.4 where  persona_id = 179403 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 419646 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 217361 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 448426 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20.1 where  persona_id = 218581 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 476229 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 497948 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 196438 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 496848 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 190350 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 105 where  persona_id = 1343 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 7312 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 8282 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64.5 where  persona_id = 9228 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 94.5 where  persona_id = 13652 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 24036 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 85320 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 89787 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 91607 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 127357 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 140269 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 114 where  persona_id = 140272 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 152250 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 164049 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 165065 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 118.5 where  persona_id = 196793 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 143053 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 149249 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 151203 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 87 where  persona_id = 107739 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 94.5 where  persona_id = 136729 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 194138 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 217222 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 102529 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 50 where  persona_id = 14624 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 242330 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 206728 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 169848 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 9180 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 224548 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 108431 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 215568 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 214688 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 14236 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 16927 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 123 where  persona_id = 94255 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 14117 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 211399 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 219208 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 23835 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 137988 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 200264 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 8960 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 196854 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 168068 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 86 where  persona_id = 214073 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 217217 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 108 where  persona_id = 23666 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 48 where  persona_id = 224934 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 239578 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 194261 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 220608 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214009 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 104 where  persona_id = 214313 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 48 where  persona_id = 237715 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 215251 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 230028 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235610 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 231502 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 241362 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 61697 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 76228 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 234721 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 235065 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 239370 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 240080 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 232050 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 103.5 where  persona_id = 90179 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 90419 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 235490 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251719 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231709 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 252207 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251541 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 230551 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 32 where  persona_id = 63223 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 324337 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 9882 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 137151 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 148.5 where  persona_id = 281017 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 244926 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 239727 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 62923 and estudio_id = 42113;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 14308 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 152644 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 111578 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 391678 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 430781 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20.1 where  persona_id = 386435 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 239128 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 429845 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 151456 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20.4 where  persona_id = 76455 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 239805 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 1030 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 136327 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 214016 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 442106 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 431421 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 475905 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 116258 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 445546 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 449773 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 430181 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 178495 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 470783 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 170549 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 441225 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 504353 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 123 where  persona_id = 1440 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 23154 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 23311 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 60879 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 62391 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 62450 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 83428 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 89645 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 93735 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 127360 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 109.5 where  persona_id = 140268 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163947 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163986 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 164171 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 178377 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 154828 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 85.5 where  persona_id = 155125 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 390964 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 210123 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 137204 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 114 where  persona_id = 224394 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 73 where  persona_id = 61401 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 214693 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 83931 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 214368 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 44 where  persona_id = 35026 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 217472 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 87615 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 182927 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 194798 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 198068 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 211410 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 2490 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 60109 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 17622 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 213310 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 136423 and estudio_id = 42022;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 93347 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 107 where  persona_id = 215208 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 61.5 where  persona_id = 181941 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 206760 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 137197 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 195461 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 196618 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 135682 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 235888 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 230808 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 185716 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 231469 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240641 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 264977 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 196620 and estudio_id = 42004;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 56325 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 197142 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 220090 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 241991 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 234912 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231490 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 169524 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 184139 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 89261 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 13437 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 240000 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 230948 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 1837 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 11345 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 124629 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251697 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251663 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 257957 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64.5 where  persona_id = 164226 and estudio_id = 42125;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 239532 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231503 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 235712 and estudio_id = 42025;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 258217 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 137264 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 234774 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 137002 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 350482 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 152867 and estudio_id = 42113;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 205820 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240672 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 169119 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 272497 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 281097 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 343477 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 117147 and estudio_id = 42130;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 209756 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 383668 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 419912 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 263917 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 4.5 where  persona_id = 110377 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 473444 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 178636 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 469783 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 103339 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 447006 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 205313 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 239757 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 233195 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 10029 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 20829 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 75907 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 76234 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 93775 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 102606 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 127843 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 140597 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 183758 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 198829 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 151252 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 161622 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 107701 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 2386 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214327 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 12622 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 219228 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 4256 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214015 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 137458 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 137818 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 110409 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 216130 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 235528 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 102403 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 9 where  persona_id = 72989 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 89850 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 52 where  persona_id = 23805 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206522 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 141603 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206502 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 211316 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 198502 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 16632 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 100615 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 205280 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 118588 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 211403 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 14512 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 217872 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 206822 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 58663 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 24 where  persona_id = 110610 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 230951 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 5378 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 24 where  persona_id = 75885 and estudio_id = 42001;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210820 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 211317 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 207308 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 14447 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 207579 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 154710 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 214012 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 220088 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 211397 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 939 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 90030 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 165207 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 21 where  persona_id = 232593 and estudio_id = 42026;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 198066 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210777 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 88764 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 137892 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 161695 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 229648 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 240128 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 254409 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251943 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 107068 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 231995 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 139875 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 231449 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235303 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 240664 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251664 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 181657 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 133608 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 78 where  persona_id = 231914 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 183718 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231468 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 232278 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 174 where  persona_id = 117500 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 9621 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 232000 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 165055 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251977 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240114 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 12919 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 128672 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 163731 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 448049 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 136889 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 419285 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 377782 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 110474 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 430841 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 152674 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 116842 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 441525 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 19.6 where  persona_id = 210197 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 21.3 where  persona_id = 419392 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 17050 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 209049 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 20725 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 81 where  persona_id = 2508 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 4896 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 85.5 where  persona_id = 5661 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 8207 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 13140 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 14107 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 14446 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 16930 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 58637 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 89631 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 127344 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 165577 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 196921 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 95464 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 170437 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 178560 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 154549 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 155426 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 103547 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 103638 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 107013 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 97.5 where  persona_id = 107709 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 103.5 where  persona_id = 117771 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 378082 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 42 where  persona_id = 34946 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 17020 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 118019 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 211420 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 82 where  persona_id = 194298 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 90036 and estudio_id = 42025;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 110 where  persona_id = 211229 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 6957 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64 where  persona_id = 191459 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 9984 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 210782 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 234911 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 61239 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64 where  persona_id = 186518 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 60051 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 46 where  persona_id = 99553 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 62696 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 182924 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 8670 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 176955 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 89434 and estudio_id = 42026;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210822 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 76032 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 1547 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 61137 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 194483 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 108 where  persona_id = 93456 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 97319 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 102465 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 102468 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206521 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 214710 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 114 where  persona_id = 207973 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 234715 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 5587 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 44926 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 65326 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 8000 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 3709 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 118505 and estudio_id = 42025;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 138252 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 116667 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 90447 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = .75 where  persona_id = 206282 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 228868 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 59900 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 76084 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210812 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 90978 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 84285 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 36 where  persona_id = 206821 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 10268 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 23344 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 57360 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 234488 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 227588 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 107313 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 163564 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240060 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 118740 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235061 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 240282 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 250260 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251665 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 304417 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 136268 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251233 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235669 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 248372 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 26.2 where  persona_id = 138595 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 234408 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 237755 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 165306 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 234569 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251388 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 144 where  persona_id = 239664 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 255497 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 251950 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 232770 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 241812 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 281077 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 85 where  persona_id = 256637 and estudio_id = 42120;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 238929 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 161014 and estudio_id = 42113;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 373382 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 341781 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 10 where  persona_id = 356962 and estudio_id = 42130;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 208604 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 11218 and estudio_id = 42113;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 169538 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 6202 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 443425 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 443951 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 470663 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 446811 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 471403 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 478085 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 89830 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 209808 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 473185 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 500712 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 103811 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 178485 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 475344 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 493465 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 492068 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 230808 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 476233 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 15406 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 129 where  persona_id = 15538 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 17167 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 103.5 where  persona_id = 59810 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 75939 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 93197 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 93739 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 116586 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 127353 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 138247 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 140270 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163935 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 164250 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 182126 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 191779 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 209565 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 150853 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 112.5 where  persona_id = 95095 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 105 where  persona_id = 154832 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 161097 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 192019 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 90438 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 210053 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 211314 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 221809 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 136983 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 219069 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 131747 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 6481 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 137360 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 217211 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 207762 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 215808 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 102070 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 62 where  persona_id = 55126 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 16906 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 210732 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 94054 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 61854 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 151123 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 213332 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 93443 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 9016 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 140271 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 118026 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 136931 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 108 where  persona_id = 211426 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 109990 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206501 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 168481 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214548 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 94169 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 169074 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 114 where  persona_id = 224937 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 164769 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 207459 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64 where  persona_id = 16721 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 207200 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 164234 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 210821 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 214650 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 12294 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 211395 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 229289 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 224938 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 220068 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 1447 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 218628 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 83688 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 231496 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 231748 and estudio_id = 42025;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 48 where  persona_id = 239662 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 250711 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 64.5 where  persona_id = 17607 and estudio_id = 42125;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 50 where  persona_id = 93307 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231528 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 235244 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206804 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 122 where  persona_id = 170841 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 89402 and estudio_id = 42108;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 234772 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 231454 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 122 where  persona_id = 137729 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 232648 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 281117 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 281857 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 214720 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58.5 where  persona_id = 283218 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 390938 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 22 where  persona_id = 136292 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 138096 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 419426 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 440728 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 420457 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 179544 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 152367 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 6 where  persona_id = 439425 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 390316 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 197482 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 193309 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 210012 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 3397 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 108 where  persona_id = 8316 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 115.5 where  persona_id = 10509 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 112.5 where  persona_id = 13275 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 121.5 where  persona_id = 59976 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 76069 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 91502 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 103246 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 121.5 where  persona_id = 140266 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 242047 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 196920 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 142901 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 168808 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 156602 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163697 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 163880 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 386074 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 97.5 where  persona_id = 91259 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 210153 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 115.5 where  persona_id = 217587 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 233167 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 48 where  persona_id = 215530 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 211391 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 114 where  persona_id = 224393 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 184140 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 163779 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 58 where  persona_id = 8226 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 10499 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 35 where  persona_id = 210610 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 89 where  persona_id = 217468 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 214668 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 110 where  persona_id = 215579 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 206520 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 74 where  persona_id = 214069 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 194878 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 170030 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 217476 and estudio_id = 42026;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 53 where  persona_id = 16693 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 2 where  persona_id = 250694 and estudio_id = 42101;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 205599 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 103660 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 244019 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 56 where  persona_id = 191498 and estudio_id = 42006;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 11899 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 12 where  persona_id = 169469 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 24 where  persona_id = 14193 and estudio_id = 42007;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 9879 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 66 where  persona_id = 211424 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 110 where  persona_id = 213328 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235808 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 230030 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 136909 and estudio_id = 42005;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 102598 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 61.5 where  persona_id = 116870 and estudio_id = 42008;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 228871 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 110 where  persona_id = 210701 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 75 where  persona_id = 116410 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 208577 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 207380 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 191360 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 36 where  persona_id = 90050 and estudio_id = 42009;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 218728 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 131615 and estudio_id = 42002;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 152292 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 220289 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 240022 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 189520 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 207542 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 90 where  persona_id = 207543 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84 where  persona_id = 102963 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 75783 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 118875 and estudio_id = 42015;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 231108 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 231457 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 118.5 where  persona_id = 232272 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 234916 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 11310 and estudio_id = 42013;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 76496 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 4205 and estudio_id = 42030;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 240079 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 240061 and estudio_id = 42027;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 120 where  persona_id = 143145 and estudio_id = 42025;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 235809 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 250554 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 72 where  persona_id = 240678 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 230788 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 84.6 where  persona_id = 163985 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 241982 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 170320 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 80 where  persona_id = 239658 and estudio_id = 42016;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 240685 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 77.5 where  persona_id = 103070 and estudio_id = 42018;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 60 where  persona_id = 249067 and estudio_id = 42003;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 271877 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 252182 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 276299 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 240920 and estudio_id = 42116;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 70 where  persona_id = 65395 and estudio_id = 42113;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 30 where  persona_id = 137743 and estudio_id = 42127;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 359582 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 5 where  persona_id = 388675 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 269857 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 45 where  persona_id = 6578 and estudio_id = 42109;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 375982 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 18 where  persona_id = 138479 and estudio_id = 42135;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 259697 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 198947 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 439765 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 419901 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 133726 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 418077 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 209074 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 420279 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 189764 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 430641 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 443487 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 420149 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 477207 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 209391 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 192276 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 15 where  persona_id = 58692 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 3 where  persona_id = 234353 and estudio_id = 42111;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 20 where  persona_id = 473983 and estudio_id = 42114;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 9889 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 99 where  persona_id = 17130 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 20960 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 21125 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 102 where  persona_id = 83690 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 97.5 where  persona_id = 140271 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 100.5 where  persona_id = 140273 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 97.5 where  persona_id = 140275 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 164144 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 179480 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 179594 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 182358 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 96 where  persona_id = 150885 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 106.5 where  persona_id = 154541 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 160854 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 161581 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 115.5 where  persona_id = 107699 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 103.5 where  persona_id = 107708 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 103.5 where  persona_id = 107777 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 110221 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 111 where  persona_id = 126679 and estudio_id = 9;
update uji_becas.bc2_ext_personas_estudios set creditos_complementos = 91.5 where  persona_id = 137753 and estudio_id = 9;

-- Actualizar datos de familia numerosa
DECLARE
  CURSOR SOLICITANTES IS
    SELECT DISTINCT C.IDENTIFICACION, C.CODIGO_ARCHIVO_TEMPORAL, S.* 
    FROM BC2_SOLICITANTES S, BC2_BECAS C
    WHERE C.SOLICITANTE_ID = S.ID AND
      C.CONVOCATORIA_ID IN (1,2);
      
BEGIN
  FOR rsoli in solicitantes LOOP
    
    UPDATE BC2_SOLICITANTES
      set (carnet_familia_numerosa, ccaa_familia_numerosa_id, fecha_fin_familia_numerosa, fecha_orfandad) =
      (select SISOCARNETFAMNUM, SISOCCAAFAMNUM, SISOFECHAFINFAMNUMITEM, SISOFECHAFALLULTPROGITEM
       from uji_becas_ws_solicitudes.situacionsolicitud ss
       where ss.identificacion = rsoli.identificacion AND
             ss.codigo_archivo_temporal = rsoli.codigo_archivo_temporal
              )
    where id = rsoli.id;
  END LOOP;
  
  commit;
END;
 
 
 