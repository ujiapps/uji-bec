Insert into UJI_BECAS.BC2_ORGANISMOS 
 (ID, NOMBRE, WEBSERVICE)
 Values
   (3, 'GVA Altres', NULL);
   
   Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (7, 3, 1, 'CS', 'GVA Sobrevenides');
   
  commit;
  
  INSERT INTO UJI_BECAS.BC2_DENEGACIONES (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA) 
VALUES (5827048,'GVA Altres',3,1,'10',1,null);

Insert into UJI_BECAS.BC2_CUANTIAS
   (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO)
 Values
   (46, 'Tasas GVA Sobrevenidas', 7, 1, 0);
         
Insert into UJI_BECAS.BC2_CUANTIAS_POR_CURSO
   (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE)
 Values
   (hibernate_sequence.nextval, 46, 2012, 0);
   
   --insert de solicitantes para un curso_academico con becas sobrevenidas concedidas


insert into bc2_solicitantes s  (ID, CURSO_ACADEMICO_ID, PERSONA_ID) 
(select hibernate_sequence.nextval, 2012,x.id
 from         (select id
                     from gri_per.per_personas
                      where  identificacion in ( '53380738S', '45799710V', '20472863B','53875317W','53785341W','45797625W','20482488E', 'X1613449E','24477332L', 'X4712787H', '20468854G', '72791891L','20468850T','73595128G',
                                                    '73099972S', '73398552D', '20903405Q','20492653K','44798427Q','53378841G','20484794M', '18971349Y','44528005M',
                                                    '53723393Q', '20478682B', '53726180C','53728291S','20489041C','20056081N','21005479Q', '20912794K','53725236L', '71522644G', '20473378C','46075025E','26751148D',
                                                    '20903385L', '44532852E' ,'03150037A' ,'53663132S','73592457R','19011702V','20853536B','73396695S','19005291T','20490137N','20490326V','33450720A' ,
                                                    '20484669H','20486262R','20483150V','44889775P','73944695V','45804097B','20464237X','20494186J','20485148Z','X4687589M','X4937187F','45802616W'
                                                  )) x
 where x.id  not in  (select persona_id from bc2_solicitantes where curso_academico_id = 2012) )                                                              

DROP TABLE BC2_TMP_SOBREVENIDAS_DENEGADAS CASCADE CONSTRAINTS;

CREATE TABLE BC2_TMP_SOBREVENIDAS_DENEGADAS
(
  NIF                VARCHAR2(30 BYTE),
  NOMBRE             VARCHAR2(100 BYTE),
  CENTRO             VARCHAR2(100 BYTE),
  TITULACION         VARCHAR2(100 BYTE),
  MOTIVO_DENEGACION  VARCHAR2(2000 BYTE)
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

-- insertamos los datos

SET DEFINE OFF;
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73396442S', 'ALBALATE CHALER, SUSANA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Administración de Empresas', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20904938P', 'ALBERT RABAZA, CRISTINA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Agroalimentaria y del Medio Rural', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73396995Q', 'ALEDON PITARCH, BELÉN', 'Fac. CC Humanas y  Sociales', 'Licenciado en Psicopedagogía', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45804925B', 'ARJONA FERNÁNDEZ-SARABIA, CLARA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Criminología y Seguridad', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53382234Q', 'ARROYO ALBERT, MARÍA JESÚS', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('18992404Q', 'AVILA MARTÍN, ANTONIO', 'Fac. CC de la Salud', 'Licenciado en Psicología', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20492724T', 'BALDRES RAMOS, MIGUEL', 'Fac. CC Jurídicas y Económicas', 'Diplomado en Turismo', 'No cursar los estudios comprendidos en la base 1ª de la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20047118L', 'BANYULS FUSTER, GLÒRIA', 'Fac. CC Humanas y  Sociales', 'Graduado en Traducción e Interpretación', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53729458D', 'BAREA MOLÉS, ANA', 'Fac. CC de la Salud', 'Graduado en Psicología', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53729915Y', 'BARRUÉ VICENT, JOSEP', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Mecánica', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45802450C', 'BAYARRI MORENO, HELENA', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20482043Z', 'BENAGES GRIFO, LYDIA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería en Diseño Industrial y Desarrollo de Productos', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('44522393M', 'BONO IBORRA, VICTOR VICENTE', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería en Diseño Industrial y Desarrollo de Productos', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20903723N', 'CABALLER GARRETA, MARTA', 'Fac. CC de la Salud', 'Licenciado en Psicología', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('47820764S', 'CALDUCH JIMENEZ, LUCÍA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X4837626J', 'CARASEL , LUIZIANA', 'Fac. CC Humanas y  Sociales', 'Graduado en Traducción e Interpretación', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20464461G', 'CAUSANILLES DOMINGUEZ, MAURO', 'Fac. CC Humanas y  Sociales', 'Graduado en Comunicación Audiovisual', 'Disfrutar de beca del Ministerio de Educación, Cultura y Deporte u otra beca incompatible, para los mismos estudios.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X3086666C', 'CHAREF , OUAHIBA', 'Fac. CC de la Salud', 'Licenciado en Psicología', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X8751323F', 'CIOCARLAN , ELENA ALEXANDRA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X7600644H', 'CIUREA , BIANCA COSTINELA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Finanzas y Contabilidad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53384538C', 'COMES DEVÍS, JOSÉ', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20493935S', 'CONESA MILIÁN, LAURA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Química', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X3829511B', 'CONTRERAS RECANATTI, MATÍAS', 'E.S. Tecnología y CC Experimentales', 'Ingeniero Industrial', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('24221599T', 'CORRAL FERNANDEZ, FRANCISCO', 'Fac. CC de la Salud', 'Graduado en Psicología', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53381822H', 'CUBERTORER GALAN, DAVID', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Eléctrica', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53725616P', 'CUBERTORER GALAN, SANDRA', 'E.S. Tecnología y CC Experimentales', 'Licenciado en Química', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X5032960P', 'EL GHOUFAIRI , BOUCHRA', 'Fac. CC Jurídicas y Económicas', 'Licenciado en Derecho', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X4088061H', 'ES SAADY EL QORCHY, MOHAMED', 'Fac. CC Jurídicas y Económicas', 'Graduado en Finanzas y Contabilidad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45801220D', 'ESCOBÍ GAMON, CAROLINA', 'Fac. CC Humanas y  Sociales', 'Licenciado en Psicopedagogía', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('26759443R', 'ESTELLES GILABERT, MIRIAM TSUWANO', 'Fac. CC Humanas y  Sociales', 'Graduado en Humanidades: Estudios Interculturales', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20488419L', 'FERRER MESEGUER, VANESA ROMINA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Turismo', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20900685X', 'FONTANET MANZANEQUE, JUAN', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Agroalimentaria y del Medio Rural', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X3814806A', 'GARSIA RODRIGUES, YELIZAVETA', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('26751124P', 'GONZÁLEZ FERNÁNDEZ, JESSICA', 'Fac. CC Humanas y  Sociales', 'Graduado en Periodismo', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53726663C', 'GONZÁLEZ PERIS, RUT', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('33470112Y', 'GONZÁLEZ TRAMOYERES, SONIA', 'Fac. CC Humanas y  Sociales', 'Graduado en Publicidad y Relaciones Públicas', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X4700000L', 'GRIGORE , ALEXANDRA - HANA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Finanzas y Contabilidad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45799607Y', 'IBORRA TORRES, CARLA', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Primaria', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53792568F', 'JIMÉNEZ ARIAS, LUISA FERNANDA', 'Fac. CC Humanas y  Sociales', 'Licenciado en Publicidad y Relaciones Públicas', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X9999398X', 'JUCIUTE -, VAIDA', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Infantil', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73399653Y', 'LUIS LOPEZ, DAVID', 'Fac. CC Humanas y  Sociales', 'Licenciado en Humanidades', 'Estar propuesto para concesión de beca de la Generalitat (Orden 1/2013, de la Conselleria de Cultura, Educación y Deporte) para los mismos estudios');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20248843B', 'MAÑES FUSTER, CARLOS JAVIER', 'Fac. CC Jurídicas y Económicas', 'Graduado en Administración de Empresas', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53663409Q', 'MARQUÉS NAVARRO, ADRIAN', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53662830N', 'MARTI ORTELLS, MARIA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53258696B', 'MASIP RIBES, LARA', 'Fac. CC Humanas y  Sociales', 'Graduado en Traducción e Interpretación', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X5908447T', 'MESHOULI NAIT ABDI, HASNA', 'E.S. Tecnología y CC Experimentales', 'Ingeniero Químico', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20903468X', 'MINGUEZ ARBONIES, ALFONSO DIEGO', 'Fac. CC Jurídicas y Económicas', 'Graduado en Turismo', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20478301K', 'MONSERRAT PALACIOS, ANAIS', 'Fac. CC Jurídicas y Económicas', 'Graduado en Turismo', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53384615M', 'MONTOLIO CABEZUELO, MARIA CARME', 'Fac. CC de la Salud', 'Licenciado en Psicología', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73399491M', 'MONTOLIO ESTEBAN, JOSE MARIA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Agroalimentaria y del Medio Rural', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('48596351L', 'MORENO BAEZA, BÁRBARA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería en Diseño Industrial y Desarrollo de Productos', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45931202H', 'MUÑOZ MENDIOLA, JESSICA', 'Fac. CC de la Salud', 'Graduado en Psicología', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20480122W', 'NAVARRO ARTESERO, GABRIEL', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Primaria', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20492480D', 'NAVARRO CUARTIELLA, TARAY', 'Fac. CC Humanas y  Sociales', 'Graduado en Periodismo', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X8076878Z', 'NITA , MARIA MONICA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Relaciones Laborales y Recursos Humanos', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('24397601Y', 'ODEH TORRO, SAMY', 'Fac. CC Jurídicas y Económicas', 'Graduado en Economía', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('45910043L', 'OLIVA NAVARRO, ROSA BELÉN', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20907034B', 'ORDAZ TENA, MARTA MARÍA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Derecho', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20493204C', 'OSSORIO CASQUERO, LAURA', 'Fac. CC Humanas y  Sociales', 'Graduado en Comunicación Audiovisual', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X5315959S', 'PAL , IOANA RAMONA', 'Fac. CC Jurídicas y Económicas', 'Licenciado en Administración y Dirección de Empresas', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('19010425M', 'PAUNER MESEGUER, PRISCILA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Relaciones Laborales y Recursos Humanos', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('X7978624Q', 'POPA -, GEORGIANA MIRUNA', 'Fac. CC Jurídicas y Económicas', 'Licenciado en Derecho', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20489037Q', 'PRADES MONFORT, MARTA', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Infantil', 'Disfrutar de beca del Ministerio de Educación, Cultura y Deporte u otra beca incompatible, para los mismos estudios.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73399344L', 'PUIG GARGALLO, ALBERTO', 'Fac. CC Jurídicas y Económicas', 'Graduado en Finanzas y Contabilidad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73396599B', 'PUIG GUEROLA, SANDRO', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería Informática', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('26749161T', 'RAHMATI BARBERÁ, SORAYA', 'Fac. CC Humanas y  Sociales', 'Graduado en Humanidades: Estudios Interculturales', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('78583162M', 'REVERTÉ FIBLA, AIDA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Criminología y Seguridad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53788921V', 'RIBES FERRER, ALEJANDRO', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Primaria', 'Disfrutar de beca del Ministerio de Educación, Cultura y Deporte u otra beca incompatible, para los mismos estudios.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20489123X', 'RODRIGUEZ MOLINER, SANDRA', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería en Tecnologías Industriales', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20493492D', 'RODRÍGUEZ ORTEGA, SILVIA', 'Fac. CC Humanas y  Sociales', 'Licenciado en Humanidades', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53052122T', 'ROSADO ORQUIN, SANTIAGO PABLO', 'Fac. CC Humanas y  Sociales', 'Graduado en Periodismo', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20900920S', 'ROYO VIZCAÍNO, SOFÍA', 'Fac. CC Humanas y  Sociales', 'Graduado en Estudios Ingleses', 'Las circunstancias alegadas no se incluyen en las previstas en la base 3ª');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20909303A', 'SÁBADO NICOLAU, SARA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Administración de Empresas', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53227393B', 'SALES SERRA, M DOLORES', 'E.S. Tecnología y CC Experimentales', 'Graduado en Arquitectura Técnica', 'Por superar el umbral de renta y/o patrimonio establecido en la convocatoria');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('21702553Y', 'SANCHEZ SALES, MARIA', 'Fac. CC Jurídicas y Económicas', 'Graduado en Finanzas y Contabilidad', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20905428S', 'STEPNOWSKA MACIEJKO, ADRIANA', 'Fac. CC Humanas y  Sociales', 'Graduado en Historia y Patrimonio', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('20486245F', 'SUAREZ MEDINA, ANGEL', 'E.S. Tecnología y CC Experimentales', 'Graduado en Matemática Computacional', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53727699K', 'TORRES LARA, AINARA', 'Fac. CC de la Salud', 'Graduado en Enfermería', 'Circunstancias alegadas anteriores al año 2012');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('53761797X', 'YÉBENES COLORADO, JOSÉ MIGUEL', 'E.S. Tecnología y CC Experimentales', 'Graduado en Ingeniería en Diseño Industrial y Desarrollo de Productos', 'No acreditar documentalmente las circunstancias sobrevenidas y el cumplimiento de los requisitos para la concesión de la ayuda, de acuerdo con lo previsto en la base 7ª, punto 3, de la convocatoria.');
Insert into UJI_BECAS.BC2_TMP_SOBREVENIDAS_DENEGADAS
   (NIF, NOMBRE, CENTRO, TITULACION, MOTIVO_DENEGACION)
 Values
   ('73398533J', 'ZARAGOZA AGUILAR, ADRIAN', 'Fac. CC Humanas y  Sociales', 'Graduado en Maestro en Educación Primaria', 'Circunstancias alegadas anteriores al año 2012');
COMMIT;



--- Insertamos los solicitantes que faltan

  insert into bc2_solicitantes s  (ID, CURSO_ACADEMICO_ID, PERSONA_ID) 
  (select  hibernate_sequence.nextval,2012,x.id
   from    (select id, identificacion,apellido1||' '||apellido2||','||p.nombre
            from gri_per.per_personas p, bc2_tmp_sobrevenidas_denegadas  d
             where p.identificacion = d.nif) x
 where x.id  not in  (select persona_id from bc2_solicitantes where curso_academico_id = 2012) ) ;         
 
-- insert becas  Concedidas

insert into bc2_becas (ID, SOLICITANTE_ID, CONVOCATORIA_ID, PROCESO_ID, ESTADO_ID,TANDA_ID, TIPO_IDENTIFICACION_ID, IDENTIFICACION, ESTUDIO_ANT, beca_concedida)
(
   select  hibernate_sequence.nextval,soli.solicitante,'7',1,5,null,soli.tipo_iden, soli.identificacion, estudio_id, 1
   from 
       (select soli.id persona, s.id solicitante,identificacion, decode(upper(substr(soli.identificacion,1,length(soli.identificacion)-1)), lower(substr(soli.identificacion,1,length(soli.identificacion)-1)),1,2) tipo_iden
        from gri_per.per_personas soli, bc2_solicitantes s
        where  identificacion in ( '53380738S', '45799710V', '20472863B','53875317W','53785341W','45797625W','20482488E', 'X1613449E','24477332L', 'X4712787H', '20468854G', '72791891L','20468850T','73595128G',
                                 '73099972S', '73398552D', '20903405Q','20492653K','44798427Q','53378841G','20484794M', '18971349Y','44528005M',
                                  '53723393Q', '20478682B', '53726180C','53728291S','20489041C','20056081N','21005479Q', '20912794K','53725236L', '71522644G', '20473378C','46075025E','26751148D',
                                  '20903385L', '44532852E' ,'03150037A' ,'53663132S','73592457R','19011702V','20853536B','73396695S','19005291T','20490137N','20490326V','33450720A' ,
                                   '20484669H','20486262R','20483150V','44889775P','73944695V','45804097B','20464237X','20494186J','20485148Z','X4687589M','X4937187F','45802616W'
                                                  )
            and soli.id = s.persona_id
            and s.curso_academico_id = 2012) soli,
     bc2_ext_personas_estudios pe
where soli.persona = pe.persona_id
  and pe.curso_academico_id = 2012
  and estudio_id <500
  and estudio_id = decode(soli.identificacion,'24477332L',205,estudio_id)
)  ;


---- INSERT AYUDA 46 ( TASAS BECAS SOBREVENIDAS)
              
insert into bc2_becas_cuantias  (ID, BECA_ID, CUANTIA_ID, IMPORTE)
(select hibernate_sequence.nextval, b.id,46,0
  from bc2_becas b
  where convocatoria_id = 7
    and beca_concedida = '1'
    and solicitante_id in (select id from bc2_solicitantes where curso_academico_id = 2012));

--- Insertamos las Becas sobrevenidas denegadas

insert into bc2_becas (ID, SOLICITANTE_ID, CONVOCATORIA_ID, PROCESO_ID, ESTADO_ID,TANDA_ID, TIPO_IDENTIFICACION_ID, IDENTIFICACION, ESTUDIO_ANT, beca_concedida,observaciones_uji)
(
   select hibernate_sequence.nextval,  soli.solicitante,'7',1,6,null,soli.tipo_iden, soli.identificacion, decode(soli.solicitante,44517,4, estudio_id), 0,motivo_denegacion
   from 
       (select soli.id persona, s.id solicitante,identificacion, decode(upper(substr(soli.identificacion,1,length(soli.identificacion)-1)), lower(substr(soli.identificacion,1,length(soli.identificacion)-1)),1,2) tipo_iden,motivo_denegacion
        from gri_per.per_personas soli, bc2_solicitantes s,bc2_tmp_sobrevenidas_denegadas  d
        where  soli.identificacion = d.nif
            and soli.id = s.persona_id
            and s.curso_academico_id = 2012) soli,
     bc2_ext_personas_estudios pe
where soli.persona = pe.persona_id
  and pe.curso_academico_id = 2012
  and estudio_id = decode(soli.solicitante,  40478,7, estudio_id));


----- insertamos denegacion

Insert into UJI_BECAS.BC2_BECAS_DENEGACIONES
   (ID, DENEGACION_ID, BECA_ID, ORDEN_DENEGACION)
 (select hibernate_sequence.nextval, 5827048, id,1
 from bc2_becas 
 where convocatoria_id =7 
   and beca_concedida = 0
   and solicitante_id in ( select id from bc2_solicitantes where curso_academico_id = 2012);
   );
   
   Commit;

   -- Convocatoria Altres Comunitats
  Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (8, 1, 1, 'MA', 'Altres Comunitats');
   
   
   -- modificacion vista para extraer las becas de comunidades con competencias transferidas
   DROP VIEW BC2_EXT_IMPORTES_MEC;

/* Formatted on 27/01/2014 13:15:35 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW BC2_EXT_IMPORTES_MEC
(
   PERSONA_ID,
   SOLICITANTE_ID,
   BECA_ID,
   CURSO_ACADEMICO_ID,
   CONVOCATORIA_ID,
   PROCESO_ID,
   ESTADO_ID,
   UT,
   CONV,
   NIF,
   APELLIDO1,
   APELLIDO2,
   NOMBRE,
   TIPO_FAMILIA_ID,
   TASAS_TOTAL,
   TASAS_MINISTERIO,
   TASAS_CONSELLERIA,
   CREDITOS_CUBIERTOS,
   CURSO_ACA,
   ESTUDIO_ID
)
AS
   (SELECT   s.persona_id,
             b.solicitante_id,
             b.id beca_id,
             S.CURSO_ACADEMICO_ID,
             B.CONVOCATORIA_ID,
             b.proceso_id,
             b.estado_id,
             '40' ut,
             c.acronimo conv,
             B.IDENTIFICACION nif,
             m.apellido1,
             m.apellido2,
             m.nombre,
             s.tipo_familia_id,
             e.tasas_total,
             e.tasas_ministerio,
             e.tasas_conselleria,
             e.creditos_cubiertos,
             s.curso_academico_id || '/' || (s.curso_academico_id + 1)
                curso_aca,
             e.estudio_id
      FROM   bc2_becas b,
             bc2_convocatorias c,
             bc2_miembros m,
             bc2_solicitantes s,
             bc2_ext_personas_estudios e
     WHERE       b.convocatoria_id = c.id
             AND c.organismo_id = 1
             AND b.solicitante_id = m.solicitante_id
             AND m.tipo_miembro_id = 1
             AND b.solicitante_id = s.id
             AND s.persona_id = e.persona_id
             AND b.estudio_id = e.estudio_id
             AND s.CURSO_ACADEMICO_id = e.curso_academico_id
             AND beca_concedida = 1
             and convocatoria_id in (1,2));
             
 --  BECAS comunidades transferidas
 DROP VIEW BC2_EXT_OTRAS_COMUNIDADES;
CREATE OR REPLACE FORCE VIEW BC2_EXT_OTRAS_COMUNIDADES
(
   PERSONA_ID,
   SOLICITANTE_ID,
   BECA_ID,
   CURSO_ACADEMICO_ID,
   CONVOCATORIA_ID,
   PROCESO_ID,
   ESTADO_ID,
   UT,
   CONV,
   NIF,
   APELLIDO1,
   APELLIDO2,
   NOMBRE,
   TIPO_FAMILIA_ID,
   TASAS_TOTAL,
   TASAS_MINISTERIO,
   TASAS_CONSELLERIA,
   CREDITOS_CUBIERTOS,
   CURSO_ACA,
   ESTUDIO_ID,
   CONCEDIDA,
   PROVINCIA_ID,
   PROVINCIA_NOMBRE
)
AS
   (SELECT   s.persona_id,
             b.solicitante_id,
             b.id beca_id,
             S.CURSO_ACADEMICO_ID,
             B.CONVOCATORIA_ID,
             b.proceso_id,
             b.estado_id,
             '40' ut,
             c.acronimo conv,
             B.IDENTIFICACION nif,
             m.apellido1,
             m.apellido2,
             m.nombre,
             s.tipo_familia_id,
             e.tasas_total,
             e.tasas_ministerio,
             e.tasas_conselleria,
             e.creditos_cubiertos,
             s.curso_academico_id || '/' || (s.curso_academico_id + 1)
                curso_aca,
             e.estudio_id,
             decode(b.beca_concedida , 1,'S','N'),             
             d.provincia_id ,
             p.nombre
      FROM   bc2_becas b,
             bc2_convocatorias c,
             bc2_miembros m,
             bc2_solicitantes s,
             bc2_ext_personas_estudios e,
             bc2_domicilios d,
             bc2_ext_provincias p
     WHERE       b.convocatoria_id = c.id
             AND c.organismo_id = 1
             AND b.solicitante_id = m.solicitante_id
             AND m.tipo_miembro_id = 1
             AND b.solicitante_id = s.id
             AND s.persona_id = e.persona_id
             AND b.estudio_id = e.estudio_id
             AND s.CURSO_ACADEMICO_id = e.curso_academico_id
             AND b.convocatoria_id = 8
             AND b.solicitante_id = d.solicitante_id
             AND d.tipo_domicilio_id = 10
             AND d.provincia_id = p.id);
------ Vista errores

             DROP VIEW BC2_V_ERRORES_BECA_MATRICULA;

/* Formatted on 28/01/2014 12:16:47 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW BC2_V_ERRORES_BECA_MATRICULA
(
   PERSONA_ID,
   ESTUDIO_ID,
   CURSO_ACADEMICO,
   ERROR,
   ERROR_ID
)
AS
   SELECT   exp_per_id persona_id,
            exp_tit_id estudio_id,
            curso_aca curso_academico,
            'Circunstancia becario en matricula y no existe beca' error,
            1 error_id
     FROM   ( (SELECT   exp_per_id, exp_tit_id, curso_aca
                 FROM   exp_matriculas
                WHERE   (exp_per_id, exp_tit_id, curso_aca) IN
                              (SELECT   mat_exp_per_id,
                                        mat_exp_tit_id,
                                        mat_curso_aca
                                 FROM   exp_mat_circunstancias
                                WHERE   cir_id = 2)
                        AND curso_aca >= 2012
               UNION
               SELECT   pexp_per_id, pexp_pmas_id, curso_aca
                 FROM   pop_matriculas
                WHERE   (pexp_per_id, pexp_pmas_id, curso_aca) IN
                              (SELECT   pmat_pexp_per_id,
                                        pmat_pexp_pmas_id,
                                        pmat_curso_aca
                                 FROM   pop_mat_circunstancias
                                WHERE   cir_id = 2)
                        AND curso_aca >= 2012)
             MINUS
             SELECT   s.persona_id, b.estudio_id, s.curso_academico_id --, b.proceso_id, b.estado_id, b.beca_concedida
               FROM   bc2_solicitantes s, bc2_becas b
              WHERE   s.id = b.solicitante_id
                      AND s.curso_academico_id >= 2012)
   UNION ALL
   SELECT   persona_id,
            estudio_id,
            curso_academico_id,
            'Exite beca concedida y no hay circunstancia en matricula',
            2
     FROM   (SELECT   s.persona_id, b.estudio_id, s.curso_academico_id --, b.proceso_id, b.estado_id, b.beca_concedida
               FROM   bc2_solicitantes s, bc2_becas b
              WHERE       s.id = b.solicitante_id
                      AND s.curso_academico_id >= 2012
                      AND beca_concedida = 1
             MINUS
             (SELECT   exp_per_id, exp_tit_id, curso_aca
                FROM   exp_matriculas
               WHERE   (exp_per_id, exp_tit_id, curso_aca) IN
                             (SELECT   mat_exp_per_id,
                                       mat_exp_tit_id,
                                       mat_curso_aca
                                FROM   exp_mat_circunstancias
                               WHERE   cir_id = 2)
                       AND curso_aca >= 2012
              UNION
              SELECT   pexp_per_id, pexp_pmas_id, curso_aca
                FROM   pop_matriculas
               WHERE   (pexp_per_id, pexp_pmas_id, curso_aca) IN
                             (SELECT   pmat_pexp_per_id,
                                       pmat_pexp_pmas_id,
                                       pmat_curso_aca
                                FROM   pop_mat_circunstancias
                               WHERE   cir_id = 2)
                       AND curso_aca >= 2012))
   UNION ALL
   SELECT   persona_id,
            estudio_id,
            curso_academico_id,
            'Beca concedida y no existe matricula',
            3
     FROM   (SELECT   s.persona_id, b.estudio_id, s.curso_academico_id
               FROM   bc2_solicitantes s, bc2_becas b
              WHERE       s.id = b.solicitante_id
                      AND s.curso_academico_id >= 2012
                      AND beca_concedida = 1
             MINUS
             (SELECT   exp_per_id, exp_tit_id, curso_aca
                FROM   exp_matriculas
               WHERE   curso_aca >= 2012
              UNION
              SELECT   pexp_per_id, pexp_pmas_id, curso_aca
                FROM   pop_matriculas
               WHERE   curso_aca >= 2012))
   UNION ALL
   SELECT   s.persona_id,
            b.estudio_id,
            s.curso_academico_id,
            'Becas de MEC y CONS concedidas',
            4
     FROM   bc2_solicitantes s, bc2_becas b
    WHERE       s.id = B.SOLICITANTE_ID
            AND beca_concedida = 1
            AND convocatoria_id IN (SELECT   c.id
                                      FROM   bc2_convocatorias c
                                     WHERE   organismo_id = 1)
            AND s.id IN
                     (SELECT   bc.solicitante_id
                        FROM   bc2_becas bc
                       WHERE   beca_concedida = 1
                               AND convocatoria_id IN
                                        (SELECT   id
                                           FROM   bc2_convocatorias c
                                          WHERE   organismo_id = 2))
   UNION ALL
   SELECT   s.persona_id,
            b.estudio_id,
            s.curso_academico_id,
            'Beca concedida y estado pendiente',
            5
     FROM   bc2_solicitantes s, bc2_becas b
    WHERE       s.id = B.SOLICITANTE_ID
            AND beca_concedida = 1
            AND estado_id = 1
            AND proceso_id IN (1, 2, 4)
   UNION ALL
   SELECT   s.persona_id,
            b.estudio_id,
            s.curso_academico_id,
            'Beca denegada y proceso alegacion,',
            6
     FROM   bc2_solicitantes s, bc2_becas b
    WHERE   s.id = B.SOLICITANTE_ID AND beca_concedida = 0 AND proceso_id = 3
   UNION ALL
   SELECT   s.persona_id,
            b.estudio_id,
            s.curso_academico_id,
            'Beca denegada y proceso alegacion,',
            7
     FROM   bc2_solicitantes s, bc2_becas b
    WHERE   s.id = B.SOLICITANTE_ID AND beca_concedida = 0 AND proceso_id = 5;


GRANT SELECT ON BC2_V_ERRORES_BECA_MATRICULA TO CON_UJI_BEC;

             
