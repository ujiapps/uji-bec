ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (nota_media_ultimo_curso  NUMBER);

 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS
 ADD (persona_nombre  VARCHAR2(100));

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS
 ADD (persona_apellido1  VARCHAR2(100));

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS
 ADD (persona_apellido2  VARCHAR2(100));


declare
   cursor lista is
      select p.id, p.nombre, apellido1, apellido2
      from   bc2_ext_personas e,
             per_personas@alma1 p
      where  e.id = p.id;
begin
   for x in lista loop
      update bc2_ext_personas
      set persona_nombre = x.nombre,
          persona_apellido1 = x.apellido1,
          persona_apellido2 = x.apellido2
      where  id = x.id;
      commit;
   end loop;
end;

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (nota_media_ultimo_curso  NUMBER);


DROP VIEW BC2_EXT_IMPORTES_MEC;

/* Formatted on 10/07/2013 10:40:18 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW BC2_EXT_IMPORTES_MEC
(
   PERSONA_ID,
   SOLICITANTE_ID,
   BECA_ID,
   CURSO_ACADEMICO_ID,
   CONVOCATORIA_ID,
   PROCESO_ID,
   ESTADO_ID,
   UT,
   CONV,
   NIF,
   APELLIDO1,
   APELLIDO2,
   NOMBRE,
   TIPO_FAMILIA_ID,
   TASAS_TOTAL,
   TASAS_MINISTERIO,
   TASAS_CONSELLERIA,
   CREDITOS_CUBIERTOS,
   CURSO_ACA,
   ESTUDIO_ID
)
AS
   (SELECT   s.persona_id,
             b.solicitante_id,
             b.id beca_id,
             S.CURSO_ACADEMICO_ID,
             B.CONVOCATORIA_ID,
             b.proceso_id,
             b.estado_id,
             '40' ut,
             c.acronimo conv,
             B.IDENTIFICACION nif,
             m.apellido1,
             m.apellido2,
             m.nombre,
             s.tipo_familia_id,
             e.tasas_total,
             e.tasas_ministerio,
             e.tasas_conselleria,
             e.creditos_cubiertos,
             s.curso_academico_id || '/' || (s.curso_academico_id + 1)
                curso_aca,
             e.estudio_id
      FROM   bc2_becas b,
             bc2_convocatorias c,
             bc2_miembros m,
             bc2_solicitantes s,
             bc2_ext_personas_estudios e
     WHERE       b.convocatoria_id = c.id
             AND c.organismo_id = 1
             AND b.solicitante_id = m.solicitante_id
             AND m.tipo_miembro_id = 1
             AND b.solicitante_id = s.id
             AND s.persona_id = e.persona_id
             AND b.estudio_id = e.estudio_id
             AND s.CURSO_ACADEMICO_id = e.curso_academico_id
             AND (b.estado_id = 5 OR proceso_id = 3));

GRANT SELECT ON UJI_BECAS.BC2_EXT_IMPORTES_MEC TO CON_UJI_BEC;
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_total  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_ministerio  NUMBER);
 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_conselleria  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_cubiertos  NUMBER);


 
CREATE TABLE uji_becas.bc2_listados 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     url VARCHAR2 (4000) , 
     activo NUMBER DEFAULT 1  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_listados__IDX ON uji_becas.bc2_listados 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;

