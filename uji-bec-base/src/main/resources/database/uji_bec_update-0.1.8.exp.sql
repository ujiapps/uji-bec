CREATE TABLE uji_becas.bc2_becas_recursos 
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     texto_ayudas_reclamadas VARCHAR2 (4000) , 
     curso_ant_completo NUMBER , 
     curso_ant_creditos_matr NUMBER , 
     curso_ant_creditos NUMBER , 
     curso_ant_nota_media NUMBER , 
     curso_ant_creditos_pend NUMBER , 
     curso_ant_creditos_pend_max NUMBER , 
     curso_act_curso_posterior NUMBER , 
     curso_act_curso_posterior_txt VARCHAR2 (4000) , 
     curso_act_curso_completo NUMBER , 
     curso_act_creditos_matr NUMBER , 
     curso_act_creditos NUMBER , 
     curso_act_anios_becario NUMBER , 
     curso_act_perdida_curso NUMBER , 
     curso_Act_perdida_curso_txt VARCHAR2 (4000) , 
     ingresos_familiares NUMBER , 
     deducciones NUMBER , 
     renta_familiar_disponible NUMBER , 
     renta_familiar_per_capita NUMBER , 
     porcentaje_incremento_umbral NUMBER , 
     supera_umbrales NUMBER , 
     supera_umbrales_texto VARCHAR2 (4000) , 
     supera_umbrales_cuantia NUMBER , 
     distancia VARCHAR2 (4000) , 
     existe_centro_mas_cercano NUMBER , 
     circunstancias_transporte VARCHAR2 (4000) , 
     otras_becas NUMBER , 
     otras_circunstancias VARCHAR2 (4000) , 
     texto_propuesta VARCHAR2 (4000) , 
     texto_justificacion VARCHAR2 (4000) 
    ) 
;


CREATE INDEX uji_becas.bc2_becas_recursos__IDX ON uji_becas.bc2_becas_recursos 
    ( 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_becas_recursos 
    ADD CONSTRAINT bc2_becas_reclamaciones_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_becas_recursos 
    ADD CONSTRAINT bc2_becas_recl_becas_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;



ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (tipo_titulacion  VARCHAR2(10));


DECLARE
  CURSOR cbecas IS
    SELECT * FROM bc2_becas
      WHERE convocatoria_id IN (1,2);

  vtitusol_id number;
  vtipo_titu  varchar2(10);
BEGIN

  FOR rbecas IN cbecas LOOP
    IF UPPER(rbecas.codigo_archivo_temporal) != 'PAIS VASCO' AND
       UPPER(rbecas.codigo_archivo_temporal) != 'BECA PAIS VASCO' THEN
      BEGIN
          SELECT titulacion_solicitudtype_hjid
            INTO vtitusol_id
            FROM uji_becas_ws_solicitudes.solicitudes
          WHERE UPPER(identificacion) = UPPER(rbecas.identificacion) AND
                codigo_archivo_temporal = rbecas.codigo_archivo_temporal;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN vtitusol_id := NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20000, rbecas.identificacion || ' - ' || sqlerrm);
      END;

      BEGIN
        IF vtitusol_id IS NOT NULL THEN
          SELECT titucodtipotit
            INTO vtipo_titu
            FROM uji_becas_ws_solicitudes.titulaciontype
            WHERE hjid = vtitusol_id;

          UPDATE bc2_becas
            SET tipo_titulacion = vtipo_titu
         WHERE id = rbecas.id;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
      END;

      vtitusol_id := null;
    END IF;
  END LOOP;

  COMMIT;
END;


ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (miembros_computables  NUMBER);

 
ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (causas_desestimacion  VARCHAR2(4000));
 
 ALTER TABLE UJI_BECAS.BC2_BECAS_RECURSOS
 ADD (curso_ant_estudio  VARCHAR2(4000));

 