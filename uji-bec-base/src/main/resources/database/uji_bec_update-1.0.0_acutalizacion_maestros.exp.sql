update bc2_cursos_academicos set activo = 0 where id = 2012;
insert into bc2_cursos_academicos values (2013,1);
commit;


Insert into UJI_BECAS.BC2_TIPOS_MINUSVALIA
   (ID, NOMBRE, ORDEN)
 Values
   (4, '80', 3);
COMMIT;

ALTER TABLE UJI_BECAS.BC2_TIPOS_MONEDA
MODIFY(CODIGO VARCHAR2(2));

Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (1, '01', 'Dirham Marruecos');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (2, '02', 'Dolar EE.UU');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (3, '03', 'Euro');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (4, '04', 'Leu Rumania');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (5, '05', 'Lev Bulgaria');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (6, '06', 'Libra Gibraltar');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (7, '07', 'Libra Inglaterra');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (8, '08', 'Peso Argentino');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (9, '09', 'Peso Chile');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (10, '10', 'Peso Mexico');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (11, '11', 'Real Brasil');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (12, '12', 'Sucre Ecuador');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (13, '13', 'Yuan China');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (14, '14', 'Zloty Polonia');
   
Insert into UJI_BECAS.BC2_TIPOS_MONEDA
   (ID, CODIGO, NOMBRE)
 Values
   (99, '99', 'Otros ');
   
COMMIT;

 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (33,'Beca Básica', '1','1','4801');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (34,'Cuantía Fija Ligada a la Residencia', '1','1','4901');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (35,'Ayuda errónea', '1','1','9999');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (36,'Ayuda de regularización', '1','1','9998');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (37,'Cuantía Fija Ligada a la Renta', '1','1','4601');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (38,'Variable Mínima', '1','1','4701');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (39,'Variable por Coeficiente', '1','1','4702');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (40,'Av1-(Lanz,Fuert,Gom,Hierro,La Palma,Menorc,Pitius)', '1','1','0701');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (41,'Av2-(Tenerife,Las Palmas,Mallorca,Ceuta y Melilla)', '1','1','0702');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (42,'Av4-(Lanz,Fuert,Gom,Hierro,La Palma/Península)', '1','1','0704');
 INSERT INTO  bc2_cuantias  (ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO) values  (43,'Av5-(Tenerife, Las Palmas G.C./Península)', '1','1','0705');

COMMIT;
update bc2_cuantias set activa = 0
where codigo not in ('4801',    '4901' ,    '9999' ,    '9998' ,    '4601' ,    '4701' ,    '4702',    '0701',    '0702',    '0704',    '0705' ,    '0901','0902','0903')
      and convocatoria_id <> 3;

 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59043,(select c.id from bc2_cuantias c where codigo = '4801'), 2013,'200');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59044,(select c.id from bc2_cuantias c where codigo = '4901'), 2013,'1500');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59045,(select c.id from bc2_cuantias c where codigo = '9999'), 2013,'0');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59046,(select c.id from bc2_cuantias c where codigo = '9998'), 2013,'0');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59047,(select c.id from bc2_cuantias c where codigo = '4601'), 2013,'1500');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59048,(select c.id from bc2_cuantias c where codigo = '4701'), 2013,'60');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59049,(select c.id from bc2_cuantias c where codigo = '4702'), 2013,'9999');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59050,(select c.id from bc2_cuantias c where codigo = '0701'), 2013,'623');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59051,(select c.id from bc2_cuantias c where codigo = '0702'), 2013,'442');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59052,(select c.id from bc2_cuantias c where codigo = '0704'), 2013,'937');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59053,(select c.id from bc2_cuantias c where codigo = '0705'), 2013,'888');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59054,(select c.id from bc2_cuantias c where codigo = '0902'), 2013,'0');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59055,(select c.id from bc2_cuantias c where codigo = '0903'), 2013,'0');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59056,(select c.id from bc2_cuantias c where codigo = '0901'), 2013,'0');
 INSERT INTO  bc2_cuantias_por_curso (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE) values  (59057,(select c.id from bc2_cuantias c where codigo = '0'), 2013,'0');
 
 COMMIT;