-- Ya ejecutado
delete bc2_denegaciones;
delete bc2_becas_denegaciones;

Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (133, 'Tenir concedit el màxim de quanties que us corresponen en funció dels estudis o modalitat de matrícula.', 1, 1, '10', 
    '01', '92');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (134, 'Haver gaudit de beca per al mateix nivell o superior al dels ensenyaments per als que solicita la ajuda', 1, 1, '10', 
    '01', '91');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (138, 'Ajuda no compatible amb la realització del Projecte de Final de carrera per no constituir una assignatura del pla d¿estudis o amb estudis de màster universitari oficial per no ser de primer curs.', 1, 1, '10', 
    '01', '30');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (140, 'No estar matriculat o matriculada per ensenyament oficial en el curs actual en la totalitat d¿assignatures o crèdits que us resten per finalitzar el segon cicle d¿ensenyament universitari o d¿estudis de grau, i en el cas de màster, en la totalitat d¿assignatures o crèdits de primer curs.', 1, 1, '10', 
    '01', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (139, 'Repetir curs.', 1, 1, '10', 
    '01', '29');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (137, 'No reunir els requisits establits en l''article 4. 1. d) del Reial decret 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes a l''estudi personalitzades (BOE de 17 de gener).', 1, 1, '10', 
    '01', '71');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (136, 'Haver sigut transferides les beques per a estudis universitaris i superiors no universitaris a la comunitat autònoma del País Basc, on té veïnat administratiu la persona sol·licitant.', 1, 1, '10', 
    '01', '76');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (141, 'No reunir els requisits establits en l¿article 4.1.c) del Reial decret 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes a l¿estudi personalitzades (BOE de 17 de gener del 2008).', 1, 1, '10', 
    '01', '78');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (142, 'Sol·licitud duplicada.', 1, 1, '10', 
    '01', '81');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (143, 'Renúncia.', 1, 1, '10', 
    '01', '82');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (144, 'Discordança entre la identificació de l¿alumne o alumna registrat en la seu electrònica i la de la persona sol·licitant.', 1, 1, '10', 
    '01', '84');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (145, 'Els ingressos anuals són inferiors a les despeses de residència del curs acadèmic.', 1, 1, '10', 
    '01', '86');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (146, 'Per haver-se comprovat inexactitud en les dades.', 1, 1, '10', 
    '01', '88');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (147, 'Les quanties de beques i ajudes així com dels seus diferents components no seran en cap cas superiors al cost real de la prestació o prestacions que cobrisquen.', 1, 1, '10', 
    '01', '93');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (148, 'No reunir els requisits acadèmics mínims per ser beneficiari o beneficiària d¿altres quanties de beca.', 1, 1, '10', 
    '01', '94');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (149, 'Per haver inclòs com a membre computable de la unitat familiar una persona perceptora de renda bàsica d¿emancipació.', 1, 1, '10', 
    '02', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (150, 'No haver-se pogut determinar els ingressos de la unitat familiar.', 1, 1, '10', 
    '02', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (151, 'No haver certificat el resum de la declaració de la renda o certificat d¿imputacions.', 1, 1, '10', 
    '02', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (152, 'No tenir totalment aprovat el curs requerit.
', 1, 1, '10', 
    '03', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (153, 'No haver superat el primer cicle i/o les assignatures o crèdits del segon cicle establits en la convocatòria actual.', 1, 1, '10', 
    '03', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (156, 'No arribar l¿expedient acadèmic a la nota mitjana exigida.', 1, 0, '10', 
    '03', '18');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (157, 'No reunir els requisits establits en la resolució de la convocatòria per al supòsit de canvi d¿estudis amb condició de becari o becària.', 1, 0, '10', 
    '03', '23');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (158, 'Haver sigut beneficiari o beneficiària de beca el nombre màxim d¿anys que permet la Resolució de convocatòria.', 1, 0, '10', 
    '03', '25');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (159, 'Repetir curs.', 1, 1, '10', 
    '05', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (160, 'Per al Projecte de Final de carrera, no haver sigut becari o becària de la convocatòria general o beca de mobilitat del Ministeri d¿Educació en el curs anterior.', 1, 1, '10', 
    '05', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (161, 'Posseir títol o estar en disposició legal per a la seua obtenció del mateix o superior nivell al dels estudis per als quals se sol·licita la beca.', 1, 1, '10', 
    '05', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (162, 'No tenir totalment aprovat el curs anterior.', 1, 1, '10', 
    '05', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (101, 'No estar matriculat en el centre i/o estudis per als que sol·licita ajuda.', 1, 1, '10', 
    '01', '87');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (131, 'Superar el llindar de renda corresponent, establit en la convocatòria, per a la concessió d¿un altre component o d¿altres components de la beca.', 1, 1, '10', 
    '02', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (135, 'La beca o ajuda que sol·licita no és per a realitzar estudis d''un nivell superior al dels últims estudis cursats', 1, 1, '10', 
    '01', '85');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (102, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet la resolució de la convocatòria.', 1, 1, '10', 
    '03', '28');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (103, 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs.', 1, 1, '10', 
    '03', '29');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (104, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 1, 1, '10', 
    '03', '30');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (105, 'No arribar a la nota mitjana mínima exigida en la convocatòria.', 1, 1, '10', 
    '03', '31');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (106, 'No haver aprovat el nombre o percentatge mínim d''assignatures o crèdits establerts en les bases de la  convocatòria.', 1, 1, '10', 
    '03', '32');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (107, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet la resolució de la convocatòria.', 1, 1, '10', 
    '05', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (108, 'No arribar a la nota mitjana mínima exigida en la convocatòria.', 1, 1, '10', 
    '05', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (109, 'No haver aprovat el nombre o percentatge mínim d¿assignatures/matèries/mòduls/crèdits establits en les bases de la convocatòria.', 1, 1, '10', 
    '05', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (110, 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs.', 1, 1, '10', 
    '05', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (111, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 1, 1, '10', 
    '05', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (112, 'No quedar suficientment acreditada la composició de la unitat familiar.', 1, 1, '10', 
    '01', '61');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (132, 'No procedeix el component de Compensació per a la modalitat d''estudis en els quals està matriculat', 1, 1, '10', 
    '01', '77');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (47, 'Per superar els llindars de patrimoni, després de consultar les dades que sobre la unitat familiar té l¿administració tributària.', 1, 1, '10', 
    '04', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (51, 'Falta el titular principal de la declaración de la renta a la que pertenece el solicitante o algun familiar', 1, 1, '10', 
    '04', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (60, 'Por existir varias delcaraciones ante la AEAT con respecto algun miembro de la unidad familar', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (9, 'No complir l''edat permesa que s''estableix en la convocatòria.', 1, 1, '10', 
    '01', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (16, 'No trobar-se en situació legal d''atur', 1, 1, '10', 
    '01', '51');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (11, 'Existència de sol·licituds en les quals concorren circumstàncies per a una assignació preferent d''acord amb els criteris establits en les bases de la convocatòria.', 1, 1, '10', 
    '01', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (17, 'Per no estar en possessió de títol universitari', 1, 1, '10', 
    '01', '53');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (18, 'Per no acreditar el sol·licitant l''edat requerida en les bases de la corresponent convocatòria', 1, 1, '10', 
    '01', '54');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (19, 'Per no acreditar el sol·licitant la situació legal d''atur requerida en les bases de la corresponent convocatòria.', 1, 1, '10', 
    '01', '55');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (20, 'Per no acreditar el sol·licitant la situació de perceptor de prestacions d''atur requerida en les bases de la corresponent convocatòria.', 1, 1, '10', 
    '01', '56');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (21, 'Per no estar matriculat en estudis emparats per la corresponent convocatòria', 1, 0, '10', 
    '01', '57');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (22, 'No estar matriculat en el curs anterior en un curs complet de Màster oficial.', 1, 0, '10', 
    '01', '58');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (55, 'Per haver inclòs com membre computable de la seua unitat familiar a un perceptor de Renda Bàsica d''Emancipació.', 1, 1, '10', 
    '04', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (41, 'En el cas d''alumnat universitari que realitza estudis no presencials, no tenir el mínim de crèdits aprovats o matriculats.', 1, 0, '10', 
    '03', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (13, 'Haver causat baixa en el centre d''estudis abans de la finalització del curs.', 1, 1, '10', 
    '01', '31');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (14, 'Tenir els serveis coberts pel Govern de la comunitat autònoma', 1, 1, '10', 
    '01', '32');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (57, 'No está o no ha estado percibiendo prestación o subsidio por desempleo concargo al presupuestodel SPE a la fecha que exige la convocatória', 1, 1, '10', 
    '04', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (45, 'No estar matriculat en el curs actual en el segon curs dels mateixos estudis per als quals va obtenir beca el curs anterior', 1, 0, '10', 
    '03', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (56, 'No se encuentra en situación laboral de desempleo e inscrito como demandandate de empleo a fecha de la convocatória.', 1, 1, '10', 
    '04', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (63, 'Presumpta ocultació de membres computables de la seva família i dades econòmiques', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (58, 'Superar els llindars en acumulació de patrimoni, després de consultar les dades econòmiques de la seva família en la AEAT', 1, 1, '10', 
    '04', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (33, 'Per superar els llindars de valors cadastrals, després de consultar les dades que sobre la seva unitat familiar obren en poder de l''Administració tributària', 1, 1, '10', 
    '02', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (69, 'Supera valors cadastrals', 2, 1, '10', 
    '14', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (54, 'Per superar els llindars de valors cadastrals, després de consultar les dades que sobre la unitat familiar té l¿administració tributària.', 1, 1, '10', 
    '04', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (50, 'Superar el volum de negoci', 1, 1, '10', 
    '04', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (52, 'Falten familiars a efectes fiscals', 1, 1, '10', 
    '04', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (65, 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', 2, 1, '10', 
    '1', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (2, 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', 1, 1, '10', 
    '01', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (80, 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', 2, 1, '10', 
    '2', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (46, 'Per superar els llindars de renda, després de consultar les dades que sobre la unitat familiar té l¿administració tributària.', 1, 1, '10', 
    '04', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (85, 'Superar els llindars de renda establits per a la concesió de beca', 2, 1, '10', 
    '3.1', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (1, 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', 1, 1, '10', 
    '01', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (25, 'Superar els llindars de renda establits per a l''exempció de preus per serveis acadèmics o beca bàsica', 1, 1, '10', 
    '02', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (24, 'Superar els llindars establits per a la concessió de la beca', 1, 1, '10', 
    '02', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (26, 'No tindre dret a l''excepció de taxes i superar el primer llindar económic de renda disponible (cent.privat)', 1, 1, '10', 
    '02', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (37, 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', 1, 0, '10', 
    '03', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (87, 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', 2, 1, '10', 
    '4', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (38, 'No haver aprovat el nombre o percentatge mínim d¿assignatures/matèries/mòduls/crèdits establits en les bases de la convocatòria.', 1, 0, '10', 
    '03', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (88, 'No haver aprovat el nombre mínim d''assignatures o crèdits establits en les bases de la  convocatòria', 2, 1, '10', 
    '5', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (3, 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució d''aquesta, a pesar d''haver-se-li requerit', 1, 1, '10', 
    '01', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (89, 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució de la convocatòria, a pesar d''haver-se-li requerit', 2, 1, '10', 
    '6', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (90, 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes a l''estudi personalitzades (BOE de 17 de gener).', 2, 1, '10', 
    '7', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (36, 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', 1, 1, '10', 
    '02', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (39, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 1, 0, '10', 
    '03', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (40, 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs', 1, 0, '10', 
    '03', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (99, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 2, 1, '10', 
    '9.1', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (4, 'No reunir els requisits establits en la resolució de la convocatòria per al supòsit de canvi d¿estudis amb condició de becari o becària.', 1, 1, '10', 
    '01', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (66, 'Pèrdua de curs/os lectiu/s en el supòsit de canvi d''estudis cursats totalment o parcialment amb la condició debecari/ària.', 2, 1, '10', 
    '10', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (5, 'Gaudir d''ajuda o beca incompatible', 1, 1, '10', 
    '01', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (67, 'Gaudir d''ajuda o beca incompatible', 2, 1, '10', 
    '12', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (6, 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', 1, 1, '10', 
    '01', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (68, 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', 2, 1, '10', 
    '13', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (42, 'No haver-se matriculat d''un curs complet', 1, 1, '10', 
    '03', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (86, 'Superar els llindars de renda establits per a la concesió de beca', 2, 1, '10', 
    '3.2', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (27, 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en les bases  de la convocatòria.', 1, 1, '10', 
    '02', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (70, 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en l''ordre de convocatòria', 2, 1, '10', 
    '14.1', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (28, 'La facturació del negoci/activitat econòmica supera el llindar establit en les bases de la de convocatòria.', 1, 1, '10', 
    '02', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (71, 'La facturació del negoci/activitat econòmica supera el límit establit en l''ordre de convocatòria', 2, 1, '10', 
    '14.2', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (29, 'El valor cadastral de les finques rústiques supera els llindars patrimonials establits en les bases de la convocatòria.', 1, 1, '10', 
    '02', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (72, 'El valor cadastral de les finques rústiques  supera el límit establit en l''ordre de convocatòria', 2, 1, '10', 
    '14.3', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (30, 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en les bases de la  convocatòria.', 1, 1, '10', 
    '02', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (73, 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en l''ordre de convocatòria', 2, 1, '10', 
    '14.4', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (31, 'El conjunt d''elements patrimonials supera el límit establit en les bases de la convocatòria.', 1, 1, '10', 
    '02', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (74, 'La suma d''elements patrimonials supera el límit establit en l''ordre de convocatòria', 2, 1, '10', 
    '14.5', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (75, 'Per deduir-se de les dades que consten en el seu expedient, que el llindar patrimonial de la unitat familiar supera l''establit per a la concessió de la beca.', 2, 1, '10', 
    '15', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (44, 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', 1, 1, '10', 
    '03', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (10, 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', 1, 1, '10', 
    '01', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (77, 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', 2, 1, '10', 
    '17', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (7, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''ordre de convocatoria.', 1, 0, '10', 
    '01', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (78, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''orde de convocatòria', 2, 1, '10', 
    '18', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (8, 'No complir els requisits acadèmics exigits en l''ordre de convocatòria', 1, 1, '10', 
    '01', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (81, 'Per obtenir la beca en la convocatòria del Ministeri d''Educació i Ciència.', 2, 1, '10', 
    '27', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (82, 'Per no acreditar la residència en la CCVV amb anterioritat a la data de finalització del tèrmini', 2, 1, '10', 
    '28', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (84, 'Per haver renunciat o haver-li sigut anul·lada la matrícula en el present curs.', 2, 1, '10', 
    '30', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (100, 'No estar matriculat en el nombre mínim exigit de crèdits en aquest curs', 2, 1, '10', 
    '9.2', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (12, 'No tenir permís de residència, d¿acord amb el que estableix l¿article 1.2.d) de la resolució de convocatòria (BOE de 19 d¿agost).', 1, 1, '10', 
    '01', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (62, 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (49, 'Els NIF de la unitat familiar no estan identificats per la  AEAT', 1, 1, '10', 
    '04', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (64, 'L''Administració Tributària no posseeix dades econòmiques de la unitat familiar', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (61, 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (32, 'Per tindre l''obligació de presentar declaració per l''Impost Extraordinari sobre el Patrimoni, d''acord amb la normativa reguladora del dit impost.', 1, 1, '10', 
    '02', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (43, 'No estar matriculat en el curs següent segons el pla d''estudis vigent', 1, 1, '10', 
    '03', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (91, 'Superar la seva renda tots els llindars establerts (AEAT)', 2, 1, '10', 
    '81', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (92, 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', 2, 1, '10', 
    '82', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (93, 'Superar els llindars del cabdal mobiliari', 2, 1, '10', 
    '83', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (96, 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', 2, 1, '10', 
    '86', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (94, 'Els NIF de la unitat familiar no estan identificats per la  AEAT', 2, 1, '10', 
    '84', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (95, 'Els membres de la unitat familiar no estan identificats econòmicament per la  AEAT', 2, 1, '10', 
    '85', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (97, 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', 2, 1, '10', 
    '87', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (48, 'Per declarar-se independent la persona sol·licitant i no obstant això figurar en una declaració d¿IRPF com a descendent.', 1, 1, '10', 
    '04', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (98, 'Alumne independent', 2, 1, '10', 
    '88', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (76, 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', 2, 1, '10', 
    '16', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (83, 'Es deu presentar certificat de Renda de l''exercici anterior del membre indicat', 2, 1, '10', 
    '3', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (59, 'Ocultació de valors cadastrals.', 1, 1, '10', 
    '04', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (53, 'Per existir diverses declaracions davant l¿Agència Tributària', 1, 1, '10', 
    '04', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (79, 'Altres causes', 2, 1, '10', 
    '19', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (23, 'Pendiente de definr', 1, 0, '10', 
    '01', NULL);
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (15, 'No complir el requisit de trobar-se el o la sol·licitant o els seus sustentadors treballant a Espanya d''acord amb el que estableix l¿article 1.2.d) de la resolució de convocatòria (BOE de 19 d¿agost).', 1, 1, '10', 
    '01', '46');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (35, 'Superar els llindars de renda establits per a la concessió de la quantia fixa lligada a la renda.', 1, 1, '10', 
    '02', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (34, 'Per no coincidir la documentació aportada amb la facilitada per l¿administració tributària.', 1, 1, '10', 
    '02', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (115, 'La cuantia actual del aumento es menor que la de la solicitud o el aumento de cuantía anterior. Precisa proceso de Reintegro', 1, 1, '14', 
    '01', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (116, 'La cuantía actual del aumento es igual que la de la solicitud o el aumento de cuantía anterior', 1, 1, '14', 
    '01', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (117, 'Solicitud anterior en trámite', 1, 1, '14', 
    '01', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (118, 'No alcanzar la puntuación total obtenida por el último alumno seleccionado como suplente', 1, 1, '14', 
    '01', '24');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (119, 'No identificación de sus NIF, por las Agencias Tributarias', 1, 1, '14', 
    '04', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (120, 'No identificación económica por las Agencias Tributarias', 1, 1, '14', 
    '04', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (121, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, '14', 
    '04', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (122, 'Solicitud que debe ser tramitada sin la ayuda de residencia, al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, '14', 
    '04', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (123, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar', 1, 1, '14', 
    '04', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (124, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, '14', 
    '05', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (125, 'Solicitud que debe ser tramitada sin la ayuda de residencia, al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, '14', 
    '05', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (126, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la Comunidad Autónoma donde cursa estudios', 1, 1, '14', 
    '05', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (103423, 'No reunir els requisits establits en la resolució de la convocatòria per al supòsit de canvi d¿estudis amb condició de becari o becària.', 1, 1, '10', 
    '05', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (127, 'Tindre la ajuda màxima de desplaçament permitida segons la convocatòria', 1, 1, '10', 
    '01', '60');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (128, 'No complir el requisit per a la concessió del component de transport urbà', 1, 1, '10', 
    '01', '62');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (129, 'No complir els requisits per a l''adjudicació del component de residència', 1, 1, '10', 
    '01', '63');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, 
    CAUSA, SUBCAUSA)
 Values
   (130, 'No complir el requisit per a la concessió del component de desplaçament', 1, 1, '10', 
    '01', '75');
COMMIT;


--- Tabla para almacenar los masters con Ruct repetido.  Ya ejecutado

CREATE TABLE UJI_BECAS.bc2_diccionario_excepciones
(
  ID                  NUMBER                    NOT NULL,
  CURSO_ACADEMICO_ID  NUMBER                    NOT NULL,
  CLASE_UJI           VARCHAR2(1000 CHAR)       NOT NULL,
  ORGANISMO_ID        NUMBER                    NOT NULL,
  VALOR_ORIGEN        VARCHAR2(1000 CHAR)       NOT NULL,
  VALOR_UJI           NUMBER                    NOT NULL
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE UJI_BECAS.bc2_diccionario_excepciones ADD (
  CONSTRAINT   bc2_diccionario_excepciones_PK
 PRIMARY KEY
 (ID));

ALTER TABLE UJI_BECAS.bc2_diccionario_excepciones ADD (
  CONSTRAINT BC2_DIC_EXC_CURSOS_ACA_FK 
 FOREIGN KEY (CURSO_ACADEMICO_ID) 
 REFERENCES UJI_BECAS.BC2_CURSOS_ACADEMICOS (ID),
  CONSTRAINT BC2_DIC_EXC_ORG_FK 
 FOREIGN KEY (ORGANISMO_ID) 
 REFERENCES UJI_BECAS.BC2_ORGANISMOS (ID));



Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (1, 2013, 'Estudio', 2, '4311836', 
    42178);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (2, 2013, 'Estudio', 2, '4312378', 
    42180);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (3, 2013, 'Estudio', 2, '4312657', 
    42181);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (4, 2013, 'Estudio', 2, '4310436', 
    42177);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (5, 2013, 'Estudio', 2, '4310469', 
    42158);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (6, 2013, 'Estudio', 2, '4310366', 
    42156);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (7, 2013, 'Estudio', 2, '4310159', 
    42159);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (8, 2013, 'Estudio', 1, '4310157', 
    42157);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (9, 2013, 'Estudio', 1, '4310150', 
    42160);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (10, 2013, 'Estudio', 1, '4310161', 
    42166);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (11, 2013, 'Estudio', 1, '4310437', 
    42163);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (12, 2013, 'Estudio', 1, '4311737', 
    42154);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (13, 2013, 'Estudio', 1, '4311737', 
    42554);    
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (14, 2013, 'Estudio', 1, '4310153', 
    42155);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (15, 2013, 'Estudio', 1, '4310151', 
    42564);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (16, 2013, 'Estudio', 1, '4310151', 
    42164);    
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (17, 2013, 'Estudio', 1, '4310724', 
    42168);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (18, 2013, 'Estudio', 1, '4311733', 
    42162);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (19, 2013, 'Estudio', 1, '4311713', 
    42179);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (20, 2013, 'Estudio', 1, '4311734', 
    42169);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (21, 2013, 'Estudio', 1, '4310156', 
    42165);
Insert into UJI_BECAS.BC2_DICCIONARIO_EXCEPCIONES
   (ID, CURSO_ACADEMICO_ID, CLASE_UJI, ORGANISMO_ID, VALOR_ORIGEN, 
    VALOR_UJI)
 Values
   (22, 2013, 'Estudio', 1, '4310322', 
    42161);


COMMIT;




ALTER TABLE UJI_BECAS.BC2_BECAS
ADD (CODIGO_ESTUDIO_UJI_ANT VARCHAR2(100 BYTE));
