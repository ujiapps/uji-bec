Insert into BC2_CUANTIAS(ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO)
 Values(44, 'Tasas beca de matrícula', 1, 1, '0904');

Insert into BC2_CUANTIAS(ID, NOMBRE, CONVOCATORIA_ID, ACTIVA, CODIGO)
 Values(45, 'Variable-2 por Coeficiente', 1, 1, '4703');

Insert into BC2_CUANTIAS_POR_CURSO (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE)
 Values (hibernate_sequence.nextval, 44, 2013, 0);

Insert into BC2_CUANTIAS_POR_CURSO (ID, CUANTIA_ID, CURSO_ACADEMICO_ID, IMPORTE)
 Values (hibernate_sequence.nextval, 45, 2013, 0);
 
 Commit;
 
 
 DROP VIEW BC2_V_RESOL_BECAS_ESTADO;

/* Formatted on 14/01/2014 12:42:59 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW BC2_V_RESOL_BECAS_ESTADO
(
   MULTIENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TANDA,
   SOLICITUD_ID,
   BECA_ID,
   ARCHIVO_TEMPORAL,
   NIF,
   ESTADO_ID,
   ESTADO,
   FECHA_ESTADO,
   AEAT,
   ACOGIMIENTO_ID,
   PERSONALES_ID,
   DECLARANTE_ID,
   ECONOMICOS_ID,
   GENERALES_ID,
   SITUACION_SOLICITUD_ID,
   TITULACION_ID
)
AS
   SELECT   CR.MULTIENVIO_ID,
            CR.LOTE,
            CR.CONVOCATORIA,
            CR.CURSO_ACADEMICO,
            S.SOLILOTE,
            S.HJID SOLICITUD_ID,
            S.SOLIIDSOLICITUD BECA_ID,
            SOLISQCIUDADANO ARCHIVO,
            S.SOLINIFCUENTA,
            SOLICODESTADOACT,
            es.estado,
            SOLIFECHAESTADOACTITEM,
            S.SOLIINDAEAT,
            S.ACOGIMIENTO_SOLICITUDTYPE_HJ_0,
            S.DATOSPERSONALES_SOLICITUDTYP_0,
            S.DECLARANTE_SOLICITUDTYPE_HJID,
            S.ECONOMICOS_SOLICITUDTYPE_HJID,
            S.GENERALES_SOLICITUDTYPE_HJID,
            S.SITUACIONSOLICITUD_SOLICITUD_0,
            S.TITULACION_SOLICITUDTYPE_HJID
     FROM   UJI_BECAS.BC2_V_RESOL_CAB_MULTIENVIO CR,
            UJI_BECAS_WS_MULTIENVIO.ENVIOTYPEORDENPAGOANDERROROR_0 E,
            UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE S,
            UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD ES
    WHERE       CR.ENVIO_ID = E.ORDENPAGOANDERRORORSOLICITUD_1
            AND E.ITEMSOLICITUD_ENVIOTYPEORDEN_0 = S.HJID
            AND CR.CONVOCATORIA = S.SOLICODCONV
            AND CR.CURSO_ACADEMICO = S.SOLIIDCURSO
            AND s.SOLICODESTADOACT = es.id;

DROP VIEW UJI_BECAS.BC2_V_RESOL_ECONOMICOS;

create or replace force view uji_becas.bc2_v_resol_economicos
(
   id
 , multienvio_id
 , lote
 , convocatoria
 , curso_academico
 , tanda
 , solicitud_id
 , beca_id
 , archivo_temporal
 , nif
 , econcapital
 , econcodrepesca
 , econcoefprelacion
 , econdeducciones
 , econimpmodbecario
 , econimpmodcompen
 , econimpmodmovlespc
 , econimpmodmovlgen
 , econimpmodresid
 , econimpmodtasas
 , econimpmodi
 , econindiceb
 , econindicec
 , econindicee
 , econindicei
 , econindicem
 , econindicer
 , econindicet
 , econnegocio
 , econrenta
) as
   select e.hjid
        , be.multienvio_id
        , be.lote
        , be.convocatoria
        , be.curso_academico
        , be.tanda
        , be.solicitud_id
        , be.beca_id
        , be.archivo_temporal
        , be.nif
        , e.econcapital
        , e.econcodrepesca
        , e.econcoefprelacion
        , e.econdeducciones
        , e.econimpmodbecario
        , e.econimpmodcompen
        , e.econimpmodmovlespc
        , e.econimpmodmovlgen
        , e.econimpmodresid
        , e.econimpmodtasas
        , e.econimpmodi
        , e.econindiceb
        , e.econindicec
        , e.econindicee
        , e.econindicei
        , e.econindicem
        , e.econindicer
        , e.econindicet
        , e.econnegocio
        , e.econrenta
     from uji_becas.bc2_v_resol_becas_estado be, uji_becas_ws_multienvio.economicostype e
    where be.economicos_id = e.hjid;

DROP VIEW BC2_V_BECAS_SIN_MATRICULA;
CREATE OR REPLACE FORCE VIEW BC2_V_BECAS_SIN_MATRICULA
(
   PERSONA_ID,
   ESTUDIO_ID,
   CURSO_ACADEMICO_ID
)
AS
   SELECT   s.persona_id, b.estudio_id, s.curso_academico_id
     FROM   bc2_becas b, bc2_solicitantes s
    WHERE   b.solicitante_id = S.ID AND beca_concedida = 1
   MINUS
   (SELECT   exp_per_id, exp_tit_id, curso_aca
      FROM   gra_exp.exp_matriculas m
     WHERE   curso_aca >=2012 
    UNION
    SELECT   pexp_per_id, pexp_pmas_id, curso_aca
      FROM   gra_pop.pop_matriculas m
     WHERE   curso_aca >=2012 
   );

 
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_REVOCAR (DNI,
                                                            ALUMNO,
                                                            PERSONA_ID,
                                                            CURSO_ACADEMICO_ID,
                                                            ESTUDIO_ID,
                                                            BECA_ID,
                                                            CONVOC,
                                                            CREDITOS_MATRICULADOS,
                                                            CREDITOS_SUPERADOS,
                                                            ESTADO_ID,
                                                            ESTADO
                                                           ) AS
   (select p.identificacion dni, p.NOMBRE alumno, s.persona_id, s.curso_academico_id, ps.estudio_id, b.id beca_id,
           acronimo convoc, ps.creditos_matriculados, ps.creditos_superados, estado_id, e.nombre estado
    from   bc2_becas b,
           bc2_ext_personas_estudios ps,
           bc2_solicitantes s,
           bc2_ext_personas p,
           bc2_convocatorias c,
           bc2_estados e
    where  s.id = b.solicitante_id
    and    s.curso_academico_id = ps.curso_academico_id
    and    s.persona_id = ps.persona_id
    and    P.ID = S.PERSONA_ID
    and    p.id = ps.persona_id
    and    b.estudio_id = ps.estudio_id
    and    b.convocatoria_id = c.id
    and    b.beca_concedida = 1
    --and    b.curso = 1
    and    ps.creditos_matriculados / 2 > ps.creditos_superados
    and    estado_id = e.id);
	
	
	