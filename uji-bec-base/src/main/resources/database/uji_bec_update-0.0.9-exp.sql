ALTER TABLE UJI_BECAS.BC2_BECAS
MODIFY(CUMPLE_ACADEMICOS  NOT NULL);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_ACCESO_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (PORC_SUPERADOS_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (COEF_CORRECTOR_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (PORC_RENDIMIENTO_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_RENDIMIENTO_MEC_1 NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_RENDIMIENTO_MEC_2  NUMBER);

UPDATE BC2_MINIMOS SET NOTA_ACCESO_MEC = 5.5
WHERE TIPO_ESTUDIO IN ('12C','G') ;

UPDATE BC2_MINIMOS SET NOTA_ACCESO_MEC = 6
WHERE TIPO_ESTUDIO ='M';

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 100
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 65
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA = 'TE');

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 80
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA IN ('SA','EX'));

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 90
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA IN ('SO','HU'));

UPDATE BC2_MINIMOS SET COEF_CORRECTOR_MEC = 1.17
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET COEF_CORRECTOR_MEC = 1
WHERE TIPO_ESTUDIO <> 'M';

UPDATE BC2_MINIMOS SET PORC_RENDIMIENTO_MEC = 85
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA = 'TE');

UPDATE BC2_MINIMOS SET PORC_RENDIMIENTO_MEC = 100
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA <>'TE');

UPDATE BC2_MINIMOS SET NOTA_RENDIMIENTO_MEC_1 = 8
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET NOTA_RENDIMIENTO_MEC_2 = 8.5
WHERE TIPO_ESTUDIO = 'M';

ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO
 ADD (CONVOCATORIA_ID  NUMBER);

UPDATE BC2_BECAS_HISTORICO
SET CONVOCATORIA_ID = 1;
 
ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO
MODIFY(CONVOCATORIA_ID  NOT NULL);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
MODIFY(TIPO_IDENTIFICACION_ID  NULL);

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (ESTADO  VARCHAR2(2));

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (CAUSA  VARCHAR2(5));

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (SUBCAUSA  VARCHAR2(2));
 
ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
MODIFY(CODIGO NULL);
 
DELETE FROM BC2_DENEGACIONES;
 
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('47', 'Superar els llindars de patrimoni, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('51', 'Falta el titular principal de la declaración de la renta a la que pertenece el solicitante o algun familiar', '1', 1, '10', '04', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('60', 'Por existir varias delcaraciones ante la AEAT con respecto algun miembro de la unidad familar', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('9', 'No complir l''edat permesa que s''estableix en la convocatòria.', '1', 1, '10', '01', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('16', 'No trobar-se en situació legal d''atur', '1', 1, '10', '01', '51');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('11', 'Existència de sol·licituds en les quals concorren circumstàncies per a una assignació preferent d''acord amb els criteris establits en les bases de la convocatòria.', '1', 1, '10', '01', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('17', 'Per no estar en possessió de títol universitari', '1', 1, '10', '01', '53');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('18', 'Per no acreditar el sol·licitant l''edat requerida en les bases de la corresponent convocatòria', '1', 1, '10', '01', '54');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('19', 'Per no acreditar el sol·licitant la situació legal d''atur requerida en les bases de la corresponent convocatòria.', '1', 1, '10', '01', '55');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('20', 'Per no acreditar el sol·licitant la situació de perceptor de prestacions d''atur requerida en les bases de la corresponent convocatòria.', '1', 1, '10', '01', '56');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('21', 'Per no estar matriculat en estudis emparats per la corresponent convocatòria', '1', 1, '10', '01', '57');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('22', 'No estar matriculat en el curs anterior en un curs complet de Màster oficial.', '1', 1, '10', '01', '58');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('55', 'Per haver inclòs com membre computable de la seua unitat familiar a un perceptor de Renda Bàsica d''Emancipació.', '1', 1, '10', '04', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('41', 'En el cas d''alumnat universitari que realitza estudis no presencials, no tenir el mínim de crèdits aprovats o matriculats.', '1', 1, '10', '03', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('13', 'Haver causat baixa en el centre d''estudis abans de la finalització del curs.', '1', 1, '10', '01', '31');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('14', 'Tenir els serveis coberts pel Govern de la comunitat autònoma', '1', 1, '10', '01', '32');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('57', 'No está o no ha estado percibiendo prestación o subsidio por desempleo concargo al presupuestodel SPE a la fecha que exige la convocatória', '1', 1, '10', '04', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('45', 'No estar matriculat en el curs actual en el segon curs dels mateixos estudis per als quals va obtenir beca el curs anterior', '1', 1, '10', '03', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('56', 'No se encuentra en situación laboral de desempleo e inscrito como demandandate de empleo a fecha de la convocatória.', '1', 1, '10', '04', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('63', 'Presumpta ocultació de membres computables de la seva família i dades econòmiques', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('58', 'Superar els llindars en acumulació de patrimoni, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('33', 'Per superar els llindars de valors cadastrals, després de consultar les dades que sobre la seva unitat familiar obren en poder de l''Administració tributària', '1', 1, '10', '02', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('69', 'Supera valors cadastrals', '2', 1, '10', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('54', 'Superar els llindars dels valors catastrals, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('50', 'Superar el volum de negoci', '1', 1, '10', '04', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('52', 'Falten familiars a efectes fiscals', '1', 1, '10', '04', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('65', 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', '2', 1, '10', '1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('2', 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', '1', 1, '10', '01', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('80', 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', '2', 1, '10', '2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('46', 'Superar els llindars de renda, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('85', 'Superar els llindars de renda establits per a la concesió de beca', '2', 1, '10', '3.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('1', 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', '1', 1, '10', '01', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('25', 'Superar els llindars de renda establits per a l''exempció de preus per serveis acadèmics o beca bàsica', '1', 1, '10', '02', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('24', 'Superar els llindars establits per a la concessió de la beca', '1', 1, '10', '02', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('26', 'No tindre dret a l''excepció de taxes i superar el primer llindar económic de renda disponible (cent.privat)', '1', 1, '10', '02', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('37', 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', '1', 1, '10', '03', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('87', 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', '2', 1, '10', '4');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('38', 'No haver aprovat el nombre mínim d''assignatures o crèdits establits en les bases de la  convocatòria', '1', 1, '10', '03', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('88', 'No haver aprovat el nombre mínim d''assignatures o crèdits establits en les bases de la  convocatòria', '2', 1, '10', '5');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('3', 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució d''aquesta, a pesar d''haver-se-li requerit', '1', 1, '10', '01', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('89', 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució de la convocatòria, a pesar d''haver-se-li requerit', '2', 1, '10', '6');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('90', 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', '2', 1, '10', '7');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('36', 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', '1', 1, '10', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('39', 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', '1', 1, '10', '03', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('40', 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs', '1', 1, '10', '03', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('99', 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', '2', 1, '10', '9.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('4', 'Pèrdua de curs/os lectiu/s en el supòsit de canvi d''estudis cursats totalment o parcialment amb la condició de becari/ària.', '1', 1, '10', '01', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('66', 'Pèrdua de curs/os lectiu/s en el supòsit de canvi d''estudis cursats totalment o parcialment amb la condició debecari/ària.', '2', 1, '10', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('5', 'Gaudir d''ajuda o beca incompatible', '1', 1, '10', '01', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('67', 'Gaudir d''ajuda o beca incompatible', '2', 1, '10', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('6', 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', '1', 1, '10', '01', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('68', 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', '2', 1, '10', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('42', 'No haver-se matriculat d''un curs complet', '1', 1, '10', '03', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('86', 'Superar els llindars de renda establits per a la concesió de beca', '2', 1, '10', '3.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('27', 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en les bases  de la convocatòria.', '1', 1, '10', '02', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('70', 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('28', 'La facturació del negoci/activitat econòmica supera el llindar establit en les bases de la de convocatòria.', '1', 1, '10', '02', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('71', 'La facturació del negoci/activitat econòmica supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('29', 'El valor cadastral de les finques rústiques supera els llindars patrimonials establits en les bases de la convocatòria.', '1', 1, '10', '02', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('72', 'El valor cadastral de les finques rústiques  supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.3');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('30', 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en les bases de la  convocatòria.', '1', 1, '10', '02', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('73', 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en l''ordre de convocatòria', '2', 1, '10', '14.4');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('31', 'El conjunt d''elements patrimonials supera el límit establit en les bases de la convocatòria.', '1', 1, '10', '02', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('74', 'La suma d''elements patrimonials supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.5');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('75', 'Per deduir-se de les dades que consten en el seu expedient, que el llindar patrimonial de la unitat familiar supera l''establit per a la concessió de la beca.', '2', 1, '10', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('44', 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', '1', 1, '10', '03', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('10', 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', '1', 1, '10', '01', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('77', 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', '2', 1, '10', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('7', 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''ordre de convocatoria.', '1', 1, '10', '01', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('78', 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''orde de convocatòria', '2', 1, '10', '18');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('8', 'No complir els requisits acadèmics exigits en l''ordre de convocatòria', '1', 1, '10', '01', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('81', 'Per obtenir la beca en la convocatòria del Ministeri d''Educació i Ciència.', '2', 1, '10', '27');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('82', 'Per no acreditar la residència en la CCVV amb anterioritat a la data de finalització del tèrmini', '2', 1, '10', '28');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('84', 'Per haver renunciat o haver-li sigut anul·lada la matrícula en el present curs.', '2', 1, '10', '30');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('100', 'No estar matriculat en el nombre mínim exigit de crèdits en aquest curs', '2', 1, '10', '9.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('12', 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', '1', 1, '10', '01', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('62', 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('49', 'Els NIF de la unitat familiar no estan identificats per la  AEAT', '1', 1, '10', '04', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('64', 'L''Administració Tributària no posseeix dades econòmiques de la unitat familiar', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('61', 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('32', 'Per tindre l''obligació de presentar declaració per l''Impost Extraordinari sobre el Patrimoni, d''acord amb la normativa reguladora del dit impost.', '1', 1, '10', '02', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('43', 'No estar matriculat en el curs següent segons el pla d''estudis vigent', '1', 1, '10', '03', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('91', 'Superar la seva renda tots els llindars establerts (AEAT)', '2', 1, '10', '81');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('92', 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', '2', 1, '10', '82');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('93', 'Superar els llindars del cabdal mobiliari', '2', 1, '10', '83');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('96', 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', '2', 1, '10', '86');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('94', 'Els NIF de la unitat familiar no estan identificats per la  AEAT', '2', 1, '10', '84');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('95', 'Els membres de la unitat familiar no estan identificats econòmicament per la  AEAT', '2', 1, '10', '85');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('97', 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', '2', 1, '10', '87');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('48', 'No existir independència familiar, en aparèixer inclòs en una altra declaració de la renda.', '1', 1, '10', '04', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('98', 'Alumne independent', '2', 1, '10', '88');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('76', 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', '2', 1, '10', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('83', 'Es deu presentar certificat de Renda de l''exercici anterior del membre indicat', '2', 1, '10', '3');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('59', 'Ocultació de valors cadastrals.', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('53', 'Situació irregular davant AEAT. Algun membre computable es troba en dues declaracions de la renda.', '1', 1, '10', '04', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('79', 'No complir els requisits acadèmics exigits en l''ordre de convocatòria', '2', 1, '10', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('23', 'Pendiente de definr', '1', 1, '10', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('15', 'No complir el requisit de trobar-se el sol·licitant o els seus sustentadors treballant a Espanya (art. 1.2.d) de l''Ordre EDU 1901/2009, per la qual es convoquen beques de caràcter general i de mobilitat per a estudiants d''ensenyaments universitaris) (BOE del 15 de juliol).', '1', 1, '10', '01', '46');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('35', 'Superar els llindars de renda establits per a la concessió de l''ajuda compensatòria, beca-salari  o mobilitat especial', '1', 1, '10', '02', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('34', 'Falta informació fiscal.', '1', 1, '10', '02', '11');
   
COMMIT;

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES DROP COLUMN CODIGO;

grant execute on gra_tit.POSEE_TITULO to uji_Becas;

grant execute on gra_bec.pack_bec_2011 to uji_Becas;

grant execute on gra_pop.pack_pop to uji_Becas;
grant execute on gra_pop.CRD_sup_BECAS to uji_Becas;
grant execute on gra_pop.crd_faltan_ca to uji_Becas;
grant execute on gra_pop.CRD_PTE_CONVALIDACION to uji_Becas;
grant execute on gra_pop.CRD_MAT_BECAS to uji_Becas;
grant execute on gra_pop.CRD_PRESENTADOS to uji_Becas;
grant execute on gra_pop.crd_convalidados to uji_Becas;
grant execute on gra_pop.CRD_MAT_BECAS to uji_Becas;      
grant execute on gra_pop.curso_mat_master to uji_Becas;
grant execute on gra_pop.nota_acceso to uji_Becas;
grant execute on gra_pop.pack_pop to uji_Becas;
grant execute on gra_pop.ESTUDIO_ACCESO to uji_Becas;         
grant select on gra_pop.pop_matriculas to uji_Becas;          
grant select on gra_pop.pop_tipos_matricula to uji_Becas;          
grant select on gra_pop.pop_est_matriculas to uji_Becas;
          
grant execute on gra_exp.crd_pte_convalidacion_exp to uji_Becas;
grant select on gra_exp.exp_expedientes to uji_Becas;
grant select on gra_exp.exp_matriculas  to uji_Becas;
grant select on gra_exp.exp_tipos_matricula  to uji_Becas;          
grant select on gra_exp.exp_est_matriculas to uji_Becas;


CREATE OR REPLACE FORCE VIEW BC2_EXT_PERSONAS_ESTUDIOS (ID,
                                                                   PERSONA_ID,
                                                                   ESTUDIO_ID,
                                                                   CURSO_ACADEMICO_ID,
                                                                   CREDITOS_MATRICULADOS,
                                                                   CREDITOS_SUPERADOS,
                                                                   CREDITOS_FALTANTES,
                                                                   CREDITOS_PENDIENTES_CONVA,
                                                                   CREDITOS_FUERA_BECA,
                                                                   CREDITOS_PRESENTADOS,
                                                                   LIMITAR_CREDITOS,
                                                                   LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                   MATRICULA_PARCIAL,
                                                                   NUMERO_BECAS_MEC,
                                                                   NUMERO_BECAS_CONS,
                                                                   MINUSVALIA,
                                                                   NUMERO_SEMESTRES,
                                                                   PROGRAMA_INTERCAMBIO,
                                                                   CREDITOS_CONVALIDADOS,
                                                                   CREDITOS_PARA_BECA,
                                                                   CURSO,
                                                                   NOTA_ACCESO,
                                                                   NOTA_MEDIA,
                                                                   FAMILIA_NUMEROSA_ID,
                                                                   BECARIO,
                                                                   TIPO_MATRICULA,
                                                                   ESTUDIO_ACCESO_ID,
                                                                   SIN_DOCENCIA,
                                                                   POSEE_TITULO
                                                                  ) AS
          select rownum id, pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
          pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_exp,   -- todos ,
          GRA_POP.CRD_sup_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_superados_bec,
          -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          gra_pop.crd_faltan_ca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_faltan_ca,
          gra_pop.CRD_PTE_CONVALIDACION (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_pendiente_conva,
          (pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
           - GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
          ),                                                                                           -- crd_fuera_becs
          gra_pop.CRD_PRESENTADOS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) crd_PRESENTADOS, 0 Limitar_creditos,
          '0' limitar_creditos_fin_estudios, '0' matricula_parcial,     -- nos deberia dar igual pues es la de matricula
          gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'MIN', m.pexp_pmas_id) numero_becas_mec,  -- modificar para master,         
          gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'CON', m.pexp_pmas_id) numero_becas_cons,
          decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,                                    -- modificar para master,
          decode (gra_bec.pack_bec_2011.crd_mismo_semestre (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca, 'G'),
                  'N', 2,
                  1
                 ) numero_semestres,
          decode (gra_bec.pack_bec_2011.erasmus (m.pexp_per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
          gra_pop.crd_convalidados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_convalidados,
          GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_bec,
   -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          gra_pop.curso_mat_master (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) curso,
          gra_pop.nota_acceso (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) nota_acceso,
          pack_pop.media_exp_beca (m.pexp_per_id, m.pexp_pmas_id, 'S') nota_media,
   --decode(gra_bec.pack_bec.media_beca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca),'N',0,1) media_beca, -- se podria calcular en la logica de negocio????
          decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
          decode (pack_pop.es_becario (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca), 'S', 1, 0) es_becario,
          tm.nombre tipo_matricula,
          GRA_POP.ESTUDIO_ACCESO (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca) estudio_acceso_id, 0 sin_docencia,
          GRA_TIT.POSEE_TITULO(m.pexp_per_id, m.pexp_pmas_id) posee_titulo
   from   pop_matriculas m,
          pop_tipos_matricula tm,
          pop_est_matriculas em
   where  m.curso_aca = tm.curso_aca
   and    m.ptm_id = tm.id
   and    m.pexp_per_id = em.pmat_pexp_per_id(+)
   and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
   and    m.curso_aca = em.pmat_curso_aca(+)
 -- and m.curso_aca =2012
--  and m.pexp_per_id = 239128
   union all
   select rownum id, exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id) creditos_matriculados_exp,      -- todos ,
          --   pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) creditos_matriculados_bec, 
          -- menos adapataso comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          pack_bec.crd_sup (e.per_id, e.tit_id, m.curso_aca) creditos_superados,
          -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion no se estan calculando),
          pack_exp.crd_faltantes_ca (e.per_id, e.tit_id, m.curso_aca) creditos_faltantes,
          gra_exp.crd_pte_convalidacion_exp (e.per_id, e.tit_id, m.curso_aca) creditos_pendientes_conva,
          (pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id)
           - pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca)
          ) creditos_fuera_beca,
          pack_exp.crd_presentados (m.curso_aca, e.per_id, e.tit_id) creditos_presentados,
          gra_exp.limitar_creditos (e.per_id, e.tit_id, m.curso_aca) limitar_creditos,
          '0' limitar_creditos_fin_estudios, '0' matricual_parcial,    -- nos deberia dar igual pues es la de matricula,
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'MIN', e.tit_id) numero_becas_min, 
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'CON', e.tit_id) numero_becas_cons,
          decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,
          decode (gra_bec.pack_bec_2011.crd_mismo_semestre (e.per_id, e.tit_id, m.curso_aca, 'G'),
                  'N', 2,
                  1
                 ) numero_semestres,
          decode (gra_bec.pack_bec_2011.erasmus (e.per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
          gra_exp.crd_convalidados_exp (e.per_id, e.tit_id, m.curso_aca) creditos_convalidados,
          pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) CREDITOS_PARA_BECA,
          gra_bec.pack_bec.curso_mat (e.per_id, e.tit_id, m.curso_aca) curso,
          to_number (replace (e.nau, '.', ',')) nota_acceso,
          gra_exp.media_exp_beca (m.exp_per_id, m.exp_tit_id, m.curso_aca) nota_media,
          decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
          --decode(gra_bec.pack_bec.media_beca (e.per_id, e.tit_id, m.curso_aca),'S',1,0) media_beca, -- se podria calcular en la logica de negocio????
          decode (gra_exp.es_becario (m.exp_per_id, m.exp_tit_id, m.curso_aca), 'S', 1, 0), tm.nombre tipo_matricula,
          '' estudio_acceso_id, 0 sin_docencia, 0 posee_titulo
   from   exp_expedientes e,
          exp_matriculas m,
          exp_tipos_matricula tm,
          exp_est_matriculas em
   where  e.per_id = m.exp_per_id
   and    e.tit_id = m.exp_tit_id
   and    m.tma_id = tm.id
   --and    m.curso_aca > 2011
   --and    exp_per_id in (480774, 320437,212842, 176559,137154)
   and    m.curso_aca = tm.curso_aca
   and    m.exp_per_id = em.mat_exp_per_id(+)
   and    m.exp_tit_id = em.mat_exp_tit_id(+)
   and    m.curso_aca = em.mat_curso_aca(+);

   
   
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS
(
   ID,
   NOMBRE,
   TIPO,
   OFICIAL,
   NUMERO_CURSOS,
   CREDITOS_TOTALES,
   CREDITOS_POR_CURSO,
   SIN_DOCENCIA,
   EXTINGUIDA,
   PRESENCIAL,
   TITULACION_TECNICA,
   RAMA,
   CICLOS
)
AS
   SELECT   id,
            nombre,
            tipo,
            oficial,
            numero_cursos,
            creditos_totales,
            DECODE (numero_cursos,
                    NULL,
                    ROUND (creditos_totales / numero_cursos, 0))
               creditos_por_curso,
            sin_docencia,
            extinguida,
            presencial,
            titulacion_tecnica,
            rama,
            ciclos
     FROM   (
     SELECT   id,
                      nombre,
                      tipo_estudio tipo,
                      DECODE (
                         tipo_estudio,
                         'G',
                         1,
                         '12C',
                         DECODE (id,
                                 51001, 0,
                                 51002, 0,
                                 51003, 0,
                                 100, 0,
                                 101, 0,
                                 103, 0,
                                 104, 0,
                                 1)
                      )
                         oficial,
                      (SELECT   COUNT ( * )
                         FROM   gra_pod.pod_cursos
                        WHERE   tit_id = t.id AND id <> 7)
                         numero_cursos,
                      crd_tr + crd_ob + crd_op + crd_le + NVL (crd_pfc, 0)
                         creditos_totales,
                      DECODE (activa, 'S', 0, 1) sin_docencia,
                      DECODE (activa, 'S', 0, 1) extinguida,
                      DECODE (activa, 'S', 1, 0) presencial,
                      DECODE(GRA_BEC.PACK_BEC.ES_TECNICA(t.id),'S',1,0) titulacion_tecnica,
                      nvl(rama,'SO') rama,
                      tipo ciclos
               FROM   gra_pod.pod_titulaciones t 
             UNION ALL
             SELECT   m.id,
                      nombre,
                      'M' tipo,
                      DECODE (oficial, 'S', 1, 0),
                      DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2)
                         numero_cursos,
                      creditos creditos_totales,
                      DECODE (activo, 'S', 0, 1) sin_docencia,
                      DECODE (activo, 'S', 0, 1) extinguida,
                      DECODE (presencial, 'P', 1, 0) presencial,
                      0 titulacion_tecnica,
                      nvl(r.cod,'SO') rama,
                      1 ciclos
               FROM   gra_pop.pop_masters m, pod_ramas r
              WHERE   m.rama_id = r.id
             UNION ALL
             SELECT   id,
                      nombre,
                      'DOC' tipo,
                      1 oficial,
                      1 numero_cursos,
                      0 creditos_totales,
                      DECODE (fin_docencia, NULL, 0, 1) sin_docencia,
                      DECODE (fin_docencia, NULL, 0, 1) extinguida,
                      1 presencial,
                      0 titulacion_tecnica,
                      NULL rama,
                      1 ciclos
               FROM   gra_doc.doc_programas);

ALTER TABLE UJI_BECAS.BC2_BECAS
  ADD (POSEE_TITULO NUMBER DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_BECAS
  RENAME COLUMN BECAS_CONCEDIDAS_ANT TO BECAS_CONCEDIDAS_MEC;

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (BECAS_CONCEDIDAS_CONS  NUMBER);
 
 
 
 
CREATE OR REPLACE FORCE VIEW BC2_VW_EXT_PERSONAS_ESTUDIOS (ID,
                                                                   PERSONA_ID,
                                                                   ESTUDIO_ID,
                                                                   CURSO_ACADEMICO_ID,
                                                                   CREDITOS_MATRICULADOS,
                                                                   CREDITOS_SUPERADOS,
                                                                   CREDITOS_FALTANTES,
                                                                   CREDITOS_PENDIENTES_CONVA,
                                                                   CREDITOS_FUERA_BECA,
                                                                   CREDITOS_PRESENTADOS,
                                                                   LIMITAR_CREDITOS,
                                                                   LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                   MATRICULA_PARCIAL,
                                                                   NUMERO_BECAS_MEC,
                                                                   NUMERO_BECAS_CONS,
                                                                   MINUSVALIA,
                                                                   NUMERO_SEMESTRES,
                                                                   PROGRAMA_INTERCAMBIO,
                                                                   CREDITOS_CONVALIDADOS,
                                                                   CREDITOS_PARA_BECA,
                                                                   CURSO,
                                                                   NOTA_ACCESO,
                                                                   NOTA_MEDIA,
                                                                   FAMILIA_NUMEROSA_ID,
                                                                   BECARIO,
                                                                   TIPO_MATRICULA,
                                                                   ESTUDIO_ACCESO_ID,
                                                                   SIN_DOCENCIA,
                                                                   POSEE_TITULO
                                                                  ) AS
          select rownum id, pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
          pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_exp,   -- todos ,
          GRA_POP.CRD_sup_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_superados_bec,
          -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          gra_pop.crd_faltan_ca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_faltan_ca,
          gra_pop.CRD_PTE_CONVALIDACION (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_pendiente_conva,
          (pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
           - GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
          ),                                                                                           -- crd_fuera_becs
          gra_pop.CRD_PRESENTADOS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) crd_PRESENTADOS, 0 Limitar_creditos,
          '0' limitar_creditos_fin_estudios, '0' matricula_parcial,     -- nos deberia dar igual pues es la de matricula
          gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'MIN', m.pexp_pmas_id) numero_becas_mec,  -- modificar para master,         
          gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'CON', m.pexp_pmas_id) numero_becas_cons,
          decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,                                    -- modificar para master,
          decode (gra_bec.pack_bec_2011.crd_mismo_semestre (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca, 'G'),
                  'N', 2,
                  1
                 ) numero_semestres,
          decode (gra_bec.pack_bec_2011.erasmus (m.pexp_per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
          gra_pop.crd_convalidados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_convalidados,
          GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_bec,
   -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          gra_pop.curso_mat_master (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) curso,
          gra_pop.nota_acceso (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) nota_acceso,
          pack_pop.media_exp_beca (m.pexp_per_id, m.pexp_pmas_id, 'S') nota_media,
   --decode(gra_bec.pack_bec.media_beca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca),'N',0,1) media_beca, -- se podria calcular en la logica de negocio????
          decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
          decode (pack_pop.es_becario (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca), 'S', 1, 0) es_becario,
          tm.nombre tipo_matricula,
          GRA_POP.ESTUDIO_ACCESO (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca) estudio_acceso_id, 0 sin_docencia,
          GRA_TIT.POSEE_TITULO(m.pexp_per_id, m.pexp_pmas_id) posee_titulo
   from   pop_matriculas m,
          pop_tipos_matricula tm,
          pop_est_matriculas em
   where  m.curso_aca = tm.curso_aca
   and    m.ptm_id = tm.id
   and    m.pexp_per_id = em.pmat_pexp_per_id(+)
   and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
   and    m.curso_aca = em.pmat_curso_aca(+)
 -- and m.curso_aca =2012
--  and m.pexp_per_id = 239128
   union all
   select rownum id, exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id) creditos_matriculados_exp,      -- todos ,
          --   pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) creditos_matriculados_bec, 
          -- menos adapataso comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          pack_bec.crd_sup (e.per_id, e.tit_id, m.curso_aca) creditos_superados,
          -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion no se estan calculando),
          pack_exp.crd_faltantes_ca (e.per_id, e.tit_id, m.curso_aca) creditos_faltantes,
          gra_exp.crd_pte_convalidacion_exp (e.per_id, e.tit_id, m.curso_aca) creditos_pendientes_conva,
          (pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id)
           - pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca)
          ) creditos_fuera_beca,
          pack_exp.crd_presentados (m.curso_aca, e.per_id, e.tit_id) creditos_presentados,
          gra_exp.limitar_creditos (e.per_id, e.tit_id, m.curso_aca) limitar_creditos,
          '0' limitar_creditos_fin_estudios, '0' matricual_parcial,    -- nos deberia dar igual pues es la de matricula,
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'MIN', e.tit_id) numero_becas_min, 
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'CON', e.tit_id) numero_becas_cons,
          decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,
          decode (gra_bec.pack_bec_2011.crd_mismo_semestre (e.per_id, e.tit_id, m.curso_aca, 'G'),
                  'N', 2,
                  1
                 ) numero_semestres,
          decode (gra_bec.pack_bec_2011.erasmus (e.per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
          gra_exp.crd_convalidados_exp (e.per_id, e.tit_id, m.curso_aca) creditos_convalidados,
          pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) CREDITOS_PARA_BECA,
          gra_bec.pack_bec.curso_mat (e.per_id, e.tit_id, m.curso_aca) curso,
          to_number (replace (e.nau, '.', ',')) nota_acceso,
          gra_exp.media_exp_beca (m.exp_per_id, m.exp_tit_id, m.curso_aca) nota_media,
          decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
          --decode(gra_bec.pack_bec.media_beca (e.per_id, e.tit_id, m.curso_aca),'S',1,0) media_beca, -- se podria calcular en la logica de negocio????
          decode (gra_exp.es_becario (m.exp_per_id, m.exp_tit_id, m.curso_aca), 'S', 1, 0), tm.nombre tipo_matricula,
          '' estudio_acceso_id, 0 sin_docencia, 0 posee_titulo
   from   exp_expedientes e,
          exp_matriculas m,
          exp_tipos_matricula tm,
          exp_est_matriculas em
   where  e.per_id = m.exp_per_id
   and    e.tit_id = m.exp_tit_id
   and    m.tma_id = tm.id
   --and    m.curso_aca > 2011
   --and    exp_per_id in (480774, 320437,212842, 176559,137154)
   and    m.curso_aca = tm.curso_aca
   and    m.exp_per_id = em.mat_exp_per_id(+)
   and    m.exp_tit_id = em.mat_exp_tit_id(+)
   and    m.curso_aca = em.mat_curso_aca(+);

   
   
  drop view bc2_ext_personas_estudiosdrop view bc2_ext_personas_estudios;

CREATE TABLE uji_becas.bc2_ext_personas_estudios 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     creditos_matriculados NUMBER , 
     creditos_superados NUMBER , 
     creditos_faltantes NUMBER , 
     creditos_pendientes_conva NUMBER , 
     creditos_fuera_beca NUMBER , 
     creditos_presentados NUMBER , 
     limitar_creditos NUMBER DEFAULT 0 CHECK ( limitar_creditos IN (0, 1)) , 
     limitar_creditos_fin_estudios NUMBER DEFAULT 0 CHECK ( limitar_creditos_fin_estudios IN (0, 1)) , 
     matricula_parcial NUMBER DEFAULT 0 CHECK ( matricula_parcial IN (0, 1)) , 
     numero_becas_mec NUMBER , 
     numero_becas_cons NUMBER , 
     minusvalia NUMBER DEFAULT 0 CHECK ( minusvalia IN (0, 1)) , 
     numero_semestres NUMBER , 
     programa_intercambio NUMBER DEFAULT 0 CHECK ( programa_intercambio IN (0, 1)) , 
     creditos_convalidados NUMBER , 
     creditos_para_beca NUMBER , 
     curso NUMBER , 
     nota_acceso NUMBER , 
     nota_media NUMBER , 
     familia_numerosa_id NUMBER  NOT NULL , 
     becario NUMBER DEFAULT 0  NOT NULL CHECK ( becario IN (0, 1)) , 
     tipo_matricula VARCHAR2 (1000) , 
     estudio_acceso_id NUMBER , 
     sin_docencia NUMBER DEFAULT 0 CHECK ( sin_docencia IN (0, 1)) , 
     posee_titulo NUMBER DEFAULT 0 CHECK ( posee_titulo IN (0, 1)) 
    ) 
;



ALTER TABLE uji_becas.bc2_ext_personas_estudios 
    ADD CONSTRAINT bc2_ext_personas_estudios_PK PRIMARY KEY ( id ) ;


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
MODIFY(FAMILIA_NUMEROSA_ID  NULL);



CREATE INDEX uji_becas.bc2_ext_per_est_1_idx ON uji_becas.bc2_ext_personas_estudios 
    ( 
     persona_id ASC , 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_ext_per_est_2_idx ON uji_becas.bc2_ext_personas_estudios 
    ( 
     persona_id ASC , 
     estudio_id ASC , 
     curso_academico_id ASC 
    ) 
;



  