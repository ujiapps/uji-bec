
ALTER TABLE UJI_BECAS.BC2_EXT_ESTUDIOS
 ADD (rama  VARCHAR2(2));
 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (estudio_acceso_id  NUMBER);

 
ALTER TABLE UJI_BECAS.BC2_MINIMOS
RENAME COLUMN CREDITOS_MINIMOS TO CREDITOS_BECA_PARCIAL;

ALTER TABLE UJI_BECAS.BC2_MINIMOS
RENAME COLUMN CREDITOS TO CREDITOS_BECA_COMPLETA;


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_ESTUDIOS (ID,
                                                             ESTUDIO_ID,
                                                             NUMERO_CURSOS,
                                                             CREDITOS_TOTALES,
                                                             PRESENCIAL,
                                                             SIN_DOCENCIA,
                                                             CREDITOS_BECA_COMPLETA,
                                                             CREDITOS_BECA_PARCIAL,
                                                             TIPO_MATRICULA_ID,
                                                             BECA_ID,
                                                             CURSO_ACADEMICO_ID,
                                                             CURSO_ACADEMICO_ACTIVO,
                                                             TITULACION_TECNICA
                                                            ) AS
   SELECT solicitud.id * 100000 + becas.id AS id, estudio.id AS estudio_Id, estudio.NUMERO_CURSOS AS numero_Cursos,
          estudio.CREDITOS_TOTALES AS creditos_Totales, estudio.presencial AS presencial,
          estudio.SIN_DOCENCIA AS sin_Docencia, minimos.CREDITOS_BECA_COMPLETA AS creditos_beca_completa,
          minimos.CREDITOS_BECA_PARCIAL AS creditos_beca_parcial, becas.TIPO_MATRICULA_ID AS tipo_Matricula_Id,
          becas.id AS beca_Id, CURSO_ACA.id AS curso_Academico_Id, curso_aca.activo AS curso_Academico_Activo,
          estudio.titulacion_tecnica AS titulacion_tecnica
   FROM   BC2_EXT_ESTUDIOS estudio,
          BC2_BECAS becas,
          BC2_SOLICITANTES solicitud,
          BC2_MINIMOS minimos,
          BC2_CURSOS_ACADEMICOS curso_aca
   WHERE  estudio.id = becas.estudio_id
   AND    estudio.id = minimos.estudio_id
   AND    minimos.CURSO_ACADEMICO_ID = curso_aca.id
   AND    solicitud.id = becas.solicitante_id
   AND    solicitud.curso_academico_id = curso_aca.id;



