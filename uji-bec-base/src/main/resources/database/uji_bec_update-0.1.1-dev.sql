CREATE TABLE uji_becas.bc2_registro_envios 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     envio_id NUMBER  , 
     tanda_id NUMBER  NOT NULL , 
     correcto NUMBER DEFAULT 0 CHECK ( correcto IN (0, 1)) NOT NULL , 
     fecha DATE  NOT NULL , 
     persona_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_registro_envios__IDXv2 ON uji_becas.bc2_registro_envios 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv3 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC , 
     organismo_id ASC , 
     envio_id ASC , 
     tanda_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_registro_errores_envio 
    ( 
     id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     error VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_becas.bc2_registro_errores_IDX ON uji_becas.bc2_registro_errores_envio 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv1 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC , 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_registro_errores_envio_PK PRIMARY KEY ( id ) ;



CREATE INDEX uji_becas.bc2_registro_errores_IDXv3 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv4 ON uji_becas.bc2_registro_errores_envio 
    ( 
     beca_id ASC 
    ) 
;


ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_reg_err_env_bec_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_regi_err_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


CREATE INDEX uji_becas.bc2_registro_envios__IDXv3v2 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv4 ON uji_becas.bc2_registro_envios 
    ( 
     convocatoria_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv5 ON uji_becas.bc2_registro_envios 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv6 ON uji_becas.bc2_registro_envios 
    ( 
     persona_id ASC 
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_ext_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_becas.bc2_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;



ALTER TABLE UJI_BECAS.BC2_REGISTRO_ENVIOS
 ADD (estado  NUMBER                                DEFAULT 0                     NOT NULL)
;

ALTER TABLE UJI_BECAS.BC2_REGISTRO_ENVIOS DROP COLUMN CORRECTO;



ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_CK 
    CHECK (estado in (0,1,-1))
;

alter table bc2_becas add (   
     titulo_espanol NUMBER CHECK ( titulo_espanol IN (0, 1)) ,
     estudios_homologados NUMBER CHECK ( estudios_homologados IN (0, 1))
    );
	
alter table uji_becas.bc2_becas add (
     univ_titulacion VARCHAR2 (10) , 
     ind_coef_corrector NUMBER DEFAULT 0 CHECK ( ind_coef_corrector IN (0, 1)) , 
     nota_corregida NUMBER
     ); 

--Carga de datos que faltaban de las solicitudes de master de curso 1     
update bc2_becas a
  set (titulo_espanol, estudios_homologados, codigo_estudio_ant, univ_titulacion) =
  (select decode(g.geneindtituespana,'S',1,0), decode(g.geneindestudioshomo,'S',1,0), 
          g.genecodtitulacion, g.genecodunivtitu
    from uji_becas_ws_solicitudes.generales g
    where g.identificacion = a.identificacion and
          g.codigo_archivo_temporal = a.codigo_archivo_temporal)
where a.id in (
select b.id
from bc2_becas b, bc2_ext_estudios e
where e.id = b.estudio_id and
      e.tipo = 'M' and
      b.curso = 1);	
	  
	  
	 ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (cod_titu_acceso  VARCHAR2(10));
 