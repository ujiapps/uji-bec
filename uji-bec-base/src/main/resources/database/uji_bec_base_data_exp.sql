Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2010, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2011, 1);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2003, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2007, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2008, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1999, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2000, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1998, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1997, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1996, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1995, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1994, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1993, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (1992, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2001, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2002, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2004, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2005, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2006, 0);
Insert into UJI_BECAS.BC2_CURSOS_ACADEMICOS
   (ID, ACTIVO)
 Values
   (2009, 0);
COMMIT;




insert into uji_becas.bc2_organismos (id, nombre)
values (1, 'Ministerio');
insert into uji_becas.bc2_organismos (id, nombre)
values (2, 'Conselleria GVA');

commit;



Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (1, 1, 1, 'AE', 'General');
Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (2, 1, 1, 'AM', 'Movilidad');
Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (3, 2, 1, 'CON', 'Conselleria');
Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (4, 1, 0, 'Desempleo', 'Desempleo');
Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (5, 1, 0, 'AI', 'Inicio');
Insert into UJI_BECAS.BC2_CONVOCATORIAS
   (ID, ORGANISMO_ID, ACTIVA, ACRONIMO, NOMBRE)
 Values
   (6, 1, 0, 'CO', 'Colaboraci�n');
COMMIT;


insert into uji_becas.bc2_procesos (id, nombre, orden)
values (1, 'Ordinario', 10);
insert into uji_becas.bc2_procesos (id, nombre, orden)
values (2, 'Alegacion', 20);
insert into uji_becas.bc2_procesos (id, nombre, orden)
values (3, 'Aumento de cuantia', 30);
insert into uji_becas.bc2_procesos (id, nombre, orden)
values (4, 'Recurso', 40);

commit;

insert into uji_becas.bc2_estados (id, nombre, orden)
values (1, 'Pendiente', 10);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (2, 'Trabajada', 20);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (3, 'Denegada academicos', 30);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (4, 'Enviada', 40);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (5, 'Concedida', 50);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (6, 'Denegada', 60);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (7, 'Baja', 70);
insert into uji_becas.bc2_estados (id, nombre, orden)
values (8, 'Trasladada', 80);

commit;


insert into uji_becas.bc2_tipos_identificacion (id, nombre, orden)
values ('1','NIF',1);
insert into uji_becas.bc2_tipos_identificacion (id, nombre, orden)
values ('2','NIE',2);
insert into uji_becas.bc2_tipos_identificacion (id, nombre, orden)
values ('3','CIF',3);

commit;

insert into uji_becas.bc2_tipos_sexo (id, nombre, orden)
values ('1','Hombre',1);
insert into uji_becas.bc2_tipos_sexo (id, nombre, orden)
values ('2','Mujer',2);
insert into uji_becas.bc2_tipos_sexo (id, nombre, orden)
values ('3','No consta',3);

commit;



insert into uji_becas.bc2_tipos_domicilios (id, nombre, orden)
values (10,'Familiar',1);
insert into uji_becas.bc2_tipos_domicilios (id, nombre, orden)
values (20,'Residencia',2);

commit;


Insert into UJI_BECAS.BC2_TIPOS_PROPIEDAD   (ID, NOMBRE, ORDEN)
 Values   (1, 'Propiedad de la unidad familiar', 1);
Insert into UJI_BECAS.BC2_TIPOS_PROPIEDAD   (ID, NOMBRE, ORDEN)
 Values   (2, 'Propiedad de otros familiares', 2);
Insert into UJI_BECAS.BC2_TIPOS_PROPIEDAD   (ID, NOMBRE, ORDEN)
 Values   (3, 'Piso alquilado', 3);
Insert into UJI_BECAS.BC2_TIPOS_PROPIEDAD   (ID, NOMBRE, ORDEN)
 Values   (4, 'Piso subvencionado', 4);
Insert into UJI_BECAS.BC2_TIPOS_PROPIEDAD   (ID, NOMBRE, ORDEN)
 Values   (5, 'Ninguno de los anteriores', 5);

commit;

insert into uji_becas.bc2_tipos_residencias (id, nombre, orden)
values ('1','Familiar', 1);
insert into uji_becas.bc2_tipos_residencias (id, nombre, orden)
values ('2','Residencia o colegio', 2);
insert into uji_becas.bc2_tipos_residencias (id, nombre, orden)
values ('3','Otro domicilio', 3);

commit;


insert into uji_becas.bc2_tipos_sustentador (id, nombre, orden)
values ('1','Sustentador principal', 1);
insert into uji_becas.bc2_tipos_sustentador (id, nombre, orden)
values ('2','C�nyuge del sustentador principal', 2);
insert into uji_becas.bc2_tipos_sustentador (id, nombre, orden)
values ('3','Otros', 3);

commit;


insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('1','Solicitante', 1);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('2','Padre/Tutor', 2);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('3','Madre/Tutora', 3);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('4','Hemano/a', 4);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('5','Abuelo/a', 5);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('8','Esposo/a', 8);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('9','Hijo/a', 9);
insert into uji_becas.bc2_tipos_miembros (id, nombre, orden)
values ('0','Pareja de hecho', 10);

commit;

insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('1','Soltero',10);
insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('2','Casado',20);
insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('3','Viudo',30);
insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('4','Separado',40);
insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('5','Divorciado',50);
insert into uji_becas.bc2_estados_civiles (id, nombre, orden)
values ('6','Religioso',60);

commit;


Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (1, 'Estudiante', 1);
Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (2, 'Activo', 2);
Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (3, 'Desempleado', 3);
Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (4, 'Invalidez', 4);
Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (5, 'Jubilado', 5);
Insert into UJI_BECAS.BC2_SITUACIONES_LABORALES   (ID, NOMBRE, ORDEN)
 Values   (6, 'Ama de casa', 6);

commit;


insert into uji_becas.bc2_tipos_matricula (id, nombre, orden)
values (1,'Oficial',1);

commit;



Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (18, 3, 2006, 1, 'conselleria', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (1, 1, 2011, 1, 'general', 1);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (2, 1, 2011, 2, 'general', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (3, 3, 2011, 1, 'conselleria', 1);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (4, 2, 2011, 1, 'movilidad', 1);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (5, 2, 2010, 1, 'movilidad', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (6, 5, 2008, 1, 'Inicio', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (7, 1, 2009, 1, 'general', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (8, 2, 2009, 1, 'movilidad', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (9, 2, 2008, 1, 'movilidiad', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (10, 4, 2009, 1, 'desempleo', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (11, 4, 2010, 1, 'desempleo', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (12, 1, 2010, 1, 'General', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (13, 3, 2010, 1, 'Conselleria', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (14, 3, 2009, 1, 'conselleria', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (15, 1, 2008, 1, 'general', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (16, 3, 2009, 1, 'conselleria', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (17, 5, 2006, 1, 'inicio', 0);
Insert into UJI_BECAS.BC2_TANDAS
   (ID, CONVOCATORIA_ID, CURSO_ACADEMICO_ID, TANDA_ID, DESCRIPCION, ACTIVA)
 Values
   (19, 1, 2007, 1, 'general', 0);
COMMIT;


Insert into UJI_BECAS.BC2_TIPOS_FAMILIAS
   (ID, NOMBRE, ORDEN)
 Values
   (1, 'Ordinaria', 0);
Insert into UJI_BECAS.BC2_TIPOS_FAMILIAS
   (ID, NOMBRE, ORDEN)
 Values
   (2, 'Numerosa Primera', 1);
Insert into UJI_BECAS.BC2_TIPOS_FAMILIAS
   (ID, NOMBRE, ORDEN)
 Values
   (3, 'Numerosa de Segunda', 2);
COMMIT;



Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (11, 'Empresarios agrarios (o similares) con asalariados', 11);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (12, 'Empresarios agrarios (o similares) sin asalariados', 12);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (13, 'Resto de trabajadores agrarios o similares', 13);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (21, 'Empresarios con 10 o mas asalariados', 21);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (22, 'Empresarios con menos de 10 asalariados', 22);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (23, 'Empresarios sin asalariados y miembros de cooperat', 23);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (31, 'Directores Generales de grandes empresas y alto pe', 31);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (32, 'Jefes de departamentos administrativos,comerciales', 32);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (33, 'Resto de personal administrativo y comercial', 33);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (41, 'Prof. y alto personal tecnico (Arquitectos,Economi', 41);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (42, 'Profesionales y tecnicos medios (Peritos, Maestros', 42);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (51, 'Contramaestres y capataces', 51);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (52, 'Obreros cualificados y especializados', 52);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (53, 'Resto de trabajadores de servicios (transporte,hos', 53);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (54, 'Obreros sin especializacion', 54);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (61, 'Altos cargos y cuerpos esp. de la adm. Publica (Di', 61);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (62, 'Funcionarios con titulaciones superiores y medias', 62);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (63, 'Resto del personal de la administracion', 63);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (71, 'Profesiones liberales (Dentista, Notario, Comision', 71);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (72, 'Trabajador independiente(Electricista,Artesano,Mod', 72);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (81, 'Generales, Jefes y Oficiales', 81);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (82, 'Suboficiales,Numero Guardia Civil,Policia Nacional', 82);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (91, 'Trabajos esporadicos propios de estud.(guardar niq', 91);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (92, 'Amas de casa', 92);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (93, 'El estudiante no trabaja', 93);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (97, 'En situacion de privacion de libertad', 97);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (98, 'En situacion de desempleo', 98);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (99, 'Pensionistas por cualquier concepto(jubilacion,inv', 99);
Insert into UJI_BECAS.BC2_PROFESIONES
   (ID, NOMBRE, ORDEN)
 Values
   (90, 'No consta', 90);
COMMIT;


Insert into UJI_BECAS.BC2_TIPOS_MINUSVALIA
   (ID, NOMBRE, ORDEN)
 Values
   (1, 'Ninguna', 0);
Insert into UJI_BECAS.BC2_TIPOS_MINUSVALIA
   (ID, NOMBRE, ORDEN)
 Values
   (2, '33', 1);
Insert into UJI_BECAS.BC2_TIPOS_MINUSVALIA
   (ID, NOMBRE, ORDEN)
 Values
   (3, '65', 2);
COMMIT;



Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (1, 'Calle', 1);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (2, 'Plaza', 2);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (3, 'Avenida', 3);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (4, 'Paseo', 4);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (5, 'Ronda', 5);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (6, 'Carretera', 6);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (7, 'Travesia', 7);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (8, 'Urbanizacion', 8);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (9, 'Otros', 9);
Insert into UJI_BECAS.BC2_TIPOS_VIAS
   (ID, NOMBRE, ORDEN)
 Values
   (99, 'No Consta', 99);
COMMIT;




Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (136, 2010, 1, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (137, 2010, 7, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (138, 2010, 9, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (139, 2010, 10, 39, 78, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (140, 2010, 12, 34, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (141, 2010, 2, 30, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (142, 2010, 4, 31, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (143, 2010, 6, 30, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (144, 2010, 11, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (147, 2010, 230, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (156, 2010, 13, 33, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (157, 2010, 14, 34, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (158, 2010, 15, 33, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (159, 2010, 16, 34, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (160, 2010, 17, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (161, 2010, 21, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (162, 2010, 23, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (163, 2010, 24, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (164, 2010, 25, 29, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (165, 2010, 26, 32, 63, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (166, 2010, 27, 33, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (167, 2010, 28, 32, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (168, 2010, 29, 27, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (169, 2010, 30, 27, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (170, 2010, 31, 29, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (171, 2010, 32, 31, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (172, 2010, 33, 29, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (173, 2010, 34, 34, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (174, 2010, 35, 29, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (175, 2010, 204, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (176, 2010, 201, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (177, 2010, 202, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (178, 2010, 203, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (179, 2010, 205, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (180, 2010, 206, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (181, 2010, 207, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (182, 2010, 208, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (183, 2010, 209, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (184, 2010, 210, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (185, 2010, 211, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (186, 2010, 212, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (187, 2010, 213, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (188, 2010, 214, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (189, 2010, 215, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (190, 2010, 216, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (191, 2010, 217, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (192, 2010, 218, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (193, 2010, 219, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (194, 2010, 220, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (195, 2010, 221, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (196, 2010, 222, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (197, 2010, 223, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (198, 2010, 224, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (199, 2010, 225, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (200, 2010, 226, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (201, 2010, 227, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (202, 2010, 228, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (203, 2010, 42103, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (204, 2010, 42101, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (205, 2010, 42102, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (206, 2010, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (207, 2010, 42105, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (208, 2010, 42106, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (209, 2010, 42107, 30, 53, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (210, 2010, 42109, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (211, 2010, 42110, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (212, 2010, 42111, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (213, 2010, 42112, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (214, 2010, 42113, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (215, 2010, 42114, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (216, 2010, 42115, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (217, 2010, 42116, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (218, 2010, 42117, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (219, 2010, 42118, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (220, 2010, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (221, 2010, 42120, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (222, 2010, 42121, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (223, 2010, 42122, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (224, 2010, 42123, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (225, 2010, 42124, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (226, 2010, 42125, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (227, 2010, 42127, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (228, 2010, 42128, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (229, 2010, 42130, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (230, 2010, 42131, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (231, 2010, 42132, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (232, 2010, 42133, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (235, 2010, 42134, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (237, 2010, 42135, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (240, 2010, 42136, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (562, 2010, 42137, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (563, 2010, 42138, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (564, 2010, 42139, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (565, 2010, 42140, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (566, 2010, 42141, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (567, 2010, 42142, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (568, 2010, 42143, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (569, 2010, 42144, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (570, 2010, 42145, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (571, 2010, 42146, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (572, 2010, 42147, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (573, 2010, 42129, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (574, 2010, 42108, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (575, 2010, 42119, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (576, 2010, 42030, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (577, 2010, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (578, 2010, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (149, 2009, 42130, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (150, 2009, 42135, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (5, 2011, 230, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (145, 2011, 42148, 30, 90, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (146, 2011, 229, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (294, 2011, 1, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (295, 2011, 7, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (296, 2011, 9, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (297, 2011, 10, 35, 78, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (298, 2011, 12, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (299, 2011, 2, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (300, 2011, 4, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (301, 2011, 6, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (302, 2011, 11, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (303, 2011, 13, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (304, 2011, 14, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (305, 2011, 15, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (306, 2011, 16, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (307, 2011, 17, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (308, 2011, 21, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (309, 2011, 23, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (310, 2011, 24, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (311, 2011, 25, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (312, 2011, 26, 35, 63, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (313, 2011, 27, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (314, 2011, 28, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (315, 2011, 29, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (316, 2011, 30, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (317, 2011, 31, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (318, 2011, 32, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (319, 2011, 33, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (320, 2011, 34, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (321, 2011, 35, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (322, 2011, 204, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (323, 2011, 201, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (324, 2011, 202, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (325, 2011, 203, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (326, 2011, 205, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (327, 2011, 206, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (328, 2011, 207, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (329, 2011, 208, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (330, 2011, 209, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (331, 2011, 210, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (332, 2011, 211, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (333, 2011, 212, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (334, 2011, 213, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (335, 2011, 214, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (336, 2011, 215, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (337, 2011, 216, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (338, 2011, 217, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (339, 2011, 218, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (340, 2011, 219, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (341, 2011, 220, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (342, 2011, 221, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (343, 2011, 222, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (344, 2011, 223, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (345, 2011, 224, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (346, 2011, 225, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (347, 2011, 226, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (348, 2011, 227, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (349, 2011, 228, 30, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (350, 2011, 42103, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (351, 2011, 42101, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (352, 2011, 42102, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (353, 2011, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (354, 2011, 42105, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (355, 2011, 42106, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (356, 2011, 42107, 30, 53, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (357, 2011, 42109, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (358, 2011, 42110, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (359, 2011, 42111, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (360, 2011, 42112, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (361, 2011, 42113, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (362, 2011, 42114, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (363, 2011, 42115, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (364, 2011, 42116, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (365, 2011, 42117, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (366, 2011, 42118, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (367, 2011, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (368, 2011, 42120, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (369, 2011, 42121, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (370, 2011, 42122, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (371, 2011, 42123, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (372, 2011, 42124, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (373, 2011, 42125, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (374, 2011, 42127, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (375, 2011, 42128, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (376, 2011, 42130, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (377, 2011, 42131, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (378, 2011, 42132, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (379, 2011, 42133, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (380, 2011, 42134, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (381, 2011, 42135, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (382, 2011, 42136, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (383, 2011, 42137, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (384, 2011, 42138, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (385, 2011, 42139, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (386, 2011, 42140, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (387, 2011, 42141, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (388, 2011, 42142, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (389, 2011, 42143, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (390, 2011, 42144, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (391, 2011, 42145, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (392, 2011, 42146, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (393, 2011, 42147, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (394, 2011, 42129, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (395, 2011, 42108, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (396, 2011, 42119, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (397, 2011, 42030, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (398, 2011, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (399, 2011, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (6, 2010, 229, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (151, 2009, 42134, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (152, 2009, 42133, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (153, 2009, 42132, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (154, 2009, 42131, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (155, 2009, 42136, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (466, 2009, 42031, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (467, 2009, 42032, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (468, 2009, 42033, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (469, 2009, 42034, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (470, 2009, 1, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (471, 2009, 2, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (472, 2009, 4, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (473, 2009, 6, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (474, 2009, 7, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (475, 2009, 9, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (476, 2009, 10, 35, 78, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (477, 2009, 11, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (478, 2009, 12, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (479, 2009, 13, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (480, 2009, 14, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (481, 2009, 15, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (482, 2009, 16, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (483, 2009, 17, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (484, 2009, 21, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (485, 2009, 23, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (486, 2009, 24, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (487, 2009, 25, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (488, 2009, 26, 35, 63, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (489, 2009, 27, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (490, 2009, 28, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (491, 2009, 29, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (492, 2009, 30, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (493, 2009, 31, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (494, 2009, 32, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (495, 2009, 33, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (496, 2009, 42010, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (497, 2009, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (498, 2009, 42012, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (499, 2009, 42014, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (500, 2009, 42015, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (501, 2009, 42016, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (502, 2009, 42017, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (503, 2009, 42018, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (504, 2009, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (505, 2009, 42020, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (506, 2009, 42021, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (507, 2009, 42022, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (508, 2009, 42023, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (509, 2009, 42024, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (510, 2009, 42025, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (511, 2009, 42026, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (512, 2009, 42027, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (513, 2009, 42028, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (514, 2009, 42107, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (515, 2009, 42112, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (516, 2009, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (517, 2009, 42001, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (518, 2009, 42009, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (519, 2009, 42002, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (520, 2009, 42003, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (521, 2009, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (522, 2009, 42005, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (523, 2009, 42006, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (524, 2009, 42007, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (525, 2009, 35, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (526, 2009, 34, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (527, 2009, 42030, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (528, 2009, 201, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (529, 2009, 202, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (530, 2009, 203, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (531, 2009, 204, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (532, 2009, 205, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (533, 2009, 206, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (534, 2009, 207, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (535, 2009, 208, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (536, 2009, 209, 35, 60, 'G');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (537, 2009, 42119, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (538, 2009, 42124, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (539, 2009, 42128, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (540, 2009, 42106, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (541, 2009, 42100, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (542, 2009, 42129, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (543, 2009, 42110, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (544, 2009, 42111, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (545, 2009, 42113, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (546, 2009, 42114, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (547, 2009, 42116, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (548, 2009, 42118, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (549, 2009, 42120, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (550, 2009, 42121, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (551, 2009, 42122, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (552, 2009, 42123, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (553, 2009, 42125, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (554, 2009, 42127, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (555, 2009, 42101, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (556, 2009, 42102, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (557, 2009, 42115, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (558, 2009, 42103, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (559, 2009, 42105, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (560, 2009, 42108, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (561, 2009, 42109, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (1, 2008, 42031, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (2, 2008, 42032, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (3, 2008, 42033, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (4, 2008, 42034, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (233, 2008, 1, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (234, 2008, 2, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (236, 2008, 4, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (238, 2008, 6, 35, 60, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (239, 2008, 7, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (241, 2008, 9, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (242, 2008, 10, 35, 78, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (243, 2008, 11, 35, 75, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (244, 2008, 12, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (245, 2008, 13, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (246, 2008, 14, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (247, 2008, 15, 35, 66, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (248, 2008, 16, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (249, 2008, 17, 35, 70, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (250, 2008, 21, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (251, 2008, 23, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (252, 2008, 24, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (253, 2008, 25, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (254, 2008, 26, 35, 63, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (255, 2008, 27, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (256, 2008, 28, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (257, 2008, 29, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (258, 2008, 30, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (259, 2008, 31, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (260, 2008, 32, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (261, 2008, 33, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (262, 2008, 42010, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (263, 2008, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (264, 2008, 42012, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (265, 2008, 42014, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (266, 2008, 42015, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (267, 2008, 42016, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (268, 2008, 42017, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (269, 2008, 42018, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (270, 2008, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (271, 2008, 42020, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (272, 2008, 42021, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (273, 2008, 42022, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (274, 2008, 42023, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (275, 2008, 42024, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (276, 2008, 42025, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (277, 2008, 42026, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (278, 2008, 42027, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (279, 2008, 42028, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (282, 2008, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (283, 2008, 42001, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (284, 2008, 42009, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (285, 2008, 42002, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (286, 2008, 42003, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (287, 2008, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (288, 2008, 42005, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (289, 2008, 42006, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (290, 2008, 42007, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (291, 2008, 42008, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (292, 2008, 35, 35, 58, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (293, 2008, 34, 35, 68, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (465, 2008, 42030, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (7, 2006, 1, 35, 59.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (8, 2006, 2, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (10, 2006, 4, 35, 58.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (12, 2006, 6, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (13, 2006, 7, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (15, 2006, 9, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (16, 2006, 10, 35, 69.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (17, 2006, 11, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (18, 2006, 12, 35, 59.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (19, 2006, 13, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (20, 2006, 14, 35, 57.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (21, 2006, 15, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (22, 2006, 16, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (23, 2006, 17, 35, 61.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (26, 2006, 21, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (28, 2006, 23, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (29, 2006, 24, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (30, 2006, 25, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (31, 2006, 26, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (32, 2006, 27, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (33, 2006, 28, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (34, 2006, 29, 35, 50, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (35, 2006, 30, 35, 45, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (36, 2006, 31, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (37, 2006, 32, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (38, 2006, 33, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (39, 2006, 42010, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (40, 2006, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (41, 2006, 42012, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (42, 2006, 42014, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (43, 2006, 42015, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (44, 2006, 42016, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (45, 2006, 42017, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (46, 2006, 42018, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (47, 2006, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (48, 2006, 42020, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (49, 2006, 42021, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (50, 2006, 42022, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (51, 2006, 42023, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (52, 2006, 42024, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (53, 2006, 42025, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (54, 2006, 42026, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (55, 2006, 42027, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (56, 2006, 42028, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (60, 2006, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (61, 2006, 42001, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (62, 2006, 42009, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (63, 2006, 42002, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (64, 2006, 42003, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (65, 2006, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (66, 2006, 42005, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (67, 2006, 42006, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (68, 2006, 42007, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (69, 2006, 42008, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (70, 2006, 35, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (71, 2006, 34, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (400, 2007, 1, 35, 59.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (401, 2007, 2, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (403, 2007, 4, 35, 58.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (405, 2007, 6, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (406, 2007, 7, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (408, 2007, 9, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (409, 2007, 10, 35, 69.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (410, 2007, 11, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (411, 2007, 12, 35, 59.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (412, 2007, 13, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (413, 2007, 14, 35, 57.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (414, 2007, 15, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (415, 2007, 16, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (416, 2007, 17, 35, 61.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (419, 2007, 21, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (421, 2007, 23, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (422, 2007, 24, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (423, 2007, 25, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (424, 2007, 26, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (425, 2007, 27, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (426, 2007, 28, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (427, 2007, 29, 35, 50, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (428, 2007, 30, 35, 45, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (429, 2007, 31, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (430, 2007, 32, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (431, 2007, 33, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (432, 2007, 42010, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (433, 2007, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (434, 2007, 42012, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (435, 2007, 42014, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (436, 2007, 42015, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (437, 2007, 42016, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (438, 2007, 42017, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (439, 2007, 42018, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (440, 2007, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (441, 2007, 42020, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (442, 2007, 42021, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (443, 2007, 42022, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (444, 2007, 42023, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (445, 2007, 42024, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (446, 2007, 42025, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (447, 2007, 42026, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (448, 2007, 42027, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (449, 2007, 42028, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (453, 2007, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (454, 2007, 42001, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (455, 2007, 42009, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (456, 2007, 42002, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (457, 2007, 42003, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (458, 2007, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (459, 2007, 42005, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (460, 2007, 42006, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (461, 2007, 42007, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (462, 2007, 42008, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (463, 2007, 35, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (464, 2007, 34, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (72, 2005, 2, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (74, 2005, 4, 35, 58.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (76, 2005, 6, 35, 54.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (77, 2005, 7, 35, 62, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (79, 2005, 9, 35, 65, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (80, 2005, 10, 35, 69.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (81, 2005, 11, 35, 67, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (82, 2005, 12, 35, 59.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (83, 2005, 13, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (84, 2005, 14, 35, 57.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (85, 2005, 15, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (86, 2005, 16, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (87, 2005, 17, 35, 61.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (90, 2005, 21, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (92, 2005, 23, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (93, 2005, 24, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (94, 2005, 25, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (95, 2005, 26, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (96, 2005, 27, 35, 61, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (97, 2005, 28, 35, 60.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (98, 2005, 29, 35, 50, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (99, 2005, 30, 35, 45, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (100, 2005, 31, 35, 54, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (101, 2005, 32, 35, 57, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (102, 2005, 33, 35, 55, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (103, 2005, 42010, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (104, 2005, 42011, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (105, 2005, 42012, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (106, 2005, 42014, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (107, 2005, 42015, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (108, 2005, 42016, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (109, 2005, 42017, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (110, 2005, 42018, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (111, 2005, 42019, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (112, 2005, 42020, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (113, 2005, 42021, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (114, 2005, 42022, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (115, 2005, 42023, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (116, 2005, 42024, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (117, 2005, 42025, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (118, 2005, 42026, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (119, 2005, 42027, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (120, 2005, 42028, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (124, 2005, 42013, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (125, 2005, 42001, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (126, 2005, 42009, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (127, 2005, 42002, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (128, 2005, 42003, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (129, 2005, 42004, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (130, 2005, 42005, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (131, 2005, 42006, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (132, 2005, 42007, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (133, 2005, 42008, 30, 60, 'M');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (134, 2005, 35, 35, 55.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (135, 2005, 34, 35, 65.5, '12C');
Insert into UJI_BECAS.BC2_MINIMOS
   (ID, CURSO_ACADEMICO_ID, ESTUDIO_ID, CREDITOS_MINIMOS, CREDITOS, TIPO_ESTUDIO)
 Values
   (148, 2005, 1, 35, 59.5, '12C');
COMMIT;



