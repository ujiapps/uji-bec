CREATE OR REPLACE TRIGGER UJI_BECAS.NOTIFICA_DENEGACIONES
BEFORE UPDATE OF FECHA_NOTIFICACION
ON UJI_BECAS.BC2_TANDAS 
FOR EACH ROW
DECLARE
  v_cabecera_ca  VARCHAR2(4000);
  v_pie_ca       VARCHAR2(4000);
  v_proceso      NUMBER;
  v_estado       NUMBER;
  v_organismo_id NUMBER;
  v_organismo    bc2_organismos.nombre%type;
  v_orden        NUMBER;
  v_asunto       VARCHAR2(200)  := 'Comunicació de denegació de beca ';
  v_nombre       VARCHAR2(1000);
  v_dni          VARCHAR2(100);
  v_intro        VARCHAR2(1000);
  v_firma        VARCHAR2(1000);
  v_detalle      VARCHAR2(4000);
  v_texto        VARCHAR2(4000);
  v_fecha        DATE;
  
  hay_error     EXCEPTION;
  
  CURSOR lista_becas (p_tanda NUMBER) IS
    SELECT b.id, s.persona_id, s.numero_beca_uji
      FROM bc2_becas b, bc2_solicitantes s
      WHERE s.id = b.solicitante_id AND
            b.tanda_id = p_tanda  AND
            fecha_notificacion is null;
      
  CURSOR lista_motivos (p_beca_id NUMBER, p_orden NUMBER) IS
    SELECT b.beca_id, a.causa || a.subcausa codigo, a.nombre denegacion
      FROM bc2_becas_denegaciones b, bc2_denegaciones a
      WHERE a.id = b.denegacion_id AND
            b.beca_id = p_beca_id AND
            b.orden_denegacion = p_orden
      ORDER BY codigo;
 
BEGIN
   
   IF (:old.fecha_notificacion  <> :new.fecha_notificacion ) or (:old.fecha_notificacion is null and :new.fecha_notificacion is not null) THEN
   
       BEGIN
         select distinct proceso_id, estado_id
           INTO v_proceso, v_estado
           FROM bc2_becas
           WHERE tanda_id = :new.id;
           
           IF v_proceso = 1 THEN
             v_orden := 1;
           ELSE
             v_orden := 2;
           END IF;
       EXCEPTION
         WHEN TOO_MANY_ROWS THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'La tanda te beques en diferent procès i/o estat.');
           RAISE hay_error;           
       END;
       
       BEGIN
         SELECT o.id, o.nombre
           INTO v_organismo_id, v_organismo
           FROM bc2_convocatorias c, bc2_organismos o
           WHERE o.id = c.organismo_id AND
                 c.id = :new.convocatoria_id;
                 
         v_asunto := v_asunto || '(' || v_organismo || ')';       
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troba l''organisme.');
           RAISE hay_error;
       END;
     
       BEGIN
         SELECT cabecera_ca, pie_ca
           INTO v_cabecera_ca, v_pie_ca
           FROM bc2_textos
           WHERE proceso_id = v_proceso AND
                 organismo_id = v_organismo_id;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troben textes per a l''organisme ' || v_organismo || ' i el procès ' || v_proceso || '.');
           RAISE hay_error;
       END;   

       v_intro := '- Data ' || TO_CHAR(SYSDATE,'dd/mm/yyyy') || CHR(10) || '- R/n  Servei de Gestió de la Docència i Estudiants' || CHR(10) || '- ' || v_asunto || CHR(10);
       v_firma := 'La secretària del Jurat de Selecció de Becaris' || CHR(10) || 'Leticia Falomir Abillar' || CHR(10);

       FOR x IN lista_becas (:new.id) LOOP
         SELECT nombre, identificacion
            INTO   v_nombre, v_dni
            FROM   BC2_EXT_PERSONAS
            WHERE  id = x.persona_id;

            v_detalle := NULL;

            FOR y IN lista_motivos (x.id, v_orden) LOOP
               v_detalle := v_detalle || y.codigo || '. ' || y.denegacion || CHR(10);
            END LOOP;
            
            v_texto := v_intro || CHR(10) || CHR(10) || v_nombre || CHR(10) || CHR(10) ||
                       v_cabecera_ca || CHR(10) || CHR(10) ||
                       v_detalle || CHR(10) || CHR(10) ||
                       v_pie_ca || CHR(10) || CHR(10) || v_dni || CHR(10) || CHR(10) ||
                       v_firma;      
            
            BEGIN       
              notifica.envia_notificacion(2, x.persona_id, v_asunto, v_texto);
            EXCEPTION
              WHEN OTHERS THEN
                INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
                VALUES(HIBERNATE_SEQUENCE.Nextval, x.id, :new.id, 'No s''ha pogut notificar la beca.');
            END;
            
            v_fecha := SYSDATE;
            
            UPDATE bc2_becas
              SET fecha_notificacion = v_fecha
              WHERE id = x.id;
              
            INSERT INTO bc2_becas_historico (ID, BECA_ID, PROCESO_ID, ESTADO_ID, TANDA_ID, FECHA, USUARIO, CONVOCATORIA_ID, FECHA_NOTIFICACION)
              VALUES (hibernate_sequence.nextval, x.id, v_proceso, v_estado, :new.id, SYSDATE, 'uji_becas', :new.convocatoria_id, v_fecha);
              
       END LOOP;

   END IF;
EXCEPTION
      WHEN NO_DATA_FOUND THEN
       :new.fecha_notificacion := :old.fecha_notificacion;
     WHEN hay_error then
       :new.fecha_notificacion := NULL;
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR(-20000,'Error others: ' || sqlerrm);
END NOTIFICA_DENEGACIONES;


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_MATRICULAS (PERSONA_ID,
                                                           ESTUDIO_ID,
                                                           CURSO_ACA,
                                                           C_ARCHIVO_TEMPORAL,
                                                           BECARIO,
                                                           MATRICULAS
                                                          ) AS
   select persona_id, estudio_id, curso_Aca, c_archivo_temporal, becario,
          count (*) over (partition by curso_aca, persona_id) matriculas
   from   (select exp_per_id persona_id, exp_tit_id estudio_id, curso_aca, c_archivo_temporal,
                  decode (c.cir_id, null, 'N', 'S') becario
           from   exp_matriculas m,
                  exp_est_matriculas e,
                  (select *
                   from   exp_mat_circunstancias
                   where  cir_id = 2) c
           where  exp_per_id = e.mat_exp_per_id(+)
           and    exp_tit_id = e.mat_exp_tit_id(+)
           and    curso_aca = e.mat_curso_aca(+)
           and    exp_per_id = c.mat_exp_per_id(+)
           and    exp_tit_id = c.mat_exp_tit_id(+)
           and    curso_aca = c.mat_curso_aca(+)
           union all
           select pexp_per_id, pexp_pmas_id, curso_aca, c_archivo_temporal, decode (c.cir_id, null, 'N', 'S') becario
           from   pop_matriculas m,
                  pop_est_matriculas e,
                  (select *
                   from   pop_mat_circunstancias
                   where  cir_id = 2) c
           where  pexp_per_id = e.pmat_pexp_per_id(+)
           and    pexp_pmas_id = e.pmat_pexp_pmas_id(+)
           and    curso_aca = e.pmat_curso_aca(+)
           and    pexp_per_id = c.pmat_pexp_per_id(+)
           and    pexp_pmas_id = c.pmat_pexp_pmas_id(+)
           and    curso_aca = c.pmat_curso_aca(+)
           and    pexp_pmas_id like '42%');
		   
DROP VIEW UJI_BECAS.BC2_EXT_ESTUDIOS;

CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS
(
   ID,
   NOMBRE,
   TIPO,
   OFICIAL,
   NUMERO_CURSOS,
   CREDITOS_TOTALES,
   CREDITOS_POR_CURSO,
   SIN_DOCENCIA,
   EXTINGUIDA,
   PRESENCIAL,
   TITULACION_TECNICA,
   RAMA,
   CICLOS,
   CODIGO_CENTRO,
   TIPO_TITULO
)
AS
   SELECT   id,
            nombre,
            tipo,
            oficial,
            numero_cursos,
            creditos_totales,
            DECODE (numero_cursos,
                    NULL,
                    ROUND (creditos_totales / numero_cursos, 0))
               creditos_por_curso,
            sin_docencia,
            extinguida,
            presencial,
            titulacion_tecnica,
            rama,
            ciclos,
            codigo_centro,
            tipo_titulo
     FROM   (SELECT   t.id,
                      t.nombre,
                      tipo_estudio tipo,
                      DECODE (
                         tipo_estudio,
                         'G',
                         1,
                         '12C',
                         DECODE (t.id,
                                 51001, 0,
                                 51002, 0,
                                 51003, 0,
                                 100, 0,
                                 101, 0,
                                 103, 0,
                                 104, 0,
                                 1)
                      )
                         oficial,
                      (SELECT   COUNT ( * )
                         FROM   gra_pod.pod_cursos
                        WHERE   tit_id = t.id AND id <> 7)
                         numero_cursos,
                      crd_tr + crd_ob + crd_op + crd_le + NVL (crd_pfc, 0)
                         creditos_totales,
                      DECODE (SIGN (NVL (curso_sin_docencia, 2013) - 2012),
                              1, 0,
                              1)
                         sin_docencia,
                      DECODE (activa, 'S', 0, 1) extinguida,
                      DECODE (activa, 'S', 1, 0) presencial,
                      DECODE (GRA_BEC.PACK_BEC.ES_TECNICA (t.id), 'S', 1, 0)
                         titulacion_tecnica,
                      NVL (t.rama, 'SO') rama,
                      t.tipo ciclos,
                      u.cod_mec codigo_centro,
                      decode(t.tipo_titulo,'0H','02','01','01','02','04','03','04','04','03','06','05',null) tipo_titulo
               FROM   gra_pod.pod_titulaciones t,
                      gri_est.est_ubic_estructurales u,
                      (SELECT   34 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   32 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   4 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   2 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   6 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   23 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   21 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   27 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   28 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   24 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   11 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   12 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   14 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   13 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   16 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   15 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   10 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   1 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   29 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   35 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   7 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   17 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   30 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   25 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   33 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   9 tit_id, 2014 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   26 tit_id, 2014 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   31 tit_id, 2014 curso_sin_docencia FROM DUAL)
                      SIN
              WHERE   t.uest_id = u.id AND t.id = SIN.tit_id(+)
             UNION ALL
             SELECT   m.id,
                      nombre,
                      'M' tipo,
                      DECODE (oficial, 'S', 1, 0),
                      DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2)
                         numero_cursos,
                      creditos creditos_totales,
                      DECODE (activo, 'S', 0, 1) sin_docencia,
                      DECODE (activo, 'S', 0, 1) extinguida,
                      DECODE (presencial, 'P', 1, 0) presencial,
                      0 titulacion_tecnica,
                      NVL (r.cod, 'SO') rama,
                      1 ciclos,
                      NULL codigo_centro,
                      '06' tipo_titulo
               FROM   gra_pop.pop_masters m, pod_ramas r
              WHERE   m.rama_id = r.id(+)
             UNION ALL
             SELECT   id,
                      nombre,
                      'DOC' tipo,
                      1 oficial,
                      1 numero_cursos,
                      0 creditos_totales,
                      DECODE (fin_docencia, NULL, 0, 1) sin_docencia,
                      DECODE (fin_docencia, NULL, 0, 1) extinguida,
                      1 presencial,
                      0 titulacion_tecnica,
                      NULL rama,
                      1 ciclos,
                      NULL codigo_centro,
                      '07' tipo_titulo
               FROM   gra_doc.doc_programas);
             
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (TIENE_BECA_MEC  NUMBER   DEFAULT 0);
          
GRANT SELECT ON UJI_BECAS_WS.HIBERNATE_SEQUENCE TO UJI_BECAS;		   


CREATE TABLE uji_becas.bc2_envios 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     numero_envio NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_org_curso_UN UNIQUE ( organismo_id , curso_academico_id ) ;




ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_envios 
    ADD CONSTRAINT bc2_envios_organismos_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;

ALTER TABLE UJI_BECAS.BC2_BECAS
	ADD (nombre_centro_ant VARCHAR2(60 CHAR));
