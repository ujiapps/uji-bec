CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_RECUENTO (CURSO_ACADEMICO_ID,
                                                             ORGANISMO_ID,
                                                             ORGANISMO,
                                                             CONVOCATORIA_ID,
                                                             CONVOCATORIA,
                                                             PROCESO_ID,
                                                             PROCESO,
                                                             ESTADO_ID,
                                                             ESTADO,
                                                             BECA_CONCEDIDA,
                                                             CUANTOS
                                                            ) AS
   SELECT DISTINCT cur.id curso_academico_id, o.id organismo_id, o.nombre organismo, c.id convocatoria_id,
                   c.nombre convocatoria, p.id proceso_id, p.nombre proceso, e.id estado_id, e.nombre estado,
                   con.id beca_concedida,
                   (SELECT COUNT (*)
                    FROM   bc2_solicitantes s,
                           bc2_becas b
                    WHERE  s.id = b.solicitante_id
                    AND    (    s.curso_academico_id = cur.id
                            AND b.convocatoria_id = c.id
                            AND b.proceso_id = p.id
                            AND b.estado_id = e.id
                            AND b.beca_concedida = con.id2
                           )) cuantos
   FROM            bc2_organismos o,
                   bc2_convocatorias c,
                   bc2_estados e,
                   bc2_procesos p,
                   bc2_cursos_academicos cur,
                   (SELECT 'S' id, 1 id2, 'Si' nombre
                    FROM   dual
                    UNION ALL
                    SELECT 'N' id, 0 id2, 'No' nombre
                    FROM   dual) con
   WHERE           o.id = c.organismo_id
   AND             (cur.id >= 2012);

   
   
   
   