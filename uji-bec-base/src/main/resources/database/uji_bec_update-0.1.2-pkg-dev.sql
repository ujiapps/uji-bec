CREATE OR REPLACE PACKAGE UJI_BECAS.pack_multienvio_denegaciones AS
   TYPE tmp_errores IS TABLE OF bc2_registro_errores_envio%ROWTYPE
     INDEX BY BINARY_INTEGER;
   v_tmp_errores tmp_errores;
   conta_error   BINARY_INTEGER;
  
  function lanzageneracion(ptanda_id in number, pregistro_id in number) return number;  
  function generamultienviopropuestas(ptanda_id in number) return number;
  function genera_num_lote(ptanda_id in number) return number;
  function  carga_cabeceraenviotype(ptanda_id in number, plote in number) return number;
  procedure carga_solicitudtype(ptanda_id in number, penviotype_id in number, plote in number);
  function  carga_acogimientotype(psolicitante_id in number,parchivo_temporal in varchar2, pidentificacion in varchar2) return number;
  function  carga_datosPersonalestype(psolicitante_id in number,parchivo_temporal in varchar2, pidentificacion in varchar2, ptanda_id in number, pbeca_id in number) return number;
  function  carga_economicostype(psolicitante_id in number) return number;
  function  carga_datosgeneralestype(psolicitante_id in number,pbeca_id in number) return number;
  function  carga_ultimocursoType(pbeca_id in number, pcursoaca_beca in number) return number;
  function  carga_nuevocursotype(pbeca_id in number, pcurso_academico_id in number) return number;
  function  carga_declarantetype(parchivo_temporal in varchar2,pidentificacion in varchar2, psolicitante_id in number, pbeca_id in number, ptanda_id in number) return number; 
  function  carga_titulaciontype(pbeca_id in number) return number;
  function  carga_situacionsolicitudtype(psolicitante_id in number,pbeca_id in number ,ptanda_id in number) return number;
  procedure carga_domiciliotype (pdatospersonales_id in number, psolicitante_id in number, parchivo_temporal in varchar2, pidentificacion in varchar2, ptanda_id in number, pbeca_id in number);
  procedure carga_familiartype(psolicitante_id in number,psolicitud_id in number,parchivo_temporal in varchar2,pidentificacion in varchar2);
  procedure carga_ayudatype (pbeca_id in number,psolicitud_id in number);
  procedure carga_estadosolicitudtype (pbeca_id in number, psolicitud_id in number);
  procedure carga_estadocausaType (pbeca_id in number,pestadosolicitud_id in number);
  procedure carga_arrendatariosType(pdomicilio_id in number,pcoddomicilio in varchar2,parchivo_temporal in varchar2,pidentificacion in varchar2, ptanda_id in number, pbeca_id in number);


END pack_multienvio_denegaciones;
/

CREATE OR REPLACE PACKAGE BODY UJI_BECAS.pack_multienvio_denegaciones AS
     
  function lanzageneracion(ptanda_id in number, pregistro_id in number) return number
  is
    v_multienvio_id number;
  begin
    
    v_multienvio_id := GeneraMultienvioPropuestas(ptanda_id);
   
    if v_multienvio_id is not null
    then   
      update bc2_registro_envios set envio_id = v_multienvio_id, fecha = sysdate,estado = 1
      where id = pregistro_id;      
    else
      update bc2_registro_envios set estado = -1
       where id = pregistro_id;      
    end if;
    
    commit;
    
    return v_multienvio_id;
  end;  
  
  Function GeneraMultienvioPropuestas(PTanda_id IN NUMBER)
  return number
  is
   multienvio_id    number;
   cabeceraEnvio_id number;
   enviotype_id     number;
   v_lote            number;
  begin
    delete from bc2_registro_errores_envio
      where tanda_id = ptanda_id;
    commit;
    
    conta_error := 1;
    v_tmp_errores.delete;
    
    multienvio_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;
    insert into uji_becas_ws_denegaciones.multienvioType values(multienvio_id);
    v_lote := genera_num_lote(ptanda_id);
    cabeceraEnvio_id := carga_cabeceraEnvioType(ptanda_id, v_lote);
    
    enviotype_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
    insert into uji_becas_ws_denegaciones.enviotype (hjid, cabeceraenvio_enviotype_hjid, envio_multienviotype_hjid)
    values(enviotype_id,cabeceraenvio_id,multienvio_id);
    carga_solicitudType(ptanda_id, enviotype_id, v_lote);
    
    if v_tmp_errores.count > 0 then
      rollback;
      for i in 1..v_tmp_errores.count loop
          begin
            insert into bc2_registro_errores_envio (id, tanda_id, beca_id, error)
              values (v_tmp_errores(i).id, v_tmp_errores(i).tanda_id, v_tmp_errores(i).beca_id, v_tmp_errores(i).error);
          exception
              when others then
                raise_application_error(-20000,'id:' ||v_tmp_errores(i).id || ' tanda: ' || v_tmp_errores(i).tanda_id ||
                                      ' beca: ' || v_tmp_errores(i).beca_id || sqlerrm);
          end;
          commit;
      end loop;
      return null;
    else
      commit;
      return (multienvio_id);
    end if;

  end;
  
  Function genera_num_lote(ptanda_id in number)
  return number
  is
    v_lote         number;
  begin
    select t.tanda_id
      into v_lote
      from bc2_tandas t
      where t.id = ptanda_id;
       
    return v_lote;
  end;
   
  Function Carga_CabeceraEnvioType(ptanda_id in number, plote in number)
  return number
  is
  vcabecera_id          number;
  vcurso_academico_id   varchar2(4);
  vconvocatoria         varchar2(100);
  vtanda                varchar2(5);
  vtot_soli             number;
  vproceso              number;
  vestado               number;
  vtipo_envio           varchar2(255);
  
  begin
   vcabecera_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
   
   select curso_Academico_id, lpad(tanda_id,5,'0') into vcurso_academico_id, vtanda
   from bc2_tandas
   where id = ptanda_id;
   
   select acronimo into vconvocatoria
   from bc2_convocatorias c, bc2_tandas t
   where t.id = ptanda_id
   and   t.convocatoria_id = c.id;
   
   select count(*)  into vtot_soli
   from bc2_becas 
   where tanda_id = ptanda_id;
   
   Begin
      select distinct estado_id into vestado
        from bc2_becas
        where tanda_id = ptanda_id;  
           
    exception
        when too_many_rows then
          raise_application_error(-20000,'En aquesta tanda hi ha beques amb diferent estat');
--          rollback;
--          insert into bc2_registro_errores_envio (id, tanda_id, beca_id, error)
--            values(uji_becas.hibernate_sequence.nextval,
--                   ptanda_id, null, 'En aquesta tanda hi ha beques amb diferent estat');
         commit;
            
          
   end;
      
   select distinct proceso_id into vproceso
      from bc2_becas
      where tanda_id = ptanda_id; 
       
   select orden into vtipo_envio
   from bc2_procesos
   where id = vproceso;
   
   insert into uji_becas_ws_denegaciones.cabeceraenviotype (hjid, caencodconv, caencodsituacenvio, caencodutgen, 
               caencoduttramite, caenfechabditem, caenfechaenvitem, caenfechagenitem,caenfechapaitem,
               caenfechapbitem, caenfecharecepitem, 
               caenidcurso, caenindnacional, caenlote, caenlotegen, caennumsolicarg, caennumsolierr, 
               caensecuenc, caentipoenvio, caentipoutgen, caentipouttramite, caentotsoli)
   values(vcabecera_id,vconvocatoria,null,'400',
          '400',null,sysdate,sysdate,null,
          null, null,
          vcurso_academico_id,null,lpad(plote,5,'0'),lpad(plote,5,'0'),null,null,
          1,vtipo_envio,'1','1',vtot_soli);
    
   return(vcabecera_id);         
          
  End;
  
  procedure carga_solicitudtype(ptanda_id in number, penviotype_id in number, plote in number)
  is
  cursor becas is
  select * from bc2_becas
  where tanda_id = ptanda_id;
  
  vsolicitud_id       number;
  vcurso_academico_id varchar2(4);
  vconvocatoria       varchar2(100);
  vcodtiso            number(2);
  vcodnivel           varchar2(2);
  vcodestudio         varchar2(8);
  vindbecaant         varchar2(1);
  vcodprovsoli        varchar2(2);
  vbeca_uji           number;
  vccentro            varchar2(8);
  vdatospersonales_id number;
  veconomicos_id      number;
  vacogimiento_id     number;
  vdeclarante_id      number;
  vdatosgenerales_id  number;
  vsituacionsolicitud_id number;
  vtitulacion_id      number;
  vcruce_id           number;           
  todo_correcto       boolean;
  
  begin
    todo_correcto := true;
    
    for rbecas in becas loop
    
     vsolicitud_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
   
     select curso_Academico_id, numero_beca_uji
     into vcurso_academico_id , vbeca_uji
     from bc2_solicitantes s     
     where s.id = rbecas.solicitante_id;
     
     select acronimo into vconvocatoria
     from bc2_convocatorias c
     where  c.id = rbecas.convocatoria_id;
     
     select orden into vcodtiso
     from bc2_procesos
     where id = rbecas.proceso_id;
     
     select decode(tipo,'M','10','06'), codigo_centro 
     into vcodnivel, vccentro
     from bc2_ext_estudios
     where id = rbecas.estudio_id;
     
     select valor_origen into vcodestudio
     from bc2_diccionario
     where clase_uji = 'Estudio'
       and curso_Academico_id = vcurso_academico_id
       and organismo_id = 1
       and valor_uji= rbecas.estudio_id;
     
     select decode(rbecas.beca_curso_ant,1,'S','N')
     into vindbecaant 
     from dual;
     
     begin
        select lpad(provincia_id,2,0) into vcodprovsoli
        from bc2_domicilios
        where solicitante_id = rbecas.solicitante_id
        and tipo_domicilio_id = 10; -- 'FAM'
     exception when no_data_found then
       v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
       v_tmp_errores(conta_error).tanda_id := ptanda_id;
       v_tmp_errores(conta_error).beca_id := rbecas.id;
       v_tmp_errores(conta_error).error := 'Falta domicili familiar';

       conta_error := conta_error + 1;
       --continue;
     end;  
     
     if rbecas.entidad is null 
     then 
       v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
       v_tmp_errores(conta_error).tanda_id := ptanda_id;
       v_tmp_errores(conta_error).beca_id := rbecas.id;
       v_tmp_errores(conta_error).error := 'No hi ha dades bancaries 1';

       conta_error := conta_error + 1;
       --continue;
     elsif (length(rbecas.sucursal)<> 4 or length(rbecas.entidad) <> 4 or 
           length(rbecas.digitos_control)<> 2 or length(rbecas.cuenta_bancaria)<> 10) 
       then  
       v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
       v_tmp_errores(conta_error).tanda_id := ptanda_id;
       v_tmp_errores(conta_error).beca_id := rbecas.id;
       v_tmp_errores(conta_error).error := 'Dades bancaries errònies';

       conta_error := conta_error + 1;
       --continue;
     end if;
     
     vacogimiento_id     := carga_acogimientotype(rbecas.solicitante_id,rbecas.codigo_archivo_temporal, upper(rbecas.identificacion));
     vdatospersonales_id := carga_datosPersonalestype(rbecas.solicitante_id,rbecas.codigo_archivo_temporal, upper(rbecas.identificacion), ptanda_id, rbecas.id);     
     veconomicos_id      := carga_economicostype(rbecas.solicitante_id);
     vdatosgenerales_id  := carga_datosgeneralestype(rbecas.solicitante_id,rbecas.id );
     vdeclarante_id      := carga_declarantetype(rbecas.codigo_archivo_temporal,rbecas.identificacion,rbecas.solicitante_id, rbecas.id, ptanda_id); 
     vtitulacion_id      := carga_titulaciontype(rbecas.id);
     vsituacionsolicitud_id := carga_situacionsolicitudtype(rbecas.solicitante_id, rbecas.id, ptanda_id); 
     
     insert into uji_becas_ws_denegaciones.solicitudtype (hjid, solicambiout, solicbanco, soliccentro, 
                 solicodcausainc,solicodcolectivo ,solicodconv, solicoddocuident,solicodestadoact, 
                 solicodestudio, solicodnivel, solicodprovcentro, solicodprovsoli, 
                 solicodtcentro, solicodtipbec, solicodtiso, solicodutdesgsc, solicodutgen, 
                 solicoduttramite, solicsucu, solicursoeduc, solidc, solifechaestadoactitem, 
                 solifechaintfinincitem, solifechaintiniincitem, solifirmadigital, soliidarchivo, 
                 soliidcurso, soliidsolicitud, soliindaeat, SOLIINDABANDONO,soliindbecant,
                 SOLIINDBECA6000,SOLIINDCARGALECTIVA ,soliindcertfelectr, 
                 soliindcredencialinc,SOLIINDCUPR, SOLIINDESTSUPER, soliindgestnosscc, soliindgrabsscc,
                 SOLIINDIDIDIS ,soliindincidencia, SOLIINDINEM,
                 soliindinteresesinc, SOLIINDINTERVCCAAC, SOLIINDINTERVCCAAT, SOLIINDJUSTIFICADO, SOLIINDSUBANT, SOLIINDVIAJE,
                 solilote, solilotesscc, SOLIMOTIVO, solinifcuenta, solinumcuenta, 
                 soliobservaciones, solisecuenc, solisecuencestadoenvio, solisqciudadano, 
                 solitipcentctipocentro, solitipoutdesgsc, solitipoutgen, solitipouttramite, 
                 soliusuariocreacion, solivalfirmaut,
                 acogimiento_solicitudtype_hj_0, 
                 datospersonales_solicitudtyp_0,
                 declarante_solicitudtype_hjid, 
                 economicos_solicitudtype_hjid, 
                 generales_solicitudtype_hjid, 
                 situacionsolicitud_solicitud_0, 
                 titulacion_solicitudtype_hjid)
                 
       values (vsolicitud_id,null,rbecas.entidad, vccentro,
               null,null,vconvocatoria,rbecas.tipo_identificacion_id,'02',
               vcodestudio,vcodnivel,'12',vcodprovsoli,
               '6','2',vcodtiso,null,'400',
               '400',rbecas.sucursal,rbecas.curso,rbecas.digitos_control,sysdate,
               null,null,null,null,
               vcurso_academico_id,rbecas.id,'S', NULL,vindbecaant,
               NULL,NULL,decode(rbecas.sms,1,'S','N'),
               null,NULL, NULL,null,null,
               NULL,'N',NULL,
               null,NULL, NULL, NULL, NULL,NULL,
               lpad(plote,5,'0'),null,NULL,upper(rbecas.identificacion),rbecas.cuenta_bancaria,
               null,1,null,rbecas.codigo_archivo_temporal,
               '4','1','1','1',
               null,null,
               vacogimiento_id,
               vdatospersonales_id,
               vdeclarante_id,
               veconomicos_id,
               vdatosgenerales_id,
               vsituacionsolicitud_id,
               vtitulacion_id
               );
       
       vcruce_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
      
        
       insert into uji_becas_ws_denegaciones.enviotypeordenpagoandsolicit_0(hjid, itemordenpago_enviotypeorden_0, 
                                                                            itemsolicitud_enviotypeorden_0, 
                                                                             ordenpagoandsolicituditems_e_0)
        values(vcruce_id,null,vsolicitud_id,penviotype_id);
        
        carga_familiartype(rbecas.solicitante_id,vsolicitud_id, rbecas.codigo_archivo_temporal, rbecas.identificacion);
        carga_ayudatype (rbecas.id,vsolicitud_id);
        carga_estadosolicitudtype(rbecas.id,vsolicitud_id);
       
        
        
    end loop; 

  end;
  function  carga_acogimientotype(psolicitante_id in number,parchivo_temporal in varchar2, pidentificacion in varchar2) return number
  is 
    vacogimiento_id         number;
    vacogcifacogim          varchar2(9); 
    vacogdescentroacogim    varchar2(50);
    
  begin
     
     begin
       select acogcifacogim, acogdescentroacogim
       into vacogcifacogim, vacogdescentroacogim
       from uji_becas_ws_solicitudes.solicitudes s, uji_becas_ws_solicitudes.acogimientotype p
       where p.hjid = s.datospersonales_solicitudtyp_0 and
             s.codigo_archivo_temporal = parchivo_temporal and
             s.identificacion = upper(pidentificacion);
     exception when others then 
        return (null);
     end;
     vacogimiento_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
       
     insert into uji_becas_ws_denegaciones.acogimientotype (hjid, acogcifacogim, acogdescentroacogim)
     values(vacogimiento_id,vacogcifacogim, vacogdescentroacogim);

     return (vacogimiento_id);
      
  end;
  function  carga_datosPersonalestype(psolicitante_id in number,parchivo_temporal in varchar2, pidentificacion in varchar2, ptanda_id in number, pbeca_id in number) return number
  is
    vdatospersonales_id number;
    vnombre             varchar2(24);
    vapellido1          varchar2(24);
    vapellido2          varchar2(24);
    vcoddocuident       number;
    vnif                varchar2(15);
    vtelefono           varchar2(15);
    vtelefmovil         varchar2(15);
    vfechaNac           date;
    vidsexo             varchar2(2);
    vemail              varchar2(150);
    vindext             varchar2(1);
    vpais               varchar2(3);
    vfechacadnif        date;
    vcodnsoporte        varchar(100);
   
    
  begin
     vdatospersonales_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
     
    
     select substr(nombre,1,24),substr(apellido1,1,24),substr(apellido2,1,24),
            tipo_identificacion_id, identificacion, fecha_nacimiento, sexo_id,
            decode(nacionalidad_id, 'E','N','S'), nacionalidad_id,
            identificacion_idesp, fecha_cad_nif      
     into vnombre, vapellido1, vapellido2,vcoddocuident, vnif, vfechaNac, vidsexo,
          vindext,vpais,vcodnsoporte,vfechacadnif
     from bc2_miembros
     where solicitante_id = psolicitante_id
       and tipo_miembro_id =1;

     select telefono1, telefono2,email
     into   vtelefono, vtelefmovil,vemail
     from bc2_solicitantes 
     where id = psolicitante_id;
     
   
     
     insert into uji_becas_ws_denegaciones.datospersonalestype (hjid, dapeabrvnomb, dapeapellido1, dapeapellido2, dapebusqape1, dapebusqape2,
                                                   dapebusqnom, dapecoddocuident, dapecodnsoporte, dapedesemp, dapeemail, 
                                                   dapefechacadnifitem, dapefechaestanciaestitem, dapefechanacitem, dapefechaperresitem,
                                                   dapeidsexo, dapeindext, dapeindinstpent, dapenif, dapenombre, dapenotelectronica, 
                                                   dapepais, dapetelefmovil,dapetelefono)
     values(vdatospersonales_id,null,vapellido1,vapellido2,null,null,
            null,vcoddocuident,vcodnsoporte,null,vemail,
            vfechacadnif,null,vfechanac,null,
            vidsexo,vindext,'N',vnif,vnombre,null,
            vpais,vtelefmovil,vtelefono);
            
     carga_domiciliotype(vdatospersonales_id, psolicitante_id,parchivo_temporal,vnif, ptanda_id, pbeca_id);       
     return (vdatospersonales_id);
     
     
     
  end;
    function  carga_economicostype(psolicitante_id in number) return number
     is
    veconimpingex       number(13,2);
    veconidindep        varchar2(1);
    veconomicos_id      number;
  begin
     
     begin
        select ingresos_extranjero, decode(unidad_familiar_independiente,1,'S','N')
        into   veconimpingex, veconidindep
        from bc2_solicitantes 
        where id = psolicitante_id;
      
     exception when no_data_found then
        return null;
     end; 
     
     veconomicos_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
     insert into uji_becas_ws_denegaciones.economicostype (hjid, econcapital,ECONCODEMIGR, econcodpoblacion, econcodrepesca, 
                                                         econcoefprelacion, econdeducciones, ECONIMPMOD6000,econimpmodbecario,
                                                         econimpmodcompen, ECONIMPMODESCOL,econimpmodmovlespc, econimpmodmovlgen, 
                                                         econimpmodresid, econimpmodtasas,ECONIMPBECACT, econimpingex, econimpmodi, 
                                                         econimpvcfr, econimpvcfunr, econimpvcfur, econindindep, 
                                                         ECONINDICE6, econindiceb, econindicec, econindicee, econindicei, 
                                                         econindicem, econindicer, ECONINDICES,econindicet, econnegocio, econrenta)
     values(veconomicos_id,NULL,null,'1',null,
            null,null,NULL,null,
            null,NULL,null,null,
            null,null,NULL,veconimpingex,null,
            null,null,null,veconidindep,
            NULL,null,null,null,null,
            null,null,NULL,null,null,null);                                                        
                     
     return (veconomicos_id);         
        
  end;
  
  function  carga_declarantetype(parchivo_temporal in varchar2,pidentificacion in varchar2, psolicitante_id in number, pbeca_id in number, ptanda_id in number) return number
  is
  vdeclarante_id        number;
  vdeclarante_solicitud_id number;
  rsoli bc2_solicitantes%rowtype;
  begin    
    vdeclarante_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
    
    begin
      select declarante_solicitudtype_hjid
      into vdeclarante_solicitud_id
      from  uji_becas_ws_solicitudes.solicitudes
      where codigo_archivo_temporal = parchivo_temporal
        and identificacion = upper(pidentificacion);
    exception
      when no_data_found then
       v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
       v_tmp_errores(conta_error).tanda_id := ptanda_id;
       v_tmp_errores(conta_error).beca_id := pbeca_id;
       v_tmp_errores(conta_error).error := 'Solicitant no trobat: ' || psolicitante_id ||'DNI:'||pidentificacion||' Arxiu temporal: '||parchivo_temporal;

       conta_error := conta_error + 1;
       return null;
    end;
    
    select *
      into rsoli
      from bc2_solicitantes
      where id = psolicitante_id;
    
    insert into uji_becas_ws_denegaciones.declarantetype (hjid, declcoddocuident, decldeclarante, declempleador, 
                                                        declfechadeclitem, declindcurant, declindcurpen, declindcurult, 
                                                        declindindep, declindinem, declindrentasextranj, 
                                                        declingresosanuales, decllocdecl, declnif, declrentasextranj)
    select vdeclarante_id, declcoddocuident, decldeclarante, declempleador, 
           declfechadeclitem, declindcurant,declindcurpen, declindcurult, 
           decode(rsoli.unidad_familiar_independiente,1,'S','N') , declindinem, declindrentasextranj, 
           rsoli.ingresos_netos, decllocdecl, declnif, declrentasextranj
           
    from uji_becas_ws_solicitudes.declarantetype
    where hjid = vdeclarante_solicitud_id;
                                                          
    return (vdeclarante_id); 
  end;
  
  function  carga_datosgeneralestype(psolicitante_id in number, pbeca_id in number) return number
  is
   vdatosgenerales_id       number;
   vcurso_academico_id      number;
   vnummiemc                number;
   vcodtipfam               varchar2(2);
   vtipfam                  number;
   vhermtot                 number;
   vheruniff                number;
   vminus33                 number;
   vminus66                 number;
   vindorfandad             varchar2(1);
   vesmaster                varchar2(1);
   vcurso                   number;
   vcodtnota                varchar2(1);
   vnotamedia               number(4,2);
   vindtranspurbano         varchar2(1);
   vnumtranspurbano         number;
   vcodprof                 varchar2(2);
   vindestexting            varchar2(1);
   vindestpresen            varchar2(1);
   vestudio_id              number;
   vcodtitulacion           varchar2(8);
   vcodigo_estudio_ant      varchar2(8);
   vindmatparcial           varchar2(1);
   vcodgrminus              varchar2(1);
   vtotalcredmatr           number(6,2);
   vidperiodo               number;     
   vindestudiosfinales      varchar2(1);
   vcrelimuniv              varchar2(1); 
   vtotalcreplanest         number;
   vanosplanest             number;
   vcreditos_matri_ant      number;
   vprimeravez              varchar2(1);
   vultimocurso_id          number;
   vnuevocurso_id           number;
   vindestudioshomo         varchar2(1);
   vindtituespana           varchar2(1);
   vcodrescompms            varchar2(1);
   vindrescompms            varchar2(1);
   verror                   number(1);
   vtanda                   number;
   vcodunivtitu             varchar2(3);
   vcodigotituacceso        varchar2(10);
   
  begin
    begin
     vdatosgenerales_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
     verror := 1;
     select curso_academico_id ,numero_miembros_computables,nvl(tipo_familia_id,1),numero_hermanos,numero_hermanos_fuera,
            numero_minusvalia_33,numero_minusvalia_65,decode(orfandad,1,'S','N'),profesion_sustentador
     into   vcurso_academico_id, vnummiemc,vtipfam,vhermtot,vheruniff,vminus33,vminus66,vindorfandad, vcodprof
     from bc2_solicitantes
     where id = psolicitante_id;
     verror := 2;
     select orden
     into vcodtipfam
     from bc2_tipos_familias
     where id = vtipfam;
     verror := 3;
    
     select decode(e.tipo,'M','S','N'), b.curso, b.nota_media_ant,decode(b.transporte_urbano,1,'S','N'),
            b.numero_transportes, decode(e.sin_docencia,1,'S','N'), decode(e.presencial,1,'S','N'),
            b.estudio_id,decode(b.matricula_parcial,1,'S','N'),b.creditos_para_beca,b.numero_semestres,
            decode(b.limitar_creditos_fin_estudios,1,'S','N'),decode(b.limitar_creditos,1,'S','N'), 
            e.creditos_totales,e.numero_cursos,nvl(b.creditos_matriculados_ant,0),
            tanda_id,decode(b.titulo_espanol,1,'S','N'),decode(estudios_homologados,1,'S','N'),univ_titulacion,
            codigo_estudio_ant, cod_titu_acceso
     into   vesmaster, vcurso, vnotamedia, vindtranspurbano,vnumtranspurbano, vindestexting,
            vindestpresen, vestudio_id, vindmatparcial, vtotalcredmatr, vidperiodo,
            vindestudiosfinales, vcrelimuniv,vtotalcreplanest,vanosplanest, vcreditos_matri_ant,
            vtanda ,vindtituespana ,vindestudioshomo,vcodunivtitu,
            vcodigo_estudio_ant, vcodigotituacceso
     from  bc2_becas b, bc2_ext_estudios e
     where b.estudio_id = e.id
       and b.id = pbeca_id;
     verror := 4;      
     
     if vesmaster = 'S'
     then
       -- estudio que da acceso al master
       vcodtitulacion :=  vcodigotituacceso;
       
       if vcodtitulacion is null then
         v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
         v_tmp_errores(conta_error).tanda_id := vtanda;
         v_tmp_errores(conta_error).beca_id := pbeca_id;
         v_tmp_errores(conta_error).error := 'Error: Falta codi d''estudi de accés.';

         conta_error := conta_error + 1;
         return null;
       end if;
       
       if vcurso = 1 then
         vcodtnota := '5';
       else
         vcodtnota := null;
         vnotamedia := null;
       end if;
       
--       if vcodunivtitu is null then
--         v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
--         v_tmp_errores(conta_error).tanda_id := vtanda;
--         v_tmp_errores(conta_error).beca_id := pbeca_id;
--         v_tmp_errores(conta_error).error := 'Error: Falta codi de la universitat de l''estudi de accés.';

--         conta_error := conta_error + 1;
--         return null;
--       end if;
     else
       vcodtnota := null;
       vnotamedia := null;
       vcodtitulacion := null;
       verror := 7;
     end if;   
     
     verror:= 8;
      select t.orden
      into vcodgrminus
      from bc2_miembros m, bc2_tipos_minusvalia t
      where solicitante_id = psolicitante_id
        and m.tipo_minusvalia_id = t.id
        and tipo_miembro_id =1;
        
--     if vcurso =1 and vcreditos_matri_ant = 0
--     then
--         vprimeravez := 'S';
--     else 
--         vprimeravez := 'N';
--     end if;    
    
     select decode(count(*),1,'S','N'),decode(count(*),1,'1',null)
     into vindrescompms, vcodrescompms
     from bc2_becas_cuantias 
     where cuantia_id = 12
       and beca_id = pbeca_id;

   exception when others
   then
     v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
     v_tmp_errores(conta_error).tanda_id := vtanda;
     v_tmp_errores(conta_error).beca_id := pbeca_id;
     v_tmp_errores(conta_error).error := 'Error beca:'|| pbeca_id||' estudio '|| vestudio_id ||' error: ' || verror || sqlerrm;

     conta_error := conta_error + 1;
     return null;
   end;
  
    vultimocurso_id := carga_ultimocursoType(pbeca_id, vcurso_academico_id);
    vnuevocurso_id  := carga_nuevocursotype(pbeca_id, vcurso_academico_id);
    
    -- genenotacorr rellenar en masters --
    insert into uji_becas_ws_denegaciones.generalestype (hjid, geneanosplanest, genecentro, geneclave,GENECODAREAMETROPOLITANA,
                                            GENECODCICLO, GENECODCOLECTIVO,genecoddependencia,
                                            genecodfase, genecodgrminus, genecodmatri, genecodotrabeca, genecodotrabecasol,
                                            genecodprof, genecodrescompms, genecodtipfam, GENECODTIPOEST,GENECODTIPOESTSOL,
                                            genecodtitulacion, genecodtnota,
                                            genecrelimuniv, genedistcentro, genedomicentro, geneespecialidad,GENEESTUDIO,
                                            GENEFECHAFINPRESTITEM, GENEFECHAINIPRESTITEM,
                                            genehermtot, geneheruniff, GENEIDENVIO,geneidperiodo, geneimpaumct, geneimpbecant, 
                                            GENEIMPORTETASAS,geneindavion,
                                            geneindbecant, GENEINDCFGSLOE,geneindcoefcorr,GENEINDCREDITOS, 
                                            geneindcursocompseneca, geneindcursocompsenecasol, 
                                            GENEINDDESEMPLEOLEGAL,GENEINDENSARPROF, geneindestexting,GENEINDESTOFICIAL,
                                            geneindestpresen, geneindestpresensol, Geneindestudiosfinales, 
                                            geneindestudioshomo, GENEINDGRALIB, GENEINDGRALIBSO, GENEINDIDIOMAS,
                                            geneindmatparcial, geneindmatparcialsol,GENEINDMATREDUCIDA ,
                                            GENEINDMODVOL, GENEINDMODVOLSOL, GENEINDMOVIEXT,geneindorfandad, 
                                            GENEINDPERADULTAS, GENEINDPERADULTASSOL, GENEINDRECPRESTACIONES, GENEINDREP,GENEINDREPSOL,
                                            geneindrescompms, geneindseminario, geneindtituespana, geneindtranspurbano,
                                            GENELECTIVAS,
                                            geneloccentro, genemesespermunivorigen, genemesespermunivorigensol, geneminus33, 
                                            geneminus66, genenotacorr,  genenotamedia,genenumtransp, genenumtranspurbano,
                                            genenummiemc, genetotalcreplanest, 
                                            genetotalcredmatr, 
                                            nuevocurso_generalestype_hjid, ultimocurso_generalestype_hj_0)
    values (vdatosgenerales_id,vanosplanest,null,pbeca_id,NULL,
            NULL,NULL,'1',
            'A',vcodgrminus,'1',null,null,
            vcodprof,vcodrescompms,vcodtipfam,NULL,NULL,
            vcodtitulacion,vcodtnota,
            vcrelimuniv,null,null,null,NULL,     
            NULL,NULL,
            vhermtot,vheruniff,NULL,vidperiodo, null,null,
            NULL,'3',
            null,NULL,'N',NULL,
            null,null,
            NULL,NULL,vindestexting,NULL,
            vindestpresen,null,vindestudiosfinales,
            vindestudioshomo,NULL,NULL,NULL,
            vindmatparcial,null,NULL,
            NULL,NULL,NULL,vindorfandad,
            NULL,NULL,NULL,NULL,NULL,
            vindrescompms,null,vindtituespana,vindtranspurbano,
            NULL,
            null,null,null,vminus33,
            vminus66,null,vnotamedia, null,vnumtranspurbano,
            vnummiemc,vtotalcreplanest,
            vtotalcredmatr,
            vnuevocurso_id, vultimocurso_id);           
                                          
    
    return (vdatosgenerales_id);

  end;
  
  function  carga_ultimocursoType(pbeca_id in number, pcursoaca_beca in number) return number
  is
   vultimocurso_id          number;
   vidultcurso              varchar2(4);
   vestudios                varchar2(255);
   vcodestudio              varchar2(8);
   vestudiouji              number(8);
   vccentro                 varchar2(8);
   vnombrecentro            varchar2(100);
   vtienetitulouni          number;
   vposeetitulo             number;
   vtotalcredmatr           number(6,2);
   vcurso                   number;
   vestudioact              number;
   vcausa                   varchar2(60);
   vtotalcredsup            number(6,2);
   vporcmatr                number(5,2);
   vnotamedia               number(4,2);
   vtotalcredplanest        number(6,2);
   vaniosplanest            number;
   vtipo_estudio            varchar2(5);
   vcodrama                 varchar2(2);
   vtipcentctipocentro      varchar2(1);
   
  begin
    vultimocurso_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
    
    select nvl(curso_academico_ant,pcursoaca_beca-1), estudio_ant,codigo_estudio_ant,tiene_titulo_universitario,
          posee_titulo,creditos_matriculados_ant,curso, estudio_id, causa_otros,
          creditos_superados_ant,round((100-creditos_suspensos_ant),2), nota_media_ant,
          codigo_centro_ant, nombre_centro_ant
    into  vidultcurso, vestudios, vcodestudio,vtienetitulouni,vposeetitulo,vtotalcredmatr,
          vcurso, vestudioact, vcausa, vtotalcredsup, vporcmatr, vnotamedia,
          vccentro, vnombrecentro
    from bc2_becas
    where id = pbeca_id;
    
    if vcodestudio is not null
    then    
         
       begin
        select valor_uji into vestudiouji
        from bc2_diccionario
        where clase_uji = 'Estudio'
          and curso_Academico_id = pcursoaca_beca
          and organismo_id = 1
          and valor_origen= vcodestudio;
          
        select codigo_centro,creditos_totales,numero_cursos
        into vccentro, vtotalcredplanest,vaniosplanest
        from bc2_ext_estudios
        where id = vestudiouji;
        
      exception when no_data_found then
          vtotalcredplanest := null;
          vaniosplanest := null;         
      end;
    end if;  
    
    select tipo , decode(rama,'SO','SJ','TE','IA','EX','CC','HU','AH','SA','CS')
    into vtipo_estudio, vcodrama
    from bc2_ext_estudios
    where id = vestudioact;      
      
    if vtipo_estudio <> 'M' then
      if vcurso = 1 and vposeetitulo =0 and
         vtienetitulouni =0 and vtotalcredmatr =0 then
        vtipcentctipocentro := '1';
      else
        vtipcentctipocentro := '4';    
      end if;
    else
      vtipcentctipocentro := null;
    end if;
-------------- OJO cambia bastante al tabla, habria que ver el esquema    
--    insert into uji_becas_ws_denegaciones.ultimocursotype (hjid, ulcuaniosplanest, ulcucausa, ulcuccentro, 
--                                              ulcuccentrocertf, ulcucertifica, ulcucloc, ulcucodestudio,
--                                              ulcucodrama, ulcucureduc, ulcuespecialidad, ulcuestudios, 
--                                              ulcufechacertfitem, ulcuidultcurso, ulculoccertf, ulcunombrecentro, 
--                                              ulcunotamedia, ulcuporcmatr, ulcutipcentctipocentro,
--                                              ulcutipcentctipocentrocertf, ulcutitucodtipotit, ulcutotalcredmatr, 
--                                              ulcutotalcredplanest, ulcutotalcredsup) 
--    values (vultimocurso_id,vaniosplanest,vcausa,vccentro,
--            null,null,null,vcodestudio,
--            vcodrama,null,null,vestudios,
--            null,vidultcurso,null,null,
--            vnotamedia,vporcmatr,vtipcentctipocentro,
--            null,null,decode(vtotalcredmatr,0,null,vtotalcredmatr),
--            vtotalcredplanest,vtotalcredsup);

     insert into uji_becas_ws_denegaciones.ultimocursotype(hjid, ulcucausa, ulcuccentro, ulcuccentrocertf, ulcucertifica, 
                                                           ulcucloc, ulcucureduc, ulcuespecialidad, ulcuestudios, ulcufechacertfitem, 
                                                           ulcuidultcurso, ulcuindcompleto, ulcuindcursocompleto, ulcuindestpresen, 
                                                           ulcuindmatparcial, ulcuindmitadmodulos, ulcuindperadultas, ulcuindpromcurso, 
                                                           ulcuindrepetidor, ulculoccertf, ulcunombrecentro, ulcunotamedia, ulcuporcmatr, 
                                                           ulcusumahoraria, ulcutipcentctipocentro, ulcutipcentctipocentrocertf, 
                                                           ulcutotalcredmatr, ulcutotalcredsup, ulcutotalmodpen)
     values(vultimocurso_id,vcausa,vccentro, null, null,
            null,null,null,vestudios,null,
            vidultcurso,NULL,NULL,NULL,
            NULL, NULL, NULL ,NULL,
            NULL,null,null,vnotamedia,vporcmatr,
            NULL,vtipcentctipocentro, null,
            decode(vtotalcredmatr,0,null,vtotalcredmatr),vtotalcredsup,NULL);
                                                                    
     return vultimocurso_id;
     
     -- CUELGA ACADEMCOSUCURSOTYPE y ASIGNATURASPENDIENTESTYPE.
  end;
  
  function  carga_nuevocursotype(pbeca_id in number, pcurso_academico_id in number) return number
  is
  vnuevocurso_id        number;
  vestudio_uji          number;
  vestudios             varchar2(255);
  vccentro              varchar2(8);
  vcureduc              number;
  vtotalcredmatr        number(6,2);
  vindmatparcial        varchar2(1);
  vidperiodo            number;
  vindestudiosfinales   varchar2(1);
  vcrelimuniv           varchar2(1);
  vtotalcreplanest      number;
  vanosplanest          number;
  
  begin
  
    vnuevocurso_id :=  uji_becas_ws_denegaciones.hibernate_sequence.nextval;
    
    select estudio_id,curso,creditos_para_beca, decode(matricula_parcial, 1,'S','N'),
           numero_semestres,decode(limitar_creditos_fin_estudios, 1,'S','N'), decode(limitar_creditos, 1,'S','N')
    into   vestudio_uji, vcureduc,vtotalcredmatr,vindmatparcial,
           vidperiodo,vindestudiosfinales,vcrelimuniv
    from bc2_becas
    where id = pbeca_id;
              
    select codigo_centro, nombre, creditos_totales, numero_cursos
    into vccentro, vestudios, vtotalcreplanest,vanosplanest
    from bc2_ext_estudios
    where id = vestudio_uji;
    
    insert into uji_becas_ws_denegaciones.nuevocursotype(hjid, ncuranosplanest, ncurccentro, ncurcertifica, ncurcodmatri,
                                            ncurcodtcentro, NCURCODTIPOEST,ncurcrelimuniv, ncurcureduc, ncurcureduccompl,
                                            ncurespecialidad, ncurestudios, ncurfechacertifitem, ncuridperiodo,
                                            NCURINDASIGSUELTAS, NCURINDCFGSLOE,ncurindcompleto, NCURINDCURSOCOMPLETO,NCURINDESTPRESEN,                                            
                                            ncurindestudiosfinales,NCURINDIDIOMAS,ncurindmatparcial, NCURINDMATREDUCIDA,
                                            NCURINDMITADMODULOS, NCURINDMODVOL, NCURINDPERADULTAS, NCURINDREPETIDOR, NCURLECTIVAS,
                                            ncurloccertif,NCURSUMAHORARIA,
                                            ncurtipcentctipocentro, ncurtotalcreplanest,NCURTOTALCREDCARRERA, ncurtotalcredmatr)
    values (vnuevocurso_id, vanosplanest,vccentro,null,'1',
            '6',NULL,vcrelimuniv,vcureduc,null,
            null,vestudios,null,vidperiodo,
            NULL,NULL, null,NULL,NULL,
            vindestudiosfinales,NULL,vindmatparcial,NULL,
            NULL,NULL,NULL,NULL,NULL,
            null,NULL,            
            '4',vtotalcreplanest,NULL,decode(vtotalcredmatr,0,null,vtotalcredmatr));                                                                   
    return vnuevocurso_id;
    -- CUELGA ACADEMCOSNCURSOTYPE.
  
  end;
  
  function  carga_titulaciontype(pbeca_id in number) return number
  is
    vtitulacion_id   number;
    vposee_titulo    varchar2(1);
    vesmaster        varchar2(1);
    videntificacion  varchar2(100);
    vcodigo_archivo  varchar2(100);
    vtitusol_id      number;
    vtitucodtipotit  varchar2(2);
    vtituindtitulo   varchar2(1);
    vtitutitulo      varchar2(255);
  begin 
    select decode(e.tipo,'M','S','N'), decode(posee_titulo,1,'S','N'), upper(identificacion), codigo_archivo_temporal
    into   vesmaster, vposee_titulo, videntificacion, vcodigo_archivo
    from  bc2_becas b, bc2_ext_estudios e
    where b.estudio_id = e.id
      and b.id = pbeca_id;
      
    if vesmaster = 'N' then
      vtitulacion_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;
      
      if vposee_titulo = 'S' then
      
        select titulacion_solicitudtype_hjid
          into vtitusol_id
          from uji_becas_ws_solicitudes.solicitudes
          where identificacion = videntificacion and
                codigo_archivo_temporal = vcodigo_archivo;     
 
        select titucodtipotit, tituindtitulo, titutitulo
          into vtitucodtipotit, vtituindtitulo, vtitutitulo
          from uji_becas_ws_solicitudes.titulaciontype
          where hjid = vtitusol_id;
      
      insert into uji_becas_ws_denegaciones.titulacionType(hjid,  titucodtipotit, tituindtitulo, titutitulo)
        values (vtitulacion_id,  vtitucodtipotit, vposee_titulo, vtitutitulo);
      else
        insert into uji_becas_ws_denegaciones.titulacionType(hjid, titucodtipotit, tituindtitulo, titutitulo)
          values (vtitulacion_id, null, 'N', null);
      end if;
    
      return vtitulacion_id;
    else
      return null;
    end if;

  end;
  
  function  carga_situacionsolicitudtype(psolicitante_id in number,pbeca_id in number ,ptanda_id in number) return number
  is
  vsituacionsolicitud_id        number;
  vtipo_fam                     number;
  vhermtot                      number; 
  vminus33                      number;
  vminus66                      number;
  vhermuniff                    number;
  vindorfandad                     varchar2(1);
  vcodtipfam                    varchar2(1);
  vcodgrminus                   varchar2(1);
  
  begin
      vsituacionsolicitud_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;  
            
      select tipo_familia_id, numero_hermanos, numero_minusvalia_33, numero_minusvalia_65,
             numero_hermanos_fuera, decode(orfandad,1,'S','N')
      into vtipo_fam, vhermtot, vminus33, vminus66, vhermuniff, vindorfandad 
      from bc2_solicitantes
      where id = psolicitante_id;
      begin
         select orden into vcodtipfam
         from bc2_tipos_familias 
         where id = vtipo_fam;
      exception when others then
         v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
         v_tmp_errores(conta_error).tanda_id := ptanda_id;
         v_tmp_errores(conta_error).beca_id := pbeca_id;
         v_tmp_errores(conta_error).error := 'Error no es troba el tipus familia';
       
      end;
      select tm.orden into vcodgrminus
      from bc2_miembros m, bc2_tipos_minusvalia tm
      where m.solicitante_id = psolicitante_id
        and m.tipo_minusvalia_id = tm.id
        and m.tipo_miembro_id =1;
      
      insert into uji_becas_ws_denegaciones.situacionsolicitudtype (hjid, sisocarnetfamnum, sisoccaafamnum, sisocodgrminus, 
                                                       sisocodtipfam, sisofechafallultprogitem, sisofechafinfamnumitem, 
                                                       SISOFECHAFINPRESTITEM, SISOFECHAINIPRESTITEM,
                                                       sisohermtot, sisohermuniff, SISOIMPINGEX, SISOIND65MOTORA, 
                                                       SISOINDDESEMSUSTPRINC, SISOINDDESEMPPREST, SISOINDDESEMPLEOLEGAL, 
                                                       SISOINDFAMMONOPARENTAL, SISOINDINVCONY, SISOINDINVSOLIC, SISOINDINVSUSTPRINC,
                                                       sisoindorfandad, SISOINDRECPRESTACIONES,sisominus33, sisominus66)    
      values (vsituacionsolicitud_id,null,null,vcodgrminus,
              vcodtipfam,null,null,
              NULL,NULL,
              vhermtot,vhermuniff,NULL,NULL,
              NULL,NULL,NULL,
              NULL,NULL,NULL,NULL,
              vindorfandad,NULL,vminus33,vminus66);
      
      return (vsituacionsolicitud_id);                                                       
  end;
 

  procedure carga_domiciliotype (pdatospersonales_id in number, psolicitante_id in number, parchivo_temporal in varchar2, pidentificacion in varchar2, ptanda_id in number, pbeca_id in number)
  is
 
 cursor domi is
  select d.*, td.orden,upper(substr(td.nombre,1,3)) coddomicilio
  from bc2_domicilios d, bc2_tipos_domicilios td
  where solicitante_id = psolicitante_id
    and td.id=d.tipo_domicilio_id;
  
  vdomicilio_id             number;
  vcontador                 number := 1;
  vcloc                     varchar2(9);
  vtelefono                 varchar2(15);
  soldomi                   uji_becas_ws_solicitudes.domicilios%rowtype;
  vdomiarrendador           varchar2(80);
  vdomicoddocuident         number;
  vdomifechafincontratoitem date;
  vdomifechainicontratoitem date;
  vdomiimportefianza        number(9,2);
  vdominif                  varchar2(15);
  vdominumarrendatarios     number;
  begin
   
    
    for rdomi in domi loop
        vdomicilio_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;      
        begin         
           select codigo into vcloc
           from bc2_localidades
           where id = rdomi.localidad_id;
        exception
        when no_data_found then
           v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
           v_tmp_errores(conta_error).tanda_id := ptanda_id;
           v_tmp_errores(conta_error).beca_id := pbeca_id;
           v_tmp_errores(conta_error).error := 'Error no es troba localitat:'|| rdomi.localidad_id ||' solicitante: '|| psolicitante_id;

           conta_error := conta_error + 1;
        end;
        select telefono2 into vtelefono
        from bc2_solicitantes
        where id = psolicitante_id;
        
        begin
          select domiarrendador,domicoddocuident,domifechafincontratoitem,
                 domifechainicontratoitem,domiimportefianza,dominif,
                 dominumarrendatarios
          into vdomiarrendador,vdomicoddocuident,vdomifechafincontratoitem,
               vdomifechainicontratoitem,vdomiimportefianza,vdominif,
               vdominumarrendatarios
          from uji_becas_ws_solicitudes.domicilios
          where identificacion = upper(pidentificacion) and
                codigo_archivo_temporal = parchivo_temporal and
                domicoddomicilio = rdomi.coddomicilio;  
        exception
          when no_data_found then 
            vdomiarrendador           := null;
            vdomicoddocuident         := null;
            vdomifechafincontratoitem := null;
            vdomifechainicontratoitem := null;
            vdomiimportefianza        := null;
            vdominif                  := null;
            vdominumarrendatarios     := null;
        end;
     begin
             
        insert into uji_becas_ws_denegaciones.domiciliotype (hjid, domiarrendador, domicloc, domicoddocuident,
                    domicoddomicilio, domicodpostal, domicodresidencia, domicodvia, 
                    domicorrespondencia,domicprov,DOMIDEPARTAMENTO, domidomicilio, domiescalera, DOMIETC,
                    domifechafincontratoitem, domifechainicontratoitem,
                    domiimportefianza, domiimportemensual, domiindespana, domiindgratuito, domiletra,
                    domilocalidad, dominif, dominumarrendatarios, dominumero, domiotrodomicilio, domipais,
                    domipiso, domisecuencdomc, domitelefono, domicilio_datospersonalestyp_0)
        values(vdomicilio_id,vdomiarrendador,vcloc,vdomicoddocuident,
               rdomi.coddomicilio,rdomi.codigo_postal, rdomi.tipo_residencia_id,lpad (rdomi.tipo_via_id,2,'0') , 
               decode(rdomi.correspondencia,1,'S','N'), lpad(rdomi.provincia_id,2,0),NULL,rdomi.nombre_via,rdomi.escalera,NULL,
               vdomifechafincontratoitem,vdomifechainicontratoitem,
               vdomiimportefianza,rdomi.importe_alquiler,'S',decode(rdomi.gratuito,1,'S','N'), translate(rdomi.puerta,'ºª', '  '), 
               null,vdominif,vdominumarrendatarios,rdomi.numero,rdomi.otra_clase_domicilio,'E',
               substr(translate(rdomi.piso,'ºª', '  '),1,2),vcontador,vtelefono,pdatospersonales_id);
               
        carga_arrendatariosType(vdomicilio_id, rdomi.coddomicilio,parchivo_temporal, upper(pidentificacion), ptanda_id, pbeca_id);
        vcontador:= vcontador +1; 
     exception
     when others then 
       v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
       v_tmp_errores(conta_error).tanda_id := ptanda_id;
       v_tmp_errores(conta_error).beca_id := pbeca_id;
       v_tmp_errores(conta_error).error := 'Error Domicili: DNI:'||pidentificacion||' error: '|| sqlerrm;

       conta_error := conta_error + 1;
     end;
    end loop;
  
  end;

  procedure carga_arrendatariosType(pdomicilio_id in number,pcoddomicilio in varchar2,parchivo_temporal in varchar2,pidentificacion in varchar2, ptanda_id in number, pbeca_id in number)
  is
  cursor arrendatarios is
  select *
  from uji_becas_ws_solicitudes.arrendatarios
  where domicoddomicilio = pcoddomicilio 
    and codigo_archivo_temporal = parchivo_temporal 
    and identificacion = upper(pidentificacion);
    
   varrendatario_id          number;  
  begin
    for rarre in arrendatarios loop
        varrendatario_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;
        
        if rarre.arrearrendatario is not null and
           rarre.arrecoddocuident is not null and
           rarre.arrenif is not null and
           rarre.arrecoddocuident in (1, 2, 3) then
          insert into uji_becas_ws_denegaciones.arrendatariostype(hjid, arrearrendatario, arrecoddocuident, arrenif, arrendatarios_domiciliotype__0 )
          values (varrendatario_id, rarre.arrearrendatario, rarre.arrecoddocuident, rarre.arrenif, pdomicilio_id);
        else
          v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
          v_tmp_errores(conta_error).tanda_id := ptanda_id;
          v_tmp_errores(conta_error).beca_id := pbeca_id;
          v_tmp_errores(conta_error).error := 'Error en les dades dels arrendataris';

          conta_error := conta_error + 1;
        end if;  
    end loop;
    
  end;
  
  
  
   procedure carga_familiartype(psolicitante_id in number,psolicitud_id in number, parchivo_temporal in varchar2, pidentificacion in varchar2)
   is
   cursor miembros is
   select m.*
   from bc2_miembros m
      , bc2_tipos_miembros t
   where m.solicitante_id = psolicitante_id
     and m.tipo_miembro_id = t.id
   order by t.orden;
   
   vcontador            number := 1;
   vmiembro_id          number;
 --  vfamiindvalidciud    varchar2(5);
   vccaaminus           varchar2(20);
   
   begin
    for rmi in miembros loop
        
        vmiembro_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval; 
        
    
        if  rmi.ccaa_minus is not null
        then
             select codigo 
             into  vccaaminus
             from bc2_ccaa
             where id =  rmi.ccaa_minus;        
        else
             vccaaminus := null;
        end if;
     
        insert into uji_becas_ws_denegaciones.familiartype (hjid, famiape1, famiape2, famiccaaminus, famicestcivil,
                                               famicoddocuident, famicodestuniv, famicodextr, famicodnsoporte, 
                                               famicodparentesco, famicodsus, famicoduniv, famifechacadnifitem,
                                               famifechafinminusitem, famifechanacitem, famifecharesminusitem,
                                               famiidenvio,famiindminusvalia, famiindnovalidacion,
                                               familocalidad, faminiffam, faminombre, famiorden,
                                               famiprofesion, famisitlaboral, 
                                               familiar_solicitudtype_hjid)
        values (vmiembro_id,rmi.apellido1,rmi.apellido2,vccaaminus,rmi.estado_civil_id,
                rmi.tipo_identificacion_id,rmi.codigo_estudio_hermano,decode(rmi.nacionalidad_id, 'E','N','S'),rmi.identificacion_idesp,
                rmi.tipo_miembro_id,rmi.sustentador_id, rmi.codigo_univ_hermano, rmi.fecha_cad_nif,
                rmi.fecha_fin_minus, rmi.fecha_nacimiento, rmi.fecha_res_minus,
                null, decode(nvl(rmi.tipo_minusvalia_id,1),'1','N','S'),decode(nvl(rmi.no_valida_identificacion,0),1,'S','N'),
                rmi.localidad_trabajo,upper(rmi.identificacion),rmi.nombre,vcontador,
                rmi.profesion_id,decode(rmi.situacion_laboral_id,1,'E',2,'A',3,'D',4,'I',5,'J',6,'M'),
                psolicitud_id);                
        vcontador := vcontador+1;  
        
     -- CUELGA DATOSFAMILIARESTYPE, DATOSHACIENDATYPE,FAMICATASTRALESTYPES                                                  
    end loop;
   end;
   
    procedure carga_ayudatype (pbeca_id in number,psolicitud_id in number) 
    is
     cursor ayudas is
     select bc.*, substr(c.codigo,1,2) tipoAyuda, substr(codigo,3,2) codayuda
     from bc2_becas_cuantias bc, bc2_cuantias c
     where bc.cuantia_id = c.id
       and bc.beca_id = pbeca_id;
       
     vayuda_id      number;
   
    begin
        for rayu in ayudas loop
            vayuda_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval; 
            
            insert into uji_becas_ws_denegaciones.ayudatype(hjid, ayudcodayuda, ayudcuantia, ayudcuantiaapagar, 
                                               ayudcuantiasuple, ayudidordenpago, ayudindconcedida,
                                               ayudindpropcentro, ayudindproput, ayudtipoayuda,
                                               ayuda_solicitudtype_hjid)
                                               
            values (vayuda_id, rayu.codayuda,rayu.importe,null,
                    0,null,'S',
                    null,'S',rayu.tipoayuda,
                    psolicitud_id);                                                                        
        end loop;
    end;
    
  procedure carga_estadosolicitudtype (pbeca_id in number, psolicitud_id in number)
 is
 vestadosolicitud_id      number;
 begin
  
    vestadosolicitud_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval;
  
    insert into uji_becas_ws_denegaciones.estadosolicitudtype(hjid, essocodestado, essocomunicadapor, essocomunicadasms,
                                                            essofechacomunicacionitem, essofechadescargaitem, ESSOFECHAEMPRESAITEM,
                                                            essofechaitem, ESSOINDCOMUEMP,
                                                            essoinfocomunicada, essoinfodescarga, essoordendenegacion, 
                                                            essopdrtextlib, essosecuencestado, estadosolicitud_solicitudtyp_0)
    values (vestadosolicitud_id,'10',null,null,
            null,null,NULL,
            sysdate,NULL,
            'N',null,1,
            null,1,psolicitud_id); 
            
     -- OJO el campo secuenceestado debe ir incrementando (1,999) , y hay que guardarlo cuando el mecd nos envie la beca.
                                                                    
    carga_estadocausaType (pbeca_id,vestadosolicitud_id);                                                        
 end;     
   procedure carga_estadocausaType (pbeca_id in number,pestadosolicitud_id in number) 
    is
     cursor denegaciones is
     select bd.*,d.causa, d.subcausa
     from bc2_becas_denegaciones bd, bc2_denegaciones d
     where bd.denegacion_id = d.id
       and bd.beca_id = pbeca_id;
              
     vestadocausa_id      number;
     vtanda_id            number;
   
    begin
        select tanda_id 
        into vtanda_id 
        from bc2_becas 
        where id = pbeca_id;
        
        for rdene in denegaciones loop
            vestadocausa_id := uji_becas_ws_denegaciones.hibernate_sequence.nextval; 
            
            if rdene.causa <> '01'  -- solo para denegaciones de la UT y proceso ordinario.
            then
                v_tmp_errores(conta_error).id := uji_becas.hibernate_sequence.nextval;
                v_tmp_errores(conta_error).tanda_id := vtanda_id;
                v_tmp_errores(conta_error).beca_id := rdene.beca_id;
                v_tmp_errores(conta_error).error := 'Error Codi de causa de denegacion distintes al "01"';

            else
                insert into uji_becas_ws_denegaciones.estadocausaType(hjid, escacodcausa, escacodsubcausa, 
                                                                      escaobserv, escaparam1, escaparam2, 
                                                                      estadocausa_estadosolicitudt_0)                                           
                                               
                values (vestadocausa_id, rdene.causa,rdene.subcausa,
                        null,null,null,
                        pestadosolicitud_id);
            end if;                                                                        
        end loop;
    end;  
   
 

END pack_multienvio_denegaciones;
/
