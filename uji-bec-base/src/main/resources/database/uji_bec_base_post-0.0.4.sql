grant execute on media_Exp_hasta_curso to uji_becas, uji_becas_usr;

grant execute on pack_bec to uji_becas, uji_becas_usr;



create or replace view uji_becas.bc2_ext_personas_estudios as 
select   id, persona_id, estudio_id, curso_academico_id, creditos_matriculados, creditos_superados, creditos_faltantes,
         creditos_convalidados, creditos_pendientes_conva, creditos_fuera_beca, creditos_presentados, limitar_creditos,
         limitar_creditos_fin_estudios, matricula_parcial, numero_becas, numero_semestres, sin_docencia,
         programa_intercambio, curso,
         decode (((creditos_matriculados / 3) - creditos_presentados), 1, 1, 0) tercio_presentado,
         rendimiento_Academico, nota_media, nota_acceso
from     (select curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_tit_id estudio_id,
                 curso_aca curso_academico_id,
                 pack_exp.crd_matriculados (curso_Aca, exp_per_id, exp_tit_id) creditos_matriculados,
                 pack_exp.crd_sup_ca (exp_per_id, exp_tit_id, curso_aca) creditos_superados,
                 pack_exp.crd_faltantes_ca (exp_per_id, exp_tit_id, curso_aca) creditos_faltantes,
                 (select nvl (sum (a.creditos), 0)
                  from   gra_exp.exp_asi_cursadas ac,
                         gra_pod.pod_asignaturas a
                  where  nvl (cco_id, 0) = 1
                  and    mat_exp_per_id = m.exp_per_id
                  and    mat_exp_tit_id = m.exp_tit_id
                  and    mat_curso_aca = m.curso_aca
                  and    asi_id = a.id) creditos_convalidados,
                 (select nvl (sum (a.creditos), 0)
                  from   gra_exp.exp_asi_cursadas ac,
                         gra_pod.pod_asignaturas a
                  where  estado in ('A', 'C', 'R')
                  and    cco_id = 0
                  and    mat_exp_per_id = m.exp_per_id
                  and    mat_exp_tit_id = m.exp_tit_id
                  and    mat_curso_aca = m.curso_aca
                  and    asi_id = a.id) creditos_pendientes_conva,
                 decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id),
                         0, 0,
                         pack_exp.imp_creditos_no_beca (exp_tit_id, exp_per_id, curso_aca)
                         / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id)
                        ) creditos_fuera_beca,
                 0 limitar_creditos, 0 limitar_creditos_fin_estudios,
                 (select decode (count (*), 0, 0, 1)
                  from   exp_mat_circunstancias
                  where  mat_exp_per_id = m.exp_per_id
                  and    mat_exp_tit_id = m.exp_tit_id
                  and    mat_curso_aca = m.curso_aca
                  and    cir_id = 3) matricula_parcial,
                 (select count (*)
                  from   gra_bec.bec_solicitudes
                  where  curso_aca < m.curso_Aca
                  and    per_id = m.exp_per_id
                  and    tit_id = m.exp_tit_id) numero_becas,
                 (select count (distinct semestre)
                  from   gra_exp.exp_asi_cursadas ac,
                         gra_pod.pod_grupos p
                  where  mat_exp_per_id = m.exp_per_id
                  and    mat_exp_tit_id = m.exp_tit_id
                  and    mat_curso_aca = m.curso_aca
                  and    ac.asi_id = p.asi_id
                  and    mat_curso_aca = p.curso_aca) numero_semestres,
                 (select decode (count (*), 0, 0, 1)
                  from   exp_asi_cursadas
                  where  mat_exp_per_id = exp_per_id
                  and    mat_exp_tit_id = exp_tit_id
                  and    mat_curso_aca = curso_aca
                  and    grp_id = 'Z') sin_docencia,
                 (select decode (count (*), 0, 0, 1)
                  from   gra_exp.exp_sol_convocatorias
                  where  tipo = 'E'
                  and    asc_mat_exp_per_id = exp_per_id
                  and    asc_mat_exp_tit_id = exp_tit_id
                  and    asc_mat_curso_aca = curso_aca) programa_intercambio,
                 pack_bec.curso_mat (exp_per_id, exp_tit_id, curso_aca) curso,
                 pack_exp.crd_presentados (curso_aca, exp_per_id, exp_tit_id) creditos_presentados,
                 0 rendimiento_academico,
                 gra_exp.media_exp_hasta_curso(exp_per_id, exp_tit_id, curso_aca,'S') nota_media,
                 null nota_acceso
          from   gra_exp.exp_matriculas m
          --where  exp_per_id in (1, 191800, 148102, 189420, 117260, 8126)
          )
 
