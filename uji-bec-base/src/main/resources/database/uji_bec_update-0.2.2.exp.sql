DROP VIEW BC2_EXT_TODAS_CONSELLERIA;

/* Formatted on 17/07/2013 9:33:36 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW BC2_EXT_TODAS_CONSELLERIA
(
   PERSONA_ID,
   SOLICITANTE_ID,
   BECA_ID,
   CURSO_ACADEMICO_ID,
   CONVOCATORIA_ID,
   PROCESO_ID,
   ESTADO_ID,
   UT,
   APELLIDO1,
   APELLIDO2,
   NOMBRE,
   SEXO,
   CODIGO_POSTAL,
   LOCALIDAD,
   NIF,
   FECHA_NACIMIENTO,
   ESTUDIO_ID_MEC,
   ESTUDIO_ID,
   CURSO,
   CICLO,
   CODIGO_CENTRO,
   CREDITOS_MATRICULADOS,
   TASAS_TOTAL,
   CREDITOS_CUBIERTOS,
   TIPOBECA,
   DEN1,
   DEN2,
   DEN3,
   DEN4,
   DEN5,
   DEN6
)
AS
   SELECT   s.persona_id,
            b.solicitante_id,
            b.id beca_id,
            S.CURSO_ACADEMICO_ID,
            B.CONVOCATORIA_ID,
            b.proceso_id,
            DECODE (b.beca_concedida, 1, 5, b.estado_id) estado_id,
            'UJI' ut,
            SUBSTR (m.apellido1, 1, 25) apellido1,
            SUBSTR (m.apellido2, 1, 25) apellido2,
            SUBSTR (m.nombre, 1, 25) nombre,
            DECODE (m.sexo_id, 1, 0, 2) sexo,
            NVL (domi.codigo_Postal, '00000') codigo_postal,
            SUBSTR (loc.nombre, 1, 25) localidad,
            B.IDENTIFICACION nif,
            m.FECHA_NACIMIENTO,
            DIC.VALOR_ORIGEN estudio_id_mec,
            e.estudio_id,
            NVL (b.curso, 1) curso,
            '1' ciclo,
            ee.codigo_centro,
            e.creditos_matriculados,
            e.tasas_total,
            e.creditos_cubiertos,
            (SELECT   COUNT ( * )
               FROM   bc2_becas bb, bc2_solicitantes ss
              WHERE       bb.solicitante_id = ss.id
                      AND ss.curso_academico_id = s.curso_academico_id
                      AND bb.solicitante_id = b.solicitante_id
                      AND bb.convocatoria_id <> 3)
               tipobeca,
            den_1,
            den_2,
            den_3,
            den_4,
            den_5,
            den_6
     FROM   bc2_becas b,
            bc2_convocatorias c,
            bc2_miembros m,
            bc2_solicitantes s,
            bc2_ext_personas_estudios e,
            bc2_diccionario dic,
            bc2_ext_estudios ee,
            bc2_domicilios domi,
            bc2_localidades loc,
            (  SELECT   beca_id,
                        MIN (
                           CASE WHEN orden = 1 THEN denegacion_id ELSE NULL END
                        )
                           den_1,
                        MIN (
                           CASE WHEN orden = 2 THEN denegacion_id ELSE NULL END
                        )
                           den_2,
                        MIN (
                           CASE WHEN orden = 3 THEN denegacion_id ELSE NULL END
                        )
                           den_3,
                        MIN (
                           CASE WHEN orden = 4 THEN denegacion_id ELSE NULL END
                        )
                           den_4,
                        MIN (
                           CASE WHEN orden = 5 THEN denegacion_id ELSE NULL END
                        )
                           den_5,
                        MIN (
                           CASE WHEN orden = 6 THEN denegacion_id ELSE NULL END
                        )
                           den_6
                 FROM   (SELECT   beca_id,
                                  denegacion_id,
                                  (ROW_NUMBER ()
                                      OVER (PARTITION BY beca_id
                                            ORDER BY denegacion_id))
                                     orden
                           FROM   bc2_becas_denegaciones)
             GROUP BY   beca_id) den
    WHERE       b.convocatoria_id = c.id
            AND c.organismo_id = 2
            AND b.solicitante_id = m.solicitante_id
            AND m.tipo_miembro_id = 1
            AND b.solicitante_id = s.id
            AND s.persona_id = e.persona_id
            AND b.estudio_id = e.estudio_id
            AND s.CURSO_ACADEMICO_id = e.curso_academico_id
            AND s.curso_academico_id = dic.curso_academico_id
            AND e.estudio_id = dic.valor_uji
            AND dic.clase_uji = 'Estudio'
            AND dic.organismo_id = c.organismo_id
            AND e.estudio_id = ee.id
            AND domi.tipo_domicilio_id IN
                     (SELECT   MIN (d.tipo_domicilio_id)
                        FROM   bc2_domicilios d, bc2_solicitantes ss
                       WHERE   d.solicitante_id = ss.id
                               AND ss.curso_academico_id =
                                     S.CURSO_ACADEMICO_ID
                               AND ss.id = b.solicitante_id)
            AND domi.localidad_id = loc.id
            AND domi.solicitante_id = s.id
            AND b.id = den.beca_id(+);



ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO
 ADD (CONCEDIDA  NUMBER                             DEFAULT 0                     NOT NULL);