CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS (ID,
                                                                  PERSONA_ID,
                                                                  ESTUDIO_ID,
                                                                  CURSO_ACADEMICO_ID,
                                                                  CREDITOS_MATRICULADOS,
                                                                  CREDITOS_SUPERADOS,
                                                                  CREDITOS_FALTANTES,
                                                                  CREDITOS_PENDIENTES_CONVA,
                                                                  CREDITOS_FUERA_BECA,
                                                                  CREDITOS_PRESENTADOS,
                                                                  LIMITAR_CREDITOS,
                                                                  LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                  MATRICULA_PARCIAL,
                                                                  NUMERO_BECAS,
                                                                  NUMERO_SEMESTRES,
                                                                  SIN_DOCENCIA,
                                                                  PROGRAMA_INTERCAMBIO,
                                                                  CREDITOS_CONVALIDADOS,
                                                                  CREDITOS_PARA_BECA,
                                                                  CURSO,
                                                                  TERCIO_PRESENTADO,
                                                                  RENDIMIENTO_ACADEMICO,
                                                                  NOTA_ACCESO,
                                                                  NOTA_MEDIA,
                                                                  FAMILIA_NUMEROSA_ID,
                                                                  MINUSVALIA,
                                                                  BECARIO,
                                                                  TIPO_MATRICULA
                                                                 ) AS
   select m.curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_tit_id estudio_id,
          m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_Aca, exp_per_id, exp_tit_id) creditos_matriculados,
          pack_exp.crd_sup_ca (exp_per_id, exp_tit_id, m.curso_aca) creditos_superados,
          pack_exp.crd_faltantes_ca (exp_per_id, exp_tit_id, m.curso_aca) creditos_faltantes,
          (select nvl (sum (a.creditos), 0)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_asignaturas a
           where  estado in ('A', 'C', 'R')
           and    cco_id = 0
           and    mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    asi_id = a.id) creditos_pendientes_conva,
          decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id),
                  0, 0,
                  pack_exp.imp_creditos_no_beca (exp_tit_id, exp_per_id, m.curso_aca)
                  / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id)
                 ) creditos_fuera_beca,
          (select sum (a.creditos)
           from   (select distinct asc_mat_exp_per_id, asc_mat_exp_tit_id, asc_mat_curso_aca, asc_asi_id
                   from            gra_exp.exp_notas) n,
                  gra_pod.pod_asignaturas a
           where  asc_mat_exp_per_id = m.exp_per_id
           and    asc_mat_exp_tit_id = m.exp_tit_id
           and    asc_mat_curso_aca = m.curso_aca
           and    asc_asi_id = a.id) creditos_presentados,
          0 limitar_creditos, 0 limitar_creditos_fin_estudios,
          (select decode (count (*), 0, 0, 1)
           from   exp_mat_circunstancias
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    cir_id = 3) matricula_parcial,
          (select count (*)
           from   gra_bec.bec_solicitudes
           where  curso_aca < m.curso_Aca
           and    per_id = m.exp_per_id
           and    tit_id = m.exp_tit_id) numero_becas,
          (select count (distinct semestre)
           from   gra_exp.exp_asi_cursadas ac,
                  gra_pod.pod_grupos p
           where  mat_exp_per_id = m.exp_per_id
           and    mat_exp_tit_id = m.exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    ac.asi_id = p.asi_id
           and    mat_curso_aca = p.curso_aca) numero_semestres,
          (select decode (count (*), 0, 0, 1)
           from   exp_asi_cursadas
           where  mat_exp_per_id = exp_per_id
           and    mat_exp_tit_id = exp_tit_id
           and    mat_curso_aca = m.curso_aca
           and    grp_id = 'Z') sin_docencia,
          (select decode (count (*), 0, 0, 1)
           from   gra_exp.exp_sol_convocatorias
           where  tipo = 'E'
           and    asc_mat_exp_per_id = exp_per_id
           and    asc_mat_exp_tit_id = exp_tit_id
           and    asc_mat_curso_aca = m.curso_aca) programa_intercambio,
          0 creditos_convalidados, 0 creditos_para_beca, 0 curso, 0 tercio_presentado, 0 rendimiento_academico,
          10 nota_acceso, 10 nota_media, 1 familia_numerosa, 1 minusvalia, 1 becario, tma.nombre tipo_matricula
   from   gra_exp.exp_matriculas m,
          exp_tipos_matricula tma
   where  m.curso_aca = tma.curso_aca
   and    m.tma_id = tma.id;

