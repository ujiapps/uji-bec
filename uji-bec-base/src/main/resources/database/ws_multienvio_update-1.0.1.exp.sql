Se ha generado el exquema

GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ACADEMICOSNCURSOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ACOGIMIENTOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ACTIVECONOMICASTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ARRENDATARIOSTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.AYUDATYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.CABECERAENVIOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DATOSFAMILIARESTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DATOSHACIENDATYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DATOSPERSONALESTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DECLARANTETYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DOCUMENTOSTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.DOMICILIOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ECONOMICOSTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ENVIOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ENVIOTYPEORDENPAGOANDERROROR_0 TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ERRORTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ESTADOCAUSATYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ESTADOSOLICITUDTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.FAMICATASTRALESTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.FAMILIARTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.GENERALESTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.MULTIENVIOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.NUEVOCURSOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ORDENPAGONIVELTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ORDENPAGOTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.PAGOSTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.SITUACIONSOLICITUDTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.TITULACIONESSSCCTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.TITULACIONTYPE TO UJI_BECAS;
GRANT SELECT,INSERT ON UJI_BECAS_WS_MULTIENVIO.ULTIMOCURSOTYPE TO UJI_BECAS;

GRANT SELECT ON UJI_BECAS_WS_MULTIENVIO.HIBERNATE_SEQUENCE TO UJI_BECAS;