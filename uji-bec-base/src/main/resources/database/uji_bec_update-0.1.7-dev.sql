ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (nota_acceso_cons  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (porc_superados_cons  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (coef_corrector_cons  NUMBER);

UPDATE bc2_minimos
  SET nota_acceso_cons = 6.5
WHERE curso_academico_id = 2012 AND
      tipo_estudio = 'M';

UPDATE bc2_minimos
  SET nota_acceso_cons = 5.5
WHERE curso_academico_id = 2012 AND
      tipo_estudio != 'M';

UPDATE bc2_minimos
  SET porc_superados_cons = 85
WHERE curso_academico_id = 2012 AND
      estudio_id IN (SELECT id
                     FROM bc2_ext_estudios
                     WHERE tipo IN ('G', '12C') and
                           (rama = 'HU' OR rama = 'SO'));

UPDATE bc2_minimos
  SET porc_superados_cons = 75
WHERE curso_academico_id = 2012 AND
      estudio_id IN (SELECT id
                     FROM bc2_ext_estudios
                     WHERE tipo IN ('G', '12C') and
                           (rama = 'EX' OR rama = 'SA'));

UPDATE bc2_minimos
  SET porc_superados_cons = 60
WHERE curso_academico_id = 2012 AND
      estudio_id IN (SELECT id
                     FROM bc2_ext_estudios
                     WHERE tipo IN ('G', '12C') and
                           (rama = 'TE'));

UPDATE bc2_minimos
  SET coef_corrector_mec = 1.17,
      coef_corrector_cons = 1.17
WHERE curso_academico_id = 2012 AND
      estudio_id IN (SELECT id
                     FROM bc2_ext_estudios
                     WHERE tipo IN ('G', '12C') and
                           titulacion_tecnica = 1);

UPDATE bc2_minimos
  SET coef_corrector_mec = 1,
      coef_corrector_cons = 1
WHERE curso_academico_id = 2012 AND
      estudio_id IN (SELECT id
                     FROM bc2_ext_estudios
                     WHERE tipo IN ('G', '12C') and
                           titulacion_tecnica = 0);

UPDATE bc2_minimos
  SET coef_corrector_mec = 1,
      coef_corrector_cons = 1
WHERE curso_academico_id = 2012 AND
      tipo_estudio = 'M';
	  
	  
/* Formatted on 24/05/2013 10:59 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_g (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_titus is
         select e.*
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;
   begin
      for x in c_titus loop
         if p_parte >= 1 then
            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id),
                creditos_superados = pack_bec.crd_sup (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes =
                   pack_exp.crd_faltantes_ca (x.persona_id, x.estudio_id, x.curso_academico_id - 1)
                   + pack_exp.crd_faltantes_cf_ca (x.curso_academico_id - 1, x.persona_id, x.estudio_id),
                creditos_pendientes_conva =
                                    gra_exp.crd_pte_convalidacion_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id)
                    - pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = pack_exp.crd_presentados (x.curso_academico_id, x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_exp.creditos_cf (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = gra_exp.limitar_creditos (x.persona_id, x.estudio_id, x.curso_academico_id),
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_exp.minusvalia_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'G'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_exp.crd_convalidados_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id),
                curso = gra_bec.pack_bec.curso_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_exp.nota_acceso_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = gra_exp.media_curso_beca (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_exp.familia_numerosa_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_exp.es_becario (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_EXP.TIPO_MATRICULA_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_EXP.ESTUDIO_ACCESO_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (gra_bec.alguna_vez_media_beca (x.persona_id, x.estudio_id), 'S', 1, 0),
                tiene_beca_mec =
                   decode (gra_bec.ES_BECARIO_CURSO (x.persona_id, x.estudio_id, x.curso_academico_id, 'MIN'),
                           'S', 1,
                           0
                          )
            where  x.id = e.id;

            commit;
         end if;
      end loop;
   end;
END sincroniza_datos_g;
	  

CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_pop (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_masters is
         select *
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;
   begin
      for x in c_masters loop
         if p_parte >= 1 then
            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_superados = gra_pop.crd_sup_becas (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes = gra_pop.crd_faltan_becas_ca (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_pendientes_conva =
                                        gra_pop.crd_pte_convalidacion (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id)
                    - gra_pop.crd_mat_becas (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = gra_pop.CRD_PRESENTADOS (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = 0,
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_pop.crd_complementos_pop (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;

            --FALTA CALCULAR MINUSVALIA
            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_pop.minusvalia_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'M'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_pop.crd_convalidados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = GRA_POP.CRD_MAT_BECAS (x.persona_id, x.estudio_id, x.curso_academico_id),
                curso = gra_pop.curso_mat_master (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_pop.nota_acceso (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = pack_pop.media_exp_beca (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_pop.familia_numerosa_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_pop.es_becario_pop (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_POP.TIPO_MATRICULA_POP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_POP.ESTUDIO_ACCESO (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (GRA_BEC.ALGUNA_VEZ_MEDIA_BECA (x.persona_id, x.estudio_id), 'S', 1, 0),
             tiene_beca_mec = decode(gra_bec.ES_BECARIO_CURSO(x.persona_id,  x.estudio_id,x.curso_academico_id,'MIN'),'S',1,0) 
            where  x.id = e.id;

            commit;
         end if;
      end loop;
   end;
END sincroniza_datos_pop;
	  