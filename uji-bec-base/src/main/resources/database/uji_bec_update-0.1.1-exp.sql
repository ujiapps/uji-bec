CREATE TABLE uji_becas.bc2_registro_envios 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER  NOT NULL , 
     envio_id NUMBER  , 
     tanda_id NUMBER  NOT NULL , 
     correcto NUMBER DEFAULT 0 CHECK ( correcto IN (0, 1))  NOT NULL , 
     fecha DATE  NOT NULL , 
     persona_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_registro_envios__IDXv2 ON uji_becas.bc2_registro_envios 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv3 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC , 
     organismo_id ASC , 
     envio_id ASC , 
     tanda_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_becas.bc2_registro_errores_envio 
    ( 
     id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     error VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_becas.bc2_registro_errores_IDX ON uji_becas.bc2_registro_errores_envio 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv1 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC , 
     beca_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_registro_errores_envio_PK PRIMARY KEY ( id ) ;



CREATE INDEX uji_becas.bc2_registro_errores_IDXv3 ON uji_becas.bc2_registro_errores_envio 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_errores_IDXv4 ON uji_becas.bc2_registro_errores_envio 
    ( 
     beca_id ASC 
    ) 
;



ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_reg_err_env_bec_FK FOREIGN KEY 
    ( 
     beca_id
    ) 
    REFERENCES uji_becas.bc2_becas 
    ( 
     id
    ) 
;



ALTER TABLE uji_becas.bc2_registro_errores_envio 
    ADD CONSTRAINT bc2_regi_err_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;


CREATE INDEX uji_becas.bc2_registro_envios__IDXv3v2 ON uji_becas.bc2_registro_envios 
    ( 
     curso_academico_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv4 ON uji_becas.bc2_registro_envios 
    ( 
     convocatoria_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv5 ON uji_becas.bc2_registro_envios 
    ( 
     tanda_id ASC 
    ) 
;
CREATE INDEX uji_becas.bc2_registro_envios__IDXv6 ON uji_becas.bc2_registro_envios 
    ( 
     persona_id ASC 
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_convoc_FK FOREIGN KEY 
    ( 
     convocatoria_id
    ) 
    REFERENCES uji_becas.bc2_convocatorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_cursos_aca_FK FOREIGN KEY 
    ( 
     curso_academico_id
    ) 
    REFERENCES uji_becas.bc2_cursos_academicos 
    ( 
     id
    ) 
;


ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_reg_env_tan_FK FOREIGN KEY 
    ( 
     tanda_id
    ) 
    REFERENCES uji_becas.bc2_tandas 
    ( 
     id
    ) 
;



ALTER TABLE UJI_BECAS.BC2_REGISTRO_ENVIOS
 ADD (estado  NUMBER                                DEFAULT 0                     NOT NULL)
;

ALTER TABLE UJI_BECAS.BC2_REGISTRO_ENVIOS DROP COLUMN CORRECTO;



ALTER TABLE uji_becas.bc2_registro_envios 
    ADD CONSTRAINT bc2_registro_envios_CK 
    CHECK (estado in (0,1,-1))
;


CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_ESTUDIOS
(
   ID,
   NOMBRE,
   TIPO,
   OFICIAL,
   NUMERO_CURSOS,
   CREDITOS_TOTALES,
   CREDITOS_POR_CURSO,
   SIN_DOCENCIA,
   EXTINGUIDA,
   PRESENCIAL,
   TITULACION_TECNICA,
   RAMA,
   CICLOS,
   CODIGO_CENTRO,
   TIPO_TITULO
)
AS
   SELECT   id,
            nombre,
            tipo,
            oficial,
            numero_cursos,
            creditos_totales,
            DECODE (numero_cursos,
                    NULL,
                    ROUND (creditos_totales / numero_cursos, 0))
               creditos_por_curso,
            sin_docencia,
            extinguida,
            presencial,
            titulacion_tecnica,
            rama,
            ciclos,
            codigo_centro,
            tipo_titulo
     FROM   (SELECT   t.id,
                      t.nombre,
                      tipo_estudio tipo,
                      DECODE (
                         tipo_estudio,
                         'G',
                         1,
                         '12C',
                         DECODE (t.id,
                                 51001, 0,
                                 51002, 0,
                                 51003, 0,
                                 100, 0,
                                 101, 0,
                                 103, 0,
                                 104, 0,
                                 1)
                      )
                         oficial,
                      (SELECT   COUNT ( * )
                         FROM   gra_pod.pod_cursos
                        WHERE   tit_id = t.id AND id <> 7)
                         numero_cursos,
                        crd_tr
                      + crd_ob
                      + crd_op
                      + NVL (crd_pfc, 0)
                      + DECODE (tipo_estudio, 'G', crd_le, 0)
                      - DECODE (t.id, 11, 32, 0)
                         creditos_totales,
                      DECODE (SIGN (NVL (curso_sin_docencia, 2013) - 2012),
                              1, 0,
                              1)
                         sin_docencia,
                      DECODE (activa, 'S', 0, 1) extinguida,
                      DECODE (activa, 'S', 1, 0) presencial,
                      DECODE (GRA_BEC.PACK_BEC.ES_TECNICA (t.id), 'S', 1, 0)
                         titulacion_tecnica,
                      NVL (t.rama, 'SO') rama,
                      t.tipo ciclos,
                      u.cod_mec codigo_centro,
                      DECODE (t.tipo_titulo,
                              '0H', '02',
                              '01', '01',
                              '02', '04',
                              '03', '04',
                              '04', '03',
                              '06', '05',
                              NULL)
                         tipo_titulo
               FROM   gra_pod.pod_titulaciones t,
                      gri_est.est_ubic_estructurales u,
                      (SELECT   34 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   32 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   4 tit_id, 2011 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   2 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   6 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   23 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   21 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   27 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   28 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   24 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   11 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   12 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   14 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   13 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   16 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   15 tit_id, 2012 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   10 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   1 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   29 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   35 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   7 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   17 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   30 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   25 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   33 tit_id, 2013 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   9 tit_id, 2014 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   26 tit_id, 2014 curso_sin_docencia FROM DUAL
                       UNION ALL
                       SELECT   31 tit_id, 2014 curso_sin_docencia FROM DUAL)
                      SIN
              WHERE   DECODE (t.id, 17, 2, 230, 2, t.uest_id) = u.id
                      AND t.id = SIN.tit_id(+)
             UNION ALL
             SELECT   m.id,
                      m.nombre,
                      'M' tipo,
                      DECODE (oficial, 'S', 1, 0),
                      DECODE (SIGN (creditos - 60), -1, 1, 0, 1, 2)
                         numero_cursos,
                      creditos creditos_totales,
                      DECODE (activo, 'S', 0, 1) sin_docencia,
                      DECODE (activo, 'S', 0, 1) extinguida,
                      DECODE (presencial, 'P', 1, 0) presencial,
                      0 titulacion_tecnica,
                      NVL (r.cod, 'SO') rama,
                      1 ciclos,
                      u.cod_mec codigo_centro,
                      '06' tipo_titulo
               FROM   gra_pop.pop_masters m, pod_ramas r,
                      gri_est.est_ubic_estructurales u
              WHERE   m.rama_id = r.id(+) and
                      u.id = m.uest_id
             UNION ALL
             SELECT   id,
                      nombre,
                      'DOC' tipo,
                      1 oficial,
                      1 numero_cursos,
                      0 creditos_totales,
                      DECODE (fin_docencia, NULL, 0, 1) sin_docencia,
                      DECODE (fin_docencia, NULL, 0, 1) extinguida,
                      1 presencial,
                      0 titulacion_tecnica,
                      NULL rama,
                      1 ciclos,
                      NULL codigo_centro,
                      '07' tipo_titulo
               FROM   gra_doc.doc_programas);
			   
		
alter table bc2_becas add (   
     titulo_espanol NUMBER CHECK ( titulo_espanol IN (0, 1)) ,
     estudios_homologados NUMBER CHECK ( estudios_homologados IN (0, 1))
    ); 
	
	
alter table uji_becas.bc2_becas add (
     univ_titulacion VARCHAR2 (10) , 
     ind_coef_corrector NUMBER DEFAULT 0 CHECK ( ind_coef_corrector IN (0, 1)) , 
     nota_corregida NUMBER
     ); 

--Carga de datos que faltaban de las solicitudes de master de curso 1         
update bc2_becas a
  set (titulo_espanol, estudios_homologados, codigo_estudio_ant, univ_titulacion) =
  (select decode(g.geneindtituespana,'S',1,0), decode(g.geneindestudioshomo,'S',1,0), 
          g.genecodtitulacion, g.genecodunivtitu
    from uji_becas_ws_solicitudes.generales g
    where g.identificacion = a.identificacion and
          g.codigo_archivo_temporal = a.codigo_archivo_temporal)
where a.id in (
select b.id
from bc2_becas b, bc2_ext_estudios e
where e.id = b.estudio_id and
      e.tipo = 'M' and
      b.curso = 1);	
	  
ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (cod_titu_acceso  VARCHAR2(10));


 