CREATE TABLE UJI_BECAS.BC2_BECAS_DOCUMENTOS
  (
    id                NUMBER NOT NULL ,
    beca_id           NUMBER NOT NULL ,
    tipo_documento_id VARCHAR2 (10 CHAR) NOT NULL ,
    nombre            VARCHAR2 (1000 CHAR) ,
    fichero BLOB ,
    mime_type       VARCHAR2 (1000 CHAR) ,
    fecha_solicita  DATE ,
    fecha_actualiza DATE ,
    validado        NUMBER
  ) ;
CREATE INDEX UJI_BECAS.bc2_becas_doc_doc_IDX ON UJI_BECAS.BC2_BECAS_DOCUMENTOS
  (
    tipo_documento_id ASC
  ) ;
CREATE INDEX UJI_BECAS.bc2_becas_doc_beca_IDX ON UJI_BECAS.BC2_BECAS_DOCUMENTOS
  (
    beca_id ASC
  ) ;
ALTER TABLE UJI_BECAS.BC2_BECAS_DOCUMENTOS ADD CONSTRAINT bc2_becas_documentos_PK PRIMARY KEY ( id ) ;
CREATE TABLE UJI_BECAS.BC2_TIPOS_DOCUMENTOS
  (
    id     VARCHAR2 (10 CHAR) NOT NULL ,
    nombre VARCHAR2 (1000 CHAR) NOT NULL ,
    orden  NUMBER
  ) ;
ALTER TABLE UJI_BECAS.BC2_TIPOS_DOCUMENTOS ADD CONSTRAINT bc2_tipos_documentos_PK PRIMARY KEY ( id ) ;
ALTER TABLE UJI_BECAS.BC2_BECAS_DOCUMENTOS ADD CONSTRAINT bc2_becas_doc_BECAS_FK FOREIGN KEY ( beca_id ) REFERENCES UJI_BECAS.BC2_BECAS ( ID ) ;
ALTER TABLE UJI_BECAS.BC2_BECAS_DOCUMENTOS ADD CONSTRAINT bc2_becas_doc_tipo_doc_FK FOREIGN KEY ( tipo_documento_id ) REFERENCES UJI_BECAS.BC2_TIPOS_DOCUMENTOS ( id ) ;


create view bc2_ext_mensajes as
select   m.id, m.agente_id, m.categoria_id, m.tipo, m.fecha_creacion, m.fecha_inicio, m.fecha_fin, m.referencia,
         m.remitente, m.reply_to, m.asunto, m.cuerpo, m.content_type, m.link, a.nombre agente, c.nombre categoria,
         (select wm_concat (nombre)
          from   uji_mensajeria.msg_destinos
          where  mensaje_id = m.id) enviado_a
from     uji_mensajeria.msg_mensajes m,
         uji_mensajeria.msg_agentes a,
         uji_mensajeria.msg_categorias c
where    agente_id = a.id
and      categoria_id = c.id
and      agente_id = 7;

/*
desde uji_mensajeria

grant select on uji_mensajeria.msg_destinos to uji_becas;
grant select on uji_mensajeria.msg_mensajes to uji_becas;
grant select on uji_mensajeria.msg_agentes to uji_becas;
grant select on uji_mensajeria.msg_categorias to uji_becas;

*/

DROP VIEW BC2_EXT_PERSONAS;

/* Formatted on 11/11/2015 12:17:33 (QP5 v5.115.810.9015) */
create or replace force view BC2_EXT_PERSONAS(ID, NOMBRE, IDENTIFICACION, SEXO, PERSONA_NOMBRE, PERSONA_APELLIDO1, PERSONA_APELLIDO2, FECHA_NACIMIENT, CORREO_UJI) as
   select id, rtrim(apellido1 || ' ' || apellido2) || ', ' || nombre nombre, identificacion, decode(SEXO, 1, 'H', 'M'), nombre persona_nombre, apellido1 persona_apellido1, apellido2 persona_apellido2, fecha fecha_nacimiento, busca_cuenta(id, 'N')
     from gri_per.per_personas p, gri_per.per_nacimIentos n
    where p.id = n.per_id(+);


GRANT SELECT ON BC2_EXT_PERSONAS TO CON_UJI_BEC;

GRANT SELECT ON BC2_EXT_PERSONAS TO GRA_BEC;

GRANT SELECT ON BC2_EXT_PERSONAS TO GRA_EXP;

GRANT SELECT ON BC2_EXT_PERSONAS TO UJI_BECAS_WS_SOLICITUDES_CON;


