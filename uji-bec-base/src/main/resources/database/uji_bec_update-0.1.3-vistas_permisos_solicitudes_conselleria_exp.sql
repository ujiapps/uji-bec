CREATE OR REPLACE PROCEDURE UJI_BECAS.pasar_becas_conselleria
IS
BEGIN
   DECLARE
      CURSOR solicitudes
      IS
         SELECT   s.nif,
                  s.personales_id,
                  DECODE (AUTORIZACION, 'S', 1, 0) autorizacion,
                  IDSOLICITUD,
                  DATOSESTUDIOS_SOLICITUD_HJID,
                  DATOSFAMILIARES_SOLICITUD_HJ_0,
                  DATOSPERSONALES_SOLICITUD_HJ_0,
                  SITUACIONESDECLARADAS_SOLICI_0,
                  SOLICITUD_SOLICITUDES_HJID
           FROM   uji_becas_ws_solicitudes_con.ws_v_con_solicitudes s,
                        uji_becas_ws_solicitudes_con.solicitud ss
          WHERE  s.personales_id = SS.DATOSPERSONALES_SOLICITUD_HJ_0;

      vnif_soli          VARCHAR2 (100);
      vid_solicitante    NUMBER;
      vid_beca           NUMBER;
      vestudio_id        NUMBER;
      vestudio_mec       NUMBER;
      vcontador          NUMBER;
      vpersona_id        NUMBER;
      vnum_beca_uji      NUMBER;
      vtipo_familia_id   NUMBER;
      vminus33           NUMBER;
      vminus65           NUMBER;
      vorfandad          NUMBER;
      vingresos          NUMBER;
      vtelefono          VARCHAR2 (20);
      vemail             VARCHAR2 (100);
      vmovil             VARCHAR2 (20);
   BEGIN
      vcontador := 0;

      delete bc2_becas where convocatoria_id = 3
      and solicitante_id in (select id from bc2_solicitantes where curso_academico_id = 2012);
      commit;
      FOR x IN solicitudes
      LOOP
         BEGIN
            BEGIN
               SELECT   id
                 INTO   vpersona_id
                 FROM   bc2_ext_personas
                WHERE   identificacion =
                           DECODE (x.nif,'X3871824G', '54283019Y',
                                   x.nif); --'X6543007J', 'X06543007J', '04229849B', '04229849',
            EXCEPTION
               WHEN OTHERS
               THEN
                  raise_application_error (-20000,  x.nif || ' nif no encontrdo ');
            END;

            BEGIN
               SELECT   id
                 INTO   vid_solicitante
                 FROM   bc2_solicitantes
                WHERE   persona_id = vpersona_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vid_solicitante := uji_becas_ws_multienvio.hibernate_sequence.NEXTVAL;


                  SELECT   MAX (numero_beca_uji) + 1
                    INTO   vnum_beca_uji
                    FROM   bc2_becas b, bc2_solicitantes s
                   WHERE   b.solicitante_id = s.id
                           AND s.curso_academico_id = 2012;

                  SELECT   telefono, email, movil
                    INTO   vtelefono, vemail, vmovil
                    FROM   uji_becas_ws_solicitudes_con.datospersonales
                   WHERE   HJID = x.personales_id;

                  SELECT   NVL (tipofamilianumerosa + 1, 1),
                           nminusvaliasuperior33,
                           nminusvaliasuperior65,
                           DECODE (orfandadsolicitante, 'S', 1, 0),
                           DECODE (hjid, 82770, 1200, 93311, 1545, 0)
                    INTO   vtipo_familia_id,
                           vminus33,
                           vminus65,
                           vorfandad,
                           vingresos
                    FROM   uji_becas_ws_solicitudes_con.situacionesdeclaradas
                   WHERE   hjid = x.situacionesdeclaradas_solici_0;


                  BEGIN
                     INSERT INTO bc2_solicitantes (id,
                                                      curso_academico_id,
                                                      persona_id,
                                                      numero_beca_uji,
                                                      profesion_sustentador,
                                                      telefono1,
                                                      telefono2,
                                                      email,
                                                      tipo_familia_id,
                                                      numero_miembros_computables,
                                                      numero_hermanos,
                                                      numero_hermanos_fuera,
                                                      numero_minusvalia_33,
                                                      numero_minusvalia_65,
                                                      orfandad,
                                                      unidad_familiar_independiente,
                                                      ingresos_netos,
                                                      ingresos_extranjero
                                )
                       VALUES   (vid_solicitante,
                                 2012,
                                 vpersona_id,
                                 vnum_beca_uji,
                                 NULL,
                                 vtelefono,
                                 vmovil,
                                 vemail,
                                 vtipo_familia_id,
                                 0,
                                 0,
                                 0,
                                 vminus33,
                                 vminus65,
                                 vorfandad,
                                 0,
                                 0,
                                 vingresos);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        raise_application_error (
                           -20000,'error insertar solicitante: '|| x.nif|| '---'
                           || vid_solicitante|| '---'|| 2012|| '---'|| vpersona_id|| '---'
                           || vnum_beca_uji  || '---'|| NULL|| '---'|| vtelefono  || '---'
                           || vmovil       || '---'|| vemail|| '---' || vtipo_familia_id
                           || '---'|| 0 || '---'|| 0 || '---'|| 0|| '--33' || vminus33
                           || '-65' || vminus65|| '---'|| vorfandad|| '-orf'|| 0|| '---'
                           || 0|| '-ing'|| vingresos|| '---'|| SQLERRM );
                  END;
            END;

            vid_beca := uji_becas_ws_multienvio.hibernate_sequence.NEXTVAL;



            BEGIN
               SELECT   valor_uji
                 INTO   vestudio_id
                 FROM   uji_becas_ws_solicitudes_con.datosestudios e,
                        bc2_diccionario d
                WHERE       x.DATOSESTUDIOS_SOLICITUD_HJID = e.hjid
                        AND d.clase_uji = 'Estudio'
                        AND e.codtitulacion = d.valor_origen
                        and curso_academico_id = 2012;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vestudio_id := NULL;
            END;

           begin

                INSERT INTO bc2_becas (
                                      id,
                                      solicitante_id,
                                      convocatoria_id,
                                      proceso_id,
                                      estado_id,
                                      tanda_id,
                                      tipo_identificacion_id,
                                      identificacion,
                                      codigo_beca,
                                      codigo_archivo_temporal,
                                      estudio_id,
                                      tipo_matricula_id,
                                      autorizacion_renta
                        )
                 VALUES   (
                           vid_beca,
                           vid_solicitante,
                           3,
                           1,
                           1,
                           NULL,
                           DECODE (
                              UPPER (SUBSTR (x.nif, 1, LENGTH (x.nif) - 1)),
                              LOWER (SUBSTR (x.nif, 1, LENGTH (x.nif) - 1)),
                              1,
                              2
                           ),
                           x.nif,
                           NULL,
                           x.idsolicitud,
                           vestudio_id,
                           1,
                           x.autorizacion
                          );
                          
           exception when others then
                raise_application_error (-20000,'error insertar beca: '|| x.nif|| '---'||sqlerrm);
           end;
            vcontador := vcontador + 1;
            COMMIT;
         END;
      END LOOP;

      raise_application_error (-20000, 'total : ' || vcontador);
   END;
END pasar_becas_conselleria;
/