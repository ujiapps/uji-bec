CREATE TABLE uji_becas.bc2_economicos 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     coeficiente_prelacion NUMBER , 
     ind_becario NUMBER , 
     umbral_becario NUMBER , 
     ind_compensa NUMBER , 
     umbral_compensa NUMBER , 
     ind_tasas NUMBER , 
     umbral_tasas NUMBER , 
     ind_mov_general NUMBER , 
     umbral_mov_general NUMBER , 
     ind_mov_especial NUMBER , 
     umbral_mov_especial NUMBER , 
     ind_resid_mov NUMBER , 
     umbral_resid_mov NUMBER , 
     ind_patrimonio NUMBER , 
     umbral_patrimonio NUMBER , 
     renta NUMBER , 
     capital_mobiliario NUMBER , 
     volumen_negocio NUMBER , 
     cod_repesca VARCHAR2 (10) , 
     deducciones NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_economicos__IDX ON uji_becas.bc2_economicos 
    ( 
     solicitante_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos__UN UNIQUE ( solicitante_id ) ;




ALTER TABLE uji_becas.bc2_economicos 
    ADD CONSTRAINT bc2_economicos_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    ) 
;


DROP VIEW UJI_BECAS.BC2_V_RESOL_ECONOMICOS;

CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_RESOL_ECONOMICOS
(
   ID,
   MULTIENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TANDA,
   SOLICITUD_ID,
   BECA_ID,
   ARCHIVO_TEMPORAL,
   NIF,
   ECONCAPITAL,
   ECONCODREPESCA,
   ECONCOEFPRELACION,
   ECONDEDUCCIONES,
   ECONIMPMODBECARIO,
   ECONIMPMODCOMPEN,
   ECONIMPMODMOVLESPC,
   ECONIMPMODMOVLGEN,
   ECONIMPMODRESID,
   ECONIMPMODTASAS,
   ECONIMPMODI,
   ECONINDICEB,
   ECONINDICEC,
   ECONINDICEE,
   ECONINDICEI,
   ECONINDICEM,
   ECONINDICER,
   ECONINDICET,
   ECONNEGOCIO,
   ECONRENTA
)
AS
   SELECT   E.HJID ID,
            BE.MULTIENVIO_ID,
            BE.LOTE,
            BE.CONVOCATORIA,
            BE.CURSO_ACADEMICO,
            BE.TANDA,
            BE.SOLICITUD_ID,
            BE.BECA_ID,
            BE.ARCHIVO_TEMPORAL,
            BE.NIF,
            E.ECONCAPITAL,
            E.ECONCODREPESCA,
            E.ECONCOEFPRELACION,
            E.ECONDEDUCCIONES,
            E.ECONIMPMODBECARIO,
            E.ECONIMPMODCOMPEN,
            E.ECONIMPMODMOVLESPC,
            E.ECONIMPMODMOVLGEN,
            E.ECONIMPMODRESID,
            E.ECONIMPMODTASAS,
            E.ECONIMPMODI,
            E.ECONINDICEB,
            E.ECONINDICEC,
            E.ECONINDICEE,
            E.ECONINDICEI,
            E.ECONINDICEM,
            E.ECONINDICER,
            E.ECONINDICET,
            E.ECONNEGOCIO,
            E.ECONRENTA
     FROM   UJI_BECAS.BC2_V_RESOL_BECAS_ESTADO BE,
            UJI_BECAS_WS_MULTIENVIO.ECONOMICOSTYPE E
    WHERE   BE.ECONOMICOS_ID = E.HJID;
 
INSERT INTO bc2_economicos 
  (id, solicitante_id, coeficiente_prelacion, ind_becario, umbral_becario, 
   ind_compensa, umbral_compensa, ind_tasas, umbral_tasas, ind_mov_general, 
   umbral_mov_general, ind_mov_especial, umbral_mov_especial, ind_resid_mov, 
   umbral_resid_mov, ind_patrimonio, umbral_patrimonio, renta, capital_mobiliario, 
   volumen_negocio, cod_repesca, deducciones) 
SELECT DISTINCT
    e.beca_id, s.id solicitante_id, e.econcoefprelacion, decode(e.econindiceb,null,null,'S',1,0), E.ECONIMPMODBECARIO,
    decode(e.econindicec,null,null,'S',1,0), e.econimpmodcompen, decode(e.econindicet,null,null,'S',1,0), e.econimpmodtasas, decode(e.econindicem,null,null,'S',1,0),
    e.econimpmodmovlgen, decode(e.econindicee,null,null,'S',1,0), e.econimpmodmovlespc, decode(e.econindicer,null,null,'S',1,0),
    e.econimpmodresid, decode(e.econindicei,null,null,'S',1,0), e.econimpmodi, e.econrenta, e.econcapital,
    e.econnegocio, e.econcodrepesca, e.econdeducciones
  FROM BC2_V_RESOL_ECONOMICOS E, BC2_BECAS B, BC2_SOLICITANTES S
  WHERE B.ID = E.BECA_ID AND
        S.ID = B.SOLICITANTE_ID; 

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (ccaa_familia_numerosa_id  NUMBER);

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (fecha_fin_familia_numerosa  DATE);

ALTER TABLE UJI_BECAS.BC2_SOLICITANTES
 ADD (fecha_orfandad  DATE);


CREATE INDEX uji_becas.bc2_solicitantes_ccaa_IDX ON uji_becas.bc2_solicitantes 
    ( 
     ccaa_familia_numerosa_id ASC 
    ) 
;


ALTER TABLE uji_becas.bc2_solicitantes 
    ADD CONSTRAINT bc2_sol_ccaa_FK FOREIGN KEY 
    ( 
     ccaa_familia_numerosa_id
    ) 
    REFERENCES uji_becas.bc2_ccaa 
    ( 
     id
    ) 
;

/* se ha escapado la actualizacion de la tabla no habria que lanzar esta accion en explotacion */
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_complementos  NUMBER                 DEFAULT 0);

 
/* esto si hay que hacerlo */ 
declare
   cursor lista is
      select *
      from   (select pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
                     gra_pop.crd_complementos_pop (m.pexp_per_id, m.pexp_pmas_id) crd_complementos
              from   pop_matriculas m,
                     pop_tipos_matricula tm,
                     pop_est_matriculas em
              where  m.curso_aca = tm.curso_aca
              and    m.ptm_id = tm.id
              and    m.pexp_per_id = em.pmat_pexp_per_id(+)
              and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
              and    m.curso_aca = em.pmat_curso_aca(+)
              union all
              select exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
                     gra_exp.crd_complementos (e.per_id, e.tit_id) crd_complementos
              from   exp_expedientes e,
                     exp_matriculas m,
                     exp_tipos_matricula tm,
                     exp_est_matriculas em
              where  e.per_id = m.exp_per_id
              and    e.tit_id = m.exp_tit_id
              and    m.tma_id = tm.id
              and    m.curso_aca = tm.curso_aca
              and    m.exp_per_id = em.mat_exp_per_id(+)
              and    m.exp_tit_id = em.mat_exp_tit_id(+)
              and    m.curso_aca = em.mat_curso_aca(+))
      where  crd_complementos <> 0;
begin
   for x in lista loop
      update uji_becas.bc2_ext_personas_estudios
      set creditos_complementos = x.crd_complementos
      where  persona_id = x.persona_id
      and    estudio_id = x.estudio_id
      and    curso_academico_id = x.curso_academico_id;

      commit;
   end loop;
end;

CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_VW_EXT_PERSONAS_ESTUDIOS (ID,
                                                                     PERSONA_ID,
                                                                     ESTUDIO_ID,
                                                                     CURSO_ACADEMICO_ID,
                                                                     CREDITOS_MATRICULADOS,
                                                                     CREDITOS_SUPERADOS,
                                                                     CREDITOS_FALTANTES,
                                                                     CREDITOS_PENDIENTES_CONVA,
                                                                     CREDITOS_FUERA_BECA,
                                                                     CREDITOS_PRESENTADOS,
                                                                     LIMITAR_CREDITOS,
                                                                     LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                     MATRICULA_PARCIAL,
                                                                     NUMERO_BECAS_MEC,
                                                                     NUMERO_BECAS_CONS,
                                                                     MINUSVALIA,
                                                                     NUMERO_SEMESTRES,
                                                                     PROGRAMA_INTERCAMBIO,
                                                                     CREDITOS_CONVALIDADOS,
                                                                     CREDITOS_PARA_BECA,
                                                                     CURSO,
                                                                     NOTA_ACCESO,
                                                                     NOTA_MEDIA,
                                                                     FAMILIA_NUMEROSA_ID,
                                                                     BECARIO,
                                                                     TIPO_MATRICULA,
                                                                     ESTUDIO_ACCESO_ID,
                                                                     SIN_DOCENCIA,
                                                                     POSEE_TITULO,
                                                                     CREDITOS_COMPLEMENTOS
                                                                    ) AS
   select *
   from   (select rownum id, pexp_per_id persona_id, pexp_pmas_id estudio_id, m.curso_aca curso_academico_id,
                  pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_exp,
                  -- todos ,
                  GRA_POP.CRD_sup_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_superados_bec,
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  gra_pop.crd_faltan_ca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_faltan_ca,
                  gra_pop.CRD_PTE_CONVALIDACION (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_pendiente_conva,
                  (pack_pop.crd_matriculados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
                   - GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca)
                  ),                                                                                   -- crd_fuera_becs
                  gra_pop.CRD_PRESENTADOS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) crd_PRESENTADOS,
                  0 Limitar_creditos, '0' limitar_creditos_fin_estudios, '0' matricula_parcial,
                  -- nos deberia dar igual pues es la de matricula
                  gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'MIN', m.pexp_pmas_id) numero_becas_mec,
                  -- modificar para master,
                  gra_bec.pack_bec_2011.numbecas (m.pexp_per_id, m.curso_aca, 'CON', m.pexp_pmas_id) numero_becas_cons,
                  decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,                           -- modificar para master,
                  decode (gra_bec.pack_bec_2011.crd_mismo_semestre (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca, 'G'),
                          'N', 2,
                          1
                         ) numero_semestres,
                  decode (gra_bec.pack_bec_2011.erasmus (m.pexp_per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
                  gra_pop.crd_convalidados (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_convalidados,
                  GRA_POP.CRD_MAT_BECAS (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) creditos_matriculados_bec,
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  gra_pop.curso_mat_master (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) curso,
                  gra_pop.nota_acceso (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca) nota_acceso,
                  pack_pop.media_exp_beca (m.pexp_per_id, m.pexp_pmas_id, 'S') nota_media,
                  --decode(gra_bec.pack_bec.media_beca (m.pexp_per_id, m.pexp_pmas_id, m.curso_aca),'N',0,1) media_beca, -- se podria calcular en la logica de negocio????
                  decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
                  decode (pack_pop.es_becario (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca), 'S', 1, 0) es_becario,
                  tm.nombre tipo_matricula,
                  GRA_POP.ESTUDIO_ACCESO (m.pexp_pmas_id, m.pexp_per_id, m.curso_aca) estudio_acceso_id, 0 sin_docencia,
                  GRA_TIT.POSEE_TITULO (m.pexp_per_id, m.pexp_pmas_id) posee_titulo,
                  gra_pop.crd_complementos_pop (m.pexp_per_id, m.pexp_pmas_id) crd_complementos
           from   pop_matriculas m,
                  pop_tipos_matricula tm,
                  pop_est_matriculas em
           where  m.curso_aca = tm.curso_aca
           and    m.ptm_id = tm.id
           and    m.pexp_per_id = em.pmat_pexp_per_id(+)
           and    m.pexp_pmas_id = em.pmat_pexp_pmas_id(+)
           and    m.curso_aca = em.pmat_curso_aca(+)
           union all
           select rownum id, exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
                  pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id) creditos_matriculados_exp,      -- todos ,
                                 --   pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) creditos_matriculados_bec,
                  -- menos adapataso comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
                  pack_bec.crd_sup (e.per_id, e.tit_id, m.curso_aca) creditos_superados,
                  -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion no se estan calculando),
                  pack_exp.crd_faltantes_ca (e.per_id, e.tit_id, m.curso_aca) creditos_faltantes,
                  gra_exp.crd_pte_convalidacion_exp (e.per_id, e.tit_id, m.curso_aca) creditos_pendientes_conva,
                  (pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id)
                   - pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca)
                  ) creditos_fuera_beca,
                  pack_exp.crd_presentados (m.curso_aca, e.per_id, e.tit_id) creditos_presentados,
                  gra_exp.limitar_creditos (e.per_id, e.tit_id, m.curso_aca) limitar_creditos,
                  '0' limitar_creditos_fin_estudios, '0' matricual_parcial,
                  -- nos deberia dar igual pues es la de matricula,
                  gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'MIN', e.tit_id) numero_becas_min,
                  gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'CON', e.tit_id) numero_becas_cons,
                  decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,
                  decode (gra_bec.pack_bec_2011.crd_mismo_semestre (e.per_id, e.tit_id, m.curso_aca, 'G'),
                          'N', 2,
                          1
                         ) numero_semestres,
                  decode (gra_bec.pack_bec_2011.erasmus (e.per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
                  gra_exp.crd_convalidados_exp (e.per_id, e.tit_id, m.curso_aca) creditos_convalidados,
                  pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) CREDITOS_PARA_BECA,
                  gra_bec.pack_bec.curso_mat (e.per_id, e.tit_id, m.curso_aca) curso,
                  to_number (replace (e.nau, '.', ',')) nota_acceso,
                  gra_exp.media_exp_beca (m.exp_per_id, m.exp_tit_id, m.curso_aca) nota_media,
                  decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
                  --decode(gra_bec.pack_bec.media_beca (e.per_id, e.tit_id, m.curso_aca),'S',1,0) media_beca, -- se podria calcular en la logica de negocio????
                  decode (gra_exp.es_becario (m.exp_per_id, m.exp_tit_id, m.curso_aca), 'S', 1, 0),
                  tm.nombre tipo_matricula, '' estudio_acceso_id, 0 sin_docencia, 0 posee_titulo,
                  gra_exp.crd_complementos (e.per_id, e.tit_id) crd_complementos
           from   exp_expedientes e,
                  exp_matriculas m,
                  exp_tipos_matricula tm,
                  exp_est_matriculas em
           where  e.per_id = m.exp_per_id
           and    e.tit_id = m.exp_tit_id
           and    m.tma_id = tm.id
           and    m.curso_aca = tm.curso_aca
           and    m.exp_per_id = em.mat_exp_per_id(+)
           and    m.exp_tit_id = em.mat_exp_tit_id(+)
           and    m.curso_aca = em.mat_curso_aca(+));


CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_g (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_titus is
         select e.*
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;
   begin
      for x in c_titus loop
         if p_parte >= 1 then
            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id),
                creditos_superados = pack_bec.crd_sup (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes = pack_exp.crd_faltantes_ca (x.persona_id, x.estudio_id, x.curso_academico_id - 1),
                creditos_pendientes_conva =
                                    gra_exp.crd_pte_convalidacion_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id)
                    - pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = pack_exp.crd_presentados (x.curso_academico_id, x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_exp.creditos_cf (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = gra_exp.limitar_creditos (x.persona_id, x.estudio_id, x.curso_academico_id),
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_exp.minusvalia_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'G'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_exp.crd_convalidados_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id),
                curso = gra_bec.pack_bec.curso_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_exp.nota_acceso_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = gra_exp.media_curso_beca (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_exp.familia_numerosa_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_exp.es_becario (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_EXP.TIPO_MATRICULA_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_EXP.ESTUDIO_ACCESO_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (gra_bec.alguna_vez_media_beca (x.persona_id, x.estudio_id), 'S', 1, 0),
                tiene_beca_mec =
                   decode (gra_bec.ES_BECARIO_CURSO (x.persona_id, x.estudio_id, x.curso_academico_id, 'MIN'),
                           'S', 1,
                           0
                          )
            where  x.id = e.id;

            commit;
         end if;
      end loop;
   end;
END sincroniza_datos_g;


CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_pop (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_masters is
         select *
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;
   begin
      for x in c_masters loop
         if p_parte >= 1 then
            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_superados = gra_pop.crd_sup_becas (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes = gra_pop.crd_faltan_ca (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_pendientes_conva =
                                        gra_pop.crd_pte_convalidacion (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id)
                    - gra_pop.crd_mat_becas (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = gra_pop.CRD_PRESENTADOS (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = 0,
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_pop.crd_complementos_pop (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;

            --FALTA CALCULAR MINUSVALIA
            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_pop.minusvalia_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'M'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_pop.crd_convalidados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = GRA_POP.CRD_MAT_BECAS (x.persona_id, x.estudio_id, x.curso_academico_id),
                curso = gra_pop.curso_mat_master (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_pop.nota_acceso (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = pack_pop.media_exp_beca (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_pop.familia_numerosa_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_pop.es_becario_pop (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_POP.TIPO_MATRICULA_POP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_POP.ESTUDIO_ACCESO (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (GRA_BEC.ALGUNA_VEZ_MEDIA_BECA (x.persona_id, x.estudio_id), 'S', 1, 0),
             tiene_beca_mec = decode(gra_bec.ES_BECARIO_CURSO(x.persona_id,  x.estudio_id,x.curso_academico_id,'MIN'),'S',1,0) 
            where  x.id = e.id;

            commit;
         end if;
      end loop;
   end;
END sincroniza_datos_pop;

-- Actualizar datos de familia numerosa
DECLARE
  CURSOR SOLICITANTES IS
    SELECT DISTINCT C.IDENTIFICACION, C.CODIGO_ARCHIVO_TEMPORAL, S.* 
    FROM BC2_SOLICITANTES S, BC2_BECAS C
    WHERE C.SOLICITANTE_ID = S.ID AND
      C.CONVOCATORIA_ID IN (1,2);
      
BEGIN
  FOR rsoli in solicitantes LOOP
    
    UPDATE BC2_SOLICITANTES
      set (carnet_familia_numerosa, ccaa_familia_numerosa_id, fecha_fin_familia_numerosa, fecha_orfandad) =
      (select SISOCARNETFAMNUM, SISOCCAAFAMNUM, SISOFECHAFINFAMNUMITEM, SISOFECHAFALLULTPROGITEM
       from uji_becas_ws_solicitudes.situacionsolicitud ss
       where ss.identificacion = rsoli.identificacion AND
             ss.codigo_archivo_temporal = rsoli.codigo_archivo_temporal
              )
    where id = rsoli.id;
  END LOOP;
  
  commit;
END;
