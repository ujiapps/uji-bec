CREATE INDEX UJI_BECAS.BC2_MIEMBROS_EC_IDX ON UJI_BECAS.BC2_MIEMBROS
(ESTADO_CIVIL_ID)
LOGGING
NOPARALLEL;

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_ACCESO_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (PORC_SUPERADOS_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (COEF_CORRECTOR_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (PORC_RENDIMIENTO_MEC  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_RENDIMIENTO_MEC_1 NUMBER);

ALTER TABLE UJI_BECAS.BC2_MINIMOS
 ADD (NOTA_RENDIMIENTO_MEC_2  NUMBER);

UPDATE BC2_MINIMOS SET NOTA_ACCESO_MEC = 5.5
WHERE TIPO_ESTUDIO IN ('12C','G') ;

UPDATE BC2_MINIMOS SET NOTA_ACCESO_MEC = 6
WHERE TIPO_ESTUDIO ='M';

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 100
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 65
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA = 'TE');

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 80
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA IN ('SA','EX'));

UPDATE BC2_MINIMOS SET PORC_SUPERADOS_MEC = 90
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA IN ('SO','HU'));

UPDATE BC2_MINIMOS SET COEF_CORRECTOR_MEC = 1.17
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET COEF_CORRECTOR_MEC = 1
WHERE TIPO_ESTUDIO <> 'M';

UPDATE BC2_MINIMOS SET PORC_RENDIMIENTO_MEC = 85
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA = 'TE');

UPDATE BC2_MINIMOS SET PORC_RENDIMIENTO_MEC = 100
WHERE TIPO_ESTUDIO <> 'M'
AND ESTUDIO_ID IN ( SELECT ID FROM BC2_EXT_ESTUDIOS WHERE RAMA <>'TE');

UPDATE BC2_MINIMOS SET NOTA_RENDIMIENTO_MEC_1 = 8
WHERE TIPO_ESTUDIO = 'M';

UPDATE BC2_MINIMOS SET NOTA_RENDIMIENTO_MEC_2 = 8.5
WHERE TIPO_ESTUDIO = 'M';

ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO
 ADD (CONVOCATORIA_ID  NUMBER);
 
UPDATE BC2_BECAS_HISTORICO
SET CONVOCATORIA_ID = 1;
 
ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO
MODIFY(CONVOCATORIA_ID  NOT NULL);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
MODIFY(TIPO_IDENTIFICACION_ID  NULL);

ALTER TABLE UJI_BECAS.BC2_EXT_ESTUDIOS
 ADD (ciclos  NUMBER);

update bc2_ext_estudios set ciclos=2 where id = 9;
update bc2_ext_estudios set ciclos=2 where id = 17;
update bc2_ext_estudios set ciclos=2 where id = 52;
update bc2_ext_estudios set ciclos=2 where id = 10;
update bc2_ext_estudios set ciclos=2 where id = 11;
update bc2_ext_estudios set ciclos=1 where id = 20;
update bc2_ext_estudios set ciclos=1 where id = 16;
update bc2_ext_estudios set ciclos=2 where id = 25;
update bc2_ext_estudios set ciclos=2 where id = 12;
update bc2_ext_estudios set ciclos=1 where id = 210;
update bc2_ext_estudios set ciclos=1 where id = 211;
update bc2_ext_estudios set ciclos=1 where id = 212;
update bc2_ext_estudios set ciclos=1 where id = 213;
update bc2_ext_estudios set ciclos=1 where id = 214;
update bc2_ext_estudios set ciclos=1 where id = 215;
update bc2_ext_estudios set ciclos=1 where id = 216;
update bc2_ext_estudios set ciclos=1 where id = 101;
update bc2_ext_estudios set ciclos=2 where id = 1;
update bc2_ext_estudios set ciclos=1 where id = 4;
update bc2_ext_estudios set ciclos=9 where id = 102;
update bc2_ext_estudios set ciclos=1 where id = 65;
update bc2_ext_estudios set ciclos=1 where id = 2;
update bc2_ext_estudios set ciclos=1 where id = 217;
update bc2_ext_estudios set ciclos=1 where id = 218;
update bc2_ext_estudios set ciclos=1 where id = 14;
update bc2_ext_estudios set ciclos=1 where id = 53;
update bc2_ext_estudios set ciclos=2 where id = 54;
update bc2_ext_estudios set ciclos=1 where id = 55;
update bc2_ext_estudios set ciclos=2 where id = 56;
update bc2_ext_estudios set ciclos=2 where id = 58;
update bc2_ext_estudios set ciclos=2 where id = 59;
update bc2_ext_estudios set ciclos=1 where id = 60;
update bc2_ext_estudios set ciclos=1 where id = 61;
update bc2_ext_estudios set ciclos=1 where id = 62;
update bc2_ext_estudios set ciclos=1 where id = 64;
update bc2_ext_estudios set ciclos=1 where id = 50;
update bc2_ext_estudios set ciclos=9 where id = 70;
update bc2_ext_estudios set ciclos=9 where id = 71;
update bc2_ext_estudios set ciclos=9 where id = 72;
update bc2_ext_estudios set ciclos=9 where id = 73;
update bc2_ext_estudios set ciclos=9 where id = 74;
update bc2_ext_estudios set ciclos=9 where id = 100;
update bc2_ext_estudios set ciclos=1 where id = 231;
update bc2_ext_estudios set ciclos=1 where id = 232;
update bc2_ext_estudios set ciclos=1 where id = 24;
update bc2_ext_estudios set ciclos=3 where id = 29;
update bc2_ext_estudios set ciclos=2 where id = 33;
update bc2_ext_estudios set ciclos=1 where id = 34;
update bc2_ext_estudios set ciclos=2 where id = 35;
update bc2_ext_estudios set ciclos=9 where id = 51001;
update bc2_ext_estudios set ciclos=3 where id = 30;
update bc2_ext_estudios set ciclos=1 where id = 103;
update bc2_ext_estudios set ciclos=1 where id = 23;
update bc2_ext_estudios set ciclos=1 where id = 6;
update bc2_ext_estudios set ciclos=2 where id = 7;
update bc2_ext_estudios set ciclos=2 where id = 8;
update bc2_ext_estudios set ciclos=1 where id = 13;
update bc2_ext_estudios set ciclos=1 where id = 15;
update bc2_ext_estudios set ciclos=2 where id = 18;
update bc2_ext_estudios set ciclos=1 where id = 22;
update bc2_ext_estudios set ciclos=1 where id = 27;
update bc2_ext_estudios set ciclos=9 where id = 104;
update bc2_ext_estudios set ciclos=2 where id = 31;
update bc2_ext_estudios set ciclos=1 where id = 32;
update bc2_ext_estudios set ciclos=1 where id = 63;
update bc2_ext_estudios set ciclos=2 where id = 3;
update bc2_ext_estudios set ciclos=2 where id = 51;
update bc2_ext_estudios set ciclos=2 where id = 57;
update bc2_ext_estudios set ciclos=9 where id = 51002;
update bc2_ext_estudios set ciclos=1 where id = 5;
update bc2_ext_estudios set ciclos=2 where id = 19;
update bc2_ext_estudios set ciclos=1 where id = 21;
update bc2_ext_estudios set ciclos=2 where id = 26;
update bc2_ext_estudios set ciclos=1 where id = 28;
update bc2_ext_estudios set ciclos=9 where id = 51003;
update bc2_ext_estudios set ciclos=1 where id = 201;
update bc2_ext_estudios set ciclos=1 where id = 202;
update bc2_ext_estudios set ciclos=1 where id = 203;
update bc2_ext_estudios set ciclos=1 where id = 204;
update bc2_ext_estudios set ciclos=1 where id = 205;
update bc2_ext_estudios set ciclos=1 where id = 206;
update bc2_ext_estudios set ciclos=1 where id = 207;
update bc2_ext_estudios set ciclos=1 where id = 208;
update bc2_ext_estudios set ciclos=1 where id = 209;
update bc2_ext_estudios set ciclos=1 where id = 219;
update bc2_ext_estudios set ciclos=1 where id = 220;
update bc2_ext_estudios set ciclos=1 where id = 221;
update bc2_ext_estudios set ciclos=1 where id = 222;
update bc2_ext_estudios set ciclos=1 where id = 223;
update bc2_ext_estudios set ciclos=1 where id = 224;
update bc2_ext_estudios set ciclos=1 where id = 225;
update bc2_ext_estudios set ciclos=1 where id = 226;
update bc2_ext_estudios set ciclos=1 where id = 227;
update bc2_ext_estudios set ciclos=1 where id = 228;
update bc2_ext_estudios set ciclos=1 where id = 229;
update bc2_ext_estudios set ciclos=1 where id = 230;

update bc2_ext_estudios set ciclos = 1 where ciclos is null;

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (ESTADO  VARCHAR2(2));

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (CAUSA  VARCHAR2(5));

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES
 ADD (SUBCAUSA  VARCHAR2(2));

DELETE FROM BC2_DENEGACIONES;
 
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('47', 'Superar els llindars de patrimoni, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('51', 'Falta el titular principal de la declaración de la renta a la que pertenece el solicitante o algun familiar', '1', 1, '10', '04', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('60', 'Por existir varias delcaraciones ante la AEAT con respecto algun miembro de la unidad familar', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('9', 'No complir l''edat permesa que s''estableix en la convocatòria.', '1', 1, '10', '01', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('16', 'No trobar-se en situació legal d''atur', '1', 1, '10', '01', '51');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('11', 'Existència de sol·licituds en les quals concorren circumstàncies per a una assignació preferent d''acord amb els criteris establits en les bases de la convocatòria.', '1', 1, '10', '01', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('17', 'Per no estar en possessió de títol universitari', '1', 1, '10', '01', '53');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('18', 'Per no acreditar el sol·licitant l''edat requerida en les bases de la corresponent convocatòria', '1', 1, '10', '01', '54');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('19', 'Per no acreditar el sol·licitant la situació legal d''atur requerida en les bases de la corresponent convocatòria.', '1', 1, '10', '01', '55');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('20', 'Per no acreditar el sol·licitant la situació de perceptor de prestacions d''atur requerida en les bases de la corresponent convocatòria.', '1', 1, '10', '01', '56');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('21', 'Per no estar matriculat en estudis emparats per la corresponent convocatòria', '1', 1, '10', '01', '57');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('22', 'No estar matriculat en el curs anterior en un curs complet de Màster oficial.', '1', 1, '10', '01', '58');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('55', 'Per haver inclòs com membre computable de la seua unitat familiar a un perceptor de Renda Bàsica d''Emancipació.', '1', 1, '10', '04', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('41', 'En el cas d''alumnat universitari que realitza estudis no presencials, no tenir el mínim de crèdits aprovats o matriculats.', '1', 1, '10', '03', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('13', 'Haver causat baixa en el centre d''estudis abans de la finalització del curs.', '1', 1, '10', '01', '31');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('14', 'Tenir els serveis coberts pel Govern de la comunitat autònoma', '1', 1, '10', '01', '32');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('57', 'No está o no ha estado percibiendo prestación o subsidio por desempleo concargo al presupuestodel SPE a la fecha que exige la convocatória', '1', 1, '10', '04', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('45', 'No estar matriculat en el curs actual en el segon curs dels mateixos estudis per als quals va obtenir beca el curs anterior', '1', 1, '10', '03', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('56', 'No se encuentra en situación laboral de desempleo e inscrito como demandandate de empleo a fecha de la convocatória.', '1', 1, '10', '04', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('63', 'Presumpta ocultació de membres computables de la seva família i dades econòmiques', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('58', 'Superar els llindars en acumulació de patrimoni, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('33', 'Per superar els llindars de valors cadastrals, després de consultar les dades que sobre la seva unitat familiar obren en poder de l''Administració tributària', '1', 1, '10', '02', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('69', 'Supera valors cadastrals', '2', 1, '10', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('54', 'Superar els llindars dels valors catastrals, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('50', 'Superar el volum de negoci', '1', 1, '10', '04', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('52', 'Falten familiars a efectes fiscals', '1', 1, '10', '04', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('65', 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', '2', 1, '10', '1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('2', 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', '1', 1, '10', '01', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('80', 'Posseir títol que l''habilite per a l''exercici professional o estar en disposició legal de obtenir-lo i/o no ser la beca o ajuda que sol·licita per a cursar estudis d''un cicle o grau superior als dels ensenyaments cursats.', '2', 1, '10', '2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('46', 'Superar els llindars de renda, després de consultar les dades econòmiques de la seva família en la AEAT', '1', 1, '10', '04', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('85', 'Superar els llindars de renda establits per a la concesió de beca', '2', 1, '10', '3.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('1', 'Sol·licitar ajuda per a estudis no emparats per la convocatòria', '1', 1, '10', '01', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('25', 'Superar els llindars de renda establits per a l''exempció de preus per serveis acadèmics o beca bàsica', '1', 1, '10', '02', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('24', 'Superar els llindars establits per a la concessió de la beca', '1', 1, '10', '02', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('26', 'No tindre dret a l''excepció de taxes i superar el primer llindar económic de renda disponible (cent.privat)', '1', 1, '10', '02', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('37', 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', '1', 1, '10', '03', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('87', 'No arribar a la nota mitjana mínima exigida entre les convocatòries de juny i setembre.', '2', 1, '10', '4');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('38', 'No haver aprovat el nombre mínim d''assignatures o crèdits establits en les bases de la  convocatòria', '1', 1, '10', '03', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('88', 'No haver aprovat el nombre mínim d''assignatures o crèdits establits en les bases de la  convocatòria', '2', 1, '10', '5');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('3', 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució d''aquesta, a pesar d''haver-se-li requerit', '1', 1, '10', '01', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('89', 'No consignar en la sol·licitud les dades bàsiques o no haver aportat la documentació necessària per a la resolució de la convocatòria, a pesar d''haver-se-li requerit', '2', 1, '10', '6');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('90', 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', '2', 1, '10', '7');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('36', 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', '1', 1, '10', '02');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('39', 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', '1', 1, '10', '03', '03');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('40', 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs', '1', 1, '10', '03', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('99', 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', '2', 1, '10', '9.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('4', 'Pèrdua de curs/os lectiu/s en el supòsit de canvi d''estudis cursats totalment o parcialment amb la condició de becari/ària.', '1', 1, '10', '01', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('66', 'Pèrdua de curs/os lectiu/s en el supòsit de canvi d''estudis cursats totalment o parcialment amb la condició debecari/ària.', '2', 1, '10', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('5', 'Gaudir d''ajuda o beca incompatible', '1', 1, '10', '01', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('67', 'Gaudir d''ajuda o beca incompatible', '2', 1, '10', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('6', 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', '1', 1, '10', '01', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('68', 'No complir els terminis establits per a la presentació de la sol·licitut i/o dels documents', '2', 1, '10', '13');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('42', 'No haver-se matriculat d''un curs complet', '1', 1, '10', '03', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('86', 'Superar els llindars de renda establits per a la concesió de beca', '2', 1, '10', '3.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('27', 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en les bases  de la convocatòria.', '1', 1, '10', '02', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('70', 'El valor cadastral de les finques urbanes (exclosa la vivenda habitual) supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.1');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('28', 'La facturació del negoci/activitat econòmica supera el llindar establit en les bases de la de convocatòria.', '1', 1, '10', '02', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('71', 'La facturació del negoci/activitat econòmica supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('29', 'El valor cadastral de les finques rústiques supera els llindars patrimonials establits en les bases de la convocatòria.', '1', 1, '10', '02', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('72', 'El valor cadastral de les finques rústiques  supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.3');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('30', 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en les bases de la  convocatòria.', '1', 1, '10', '02', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('73', 'La suma dels rendiments nets del capital mobiliari més el saldo net de guanys i pèrdues patrimonials supera els límits establits en l''ordre de convocatòria', '2', 1, '10', '14.4');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('31', 'El conjunt d''elements patrimonials supera el límit establit en les bases de la convocatòria.', '1', 1, '10', '02', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('74', 'La suma d''elements patrimonials supera el límit establit en l''ordre de convocatòria', '2', 1, '10', '14.5');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('75', 'Per deduir-se de les dades que consten en el seu expedient, que el llindar patrimonial de la unitat familiar supera l''establit per a la concessió de la beca.', '2', 1, '10', '15');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('44', 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', '1', 1, '10', '03', '08');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('10', 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', '1', 1, '10', '01', '14');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('77', 'No acreditar suficientment, segons el parer de la comissió competent,  la independència econòmica i/o familiar', '2', 1, '10', '17');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('7', 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''ordre de convocatoria.', '1', 1, '10', '01', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('78', 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet l''orde de convocatòria', '2', 1, '10', '18');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('8', 'No complir els requisits acadèmics exigits en l''ordre de convocatòria', '1', 1, '10', '01', '10');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('81', 'Per obtenir la beca en la convocatòria del Ministeri d''Educació i Ciència.', '2', 1, '10', '27');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('82', 'Per no acreditar la residència en la CCVV amb anterioritat a la data de finalització del tèrmini', '2', 1, '10', '28');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('84', 'Per haver renunciat o haver-li sigut anul·lada la matrícula en el present curs.', '2', 1, '10', '30');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('100', 'No estar matriculat en el nombre mínim exigit de crèdits en aquest curs', '2', 1, '10', '9.2');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('12', 'No reunir els requisits de nacionalitat establits en l''Article 4.1.d del RD 1721/2007, de 21 de desembre, pel qual es regula el règim de les beques i ajudes
a l''estudi personalitzades (BOE de 17 de gener).', '1', 1, '10', '01', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('62', 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('49', 'Els NIF de la unitat familiar no estan identificats per la  AEAT', '1', 1, '10', '04', '06');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('64', 'L''Administració Tributària no posseeix dades econòmiques de la unitat familiar', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('61', 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('32', 'Per tindre l''obligació de presentar declaració per l''Impost Extraordinari sobre el Patrimoni, d''acord amb la normativa reguladora del dit impost.', '1', 1, '10', '02', '09');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('43', 'No estar matriculat en el curs següent segons el pla d''estudis vigent', '1', 1, '10', '03', '07');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('91', 'Superar la seva renda tots els llindars establerts (AEAT)', '2', 1, '10', '81');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('92', 'Algun membre de la unitat familiar està obligat a presentar declaració de patrimoni', '2', 1, '10', '82');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('93', 'Superar els llindars del cabdal mobiliari', '2', 1, '10', '83');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('96', 'L''agència tributària té coneixement que s''ha presentat declaració renda o liquidació 104', '2', 1, '10', '86');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('94', 'Els NIF de la unitat familiar no estan identificats per la  AEAT', '2', 1, '10', '84');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('95', 'Els membres de la unitat familiar no estan identificats econòmicament per la  AEAT', '2', 1, '10', '85');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('97', 'L''agència tributària considera que per les dades que consten en el seu poder deuria haver declaració de renda', '2', 1, '10', '87');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('48', 'No existir independència familiar, en aparèixer inclòs en una altra declaració de la renda.', '1', 1, '10', '04', '05');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('98', 'Alumne independent', '2', 1, '10', '88');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('76', 'Per haver-se comprovat inexactitud en les dades acadèmiques aportades', '2', 1, '10', '16');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('83', 'Es deu presentar certificat de Renda de l''exercici anterior del membre indicat', '2', 1, '10', '3');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('59', 'Ocultació de valors cadastrals.', '1', 1, '10', '04');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('53', 'Situació irregular davant AEAT. Algun membre computable es troba en dues declaracions de la renda.', '1', 1, '10', '04', '11');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('79', 'No complir els requisits acadèmics exigits en l''ordre de convocatòria', '2', 1, '10', '19');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA)
 Values
   ('23', 'Pendiente de definr', '1', 1, '10', '01');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('15', 'No complir el requisit de trobar-se el sol·licitant o els seus sustentadors treballant a Espanya (art. 1.2.d) de l''Ordre EDU 1901/2009, per la qual es convoquen beques de caràcter general i de mobilitat per a estudiants d''ensenyaments universitaris) (BOE del 15 de juliol).', '1', 1, '10', '01', '46');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('35', 'Superar els llindars de renda establits per a la concessió de l''ajuda compensatòria, beca-salari  o mobilitat especial', '1', 1, '10', '02', '12');
Insert into BC2_DENEGACIONES
   (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
 Values
   ('34', 'Falta informació fiscal.', '1', 1, '10', '02', '11');
   
COMMIT;

ALTER TABLE UJI_BECAS.BC2_DENEGACIONES DROP COLUMN CODIGO;

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
  ADD (POSEE_TITULO NUMBER DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_BECAS
  ADD (POSEE_TITULO NUMBER DEFAULT 0);
  
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
  RENAME COLUMN NUMERO_BECAS TO NUMERO_BECAS_MEC;
  
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
  ADD (NUMERO_BECAS_CONS NUMBER);
  
ALTER TABLE UJI_BECAS.BC2_BECAS
  RENAME COLUMN BECAS_CONCEDIDAS_ANT TO BECAS_CONCEDIDAS_MEC;

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (BECAS_CONCEDIDAS_CONS  NUMBER);
 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
MODIFY(FAMILIA_NUMEROSA_ID  NULL);

 