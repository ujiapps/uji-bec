CREATE OR REPLACE TRIGGER UJI_BECAS.NOTIFICA_DENEGACIONES
BEFORE UPDATE
OF FECHA_NOTIFICACION
ON UJI_BECAS.BC2_TANDAS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  v_cabecera_ca  VARCHAR2(4000);
  v_pie_ca       VARCHAR2(4000);
  v_proceso      NUMBER;
  v_estado       NUMBER;
  v_organismo_id NUMBER;
  v_organismo    bc2_organismos.nombre%type;
  v_orden        NUMBER;
  v_asunto       VARCHAR2(200)  := 'Comunicació de denegació de beca ';
  v_nombre       VARCHAR2(1000);
  v_dni          VARCHAR2(100);
  v_intro        VARCHAR2(1000);
  v_firma        VARCHAR2(1000);
  v_detalle      VARCHAR2(4000);
  v_texto        VARCHAR2(4000);
  v_fecha        DATE;
  v_emisor  NUMBER;
  v_error number;
  
  hay_error     EXCEPTION;
  
  CURSOR lista_becas (p_tanda NUMBER) IS
    SELECT b.id, s.persona_id, s.numero_beca_uji
      FROM bc2_becas b, bc2_solicitantes s
      WHERE s.id = b.solicitante_id AND
            b.tanda_id = p_tanda  AND
            fecha_notificacion is null;
      
  CURSOR lista_motivos (p_beca_id NUMBER, p_orden NUMBER) IS
    SELECT b.beca_id, a.causa || a.subcausa codigo, a.nombre denegacion
      FROM bc2_becas_denegaciones b, bc2_denegaciones a
      WHERE a.id = b.denegacion_id AND
            b.beca_id = p_beca_id AND
            b.orden_denegacion = p_orden
      ORDER BY codigo;
 
BEGIN
   
   IF (:old.fecha_notificacion  <> :new.fecha_notificacion ) or (:old.fecha_notificacion is null and :new.fecha_notificacion is not null) THEN
       
       BEGIN
            DELETE bc2_log_notificaciones WHERE tanda_id = :new.id;
       EXCEPTION WHEN OTHERS THEN 
            null;
       END;
       
       BEGIN
         select distinct proceso_id, estado_id
           INTO v_proceso, v_estado
           FROM bc2_becas
           WHERE tanda_id = :new.id;
           
           IF v_proceso = 1 THEN
             v_orden := 1;
           ELSE
             v_orden := 2;
           END IF;
       EXCEPTION
         WHEN TOO_MANY_ROWS THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
            VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'La tanda te beques en diferent procès i/o estat.');
           RAISE hay_error;           
       END;
       
       BEGIN
         SELECT o.id, o.nombre
           INTO v_organismo_id, v_organismo
           FROM bc2_convocatorias c, bc2_organismos o
           WHERE o.id = c.organismo_id AND
                 c.id = :new.convocatoria_id;
                 
         v_asunto := v_asunto || '(' || v_organismo || ')';       
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troba l''organisme.');
           RAISE hay_error;
       END;
     
       BEGIN
         SELECT cabecera_ca, pie_ca
           INTO v_cabecera_ca, v_pie_ca
           FROM bc2_textos
           WHERE proceso_id = v_proceso AND
                 organismo_id = v_organismo_id;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troben textes per a l''organisme ' || v_organismo || ' i el procès ' || v_proceso || '.');
           RAISE hay_error;
       END;   

       v_intro := '- Data ' || TO_CHAR(SYSDATE,'dd/mm/yyyy') || ' <br>- R/n  Servei de Gestió de la Docència i Estudiants <br> - ' || v_asunto || '<br>';
       v_firma := 'La secretària del Jurat de Selecció de Becaris <br>Leticia Falomir Abillar<br>';

       FOR x IN lista_becas (:new.id) LOOP
         SELECT nombre, identificacion
            INTO   v_nombre, v_dni
            FROM   BC2_EXT_PERSONAS
            WHERE  id = x.persona_id;

            v_detalle := NULL;

            FOR y IN lista_motivos (x.id, v_orden) LOOP
               v_detalle := v_detalle || y.codigo || '. ' || y.denegacion || '<br>';
            END LOOP;
            
            v_texto := v_intro || '<br><br>'  || v_nombre || '<br><br>' ||
                             v_cabecera_ca || '<br><br>' ||
                             v_detalle || '<br><br>' ||
                             v_pie_ca ||  '<br><br>' || v_dni || '<br><br>'  ||
                             v_firma;      
            
            BEGIN       
             
              select persona_id into v_emisor
              from uji_apa.apa_personas_cuentas
              where upper(cuenta) = upper(:new.usuario);

              notifica.envia_notificacion(2, x.persona_id, v_asunto, v_texto,v_emisor,'N');
              v_error := 3;
              
              v_fecha := SYSDATE;
              UPDATE bc2_becas
              SET fecha_notificacion = v_fecha
              WHERE id = x.id;
              
              v_error := 4;
              INSERT INTO bc2_becas_historico (ID, BECA_ID, PROCESO_ID, ESTADO_ID, TANDA_ID, FECHA, USUARIO, CONVOCATORIA_ID, FECHA_NOTIFICACION)
              VALUES (hibernate_sequence.nextval, x.id, v_proceso, v_estado, :new.id, SYSDATE, :NEW.USUARIO, :new.convocatoria_id, v_fecha);
              v_error := 5;
              
            EXCEPTION
            WHEN OTHERS THEN
                INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
                VALUES(HIBERNATE_SEQUENCE.Nextval, x.id, :new.id, 'No s''ha pogut notificar la beca. '|| v_error);
                :new.fecha_notificacion := NULL;
            END;
                             
       END LOOP;

   END IF;
EXCEPTION
      WHEN NO_DATA_FOUND THEN
       :new.fecha_notificacion := :old.fecha_notificacion;
     WHEN hay_error then
       :new.fecha_notificacion := NULL;
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR(-20000,'Error others: ' || sqlerrm);
END NOTIFICA_DENEGACIONES;