---- Creación de usuarios 
drop table uji_becas.bc2_ext_paises cascade constraints;

grant select on gri_per.per_paises to uji_becas;

create view uji_becas.bc2_ext_paises as
select id, nombre, nacionalidad_cas nacionalidad, codigo_mec codigo_iso
from   gri_per.per_paises;


drop table uji_becas.bc2_ext_provincias cascade constraints;

grant select on gri_per.per_provincias to uji_becas;

create view uji_becas.bc2_ext_provincias as
select id, nombre, to_number(id) orden, id codigo
from gri_per.per_provincias;


drop table uji_becas.bc2_ext_localidades cascade constraints;

grant select on gri_per.per_pueblos to uji_becas;

create view uji_becas.bc2_ext_localidades as
select id, pro_id provincia_id, nombre, row_number () over (order by pro_id, nombre) orden, ' ' codigo, ayuntamiento codigo_ayuntamiento, codmec codigo_mec
from gri_per.per_pueblos;


drop table uji_becas.bc2_ext_personas cascade constraints;

grant select on gri_per.per_personas to uji_becas;

create view uji_becas.bc2_ext_personas as
select id, nombre||' '||apellido1||' '||apellido2 nombre, identificacion
from gri_per.per_personas;


drop table uji_becas.bc2_ext_estudios cascade constraints;

grant select on gra_pod.pod_titulaciones to uji_becas;
grant select on gra_pod.pod_cursos to uji_becas;
grant select on gra_pop.pop_masters to uji_becas;
grant select on gra_doc.doc_programas to uji_becas;

create view uji_becas.bc2_ext_estudios as
   select id, nombre, tipo_estudio tipo,
          decode (tipo_estudio,
                  'G', 'S',
                  '12C', decode (id, 51001, 'N', 51002, 'N', 51003, 'N', 100, 'N', 101, 'N', 103, 'N', 104, 'N', 'S')
                 ) oficial,
          (select count (*)
           from   gra_pod.pod_cursos
           where  tit_id = t.id
           and    id <> 7) numero_cursos, crd_tr + crd_ob + crd_op + crd_le + nvl (crd_pfc, 0) creditos_totales,
          decode (activa, 'S', 'N', 'S') sin_docencia, decode (activa, 'S', 'N', 'S') extinguida,
          decode (activa, 'S', 'S', 'N') presencial, codmec, codmec_nuevo, codmec_becas, codmec_becas_nuevo,
          cod_conselleria,
		  decode(id,1,0,1) titulacion_tecnica
   from   gra_pod.pod_titulaciones t
   union all
   select id, nombre, 'POP' tipo, oficial, decode (sign (creditos - 60), -1, 1, 0, 1, 2) numero_cursos,
          creditos creditos_totales, decode (activo, 'S', 'S', 'N') sin_docencia,
          decode (activo, 'S', 'N', 'S') extinguida, decode (presencial, 'P', 'S', 'N') presencial, cod_mec,
          null codmec_nuevo, null codmec_becas, null codmec_becas_nuevo, null cod_conselleria,
		  0 titulacion_tecnica
   from   gra_pop.pop_masters
   union all
   select id, nombre, 'DOC' tipo, 'S' oficial, 1 numero_cursos, 0 creditos_totales,
          decode (fin_docencia, null, 'N', 'S') sin_docencia, decode (fin_docencia, null, 'N', 'S') extinguida,
          'S' presencial, codigo_mec codmec, null codmec_nuevo, null codmec_becas, null codmec_becas_nuevo,
          null cod_conselleria, 0 titulacion_tecnica
   from   gra_doc.doc_programas;
		  
		  
		  
		  
drop table uji_becas.bc2_ext_personas_estudios cascade constraints;

grant select on gra_exp.exp_matriculas to uji_becas;
grant select on gra_exp.exp_asi_cursadas to uji_becas;
grant select on gra_exp.exp_sol_convocatorias to uji_becas;
grant select on gra_exp.exp_notas to uji_becas;
grant select on gra_exp.exp_mat_circunstancias to uji_becas;
grant select on gra_pod.pod_asignaturas to uji_becas;
grant select on gra_pod.pod_grupos to uji_becas;
grant select on gra_bec.bec_solicitudes to uji_becas;
grant execute on gra_exp.pack_exp to uji_becas;
grant execute on gra_pod.pack_pod to uji_becas;


create view uji_becas.bc2_ext_personas_estudios as
select curso_aca * 1000000 + exp_per_id id, exp_per_id persona_id, exp_tit_id estudio_id, curso_aca curso_academico_id,
       pack_exp.crd_matriculados (curso_Aca, exp_per_id, exp_tit_id) creditos_matriculados,
       pack_exp.crd_sup_ca (exp_per_id, exp_tit_id, curso_aca) creditos_superados,
       pack_exp.crd_faltantes_ca (exp_per_id, exp_tit_id, curso_aca) creditos_faltantes,
       (select nvl (sum (a.creditos), 0)
        from   gra_exp.exp_asi_cursadas ac,
               gra_pod.pod_asignaturas a
        where  estado in ('A', 'C', 'R')
        and    cco_id = 0
        and    mat_exp_per_id = m.exp_per_id
        and    mat_exp_tit_id = m.exp_tit_id
        and    mat_curso_aca = m.curso_aca
        and    asi_id = a.id) creditos_pendientes_conva,
       decode (gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id),
               0, 0,
               pack_exp.imp_creditos_no_beca (exp_tit_id, exp_per_id, curso_aca)
               / gra_pod.pack_pod.precio_crd (m.curso_aca, m.exp_tit_id)
              ) creditos_fuera_beca,
       (select sum (a.creditos)
        from   (select distinct asc_mat_exp_per_id, asc_mat_exp_tit_id, asc_mat_curso_aca, asc_asi_id
                from            gra_exp.exp_notas) n,
               gra_pod.pod_asignaturas a
        where  asc_mat_exp_per_id = m.exp_per_id
        and    asc_mat_exp_tit_id = m.exp_tit_id
        and    asc_mat_curso_aca = m.curso_aca
        and    asc_asi_id = a.id) creditos_presentados,
       'N' limitar_creditos, 'N' limitar_creditos_fin_estudios,
       (select decode (count (*), 0, 'N', 'S')
        from   exp_mat_circunstancias
        where  mat_exp_per_id = m.exp_per_id
        and    mat_exp_tit_id = m.exp_tit_id
        and    mat_curso_aca = m.curso_aca
        and    cir_id = 3) matricula_parcial,
       (select count (*)
        from   gra_bec.bec_solicitudes
        where  curso_aca < m.curso_Aca
        and    per_id = m.exp_per_id
        and    tit_id = m.exp_tit_id) numero_becas,
       (select count (distinct semestre)
        from   gra_exp.exp_asi_cursadas ac,
               gra_pod.pod_grupos p
        where  mat_exp_per_id = m.exp_per_id
        and    mat_exp_tit_id = m.exp_tit_id
        and    mat_curso_aca = m.curso_aca
        and    ac.asi_id = p.asi_id
        and    mat_curso_aca = p.curso_aca) numero_semestres,
       (select decode (count (*), 0, 'N', 'S')
        from   exp_asi_cursadas
        where  mat_exp_per_id = exp_per_id
        and    mat_exp_tit_id = exp_tit_id
        and    mat_curso_aca = curso_aca
        and    grp_id = 'Z') sin_docencia,
       (select decode (count (*), 0, 'N', 'S')
        from   gra_exp.exp_sol_convocatorias
        where  tipo = 'E'
        and    asc_mat_exp_per_id = exp_per_id
        and    asc_mat_exp_tit_id = exp_tit_id
        and    asc_mat_curso_aca = curso_aca) programa_intercambio
from   gra_exp.exp_matriculas m;

 