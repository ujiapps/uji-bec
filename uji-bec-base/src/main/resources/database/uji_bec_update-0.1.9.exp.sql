ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (nota_media_ultimo_curso  NUMBER);

 
 CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_PERSONAS (ID, NOMBRE, IDENTIFICACION, PERSONA_NOMBRE, PERSONA_APELLIDO1, PERSONA_APELLIDO2) AS
   SELECT id, rtrim (apellido1 || ' ' || apellido2) || ', ' || nombre nombre, identificacion, nombre persona_nombre, apellido1 persona_apellido1, apellido2 persona_apellido2
   FROM   gri_per.per_personas;

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (nota_media_ultimo_curso  NUMBER);

 
/* Formatted on 24/06/2013 15:36 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS_TMP (ID,
                                                                      PERSONA_ID,
                                                                      ESTUDIO_ID,
                                                                      CURSO_ACADEMICO_ID,
                                                                      CREDITOS_MATRICULADOS,
                                                                      CREDITOS_SUPERADOS,
                                                                      CREDITOS_FALTANTES,
                                                                      CREDITOS_PENDIENTES_CONVA,
                                                                      CREDITOS_FUERA_BECA,
                                                                      CREDITOS_PRESENTADOS,
                                                                      LIMITAR_CREDITOS,
                                                                      LIMITAR_CREDITOS_FIN_ESTUDIOS,
                                                                      MATRICULA_PARCIAL,
                                                                      NUMERO_BECAS_MEC,
                                                                      NUMERO_BECAS_CONS,
                                                                      MINUSVALIA,
                                                                      NUMERO_SEMESTRES,
                                                                      PROGRAMA_INTERCAMBIO,
                                                                      CREDITOS_CONVALIDADOS,
                                                                      CREDITOS_PARA_BECA,
                                                                      CURSO,
                                                                      NOTA_ACCESO,
                                                                      NOTA_MEDIA,
                                                                      FAMILIA_NUMEROSA_ID,
                                                                      BECARIO,
                                                                      TIPO_MATRICULA,
                                                                      ESTUDIO_ACCESO_ID,
                                                                      SIN_DOCENCIA,
                                                                      POSEE_TITULO,
                                                                      IMPORTE_BECAS,
                                                                      IMPORTE_BECAS_MINISTERIO
                                                                     ) AS
   select rownum id, exp_per_id persona_id, exp_tit_id estudio_id, m.curso_aca curso_academico_id,
          pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id) creditos_matriculados_exp,              -- todos ,
          --   pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) creditos_matriculados_bec,
          -- menos adapataso comvalidados, reconocidos y (los que superen la titulacion_no se estan calculando),
          pack_bec.crd_sup (e.per_id, e.tit_id, m.curso_aca) creditos_superados,
          -- menos adapatados comvalidados, reconocidos y (los que superen la titulacion no se estan calculando),
          pack_exp.crd_faltantes_ca (e.per_id, e.tit_id, m.curso_aca) creditos_faltantes,
          gra_exp.crd_pte_convalidacion_exp (e.per_id, e.tit_id, m.curso_aca) creditos_pendientes_conva,
          (pack_exp.crd_matriculados (m.curso_aca, e.per_id, e.tit_id)
           - pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca)
          ) creditos_fuera_beca,
          pack_exp.crd_presentados (m.curso_aca, e.per_id, e.tit_id) creditos_presentados,
          gra_exp.limitar_creditos (e.per_id, e.tit_id, m.curso_aca) limitar_creditos,
          '0' limitar_creditos_fin_estudios, '0' matricual_parcial,    -- nos deberia dar igual pues es la de matricula,
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'MIN', e.tit_id) numero_becas_min,
          gra_bec.pack_bec_2011.numbecas (e.per_id, m.curso_aca, 'CON', e.tit_id) numero_becas_cons,
          decode (tm.id, 41, 1, 59, 1, 60, 1, 0) minusvalia,
          decode (gra_bec.pack_bec_2011.crd_mismo_semestre (e.per_id, e.tit_id, m.curso_aca, 'G'),
                  'N', 2,
                  1
                 ) numero_semestres,
          decode (gra_bec.pack_bec_2011.erasmus (e.per_id, m.curso_aca), 'S', 1, 0) programa_intercambio,
          gra_exp.crd_convalidados_exp (e.per_id, e.tit_id, m.curso_aca) creditos_convalidados,
          pack_bec.crd_mat (e.per_id, e.tit_id, m.curso_aca) CREDITOS_PARA_BECA,
          gra_bec.pack_bec.curso_mat (e.per_id, e.tit_id, m.curso_aca) curso,
          to_number (replace (e.nau, '.', ',')) nota_acceso,
          gra_exp.media_exp_beca (m.exp_per_id, m.exp_tit_id, m.curso_aca) nota_media,
          decode (fam_num_categoria, null, null, 'G', 1, 'E', 2, null) familia_numerosa_id,
          --decode(gra_bec.pack_bec.media_beca (e.per_id, e.tit_id, m.curso_aca),'S',1,0) media_beca, -- se podria calcular en la logica de negocio????
          decode (gra_exp.es_becario (m.exp_per_id, m.exp_tit_id, m.curso_aca), 'S', 1, 0) becario, tm.nombre tipo_matricula,
          '' estudio_acceso_id, 0 sin_docencia, 0 posee_titulo, pack_exp.imp_creditos_beca (m.exp_tit_id, m.exp_per_id, m.curso_aca) 
          importe_becas, pack_exp.imp_creditos_beca (m.exp_tit_id, m.exp_per_id, m.curso_aca, 'S') 
          importe_becas_ministerio
   from   exp_expedientes e,
          exp_matriculas m,
          exp_tipos_matricula tm,
          exp_est_matriculas em
   where  e.per_id = m.exp_per_id
   and    e.tit_id = m.exp_tit_id
   and    m.tma_id = tm.id
   --and    m.curso_aca > 2011
   --and    exp_per_id in (480774, 320437,212842, 176559,137154)
   and    m.curso_aca = tm.curso_aca
   and    m.exp_per_id = em.mat_exp_per_id(+)
   and    m.exp_tit_id = em.mat_exp_tit_id(+)
   and    m.curso_aca = em.mat_curso_aca(+);


 

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_total  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_ministerio  NUMBER);
 
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (tasas_conselleria  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (creditos_cubiertos  NUMBER);




CREATE TABLE uji_becas.bc2_listados 
    ( 
     id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     url VARCHAR2 (4000) , 
     activo NUMBER DEFAULT 1  NOT NULL 
    ) 
;


CREATE INDEX uji_becas.bc2_listados__IDX ON uji_becas.bc2_listados 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_listados 
    ADD CONSTRAINT bc2_listados_org_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;

CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_g (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_titus is
         select e.*
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;
   begin
      for x in c_titus loop
         if p_parte >= 1 then
            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id),
                creditos_superados = pack_bec.crd_sup (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes =
                   pack_exp.crd_faltantes_ca (x.persona_id, x.estudio_id, x.curso_academico_id - 1)
                   + pack_exp.crd_faltantes_cf_ca (x.curso_academico_id - 1, x.persona_id, x.estudio_id),
                creditos_pendientes_conva =
                                    gra_exp.crd_pte_convalidacion_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_exp.crd_matriculados (x.curso_academico_id, x.persona_id, x.estudio_id)
                    - pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = pack_exp.crd_presentados (x.curso_academico_id, x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_exp.creditos_cf (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = gra_exp.limitar_creditos (x.persona_id, x.estudio_id, x.curso_academico_id),
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_exp.minusvalia_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'G'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_exp.crd_convalidados_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = pack_bec.crd_mat (x.persona_id, x.estudio_id, x.curso_academico_id),
                curso = gra_bec.pack_bec.curso_mat (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_exp.nota_acceso_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = gra_exp.media_curso_beca (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_exp.familia_numerosa_exp (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_exp.es_becario (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_EXP.TIPO_MATRICULA_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_EXP.ESTUDIO_ACCESO_EXP (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (gra_bec.alguna_vez_media_beca (x.persona_id, x.estudio_id), 'S', 1, 0),
                tiene_beca_mec =
                   decode (gra_bec.ES_BECARIO_CURSO (x.persona_id, x.estudio_id, x.curso_academico_id, 'MIN'),
                           'S', 1,
                           0
                          )
            where  x.id = e.id;

            commit;

         end if;
      end loop;
   end;
END sincroniza_datos_g;


CREATE OR REPLACE PROCEDURE UJI_BECAS.sincroniza_datos_pop (
   p_persona   in   number,
   p_estudio   in   number,
   p_curso     in   number,
   p_parte     in   number default 999
) IS
BEGIN
   declare
      cursor c_masters is
         select *
         from   bc2_ext_personas_estudios e
         where  persona_id = p_persona
         and    estudio_id = p_estudio
         and    curso_academico_id = p_curso;

      v_crd_beca        number;
      v_crd_faltan      number;
      v_curso           number;
      v_curso_estudio   number;
   begin
      for x in c_masters loop
         if p_parte >= 1 then
            v_crd_faltan := gra_pop.crd_faltan_becas_ca (x.persona_id, x.estudio_id, x.curso_academico_id);

            update bc2_ext_personas_estudios e
            set creditos_matriculados = pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_superados = gra_pop.crd_sup_becas (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_faltantes = v_crd_faltan,
                creditos_pendientes_conva =
                                        gra_pop.crd_pte_convalidacion (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_fuera_beca =
                   (pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id)
                    - gra_pop.crd_mat_becas (x.persona_id, x.estudio_id, x.curso_academico_id)
                   ),
                creditos_presentados = gra_pop.CRD_PRESENTADOS (x.persona_id, x.estudio_id, x.curso_academico_id)
            where  x.id = e.id;

            commit;
         end if;

         if p_parte >= 2 then
            update bc2_ext_personas_estudios e
            set limitar_creditos = 0,
                limitar_creditos_fin_estudios = '0',
                matricula_parcial = '0'
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set creditos_complementos = gra_pop.crd_complementos_pop (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set numero_becas_mec =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'MIN', x.estudio_id),
                numero_becas_cons =
                                gra_bec.pack_bec_2011.numbecas (x.persona_id, x.curso_academico_id, 'CON', x.estudio_id)
            where  x.id = e.id;

            commit;
            --FALTA CALCULAR MINUSVALIA
            v_crd_beca := GRA_POP.CRD_MAT_BECAS (x.persona_id, x.estudio_id, x.curso_academico_id);

            if v_crd_faltan < v_crd_beca then
               v_crd_beca := v_crd_faltan;

               update bc2_ext_personas_estudios e
               set creditos_fuera_beca =
                             (pack_pop.crd_matriculados (x.persona_id, x.estudio_id, x.curso_academico_id) - v_crd_beca
                             )
               where  x.id = e.id;
            end if;

            v_curso := gra_pop.curso_mat_master (x.persona_id, x.estudio_id, x.curso_academico_id);

            begin
               select numero_cursos
               into   v_curso_estudio
               from   bc2_ext_estudios
               where  id = x.estudio_id;

               if v_curso_estudio < v_curso then
                  v_curso := v_curso_estudio;
               end if;
            exception
               when no_data_found then
                  null;
            end;

            update bc2_ext_personas_estudios e
            set minusvalia =
                           decode (gra_pop.minusvalia_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                                   'S', 1,
                                   0
                                  ),
                numero_semestres =
                   decode (gra_bec.pack_bec_2011.crd_mismo_semestre (x.persona_id, x.estudio_id, x.curso_academico_id,
                                                                     'M'),
                           'N', 2,
                           1
                          ),
                programa_intercambio =
                                  decode (gra_bec.pack_bec_2011.erasmus (x.persona_id, x.curso_academico_id),
                                          'S', 1,
                                          0
                                         ),
                creditos_convalidados = gra_pop.crd_convalidados (x.persona_id, x.estudio_id, x.curso_academico_id),
                creditos_para_beca = v_crd_beca,
                curso = v_curso
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set nota_acceso = gra_pop.nota_acceso (x.persona_id, x.estudio_id, x.curso_academico_id),
                nota_media = pack_pop.media_exp_beca (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            commit;

            update bc2_ext_personas_estudios e
            set familia_numerosa_id =
                   decode (gra_pop.familia_numerosa_pop (x.persona_id, x.estudio_id, x.curso_academico_id),
                           'G', 1,
                           'E', 2,
                           NULL
                          ),
                becario = decode (gra_pop.es_becario_pop (x.persona_id, x.estudio_id, x.curso_academico_id), 'S', 1, 0),
                tipo_matricula = GRA_POP.TIPO_MATRICULA_POP (x.persona_id, x.estudio_id, x.curso_academico_id),
                estudio_acceso_id = GRA_POP.ESTUDIO_ACCESO (x.persona_id, x.estudio_id, x.curso_academico_id),
                posee_titulo = GRA_TIT.POSEE_TITULO (x.persona_id, x.estudio_id)
            where  x.id = e.id;

            update bc2_ext_personas_estudios e
            set alguna_beca_parcial = decode (GRA_BEC.ALGUNA_VEZ_MEDIA_BECA (x.persona_id, x.estudio_id), 'S', 1, 0),
                tiene_beca_mec =
                   decode (gra_bec.ES_BECARIO_CURSO (x.persona_id, x.estudio_id, x.curso_academico_id, 'MIN'),
                           'S', 1,
                           0
                          )
            where  x.id = e.id;

            commit;

         end if;
      end loop;
   end;
END sincroniza_datos_pop;


CREATE OR REPLACE PROCEDURE UJI_BECAS.sincronizar_importes (p_curso_aca in number) IS
   cursor lista (p_curso_aca in number) is
      select persona_id, estudio_id, curso_academico_id
      from   bc2_ext_personas_estudios
      where  curso_academico_id = p_curso_aca;

   PROCEDURE sincroniza_importes_g (
      p_persona   in   number,
      p_estudio   in   number,
      p_curso     in   number,
      p_parte     in   number default 999
   ) IS
   BEGIN
      declare
         cursor c_titus is
            select e.*
            from   bc2_ext_personas_estudios e
            where  persona_id = p_persona
            and    estudio_id = p_estudio
            and    curso_academico_id = p_curso;
      begin
         for x in c_titus loop
            update bc2_ext_personas_estudios e
            set tasas_total = pack_Exp.imp_creditos_beca (x.estudio_id, x.persona_id, x.curso_academico_id),
                tasas_ministerio =
                                  pack_Exp.imp_creditos_beca (x.estudio_id, x.persona_id, x.curso_academico_id - 1, 'S')
            where  x.id = e.id;

            update bc2_ext_personas_estudios e
            set tasas_conselleria = tasas_total - tasas_ministerio
            where  x.id = e.id;

            commit;

            if x.creditos_matriculados > x.creditos_faltantes then
               update bc2_ext_personas_estudios e
               set creditos_cubiertos = x.creditos_faltantes
               where  x.id = e.id;
            else
               update bc2_ext_personas_estudios e
               set creditos_cubiertos = x.creditos_matriculados
               where  x.id = e.id;
            end if;

            commit;
         end loop;
      end;
   END;

   PROCEDURE sincroniza_importes_pop (
      p_persona   in   number,
      p_estudio   in   number,
      p_curso     in   number,
      p_parte     in   number default 999
   ) IS
   BEGIN
      declare
         cursor c_titus is
            select e.*
            from   bc2_ext_personas_estudios e
            where  persona_id = p_persona
            and    estudio_id = p_estudio
            and    curso_academico_id = p_curso;
      begin
         for x in c_titus loop
            update bc2_ext_personas_estudios e
            set tasas_total = pack_pop.imp_creditos_beca (x.persona_id, x.estudio_id, x.curso_academico_id),
                tasas_ministerio =
                            pack_pop.imp_creditos_beca (x.persona_id, x.estudio_id, x.curso_academico_id - 1, null, 'S')
            where  x.id = e.id;

            update bc2_ext_personas_estudios e
            set tasas_conselleria = tasas_total - tasas_ministerio
            where  x.id = e.id;

            commit;

            if x.creditos_matriculados > x.creditos_faltantes then
               update bc2_ext_personas_estudios e
               set creditos_cubiertos = x.creditos_faltantes
               where  x.id = e.id;
            else
               update bc2_ext_personas_estudios e
               set creditos_cubiertos = x.creditos_matriculados
               where  x.id = e.id;
            end if;

            commit;
         end loop;
      end;
   END;
begin
   for x in lista (p_curso_aca) loop
      if x.estudio_id between 42000 and 49999 then
         sincroniza_importes_pop (x.persona_id, x.estudio_id, x.curso_academico_id);
      else
         sincroniza_importes_g (x.persona_id, x.estudio_id, x.curso_academico_id);
      end if;
   end loop;
END sincronizar_importes;




 
DROP TABLE BC2_EXT_IMPORTES_MEC CASCADE CONSTRAINTS;

CREATE TABLE BC2_EXT_IMPORTES_MEC
(
  PERSONA_ID          NUMBER,
  SOLICITANTE_ID      NUMBER,
  BECA_ID             NUMBER,
  CURSO_ACADEMICO_ID  NUMBER,
  CONVOCATORIA_ID     NUMBER,
  PROCESO_ID          NUMBER,
  ESTADO_ID           NUMBER,
  UT                  VARCHAR2(2 BYTE),
  CONV                VARCHAR2(100 BYTE),
  NIF                 VARCHAR2(100 BYTE),
  APELLIDO1           VARCHAR2(100 BYTE),
  APELLIDO2           VARCHAR2(100 BYTE),
  NOMBRE              VARCHAR2(100 BYTE),
  TIPO_FAMILIA_ID     NUMBER,
  TASAS_TOTAL         NUMBER,
  TASAS_MINISTERIO    NUMBER,
  TASAS_CONSELLERIA   NUMBER,
  CREDITOS_CUBIERTOS  NUMBER,
  CURSO_ACA           VARCHAR2(100 BYTE),
  ESTUDIO_ID          NUMBER
)
TABLESPACE DATOS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
