------------------------------
-- UJI_BECAS_WS_SOLICITUDES --
------------------------------
CREATE OR REPLACE FORCE VIEW UJI_BECAS_WS_SOLICITUDES.DECLARANTE
(
   CODIGO_ARCHIVO_TEMPORAL,
   IDENTIFICACION,
   HJID,
   DECLCODDOCUIDENT, 
   DECLCODPARENTESCO, 
   DECLDECLARANTE, 
   DECLEMPLEADOR, 
   DECLFECHADECLITEM, 
   DECLINDCURANT, 
   DECLINDCURPEN, 
   DECLINDCURULT, 
   DECLINDINDEP, 
   DECLINDINEM, 
   DECLINDRENTASEXTRANJ, 
   DECLINGRESOSANUALES, 
   DECLLOCDECL, 
   DECLNIF, 
   DECLRENTASEXTRANJ
)
AS
     SELECT   s.solisqciudadano codigo_archivo_temporal,
              p.dapenif identificacion,
              d.hjid,
              d.DECLCODDOCUIDENT, 
              d.DECLCODPARENTESCO, 
              d.DECLDECLARANTE, 
              d.DECLEMPLEADOR, 
              d.DECLFECHADECLITEM, 
              d.DECLINDCURANT, 
              d.DECLINDCURPEN, 
              d.DECLINDCURULT, 
              d.DECLINDINDEP, 
              d.DECLINDINEM, 
              d.DECLINDRENTASEXTRANJ, 
              d.DECLINGRESOSANUALES, 
              d.DECLLOCDECL, 
              d.DECLNIF, 
              d.DECLRENTASEXTRANJ
       FROM   uji_becas_ws_solicitudes.solicitudType s,
              uji_becas_ws_solicitudes.datospersonalesType p,
              uji_becas_ws_solicitudes.declaranteType d
      WHERE   s.datospersonales_solicitudtyp_0 = p.hjid
          AND s.declarante_solicitudtype_hjid = d.hjid
   ORDER BY   1, 2;
   
GRANT SELECT ON UJI_BECAS_WS_SOLICITUDES.DECLARANTE TO UJI_BECAS;

---------------
-- UJI_BECAS --
---------------

CREATE TABLE uji_becas.bc2_declarante 
    ( 
     id NUMBER  NOT NULL , 
     solicitante_id NUMBER  NOT NULL , 
     declarante VARCHAR2 (100) , 
     indicador_rentas_extranjero NUMBER , 
     rentas_extranjero NUMBER , 
     localidad_declarante VARCHAR2 (100) , 
     fecha_declarante DATE , 
     indicador_ult_curso NUMBER , 
     indicador_curso_pendiente NUMBER , 
     indicador_curso_anterior NUMBER , 
     indicador_independiente VARCHAR2 (1000) , 
     indicador_inem VARCHAR2 (1000) , 
     empleador VARCHAR2 (100) , 
     tipo_identificacion NUMBER , 
     identificacion VARCHAR2 (100) , 
     ingresos_anuales NUMBER , 
     codigo_parentesco VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_becas.bc2_declarante__IDX ON uji_becas.bc2_declarante 
    ( 
     solicitante_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante__UN UNIQUE ( solicitante_id ) ;




ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_sol_FK FOREIGN KEY 
    ( 
     solicitante_id
    ) 
    REFERENCES uji_becas.bc2_solicitantes 
    ( 
     id
    );

ALTER TABLE UJI_BECAS.BC2_DECLARANTE
MODIFY(INDICADOR_INDEPENDIENTE NUMBER);

ALTER TABLE UJI_BECAS.BC2_DECLARANTE
MODIFY(INDICADOR_INEM NUMBER);

INSERT INTO bc2_declarante(id,
    solicitante_id, declarante,
    indicador_rentas_extranjero, rentas_extranjero,
    localidad_declarante, fecha_declarante, 
    indicador_ult_curso, indicador_curso_pendiente,
    indicador_curso_anterior, indicador_independiente, 
    indicador_inem, empleador, 
    tipo_identificacion, identificacion, 
    ingresos_anuales, codigo_parentesco)
  SELECT  hibernate_sequence.nextval, s.* FROM
  (SELECT DISTINCT s.id solicitante_id, d.decldeclarante, 
         DECODE(d.declindrentasextranj, null, null, 'S',1,0), d.declrentasextranj,
         d.decllocdecl, d.declfechadeclitem, 
         DECODE(d.declindcurult, null, null,'S',1,0), DECODE(d.declindcurpen, null, null,'S',1,0),
         DECODE(d.declindcurant, null, null,'S',1,0), DECODE(d.declindindep, null, null,'S',1,0), 
         DECODE(d.declindinem, null, null,'S',1,0), d.declempleador, 
         d.declcoddocuident, d.declnif, 
         d.declingresosanuales, d.declcodparentesco
  FROM UJI_BECAS_WS_SOLICITUDES.DECLARANTE d,
       bc2_solicitantes s,
       bc2_becas b
  WHERE b.solicitante_id = s.id AND
        UPPER(b.identificacion) = UPPER(d.identificacion) AND
        B.CONVOCATORIA_ID in (1, 2) AND
        TO_CHAR(B.CODIGO_ARCHIVO_TEMPORAL) = D.CODIGO_ARCHIVO_TEMPORAL) s;


-- Actualización de los datos que originalmente estaban en bc2_solicitantes, y ahora están en bc2_declarante
--    s.unidad_familiar_independiente  ---> d.indicador_independiente
--    s.ingresos_extranjero            ---> d.rentas_extranjero
--    s.ingresos_netos                 ---> d.ingresos_anuales
begin
   declare
      vDeclarante bc2_declarante%rowtype;
      cursor cSolicitantes is
         select *
           from bc2_solicitantes
      ;
   begin
      for solicitante in cSolicitantes loop
         begin
            select *
              into vDeclarante
              from bc2_declarante d
             where d.solicitante_id = solicitante.id
            ;
            if (solicitante.unidad_familiar_independiente != vDeclarante.indicador_independiente) then
               update bc2_declarante d
                  set d.indicador_independiente = solicitante.unidad_familiar_independiente
                where d.solicitante_id = solicitante.id
               ;
               dbms_output.put_line('Solicitante '||solicitante.id||': actualizando indicador independiente');
            end if;
            if (trunc(vDeclarante.rentas_extranjero) != trunc(solicitante.ingresos_extranjero)) then
               update bc2_declarante d
                  set d.rentas_extranjero = solicitante.ingresos_extranjero
                where d.solicitante_id = solicitante.id
               ;
               dbms_output.put_line('Solicitante '||solicitante.id||': actualizando rentas_extranjero');
            end if;
            if (trunc(vDeclarante.ingresos_anuales) != trunc(solicitante.ingresos_netos)) then
               update bc2_declarante d
                  set d.ingresos_anuales = solicitante.ingresos_netos
                where d.solicitante_id = solicitante.id
               ;
               dbms_output.put_line('Solicitante '||solicitante.id||': actualizando ingresos_anuales');
            end if;
         exception
            when no_data_found then
              insert into bc2_declarante (id, solicitante_id, rentas_extranjero, indicador_independiente, ingresos_anuales)
                   values (hibernate_sequence.nextval, solicitante.id, solicitante.ingresos_extranjero, solicitante.unidad_familiar_independiente, solicitante.ingresos_netos);
              dbms_output.put_line('Solicitante '||solicitante.id||': insertado declarante ('||solicitante.unidad_familiar_independiente||'-'||solicitante.ingresos_extranjero||'-'||solicitante.ingresos_netos||')');
         end;
      end loop;
   end;
end;


ALTER TABLE UJI_BECAS.BC2_DECLARANTE RENAME COLUMN TIPO_IDENTIFICACION TO TIPO_IDENTIFICACION_id;
		
		
CREATE INDEX uji_becas.bc2_declarante_tip_id_IDX ON uji_becas.bc2_declarante 
    ( 
     tipo_identificacion_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_declarante 
    ADD CONSTRAINT bc2_declarante_tip_ident_FK FOREIGN KEY 
    ( 
     tipo_identificacion_id
    ) 
    REFERENCES uji_becas.bc2_tipos_identificacion 
    ( 
     id
    ) 
;
		
ALTER TABLE UJI_BECAS.BC2_MIEMBROS
ADD (FIRMA_SOLICITUD NUMBER DEFAULT 0 NOT NULL);

UPDATE bc2_miembros m set
  m.firma_solicitud = (
SELECT distinct decode(f.famiindvalidciud,'S',1,0)
from uji_becas_ws_solicitudes.familiares f, bc2_becas b, bc2_solicitantes s
where upper(b.identificacion) = upper(f.identificacion) and
      b.codigo_archivo_temporal = to_char(f.codigo_archivo_temporal) and
      s.id = b.solicitante_id and
      s.id =  m.solicitante_id and
      upper(f.faminiffam) = upper(m.identificacion))
where exists (select decode(f.famiindvalidciud,'S',1,0)
from uji_becas_ws_solicitudes.familiares f, bc2_becas b, bc2_solicitantes s
where upper(b.identificacion) = upper(f.identificacion) and
      b.codigo_archivo_temporal = to_char(f.codigo_archivo_temporal) and
      s.id = b.solicitante_id and
      s.id =  m.solicitante_id and
      upper(f.faminiffam) = upper(m.identificacion));