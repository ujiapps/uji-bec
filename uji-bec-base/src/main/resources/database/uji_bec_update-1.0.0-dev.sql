ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (IBANPAIS  VARCHAR2(10)                        DEFAULT 'ES');

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (IBANDC  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (NOTA_PRUEBA_ESPECIFICA  NUMBER);

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (PROCEDENCIA_NOTA  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (NOTA_MEDIA_SIN_SUSPENSO  NUMBER);


ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (PORC_ACTIV_ECONOMICAS  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (IMPORT_ACTIV_ECONOMICAS  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (IMPORT_RENTAS_EXTRAN  NUMBER);

ALTER TABLE UJI_BECAS.BC2_MIEMBROS
 ADD (TIPO_MONEDA_ID  NUMBER);


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (NOTA_ESPECIFICA  NUMBER);

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (PROCEDENCIA_NOTA_ID  VARCHAR2(10));

ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (MEDIA_SIN_SUSPENSOS  NUMBER);

 
 CREATE TABLE uji_becas.bc2_tipos_moneda 
    ( 
     id NUMBER  NOT NULL , 
     codigo NUMBER , 
     nombre VARCHAR2 (1000) 
    ) 
;

ALTER TABLE UJI_BECAS.BC2_EXT_ESTUDIOS
 ADD (doble_titulacion  NUMBER                      DEFAULT 0                     NOT NULL);


ALTER TABLE uji_becas.bc2_tipos_moneda 
    ADD CONSTRAINT bc2_tipos_moneda_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_becas.bc2_actividades_economicas 
    ( 
     id NUMBER  NOT NULL , 
     miembro_id NUMBER  NOT NULL , 
     num_sociedad NUMBER , 
     cif_sociedad VARCHAR2(10) , 
     porc_participacion NUMBER , 
     imp_participacion NUMBER 
    ) 
;


CREATE INDEX uji_becas.bc2_actividades_eco_IDX ON uji_becas.bc2_actividades_economicas 
    ( 
     miembro_id ASC 
    ) 
;

ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_actividades_economicas_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_becas.bc2_actividades_economicas 
    ADD CONSTRAINT bc2_aeco_miembros_FK FOREIGN KEY 
    ( 
     miembro_id
    ) 
    REFERENCES uji_becas.bc2_miembros 
    ( 
     id
    ) 
;




CREATE INDEX uji_becas.bc2_miembros_mon_IDX ON uji_becas.bc2_miembros 
    ( 
     tipo_moneda_id ASC 
    ) 
;
ALTER TABLE uji_becas.bc2_miembros 
    ADD CONSTRAINT bc2_miembros_tipos_mon_FK FOREIGN KEY 
    ( 
     tipo_moneda_id
    ) 
    REFERENCES uji_becas.bc2_tipos_moneda 
    ( 
     id
    ) 
;


ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
MODIFY(NOTA_ESPECIFICA  DEFAULT 0);

