ALTER TABLE UJI_BECAS.BC2_TANDAS
 ADD (USUARIO  VARCHAR2(100));

ALTER TABLE UJI_BECAS.BC2_LOG_NOTIFICACIONES
MODIFY(BECA_ID  NULL);

ALTER TABLE UJI_BECAS.BC2_LOG_NOTIFICACIONES
MODIFY(TANDA_ID  NULL);


ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (plan_anterior_num_cursos  NUMBER);

CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_BECAS_ESTUDIOS
(
   ID,
   ESTUDIO_ID,
   NUMERO_CURSOS,
   CREDITOS_TOTALES,
   PRESENCIAL,
   SIN_DOCENCIA,
   CREDITOS_BECA_COMPLETA,
   CREDITOS_BECA_PARCIAL,
   TIPO_MATRICULA_ID,
   BECA_ID,
   CURSO_ACADEMICO_ID,
   CURSO_ACADEMICO_ACTIVO,
   TITULACION_TECNICA
)
AS
SELECT solicitud.id * 100000 + becas.id AS id,
            estudio.id AS estudio_Id,
            estudio.NUMERO_CURSOS AS numero_Cursos,
            estudio.CREDITOS_TOTALES AS creditos_Totales,
            estudio.presencial AS presencial,
            estudio.SIN_DOCENCIA AS sin_Docencia,
            (select creditos_beca_completa 
             from bc2_minimos minimos 
             where minimos.estudio_id = estudio.id 
               and minimos.curso_academico_id= curso_aca.id) creditos_beca_completa,
            (select CREDITOS_BECA_PARCIAL 
             from bc2_minimos minimos 
             where minimos.estudio_id = estudio.id 
               and minimos.curso_academico_id= curso_aca.id) creditos_beca_parcial,
            becas.TIPO_MATRICULA_ID AS tipo_Matricula_Id,
            becas.id AS beca_Id,
            CURSO_ACA.id AS curso_Academico_Id,
            curso_aca.activo AS curso_Academico_Activo,
            estudio.titulacion_tecnica AS titulacion_tecnica
     FROM   BC2_EXT_ESTUDIOS estudio,
            BC2_BECAS becas,
            BC2_SOLICITANTES solicitud,
            BC2_CURSOS_ACADEMICOS curso_aca
    WHERE       estudio.id = becas.estudio_id
            AND solicitud.id = becas.solicitante_id
            AND solicitud.curso_academico_id = curso_aca.id
            
			
CREATE TABLE uji_becas.bc2_lotes_ministerio 
    ( 
     id NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     lote_id NUMBER  NOT NULL , 
     convocatoria_id NUMBER , 
     fecha DATE  NOT NULL,
     multienvio_id NUMBER  NOT NULL,
     estado VARCHAR2(20)   NOT NULL
    );

ALTER TABLE uji_becas.bc2_lotes_ministerio 
    ADD CONSTRAINT bc2_lotes_ministerio_PK PRIMARY KEY ( id ) ;
    
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_RESOL_CAB_MULTIENVIO
(
   MULTIENVIO_ID,
   ENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TIPO_ENVIO,
   TOTAL_SOLI
)
AS
   SELECT   DISTINCT M.HJID MULTIENVIO_ID,
                     EN.HJID ENVIO_ID,
                     C.CAENLOTE LOTE,
                     C.CAENCODCONV CONVOCATORIA,
                     C.CAENIDCURSO CURSO_ACADEMICO,
                     C.CAENTIPOENVIO TIPO_ENVIO,
                     C.CAENTOTSOLI TOTAL_SOLI
     FROM   UJI_BECAS_WS_MULTIENVIO.ENVIOTYPEORDENPAGOANDERROROR_0 E,
            UJI_BECAS_WS_MULTIENVIO.MULTIENVIOTYPE M,
            UJI_BECAS_WS_MULTIENVIO.ENVIOTYPE EN,
            UJI_BECAS_WS_MULTIENVIO.CABECERAENVIOTYPE C
    WHERE       EN.ENVIO_MULTIENVIOTYPE_HJID = M.HJID
            AND EN.CABECERAENVIO_ENVIOTYPE_HJID = C.HJID
            AND EN.HJID = E.ORDENPAGOANDERRORORSOLICITUD_1
            AND CAENLOTE > 80000;
            
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_RESOL_BECAS_ESTADO
(
   MULTIENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TANDA,
   SOLICITUD_ID,
   BECA_ID,
   ARCHIVO_TEMPORAL,
   NIF,
   ESTADO_ID,
   ESTADO,
   FECHA_ESTADO,
   AEAT,
   ACOGIMIENTO_ID,
   PERSONALES_ID,
   DECLARANTE_ID,
   ECONOMICOS_ID,
   GENERALES_ID,
   SITUACION_SOLICITUD_ID,
   TITULACION_ID
)
AS
   SELECT   CR.MULTIENVIO_ID,
            CR.LOTE,
            CR.CONVOCATORIA,
            CR.CURSO_ACADEMICO,
            S.SOLILOTE,
            S.HJID SOLICITUD_ID,
            S.SOLIIDSOLICITUD BECA_ID,
            SOLISQCIUDADANO ARCHIVO,
            S.SOLINIFCUENTA,
            SOLICODESTADOACT,
            es.nombre,
            SOLIFECHAESTADOACTITEM,
            S.SOLIINDAEAT,
            S.ACOGIMIENTO_SOLICITUDTYPE_HJ_0,
            S.DATOSPERSONALES_SOLICITUDTYP_0,
            S.DECLARANTE_SOLICITUDTYPE_HJID,
            S.ECONOMICOS_SOLICITUDTYPE_HJID,
            S.GENERALES_SOLICITUDTYPE_HJID,
            S.SITUACIONSOLICITUD_SOLICITUD_0,
            S.TITULACION_SOLICITUDTYPE_HJID
     FROM   UJI_BECAS.BC2_V_RESOL_CAB_MULTIENVIO CR,
            UJI_BECAS_WS_MULTIENVIO.ENVIOTYPEORDENPAGOANDERROROR_0 E,
            UJI_BECAS_WS_MULTIENVIO.SOLICITUDTYPE S,
            UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD ES
    WHERE       CR.ENVIO_ID = E.ORDENPAGOANDERRORORSOLICITUD_1
            AND E.ITEMSOLICITUD_ENVIOTYPEORDEN_0 = S.HJID
            AND CR.CONVOCATORIA = S.SOLICODCONV
            AND CR.CURSO_ACADEMICO = S.SOLIIDCURSO
            AND s.SOLICODESTADOACT = es.id;

CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_RESOL_AYUDAS
(
   ID,
   MULTIENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TANDA,
   SOLICITUD_ID,
   BECA_ID,
   ARCHIVO_TEMPORAL,
   NIF,
   ESTADO_ID,
   ESTADO,
   FECHA_ESTADO,
   TIPOAYUDA,
   CODAYUDA,
   CUANTIA_ID,
   CUANTIA,
   IMPORTE
)
AS
   SELECT   A.HJID ID,
            BE.MULTIENVIO_ID,
            BE.LOTE,
            BE.CONVOCATORIA,
            BE.CURSO_ACADEMICO,
            BE.TANDA,
            BE.SOLICITUD_ID,
            BE.BECA_ID,
            BE.ARCHIVO_TEMPORAL,
            BE.NIF,
            BE.ESTADO_ID,
            BE.ESTADO,
            BE.FECHA_ESTADO,
            A.AYUDTIPOAYUDA,
            A.AYUDCODAYUDA,
            C.ID,
            C.NOMBRE,
            A.AYUDCUANTIA IMPORTE
     FROM   UJI_BECAS.BC2_V_RESOL_BECAS_ESTADO BE,
            UJI_BECAS_WS_MULTIENVIO.AYUDATYPE A,
            UJI_BECAS.BC2_CUANTIAS C
    WHERE       BE.SOLICITUD_ID = A.AYUDA_SOLICITUDTYPE_HJID
            AND A.AYUDTIPOAYUDA || A.AYUDCODAYUDA = C.CODIGO(+)
            AND ESTADO_ID <> 10;
            
CREATE OR REPLACE FORCE VIEW UJI_BECAS.BC2_V_RESOL_DENEGACIONES
(
   ID,
   MULTIENVIO_ID,
   LOTE,
   CONVOCATORIA,
   CURSO_ACADEMICO,
   TANDA,
   SOLICITUD_ID,
   BECA_ID,
   ARCHIVO_TEMPORAL,
   NIF,
   ESTADO_ID,
   ESTADO,
   FECHA_ESTADO,
   SECUENCIA_ESTADO,
   CAUSA,
   SUBCAUSA,
   DEN_ID,
   DEN_ESTADO,
   DEN_NOMBRE
)
AS
   SELECT   EX.HJID_DEN ID,
            BE.MULTIENVIO_ID,
            BE.LOTE,
            BE.CONVOCATORIA,
            BE.CURSO_ACADEMICO,
            BE.TANDA,
            BE.SOLICITUD_ID,
            BE.BECA_ID,
            BE.ARCHIVO_TEMPORAL,
            BE.NIF,
            BE.ESTADO_ID,
            BE.ESTADO,
            BE.FECHA_ESTADO,
            EX.ESSOSECUENCESTADO,
            EX.ESCACODCAUSA,
            EX.ESCACODSUBCAUSA,
            DE.ID,
            DE.ESTADO,
            DE.NOMBRE
     FROM   UJI_BECAS.BC2_V_RESOL_BECAS_ESTADO BE,
            (SELECT   ES.*,
                      EC.HJID HJID_DEN,
                      ESCACODCAUSA,
                      ESCACODSUBCAUSA,
                      ESCAOBSERV,
                      ESCAPARAM1,
                      ESCAPARAM2,
                      ESTADOCAUSA_ESTADOSOLICITUDT_0
               FROM   UJI_BECAS_WS_MULTIENVIO.ESTADOSOLICITUDTYPE ES,
                      UJI_BECAS_WS_MULTIENVIO.ESTADOCAUSATYPE EC
              WHERE   EC.ESTADOCAUSA_ESTADOSOLICITUDT_0 = ES.HJID) EX,
            UJI_BECAS_WS_MULTIENVIO.ESTADOS_SOLICITUD E,
            UJI_BECAS.BC2_DENEGACIONES DE
    WHERE       BE.SOLICITUD_ID = EX.ESTADOSOLICITUD_SOLICITUDTYP_0
            AND EX.ESSOCODESTADO = E.ID
            AND EX.ESCACODCAUSA = DE.CAUSA(+)
            AND EX.ESCACODSUBCAUSA = DE.SUBCAUSA(+)
            AND EX.ESSOCODESTADO = DE.ESTADO(+)
            AND EX.ESSOSECUENCESTADO =
                  (SELECT   MAX (MX.ESSOSECUENCESTADO)
                     FROM   UJI_BECAS_WS_MULTIENVIO.ESTADOSOLICITUDTYPE MX
                    WHERE   MX.ESTADOSOLICITUD_SOLICITUDTYP_0 =
                               EX.ESTADOSOLICITUD_SOLICITUDTYP_0);


insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(115, 'La cuantia actual del aumento es menor que la de la solicitud o el aumento de cuantía anterior. Precisa proceso de Reintegro', 1, 1, 14, '01', '05');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(116, 'La cuantía actual del aumento es igual que la de la solicitud o el aumento de cuantía anterior', 1, 1, 14, '01', '06');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(117, 'Solicitud anterior en trámite', 1, 1, 14, '01', '07');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(118, 'No alcanzar la puntuación total obtenida por el último alumno seleccionado como suplente', 1, 1, 14, '01', '24');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(119, 'No identificación de sus NIF, por las Agencias Tributarias', 1, 1, 14, '04', '03');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(120, 'No identificación económica por las Agencias Tributarias', 1, 1, 14, '04', '04');

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(121, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, 14, '04', '12'); 

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(122, 'Solicitud que debe ser tramitada sin la ayuda de residencia, al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, 14, '04', '13'); 

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(123, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar', 1, 1, 14, '04', '14'); 

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(124, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, 14, '05', '12'); 

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(125, 'Solicitud que debe ser tramitada sin la ayuda de residencia, al poseer algún miembro de la unidad familiar un inmueble en la localidad donde cursa estudios', 1, 1, 14, '05', '13'); 

insert into bc2_denegaciones (ID, NOMBRE, ORGANISMO_ID, ACTIVA, ESTADO, CAUSA, SUBCAUSA)
values(126, 'Solicitud que debe ser tramitada por la convocatoria general al poseer algún miembro de la unidad familiar un inmueble en la Comunidad Autónoma donde cursa estudios', 1, 1, 14, '05', '14'); 
