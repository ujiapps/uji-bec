ALTER TABLE UJI_BECAS.BC2_BECAS
RENAME COLUMN BECAS_CONCEDIDAS_MEC TO BECAS_CONCEDIDAS_ANT;

ALTER TABLE UJI_BECAS.BC2_BECAS DROP COLUMN BECAS_CONCEDIDAS_CONS;


ALTER TABLE uji_becas.bc2_tandas 
    ADD CONSTRAINT bc2_tandas__UN UNIQUE ( curso_academico_id , convocatoria_id , tanda_id ) ;
    
ALTER TABLE UJI_BECAS.BC2_EXT_PERSONAS_ESTUDIOS
 ADD (ALGUNA_BECA_PARCIAL  NUMBER DEFAULT 0);
    
ALTER TABLE UJI_BECAS.BC2_BECAS
 ADD (ALGUNA_BECA_PARCIAL  NUMBER DEFAULT 0);

ALTER TABLE UJI_BECAS.BC2_TANDAS ADD (fecha_notificacion  DATE);

ALTER TABLE UJI_BECAS.BC2_BECAS ADD (fecha_notificacion  DATE);

ALTER TABLE UJI_BECAS.BC2_BECAS_HISTORICO ADD (fecha_notificacion  DATE);

CREATE TABLE bc2_textos 
    ( 
     id NUMBER  NOT NULL , 
     proceso_id NUMBER  NOT NULL , 
     organismo_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100) , 
     cabecera_ca VARCHAR2 (4000) , 
     cabecera_es VARCHAR2 (4000) , 
     pie_ca VARCHAR2 (4000) , 
     pie_es VARCHAR2 (4000) 
    ) 
;


CREATE INDEX bc2_textos_proc_IDX ON bc2_textos 
    ( 
     proceso_id ASC 
    ) 
;
CREATE INDEX bc2_textos_org_IDX ON bc2_textos 
    ( 
     organismo_id ASC 
    ) 
;

ALTER TABLE bc2_textos 
    ADD CONSTRAINT bc2_textos_PK PRIMARY KEY ( id ) ;


ALTER TABLE bc2_textos 
    ADD CONSTRAINT bc2_textos__UN UNIQUE ( proceso_id , organismo_id ) ;




ALTER TABLE bc2_textos 
    ADD CONSTRAINT bc2_textos_organismos_FK FOREIGN KEY 
    ( 
     organismo_id
    ) 
    REFERENCES uji_becas.bc2_organismos 
    ( 
     id
    ) 
;


ALTER TABLE bc2_textos 
    ADD CONSTRAINT bc2_textos_procesos_FK FOREIGN KEY 
    ( 
     proceso_id
    ) 
    REFERENCES uji_becas.bc2_procesos 
    ( 
     id
    ) 
;

CREATE TABLE BC2_LOG_NOTIFICACIONES
    ( 
     id NUMBER  NOT NULL , 
     beca_id NUMBER  NOT NULL , 
     tanda_id NUMBER  NOT NULL , 
     texto VARCHAR2 (4000)
    );

ALTER TABLE UJI_BECAS.BC2_LOG_NOTIFICACIONES
 ADD PRIMARY KEY
 (ID);
 
CREATE INDEX UJI_BECAS.LOG_BECA_I ON UJI_BECAS.BC2_LOG_NOTIFICACIONES
(BECA_ID)
LOGGING
NOPARALLEL;

CREATE INDEX UJI_BECAS.LOG_TANDA_I ON UJI_BECAS.BC2_LOG_NOTIFICACIONES
(TANDA_ID)
LOGGING
NOPARALLEL;

CREATE OR REPLACE TRIGGER UJI_BECAS.NOTIFICA_DENEGACIONES
BEFORE UPDATE OF FECHA_NOTIFICACION
ON UJI_BECAS.BC2_TANDAS 
FOR EACH ROW
DECLARE
  v_cabecera_ca  VARCHAR2(4000);
  v_pie_ca       VARCHAR2(4000);
  v_proceso      NUMBER;
  v_estado       NUMBER;
  v_organismo_id NUMBER;
  v_organismo    bc2_organismos.nombre%type;
  v_orden        NUMBER;
  v_asunto       VARCHAR2(200)  := 'Comunicació de denegació de beca ';
  v_nombre       VARCHAR2(1000);
  v_dni          VARCHAR2(100);
  v_intro        VARCHAR2(1000);
  v_firma        VARCHAR2(1000);
  v_detalle      VARCHAR2(4000);
  v_texto        VARCHAR2(4000);
  v_fecha        DATE;
  
  hay_error     EXCEPTION;
  
  CURSOR lista_becas (p_tanda NUMBER) IS
    SELECT b.id, s.persona_id, s.numero_beca_uji
      FROM bc2_becas b, bc2_solicitantes s
      WHERE s.id = b.solicitante_id AND
            b.tanda_id = p_tanda;
      
  CURSOR lista_motivos (p_beca_id NUMBER, p_orden NUMBER) IS
    SELECT b.beca_id, a.causa || a.subcausa codigo, a.nombre denegacion
      FROM bc2_becas_denegaciones b, bc2_denegaciones a
      WHERE a.id = b.denegacion_id AND
            b.beca_id = p_beca_id AND
            b.orden_denegacion = p_orden
      ORDER BY codigo;
 
BEGIN
   
   IF :old.fecha_notificacion IS NULL AND :new.fecha_notificacion IS NOT NULL THEN
   
       BEGIN
         select distinct proceso_id, estado_id
           INTO v_proceso, v_estado
           FROM bc2_becas
           WHERE tanda_id = :new.id;
           
           IF v_proceso = 1 THEN
             v_orden := 1;
           ELSE
             v_orden := 2;
           END IF;
       EXCEPTION
         WHEN TOO_MANY_ROWS THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'La tanda te beques en diferent procès i/o estat.');
           RAISE hay_error;
       END;
       
       BEGIN
         SELECT o.id, o.nombre
           INTO v_organismo_id, v_organismo
           FROM bc2_convocatorias c, bc2_organismos o
           WHERE o.id = c.organismo_id AND
                 c.id = :new.convocatoria_id;
                 
         v_asunto := v_asunto || '(' || v_organismo || ')';       
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troba l''organisme.');
           RAISE hay_error;
       END;
     
       BEGIN
         SELECT cabecera_ca, pie_ca
           INTO v_cabecera_ca, v_pie_ca
           FROM bc2_textos
           WHERE proceso_id = v_proceso AND
                 organismo_id = v_organismo_id;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
             VALUES(HIBERNATE_SEQUENCE.Nextval, null, :new.id, 'No es troben textes per a l''organisme ' || v_organismo || ' i el procès ' || v_proceso || '.');
           RAISE hay_error;
       END;   

       v_intro := '- Data ' || TO_CHAR(SYSDATE,'dd/mm/yyyy') || CHR(10) || '- R/n  Servei de Gestió de la Docència i Estudiants' || CHR(10) || '- ' || v_asunto || CHR(10);
       v_firma := 'La secretària del Jurat de Selecció de Becaris' || CHR(10) || 'Leticia Falomir Abillar' || CHR(10);

       FOR x IN lista_becas (:new.id) LOOP
         SELECT nombre, identificacion
            INTO   v_nombre, v_dni
            FROM   BC2_EXT_PERSONAS
            WHERE  id = x.persona_id;

            v_detalle := NULL;

            FOR y IN lista_motivos (x.id, v_orden) LOOP
               v_detalle := v_detalle || y.codigo || '. ' || y.denegacion || CHR(10);
            END LOOP;
            
            v_texto := v_intro || CHR(10) || CHR(10) || v_nombre || CHR(10) || CHR(10) ||
                       v_cabecera_ca || CHR(10) || CHR(10) ||
                       v_detalle || CHR(10) || CHR(10) ||
                       v_pie_ca || CHR(10) || CHR(10) || v_dni || CHR(10) || CHR(10) ||
                       v_firma;      
            
            BEGIN       
              seu.notifica.envia_notificacion(2, x.persona_id, v_asunto, v_texto);
            EXCEPTION
              WHEN OTHERS THEN
                INSERT INTO BC2_LOG_NOTIFICACIONES (ID, BECA_ID, TANDA_ID, TEXTO)
                VALUES(HIBERNATE_SEQUENCE.Nextval, x.id, :new.id, 'No s''ha pogut notificar la beca.');
            END;
            
            v_fecha := SYSDATE;
            
            UPDATE bc2_becas
              SET fecha_notificacion = v_fecha
              WHERE id = x.id;
              
            INSERT INTO bc2_becas_historico (ID, BECA_ID, PROCESO_ID, ESTADO_ID, TANDA_ID, FECHA, USUARIO, CONVOCATORIA_ID, FECHA_NOTIFICACION)
              VALUES (hibernate_sequence.nextval, x.id, v_proceso, v_estado, :new.id, SYSDATE, 'uji_becas', :new.convocatoria_id, v_fecha);
              
       END LOOP;

   END IF;
EXCEPTION
     WHEN hay_error then
       :new.fecha_notificacion := NULL;
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR(-20000,'Error others: ' || sqlerrm);
END NOTIFICA_DENEGACIONES;

INSERT INTO BC2_DENEGACIONES
VALUES (102, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet la resolució de la convocatòria.', 1, 1, 10, '03', '28');

INSERT INTO BC2_DENEGACIONES
VALUES (103, 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs.', 1, 1, 10, '03', '29');

INSERT INTO BC2_DENEGACIONES
VALUES (104, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 1, 1, 10, '03', '30');

INSERT INTO BC2_DENEGACIONES
VALUES (105, 'No arribar a la nota mitjana mínima exigida en la convocatòria.', 1, 1, 10, '03', '31');

INSERT INTO BC2_DENEGACIONES
VALUES (106, 'No haver aprovat el nombre o percentatge mínim d''assignatures o crèdits establerts en les bases de la  convocatòria.', 1, 1, 10, '03', '32');

INSERT INTO BC2_DENEGACIONES
VALUES (107, 'Haver sigut beneficiari/ària de beca el nombre màxim d''anys que permet la resolució de la convocatòria.', 1, 1, 10, '05', '03');

INSERT INTO BC2_DENEGACIONES
VALUES (108, 'No arribar a la nota mitjana mínima exigida en la convocatòria.', 1, 1, 10, '05', '04');

INSERT INTO BC2_DENEGACIONES
VALUES (109, 'No haver aprovat el nombre o percentatge mínim d''assignatures o crèdits establerts en les bases de la  convocatòria.', 1, 1, 10, '05', '05');

INSERT INTO BC2_DENEGACIONES
VALUES (110, 'No estar matriculat/ada en el nombre mínim exigit de crèdits en aquest curs.', 1, 1, 10, '05', '06');

INSERT INTO BC2_DENEGACIONES
VALUES (111, 'No estar matriculat en el nombre mínim exigit de crèdits en el curs anterior o últim realitzat.', 1, 1, 10, '05', '07');

INSERT INTO BC2_DENEGACIONES
VALUES (112, 'No quedar suficientment acreditada la composició de la unitat familiar.', 1, 1, 10, '01', '61');

UPDATE BC2_DENEGACIONES
  SET ACTIVA = 0
  WHERE ID IN (21,22,41,45,37,38,39,40,7,23);