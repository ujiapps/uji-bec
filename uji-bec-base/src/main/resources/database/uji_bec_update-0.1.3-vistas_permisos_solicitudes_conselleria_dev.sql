begin

declare 

cursor solicitudes
is 
  select s.nif, s.personales_id,
         decode(AUTORIZACION,'S',1,0) autorizacion, IDSOLICITUD, 
         DATOSESTUDIOS_SOLICITUD_HJID,
         DATOSFAMILIARES_SOLICITUD_HJ_0, 
         DATOSPERSONALES_SOLICITUD_HJ_0, 
         SITUACIONESDECLARADAS_SOLICI_0, 
         SOLICITUD_SOLICITUDES_HJID
  from uji_becas_ws_solicitudes_con.ws_v_con_solicitudes s, uji_becas_ws_solicitudes_con.solicitud ss 
  where s.personales_id = SS.DATOSPERSONALES_SOLICITUD_HJ_0;

vnif_soli        varchar2(100);
vid_solicitante  number;
vid_beca         number;
vestudio_id      number;
vestudio_mec     number;
vcontador        number;
vpersona_id      number;
vnum_beca_uji    number;
vtipo_familia_id number;
vminus33         number;
vminus65         number;
vorfandad        number;
vingresos        number;
vtelefono        varchar2(20);
vemail           varchar2(100);
vmovil           varchar2(20);
  


begin

    vcontador := 0;
--  delete bc2_becas where convocatoria_id = 3;   
--   commit;
  For x in solicitudes loop
     begin
     
    

        begin
             select id 
             into vpersona_id
             from bc2_ext_personas
             where identificacion =decode(x.nif,'04229849B','04229849','X6543007J','X06543007J',x.nif);
        exception when others 
            then raise_application_error(-20000, x.nif|| ' nif no encontrdo ');
        end;
        
        begin 
            select id 
            into  vid_solicitante
            from bc2_solicitantes
            where persona_id = vpersona_id;
        exception when no_data_found
          then 
             vid_solicitante:=  uji_becas_ws_multienvio.hibernate_sequence.nextval;     
        
                 
       select max(numero_beca_uji)+1 into vnum_beca_uji
       from bc2_becas b, bc2_solicitantes s
       where b.solicitante_id = s.id
         and s.curso_academico_id = 2012;    
         
        select telefono, email,movil
       into vtelefono,  vemail,vmovil
       from uji_becas_ws_solicitudes_con.datospersonales
       where HJID = x.personales_id;
      
       select nvl(tipofamilianumerosa+1,1) ,nminusvaliasuperior33, nminusvaliasuperior65, decode(orfandadsolicitante,'S',1,0),
              decode(hjid,82770,1200,93311,1545,0)
       into  vtipo_familia_id, vminus33, vminus65,vorfandad, vingresos
       from uji_becas_ws_solicitudes_con.situacionesdeclaradas
       where hjid =x.situacionesdeclaradas_solici_0;
       
               
          begin
       
           insert into bc2_solicitantes
           (id, curso_academico_id, persona_id, numero_beca_uji, profesion_sustentador, telefono1, 
                telefono2, email, tipo_familia_id, numero_miembros_computables, numero_hermanos, 
                numero_hermanos_fuera, numero_minusvalia_33, numero_minusvalia_65, orfandad, 
                unidad_familiar_independiente, ingresos_netos, ingresos_extranjero)
            values(vid_solicitante,2012,vpersona_id,vnum_beca_uji,null, vtelefono,
                vmovil, vemail,vtipo_familia_id,0,0,
                0,vminus33, vminus65,vorfandad,
                0,0,vingresos);
            
          exception when others 
          then raise_application_error(-20000,'error insertar solicitante: '||x.nif||'---'||
               vid_solicitante||'---'||2012||'---'||vpersona_id||'---'||vnum_beca_uji||'---'||null||'---'||vtelefono||'---'||
               vmovil||'---'|| vemail||'---'||vtipo_familia_id||'---'||0||'---'||0||'---'||
               0||'--33'||vminus33||'-65'||vminus65||'---'||vorfandad||'-orf'||
               0||'---'||0||'-ing'||vingresos||'---'||sqlerrm);
          end;
      
      end;
      
       vid_beca:= uji_becas_ws_multienvio.hibernate_sequence.nextval;       
       
 
       
         begin
            select  valor_uji into vestudio_id
            from uji_becas_ws_solicitudes_con.datosestudios e, bc2_diccionario d
            where x.DATOSESTUDIOS_SOLICITUD_HJID = e.hjid
             and d.clase_uji = 'Estudio'
            and e.codtitulacion = d.valor_origen;
        exception when no_data_found
        then  vestudio_id :=null;
        end;
        
       
        
        insert into bc2_becas
        (id, solicitante_id, convocatoria_id, proceso_id, estado_id, tanda_id, 
             tipo_identificacion_id, identificacion, codigo_beca, codigo_archivo_temporal, 
             estudio_id, tipo_matricula_id, 
             autorizacion_renta)
        values(vid_beca, vid_solicitante,3,1,1,null,
              decode(upper(substr(x.nif,1,length(x.nif)-1)),lower(substr(x.nif,1,length(x.nif)-1)),1,2),x.nif,null,x.idsolicitud,
              vestudio_id,1,x.autorizacion);  
 
        vcontador := vcontador+1;   
       commit;
   end;
  
  end loop;
  raise_application_error(-20000, 'total : '||vcontador);
end;
end;