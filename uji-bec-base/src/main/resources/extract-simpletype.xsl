<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:jxb="http://java.sun.com/xml/ns/jaxb">
  <xsl:output method="xml" media-type="text/xml" indent="yes"/>

  <xsl:template match="/">
    <x>
      <xsl:for-each select="//xs:element[xs:simpleType/xs:restriction/xs:enumeration]">
        <jxb:bindings>
          <xsl:attribute name="node"><xsl:value-of select="concat('//xs:element[@name=',@name,']/xs:simpleType')" /></xsl:attribute>
          <jxb:typesafeEnumClass name="{@name}"/>
        </jxb:bindings>
      </xsl:for-each>
    </x>
  </xsl:template>
</xsl:stylesheet>
