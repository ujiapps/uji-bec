package es.uji.apps.bec.resoluciones.batch;

import es.uji.apps.bec.dao.dao.CursoAcademicoDAO;
import es.uji.apps.bec.exceptions.WsDatosEnviadosParaBajarBecaErroneosException;
import es.uji.apps.bec.model.CorreoElectronico;
import es.uji.apps.bec.model.CursoAcademico;
import es.uji.apps.bec.webservices.LotesResueltosMinisterio;
import es.uji.apps.bec.webservices.ministerio.multienvio.EnvioType;
import es.uji.apps.bec.webservices.ministerio.multienvio.MultienvioType;
import es.uji.commons.messaging.client.MessageNotSentException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.util.Date;

public class ResolucionesProcess implements Runnable {
    public static Logger log = Logger.getLogger(ResolucionesProcess.class);

    private static final int TIEMPO_12_HORAS = 1000 * 60 * 60 * 12;
    private static final int TIEMPO_ESPERA_ENTRE_CONSULTAS = TIEMPO_12_HORAS;
    private static final String CONVOCATORIA_GENERAL = "AE";
    private static final String[] CONVOCATORIAS = new String[]{CONVOCATORIA_GENERAL};

    private LotesResueltosMinisterio lotesResueltosMinisterio;
    private CursoAcademicoDAO cursoAcademicoDAO;

    public ResolucionesProcess(LotesResueltosMinisterio lotesResueltosMinisterio, CursoAcademicoDAO cursoAcademicoDAO) {
        this.lotesResueltosMinisterio = lotesResueltosMinisterio;
        this.cursoAcademicoDAO = cursoAcademicoDAO;
    }
    public static void main(String[] args) throws Exception {

        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        LotesResueltosMinisterio lotesResueltosMinisterio = context.getBean(LotesResueltosMinisterio.class);
        CursoAcademicoDAO cursoAcademicoDAO = context.getBean(CursoAcademicoDAO.class);

        ResolucionesProcess resolucionesProcess = new ResolucionesProcess(lotesResueltosMinisterio, cursoAcademicoDAO);

        log.info("START ResolucionesProcess " + new Date());

        new Thread(resolucionesProcess).start();
    }

    public void run() {
        while (true) {
            try {
                new CursoAcademico().setCursoAcademicoDAO(cursoAcademicoDAO);

                log.info("Iniciando proceso de descarga");

                for (CursoAcademico cursoAcademico : CursoAcademico.getCursoActivo()) {
                    Long cursoAca = cursoAcademico.getId();

                    for (String convocatoria : CONVOCATORIAS) {
                        try {
                            JAXBElement<MultienvioType> element = lotesResueltosMinisterio.obtenerListaLotesResoluciones(cursoAca, convocatoria);
                            MultienvioType multienvioType = element.getValue();

                            for (EnvioType envioType : multienvioType.getEnvio()) {
                                String lote = envioType.getCabeceraEnvio().getCaenLote();

                                Long idMultienvioProcesado = lotesResueltosMinisterio.descargarLoteResoluciones(lote, convocatoria, String.valueOf(cursoAca));

                                if (idMultienvioProcesado == null) {
                                    log.info("El lote " + lote + " del curso " + cursoAca + " y convocatoria " + convocatoria + " ya existe en la base de datos");
                                } else {
                                    log.info("Cargado lote " + lote + " del curso " + cursoAca + " y convocatoria " + convocatoria);

                                    try {
                                        CorreoElectronico.enviaCorreoLoteMecDescargado(Long.parseLong(lote));
                                    } catch (MessageNotSentException e) {
                                        log.error("No se ha podido enviar el aviso para el lote " + lote, e);
                                    }
                                }
                            }
                        } catch (IOException e) {
                            log.error("No es posible conectar con el WebService del ministerio", e);
                        } catch (WsDatosEnviadosParaBajarBecaErroneosException e) {
                            log.error(e.getMessage(), e);
                        } catch (JAXBException e) {
                            log.error("Error procesando respuesta XML del ministerio", e);
                        } catch (ServiceException | SOAPException e) {
                            log.error("Error en la descarga del lote", e);
                        }
                    }
                }

                log.info("Finalizado proceso de descarga");

                Thread.sleep(TIEMPO_ESPERA_ENTRE_CONSULTAS);
            } catch (InterruptedException e) {
                log.error("Error en la ejecución del thread principal", e);
            }
        }
    }
}