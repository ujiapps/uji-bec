package es.uji.apps.bec.descarga.solicitudes.batch;

import es.uji.apps.bec.dao.dao.BecaDAO;
import es.uji.apps.bec.services.ResultadoDescarga;
import es.uji.apps.bec.webservices.DescargaSolicitudes;
import es.uji.apps.bec.webservices.WebserviceMinisterio;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class DescargaSolicitudesBatch {
    private WebserviceMinisterio webserviceMinisterio;
    private BecaDAO becaDAO;

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        DescargaSolicitudes descargaSolicitudes = context.getBean(DescargaSolicitudes.class);

        Integer cursoAcademico = Integer.parseInt(args[0]);
        String convocatoria = args[1];
        Boolean descargadas = Boolean.parseBoolean(args[2]);

        List<ResultadoDescarga> resultadosDescarga = descargaSolicitudes.descarga(cursoAcademico, convocatoria, descargadas);
    }
}